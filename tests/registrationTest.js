const chai = require('chai');
const chaiHttp = require('chai-http');
const winston = require('winston');
const request = require('supertest');
const should = chai.should();
chai.use(chaiHttp);

const url = 'http://127.0.0.1:3000';

describe('API Register', () => {

    it('Test error register', (done) => {
        request(url)
            .post('/api/register')
            .send({params: {
                login:"79381453173",
                password:"123456"
            }
            })
            .end(function(err, res) {
                if (err) {
                    throw err;
                }

                console.log(res.body);

                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('result');
                res.body.should.have.property('status');
                res.body.should.have.property('message');
                res.body.status.should.equal(0);

                res.body.result.should.be.a('object');
                res.body.result.should.have.property('data');
                res.body.result.should.have.property('error');

                res.body.result.error.should.be.a('array');
                res.body.result.error.should.have.lengthOf(4);
                done();
            });
    });

    it('Test success register', function(done) {
        request(url)
            .post('/api/register')
            .send({params: {
                    email: "testr@ddd.sds",
                    phone:"123456",
                    first_name:"asdad",
                    last_name:"asdsads",
                    password:"123456",
                }
            })
            .end(function(err, res) {
                if (err) {
                    throw err;
                }

                console.log(res.body);

                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('result');
                res.body.should.have.property('status');
                res.body.should.have.property('message');
                res.body.status.should.equal(1);

                res.body.result.should.be.a('object');
                res.body.result.should.have.property('data');
                res.body.result.should.have.property('error');

                res.body.result.error.should.be.a('array');
                res.body.result.error.should.have.lengthOf(0);

                res.body.result.data.should.have.property('token');

                done();
            });
    });

});
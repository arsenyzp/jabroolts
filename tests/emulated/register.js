const axios = require('axios');
const moment = require('moment');
const json2csv  = require('json2csv');
const fs = require('fs');

const _axios = axios.create({
    //baseURL: "http://127.0.0.1:3000",
    baseURL: "http://jabrool-dev.done-it.net:3030",
    //baseURL: "http://127.0.0.1:3030",
    headers: {
        "Content-Type": "application/json"
    }
});
_axios.defaults.timeout = 2500;

let getEmail = () => {
    const chars = 'abcdefghijklmnopqrstuvwxyz1234567890';
    let string = '';
    for(let ii=0; ii<15; ii++){
        string += chars[Math.floor(Math.random() * chars.length)];
    }
    return string + '@jabrool-test.com';
};

let getPhone = () => {
    let string = '';
    for(let ii=0; ii<21; ii++){
        string += Math.floor(Math.random() * 10);
    }
    return string;
};

function register(ax) {
    return ax.post('/api/register', {params: {
            email: getEmail(),
            phone: getPhone(),
            first_name: "Test" + moment().format("DD-MM-YYYY"),
            last_name: "Test"  + moment().format("hh:mm:ss"),
            password: "123456",
        }
    });
}

function uploadAvatar(ax, user) {
    return ax.post('/api/upload_image', {params: {
            image: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxASEhAREBAWFhAXEhcSGBcVEhgYFRYXFRUXFhUYFhgYHSghGB0nGxYYIjEiJSkrLi4uGiAzODMtNyguLysBCgoKDg0OGhAQGy8lICUtLS0yMy0tLS01LSs3LS0vLy0vLS01LS0tKy0tLS0tLS0tLi0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABwEEBQYIAwL/xABIEAABAwIBCAUHBwsDBQAAAAABAAIDBBEFBgcSITFBUWETInGBkRQjMkJSYnIXVIKhorHTFjNDU2NzkpSywdIkNNGTo8Pw8f/EABsBAQACAwEBAAAAAAAAAAAAAAABBAIDBQYH/8QAMBEAAgIBAwEGBAYDAQAAAAAAAAECAxEEEiExBRNBUWGRFCJxsRVCUoGhwTPR8Ab/2gAMAwEAAhEDEQA/ANEREWR9FCIiAIiIAiIgCIiAIiIAt1yWyY6bDcSqS3rBlotWvzREshHbYN7itMYwkgAXJNgBtJOwLpLJzBm09HDTEA2j0X8HOcLyHvcSoOT2tq3TCKj1b/hcnNaK6xOjMM00Ltscj49e/QcRfvtdWqHUjJSSaCIikyCIiAIiIAiIgCIiAIiIQz6REQ0HyiIhYCIiAIiIAiIgCIiAIiqgNszYYR5RXRFwvHCOndq1XbqYO3SIP0Sp/Cj/ADO4P0VI6ocOvO+41a9Bl2t8TpHsIUgBQzxnat/e6h46Lj/f8kC52KDosQkcNkrGy99tB31s+taapcz3UF46WoHqvdEex40m+BYfFRGh6Psy3vNNF+XHsERFJ0AiIgCIiAIiIAiIgCIiEM+kRENB8oiIWAiIgCIiAIiIAiIgCucNonzyxQs9OR7Yx2uNr923uVspBzN4R0lU+pcOrC2zdX6SQEC3Y3S8QoK2rvVNMp+S/nwJjoKRsMUcTBZjGNY0cmiw+5YKryqjjxKHDza8kDpNLg+/Ub3tbIfDitkJXMWVGPPmxGesjdZwn0ojwERDYj4MBI5lb6Ke8b+h8+ut24fmTxnGoOmw+qbbW1nTDtjOmfqBHeueV0tgGJx11JFO0dSWPrN4EjRew9hBHcucsRpDDLLC7bHI6M336Li2/wBS04xwz1PYN2Yyh+5bIiIehCIiAIiIAiIgCIiAIiIQz6REQ0HyiIhYCIiAIiIAiIgCIiALoHNng/k1BFpC0knn38bvtojuaGjxUK5J4T5VV08Fuq54L/gb1n/UCO0hdJsAAsNihnne3b+I1L6v+jXc4eL+S4fUyg2eWdEzjpydQEdlye5czWUuZ+cVuaWkadgNQ/62R/8AkURrq6OG2vPmeM1Mszx5EsZjcfs+ahedTvPRfENUjR2izrcnLEZ1qDosRlcBZsrGTDhrGg77TCe9aVhGIvpp4aiP043h4522tPIi4PapWzthlRTUFfFrjcC2/uyND234WLXDtKq6uvbPcvE7X/n9RtvUX48EXIiKqe7CIiAIiIAiIgCIiAIiIQz6REUmg+UW55G4Ng9doxvqaiGq2dG58Vnn9k7o+t2bfvW6fJDRfOKjxi/DSUXF4kjnx7c0sllN+xDCKZ/kgovnFR4xfhp8kFF84qPGL8NY5MvxrTevsQwimf5IKL5xUeMX4afJBRfOKjxi/DTI/GtN6+xDCKZ/kgovnFR4xfhp8kFF84qPGL8NMj8a03r7EMKqmb5IKL5xUeMX4afJBRfOKjxi/DTI/GtN6+xjcyeEf7iscOEDD4OkP9I7ipWWPyfweOkgjp4r6DAdbraTiSXEusALkkqxy7xfyShqpgbPEZYz45OozwLr9ylLc8I8xrdR31srPD+iAMu8W8qr6qYG7OkMbPgj6jbcjbS+ksAiLuxjtSRwJPLyVUg5K4h5ThVfQPPXgb5XDx0GuDpGjsN/4+Sj1ZPJvFTS1MM21odovb7UTxoyN72k99itd9e+DRv0lzptjNeDKKi9aqIMe9gOkGuLQfaAOo941rzC4x9TjJNZRRVW0YFkDiFTYiLooz6812auTbaR8Lc1v+DZp6SOxqZHzO4DzcfgDpHxQo39p6enhyy/JckMsYSQACXHUANZPYN6z+GZE4lPYspXhp3yWjHb1rHwCnvDMGpqcWp4I4+OgwAntO096vSQNZ2KMnJt7em/8ccfUh2gzRVTvz1RFHyYHSH69ELPUmaOkbYy1EzzwGiwfcT9ayuP5ysNprtEvTSDVoQAP1833DR435KPcZzw1slxTRRwN4nzknbcgNH8JW6Gnsn0Ry7u2r31nj6Ehw5t8KZrdAXDi+Z9vqcAsfiAyapbiQUukNrReV/8LdIqFMUx2rqSTU1MsnJzzodzB1R3BY8KzHQ/qZRn2pa/zSf7slXEsucBbcU+EtlO4ugijYe83d9lani+WLZbiHDaOEa9bYA54+kbD7K1ZUViOlrj4FV6/UP8zX7mS8qf7v8A0o/8UXki291D9KI+M1H65e7LMFSNkXnTnp9GGt0poNgfe8zBzJ/ODt18zsUcKqTrjNYkivCcovKOrsIxaCqjbNTytkjO9p2Hg4bWnkdavlypgWO1NHJ0tNKWO3ja14G57djh9Y3WU3ZF5zKas0Yp7QVR1AE+bkPuOOw+6dfC65l2llDlcovVXqXD6m+Kqt62mbKx8bidFzS06Li12v2XNILTzBUC5dYZimHS/wC9qn0zz5uXyiT+B9nWD/qO0bwNVNXePGcM2WWbFnB0Ei5V/KCu+e1P8zL/AJKn5QV3z2p/mZf8lZ+Bl5mj4uPkdVouVPygrvntT/My/wCSflBXfPan+Zl/yT4GXmPi4+R1Wojz84vqpaRp2k1DxfcLsjvyJ0/4VGH5QV3z2p/mZf8AJWdVVySu0pZHyPtbSe9z3WGwXcSba1tq0jhNSbMLNQpRwkeKIqq8VCi+mMJIaASSbAAXJJ2AAbStqyPyCrK8h7R0VNvleDYj9m3a/t2c1NuSuRNFQAGGPSm3yyWdIeIB2NHIW71Wu1Ua+FyzfXRKXJF+SmbCsnDXVX+nj4EXlI+H1NXta+SlXJ/I6io7GGEGT9Y/rSdxPo91lnrL5lla0FznBrQLkkgAAbyTsXJlLdJs7U9XdKtVuXCWD6svKrqo4mukle1jGi5c9wa0DmTqCjnKvO3Tw6UdC0Ty7NM3ELTyI1yd1hzUR49lDV1r9Oqmc+xu1uyNvwsGodu3mrFWknPl8I59mojHhcktZTZ3qaLSZRRmd+zTddsQ7PWf3WHNRZlDlZXVpPlM7iz9W3qxD6A29rrlYRUXQr08IdEU53Sl1KqivsNwuWfTLBaNg0pJHao428Xu+4C5O4FWbgLmxuN3/wA3LdldDXhnyiIpICIiEl6iIoMiyREUmAREQG/ZF5zqmk0YqnSnphqFz52Me44+kPdd3EbFMVPV0OKUzw1zZoHjRc3WHNO2zhtY4bRsO8LmBXuD4tUUsgmppXRyDeDqI4OadThyKqXaVS5jwyxXe1xLlGey8yKlw6S4u+lefNycPcktsdz2HxA1NTbk5nCo8RjNHiTGRveNA3/MyX2aJOuN19l99rG60DL7ImXD5NJt30jz1JN7SdehJbfwOw/UppuedlnX7iytY3Q6GooiK0VwiK9wfC5qqZkFOzSlcdQ3Ab3OPqtG8qG0llkpZ4R4UlM+V7Y4mOfI46LWtF3OPIBTHkPmpjj0Z8RAkk2iAG8bP3h/SHl6PbtW1ZD5EwYfGLAPqXDryka/hZ7LeW/etqXMv1blxDoXqtOlzI+WMAAAFgBYAbBbgqkrHY5jlPRxGapkDGbBvc4+y1o1uPIKEcsc51VV6UdPpU9OdVgfOvHvvHoj3W+JWiqidnQ3WWxh1JMyvzj0dFpRtPT1I1dGw6mn9o/Y3sFzyULZUZYVle49PLaK9xEzqxt4avWPN1+Vlr6qunVpoV89WULL5TCoiu8Mw6aokbDBG6SV2xrRr5k7gBxOpb28dTUlktVvuRObWeq0Z6q8FJ6WvVJIB7IPoN949w3reMh82ENNoz1mjNUizg214ozyB9Nw4nuG9YTO7lxfTw+ldq2TvB/7LT/V4cVTlqHZLZV7llVKC3T9jU8ucoYZNGioGiPD4Xag39M8bZHE63DgTt2ndbUFVUVqEFFYRXlJyeQiIszEIiISXqIigyLJERSYBERAEREAW3ZN5cywRmlqmeU0LhoGJ56zW/s3HZbaGnVqFtHatRRYTgpLDMoycehnMfwmJnn6OTpaNzrAn85C4/o527Wng7Y62pYNekUzm30SRcFptvadoPEahq5L5YwkgNBLiQAALkkmwAA2kncpimlyw+XwXGGUEtRLHBCwule7Ra0ceJ4AbSdwC6LyFyPiw6HRFnVDrGWS3pH2W8GDcO9Y/NpkS2gi6WYA1kjRpHUejbt6Np+87zyAW7rl6nUb3tj0L1FO1ZfUqtTy5y4gw5mj+cqXC7IgftPPqt+s7t9rLONl6ygaYYdF9Y4amnW2MH15P7N39igKsqpJXvlleXyPdpOc43JJ4/8AG5Tp9Nv+aXT7i6/bwupd49jlRWSmapkL36wBsaweyxvqj6zvusaiLqJJLCKDbfUKq+4IXPc1jGuc9xsGtBLiTsAA1kqXMh81FtGfEhc6iKcHUP3rht+EauJOxa7bo1rMjOuuU3waVkZkLVYgQ9o6OmvYzOGo8RG31zz2DjuU8ZM5M0tBH0dPHa9tJ51yPI3udv7Ng3BZeKNrQGtADQLAAAAAbAANgWt5e5Wx4dBpmzp33bFGT6Thtcd+i24uewb1y7Lp3SwvYvwrjWsmFzp5b+Rx+TU7v9XI3aP0LD63xH1R37tcCEr2rquSaR8sry+V7i5zjtJP9uA3Cy8F06KVXHHiUbbHN5CIi3GoIiIAiIhJeoiKDIskRFJgEREAREQBERAFL+ZzIz0cRqG7vMNI3bDKe3Y3lc7wtLzdZKHEKoNeD5NHZ8p4i/VjB4use4FdHxRhoDWgBoAAAFgANQAG4KhrL8fIv3Lmnqz8zPpaZnGy3Zh8WhHZ1ZI06DTrDBs6R44DcN55AkZPLXKiLD6czO60h6scd9b3/wBmjaTw5kLm7FMRlqJZJ53l0rzpOP3ADcANQG4BaNNp973S6G2+7asLqeNVUPke+SRxdI5xc5zjcuJ2kryVVRdXoc98hERSQZbJXGDR1dPUjYx40hxY7qyD+Em3Oy6khkDg1zTdpAII2EHWCuRlP+ZvHPKKEQuN5ac9Fz6M64j4Xb9Bc/XV8KZc0s+dpvb9httsuW8qcanrKmSao1PuWBm6JrSQIx2a78TcrqUrnLOphHk2Iz2FmS2qG8OvfT+2HHvWvQtb2mZ6pPbwagiqqLqFAIiIAiIgCIiEl6iIoMiyREUmAREQBERAF6QQue5rGNLnucGNaNrnONgB3lealHMnk10kr66RvUiJjivvkI6zvotNu1x4LXbYq4OTM64b5YJLyHycbQUscAsZPTlcPWkI63cPRHIBZfE8Qip4pJ5nBsbGlzjwA+87gN6uSoJzv5X+UzeRwO/08LuuQdUko1Hta3Z234BciquV0/udGclXE1XLDKSXEKl08lwz0Y2X1Rs3DtO0nj2BYNVVF2YxUVhHMbbeWVW85L5tKitpHVIkEbnHzLXtNpAL3c4jW0E7DY7DuIVpm4yPOIT3eCKWIgyH2jtEYPE7+A7Qui4ow0BrQA0AAACwAGoADgqmp1Lg9sepZop3cyOU8XwmelkMNRE6OQbjsI4tI1OHMKxXVeOYFTVkZiqYg9m6/pNPFjhraeYUJ5Z5sKmk0paa89MNeoedYPeaPSHvN7wFNOrjPiXDIs07jyuhoK2/NXjvktfGHOtFN5h/C7j5t3c6w7HFaeqg8NqszipxcWaYycXk67CjDPrhGnTwVTRrieY3fBLaxPY9rR9Irb8g8c8tooJybyaOhJ+8Z1Xdl/S7HBXuU2FiqpainP6SJzRyda7D3OsVxq267FnwZ0prfA5XVFVzSCQ4WcDYg7QRtBVF3DlBERAEREAREQkvURFBkWSIikwCIiAIiID3oaR80kcMYvJI9sbR7zjYLqTJ7CGUlNDTR+jGwNv7TtrnHmXEnvUQ5kMB6WolrHjqQjQZ+8eNZ7mH7amevrI4Y5JZXBsbGl7idwaLlcvWWbpbF4fcv6aGI7mahnUysNFTdHE61VMC1ltrG+vJy22HM8iuell8qsekrqmWpk1BxsxvsRtvoN8NZ5krEK5p6e7h6la6zfILJZPYNNWVEdNAOu86yR1WNHpPdyA8TYbSFj42FxDWglxIaABckk2AA3kldE5tsjxQQaUgBqpLOkPsjdG08BvO833WtOouVcfUU1b36GfydwSGjp46eEdVg1k+k9x9JzuZP/G5WuP5W0NE+OOpnDHvOoWLiB7Tg30W31XP9itbzg5xo6LSgprSVlrHeyG+9/F3u+POCq2rkmkfLM9z5HHSc5xuSf8A3duVGnTSs+aRasvUPlidYUtSyRrXxva9jhcOa4OaQd4I1Fetly5k5lRWULtKllLWk3MbutE74mf3FjzUx5J50qSp0Y6n/TznV1j5px91+7sdbtKwt0s4crlGVd8ZcPqeuWmbSmrNKWG0FSdek0ebef2jRv8AeGvjdQlj+AVVFJ0VTEWH1XbWPHFjth+8bwF1Q03VpiuFwVMboaiJskZ2tcL9hG8HmNamnVSr4fKFlEZcrqQ1mPx3o6iWje7qTDTZ+8YNYHxM/oCnBQtlFm3qaGVlZhhdK2N4lEZ/Os0Tew/WN3W9K3tKX8JrmTwxTsvoyMDwDtFxrBG4g3BHEKNTtlLfHxFOUtrOec6OE+TYlUACzJLVDeyS+l9sPWpqas+2EaUFPVtGuN5id8EmtpPY5oH0lCq6OmnurTKd8ds2ERFvNIREQBERCS9REUGRZIiKTAIiIAhRbBkHhPlVfSwkXZ0gkfw0I+u4HkbBv0ljKW1NsyisvBPOb7BPI6GniItIW9LJ8cnWIPZqb9FaJnuyn9DD4ncJZrcNsbD/AFH6PFSblDi8dJTTVMnoxs0rb3HY1o5lxA71y7iVdJPLJPKbySPL3Hmdw5DYOQC5ulh3k3ZL/mXb57I7EWyqqLbs32S7auR89SQygg68rnGzXEaxHfhvPAdoXRnNQWWUoxcnhG45nsjLAYjUt3eYadw3ynt2N5XO8L6zh5zradLh7+tra+cbBxbDxPv7t3EYDOBnFdVA01HeOj9EkdV0oGq1vUj93ad9tij1VYUOcu8s9jfK1RW2B9OcSSSbkm5J1kk7SV8oiulYIiIDaslMva2hLWsf0lOP0Mhu0D3HbWd2rkpnySy/oq6zGv6Oo/VSEBx+A7H92viAublUH/lVrdLCfPRm+u+Ufoddo1oGzt8dZUAZJZ0ayltHUXqIBq6x880e68+l2O8Qplyayqo65ulTSguAu6N3VkZ8Tf7i45rm20Tr69C7C2M+h65V4UKujqafe+JwbyeBeM9zgCuWS0jUQQdhB2gjaCuu1zZnMwnybEaloFmSEVDOyXW77YeO5WdDPlxNOqjwpGrIiLpFEIiIAiIhJeoiKDIskRFJgEREAUsZhcMvJV1RHotbA083HTf9TWeKihTvm7fHQYIaqUaiJKlw3u16MYHMhrAO1VdXLFeF48FjTr58+RrefDKLTljoI3dWO0stvbcOo09jTpfSHBRWrjEK2SaWSaU3kkeZHHm431ctw5BW63VV93BRNdk98snvSQh7gHPDGes4i+iBtIG1x4AbSsrjmULpo46WFpioovQivref1kxHpPJ18BfVxOERZOKbyzFSwsIKiIsjEIiIAiIgCIiAL1p53xua+N7mPabhzXFrmniCNYXkiE5JSyTzuyx6MeIM6RmzpWACQfG3Y/tFjyK987/k9ZTUuIUsjZGMeYXlp1gSC7dMbWkOFrH21E6+mSOAcASA4AOAJAcAbgOG/WAVW+GipqceDd3zcdsj4REVk0BERAEREJL1ERQZFkiIpMAiIgDjqKlLOriHk9Jh+FMNtGGN8o5MaGxtPa4Od9EKOMJha+enY/0HTRtdfZoue0OvysSrrKjGHVlXUVJvZ7zog7mN6sY/hA77rTOG6cfTk2xliD9TFqiqqLcagiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIhJeoiKDIskRFJgEREB7UvpDsP3FeTkRQT4FERFJAREQBERAEREAREQBERAEREAREQBERAEREAREQkvURFBkf/9k="
        }
    }, {headers: {Authorization: user.token}}).then(res => {
        user.avatar = res.data.result.data.name;
        return ax.post('/api/save_profile', {params: user}, {headers: {Authorization: user.token}});
    });
}

function createRequest(ax, user) {
    return ax.post('/api/register', {params: {
            email: getEmail(),
            phone: getPhone(),
            first_name: "Test" + moment().format("DD-MM-YYYY"),
            last_name: "Test"  + moment().format("hh:mm:ss"),
            password: "123456",
        }
    })
}

let fields = [
    "token",
    "email",
    "password"
];

let fieldNames = [
    "token",
    "email",
    "password"
];

let max_count = 2000;
let data = [];

function test() {
    register(_axios).then(res => {
        console.log(res.data);
        if(res.data.status === 1){
            data.push({
                token: res.data.result.data.token,
                email: res.data.result.data.email,
                password: "123456"
            });
            uploadAvatar(_axios, res.data.result.data).then(u => {
                console.log(u.data.result);
            });
        }
        max_count--;
        if(max_count > 0){
            test();
        } else {
            let result = json2csv({ data: data, fields: fields, fieldNames: fieldNames, del: ";" });
            fs.writeFile('file.csv', result, function(err) {
                if (err) throw err;
                console.log('file saved');
            });
        }
    });
}

test();
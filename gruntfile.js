module.exports = function(grunt) {
    "use strict";

    grunt.initConfig({
        ts: {
            app: {
                // compilerOptions: {
                //     "module": "es6",
                //     "moduleResolution": "node",
                //     "target": "es6",
                //     "sourceMap": true,
                //     "noImplicitAny": true,
                //     "removeComments": false,
                //     "experimentalDecorators": true
                // },
                files: [{
                    src: ["src/\*\*/\*.ts", "!src/.baseDir.ts", "!src/_all.d.ts"],
                    dest: "dist"
                }],
                options: {
                    outDir: "dist",
                    rootDir: "src",
                    module: "commonjs",
                    noLib: true,
                    target: "es6",
                    sourceMap: false,
                    experimentalDecorators: true
                }
            }
        },
        tslint: {
            options: {
                configuration: "tslint.json"
            },
            files: {
                src: ["src/\*\*/\*.ts"]
            }
        },
        watch: {
            ts: {
                files: ["js/src/\*\*/\*.ts", "src/\*\*/\*.ts"],
                tasks: ["ts", "tslint"]
            }
        }
    });

    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-ts");
    grunt.loadNpmTasks("grunt-tslint");

    grunt.registerTask("default", [
        "ts",
        "tslint"
    ]);

};
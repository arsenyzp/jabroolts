/// <reference path="../node_modules/typescript/lib/lib.es6.d.ts" />
"use strict";

import * as bodyParser from "body-parser";
import * as cookieParser from "cookie-parser";
import * as session from "express-session";
import * as express from "express";
import * as path from "path";
import * as validator from "express-validator";
import * as cron from "cron";
import * as i18n from "i18n";

import * as indexRoute from "./routes/index";
import {ConnectionDb} from "./components/mongoose";
import {AppConfig} from "./components/config";
import {StartDelivery} from "./components/cron/StartDelivery";
import {MissedOrder} from "./components/cron/MissedOrder";
import {AuthEvents} from "./components/events/AuthEvents";
import {CancelOrderEvents} from "./components/events/CancelOrderEvents";
import GErrorReporting from "./components/GErrorReporting";
import {CalculateDebt} from "./components/cron/CalculateDebt";
import {UserActivity} from "./components/cron/UserActivity";
import {ClearSocket} from "./components/cron/ClearSocket";
import {PayOrderEvent} from "./components/events/PayOrderEvent";
import {FinishOrderEvent} from "./components/events/FinishOrderEvent";
import {CurrencyComponent} from "./components/CurrencyComponent";


const MongoStore: any = require("connect-mongo")(session);

/**
 * The server.
 *
 * @class Server
 */
class Server {

    public app: express.Application;

    /**
     * Bootstrap the application.
     *
     * @class Server
     * @method bootstrap
     * @static
     */
    public static bootstrap(): Server {
        return new Server();
    }

    /**
     * Constructor.
     *
     * @class Server
     * @constructor
     */
    constructor() {
        //create expressjs application
        this.app = express();

        //configure application
        this.config();

        //configure routes
        this.routes();

        //start cron
        this.cron();

        this.eventBusInit();
    }

    /**
     * Configure application
     *
     * @class Server
     * @method config
     * @return void
     */
    private config(): void {
        //configure jade
        this.app.set("views", path.join(__dirname, "../src/views"));
        this.app.set("view engine", "ejs");

        //mount logger
        //this.app.use(logger("dev"));

        //mount json form parser
        this.app.use(bodyParser.json({limit: "10mb"}));

        //mount query string parser
        this.app.use(bodyParser.urlencoded({ extended: true }));

        this.app.use(cookieParser());

        i18n.configure({
            locales: ["en", "ar"],
            directory: __dirname + "/../locales",
            defaultLocale: "en",
            queryParameter: "lang",
            objectNotation: true
        });
        this.app.use(i18n.init);

        //add static paths
        this.app.use(express.static(path.join(__dirname, "../public")));
        this.app.use(express.static(path.join(__dirname, "../")));
        this.app.use(express.static(path.join("/mnt/data001/public")));

     //   const customValidators = require('./components/validator');
        this.app.use(validator({
           // customValidators: customValidators
        }));

        // session start
        this.app.use(session({
            secret: AppConfig.getInstanse().get("secret"),
            resave: true,
            saveUninitialized: true,
            cookie: {secure: false},
            store: new MongoStore({mongooseConnection: ConnectionDb.getInstanse()})
        }));

        // catch 404 and forward to error handler
        this.app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
            err.status = 404;
            next(err);
        });
    }

    /**
     * Configure routes
     *
     * @class Server
     * @method routes
     * @return void
     */
    private routes(): void {
        //create routes
        let index: indexRoute.Index = new indexRoute.Index();
        //use router middleware
        this.app.use("/", index.index());
    }

    private cron(): void {
        let startDelivery: StartDelivery = new StartDelivery();
        let missedOrder: MissedOrder = new MissedOrder();
        let debt: CalculateDebt = new CalculateDebt();
        let userActivity: UserActivity = new UserActivity();
        let clearSocket: ClearSocket = new ClearSocket();

        let job1: any = new cron.CronJob("0 * * * * *", () => {
            startDelivery.run();
        }, null, true, "Europe/Moscow");

        let job2: any = new cron.CronJob("0 */5 * * * *", () => {
            missedOrder.run();
        }, null, true, "Europe/Moscow");

        let job3: any = new cron.CronJob("0 */30 * * * *", () => {
            debt.run();
        }, null, true, "Europe/Moscow");

        let job4: any = new cron.CronJob("0 0 * * * *", () => {
            userActivity.run();
        }, null, true, "Europe/Moscow");

        let job5: any = new cron.CronJob("0 0 * * * *", () => {
            clearSocket.run();
        }, null, true, "Europe/Moscow");

        let job6: any = new cron.CronJob("0 */30 * * * *", () => {
            CurrencyComponent.updateData();
        }, null, true, "Europe/Moscow");
        CurrencyComponent.updateData();
    }

    private eventBusInit(): void {
        AuthEvents.init();
        CancelOrderEvents.init();
        PayOrderEvent.init();
        FinishOrderEvent.init();
    }
}

const server: Server = Server.bootstrap();
module.exports = server.app;

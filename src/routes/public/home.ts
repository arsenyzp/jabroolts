"use strict";

import * as express from "express";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {ISubscriberModel, SubscriberModel} from "../../models/SubscriberModel";
import {TermsConfig} from "../../models/configs/TermsConfig";

module Route {

    export class Index {

        public index(req: express.Request, res: express.Response, next: express.NextFunction): void {
            res.render("public/index");
        }

        public register(req: express.Request, res: express.Response, next: express.NextFunction): void {
            res.render("public/register");
        }

        public download(req: express.Request, res: express.Response, next: express.NextFunction): void {
            res.render("public/download");
        }

        public async about(req: express.Request, res: express.Response, next: express.NextFunction): Promise<any> {
            let conf: TermsConfig = new TermsConfig();
            conf
                .getInstanse()
                .then(c => {
                    res.render("public/about", {
                        privacy: c.privacy_en,
                        terms_courier: c.terms_courier_en,
                        terms_customer: c.terms_customer_en
                    });
                });
        }

        public pricing(req: express.Request, res: express.Response, next: express.NextFunction): void {
            res.render("public/pricing");
        }

        public logout(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            req.session.user = null;
            res.redirect("/");
        }

        public tmp(req: express.Request, res: express.Response, next: express.NextFunction): void {
            // temp landing page
            res.render("public/tmp");
        }

        public subscribe(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            if (!!req.body.email) {
                let m: ISubscriberModel = new SubscriberModel();
                m.email = req.body.email;
                m.name = req.body.name;
                m.text = "";
                m.phone = "";
                m.save().then(_ => {
                    res.json({});
                })
                    .catch(err => {
                        res.json({"error": ""});
                    });
            }
            res.json({"error": ""});
        }
    }
}

export = Route;

"use strict";

import * as express from "express";
import * as home from "./home";
import * as login from "./login";
import * as confirm from "./confirm";
import * as terms from "./terms";
import * as ajax from "./ajax";

import * as account from "./account/index";
import * as orders from "./account/orders";
import * as promo from "./account/promo";
import * as settings from "./account/settings";
import * as payments from "./account/payments";

import {UserAuth} from "../../components/UserAuth";

module Route {

    export class Index {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let homeRoute: home.Index = new home.Index();
            router.get("/site", homeRoute.index.bind(homeRoute.index));
            router.get("/register", homeRoute.register.bind(homeRoute.register));
            router.get("/pricing", homeRoute.pricing.bind(homeRoute.pricing));
            router.get("/about", homeRoute.about.bind(homeRoute.about));
            router.get("/download", homeRoute.download.bind(homeRoute.download));
            router.get("/logout", homeRoute.logout.bind(homeRoute.logout));

            router.get("/", homeRoute.tmp.bind(homeRoute.tmp));
            router.post("/subscribe", homeRoute.subscribe.bind(homeRoute.subscribe));

            let loginRoute: login.Index = new login.Index();
            router.get("/login", loginRoute.index.bind(loginRoute.index));

            let confirmRoute: confirm.Index = new confirm.Index();
            router.get("/confirm/:confirm_email_code", confirmRoute.index.bind(confirmRoute.index));

            let termsRoute: terms.Index = new terms.Index();
            router.get("/privacy_policy", termsRoute.index.bind(termsRoute.index));
            router.get("/privacy_policy_ar", termsRoute.indexAr.bind(termsRoute.indexAr));

            /**
             * Site
             */
            let ajaxRoute: ajax.Ajax = new ajax.Ajax();
            router.post("/site/login", ajaxRoute.login.bind(ajaxRoute.login));
            router.post("/site/register", ajaxRoute.register.bind(ajaxRoute.register));
            router.post("/site/contact_us", ajaxRoute.contact_us.bind(ajaxRoute.contact_us));
            router.use("/ajax/uploads_img", ajaxRoute.upload());
            router.post("/ajax/calculateDistance", ajaxRoute.calculateDistance.bind(ajaxRoute.calculateDistance));
            router.post("/ajax/saveTmpOrder", ajaxRoute.saveTmpOrder.bind(ajaxRoute.saveTmpOrder));

            /**
             * Account
             */
            let accountRoute: account.Index = new account.Index();
            router.get("/jid", accountRoute.jid.bind(accountRoute.jid));
            router.get("/account", UserAuth.check, accountRoute.home.bind(accountRoute.home));
            router.get("/account/getToken", UserAuth.check, accountRoute.getToken.bind(accountRoute.getToken));
            router.get("/account/ordersHistory", UserAuth.check, accountRoute.ordersHistory.bind(accountRoute.ordersHistory));
            router.get("/account/getProfile", UserAuth.check, accountRoute.getProfile.bind(accountRoute.getProfile));
            router.post("/account/saveProfile", UserAuth.check, accountRoute.saveProfile.bind(accountRoute.saveProfile));
            router.get("/account/getPaymentToken", UserAuth.check, accountRoute.getPaymentToken.bind(accountRoute.getPaymentToken));

            let ordersRoute: orders.Orders = new orders.Orders();
            router.get("/account/orders", UserAuth.check, ordersRoute.list.bind(ordersRoute.list));
            router.get("/account/cancel_cost", UserAuth.check, ordersRoute.cancelCost.bind(ordersRoute.cancelCost));
            router.get("/account/cancel", UserAuth.check, ordersRoute.cancel.bind(ordersRoute.cancel));
            router.post("/account/saveOrder", UserAuth.check, ordersRoute.saveOrder.bind(ordersRoute.saveOrder));

            let promoRoute: promo.Promo = new promo.Promo();
            router.get("/account/day_bonus", UserAuth.check, promoRoute.dayPromo.bind(promoRoute.dayPromo));
            router.get("/account/apply_code", UserAuth.check, promoRoute.save.bind(promoRoute.save));

            let settingsRoute: settings.Settings = new settings.Settings();
            router.get("/account/favoriteLocation", UserAuth.check, settingsRoute.getFavoriteLocation.bind(settingsRoute.getFavoriteLocation));
            router.get("/account/getHistory", UserAuth.check, settingsRoute.getHistory.bind(settingsRoute.getHistory));

            let paymentsRoute: payments.Payments = new payments.Payments();
            router.get("/account/getProfileCards", UserAuth.check, paymentsRoute.index.bind(paymentsRoute.index));
            router.post("/account/saveDefaultPayment", UserAuth.check, paymentsRoute.saveDefaultPayment.bind(paymentsRoute.saveDefaultPayment));
            router.post("/account/saveCard", UserAuth.check, paymentsRoute.saveCard.bind(paymentsRoute.saveCard));

            return router;
        }
    }
}

export = Route;

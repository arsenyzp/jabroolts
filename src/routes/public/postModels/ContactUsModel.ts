import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class ContactUsModel {

    @Length(2, 200, {
        message: "site.validation.InvalidContactUsName"
    })
    name: string;

    @Length(6, 16, {
        message: "site.validation.InvalidContactUsNumber"
    })
    number: string;

}

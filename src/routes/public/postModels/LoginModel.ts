import {validate, Contains, IsInt, Length, IsEmail, ValidateIf, IsDate, Min, Max} from "class-validator";

export class LoginPost {

    @Length(3, 70, {
        message: "site.validation.InvalidEmail"
    })
    email: string;

    @Length(6, 20, {
        message: "site.validation.InvalidPassword"
    })
    password: string;

    @ValidateIf(o => { return !!o.type; })
    @Length(4, 10, {
        message: "site.validation.InvalidType"
    })
    type: string;

}

import {validate, Contains, ValidateIf, Length, IsNumber, IsArray, IsDate, Min, Max, IsByteLength, IsBoolean} from "class-validator";

export class CreateOrderWebPost {

    @ValidateIf(o => { return !!o.jid; })
    @Length(4, 20, {
        message: "api.validation.InvalidJID"
    })
    jid: string = "";

    @ValidateIf(o => { return !!o.recipient_name; })
    @Length(2, 1000, {
        message: "api.validation.InvalidRecipientName"
    })
    recipient_name: string = "";

    @ValidateIf(o => { return !!o.recipient_contact; })
    @Length(4, 1000, {
        message: "api.validation.InvalidRecipientContact"
    })
    recipient_contact: string = "";

    @ValidateIf(o => { return !!o.owner_address; })
    @Length(4, 1000, {
        message: "api.validation.InvalidOwnerAddress"
    })
    owner_address: string = "";

    @ValidateIf(o => { return !!o.recipient_address; })
    @Length(4, 1000, {
        message: "api.validation.InvalidRecipientAddress"
    })
    recipient_address: string = "";

    @Min(0.1, {
        message: "api.validation.RouteMinError"
    })
    @IsNumber({
        message: "api.validation.RouteInvalidValue"
    })
    route: number;

    @Length(4, 20, {
        message: "api.validation.InvalidType"
    })
    type: string;

    // @Min(0.1, {
    //     message: "Weight - minimum 0.1"
    // })
    // @IsNumber({
    //     message: "Weight - invalid value"
    // })
    weight: number;

    @IsArray({
        message: "api.validation.InvalidArrayPhotos"
    })
    @Length(4, 40, {
        message: "api.validation.InvalidNamePhotos",
        each: true
    })
    photos: string;

    @IsNumber({
        message: "api.validation.InvalidRecipientLon lon"
    })
    owner_lon: number;

    @IsNumber({
        message: "api.validation.InvalidOwnerLat lat"
    })
    owner_lat: number;

    @IsNumber({
        message: "api.validation.InvalidRecipientLon"
    })
    recipient_lon: number;

    @IsNumber({
        message: "api.validation.InvalidRecipientLat"
    })
    recipient_lat: number;

    @ValidateIf(o => { return !!o.small_package_count; })
    @IsNumber({
        message: "api.validation.SmallPackageErrorValue"
    })
    small_package_count: number;

    @ValidateIf(o => { return !!o.medium_package_count; })
    @IsNumber({
        message: "api.validation.MediumPackageErrorValue"
    })
    medium_package_count: number;

    @ValidateIf(o => { return !!o.large_package_count; })
    @IsNumber({
        message: "api.validation.LargePackageErrorValue"
    })
    large_package_count: number;

    @ValidateIf(o => { return !!o.code; })
    @Length(4, 20, {
        message: "api.validation.InvalidPromoCode"
    })
    code: string;
}

"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {IUserModel, UserModel} from "../../../models/UserModel";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {ApiHelper} from "../../../api/ApiHelper";
import {FavoriteLocationModel, IFavoriteLocationModel} from "../../../models/FavoriteLocationModel";
import {HistoryLocationModel} from "../../../models/HistoryLocationModel";

module Route {

    export class Settings {

        public getSettings(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            UserModel
                .findOne({_id: req.session.user._id})
                .then(u => {
                    if (u == null) {
                        ajaxResponse.addErrorMessage("Item not found");
                        res.status(500);
                        return res.json(ajaxResponse.get());
                    }
                    ajaxResponse.setDate(u);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public async getHistory(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            HistoryLocationModel
                .find({user: req.session.user._id})
                .sort({created_at: -1})
                .then( (its) => {
                    let items: Array<any> = [];
                    for (let i: number = 0; i < its.length; i++) {
                        items.push(its[i].getPublicFields());
                    }
                    ajaxResponse.setDate(items);
                    res.json(ajaxResponse.get());
                })
                .catch( (err) => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public getFavoriteLocation(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            FavoriteLocationModel
                .find({user: req.session.user._id})
                .sort({created_at: -1})
                .then( (its) => {
                    let items: Array<IFavoriteLocationModel> = [];
                    for (let i: number = 0; i < its.length; i++) {
                        items.push(its[i].getPublicFields());
                    }
                    ajaxResponse.setDate(items);
                    res.json(ajaxResponse.get());
                })
                .catch( (err) => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

    }
}

export = Route;

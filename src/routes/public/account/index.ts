"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {IUserModel, UserModel} from "../../../models/UserModel";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {NotificationModule, NotificationTypes} from "../../../components/NotificationModule";
import {ProfilePost} from "../../../api/postModels/ProfilePost";
import {ApiHelper} from "../../../api/ApiHelper";
import {SmsSender} from "../../../components/jabrool/SmsSender";
import {FileHelper} from "../../../components/FileHelper";
import {ResponseInterface} from "../../../interfaces/ResponseInterface";
import {AppConfig} from "../../../components/config";
import {validate} from "class-validator";
import {EmailSender} from "../../../components/jabrool/EmailSender";
import {ApiResponse} from "../../../components/ApiResponse";
import * as async from "async";
import {OrderModel, OrderStatuses} from "../../../models/OrderModel";
import {PaymentModule} from "../../../components/jabrool/PaymentModule";

module Route {

    export class Index {

        public async home(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            //render page
            let user: IUserModel = await UserModel.findOne({_id: req.session.user._id});
            res.render("public/account/home", {
                user: user.getApiPublicFields()
            });
        }

        public getToken(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            UserModel
                .findOne({_id: req.session.user._id})
                .then(u => {
                    if (u == null) {
                        ajaxResponse.addErrorMessage("Item not found");
                        res.status(500);
                        return res.json(ajaxResponse.get());
                    }
                    ajaxResponse.setDate(u);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public getProfile(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            UserModel
                .findOne({_id: req.session.user._id})
                .then(u => {
                    if (u == null) {
                        ajaxResponse.addErrorMessage("Item not found");
                        res.status(500);
                        return res.json(ajaxResponse.get());
                    }
                    ajaxResponse.setDate(u);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public async jid(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            //render page
            req.session.jid = req.query.jid;
            res.redirect("/");
        }

        public async saveProfile(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let post: ProfilePost = Object.create(ProfilePost.prototype);
            Object.assign(post, req.body, {});
            try {
                let errors: any = await validate(post);
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.json(ajaxResponse.get());
                } else {

                    post.phone = post.phone.replace(/\D/g, "");

                    let user: IUserModel = await UserModel
                        .findOne({$or: [
                                {email: post.email, _id: {$ne: req.session.user._id}},
                                {phone: post.phone, _id: {$ne: req.session.user._id}}
                            ]});
                    if (user !== null) {
                        // User already exists
                        if (user.email === post.email) {
                            ajaxResponse.addErrorMessage(req.__("api.errors.EmailAlreadyUsedInService"));
                            res.status(500);
                            return res.json(ajaxResponse.get());
                        } else if (user.phone === post.phone) {
                            ajaxResponse.addErrorMessage(req.__("api.errors.PhoneAlreadyUsedInService"));
                            res.status(500);
                            return res.json(ajaxResponse.get());
                        }
                    } else {

                        user = await UserModel.findOne({_id: req.session.user._id});

                        let new_data: any = {};

                        new_data.first_name = post.first_name;
                        new_data.last_name = post.last_name;

                        if (req.session.user.email !== post.email) {
                            new_data.email = post.email;
                            req.session.user.email = new_data.email;
                            // send email
                            // generate confirm code
                            new_data.confirm_email_code = user.generateConfirmEmail();
                            new_data.confirm_email = false;
                            req.session.user.confirm_email_code = new_data.confirm_email_code;
                            // send email
                            EmailSender.sendTemplateToUser(req.session.user, req.__("api.email.SubjectConfirmEmail"), {}, "confirm_email");
                        }

                        if (req.session.user.phone !== post.phone) {
                            new_data.tmp_phone = post.phone;
                            req.session.user.phone = post.phone;
                            // send sms
                            // generate confirm code
                            new_data.confirm_sms_code = user.generateConfirmSmsCode();
                            req.session.user.confirm_sms_code = new_data.confirm_sms_code;
                            // send confirm sms
                            SmsSender.send(req.session.user.phone, req.__("api.sms.ConfirmCode", req.session.user.confirm_sms_code));
                            NotificationModule.getInstance().send(
                                [req.session.user._id],
                                NotificationTypes.PhoneOtpSent,
                                {id: req.session.user._id.toString()},
                                req.session.user._id.toString()
                            );
                        }

                        if (post.password) {
                            // if (!post.code || req.session.user.tmp_code !== post.code) {
                            //     ajaxResponse.addErrorMessage(req.__("api.errors.PhoneAlreadyUsedInService"));
                            //     res.status(500);
                            //     return res.json(ajaxResponse.get());
                            // } else {
                            //     req.session.user.setPassword(post.password);
                            //     new_data.password = req.session.user.password;
                            // }
                            user.setPassword(post.password);
                            new_data.password = user.password;
                        }

                        async.waterfall([
                            cb => {
                                if (post.avatar && post.avatar !== req.session.user.avatar && post.avatar.length > 5) {
                                    FileHelper.move(
                                        AppConfig.getInstanse().get("files:user_uploads")  + "/" + post.avatar,
                                        AppConfig.getInstanse().get("files:user_avatars")  + "/" + post.avatar,
                                        err => {
                                            if (err) {
                                                cb(err);
                                            } else {
                                                new_data.avatar = post.avatar;
                                                cb();
                                            }
                                        });
                                } else {
                                    cb();
                                }
                            },
                            cb => {
                                UserModel.findOneAndUpdate(
                                    {_id: req.session.user._id},
                                    new_data,
                                    {new: true})
                                    .then(user => {
                                        ajaxResponse.setDate(user);
                                        cb();
                                    })
                                    .catch(err => {
                                        cb(err);
                                    });
                            }
                        ], err => {
                            if (err) {
                                ajaxResponse.addError(err);
                                res.status(500);
                                res.json(ajaxResponse.get());
                            } else {
                                res.json(ajaxResponse.get());
                            }
                        });
                    }
                }
            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }
        }

        public async ordersHistory(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let filter: any = {
                $or: [
                    {owner: req.session.user._id}
                ],
                status: {$in: [
                        OrderStatuses.Finished
                    ]}
            };

            OrderModel
                .find(filter)
                .populate({path: "courier", model: UserModel})
                .populate({path: "recipient", model: UserModel})
                .populate({path: "owner", model: UserModel})
                .sort({created_at: -1})
                .then( (orders) => {
                    let items: Array<any> = [];
                    for (let i: number = 0; i < orders.length; i++) {
                        // let order: any = orders[i].getPublicFields();
                        // items.push({
                        //     courier: order.courier,
                        //     owner: order.owner,
                        //     recipient: order.recipient,
                        //     order: orders[i].getApiFieldsMin()
                        // });
                        let order: any = orders[i].getApiFields();
                        items.push(order);
                    }
                    ajaxResponse.setDate(items);
                    res.json(ajaxResponse.get());
                })
                .catch( (err) => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public async getPaymentToken(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            PaymentModule
                .getToken()
                .then(token => {
                    ajaxResponse.setDate({token: token});
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });

        }

    }
}

export = Route;

"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {IUserModel, UserModel} from "../../../models/UserModel";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {NotificationModule, NotificationTypes} from "../../../components/NotificationModule";
import {PromoCodeStatus, PromoCodHelper} from "../../../components/jabrool/PromoCodHelper";
import {PaymentLogModel, PaymentStatuses, PaymentTypes} from "../../../models/PaymentLog";
import {NotificationModel} from "../../../models/NotificationModel";
import {ResponseInterface} from "../../../interfaces/ResponseInterface";

module Route {

    export class Promo {

        public dayPromo(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let date: Date = new Date();
            date.setUTCHours(0);
            date.setUTCMinutes(0);
            date.setUTCSeconds(0);
            date.setUTCMilliseconds(0);

            let time: number = date.getTime();
            PaymentLogModel
                .find({
                    user: req.session.user._id,
                   // status: PaymentStatuses.Success,
                    type: {$in: [PaymentTypes.Promo, PaymentTypes.Invite, PaymentTypes.Admin]},
                    created_at: {$gte: time}
                })
                .then( (logs) => {
                    let amount: number = 0;
                    for (let i: number = 0; i < logs.length; i++) {
                        amount += logs[i].amount;
                    }
                    ajaxResponse.setDate({
                        bonus: amount.toFixed(2),
                        jabroolFee: req.session.user.jabroolFee,
                        courierLimit: req.session.user.courierLimit
                    });
                    res.json(ajaxResponse.get());
                })
                .catch( (err) => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public async save(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            try {
                let result: any = await PromoCodHelper.use(req.session.user, req.query.code, "client");
                switch (result.status) {
                    case PromoCodeStatus.NotFound:
                        res.status(500);
                        ajaxResponse.addErrorMessage(req.__("site.errors.NotFoundCode"));
                        res.json(ajaxResponse.get());
                        break;
                    case PromoCodeStatus.Used:
                        res.status(500);
                        ajaxResponse.addErrorMessage(req.__("site.errors.UsedCode"));
                        res.json(ajaxResponse.get());
                        break;
                    case PromoCodeStatus.Ready:
                        if (req.session.user.balance < 0) {
                            let user: IUserModel = await UserModel.findOne({_id: req.session.user._id});
                            if (user !== null && user.balance >= 0) {
                                await NotificationModel.update(
                                    {
                                        user: req.user._id, type: {
                                            $in: [
                                                NotificationTypes.JabroolDebt
                                            ]
                                        }
                                    }, {is_view: true},
                                    {multi: true});
                            }
                        }
                        ajaxResponse.setDate({discount: result.promo.amount});
                        res.json(ajaxResponse.get());
                        break;
                }
            } catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
            }
        }

    }
}

export = Route;

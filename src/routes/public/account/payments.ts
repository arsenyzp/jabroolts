"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {IUserModel, UserModel} from "../../../models/UserModel";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {NotificationModule, NotificationTypes} from "../../../components/NotificationModule";
import {PromoCodeStatus, PromoCodHelper} from "../../../components/jabrool/PromoCodHelper";
import {PaymentLogModel, PaymentStatuses, PaymentTypes} from "../../../models/PaymentLog";
import {NotificationModel} from "../../../models/NotificationModel";
import {ResponseInterface} from "../../../interfaces/ResponseInterface";
import {PaymentModule} from "../../../components/jabrool/PaymentModule";
import {ApiHelper} from "../../../api/ApiHelper";
import {CardLabelModel, ICardLabelModel} from "../../../models/CardLabelModel";

module Route {

    export class Payments {

        public index(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            if (!req.session.user.bt_customer_id || req.session.user.bt_customer_id === "") {
                ajaxResponse.setDate([]);
                res.json(ajaxResponse.get());
                return;
            }

            PaymentModule
                .listCards(req.session.user.bt_customer_id)
                .then(result => {
                    let cards: Array<any> = [];
                    if (result.creditCards.length > 0) {
                        // find labels
                        let uids: Array<string> = [];
                        for (let i: number = 0; i < result.creditCards.length; i++) {
                            uids.push(result.creditCards[i].uniqueNumberIdentifier);
                        }

                        CardLabelModel
                            .find({uniqueNumberIdentifier: {$in: uids}})
                            .then( (labels) => {
                                let labels_arr: any = {};
                                for (let i: number = 0; i < labels.length; i++) {
                                    labels_arr[labels[i].uniqueNumberIdentifier] = labels[i].label;
                                }

                                for (let i: number = 0; i < result.creditCards.length; i++) {
                                    let label: string = "";
                                    if (labels_arr[result.creditCards[i].uniqueNumberIdentifier]) {
                                        label = labels_arr[result.creditCards[i].uniqueNumberIdentifier];
                                    }
                                    cards.push({
                                        cardType: result.creditCards[i].cardType,
                                        uniqueNumberIdentifier: result.creditCards[i].uniqueNumberIdentifier,
                                        imageUrl: result.creditCards[i].imageUrl,
                                        maskedNumber: result.creditCards[i].maskedNumber,
                                        default: (req.session.user.pay_type === "card") ? result.creditCards[i].default : false,
                                        label: label
                                    });
                                }

                                ajaxResponse.setDate(cards);
                                res.json(ajaxResponse.get());
                            })
                            .catch(err => {
                                ajaxResponse.addError(err);
                                res.status(500);
                                res.json(ajaxResponse.get());
                            });
                    } else {
                        ajaxResponse.setDate([]);
                        res.json(ajaxResponse.get());
                        return;
                    }
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });

        }

        public async saveDefaultPayment(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            try {
                UserModel.findOneAndUpdate(
                    {_id: req.session.user._id},
                    {
                        pay_type: req.body.type,
                        defaultUniqueNumberIdentifier: req.body.uniqueNumberIdentifier
                    },
                    {new: true})
                    .then(user => {
                        if (
                            user.pay_type === "card" &&
                            req.body.uniqueNumberIdentifier &&
                            req.body.uniqueNumberIdentifier !== "" &&
                            user.bt_customer_id &&
                            user.bt_customer_id !== ""
                        ) {
                            PaymentModule.setDefault(req.session.user.bt_customer_id, req.body.uniqueNumberIdentifier).then(
                                r => {
                                    ajaxResponse.setDate(user.getApiFields());
                                    res.json(ajaxResponse.get());
                                    return;
                                }
                            ).catch(err => {
                                ajaxResponse.addErrorMessage(err.message);
                                res.status(500);
                                res.json(ajaxResponse.get());
                            });
                        } else {
                            ajaxResponse.setDate(user.getApiFields());
                            res.json(ajaxResponse.get());
                            return;
                        }
                    })
                    .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }
        }

        public async saveCard(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let promise : Promise<any>;
            if (req.session.user.bt_customer_id) {
                promise = PaymentModule.createPayment(
                    req.session.user.bt_customer_id,
                    req.body.payment_method_nonce,
                );
            } else {
                promise = PaymentModule.createCustomerAndPayment(
                    req.session.user.first_name,
                    req.session.user.last_name,
                    req.session.user.email,
                    req.session.user.phone,
                    req.body.payment_method_nonce,
                );
            }

            promise.then(result => {
                console.log(result);
                if (!result.success) {
                    ajaxResponse.addErrorMessage(result.message);
                    res.status(500);
                    res.json(ajaxResponse.get());
                } else {
                    let customerId: string = result.customer.id;
                    UserModel.findOneAndUpdate(
                        {_id: req.session.user._id},
                        {bt_customer_id: customerId}
                    )
                        .then(doc => {

                            let label: ICardLabelModel = new CardLabelModel({
                                user: req.session.user._id,
                                uniqueNumberIdentifier: result.customer.creditCards[0].uniqueNumberIdentifier,
                                label: result.customer.creditCards[0].maskedNumber
                            });
                            label
                                .save()
                                .then(r => {
                                    ajaxResponse.setDate({
                                        cardType:  result.customer.creditCards[0].cardType,
                                        uniqueNumberIdentifier:  result.customer.creditCards[0].uniqueNumberIdentifier,
                                        imageUrl:  result.customer.creditCards[0].imageUrl,
                                        maskedNumber:  result.customer.creditCards[0].maskedNumber,
                                        label: result.customer.creditCards[0].maskedNumber
                                    });
                                    res.json(ajaxResponse.get());
                                    return;
                                })
                                .catch(err => {
                                    ajaxResponse.addError(err);
                                    res.status(500);
                                    res.json(ajaxResponse.get());
                                });

                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });

                }
            })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

    }
}

export = Route;

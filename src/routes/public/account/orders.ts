"use strict";

import * as express from "express";
import * as EventBus from "eventbusjs";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {IUserModel, UserModel, UserRoles, UserStatus} from "../../../models/UserModel";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {IManufactureModel, ManufactureModel} from "../../../models/ManufactureModel";
import {IOrderModel, OrderModel, OrderStatuses} from "../../../models/OrderModel";
import {CancelParamsConfig} from "../../../models/configs/CancelParamsConfig";
import {PaymentModule} from "../../../components/jabrool/PaymentModule";
import {NotificationModule, NotificationTypes} from "../../../components/NotificationModule";
import {OrderHelper} from "../../../components/jabrool/OrderHelper";
import {EVENT_ORDER_REQUEST_CANCELED, EVENT_WEB_USER_REGISTER} from "../../../components/events/Events";
import {PromoCodHelper} from "../../../components/jabrool/PromoCodHelper";
import {SocketServer} from "../../../components/SocketServer";
import {CreateOrderWebPost} from "../postModels/CreateOrderWebPost";
import {ResponseInterface} from "../../../interfaces/ResponseInterface";
import {validate} from "class-validator";
import {CreateOrderHelper} from "../../../components/jabrool/CreateOrderHelper";
import {AppConfig} from "../../../components/config";

module Route {

    export class Orders {

        public async list(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            try {

                let carModels: Array<IManufactureModel> = await ManufactureModel.find({});
                let cars: any = [];
                carModels.map((v, k, a) => {
                    cars[v._id.toString()] = v.name;
                });

                let itemsActive: Array<any> = await  OrderModel
                    .find({
                        status: {$nin: [OrderStatuses.Missed, OrderStatuses.Finished, OrderStatuses.Canceled]},
                        owner: req.session.user._id
                    })
                    .populate({path: "courier", model: UserModel})
                    .populate({path: "owner", model: UserModel})
                    .sort({created_at: -1});

                let ordersActive: Array<any> = [];
                itemsActive.map((v: IOrderModel, k, a) => {
                    let m: any = v.getApiFields();
                    m.vehicle = v.courier ? v.courier.getVehicle() : null;
                    if (m.vehicle !== null) {
                        m.vehicle.model = cars[m.vehicle.model];
                    }
                    m.courier = v.courier ? v.courier.getSiteFields() : null;
                    let lat_center: number = (v.owner_location.coordinates[0] + v.recipient_location.coordinates[0]) / 2;
                    let lon_center: number = (v.owner_location.coordinates[1] + v.recipient_location.coordinates[1]) / 2;
                    m.map = "https://maps.googleapis.com/maps/api/staticmap?center=" +
                        lon_center + "," + lat_center + "&zoom=13&size=320x150&maptype=roadmap" +
                        "&markers=color:blue%7Clabel:S%7C" +
                        v.owner_location.coordinates[1] + "," + v.owner_location.coordinates[0] +
                        "&markers=color:green%7Clabel:G%7C40.711614,-74.012318" +
                        "&markers=color:red%7Clabel:C%7C" +
                        v.recipient_location.coordinates[1] + "," + v.recipient_location.coordinates[0] +
                        "&key=" + AppConfig.getInstanse().get("gmap_key");
                    ordersActive.push(m);
                });

                let itemsCompleted: Array<any> = await  OrderModel
                    .find({
                        status: {$in: [OrderStatuses.Finished]},
                        owner: req.session.user._id
                    })
                    .populate({path: "courier", model: UserModel})
                    .populate({path: "owner", model: UserModel})
                    .sort({created_at: -1});

                let ordersCompleted: Array<any> = [];
                itemsCompleted.map((v: IOrderModel, k, a) => {
                    let m: any = v.getApiFields();
                    m.vehicle = v.courier ? v.courier.getVehicle() : null;
                    m.courier = v.courier ? v.courier.getSiteFields() : null;
                    if (m.vehicle !== null) {
                        m.vehicle.model = cars[m.vehicle.model];
                    }
                    let lat_center: number = (v.owner_location.coordinates[0] + v.recipient_location.coordinates[0]) / 2;
                    let lon_center: number = (v.owner_location.coordinates[1] + v.recipient_location.coordinates[1]) / 2;
                    m.map = "https://maps.googleapis.com/maps/api/staticmap?center=" +
                        lon_center + "," + lat_center + "&zoom=13&size=320x150&maptype=roadmap" +
                        "&markers=color:blue%7Clabel:S%7C" +
                        v.owner_location.coordinates[1] + "," + v.owner_location.coordinates[0] +
                        "&markers=color:green%7Clabel:G%7C40.711614,-74.012318" +
                        "&markers=color:red%7Clabel:C%7C" +
                        v.recipient_location.coordinates[1] + "," + v.recipient_location.coordinates[0] +
                        "&key=" + AppConfig.getInstanse().get("gmap_key");
                    ordersCompleted.push(m);
                });

                let itemsCanceled: Array<any> = await  OrderModel
                    .find({
                        status: {$in: [OrderStatuses.Canceled, OrderStatuses.Missed]},
                        owner: req.session.user._id
                    })
                    .populate({path: "courier", model: UserModel})
                    .populate({path: "owner", model: UserModel})
                    .sort({created_at: -1});

                let ordersCanceled: Array<any> = [];
                itemsCanceled.map((v: IOrderModel, k, a) => {
                    let m: any = v.getApiFields();
                    m.vehicle = v.courier ? v.courier.getVehicle() : null;
                    m.courier = v.courier ? v.courier.getSiteFields() : null;
                    if (m.vehicle !== null) {
                        m.vehicle.model = cars[m.vehicle.model];
                    }
                    let lat_center: number = (v.owner_location.coordinates[0] + v.recipient_location.coordinates[0]) / 2;
                    let lon_center: number = (v.owner_location.coordinates[1] + v.recipient_location.coordinates[1]) / 2;
                    m.map = "https://maps.googleapis.com/maps/api/staticmap?center=" +
                        lon_center + "," + lat_center + "&zoom=13&size=320x150&maptype=roadmap" +
                        "&markers=color:blue%7Clabel:S%7C" +
                        v.owner_location.coordinates[1] + "," + v.owner_location.coordinates[0] +
                        "&markers=color:green%7Clabel:G%7C40.711614,-74.012318" +
                        "&markers=color:red%7Clabel:C%7C" +
                        v.recipient_location.coordinates[1] + "," + v.recipient_location.coordinates[0] +
                        "&key=" + AppConfig.getInstanse().get("gmap_key");
                    ordersCanceled.push(m);
                });

                ajaxResponse.setDate({
                    ordersActive: ordersActive,
                    ordersCompleted: ordersCompleted,
                    ordersCanceled: ordersCanceled
                });
                res.json(ajaxResponse.get());

            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }
        }

        public async cancelCost(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            try {
                let params: CancelParamsConfig = new CancelParamsConfig();
                let conf: CancelParamsConfig = await params.getInstanse();
                let order: IOrderModel = await OrderModel
                    .findOne({
                        _id: req.query.id,
                        owner: req.session.user._id
                    });
                if (order === null) {
                    res.status(500);
                    ajaxResponse.addErrorMessage(req.__("site.errors.OrderNotFound"));
                    res.json(ajaxResponse.get());
                } else {
                    switch (order.status) {
                        case OrderStatuses.New:
                            ajaxResponse.setDate({cost: 0});
                            break;
                        case OrderStatuses.WaitCourierPickUpConfirmCode:
                        case OrderStatuses.WaitCustomerAccept:
                        case OrderStatuses.WaitCustomerPayment:
                        case OrderStatuses.WaitPickUp:
                            if (new Date().getTime() - order.accept_at <= 60 * 3 * 1000) {
                                ajaxResponse.setDate({cost: 0});
                            } else {
                                // payment from balance - fix cost
                                ajaxResponse.setDate({cost: conf.cost});
                            }
                            break;
                        case OrderStatuses.InProgress:
                        case OrderStatuses.WaitAcceptDelivery:
                            ajaxResponse.setDate({cost: (order.cost / 2).toFixed(2)});
                            break;
                        default:
                            ajaxResponse.setDate({cost: 0});
                    }
                    res.json(ajaxResponse.get());
                }
            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }
        }

        public async cancel(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            try {
                    let params: CancelParamsConfig = new CancelParamsConfig();
                    let conf: CancelParamsConfig = await params
                        .getInstanse();
                    let order: IOrderModel = await OrderModel
                        .findOne({
                            _id: req.query.id,
                            owner: req.session.user._id,
                            status: {
                                $nin: [
                                    OrderStatuses.Canceled,
                                    OrderStatuses.Finished,
                                    OrderStatuses.WaitCourierReturnConfirmCode,
                                    OrderStatuses.ReturnInProgress,
                                    OrderStatuses.WaitAcceptReturn
                                ]
                            }
                        })
                        .populate({path: "owner", model: UserModel})
                        .populate({path: "courier", model: UserModel});
                    if (order === null) {
                        res.status(500);
                        ajaxResponse.addErrorMessage(req.__("site.errors.OrderNotFound"));
                        res.json(ajaxResponse.get());
                    } else {
                        switch (order.status) {
                            case OrderStatuses.New: {
                                let orderCanceled: IOrderModel = await OrderModel.findOneAndUpdate(
                                    {_id: order._id},
                                    {
                                        status: OrderStatuses.Canceled,
                                        owner_canceled: true
                                    },
                                    {new: true});

                                EventBus.dispatch(EVENT_ORDER_REQUEST_CANCELED, this, {
                                    order: orderCanceled
                                });

                                if (order.code) {
                                    await PromoCodHelper
                                        .unblock(req.user, order.code);
                                }

                                order.status = OrderStatuses.Canceled;
                            }
                                break;
                            case OrderStatuses.WaitCourierPickUpConfirmCode:
                            case OrderStatuses.WaitCustomerAccept:
                            case OrderStatuses.WaitCustomerPayment:
                            case OrderStatuses.WaitPickUp:
                                if (new Date().getTime() - order.accept_at <= 60 * 3 * 1000) {
                                    let orderCanceled: IOrderModel = await OrderModel.findOneAndUpdate(
                                        {_id: order._id},
                                        {
                                            status: OrderStatuses.Canceled,
                                            owner_canceled: true
                                        },
                                        {new: true});
                                    //send notification to couriers
                                    EventBus.dispatch(EVENT_ORDER_REQUEST_CANCELED, this, {
                                        order: orderCanceled
                                    });

                                    /**
                                     * Remove from courier
                                     */
                                    await OrderHelper
                                        .removeOrderFromCourier(orderCanceled);

                                    if (order.code) {
                                        await PromoCodHelper
                                            .unblock(req.user, order.code);
                                    }

                                    if (order.paid) {
                                        if (order.pay_type === "card") {
                                            await PaymentModule
                                                .moneyBack(order.owner._id, order.cost);
                                        } else if (order.pay_type === "cash") {
                                            if (order.bonus > 0) {
                                                await PaymentModule
                                                    .promoBonusAdd(order.owner._id, order.bonus);
                                            }
                                        }
                                        await OrderModel
                                            .findOneAndUpdate({_id: order._id}, {paid: false});
                                    }

                                    if (order.paid && order.userCredit !== 0) {
                                        await UserModel.findOneAndUpdate(
                                            {_id: order.owner._id},
                                            {$inc: {balance: order.userCredit}}
                                        );
                                        await PaymentModule
                                            .courierTakeUserDebt(order.courier._id, order.userCredit);
                                    }

                                } else {
                                    // payment from balance - fix cost
                                    await PaymentModule
                                        .paymentFromBalance(conf.cost, req.user);

                                    let orderCanceled: IOrderModel = await OrderModel.findOneAndUpdate(
                                        {_id: order._id},
                                        {
                                            status: OrderStatuses.Canceled,
                                            owner_canceled: true
                                        },
                                        {new: true});

                                    //send notification to couriers
                                    EventBus.dispatch(EVENT_ORDER_REQUEST_CANCELED, this, {
                                        order: orderCanceled
                                    });

                                    /**
                                     * Remove from courier
                                     */
                                    await OrderHelper
                                        .removeOrderFromCourier(orderCanceled);

                                    if (order.paid && order.userCredit !== 0) {
                                        await UserModel.findOneAndUpdate(
                                            {_id: order.owner._id},
                                            {$inc: {balance: order.userCredit}}
                                        );
                                        await PaymentModule
                                            .courierTakeUserDebt(order.courier._id, order.userCredit);
                                    }
                                }
                                break;
                            case OrderStatuses.InProgress:
                            case OrderStatuses.WaitAcceptDelivery: {
                                let orderCanceled: IOrderModel = await OrderModel.findOneAndUpdate(
                                    {_id: order._id},
                                    {
                                        status: OrderStatuses.ReturnInProgress,
                                        owner_canceled: true
                                    },
                                    {new: true});
                                //send notification to couriers
                                if (orderCanceled.courier) {
                                    SocketServer.emit([orderCanceled.courier.toString()], "request_canceled", {
                                        order: orderCanceled.getPublicFields()
                                    });
                                }

                                order.status = OrderStatuses.ReturnInProgress;
                            }

                                break;
                        }

                        let orderUpdated: IOrderModel = await OrderModel
                            .findOne({
                                _id: order._id
                            })
                            .populate({path: "owner", model: UserModel})
                            .populate({path: "courier", model: UserModel});
                        if (orderUpdated.courier) {
                            if (orderUpdated.status === OrderStatuses.Canceled) {
                                NotificationModule.getInstance().send(
                                    [orderUpdated.courier._id],
                                    NotificationTypes.RequestCancelled,
                                    {},
                                    orderUpdated._id.toString()
                                );
                            }
                            if (orderUpdated.status === OrderStatuses.ReturnInProgress) {
                                NotificationModule.getInstance().send(
                                    [orderUpdated.owner._id],
                                    NotificationTypes.RequestCancelledNeedReturn,
                                    {returnCost: orderUpdated.returnCost.toFixed(2)},
                                    orderUpdated._id.toString()
                                );
                            }
                        }
                        ajaxResponse.setDate(orderUpdated.getApiFields());
                        res.json(ajaxResponse.get());
                }
            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }
        }

        public async saveOrder(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let post: CreateOrderWebPost = Object.create(CreateOrderWebPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.json(ajaxResponse.get());
                } else {
                    /**
                     * Check promo
                     */
                    CreateOrderHelper.findRecipientWeb(req, res, post, ajaxResponse);
                }
            });
        }

    }
}

export = Route;

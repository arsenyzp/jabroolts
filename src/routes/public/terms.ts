"use strict";

import * as express from "express";
import {TermsConfig} from "../../models/configs/TermsConfig";

module Route {

    export class Index {

        public async index(req: express.Request, res: express.Response, next: express.NextFunction): Promise<any> {
            let conf: TermsConfig = new TermsConfig();
            conf
                .getInstanse()
                .then(c => {
                    res.render("public/terms", {privacy: c.privacy_en});
                });
        }

        public async indexAr(req: express.Request, res: express.Response, next: express.NextFunction): Promise<any> {
            let conf: TermsConfig = new TermsConfig();
            conf
                .getInstanse()
                .then(c => {
                    res.render("public/terms_ar", {privacy: c.privacy_ar});
                });
        }
    }
}

export = Route;

"use strict";

import * as express from "express";
import {IUserModel, UserModel} from "../../models/UserModel";

module Route {

    export class Index {

        public async index(req: express.Request, res: express.Response, next: express.NextFunction): Promise<any> {
            try {
                let user: IUserModel = await UserModel.findOneAndUpdate(
                    {confirm_email_code: req.params.confirm_email_code},
                    {confirm_email: true}
                );
                if (user === null) {
                    res.render("public/confirm", {error: true});
                } else {
                    res.render("public/confirm", {error: false});
                }
            } catch (err) {
                res.render("public/confirm", {error: true});
            }
        }
    }
}

export = Route;

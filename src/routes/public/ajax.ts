"use strict";

import * as express from "express";
import * as EventBus from "eventbusjs";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {EVENT_WEB_USER_LOGIN, EVENT_WEB_USER_REGISTER} from "../../components/events/Events";
import {IUserModel, UserModel, UserRoles, UserStatus} from "../../models/UserModel";
import {validate} from "class-validator";
import {AjaxResponse} from "../../components/AjaxResponse";
import {RegisterPost} from "./postModels/RegisterModel";
import {ContactUsModel as ContactUsModelPost} from "./postModels/ContactUsModel";
import {LoginPost} from "./postModels/LoginModel";
import {IContactUsModel, ContactUsModel} from "../../models/ContactUsModel";
import {AppConfig} from "../../components/config";

import * as multer from "multer";
import * as mime from "mime-types";
import {CreateOrderWebPost} from "./postModels/CreateOrderWebPost";
import {CreateOrderHelper} from "../../components/jabrool/CreateOrderHelper";
import {ResponseInterface} from "../../interfaces/ResponseInterface";
import axios from "axios";

const storage: any  = multer.diskStorage({
    destination: function (req: RequestInterface, file: any, cb: any): void {
        cb(null, AppConfig.getInstanse().get("files:user_uploads"));
    },
    filename: function (req: RequestInterface, file: any, cb: any): void {
        cb(null, file.fieldname + "-" + Date.now() + "." + mime.extension(file.mimetype));
    }
});
const upload: any = multer({storage: storage, limits: {fileSize: 3000000}});
const cpUpload: any = upload.single("image");

module Route {

    export class Ajax {

        public async login(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: LoginPost = Object.create(LoginPost.prototype);
            Object.assign(post, req.body, {});
            try {
                let errors: any = await validate(post);
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.json(ajaxResponse.get());
                } else {
                    let user: IUserModel = await UserModel
                        .findOne({email: post.email, deleted: false});
                    if (user == null) {
                        ajaxResponse.addErrorMessage(req.__("site.errors.UserNotFound"));
                        res.json(ajaxResponse.get());
                    } else {
                        if (user.comparePassword(post.password)) {
                            if (user.in_progress && post.type === "customer") {
                                res.status(403);
                                ajaxResponse.addErrorMessage(req.__("site.errors.YouAreAlreadyLoginAsCourier"));
                                res.json(ajaxResponse.get());
                            } else {
                                EventBus.dispatch(EVENT_WEB_USER_LOGIN, this, {user: user});

                                req.session.user = user;
                                ajaxResponse.setDate(user.getApiFields());
                                res.json(ajaxResponse.get());
                            }
                        } else {
                            ajaxResponse.addErrorMessage(req.__("site.errors.IncorrectPassword"));
                            res.json(ajaxResponse.get());
                        }
                    }
                }
            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }
        }

        public async register(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: RegisterPost = Object.create(RegisterPost.prototype);
            Object.assign(post, req.body, {});
            try {
                let errors: any = await validate(post);
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.json(ajaxResponse.get());
                } else {

                    post.phone = post.phone.replace(/\D/g, "");

                    let user: IUserModel = await UserModel.findOne({
                        $or: [
                            {email: post.email},
                            {phone: post.phone}
                        ]
                    });

                    // User already exists
                    if (user !== null) {
                        if (user.email === post.email) {
                            ajaxResponse.addErrorMessage(req.__("site.errors.EmailAlreadyUsedInService"));
                            res.json(ajaxResponse.get());
                        } else if (user.phone === post.phone) {
                            ajaxResponse.addErrorMessage(req.__("site.errors.PhoneAlreadyUsedInService"));
                            res.json(ajaxResponse.get());
                        }
                    } else {
                        let item: IUserModel = new UserModel();
                        item.location.coordinates = [0, 0];
                        item.location.type = "Point";
                        item.phone = post.getPhone();
                        item.email = post.email;
                        item.first_name = post.first_name;
                        item.last_name = post.last_name;
                        item.role = UserRoles.user;
                        item.setPassword(post.password);
                        item.status = UserStatus.Review;
                        item.genToken();

                        // generate confirm code
                        item.generateConfirmSmsCode();
                        item.generateConfirmEmail();

                        if (req.session.jid && req.session.jid !== "") {
                            let ref: IUserModel = await UserModel.findOne({jabroolid: req.session.jid});
                            if (ref !== null) {
                                post.jid = req.session.jid;
                            }
                        }

                        if (post.jid) {
                            let refferer: IUserModel = await UserModel
                                .findOneAndUpdate({
                                        jabroolid: post.jid,
                                    },
                                    {
                                        $push: {
                                            referals: item._id
                                        }
                                    }
                                );
                            if (refferer != null) {
                                item.referrer = refferer._id;
                                await item.save();
                            } else {
                                ajaxResponse.addErrorMessage(req.__("site.errors.JidUserNotFound"));
                                return  res.json(ajaxResponse.get());
                            }
                        }

                        await item
                            .generateJId();

                        await item
                            .save();
                        item.new_password = post.password;
                        EventBus.dispatch(EVENT_WEB_USER_REGISTER, this, {user: item});

                        req.session.user = item;
                        ajaxResponse.setDate(item.getApiFields());
                        return  res.json(ajaxResponse.get());
                    }
                }
            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }
        }

        public async contact_us(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: ContactUsModelPost = Object.create(ContactUsModelPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.json(ajaxResponse.get());
                } else {

                    let model: IContactUsModel = new ContactUsModel();
                    model.phone = post.number;
                    model.email = "--";
                    model.text = "from web site";
                    model.name = post.name;

                    model
                        .save()
                        .then(m => {
                            ajaxResponse.setDate(m.getPublicFields());
                            return  res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });

                }
            });
        }

        public upload(): express.Router {
            //get router
            let router: express.Router;
            router = express.Router();
            router.post("/", cpUpload,  this._upload.bind(this._upload));
            return router;
        }

        public _upload(req: RequestInterface, res: express.Response, next: express.NextFunction): void {

            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let file: any = req.file.filename;
            ajaxResponse.setDate({
                image: file,
                image_url: AppConfig.getInstanse().get("urls:user_uploads") + "/" + file
            });
            res.json(ajaxResponse.get());

        }

        public async saveTmpOrder(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            if (!req.session.user) {
                req.body.phone = req.body.phone.replace(/\D/g, "");

                let user: IUserModel = await UserModel.findOne({
                    $or: [
                        {email: req.body.email},
                        {phone: req.body.phone}
                    ]
                });

                // User already exists
                if (user !== null) {
                    if (user.email === req.body.email) {
                        ajaxResponse.addErrorMessage(req.__("site.errors.EmailAlreadyUsedInService"));
                        res.json(ajaxResponse.get());
                    } else if (user.phone === req.body.phone) {
                        ajaxResponse.addErrorMessage(req.__("site.errors.PhoneAlreadyUsedInService"));
                        res.json(ajaxResponse.get());
                    }
                } else {
                    let item: IUserModel = new UserModel();
                    item.location.coordinates = [0, 0];
                    item.location.type = "Point";
                    item.phone = "+" + req.body.phone;
                    item.email = req.body.email;
                    item.first_name = req.body.first_name;
                    item.last_name = req.body.last_name;
                    item.role = UserRoles.user;
                    item.setPassword(req.body.password);
                    item.status = UserStatus.Review;
                    item.pay_type = "cash";
                    item.genToken();

                    // generate confirm code
                    item.generateConfirmSmsCode();
                    item.generateConfirmEmail();

                    if (req.session.jid && req.session.jid !== "") {
                        let ref: IUserModel = await UserModel.findOne({jabroolid: req.session.jid});
                        if (ref !== null) {
                            req.body.rjid = req.session.jid;
                        }
                    }

                    if (req.body.rjid) {
                        let refferer: IUserModel = await UserModel
                            .findOneAndUpdate({
                                    jabroolid: req.body.rjid,
                                },
                                {
                                    $push: {
                                        referals: item._id
                                    }
                                }
                            );
                        if (refferer != null) {
                            item.referrer = refferer._id;
                            await item.save();
                        } else {
                            ajaxResponse.addErrorMessage(req.__("site.errors.JidUserNotFound"));
                            return res.json(ajaxResponse.get());
                        }
                    }

                    await item
                        .generateJId();

                    await item
                        .save();
                    item.new_password = req.body.password;
                    EventBus.dispatch(EVENT_WEB_USER_REGISTER, this, {user: item});

                    req.session.user = item;

                }
            }

            let post: CreateOrderWebPost = Object.create(CreateOrderWebPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.json(ajaxResponse.get());
                } else {
                    /**
                     * Check promo
                     */
                    CreateOrderHelper.findRecipientWeb(req, res, post, ajaxResponse);
                }
            });
        }

        public async calculateDistance(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            try {
                let response: any = await axios.get("https://maps.googleapis.com/maps/api/distancematrix/json", {
                    params: {
                        units: "metric",
                        origins: req.body.owner.lat + "," + req.body.owner.lon,
                        destinations: req.body.recipient.lat + "," + req.body.recipient.lon,
                        key: AppConfig.getInstanse().get("gmap_key")
                    }
                });
                ajaxResponse.setDate(response.data);
                return  res.json(ajaxResponse.get());
            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }
        }
    }
}

export = Route;

"use strict";

import * as express from "express";
import * as publicIndexndexRoute from "./public/index";
import * as adminIndexndexRoute from "./admin/index";
import * as apiIndexndexRoute from "./api/index";
import {ApiParser} from "../components/ApiParser";

module Route {

    export class Index {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let publicIndex: publicIndexndexRoute.Index = new publicIndexndexRoute.Index();
            router.use("/", publicIndex.index());

            let adminIndex: adminIndexndexRoute.Index = new adminIndexndexRoute.Index();
            router.use("/admin", adminIndex.index());

            let apiIndexndex: apiIndexndexRoute.Index = new apiIndexndexRoute.Index();
            router.use("/api", ApiParser.parser, apiIndexndex.index());

            return router;
        }
    }
}

export = Route;
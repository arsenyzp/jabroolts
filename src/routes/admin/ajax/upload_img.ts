"use strict";

import * as express from "express";
import * as multer from "multer";
import * as mime from "mime-types";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {AppConfig} from "../../../components/config";

const storage: any  = multer.diskStorage({
    destination: function (req: RequestInterface, file: any, cb: any): void {
        cb(null, AppConfig.getInstanse().get("files:user_uploads"));
    },
    filename: function (req: RequestInterface, file: any, cb: any): void {
        cb(null, file.fieldname + "-" + Date.now() + "." + mime.extension(file.mimetype));
    }
});
const upload: any = multer({storage: storage, limits: {fileSize: 3000000}});
const cpUpload: any = upload.single("image");

module Route {

    export class UploadsImg {

        public upload(): express.Router {
            //get router
            let router: express.Router;
            router = express.Router();
            router.post("/", cpUpload,  this.index.bind(this.index));
            return router;
        }

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {

            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let file: any = req.file.filename;
            ajaxResponse.setDate({
                image: file,
                image_url: AppConfig.getInstanse().get("urls:user_uploads") + "/" + file
            });
            res.json(ajaxResponse.get());

        }

    }
}

export = Route;
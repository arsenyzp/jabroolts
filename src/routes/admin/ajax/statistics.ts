"use strict";

import * as express from "express";
import * as escapeStringRegexp from "escape-string-regexp";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {CountryModel} from "../../../models/CountryModel";
import {IUserModel, UserModel, UserStatus} from "../../../models/UserModel";
import {IOrderModel, OrderModel, OrderStatuses} from "../../../models/OrderModel";
import {DebtLogModel, IDebtLogModel} from "../../../models/DebtLogModel";
import {UserActivityLogModel} from "../../../models/UserActivityLogModel";

module Route {

    class StatObj {
        total_users: number = 0;
        total_couriers: number = 0;
        total_orders: number = 0;
        jabrool_earning: number = 0;
        couriers_earning: number = 0;
        users_debt: number = 0;

        percent_total_users: number = 0;
        percent_total_couriers: number = 0;
        percent_total_orders: number = 0;
        percent_jabrool_earning: number = 0;
        percent_couriers_earning: number = 0;
        percent_users_debt: number = 0;
    }

    class DateInterval {
        startAt: number = 0;
        endAt: number = 0;
    }

    export class Statistics {

        public async index(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let stat: StatObj = new StatObj();

            let currentDate: Date = new Date();
            // Start - end date for current week
            let currentWeek: DateInterval = new DateInterval();
            currentWeek.endAt = currentDate.getTime();
            currentDate.setDate(currentDate.getDate() - currentDate.getDay() + 1);
            currentWeek.startAt = currentDate.getTime();
            // Start - end date for prev week
            let prevWeek: DateInterval = new DateInterval();
            prevWeek.endAt = currentDate.getTime();
            currentDate.setDate(currentDate.getDate() - 7);
            prevWeek.startAt = currentDate.getTime();

            // Users
            let totalUsers: number = await UserModel.count({});
            let totalUsersCurrentWeek: number = await UserModel.count({created_at: {$gte: currentWeek.startAt, $lte: currentWeek.endAt}});
            let totalUsersPrevWeek: number = await UserModel.count({created_at: {$gte: prevWeek.startAt, $lte: prevWeek.endAt}});

            stat.total_users = totalUsers;
            stat.percent_total_users = (totalUsersCurrentWeek - totalUsersPrevWeek) / (totalUsersPrevWeek / 100);
            if (totalUsersPrevWeek === 0) {
                stat.percent_total_users = 0;
            }

            // Couriers
            let totalCouriers: number = await UserModel.count({status: UserStatus.Active});
            let totalCouriersCurrentWeek: number = await UserModel.count({
                created_at: {$gte: currentWeek.startAt, $lte: currentWeek.endAt},
                status: UserStatus.Active
            });
            let totalCouriersPrevWeek: number = await UserModel.count({
                created_at: {$gte: prevWeek.startAt, $lte: prevWeek.endAt},
                status: UserStatus.Active
            });

            stat.total_couriers = totalCouriers;
            stat.percent_total_couriers = (totalCouriersCurrentWeek - totalCouriersPrevWeek) / (totalCouriersPrevWeek / 100);
            if (totalCouriersPrevWeek === 0) {
                stat.percent_total_couriers = 0;
            }

            // Orders
            let totalOrders: number = await OrderModel.count({});
            let totalOrdersCurrentWeek: number = await OrderModel.count({created_at: {$gte: currentWeek.startAt, $lte: currentWeek.endAt}});
            let totalOrdersPrevWeek: number = await OrderModel.count({created_at: {$gte: prevWeek.startAt, $lte: prevWeek.endAt}});

            stat.total_orders = totalOrders;
            stat.percent_total_orders = (totalOrdersCurrentWeek - totalOrdersPrevWeek) / (totalOrdersPrevWeek / 100);
            if (totalOrdersPrevWeek === 0) {
                stat.percent_total_orders = 0;
            }

            // Jabrool Earning
            let jabroolEarningTotal: number = 0;
            let jabroolEarningCurrentWeek: number = 0;
            let jabroolEarningPrevWeek: number = 0;

            let courierEarningTotal: number = 0;
            let courierEarningCurrentWeek: number = 0;
            let courierEarningPrevWeek: number = 0;

            let ordersFinished: Array<IOrderModel> = await OrderModel.find({status: OrderStatuses.Finished});
            let ordersFinishedCurrentWeek: Array<IOrderModel> = await OrderModel.find({
                created_at: {$gte: currentWeek.startAt, $lte: currentWeek.endAt},
                status: OrderStatuses.Finished
            });
            let ordersFinishedPrevWeek: Array<IOrderModel> = await OrderModel.find({
                created_at: {$gte: prevWeek.startAt, $lte: prevWeek.endAt},
                status: OrderStatuses.Finished
            });

            ordersFinished.map((v, k, a) => {
                jabroolEarningTotal += v.serviceFee;
                courierEarningTotal += v.cost - v.serviceFee;
            });

            ordersFinishedCurrentWeek.map((v, k, a) => {
                jabroolEarningCurrentWeek += v.serviceFee;
                courierEarningCurrentWeek += v.cost - v.serviceFee;
            });

            ordersFinishedPrevWeek.map((v, k, a) => {
                jabroolEarningPrevWeek += v.serviceFee;
                courierEarningPrevWeek += v.cost - v.serviceFee;
            });

            stat.jabrool_earning = jabroolEarningTotal;
            stat.percent_jabrool_earning = (jabroolEarningCurrentWeek - jabroolEarningPrevWeek) / (jabroolEarningPrevWeek / 100);
            if (jabroolEarningPrevWeek === 0) {
                stat.percent_jabrool_earning = 0;
            }

            stat.couriers_earning = courierEarningTotal;
            stat.percent_couriers_earning = (courierEarningCurrentWeek - courierEarningPrevWeek) / (courierEarningPrevWeek / 100);
            if (courierEarningPrevWeek === 0) {
                stat.percent_couriers_earning = 0;
            }

            //User's debt
            let users: Array<IUserModel> =  await UserModel.find({balance: {$lt: 0}});
            let totalDebt: number = 0;
            users.map((v, k, a) => {
                totalDebt -= v.balance;
            });

            let totalDebtCurrentWeek: number = 0;
            let totalDebtPrevWeek: number = 0;
            let logDebtCurrentWeek: Array<IDebtLogModel> = await DebtLogModel.find({
                created_at: {$gte: currentWeek.startAt, $lte: currentWeek.endAt}});
            let logDebtPrevWeek: Array<IDebtLogModel> = await DebtLogModel.find({
                created_at: {$gte: prevWeek.startAt, $lte: prevWeek.endAt}});

            logDebtCurrentWeek.map((v, k, a) => {
                totalDebtCurrentWeek += v.value;
            });

            logDebtPrevWeek.map((v, k, a) => {
                totalDebtPrevWeek += v.value;
            });

            stat.users_debt = totalDebt;
            stat.percent_users_debt = (totalDebtCurrentWeek - totalDebtPrevWeek) / (totalDebtPrevWeek / 100);
            if (totalDebtPrevWeek === 0) {
                stat.percent_users_debt = 0;
            }


            console.log(stat);

            ajaxResponse.setDate(stat);
            res.json(ajaxResponse.get());
        }

        public users(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let currentDate: Date = new Date();
            // Start - end date for current week
            let currentWeek: DateInterval = new DateInterval();
            currentWeek.endAt = currentDate.getTime();
            currentDate.setDate(currentDate.getDate() - 7);
            currentWeek.startAt = currentDate.getTime();

            UserActivityLogModel
                .find({ created_at: {$gte: currentWeek.startAt, $lte: currentWeek.endAt}})
                .sort({created_at: -1})
                .then(items => {
                    ajaxResponse.setDate(items);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public async now(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            try {
                let couriers: number =  await UserModel.count({$or: [
                        {web_socket_ids: { $exists: true, $not: {$size: 0} } },
                        {socket_ids: { $exists: true, $not: {$size: 0} } }
                    ],
                    status: UserStatus.Active
                });
                let customers: number =  await UserModel.count({$or: [
                        {web_socket_ids: { $exists: true, $not: {$size: 0} } },
                        {socket_ids: { $exists: true, $not: {$size: 0} } }
                    ],
                    status: {$ne: UserStatus.Active}
                });
                let total: number =  await UserModel.count({});

                let orders: Array<IOrderModel> = await OrderModel.find({status: {$nin: [
                            OrderStatuses.Finished,
                            OrderStatuses.Canceled,
                            OrderStatuses.Missed
                        ]}});
                let je: number = 0;
                let ce: number = 0;
                orders.map((v, k, a) => {
                    je += v.serviceFee;
                    ce += (v.cost - v.serviceFee);
                });

                let avarageCourierRating: number = 0;
                let avarageCustomerRating: number = 0;
                let avarageCourierCount: number = 0;
                let avarageCustomerCount: number = 0;
                let users: Array<IUserModel> = await UserModel.find({deleted: false});

                users.map((v, k, a) => {
                    if (v.status === UserStatus.Active) {
                        avarageCourierRating += v.rating;
                        avarageCourierCount ++;
                    } else {
                        avarageCustomerRating += v.rating;
                        avarageCustomerCount ++;
                    }
                });

                avarageCourierRating = avarageCourierRating / avarageCourierCount;
                if (avarageCourierCount === 0) {
                    avarageCourierRating = 0;
                }
                avarageCustomerRating = avarageCustomerRating / avarageCustomerCount;
                if (avarageCustomerCount === 0) {
                    avarageCustomerRating = 0;
                }

                ajaxResponse.setDate({
                    couriers: couriers,
                    customers: customers,
                    total: total,
                    je: je,
                    ce: ce,
                    avarageCourierRating: avarageCourierRating,
                    avarageCustomerRating: avarageCustomerRating,
                    orders: orders.length
                });
                res.json(ajaxResponse.get());
            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }

        }

    }
}

export = Route;
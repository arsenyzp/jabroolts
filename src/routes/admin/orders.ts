"use strict";

import * as express from "express";
import * as async from "async";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {AjaxResponse} from "../../components/AjaxResponse";
import {IUserModel, UserModel, UserStatus} from "../../models/UserModel";
import {OrderModel, OrderStatuses} from "../../models/OrderModel";
import {ContactUsModel} from "../../models/ContactUsModel";

module Route {

    export class UserOrders {

        public async index(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let user: IUserModel = await UserModel.findOne({_id: req.query.user});

            // search
            let filter: any = {owner: req.query.user};
            if (req.query.courier === "true") {
                filter = {courier: req.query.user};
            }

            // paging
            let skip: number = 0;
            let limit: number = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }

            try {
                let total_count: number = await OrderModel
                    .count(filter);
                let items: Array<any> = await  OrderModel
                    .find(filter)
                    .populate({path: "courier", model: UserModel})
                    .populate({path: "owner", model: UserModel})
                    .sort({created_at: -1})
                    .limit(limit)
                    .skip(skip);
                let data: Array<any> = [];
                async.eachOfSeries(items, (item, key, cb) => {
                    let model: any = {};
                    model._id = item._id;
                    model.order = item._id;
                    model.courier = !!item.courier ? item.courier.jabroolid : "--";
                    model.owner = !!item.owner ? item.owner.jabroolid : "--";
                    model.pickup_location = item.owner_address;
                    model.drop_off_location = item.recipient_address;
                    model.route = item.route;
                    model.number_of_parcel = item.small_package_count + item.medium_package_count + item.large_package_count;
                    model.accept_time = item.accept_at;
                    model.amount = item.cost;
                    model.pay_type = item.pay_type;
                    model.status = item.status;
                    model.created_at = item.created_at;
                    model.orderId = item.orderId;
                    data.push(model);
                    cb();
                }, err => {
                    if (err) {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    } else {
                        ajaxResponse.setDate({
                            items: data,
                            total_count: total_count,
                            user: user.getApiPublicFields()
                        });
                        res.json(ajaxResponse.get());
                    }
                });
            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }
        }

        public get(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            OrderModel
                .findOne({_id: req.params.id})
                .populate({path: "courier", model: UserModel})
                .populate({path: "owner", model: UserModel})
                .then(order => {
                    if (order == null) {
                        ajaxResponse.addErrorMessage("Item not found");
                        res.status(500);
                        return res.json(ajaxResponse.get());
                    }
                    ajaxResponse.setDate(order);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

    }
}

export = Route;
"use strict";

import * as express from "express";
import * as escapeStringRegexp from "escape-string-regexp";
import * as async from "async";
import * as json2csv from "json2csv";
import * as moment from "moment";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {CountryModel} from "../../../models/CountryModel";
import {UserModel, UserStatus} from "../../../models/UserModel";
import {OrderModel, OrderStatuses} from "../../../models/OrderModel";
import {OnlineLogModel} from "../../../models/OnlineLogModel";

module Route {

    export class Couriers {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            // search
            let filter: any = {status: UserStatus.Active};

            // paging
            let skip: number = 0;
            let limit: number = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }

            let search: string = "";
            if (req.query.q !== undefined && req.query.q !== "") {
                search = escapeStringRegexp(req.query.q);
                filter = {$or: [
                        {jabroolid: new RegExp(".*" + search + ".*", "i")},
                        // {first_name: new RegExp(".*" + search + ".*", "i")},
                        // {last_name: new RegExp(".*" + search + ".*", "i")},
                        // {phone: new RegExp(".*" + search + ".*", "i")},
                        //{email: new RegExp(".*" + search + ".*", "i")}
                    ]};
            }
            filter.deleted = {$in: [false, null]};

            UserModel
                .count(filter)
                .then(total_count => {
                    UserModel
                        .find(filter)
                        .sort({created_at: -1})
                        .limit(limit)
                        .skip(skip)
                        .then(items => {
                            let data: Array<any> = [];
                            async.eachOfSeries(items, (item, key, cb) => {
                                let model: any = {};
                                model._id = item._id;
                                model.jabroolid = item.jabroolid;
                                model.account_type = "individual";
                                model.average = item.rating;

                                async.parallel([
                                    cb => {
                                        OrderModel
                                            .count({courier: item._id, status: OrderStatuses.Finished})
                                            .then(completed => {
                                                model.completed = completed;
                                                cb();
                                            })
                                            .catch(err => {
                                                cb(err);
                                            });
                                    },
                                    cb => {
                                        OrderModel
                                            .count({courier: item._id, status: OrderStatuses.Canceled})
                                            .then(canceled => {
                                                model.canceled = canceled;
                                                cb();
                                            })
                                            .catch(err => {
                                                cb(err);
                                            });
                                    },
                                    cb => {
                                        OnlineLogModel
                                            .find({user: item._id})
                                            .then(logsOnline => {
                                                let online: number = 0;
                                                for ( let i: number = 0; i < logsOnline.length; i++) {
                                                    online += logsOnline[i].duration;
                                                }
                                                model.online = (online / (1000 * 60 * 60)).toFixed(2);
                                                data.push(model);
                                                cb();
                                            })
                                            .catch(err => {
                                                cb(err);
                                            });
                                    },
                                    cb => {
                                        OrderModel
                                            .count({
                                                $or: [
                                                    {courier: {$ne: item._id}, notified_couriers: item._id},
                                                    {courier: {$ne: item._id}, declined_couriers: item._id},
                                                    {courier: {$ne: item._id}, canceled_couriers: item._id}
                                                ]
                                            })
                                            .then(all => {
                                                OrderModel
                                                    .count({courier: item._id})
                                                    .then(real => {
                                                        if (real === 0) {
                                                            model.acceptance = 0;
                                                        } else {
                                                            model.acceptance = (real / (all / 100)).toFixed(2);
                                                        }
                                                        cb();
                                                    })
                                                    .catch(err => {
                                                        cb(err);
                                                    });
                                            })
                                            .catch(err => {
                                                cb(err);
                                            });
                                    }
                                ], err => {
                                    if (err) {
                                        cb(err);
                                    } else {
                                        cb();
                                    }
                                });
                            }, err => {
                                if (err) {
                                    ajaxResponse.addError(err);
                                    res.status(500);
                                    res.json(ajaxResponse.get());
                                } else {
                                    ajaxResponse.setDate({
                                        items: data,
                                        total_count: total_count
                                    });
                                    res.json(ajaxResponse.get());
                                }
                            });
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public export(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let fields: Array<string> = [
                "jabroolid",
                "account_type",
                "completed",
                "acceptance",
                "average",
                "online",
                "canceled"
            ];

            let fieldNames: Array<string> = [
                "Courior ID",
                "Account",
                "Number of Trips completed",
                "Acceptance rate",
                "Average rating",
                "Hours online",
                "Number of trips calcelled"
            ];

            let startTime: Date = new Date(parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000);
            let endTime: Date = new Date(parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000);
            startTime.setUTCHours(0, 0, 0, 0);
            endTime.setUTCHours(0, 0, 0, 0);
            if (startTime.getTime() === endTime.getTime()) {
                endTime.setUTCHours(23, 59, 59, 0);
            }

            let filter: any = {
                status: UserStatus.Active
            };

            UserModel
                .find(filter)
                .sort({created_at: -1})
                .then(items => {
                    let data: Array<any> = [];
                    async.eachOfSeries(items, (item, key, cb) => {
                        let model: any = {};
                        model.jabroolid = item.jabroolid;
                        model.account_type = "individual";
                        model.average = item.rating;

                        async.parallel([
                            cb => {
                                OrderModel
                                    .count({
                                        created_at: {
                                            $gte: startTime.getTime(),
                                            $lte: endTime.getTime()
                                        },
                                        courier: item._id, status: OrderStatuses.Finished
                                    })
                                    .then(completed => {
                                        model.completed = completed;
                                        cb();
                                    })
                                    .catch(err => {
                                        cb(err);
                                    });
                            },
                            cb => {
                                OrderModel
                                    .count({
                                        created_at: {
                                            $gte: parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000,
                                            $lte: parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000
                                        },
                                        courier: item._id, status: OrderStatuses.Canceled
                                    })
                                    .then(canceled => {
                                        model.canceled = canceled;
                                        cb();
                                    })
                                    .catch(err => {
                                        cb(err);
                                    });
                            },
                            cb => {
                                OnlineLogModel
                                    .find({
                                        created_at: {
                                            $gte: parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000,
                                            $lte: parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000
                                        },
                                        user: item._id
                                    })
                                    .then(logsOnline => {
                                        let online: number = 0;
                                        for ( let i: number = 0; i < logsOnline.length; i++) {
                                            online += logsOnline[i].duration;
                                        }
                                        model.online = (online / (1000 * 60 * 60)).toFixed(2);
                                        data.push(model);
                                        cb();
                                    })
                                    .catch(err => {
                                        cb(err);
                                    });
                            },
                            cb => {
                                OrderModel
                                    .count({
                                        created_at: {
                                            $gte: parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000,
                                            $lte: parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000
                                        },
                                        courier: {$ne: item._id},
                                        notified_couriers: item._id
                                    })
                                    .then(all => {
                                        OrderModel
                                            .count({
                                                created_at: {
                                                    $gte: parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000,
                                                    $lte: parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000
                                                },
                                                courier: item._id
                                            })
                                            .then(real => {
                                                if (real === 0) {
                                                    model.acceptance = 0;
                                                } else {
                                                    model.acceptance = (real / (all / 100)).toFixed(2);
                                                }
                                                cb();
                                            })
                                            .catch(err => {
                                                cb(err);
                                            });
                                    })
                                    .catch(err => {
                                        cb(err);
                                    });
                            }
                        ], err => {
                            if (err) {
                                cb(err);
                            } else {
                                cb();
                            }
                        });
                    }, err => {
                        if (err) {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        } else {
                            let result: any = json2csv({ data: data, fields: fields, fieldNames: fieldNames, del: ";" });
                            console.log(result);
                            res.setHeader("Content-disposition", "attachment; filename=data.csv");
                            res.header("Content-Type", "text/csv");
                            res.send(result);
                        }
                    });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }
    }
}

export = Route;
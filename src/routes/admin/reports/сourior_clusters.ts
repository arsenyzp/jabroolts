"use strict";

import * as express from "express";
import * as async from "async";
import * as json2csv from "json2csv";
import * as moment from "moment";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {OnlineLogModel} from "../../../models/OnlineLogModel";

module Route {

    export class СouriorClusters {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            // search
            let startTime: Date = new Date(parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000);
            let endTime: Date = new Date(parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000);
            startTime.setUTCHours(0, 0, 0, 0);
            endTime.setUTCHours(23, 59, 59, 0);

            let filter: any = {
                type: "no_orders",
                created_at: {$gte: startTime.getTime(), $lte: endTime.getTime()}
            };

            // paging
            let skip: number = 0;
            let limit: number = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }

            OnlineLogModel
                .count(filter)
                .then(total_count => {
                    OnlineLogModel
                        .find(filter)
                        .sort({created_at: -1})
                        .limit(limit)
                        .skip(skip)
                        .then(items => {
                            let data: Array<any> = [];
                            async.eachOfSeries(items, (item, key, cb) => {
                                let model: any = {};
                                model.date = item.created_at;
                                if (!!item.location && !!item.location.coordinates) {
                                    model.map = "https://www.google.com/maps/search/?api=1&query=" + item.location.coordinates[0] + "," +
                                        "" + item.location.coordinates[1];
                                } else {
                                    model.map = "--";
                                }
                                model.address = "--";
                                model.count = 1;
                                model.lat = item.location.coordinates[0];
                                model.lon = item.location.coordinates[1];
                                data.push(model);
                                cb();
                            }, err => {
                                if (err) {
                                    ajaxResponse.addError(err);
                                    res.status(500);
                                    res.json(ajaxResponse.get());
                                } else {
                                    ajaxResponse.setDate({
                                        items: data,
                                        total_count: total_count
                                    });
                                    res.json(ajaxResponse.get());
                                }
                            });
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public export(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let fields: Array<string> = [
                "address",
                "map",
                "count",
                "date"
            ];

            let fieldNames: Array<string> = [
                "Locations where no Customer avalabile while courior online",
                "Map",
                "Number of times no Couriors around at this location",
                "Timestamp"
            ];

            let startTime: Date = new Date(parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000);
            let endTime: Date = new Date(parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000);
            startTime.setUTCHours(0, 0, 0, 0);
            endTime.setUTCHours(0, 0, 0, 0);
            if (startTime.getTime() === endTime.getTime()) {
                endTime.setUTCHours(23, 59, 59, 0);
            }

            let filter: any = {
                type: "no_orders",
                created_at: {$gte: startTime.getTime(), $lte: endTime.getTime()}
            };

            OnlineLogModel
                .find(filter)
                .sort({created_at: -1})
                .then(items => {
                    let data: Array<any> = [];
                    async.eachOfSeries(items, (item, key, cb) => {
                        let model: any = {};
                        model.date = item.created_at;
                        if (!!item.location && !!item.location.coordinates) {
                            model.map = "https://www.google.com/maps/search/?api=1&query=" + item.location.coordinates[0] + "," +
                                "" + item.location.coordinates[1];
                        } else {
                            model.map = "--";
                        }
                        model.address = "--";
                        model.count = 1;
                        data.push(model);
                        cb();
                    }, err => {
                        if (err) {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        } else {
                            let result: any = json2csv({ data: data, fields: fields, fieldNames: fieldNames, del: ";" });
                            console.log(result);
                            res.setHeader("Content-disposition", "attachment; filename=data.csv");
                            res.header("Content-Type", "text/csv");
                            res.send(result);
                        }
                    });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }
    }
}

export = Route;
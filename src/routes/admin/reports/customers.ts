"use strict";

import * as express from "express";
import * as async from "async";
import * as json2csv from "json2csv";
import * as moment from "moment";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {CountryModel} from "../../../models/CountryModel";
import {UserModel, UserStatus} from "../../../models/UserModel";
import {OrderModel, OrderStatuses} from "../../../models/OrderModel";
import {OnlineLogModel} from "../../../models/OnlineLogModel";
import {OpenAppLogModel} from "../../../models/OpenAppLogModel";
import {DeliveryConfig} from "../../../models/configs/DeliveryConfig";

module Route {

    export class Customers {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            // sort
            let sort: any = {};
            if (req.query.sortKey) {
                sort[req.query.sortKey] = req.query.reverse;
            } else {
                sort = {created_at: -1};
            }

            // search
            let filter: any = {};

            // paging
            let skip: number = 0;
            let limit: number = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }

            UserModel
                .count(filter)
                .then(total_count => {
                    UserModel
                        .find(filter)
                        .sort(sort)
                        .limit(limit)
                        .skip(skip)
                        .then(items => {
                            let data: Array<any> = [];
                            async.eachOfSeries(items, (item, key, cb) => {
                                let model: any = {};
                                model._id = item._id;
                                model.jabroolid = item.jabroolid;
                                model.average = item.rating;

                                async.parallel([
                                    cb => {
                                        OrderModel
                                            .count({owner: item._id, status: OrderStatuses.Finished})
                                            .then(completed => {
                                                model.completed = completed;
                                                cb();
                                            })
                                            .catch(err => {
                                                cb(err);
                                            });
                                    },
                                    cb => {
                                        OrderModel
                                            .count({owner: item._id, status: OrderStatuses.Canceled})
                                            .then(canceled => {
                                                model.canceled = canceled;
                                                cb();
                                            })
                                            .catch(err => {
                                                cb(err);
                                            });
                                    },
                                    cb => {
                                        OpenAppLogModel
                                            .count({user: item._id})
                                            .then(c => {
                                                model.opened = c;
                                                cb();
                                            })
                                            .catch(err => {
                                                cb(err);
                                            });
                                    },
                                    cb => {
                                        OrderModel
                                            .count({owner: item._id, status: OrderStatuses.Missed})
                                            .then(all => {
                                                model.missed = all;
                                                cb();
                                            })
                                            .catch(err => {
                                                cb(err);
                                            });
                                    },
                                    cb => {
                                        OrderModel
                                            .find({owner: item._id, status: OrderStatuses.Finished})
                                            .then(all => {
                                                model.paid = 0;
                                                for (let i: number = 0; i < all.length; i++) {
                                                    model.paid += all[i].cost;
                                                }
                                                cb();
                                            })
                                            .catch(err => {
                                                cb(err);
                                            });
                                    }
                                ], err => {
                                    if (err) {
                                        cb(err);
                                    } else {
                                        data.push(model);
                                        cb();
                                    }
                                });
                            }, err => {
                                if (err) {
                                    ajaxResponse.addError(err);
                                    res.status(500);
                                    res.json(ajaxResponse.get());
                                } else {
                                    ajaxResponse.setDate({
                                        items: data,
                                        total_count: total_count
                                    });
                                    res.json(ajaxResponse.get());
                                }
                            });
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public export(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let fields: Array<string> = [
                "jabroolid",
                "completed",
                "canceled",
                "average",
                "opened",
                "missed",
                "paid"
            ];

            let fieldNames: Array<string> = [
                "Customer ID",
                "Number of Trips completed",
                "Number of trips calcelled",
                "Average rating",
                "Number of times open app",
                "Number of times no Couriors around",
                "Total amount paid"
            ];

            let startTime: Date = new Date(parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000);
            let endTime: Date = new Date(parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000);
            startTime.setUTCHours(0, 0, 0, 0);
            endTime.setUTCHours(0, 0, 0, 0);
            if (startTime.getTime() === endTime.getTime()) {
                endTime.setUTCHours(23, 59, 59, 0);
            }

            let filter: any = {
                status: UserStatus.Active
            };

            UserModel
                .find(filter)
                .sort({created_at: -1})
                .then(items => {
                    let data: Array<any> = [];
                    async.eachOfSeries(items, (item, key, cb) => {
                        let model: any = {};
                        model.jabroolid = item.jabroolid;
                        model.average = item.rating;

                        async.parallel([
                            cb => {
                                OrderModel
                                    .count({
                                        created_at: {
                                            $gte: startTime.getTime(),
                                            $lte: endTime.getTime()
                                        },
                                        owner: item._id,
                                        status: OrderStatuses.Finished
                                    })
                                    .then(completed => {
                                        model.completed = completed;
                                        cb();
                                    })
                                    .catch(err => {
                                        cb(err);
                                    });
                            },
                            cb => {
                                OrderModel
                                    .count({
                                        created_at: {
                                            $gte: parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000,
                                            $lte: parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000
                                        },
                                        owner: item._id,
                                        status: OrderStatuses.Canceled
                                    })
                                    .then(canceled => {
                                        model.canceled = canceled;
                                        cb();
                                    })
                                    .catch(err => {
                                        cb(err);
                                    });
                            },
                            cb => {
                                OpenAppLogModel
                                    .count({
                                        created_at: {
                                            $gte: parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000,
                                            $lte: parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000
                                        },
                                        user: item._id
                                    })
                                    .then(c => {
                                        model.opened = c;
                                        cb();
                                    })
                                    .catch(err => {
                                        cb(err);
                                    });
                            },
                            cb => {
                                let deliveryConfig: DeliveryConfig = new DeliveryConfig();
                                deliveryConfig
                                    .getInstanse()
                                    .then(cf => {
                                        let missedTime: number = cf.missed_time * 60 * 1000;
                                        OrderModel
                                            .find({
                                                created_at: {
                                                    $gte: parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000,
                                                    $lte: parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000
                                                },
                                                owner: item._id
                                            })
                                            .then(all => {
                                                model.missed = 0;
                                                for (let i: number = 0; i < all.length; i++) {
                                                    if (all[i].start_at > 0 && all[i].start_at - all[i].created_at > missedTime) {
                                                        model.missed ++;
                                                    }
                                                    if (all[i].notified_couriers.length === 0) {
                                                        model.missed ++;
                                                    }
                                                }
                                                cb();
                                            })
                                            .catch(err => {
                                                cb(err);
                                            });
                                    })
                                    .catch(err => {
                                        cb(err);
                                    });
                            },
                            cb => {
                                OrderModel
                                    .find({
                                        created_at: {
                                            $gte: parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000,
                                            $lte: parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000
                                        },
                                        owner: item._id,
                                        status: OrderStatuses.Finished
                                    })
                                    .then(all => {
                                        model.paid = 0;
                                        for (let i: number = 0; i < all.length; i++) {
                                            model.paid += all[i].cost;
                                        }
                                        cb();
                                    })
                                    .catch(err => {
                                        cb(err);
                                    });
                            }
                        ], err => {
                            if (err) {
                                cb(err);
                            } else {
                                cb();
                            }
                        });
                    }, err => {
                        if (err) {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        } else {
                            let result: any = json2csv({ data: data, fields: fields, fieldNames: fieldNames, del: ";" });
                            console.log(result);
                            res.setHeader("Content-disposition", "attachment; filename=data.csv");
                            res.header("Content-Type", "text/csv");
                            res.send(result);
                        }
                    });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }
    }
}

export = Route;
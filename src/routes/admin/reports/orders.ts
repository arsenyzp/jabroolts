"use strict";

import * as express from "express";
import * as async from "async";
import * as json2csv from "json2csv";
import * as moment from "moment";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {CountryModel} from "../../../models/CountryModel";
import {UserModel, UserStatus} from "../../../models/UserModel";
import {OrderModel, OrderStatuses} from "../../../models/OrderModel";
import {OnlineLogModel} from "../../../models/OnlineLogModel";

module Route {

    export class Orders {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            // search
            let filter: any = {courier : {$nin: [null]}, status: OrderStatuses.Finished};

            // paging
            let skip: number = 0;
            let limit: number = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }

            OrderModel
                .count(filter)
                .then(total_count => {
                    OrderModel
                        .find(filter)
                        .populate({path: "courier", model: UserModel})
                        .sort({created_at: -1})
                        .limit(limit)
                        .skip(skip)
                        .then(items => {
                            let data: Array<any> = [];
                            async.eachOfSeries(items, (item, key, cb) => {
                                let model: any = {};
                                model._id = item._id;
                                model.tripID = item.orderId;
                                model.date = item.created_at;
                                model.city = item.owner_map_url;
                                model.deliveryType = item.type;
                                model.courierJID = item.courier.jabroolid;
                                model.tripDuration = (item.end_at - item.start_at) / 60 * 1000;
                                model.tripDistance = item.route;
                                model.tripWaitTime = (item.start_at - item.created_at ) / 60 * 1000;
                                model.tripPrice = item.cost;
                                model.tripPrice = item.cost - item.serviceFee;
                                model.paymentMethod = item.pay_type;
                                model.jabroolEarnings = item.serviceFee;
                                model.roundDown = item.cost;
                                model.orderId = item.orderId;
                                data.push(model);
                                cb();
                            }, err => {
                                if (err) {
                                    ajaxResponse.addError(err);
                                    res.status(500);
                                    res.json(ajaxResponse.get());
                                } else {
                                    ajaxResponse.setDate({
                                        items: data,
                                        total_count: total_count
                                    });
                                    res.json(ajaxResponse.get());
                                }
                            });
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public export(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let fields: Array<string> = [
                "tripID",
                "date",
                "city",
                "deliveryType",
                "courierJID",
                "tripDuration",
                "tripDistance",
                "tripWaitTime",
                "tripPrice",
                "courierEarnings",
                "paymentMethod",
                "jabroolEarnings",
                "roundDown"
            ];

            let fieldNames: Array<string> = [
                "Trip ID",
                "Trip date and time",
                "City",
                "Delivery type",
                "Courior ID",
                "Trip duration",
                "Distance travelled (km)",
                "Courior wait time for customer at pickup (mins)",
                "Trip price (SAR)",
                "Courior earnings (SAR)",
                "Payment method",
                "Jabrool ernings",
                "round down"
            ];

            let startTime: Date = new Date(parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000);
            let endTime: Date = new Date(parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000);
            startTime.setUTCHours(0, 0, 0, 0);
            endTime.setUTCHours(0, 0, 0, 0);
            if (startTime.getTime() === endTime.getTime()) {
                endTime.setUTCHours(23, 59, 59, 0);
            }

            let filter: any = {
                courier : {$nin: [null]},
                created_at: {$gte: startTime.getTime(), $lte: endTime.getTime()},
                status: OrderStatuses.Finished};

            OrderModel
                .find(filter)
                .sort({created_at: -1})
                .then(items => {
                    let data: Array<any> = [];
                    async.eachOfSeries(items, (item, key, cb) => {
                        let model: any = {};
                        model.tripID = item.orderId;
                        model.date = item.created_at;
                        model.city = item.owner_map_url;
                        model.deliveryType = item.type;
                        model.courierJID = item.courier.jabroolid;
                        model.tripDuration = (item.end_at - item.start_at) / 60 * 1000;
                        model.tripDistance = item.route;
                        model.tripWaitTime = (item.start_at - item.created_at) / 60 * 1000;
                        model.tripPrice = item.cost;
                        model.tripPrice = item.cost - item.serviceFee;
                        model.paymentMethod = item.pay_type;
                        model.jabroolEarnings = item.serviceFee;
                        model.roundDown = item.cost;
                        data.push(model);
                        cb();
                    }, err => {
                        if (err) {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        } else {
                            let result: any = json2csv({ data: data, fields: fields, fieldNames: fieldNames, del: ";" });
                            console.log(result);
                            res.setHeader("Content-disposition", "attachment; filename=data.csv");
                            res.header("Content-Type", "text/csv");
                            res.send(result);
                        }
                    });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }
    }
}

export = Route;
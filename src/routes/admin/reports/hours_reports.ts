"use strict";

import * as express from "express";
import * as async from "async";
import * as json2csv from "json2csv";
import * as moment from "moment";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {OrderModel, OrderStatuses} from "../../../models/OrderModel";
import {OpenAppLogModel} from "../../../models/OpenAppLogModel";
import {DeliveryConfig} from "../../../models/configs/DeliveryConfig";

module Route {

    export class HoursReports {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            // paging
            let skip: number = 0;
            let limit: number = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }

            let dates: Array<any> = [];
            let currentDate: Date = new Date();
            currentDate.setUTCMinutes(0);
            currentDate.setUTCHours(currentDate.getUTCHours() - skip);
            for (let i: number = 0; i < limit; i++) {
                currentDate.setUTCHours(currentDate.getUTCHours() - 1);
                dates.push(currentDate.getTime());
            }

            const resultData: Array<any> = [];
            async.eachOfSeries(dates, (item, key, cb) => {
                let it: any = {};
                async.parallel([
                    cb => {
                        OpenAppLogModel
                            .count({created_at: {$gte: item, $lte: item + 60 * 60 * 1000 }})
                            .then(c => {
                                it.opened = c;
                                cb();
                            })
                            .catch(err => {
                                cb(err);
                            });
                    },
                    cb => {
                        OrderModel
                            .count({end_at: {$gte: item, $lte: item + 60 * 60 * 1000 }, status: OrderStatuses.Finished})
                            .then(c => {
                                it.completed = c;
                                cb();
                            })
                            .catch(err => {
                                cb(err);
                            });
                    },
                    cb => {
                        OrderModel
                            .count({
                                created_at: {$gte: item, $lte: item + 60 * 60 * 1000 },
                                status: OrderStatuses.New,
                                "notified_couriers.0": { "$exists": false }
                            })
                            .then(c => {
                                it.unoccupied = c;
                                cb();
                            })
                            .catch(err => {
                                cb(err);
                            });
                    },
                    cb => {
                        let deliveryConfig: DeliveryConfig = new DeliveryConfig();
                        deliveryConfig
                            .getInstanse()
                            .then(cf => {
                                let missedTime: number = cf.missed_time * 60 * 1000;
                                OrderModel
                                    .count({
                                        created_at: {$gte: item + missedTime, $lte: item + 60 * 60 * 1000 + missedTime },
                                        status: OrderStatuses.New,
                                        "notified_couriers.0": { "$exists": true }
                                    })
                                    .then(c => {
                                        it.missed = c;
                                        cb();
                                    })
                                    .catch(err => {
                                        cb(err);
                                    });
                            })
                            .catch(err => {
                                cb(err);
                            });
                    }
                ], err => {
                    if (err) {
                        cb(err);
                    } else {
                        it.date = item;
                        resultData.push(it);
                        cb();
                    }
                });
            }, err => {
                if (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                } else {
                    ajaxResponse.setDate({
                        items: resultData,
                        total_count: dates.length * 100
                    });
                    res.json(ajaxResponse.get());
                }
            });
        }

        public export(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let fields: Array<string> = [
                "date",
                "hour",
                "opened",
                "completed",
                "unoccupied",
                "missed"
            ];

            let fieldNames: Array<string> = [
                "Date",
                "Hour",
                "Number of times Jabrool app was opened",
                "Number of completed trips",
                "Number of unoccupied Couriors",
                "Number of missed bookings (no car was available)"
            ];

            let dates: Array<any> = [];
            let startTime: Date = new Date(parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000);
            let endTime: Date = new Date(parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000);
            startTime.setUTCHours(0, 0, 0, 0);
            endTime.setUTCHours(0, 0, 0, 0);
            if (startTime.getTime() === endTime.getTime()) {
                endTime.setUTCHours(23, 59, 59, 0);
            }
            while (startTime.getTime() <= endTime.getTime() && dates.length < 1000) {
                startTime.setUTCHours(startTime.getUTCHours() + 1);
                dates.push(startTime.getTime());

                console.log(startTime + " - " + endTime);
            }

            const resultData: Array<any> = [];
            async.eachOfSeries(dates, (item, key, cb) => {
                let it: any = {};
                async.parallel([
                    cb => {
                        OpenAppLogModel
                            .count({created_at: {$gte: item, $lte: item + 60 * 60 * 1000 }})
                            .then(c => {
                                it.opened = c;
                                cb();
                            })
                            .catch(err => {
                                cb(err);
                            });
                    },
                    cb => {
                        OrderModel
                            .count({end_at: {$gte: item, $lte: item + 60 * 60 * 1000 }, status: OrderStatuses.Finished})
                            .then(c => {
                                it.completed = c;
                                cb();
                            })
                            .catch(err => {
                                cb(err);
                            });
                    },
                    cb => {
                        OrderModel
                            .count({
                                created_at: {$gte: item, $lte: item + 60 * 60 * 1000 },
                                status: OrderStatuses.New,
                                "notified_couriers.0": { "$exists": false }
                            })
                            .then(c => {
                                it.unoccupied = c;
                                cb();
                            })
                            .catch(err => {
                                cb(err);
                            });
                    },
                    cb => {
                        let deliveryConfig: DeliveryConfig = new DeliveryConfig();
                        deliveryConfig
                            .getInstanse()
                            .then(cf => {
                                let missedTime: number = cf.missed_time * 60 * 1000;
                                OrderModel
                                    .count({
                                        created_at: {$gte: item + missedTime, $lte: item + 60 * 60 * 1000 + missedTime },
                                        status: OrderStatuses.New,
                                        "notified_couriers.0": { "$exists": true }
                                    })
                                    .then(c => {
                                        it.missed = c;
                                        cb();
                                    })
                                    .catch(err => {
                                        cb(err);
                                    });
                            })
                            .catch(err => {
                                cb(err);
                            });
                    }
                ], err => {
                    if (err) {
                        cb(err);
                    } else {
                        it.date = moment(item / 1000, "X").format("DD-MM-YYYY");
                        it.hour = moment(item / 1000, "X").format("HH");
                        resultData.push(it);
                        cb();
                    }
                });
            }, err => {
                if (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                } else {
                    let result: any = json2csv({ data: resultData, fields: fields, fieldNames: fieldNames, del: ";" });
                    console.log(result);
                    res.setHeader("Content-disposition", "attachment; filename=data.csv");
                    res.header("Content-Type", "text/csv");
                    res.send(result);
                }
            });
        }

    }
}

export = Route;
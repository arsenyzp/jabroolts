"use strict";

import * as express from "express";
import * as escapeStringRegexp from "escape-string-regexp";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {AjaxResponse} from "../../components/AjaxResponse";
import {IManufactureModel, ManufactureModel} from "../../models/ManufactureModel";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, IsArray} from "class-validator";
import {CarTypeModel, ICarTypeModelModel} from "../../models/CarTypeModel";

module Route {

    class ManufacturePost {

        _id: string = "";

        @Length(3, 20, {
            message: "admin.validation.ManufacturerName"
        })
        name: string;

        @IsArray({
            message: "admin.validation.InvalidArrayTypes"
        })
        types: Array<any>;

    }

    export class Manufacturers {

        public async index(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            // sort
            let sort: any = {};
            if (req.query.sortKey) {
                sort[req.query.sortKey] = req.query.reverse;
            } else {
                sort = {name: 1};
            }

            // search
            let filter: any = {};
            let search: string = "";
            if (req.query.q !== undefined && req.query.q !== "") {
                search = escapeStringRegexp(req.query.q);
                filter = {$or: [
                    {name: new RegExp(".*" + search + ".*", "i")}
                ]};
                sort = {name: 1};
            }

            // paging
            let skip: number = 0;
            let limit: number = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }

            try {
                let total_count: number = await ManufactureModel.count(filter);
                let items: Array<IManufactureModel> = await ManufactureModel
                    .find(filter)
                    .sort(sort)
                    .limit(limit)
                    .skip(skip);

                ajaxResponse.setDate({
                    items: items,
                    total_count: total_count
                });
                res.json(ajaxResponse.get());
            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }
        }

        public async get(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            try {
                let model: IManufactureModel = await ManufactureModel
                    .findOne({_id: req.params.id})
                    .populate({path: "types", model: CarTypeModel, sort: "name ASC"});
                if (model == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                ajaxResponse.setDate(model);
                res.json(ajaxResponse.get());
            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }
        }

        public async getTypes(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            try {
                let manufacturer: IManufactureModel = await ManufactureModel.findOne({_id: req.params.id});
                let types: Array<ICarTypeModelModel> = [];
                if (manufacturer !== null) {
                    types = await CarTypeModel
                        .find({_id: manufacturer.types}).sort({name: 1});
                }
                ajaxResponse.setDate({
                    types: types
                });
                res.json(ajaxResponse.get());
            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }
        }

        public async delete(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            try {
                let model: IManufactureModel = await ManufactureModel.findOne({_id: req.params.id});
                if (model == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                await CarTypeModel.remove({_id: model.types});
                await ManufactureModel
                    .remove({_id: req.params.id});
                res.json(ajaxResponse.get());
            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }
        }

        public async save(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: ManufacturePost = Object.create(ManufacturePost.prototype);
            Object.assign(post, req.body, {});
            try {
                let errors: any = await validate(post);
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                } else {
                    let item: IManufactureModel = await ManufactureModel
                        .findOne({_id: post._id});
                    if (item == null) {
                        item = new ManufactureModel();
                    }
                    item.name = post.name;
                    item.types = [];

                    let tt: Array<string> = [];
                    let newTypes: Array<ICarTypeModelModel> = [];

                    for (let i: number = 0; i < post.types.length; i++) {
                        let v: ICarTypeModelModel = post.types[i];
                        if (!v._id) {
                            let ct: ICarTypeModelModel = new CarTypeModel();
                            ct.size = v.size;
                            ct.name = v.name;
                            await ct.save();
                            tt.push(ct._id);
                            newTypes.push(ct);
                        } else {
                            let ct: ICarTypeModelModel = await CarTypeModel.findOneAndUpdate({_id: v._id}, {
                                size: v.size,
                                name: v.name
                            }, {new: true});
                            tt.push(ct._id);
                            newTypes.push(ct);
                        }
                    }

                    let types_to_remove: Array<any> = item.types.filter(x => tt.indexOf(x._id.toString()) === -1);

                    await CarTypeModel.remove({_id: types_to_remove});

                    item.types = newTypes;

                    await item.save();
                    item = await ManufactureModel
                        .findOne({_id: item._id})
                        .populate({path: "types", model: CarTypeModel});

                    ajaxResponse.setDate(item);
                    res.json(ajaxResponse.get());
                }
            } catch (err) {
                console.log(err);
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }
        }


    }
}

export = Route;
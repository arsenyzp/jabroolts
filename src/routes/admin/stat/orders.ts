"use strict";

import * as express from "express";
import * as async from "async";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {IUserModel, UserModel, UserStatus} from "../../../models/UserModel";
import {OrderModel, OrderStatuses} from "../../../models/OrderModel";

module Route {

    export class Orders {

        public async active(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            // search
            let filter: any = {status: {$nin: [OrderStatuses.Missed, OrderStatuses.Canceled, OrderStatuses.Finished]}};


            // paging
            let skip: number = 0;
            let limit: number = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }

            try {
                let total_count: number = await OrderModel
                    .count(filter);
                let items: Array<any> = await  OrderModel
                    .find(filter)
                    .populate({path: "courier", model: UserModel})
                    .populate({path: "owner", model: UserModel})
                    .sort({created_at: -1})
                    .limit(limit)
                    .skip(skip);
                let data: Array<any> = [];
                async.eachOfSeries(items, (item, key, cb) => {
                    let model: any = {};
                    model._id = item._id;
                    model.order = item._id;
                    model.courier = !!item.courier ? item.courier.jabroolid : "--";
                    model.owner = !!item.owner ? item.owner.jabroolid : "--";
                    model.pickup_location = item.owner_address;
                    model.drop_off_location = item.recipient_address;
                    model.route = item.route;
                    model.number_of_parcel = item.small_package_count + item.medium_package_count + item.large_package_count;
                    model.accept_time = item.accept_at;
                    model.amount = item.cost;
                    model.pay_type = item.pay_type;
                    model.status = item.status;
                    model.created_at = item.created_at;
                    model.orderId = item.orderId;
                    data.push(model);
                    cb();
                }, err => {
                    if (err) {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    } else {
                        ajaxResponse.setDate({
                            items: data,
                            total_count: total_count
                        });
                        res.json(ajaxResponse.get());
                    }
                });
            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }
        }

        public async missed(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            // search
            let filter: any = {status: {$in: [OrderStatuses.Missed]}};


            // paging
            let skip: number = 0;
            let limit: number = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }

            try {
                let total_count: number = await OrderModel
                    .count(filter);
                let items: Array<any> = await  OrderModel
                    .find(filter)
                    .populate({path: "courier", model: UserModel})
                    .populate({path: "owner", model: UserModel})
                    .sort({created_at: -1})
                    .limit(limit)
                    .skip(skip);
                let data: Array<any> = [];
                async.eachOfSeries(items, (item, key, cb) => {
                    let model: any = {};
                    model._id = item._id;
                    model.order = item._id;
                    model.courier = !!item.courier ? item.courier.jabroolid : "--";
                    model.owner = !!item.owner ? item.owner.jabroolid : "--";
                    model.pickup_location = item.owner_address;
                    model.drop_off_location = item.recipient_address;
                    model.route = item.route;
                    model.number_of_parcel = item.small_package_count + item.medium_package_count + item.large_package_count;
                    model.accept_time = item.accept_at;
                    model.amount = item.cost;
                    model.pay_type = item.pay_type;
                    model.status = item.status;
                    model.created_at = item.created_at;
                    model.orderId = item.orderId;
                    data.push(model);
                    cb();
                }, err => {
                    if (err) {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    } else {
                        ajaxResponse.setDate({
                            items: data,
                            total_count: total_count
                        });
                        res.json(ajaxResponse.get());
                    }
                });
            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }
        }

    }
}

export = Route;
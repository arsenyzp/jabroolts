"use strict";

import * as express from "express";
import * as escapeStringRegexp from "escape-string-regexp";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {IUserModel, UserModel, UserStatus} from "../../../models/UserModel";
import {Log} from "../../../components/Log";
const log: any = Log.getInstanse()(module);

module Route {

    export class Users {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            // sort
            let sort: any = {};
            if (req.query.sortKey) {
                sort[req.query.sortKey] = req.query.reverse;
            } else {
                sort = {created_at: -1};
            }

            // search
            let filter: any = {
                $and: [
                    { $or: [
                            {web_socket_ids: { $exists: true, $not: {$size: 0} } },
                            {socket_ids: { $exists: true, $not: {$size: 0} } }
                        ]}
                ]
            };
            let search: string = "";
            if (req.query.q !== undefined && req.query.q !== "") {
                search = escapeStringRegexp(req.query.q);
                filter.$and.push({$or: [
                        {first_name: new RegExp(".*" + search + ".*", "i")},
                        {last_name: new RegExp(".*" + search + ".*", "i")},
                        {phone: new RegExp(".*" + search + ".*", "i")},
                        //{email: new RegExp(".*" + search + ".*", "i")}
                    ]});
                sort = {created_at: -1};
            }
            filter.deleted = {$in: [false, null]};

            // paging
            let skip: number = 0;
            let limit: number = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }

            UserModel
                .count(filter)
                .then(total_count => {
                    UserModel
                        .find(filter)
                        .sort(sort)
                        .limit(limit)
                        .skip(skip)
                        .then(users => {
                            let items: Array<any> = [];
                            for (let i: number = 0; i < users.length; i++) {
                                let item: any = users[i].getApiFields();
                                item.bank = users[i].getApiFields();
                                item.vehicle = users[i].getVehicle();
                                item.bank = users[i].getBank();
                                item.license = users[i].getLicense();
                                item.socket_ids = users[i].socket_ids;
                                item._id = users[i]._id;
                                items.push(item);
                            }
                            ajaxResponse.setDate({
                                items: items,
                                total_count: total_count
                            });
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public get(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            UserModel
                .findOne({_id: req.params.id})
                .then(u => {
                    if (u == null) {
                        ajaxResponse.addErrorMessage("Item not found");
                        res.status(500);
                        return res.json(ajaxResponse.get());
                    }
                    ajaxResponse.setDate(u);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

    }
}

export = Route;
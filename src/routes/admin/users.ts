"use strict";

import * as express from "express";
import * as escapeStringRegexp from "escape-string-regexp";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {AjaxResponse} from "../../components/AjaxResponse";
import {IUserModel, UserModel, UserStatus, UserTypes} from "../../models/UserModel";
import {validate, Contains, IsInt, Length, IsEmail, IsNumberString, IsDate, Min, Max, IsBoolean, IsNumber, IsDateString} from "class-validator";
import {FileHelper} from "../../components/FileHelper";
import {AppConfig} from "../../components/config";
import {Log} from "../../components/Log";
import {BankModel, IBankModel} from "../../models/BankModel";
import {CountryModel, ICountryModel} from "../../models/CountryModel";
import {BankTypeModel, IBankTypeModel} from "../../models/BankTypeModel";
import {CarTypeModel, ICarTypeModelModel} from "../../models/CarTypeModel";
import {IManufactureModel, ManufactureModel} from "../../models/ManufactureModel";
import {PaymentModule} from "../../components/jabrool/PaymentModule";
import {UsersPost} from "./posts/UsersPost";
import {BankPost} from "./posts/BankPost";
import {LicensePost} from "./posts/LicensePost";
import {VehiclePost} from "./posts/VehiclePost";
import {CourierBalanceLogModel, CourierBalanceTypes, ICourierBalanceLogModel} from "../../models/CourierBalanceLogModel";
import {BonusesConfig} from "../../models/configs/BonusesConfig";
import {SocketServer} from "../../components/SocketServer";
import {NotificationModule, NotificationTypes} from "../../components/NotificationModule";

const async: any = require("async");
const log: any = Log.getInstanse()(module);

module Route {

    export class Users {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            // sort
            let sort: any = {};
            if (req.query.sortKey) {
                sort[req.query.sortKey] = req.query.reverse;
            } else {
                sort = {created_at: -1};
            }

            // search
            let filter: any = {};
            let search: string = "";
            if (req.query.q !== undefined && req.query.q !== "") {
                search = escapeStringRegexp(req.query.q);
                filter = {$or: [
                    {first_name: new RegExp(".*" + search + ".*", "i")},
                    {last_name: new RegExp(".*" + search + ".*", "i")},
                    {phone: new RegExp(".*" + search + ".*", "i")},
                    //{email: new RegExp(".*" + search + ".*", "i")}
                ]};
                sort = {created_at: -1};
            }
            filter.deleted = {$in: [false, null]};

            // paging
            let skip: number = 0;
            let limit: number = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }

            UserModel
                .count(filter)
                .then(total_count => {
                    UserModel
                        .find(filter)
                        .sort(sort)
                        .limit(limit)
                        .skip(skip)
                        .then(users => {
                            let items: Array<any> = [];
                            for (let i: number = 0; i < users.length; i++) {
                                let item: any = users[i].getApiFields();
                                item.bank = users[i].getApiFields();
                                item.vehicle = users[i].getVehicle();
                                item.bank = users[i].getBank();
                                item.license = users[i].getLicense();
                                item.socket_ids = users[i].socket_ids;
                                item._id = users[i]._id;
                                items.push(item);
                            }
                            ajaxResponse.setDate({
                                items: items,
                                total_count: total_count
                            });
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public business(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            // sort
            let sort: any = {};
            if (req.query.sortKey) {
                sort[req.query.sortKey] = req.query.reverse;
            } else {
                sort = {created_at: -1};
            }

            // search
            let filter: any = {type: UserTypes.Business};
            let search: string = "";
            if (req.query.q !== undefined && req.query.q !== "") {
                search = escapeStringRegexp(req.query.q);
                filter = {$or: [
                    {first_name: new RegExp(".*" + search + ".*", "i")},
                    {last_name: new RegExp(".*" + search + ".*", "i")},
                    {phone: new RegExp(".*" + search + ".*", "i")},
                    //{email: new RegExp(".*" + search + ".*", "i")}
                ],
                    type: UserTypes.Business
                };
                sort = {created_at: -1};
            }
            filter.deleted = {$in: [false, null]};

            // paging
            let skip: number = 0;
            let limit: number = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }

            UserModel
                .count(filter)
                .then(total_count => {
                    UserModel
                        .find(filter)
                        .sort(sort)
                        .limit(limit)
                        .skip(skip)
                        .then(users => {
                            let items: Array<any> = [];
                            for (let i: number = 0; i < users.length; i++) {
                                let item: any = users[i].getApiFields();
                                item.bank = users[i].getApiFields();
                                item.vehicle = users[i].getVehicle();
                                item.bank = users[i].getBank();
                                item.license = users[i].getLicense();
                                item.socket_ids = users[i].socket_ids;
                                item._id = users[i]._id;
                                items.push(item);
                            }
                            ajaxResponse.setDate({
                                items: items,
                                total_count: total_count
                            });
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public get(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            UserModel
                .findOne({_id: req.params.id})
                .then(u => {
                    if (u == null) {
                        ajaxResponse.addErrorMessage("Item not found");
                        res.status(500);
                        return res.json(ajaxResponse.get());
                    }
                    ajaxResponse.setDate(u);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public resetJabroolFee(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let bonusesModel: BonusesConfig = new BonusesConfig();
            bonusesModel
                .getInstanse()
                .then(conf => {
                    UserModel
                        .findOneAndUpdate(
                            {_id: req.params.id},
                            {
                                courierLimit: conf.defaultCourierLimit,
                                jabroolFee: 0
                            },
                            {new: true}
                        )
                        .then(u => {
                            if (u == null) {
                                ajaxResponse.addErrorMessage("Item not found");
                                res.status(500);
                                return res.json(ajaxResponse.get());
                            }
                            ajaxResponse.setDate(u);
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });

        }

        public getBank(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let user: IUserModel;
            let banks: Array<IBankModel>;
            let countries: Array<ICountryModel>;
            let types: Array<IBankTypeModel>;
            async.parallel(
                [
                    cb => {
                        UserModel
                            .findOne({_id: req.params.id})
                            .then(u => {
                                if (u == null) {
                                    cb("Item not found");
                                }
                                user = u;
                                cb();
                            })
                            .catch(err => {
                                cb(err);
                            });
                    },
                    cb => {
                        BankModel
                            .find({})
                            .then(u => {
                                banks = u;
                                cb();
                            })
                            .catch(err => {
                                cb(err);
                            });
                    },
                    cb => {
                        BankTypeModel
                            .find({})
                            .then(u => {
                                types = u;
                                cb();
                            })
                            .catch(err => {
                                cb(err);
                            });
                    },
                    cb => {
                        CountryModel
                            .find({})
                            .then(u => {
                                countries = u;
                                cb();
                            })
                            .catch(err => {
                                cb(err);
                            });
                    },
                ],
                err => {
                    if (err) {
                        log.error(err);
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    } else {
                        ajaxResponse.setDate({
                            user: user,
                            banks: banks,
                            types: types,
                            countries: countries
                        });
                        res.json(ajaxResponse.get());
                    }
                }
            );
        }

        public saveBank(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: BankPost = Object.create(BankPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                } else {
                    UserModel
                        .findOne({_id: req.params.id})
                        .then(item => {
                            if (item == null) {
                                item = new UserModel();
                            }

                            item.bank.bank = post.bank;
                            item.bank.account_number = post.account_number;
                            item.bank.branch = post.branch;
                            item.bank.country_code = post.country_code;
                            item.bank.type = post.type;
                            item.bank.holder_name = post.holder_name;

                            Users._save(item, res, ajaxResponse);
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                }
            });

        }

        public getLicense(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let user: IUserModel;
            let countries: Array<ICountryModel>;
            async.parallel(
                [
                    cb => {
                        UserModel
                            .findOne({_id: req.params.id})
                            .then(u => {
                                if (u == null) {
                                    cb("Item not found");
                                }
                                user = u;
                                cb();
                            })
                            .catch(err => {
                                cb(err);
                            });
                    },
                    cb => {
                        CountryModel
                            .find({})
                            .then(u => {
                                countries = u;
                                cb();
                            })
                            .catch(err => {
                                cb(err);
                            });
                    },
                ],
                err => {
                    if (err) {
                        log.error(err);
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    } else {
                        ajaxResponse.setDate({
                            user: user,
                            countries: countries
                        });
                        res.json(ajaxResponse.get());
                    }
                }
            );
        }

        public saveLicense(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: LicensePost = Object.create(LicensePost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                } else {
                    UserModel
                        .findOne({_id: req.params.id})
                        .then(item => {
                            if (item == null) {
                                item = new UserModel();
                            }

                            item.license.name = post.name;
                            item.license.number = post.number;
                            item.license.issue_date = new Date(post.issue_date).getTime();
                            item.license.expiry_date = new Date(post.expiry_date).getTime();
                            item.license.country_code = post.country_code;
                            if (item.license.image !== post.image) {
                                if (post.image !== "") {
                                    FileHelper.move(
                                        AppConfig.getInstanse().get("files:user_uploads")  + "/" + post.image,
                                        AppConfig.getInstanse().get("files:user_license")  + "/" + post.image,
                                        err => {
                                            if (err) {
                                                ajaxResponse.addError(err);
                                                res.status(500);
                                                res.json(ajaxResponse.get());
                                            } else {
                                                item.license.image = post.image;
                                                Users._save(item, res, ajaxResponse);
                                            }
                                        });
                                } else {
                                    item.license.image = "";
                                    Users._save(item, res, ajaxResponse);
                                }
                            } else {
                                Users._save(item, res, ajaxResponse);
                            }
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                }
            });

        }

        public async getVehicle(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            try {
                let user: IUserModel = await UserModel
                    .findOne({_id: req.params.id});
                let manufacturer: IManufactureModel = await ManufactureModel.findOne({_id: user.vehicle.model});
                let types: Array<ICarTypeModelModel> = [];
                if (manufacturer !== null) {
                    types = await CarTypeModel
                        .find({_id: manufacturer.types});
                }
                let manufacturers: Array<IManufactureModel> = await ManufactureModel.find({});

                let years: Array<number> = [];
                for (let i: number = 2000; i <= new Date().getUTCFullYear(); i++) {
                    years.push(i);
                }
                ajaxResponse.setDate({
                    user: user,
                    types: types,
                    models: manufacturers,
                    years: years
                });
                res.json(ajaxResponse.get());
            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }
        }

        public saveVehicle(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: VehiclePost = Object.create(VehiclePost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                } else {
                    UserModel
                        .findOne({_id: req.params.id})
                        .then(item => {
                            if (item == null) {
                                item = new UserModel();
                            }

                            item.vehicle.number = post.number;
                            item.vehicle.type = post.type;
                            item.vehicle.model = post.model;
                            item.vehicle.year = post.year;

                            async.parallel(
                               [
                                   cb => {
                                       if (item.vehicle.image_front !== post.image_front) {
                                           if (post.image_front !== "") {
                                               FileHelper.move(
                                                   AppConfig.getInstanse().get("files:user_uploads")  + "/" + post.image_front,
                                                   AppConfig.getInstanse().get("files:user_vehicle")  + "/" + post.image_front,
                                                   err => {
                                                       if (err) {
                                                           ajaxResponse.addError(err);
                                                           res.status(500);
                                                           res.json(ajaxResponse.get());
                                                       } else {
                                                           item.vehicle.image_front = post.image_front;
                                                          cb();
                                                       }
                                                   });
                                           } else {
                                               item.vehicle.image_front = "";
                                               cb();
                                           }
                                       } else {
                                           cb();
                                       }
                               },
                                   cb => {
                                       if (item.vehicle.image_side !== post.image_side) {
                                           if (post.image_side !== "") {
                                               FileHelper.move(
                                                   AppConfig.getInstanse().get("files:user_uploads")  + "/" + post.image_side,
                                                   AppConfig.getInstanse().get("files:user_vehicle")  + "/" + post.image_side,
                                                   err => {
                                                       if (err) {
                                                           ajaxResponse.addError(err);
                                                           res.status(500);
                                                           res.json(ajaxResponse.get());
                                                       } else {
                                                           item.vehicle.image_side = post.image_side;
                                                           cb();
                                                       }
                                                   });
                                           } else {
                                               item.vehicle.image_side = "";
                                               cb();
                                           }
                                       } else {
                                           cb();
                                       }
                                   },
                                   cb => {
                                       if (item.vehicle.image_back !== post.image_back) {
                                           if (post.image_back !== "") {
                                               FileHelper.move(
                                                   AppConfig.getInstanse().get("files:user_uploads")  + "/" + post.image_back,
                                                   AppConfig.getInstanse().get("files:user_vehicle")  + "/" + post.image_back,
                                                   err => {
                                                       if (err) {
                                                           ajaxResponse.addError(err);
                                                           res.status(500);
                                                           res.json(ajaxResponse.get());
                                                       } else {
                                                           item.vehicle.image_back = post.image_back;
                                                           cb();
                                                       }
                                                   });
                                           } else {
                                               item.vehicle.image_back = "";
                                               cb();
                                           }
                                       } else {
                                           cb();
                                       }
                                   },
                                   cb => {
                                        let images: Array<string> = [];
                                        async.forEachOfSeries(post.images_insurance, (v, k, cb) => {
                                            if (item.vehicle.images_insurance.indexOf(v) < 0) {
                                                FileHelper.move(
                                                    AppConfig.getInstanse().get("files:user_uploads")  + "/" + v,
                                                    AppConfig.getInstanse().get("files:user_vehicle")  + "/" + v,
                                                    err => {
                                                        if (err) {
                                                            cb(err);
                                                        } else {
                                                            images.push(v);
                                                            cb();
                                                        }
                                                    });
                                            } else {
                                                images.push(v);
                                                cb();
                                            }
                                        }, err => {
                                            if (err) {
                                                cb(err);
                                            } else {
                                                item.vehicle.images_insurance = images;
                                                cb();
                                            }
                                        });
                                   },
                                   cb => {
                                       let images: Array<string> = [];
                                       for (let i: number = 0; i < item.vehicle.images_insurance.length; i++) {
                                           if (post.images_insurance_delete.indexOf(item.vehicle.images_insurance[i]) < 0) {
                                               images.push(item.vehicle.images_insurance[i]);
                                           }
                                       }
                                       item.vehicle.images_insurance = images;
                                       cb();
                                   },
                               ],
                                err => {
                                    if (err) {
                                        ajaxResponse.addError(err);
                                        res.status(500);
                                        res.json(ajaxResponse.get());
                                    } else {
                                        Users._save(item, res, ajaxResponse);
                                    }
                                }
                            );
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                }
            });

        }

        public delete(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            UserModel
                .findOne({_id: req.params.id})
                .then(u => {
                    if (u == null) {
                        ajaxResponse.addErrorMessage("Item not found");
                        res.status(500);
                        return res.json(ajaxResponse.get());
                    }

                    // Send logout event to socket
                    if (u.socket_ids && u.socket_ids.length > 0) {
                        for (let i: number = 0; i < u.socket_ids.length; i++) {
                            if (SocketServer.getInstance().sockets.sockets[u.socket_ids[i]]) {
                                SocketServer.getInstance().sockets.sockets[u.socket_ids[i]].emit("logout", {});
                            }
                        }
                    }

                    UserModel
                        .findOneAndUpdate(
                            {_id: req.params.id},
                            {
                                phone: u._id + "+" + u.phone,
                                email: u._id + "+" + u.email,
                                deleted: true
                            })
                        .then(user => {
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public save(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: UsersPost = Object.create(UsersPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                } else {
                    UserModel.findOne({
                        $or: [
                            {email: post.email},
                            {phone: post.phone}
                        ],
                        _id: {$ne: post._id}
                    })
                        .then(u => {
                            if ( u !== null) {
                                if (u.phone === post.phone) {
                                    ajaxResponse.addErrorMessage("User phone already used");
                                } else {
                                    ajaxResponse.addErrorMessage("User email already used");
                                }
                                res.status(500);
                                res.json(ajaxResponse.get());
                            } else {

                                let bonusesModel: BonusesConfig = new BonusesConfig();
                                bonusesModel
                                    .getInstanse()
                                    .then(conf => {
                                        UserModel
                                            .findOne({_id: post._id})
                                            .then(item => {
                                                if (item == null) {
                                                    item = new UserModel();
                                                    item.location.coordinates = [0, 0];
                                                    item.location.type = "Point";
                                                    item.courierLimit = conf.defaultCourierLimit;
                                                    item.genToken();
                                                }
                                                if (post.new_password !== "") {
                                                    item.setPassword(post.new_password);
                                                    item.genToken();
                                                }
                                                item.first_name = post.first_name;
                                                item.last_name = post.last_name;
                                                item.email = post.email;
                                                item.phone = post.phone;
                                                item.role = post.role;
                                                item.type = post.type;
                                                if ( item.status !== post.status && post.status === UserStatus.Active) {
                                                    /**
                                                     * Send notification to courier
                                                     */
                                                    NotificationModule.getInstance().send(
                                                        [item._id],
                                                        NotificationTypes.Activated,
                                                        {},
                                                        item._id
                                                    );
                                                }
                                                item.status = post.status;
                                                item.visible = post.visible;
                                                item.in_progress = post.in_progress;

                                                //Correct courier balance
                                                if (post.new_courierLimit !== 0) {
                                                    let model: ICourierBalanceLogModel = new CourierBalanceLogModel();
                                                    model.user = item;
                                                    model.amount = post.new_courierLimit;
                                                    model.type = CourierBalanceTypes.Admin;
                                                    model.save();
                                                    item.courierLimit += post.new_courierLimit;
                                                }

                                                //Correct balance
                                                if (post.new_balance !== 0) {
                                                    PaymentModule
                                                        .correctBalanceFromAdmin(item._id, post.new_balance)
                                                        .then(r => {
                                                            Users._preSave(item, post, ajaxResponse, res);
                                                        })
                                                        .catch(err => {
                                                            ajaxResponse.addError(err);
                                                            res.status(500);
                                                            res.json(ajaxResponse.get());
                                                        });
                                                } else {
                                                    Users._preSave(item, post, ajaxResponse, res);
                                                }
                                            })
                                            .catch(err => {
                                                ajaxResponse.addError(err);
                                                res.status(500);
                                                res.json(ajaxResponse.get());
                                            });
                                    })
                                    .catch(err => {
                                        ajaxResponse.addError(err);
                                        res.status(500);
                                        res.json(ajaxResponse.get());
                                    });
                            }
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                }
            });
        }

        private static _preSave(item: IUserModel, post: UsersPost, ajaxResponse: AjaxResponse, res: express.Response): void {
            if (item.avatar !== post.avatar) {
                if (post.avatar !== "") {
                    FileHelper.move(
                        AppConfig.getInstanse().get("files:user_uploads")  + "/" + post.avatar,
                        AppConfig.getInstanse().get("files:user_avatars")  + "/" + post.avatar,
                        err => {
                            if (err) {
                                ajaxResponse.addError(err);
                                res.status(500);
                                res.json(ajaxResponse.get());
                            } else {
                                item.avatar = post.avatar;
                                Users._save(item, res, ajaxResponse);
                            }
                        });
                } else {
                    item.avatar = "";
                    Users._save(item, res, ajaxResponse);
                }
            } else {
                Users._save(item, res, ajaxResponse);
            }
        }

        public static _save(item: IUserModel, res: express.Response, ajaxResponse: AjaxResponse): void {
            if (!item.jabroolid) {
                item
                    .generateJId()
                    .then(() => {
                        item
                            .save()
                            .then(item => {
                                ajaxResponse.setDate(item);
                                res.json(ajaxResponse.get());
                            })
                            .catch(err => {
                                ajaxResponse.addError(err);
                                res.status(500);
                                res.json(ajaxResponse.get());
                            });
                    })
                    .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
            } else {
                item
                    .save()
                    .then(item => {
                        ajaxResponse.setDate(item);
                        res.json(ajaxResponse.get());
                    })
                    .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
            }
        }

    }
}

export = Route;

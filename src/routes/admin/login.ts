"use strict";

import * as express from "express";
import {AjaxResponse} from "../../components/AjaxResponse";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {UserModel, UserRoles} from "../../models/UserModel";

module Route {

    class LoginPost {
        @IsEmail({}, {
            message: "admin.validation.LoginInvalidEmail"
        })
        email: string;

        @Length(6, 20, {
            message: "admin.validation.LoginInvalidPassword"
        })
        password: string;
    }

    export class Index {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            //render page
            res.render("admin/login");
        }

        public login(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: LoginPost = Object.create(LoginPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                } else {
                    UserModel.findOne({email: post.email})
                        .then(user => {
                            if (user == null) {
                                ajaxResponse.addErrorMessage("User not found");
                                res.status(500);
                                return res.json(ajaxResponse.get());
                            }
                            if (!user.comparePassword(post.password)) {
                                ajaxResponse.addErrorMessage("Incorrect password");
                                res.status(500);
                                return res.json(ajaxResponse.get());
                            }
                            if (user.role !== UserRoles.dev && user.role !== UserRoles.admin) {
                                ajaxResponse.addErrorMessage("Incorrect role user");
                                res.status(500);
                                return res.json(ajaxResponse.get());
                            }
                            req.session.user = user;
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                }
            });
        }
    }
}

export = Route;

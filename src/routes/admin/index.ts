"use strict";

import * as express from "express";
import * as loginRoute from "./login";
import * as logoutRoute from "./logout";
import * as mainRoute from "./main";
import * as banksRoute from "./banks";
import * as countriesRoute from "./countries";
import * as manufacturersRoute from "./manufacturers";
import * as carTypesRoute from "./carTypes";
import * as bankTypesRoute from "./bankTypes";
import * as promocodesRoute from "./promocodes";
import * as subscriptionsRoute from "./subscriptions";
import * as usersRoute from "./users";
import * as profileRoute from "./profile";

import * as configPackageRoute from "./config/package";
import * as radiusRoute from "./config/radius";
import * as packageSizeRoute from "./config/package_size";
import * as deliveryRoute from "./config/delivery";
import * as termsRoute from "./config/terms";
import * as logsConfigRoute from "./config/logs";
import * as cancelRequestRoute from "./config/cancel_request_params";
import * as priceRoute from "./config/price";
import * as bonusesRoute from "./config/bonuses";

import * as apiLogsRoute from "./logs/api";
import * as smsLogsRoute from "./logs/sms";
import * as emailLogsRoute from "./logs/email";

import * as paymentsRoute from "./payments";
import * as newUsersRoute from "./newUsers";

import * as contactUsRoute from "./contactUs";
import * as ordersRoute from "./orders";

import * as reportsCouriersRoute from "./reports/couriers";
import * as hoursReportsRoute from "./reports/hours_reports";
import * as ordersReportsRoute from "./reports/orders";
import * as customersReportsRoute from "./reports/customers";
import * as couriorClustersReportsRoute from "./reports/сourior_clusters";
import * as customerClustersReportsRoute from "./reports/customer_clusters";

import * as serviceMessagesRoute from "./serviceMessages";

import * as paymentsNewCourierRoute from "./payments_new_courier";
import * as paymentsNewCustomerRoute from "./payments_new_customer";
import * as statisticsRoute from "./ajax/statistics";
import * as statUsersRoute from "./stat/users";
import * as statOrdersRoute from "./stat/orders";

import * as businessRoute from "./business";

import * as uploadsImgRoute from "./ajax/upload_img";
import {AdminAuth} from "../../components/AdminAuth";

module Route {

    export class Index {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let login: loginRoute.Index = new loginRoute.Index();
            router.get("/login", login.index.bind(login.index));
            router.post("/login", login.login.bind(login.login));

            let logout: logoutRoute.Index = new logoutRoute.Index();
            router.get("/logout", logout.index.bind(logout.index));
            router.post("/logout", logout.index.bind(logout.index));


            let main: mainRoute.Index = new mainRoute.Index();
            router.get("/", AdminAuth.check, main.index.bind(main.index));

            let uploads_img: uploadsImgRoute.UploadsImg = new uploadsImgRoute.UploadsImg();
            router.use("/ajax/uploads_img", AdminAuth.check, uploads_img.upload());

            let banks: banksRoute.Banks = new banksRoute.Banks();
            router.get("/banks/:perPage/:page", AdminAuth.check, banks.index.bind(banks.index));
            router.get("/bank/:id", AdminAuth.check, banks.get.bind(banks.get));
            router.get("/bank/delete/:id", AdminAuth.check, banks.delete.bind(banks.delete));
            router.post("/bank/", AdminAuth.check, banks.save.bind(banks.save));

            let business: businessRoute.Business = new businessRoute.Business();
            router.get("/businessCouriers/:id/:perPage/:page", AdminAuth.check, business.index.bind(business.index));
            router.get("/businessCouriersAdd/:id/:courier", AdminAuth.check, business.save.bind(business.save));
            router.get("/businessCouriersDelete/:id/:courier", AdminAuth.check, business.delete.bind(business.delete));

            let countries: countriesRoute.Countries = new countriesRoute.Countries();
            router.get("/countries/:perPage/:page", AdminAuth.check, countries.index.bind(countries.index));
            router.get("/country/:id", AdminAuth.check, countries.get.bind(countries.get));
            router.get("/country/delete/:id", AdminAuth.check, countries.delete.bind(countries.delete));
            router.post("/country/", AdminAuth.check, countries.save.bind(countries.save));

            let manufacturers: manufacturersRoute.Manufacturers = new manufacturersRoute.Manufacturers();
            router.get("/manufacturers/:perPage/:page", AdminAuth.check, manufacturers.index.bind(manufacturers.index));
            router.get("/manufacture/:id", AdminAuth.check, manufacturers.get.bind(manufacturers.get));
            router.get("/manufacture/getTypes/:id", AdminAuth.check, manufacturers.getTypes.bind(manufacturers.getTypes));
            router.get("/manufacture/delete/:id", AdminAuth.check, manufacturers.delete.bind(manufacturers.delete));
            router.post("/manufacture/", AdminAuth.check, manufacturers.save.bind(manufacturers.save));

            let carTypes: carTypesRoute.CarTypes = new carTypesRoute.CarTypes();
            router.get("/car_types/:perPage/:page", AdminAuth.check, carTypes.index.bind(carTypes.index));
            router.get("/car_type/:id", AdminAuth.check, carTypes.get.bind(carTypes.get));
            router.get("/car_type/delete/:id", AdminAuth.check, carTypes.delete.bind(carTypes.delete));
            router.post("/car_type/", AdminAuth.check, carTypes.save.bind(carTypes.save));

            let bankTypes: bankTypesRoute.BankTypes = new bankTypesRoute.BankTypes();
            router.get("/bank_types/:perPage/:page", AdminAuth.check, bankTypes.index.bind(bankTypes.index));
            router.get("/bank_type/:id", AdminAuth.check, bankTypes.get.bind(bankTypes.get));
            router.get("/bank_type/delete/:id", AdminAuth.check, bankTypes.delete.bind(bankTypes.delete));
            router.post("/bank_type/", AdminAuth.check, bankTypes.save.bind(bankTypes.save));

            let promocodes: promocodesRoute.Promocodes = new promocodesRoute.Promocodes();
            router.get("/promocodes/:perPage/:page", AdminAuth.check, promocodes.index.bind(promocodes.index));
            router.get("/promocode/:id", AdminAuth.check, promocodes.get.bind(promocodes.get));
            router.get("/promocode/delete/:id", AdminAuth.check, promocodes.delete.bind(promocodes.delete));
            router.post("/promocode/", AdminAuth.check, promocodes.save.bind(promocodes.save));


            let subscriptions: subscriptionsRoute.Subscriptions = new subscriptionsRoute.Subscriptions();
            router.get("/subscriptions/:perPage/:page", AdminAuth.check, subscriptions.index.bind(subscriptions.index));
            router.get("/subscriptions/delete/:id", AdminAuth.check, subscriptions.delete.bind(subscriptions.delete));

            let profile: profileRoute.Profile = new profileRoute.Profile();
            router.get("/profile", AdminAuth.check, profile.index.bind(profile.index));
            router.get("/getToken", AdminAuth.check, profile.getToken.bind(profile.getToken));

            let users: usersRoute.Users = new usersRoute.Users();
            router.get("/users/:perPage/:page", AdminAuth.check, users.index.bind(users.index));
            router.get("/business/:perPage/:page", AdminAuth.check, users.business.bind(users.business));
            router.get("/user/:id", AdminAuth.check, users.get.bind(users.get));
            router.get("/user/bank/:id", AdminAuth.check, users.getBank.bind(users.getBank));
            router.post("/user/bank/:id", AdminAuth.check, users.saveBank.bind(users.saveBank));
            router.get("/user/license/:id", AdminAuth.check, users.getLicense.bind(users.getLicense));
            router.post("/user/license/:id", AdminAuth.check, users.saveLicense.bind(users.saveLicense));
            router.get("/user/vehicle/:id", AdminAuth.check, users.getVehicle.bind(users.getVehicle));
            router.post("/user/vehicle/:id", AdminAuth.check, users.saveVehicle.bind(users.saveVehicle));
            router.post("/user/reset_jabrool_fee/:id", AdminAuth.check, users.resetJabroolFee.bind(users.resetJabroolFee));
            router.get("/user/delete/:id", AdminAuth.check, users.delete.bind(users.delete));
            router.post("/user/", AdminAuth.check, users.save.bind(users.save));

            let newUsers: newUsersRoute.NewUsers = new newUsersRoute.NewUsers();
            router.get("/newUsers/:perPage/:page", AdminAuth.check, newUsers.index.bind(newUsers.index));

            // Config
            let configPackages: configPackageRoute.Packages = new configPackageRoute.Packages();
            router.get("/config/packages/", AdminAuth.check, configPackages.index.bind(configPackages.index));
            router.post("/config/packages/", AdminAuth.check, configPackages.save.bind(configPackages.save));

            let radius: radiusRoute.Radius = new radiusRoute.Radius();
            router.get("/config/radius/", AdminAuth.check, radius.index.bind(radius.index));
            router.post("/config/radius/", AdminAuth.check, radius.save.bind(radius.save));

            let packageSize: packageSizeRoute.PackageSize = new packageSizeRoute.PackageSize();
            router.get("/config/package_ratio/", AdminAuth.check, packageSize.index.bind(packageSize.index));
            router.post("/config/package_ratio/", AdminAuth.check, packageSize.save.bind(packageSize.save));

            let delivery: deliveryRoute.Delivery = new deliveryRoute.Delivery();
            router.get("/config/delivery/", AdminAuth.check, delivery.index.bind(delivery.index));
            router.post("/config/delivery/", AdminAuth.check, delivery.save.bind(delivery.save));

            let terms: termsRoute.Terms = new termsRoute.Terms();
            router.get("/config/terms/", AdminAuth.check, terms.index.bind(terms.index));
            router.post("/config/terms/", AdminAuth.check, terms.save.bind(terms.save));

            let cancelRequest: cancelRequestRoute.CancelRequestParams = new cancelRequestRoute.CancelRequestParams();
            router.get("/config/cancel_params/", AdminAuth.check, cancelRequest.index.bind(cancelRequest.index));
            router.post("/config/cancel_params/", AdminAuth.check, cancelRequest.save.bind(cancelRequest.save));

            let logsConfig: logsConfigRoute.Logs = new logsConfigRoute.Logs();
            router.get("/config/logs/", AdminAuth.check, logsConfig.index.bind(logsConfig.index));
            router.post("/config/logs/", AdminAuth.check, logsConfig.save.bind(logsConfig.save));

            let price: priceRoute.Price = new priceRoute.Price();
            router.get("/config/price/", AdminAuth.check, price.index.bind(price.index));
            router.post("/config/price/", AdminAuth.check, price.save.bind(price.save));

            let bonuses: bonusesRoute.Bonuses = new bonusesRoute.Bonuses();
            router.get("/config/bonuses/", AdminAuth.check, bonuses.index.bind(bonuses.index));
            router.post("/config/bonuses/", AdminAuth.check, bonuses.save.bind(bonuses.save));

            //Logs
            let apiLogs: apiLogsRoute.ApiLog = new apiLogsRoute.ApiLog();
            router.get("/logs/apis/:perPage/:page", AdminAuth.check, apiLogs.index.bind(apiLogs.index));
            router.get("/logs/api/:id", AdminAuth.check, apiLogs.get.bind(apiLogs.get));
            router.get("/logs/api/delete/:id", AdminAuth.check, apiLogs.delete.bind(apiLogs.delete));

            let emailLogs: emailLogsRoute.EmailLog = new emailLogsRoute.EmailLog();
            router.get("/logs/emails/:perPage/:page", AdminAuth.check, emailLogs.index.bind(emailLogs.index));
            router.get("/logs/email/:id", AdminAuth.check, emailLogs.get.bind(emailLogs.get));
            router.get("/logs/email/delete/:id", AdminAuth.check, emailLogs.delete.bind(emailLogs.delete));

            let smsLogs: smsLogsRoute.SmsLog = new smsLogsRoute.SmsLog();
            router.get("/logs/smss/:perPage/:page", AdminAuth.check, smsLogs.index.bind(smsLogs.index));
            router.get("/logs/sms/:id", AdminAuth.check, smsLogs.get.bind(smsLogs.get));
            router.get("/logs/sms/delete/:id", AdminAuth.check, smsLogs.delete.bind(smsLogs.delete));

            let payments: paymentsRoute.Payments = new paymentsRoute.Payments();
            router.get("/payments/:perPage/:page", AdminAuth.check, payments.index.bind(payments.index));
            router.get("/payments/:id", AdminAuth.check, payments.get.bind(payments.get));

            let paymentsCourier: paymentsNewCourierRoute.PaymentsNewCourier = new paymentsNewCourierRoute.PaymentsNewCourier();
            router.get("/payments_courier/:perPage/:page", AdminAuth.check, paymentsCourier.index.bind(paymentsCourier.index));
            router.get("/payments_courier/:id", AdminAuth.check, paymentsCourier.get.bind(paymentsCourier.get));

            let paymentsCustomer: paymentsNewCustomerRoute.PaymentsNewCustomer = new paymentsNewCustomerRoute.PaymentsNewCustomer();
            router.get("/payments_customer/:perPage/:page", AdminAuth.check, paymentsCustomer.index.bind(paymentsCustomer.index));
            router.get("/payments_customer/:id", AdminAuth.check, paymentsCustomer.get.bind(paymentsCustomer.get));

            let contactUs: contactUsRoute.ContactUs = new contactUsRoute.ContactUs();
            router.get("/contact_uss/:perPage/:page", AdminAuth.check, contactUs.index.bind(contactUs.index));
            router.get("/contact_us/:id", AdminAuth.check, contactUs.get.bind(contactUs.get));
            router.get("/contact_us/delete/:id", AdminAuth.check, contactUs.delete.bind(contactUs.delete));

            let reportsCouriers: reportsCouriersRoute.Couriers = new reportsCouriersRoute.Couriers();
            router.get("/reports/couriers/:perPage/:page", AdminAuth.check, reportsCouriers.index.bind(reportsCouriers.index));
            router.get("/reports/couriers/export", AdminAuth.check, reportsCouriers.export.bind(reportsCouriers.export));

            let hoursReports: hoursReportsRoute.HoursReports = new hoursReportsRoute.HoursReports();
            router.get("/reports/hours/:perPage/:page", AdminAuth.check, hoursReports.index.bind(hoursReports.index));
            router.get("/reports/hours/export", AdminAuth.check, hoursReports.export.bind(hoursReports.export));

            let ordersReports: ordersReportsRoute.Orders = new ordersReportsRoute.Orders();
            router.get("/reports/orders/:perPage/:page", AdminAuth.check, ordersReports.index.bind(ordersReports.index));
            router.get("/reports/orders/export", AdminAuth.check, ordersReports.export.bind(ordersReports.export));

            let customersReports: customersReportsRoute.Customers = new customersReportsRoute.Customers();
            router.get("/reports/customers/:perPage/:page", AdminAuth.check, customersReports.index.bind(customersReports.index));
            router.get("/reports/customers/export", AdminAuth.check, customersReports.export.bind(customersReports.export));

            let couriorClustersReports: couriorClustersReportsRoute.СouriorClusters = new couriorClustersReportsRoute.СouriorClusters();
            router.get(
                "/reports/courior_clusters/:perPage/:page",
                AdminAuth.check, couriorClustersReports.index.bind(couriorClustersReports.index));
            router.get(
                "/reports/courior_clusters/export",
                AdminAuth.check, couriorClustersReports.export.bind(couriorClustersReports.export));

            let customerClustersReports: customerClustersReportsRoute.CustomerClusters = new customerClustersReportsRoute.CustomerClusters();
            router.get(
                "/reports/cusotmer_clusters/:perPage/:page",
                AdminAuth.check, customerClustersReports.index.bind(customerClustersReports.index));
            router.get(
                "/reports/cusotmer_clusters/export",
                AdminAuth.check, customerClustersReports.export.bind(customerClustersReports.export));

            let serviceMessages: serviceMessagesRoute.ServiceMessages = new serviceMessagesRoute.ServiceMessages();
            router.get("/service_messages/:perPage/:page", AdminAuth.check, serviceMessages.index.bind(serviceMessages.index));
            router.post("/service_messages/", AdminAuth.check, serviceMessages.save.bind(serviceMessages.save));

            let userOrders: ordersRoute.UserOrders = new ordersRoute.UserOrders();
            router.get("/user_orders/:perPage/:page", AdminAuth.check, userOrders.index.bind(userOrders.index));
            router.get("/orders/:id", AdminAuth.check, userOrders.get.bind(userOrders.get));

            let statUsers: statUsersRoute.Users = new statUsersRoute.Users();
            router.get("/stat/users/:perPage/:page", AdminAuth.check, statUsers.index.bind(statUsers.index));
            router.get("/stat/users/:id", AdminAuth.check, statUsers.get.bind(statUsers.get));

            let statistics: statisticsRoute.Statistics = new statisticsRoute.Statistics();
            router.get("/statistics", AdminAuth.check, statistics.index.bind(statistics.index));
            router.get("/statistics/users", AdminAuth.check, statistics.users.bind(statistics.users));
            router.get("/statistics/now", AdminAuth.check, statistics.now.bind(statistics.now));

            let statisticsOrders: statOrdersRoute.Orders = new statOrdersRoute.Orders();
            router.get("/stat/missed_orders/:perPage/:page", AdminAuth.check, statisticsOrders.missed.bind(statisticsOrders.missed));
            router.get("/stat/active_orders/:perPage/:page", AdminAuth.check, statisticsOrders.active.bind(statisticsOrders.active));

            return router;
        }
    }
}

export = Route;

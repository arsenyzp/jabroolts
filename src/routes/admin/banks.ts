"use strict";

import * as express from "express";
import * as escapeStringRegexp from "escape-string-regexp";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {AjaxResponse} from "../../components/AjaxResponse";
import {BankModel} from "../../models/BankModel";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

module Route {

    class BankPost {

        _id: string = "";

        @Length(4, 200, {
            message: "admin.validation.BankName"
        })
        name: string;
    }

    export class Banks {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            // sort
            let sort: any = {};
            if (req.query.sortKey) {
                sort[req.query.sortKey] = req.query.reverse;
            } else {
                sort = {name: 1};
            }

            // search
            let filter: any = {};
            let search: string = "";
            if (req.query.q !== undefined && req.query.q !== "") {
                search = escapeStringRegexp(req.query.q);
                filter = {$or: [
                    {name: new RegExp(".*" + search + ".*", "i")}
                ]};
                sort = {name: 1};
            }

            // paging
            let skip: number = 0;
            let limit: number = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }

            BankModel
                .count(filter)
                .then(total_count => {
                    BankModel
                        .find(filter)
                        .sort(sort)
                        .limit(limit)
                        .skip(skip)
                        .then(items => {
                            ajaxResponse.setDate({
                                items: items,
                                total_count: total_count
                            });
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public get(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            BankModel
                .findOne({_id: req.params.id})
                .then(bank => {
                    if (bank == null) {
                        ajaxResponse.addErrorMessage("Item not found");
                        res.status(500);
                        return res.json(ajaxResponse.get());
                    }
                    ajaxResponse.setDate(bank);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public delete(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            BankModel
                .findOne({_id: req.params.id})
                .then(bank => {
                    if (bank == null) {
                        ajaxResponse.addErrorMessage("Item not found");
                        res.status(500);
                        return res.json(ajaxResponse.get());
                    }
                    BankModel
                        .remove({_id: req.params.id})
                        .then(r => {
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public save(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: BankPost = Object.create(BankPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                } else {
                    BankModel
                        .findOne({_id: post._id})
                        .then(bank => {
                            if (bank == null) {
                                bank = new BankModel();
                            }
                            bank.name = post.name;
                            bank
                                .save()
                                .then(bank => {
                                    ajaxResponse.setDate(bank);
                                    res.json(ajaxResponse.get());
                                })
                                .catch(err => {
                                    ajaxResponse.addError(err);
                                    res.status(500);
                                    res.json(ajaxResponse.get());
                                });
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                }
            });

        }

    }
}

export = Route;
"use strict";

import * as express from "express";
import * as escapeStringRegexp from "escape-string-regexp";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {AjaxResponse} from "../../components/AjaxResponse";
import {IUserModel, UserModel, UserStatus} from "../../models/UserModel";

module Route {

    export class Business {

        public async index(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let user: IUserModel = await UserModel.findOne({_id: req.params.id});
            if (user === null) {
                ajaxResponse.addErrorMessage("Item not found");
                res.status(500);
                return res.json(ajaxResponse.get());
            }
            // sort
            let sort: any = {};
            if (req.query.sortKey) {
                sort[req.query.sortKey] = req.query.reverse;
            } else {
                sort = {created_at: -1};
            }

            // search
            let filter: any = {_id: {$in: user.couriers}};
            let search: string = "";
            if (req.query.q !== undefined && req.query.q !== "") {
                search = escapeStringRegexp(req.query.q);
                filter = {$or: [
                        {first_name: new RegExp(".*" + search + ".*", "i")},
                        {last_name: new RegExp(".*" + search + ".*", "i")},
                        {phone: new RegExp(".*" + search + ".*", "i")},
                        //{email: new RegExp(".*" + search + ".*", "i")}
                    ],
                    _id: {$in: user.couriers}
                };
                sort = {created_at: -1};
            }
            filter.deleted = {$in: [false, null]};

            // paging
            let skip: number = 0;
            let limit: number = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }

            UserModel
                .count(filter)
                .then(total_count => {
                    UserModel
                        .find(filter)
                        .sort(sort)
                        .limit(limit)
                        .skip(skip)
                        .then(users => {
                            let items: Array<any> = [];
                            for (let i: number = 0; i < users.length; i++) {
                                let item: any = users[i].getApiFields();
                                item.bank = users[i].getApiFields();
                                item.vehicle = users[i].getVehicle();
                                item.bank = users[i].getBank();
                                item.license = users[i].getLicense();
                                item.socket_ids = users[i].socket_ids;
                                item._id = users[i]._id;
                                items.push(item);
                            }
                            ajaxResponse.setDate({
                                items: items,
                                total_count: total_count
                            });
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public delete(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            UserModel
                .findOneAndUpdate(
                    {_id: req.params.id},
                    {
                        $pull: {couriers: req.params.courier}
                    })
                .then(user => {
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public async save(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let user: IUserModel = await UserModel.findOne({jabroolid: req.params.courier});
            if (user === null) {
                ajaxResponse.addErrorMessage("Item not found");
                res.status(500);
                return res.json(ajaxResponse.get());
            }
            UserModel
                .findOneAndUpdate(
                    {_id: req.params.id},
                    {
                        $pull: {couriers: user._id.toString()}
                    })
                .then(user => {
                    UserModel
                        .findOneAndUpdate(
                            {_id: req.params.id},
                            {
                                $push: {couriers: user._id.toString()}
                            })
                        .then(user => {
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

    }
}

export = Route;

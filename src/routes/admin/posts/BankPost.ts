import {validate, Contains, IsInt, Length, IsEmail, IsNumberString, IsDate, Min, Max, IsBoolean, IsNumber, IsDateString} from "class-validator";

export class BankPost {

    @Length(2, 25, {
        message: "admin.validation.BankBranch"
    })
    branch: string;

    @Length(2, 25, {
        message: "admin.validation.BankAccount"
    })
    account_number: string;

    @Length(2, 25, {
        message: "admin.validation.HolderName"
    })
    holder_name: string;

    @Length(2, 25, {
        message: "admin.validation.BankType"
    })
    type: string;

    @Length(2, 25, {
        message: "admin.validation.InvalidBank"
    })
    bank: string;

    @Length(2, 25, {
        message: "admin.validation.InvalidCountry"
    })
    country_code: string;
}
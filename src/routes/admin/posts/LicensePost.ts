import {validate, Contains, IsInt, Length, IsEmail, IsNumberString, IsDate, Min, Max, IsBoolean, IsNumber, IsDateString} from "class-validator";

export class LicensePost {

    @Length(2, 25, {
        message: "admin.validation.LicenseNumber"
    })
    number: string;

    @Length(2, 25, {
        message: "admin.validation.LicenseName"
    })
    name: string;

    @IsDateString({
        message: "admin.validation.LicenseIssueDate"
    })
    issue_date: number;

    @IsDateString({
        message: "admin.validation.LicenseInvalidExpiryDate"
    })
    expiry_date: number;

    @Length(2, 25, {
        message: "admin.validation.LicenseInvalidCountry"
    })
    country_code: string;

    @Length(0, 25, {
        message: "admin.validation.LicenseInvalidImage"
    })
    image: string;
}
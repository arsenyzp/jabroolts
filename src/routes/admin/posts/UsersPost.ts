import {validate, Contains, IsInt, Length, IsEmail, IsNumberString, IsDate, Min, Max, IsBoolean, IsNumber, IsDateString} from "class-validator";

export class UsersPost {

    _id: string = "";

    @Length(2, 25, {
        message: "admin.validation.UserFirstName"
    })
    first_name: string;

    @Length(2, 25, {
        message: "admin.validation.UserLastName"
    })
    last_name: string;

    @IsEmail({}, {
        message: "admin.validation.UserInvalidEmail"
    })
    email: string;

    @Length(2, 25, {
        message: "admin.validation.UserInvalidPhone"
    })
    phone: string;

    @IsNumber({
        message: "admin.validation.UserInvalidBalance"
    })
    new_balance: number;

    @IsNumber({
        message: "admin.validation.UserInvalidCourierValue"
    })
    new_courierLimit: number;

    @Length(2, 25, {
        message: "admin.validation.UserInvalidUserStatus"
    })
    status: string;

    @Length(2, 25, {
        message: "admin.validation.UserInvalidUserRole"
    })
    role: string;

    @Length(2, 25, {
        message: "admin.validation.UserInvalidUserType"
    })
    type: string;

    @Length(0, 25, {
        message: "admin.validation.UserInvalidUserInvalidImage"
    })
    avatar: string;

    @IsBoolean({
        message: "admin.validation.VisibleInvalidValue"
    })
    visible: boolean;

    @IsBoolean({
        message: "admin.validation.InprogressInvalidValue"
    })
    in_progress: boolean;

    @Length(0, 8, {
        message: "admin.validation.PasswordInvalidValue",
        always: false
    })
    new_password: string;
}
import {validate, Contains, IsInt, Length, IsEmail, IsNumberString, IsDate, Min, Max, IsBoolean, IsNumber, IsDateString} from "class-validator";

export class VehiclePost {

    @Length(2, 4, {
        message: "admin.validation.InvalidYear"
    })
    year: string;

    @Length(2, 25, {
        message: "admin.validation.InvalidModel"
    })
    model: string;

    @Length(2, 25, {
        message: "admin.validation.VehicleType"
    })
    type: string;

    @Length(2, 25, {
        message: "admin.validation.VehicleNumber"
    })
    number: string;

    @Length(0, 25, {
        message: "admin.validation.InvalidImageFront"
    })
    image_front: string;

    @Length(0, 25, {
        message: "admin.validation.InvalidImageBack"
    })
    image_back: string;

    @Length(0, 25, {
        message: "admin.validation.InvalidImageSide"
    })
    image_side: string;

    @Length(0, 25, {
        message: "admin.validation.InvalidImagesInsurance",
        each: true
    })
    images_insurance: string;

    @Length(0, 25, {
        message: "admin.validation.ImagesInsuranceDelete",
        each: true
    })
    images_insurance_delete: string;
}
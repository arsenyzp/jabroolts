"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, IsNumber, ValidateNested} from "class-validator";
import {DeliveryConfig} from "../../../models/configs/DeliveryConfig";

module Route {

    class DeliveryPost {

        @IsNumber({
            message: "admin.validation.WaitingPeriod"
        })
        waiting_time: number;

        @IsNumber({
            message: "admin.validation.SpeedDefault"
        })
        average_speed: number;

        @IsNumber({
            message: "admin.validation.MissedTime"
        })
        missed_time: number;

    }


    export class Delivery {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let model: DeliveryConfig = new DeliveryConfig();
            model
                .getInstanse()
                .then(config => {
                    ajaxResponse.setDate(config);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public save(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: DeliveryPost = Object.create(DeliveryPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                } else {
                    let model: DeliveryConfig = new DeliveryConfig();

                    model.waiting_time = post.waiting_time;
                    model.average_speed = post.average_speed;
                    model.missed_time = post.missed_time;

                    model
                        .save()
                        .then(bank => {
                            ajaxResponse.setDate(model);
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                }
            });

        }

    }
}

export = Route;
"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {TermsConfig} from "../../../models/configs/TermsConfig";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, IsNumber, ValidateNested} from "class-validator";

module Route {

    class TermsPost {

        @Length(0, 9000, {
            message: "admin.validation.TermsRequired"
        })
        terms_courier_en: string;

        @Length(0, 9000, {
            message: "admin.validation.TermsRequired"
        })
        terms_courier_ar: string;

        @Length(0, 9000, {
            message: "admin.validation.TermsRequired"
        })
        terms_customer_en: string;

        @Length(0, 9000, {
            message: "admin.validation.TermsRequired"
        })
        terms_customer_ar: string;

        @Length(0, 9000, {
            message: "admin.validation.PrivacyRequired"
        })
        privacy_en: string;

        @Length(0, 9000, {
            message: "admin.validation.PrivacyRequired"
        })
        privacy_ar: string;
    }


    export class Terms {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let model: TermsConfig = new TermsConfig();
            model
                .getInstanse()
                .then(config => {
                    ajaxResponse.setDate(config);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public save(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: TermsPost = Object.create(TermsPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                } else {
                    let model: TermsConfig = new TermsConfig();

                    model.terms_courier_en = post.terms_courier_en;
                    model.terms_courier_ar = post.terms_courier_ar;
                    model.terms_customer_en = post.terms_customer_en;
                    model.terms_customer_ar = post.terms_customer_ar;
                    model.privacy_en = post.privacy_en;
                    model.privacy_ar = post.privacy_ar;

                    model
                        .save()
                        .then(bank => {
                            ajaxResponse.setDate(model);
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                }
            });

        }

    }
}

export = Route;
"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {CancelParamsConfig} from "../../../models/configs/CancelParamsConfig";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, IsNumber, ValidateNested} from "class-validator";

module Route {

    class RadiusPost {

        @IsNumber({
            message: "admin.validation.CancelParamsCost"
        })
        cost: number;
    }


    export class CancelRequestParams {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let model: CancelParamsConfig = new CancelParamsConfig();
            model
                .getInstanse()
                .then(config => {
                    ajaxResponse.setDate(config);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public save(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: RadiusPost = Object.create(RadiusPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                } else {
                    let model: CancelParamsConfig = new CancelParamsConfig();

                    model.cost = post.cost;

                    model
                        .save()
                        .then(bank => {
                            ajaxResponse.setDate(model);
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                }
            });

        }

    }
}

export = Route;
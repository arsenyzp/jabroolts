"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, IsNumber, ValidateNested} from "class-validator";
import {PackageRatioConfig} from "../../../models/configs/PackageRatioConfig";

module Route {

    class RadiusPost {

        @IsNumber({
            message: "admin.validation.SmallPackage"
        })
        small: number;

        @IsNumber({
            message: "admin.validation.MediumPackage"
        })
        medium: number;

        @IsNumber({
            message: "admin.validation.LagrePackage"
        })
        large: number;

    }


    export class PackageSize {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let model: PackageRatioConfig = new PackageRatioConfig();
            model
                .getInstanse()
                .then(config => {
                    ajaxResponse.setDate(config);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public save(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: RadiusPost = Object.create(RadiusPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                } else {
                    let model: PackageRatioConfig = new PackageRatioConfig();

                    model.small = post.small;
                    model.medium = post.medium;
                    model.large = post.large;

                    model
                        .save()
                        .then(bank => {
                            ajaxResponse.setDate(model);
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                }
            });

        }

    }
}

export = Route;
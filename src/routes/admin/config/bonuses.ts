"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, IsNumber, ValidateNested, IsBoolean, ValidateIf} from "class-validator";
import {DeliveryConfig} from "../../../models/configs/DeliveryConfig";
import {BonusesConfig} from "../../../models/configs/BonusesConfig";
import {UserModel} from "../../../models/UserModel";

module Route {

    class BonusesPost {

        @IsNumber({
            message: "admin.validation.BonusValue"
        })
        inviteBonuses: number;

        @IsNumber({
            message: "admin.validation.CourierDefaultLimit"
        })
        defaultCourierLimit: number;

        setForAll: boolean = false;

    }


    export class Bonuses {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let model: BonusesConfig = new BonusesConfig();
            model
                .getInstanse()
                .then(config => {
                    ajaxResponse.setDate(config);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public async save(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: BonusesPost = Object.create(BonusesPost.prototype);

            try {
                Object.assign(post, req.body, {});
                let errors: any = await validate(post);
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                } else {
                    let model: BonusesConfig = new BonusesConfig();

                    model.inviteBonuses = post.inviteBonuses;
                    let diff: number =  post.defaultCourierLimit - model.defaultCourierLimit;
                    model.defaultCourierLimit = post.defaultCourierLimit;
                    if (post.setForAll) {
                        await UserModel.update({}, {
                            $inc: {courierLimit: diff}
                        }, {multi: true});
                    }
                    await model
                        .save();
                    ajaxResponse.setDate(model);
                    res.json(ajaxResponse.get());
                }
            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }

        }

    }
}

export = Route;
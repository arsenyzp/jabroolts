"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {LogsConfig} from "../../../models/configs/LogsConfig";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, IsNumber, ValidateNested, IsBoolean} from "class-validator";

module Route {

    class LogsConfigPost {

        @IsBoolean({
            message: "Api invalid value"
        })
        api: boolean;

        @IsBoolean({
            message: "Error invalid value"
        })
        error: boolean;

        @IsBoolean({
            message: "Email invalid value"
        })
        email: boolean;

        @IsBoolean({
            message: "Sms invalid value"
        })
        sms: boolean;

    }


    export class Logs {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let model: LogsConfig = new LogsConfig();
            model
                .getInstanse()
                .then(config => {
                    ajaxResponse.setDate(config);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public save(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: LogsConfigPost = Object.create(LogsConfigPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                } else {
                    let model: LogsConfig = new LogsConfig();

                    model.api = post.api;
                    model.error = post.error;
                    model.email = post.email;
                    model.sms = post.sms;

                    model
                        .save()
                        .then(bank => {
                            ajaxResponse.setDate(model);
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                }
            });

        }

    }
}

export = Route;
"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {RadiusConfig} from "../../../models/configs/RadiusConfig";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, IsNumber, ValidateNested} from "class-validator";
import {PriceConfig} from "../../../models/configs/PriceConfig";

module Route {

    class PricePost {

        @IsNumber({
            message: "admin.validation.EPercentRequired"
        })
        e_percent: number;

        @IsNumber({
            message: "admin.validation.JPercentRequired"
        })
        j_percent: number;

        @IsNumber({
            message: "admin.validation.EFixCost"
        })
        e_fix_cost: number;

        @IsNumber({
            message: "admin.validation.JFixCost"
        })
        j_fix_cost: number;

        @IsNumber({
            message: "admin.validation.EMinCost"
        })
        e_min_cost: number;

        @IsNumber({
            message: "admin.validation.JMinCost"
        })
        j_min_cost: number;

    }


    export class Price {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let model: PriceConfig = new PriceConfig();
            model
                .getInstanse()
                .then(config => {
                    ajaxResponse.setDate(config);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public save(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: PricePost = Object.create(PricePost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                } else {
                    let model: PriceConfig = new PriceConfig();

                    model.e_percent = post.e_percent;
                    model.j_percent = post.j_percent;

                    model.e_fix_cost = post.e_fix_cost;
                    model.j_fix_cost = post.j_fix_cost;

                    model.e_min_cost = post.e_min_cost;
                    model.j_min_cost = post.j_min_cost;

                    model
                        .save()
                        .then(bank => {
                            ajaxResponse.setDate(model);
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                }
            });

        }

    }
}

export = Route;
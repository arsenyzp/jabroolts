"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {PackageConfigPrice} from "../../../models/configs/PackageConfigPrice";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, IsNumber, ValidateNested} from "class-validator";

module Route {

    class PackagePostItem {

        @IsNumber({
            message: "admin.validation.JCost"
        })
        j_cost: number;

        @IsNumber({
            message: "admin.validation.ECost"
        })
        e_cost: number;

        @IsNumber({
            message: "admin.validation.FromDimension"
        })
        from_dimension: number;

        @IsNumber({
            message: "admin.validation.ToDimension"
        })
        to_dimension: number;

        @IsNumber({
            message: "admin.validation.MaxCount"
        })
        max_count: number;
    }

    class PackagePost {

        @ValidateNested()
        small: PackagePostItem;

        @ValidateNested()
        medium: PackagePostItem;

        @ValidateNested()
        large: PackagePostItem;

    }

    export class Packages {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let model: PackageConfigPrice = new PackageConfigPrice();
            model
                .getInstanse()
                .then(config => {
                    ajaxResponse.setDate(config);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public save(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: PackagePost = Object.create(PackagePost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                } else {
                    let model: PackageConfigPrice = new PackageConfigPrice();

                    model.small.e_cost = post.small.e_cost;
                    model.small.j_cost = post.small.j_cost;

                    model.small.max_count = post.small.max_count;
                    model.small.from_dimension = post.medium.from_dimension;
                    model.small.to_dimension = post.medium.to_dimension;

                    model.medium.e_cost = post.medium.e_cost;
                    model.medium.j_cost = post.medium.j_cost;

                    model.medium.max_count = post.medium.max_count;
                    model.medium.from_dimension = post.medium.from_dimension;
                    model.medium.to_dimension = post.medium.to_dimension;

                    model.large.e_cost = post.large.e_cost;
                    model.large.j_cost = post.large.j_cost;

                    model.large.max_count = post.large.max_count;
                    model.large.from_dimension = post.medium.from_dimension;
                    model.large.to_dimension = post.medium.to_dimension;

                    model
                        .save()
                        .then(bank => {
                            ajaxResponse.setDate(model);
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                }
            });

        }

    }
}

export = Route;
"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {AjaxResponse} from "../../../components/AjaxResponse";
import {RadiusConfig} from "../../../models/configs/RadiusConfig";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, IsNumber, ValidateNested} from "class-validator";

module Route {

    class RadiusPost {

        @IsNumber({
            message: "admin.validation.RadiusRequest"
        })
        request: number;

        @IsNumber({
            message: "admin.validation.RadiusFeed"
        })
        feed: number;

        @IsNumber({
            message: "admin.validation.RadiusDropOff"
        })
        drop_off: number;

        @IsNumber({
            message: "admin.validation.CloseRadius"
        })
        close_radius: number;

    }


    export class Radius {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            let model: RadiusConfig = new RadiusConfig();
            model
                .getInstanse()
                .then(config => {
                    ajaxResponse.setDate(config);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public save(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: RadiusPost = Object.create(RadiusPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                } else {
                    let model: RadiusConfig = new RadiusConfig();

                    model.drop_off = post.drop_off;
                    model.feed = post.feed;
                    model.request = post.request;
                    model.close_radius = post.close_radius;

                    model
                        .save()
                        .then(bank => {
                            ajaxResponse.setDate(model);
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                }
            });

        }

    }
}

export = Route;
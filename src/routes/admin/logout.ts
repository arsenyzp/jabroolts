"use strict";

import * as express from "express";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {AppConfig} from "../../components/config";

module Route {

    export class Index {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            req.session.user = null;
            res.redirect(AppConfig.getInstanse().get("urls:admin_login"));
        }
    }
}

export = Route;
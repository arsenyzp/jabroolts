"use strict";

import * as express from "express";
import * as escapeStringRegexp from "escape-string-regexp";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {AjaxResponse} from "../../components/AjaxResponse";
import {IUserModel, UserModel, UserStatus} from "../../models/UserModel";
import {validate, Contains, IsInt, Length, IsEmail, IsNumberString, IsDate, Min, Max, IsBoolean, IsNumber, IsDateString} from "class-validator";
import {FileHelper} from "../../components/FileHelper";
import {AppConfig} from "../../components/config";
import {Log} from "../../components/Log";
import {BankModel, IBankModel} from "../../models/BankModel";
import {CountryModel, ICountryModel} from "../../models/CountryModel";
import {BankTypeModel, IBankTypeModel} from "../../models/BankTypeModel";
import {CarTypeModel, ICarTypeModelModel} from "../../models/CarTypeModel";
import {IManufactureModel, ManufactureModel} from "../../models/ManufactureModel";
import {PaymentModule} from "../../components/jabrool/PaymentModule";
import {UsersPost} from "./posts/UsersPost";
import {BankPost} from "./posts/BankPost";
import {LicensePost} from "./posts/LicensePost";
import {VehiclePost} from "./posts/VehiclePost";
import {CourierBalanceLogModel, CourierBalanceTypes, ICourierBalanceLogModel} from "../../models/CourierBalanceLogModel";
import {BonusesConfig} from "../../models/configs/BonusesConfig";
import {SocketServer} from "../../components/SocketServer";
import {NotificationModule, NotificationTypes} from "../../components/NotificationModule";

const async: any = require("async");
const log: any = Log.getInstanse()(module);

module Route {

    export class Profile {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            UserModel
                .findOne({_id: req.session.user._id})
                .then(u => {
                    if (u == null) {
                        ajaxResponse.addErrorMessage("Item not found");
                        res.status(500);
                        return res.json(ajaxResponse.get());
                    }
                    ajaxResponse.setDate(u);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public getToken(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            UserModel
                .findOne({_id: req.session.user._id})
                .then(u => {
                    if (u == null) {
                        ajaxResponse.addErrorMessage("Item not found");
                        res.status(500);
                        return res.json(ajaxResponse.get());
                    }
                    ajaxResponse.setDate(u);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

    }
}

export = Route;
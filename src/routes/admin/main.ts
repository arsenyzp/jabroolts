"use strict";

import * as express from "express";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {AjaxResponse} from "../../components/AjaxResponse";
import {UserModel} from "../../models/UserModel";

module Route {

    export class Index {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {

            //render page
            let css: Array<string> = [

            ];
            let js: Array<string> = [
                "/dist_admin/admin_bundle.js",
            ];

            res.render("admin/layouts/main", {
                css: css,
                js: js
            });
        }

    }
}

export = Route;
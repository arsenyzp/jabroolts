"use strict";

import * as express from "express";
import * as escapeStringRegexp from "escape-string-regexp";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {AjaxResponse} from "../../components/AjaxResponse";
import {CountryModel} from "../../models/CountryModel";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

module Route {

    class CountyPost {

        _id: string = "";

        @Length(3, 20, {
            message: "admin.validation.CountryName"
        })
        name: string;

        @Length(1, 4, {
            message: "admin.validation.CountryCode"
        })
        code: string;
    }

    export class Countries {

        public index(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            // sort
            let sort: any = {};
            if (req.query.sortKey) {
                sort[req.query.sortKey] = req.query.reverse;
            } else {
                sort = {name: 1};
            }

            // search
            let filter: any = {};
            let search: string = "";
            if (req.query.q !== undefined && req.query.q !== "") {
                search = escapeStringRegexp(req.query.q);
                filter = {$or: [
                    {name: new RegExp(".*" + search + ".*", "i")}
                ]};
                sort = {name: 1};
            }

            // paging
            let skip: number = 0;
            let limit: number = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }

            CountryModel
                .count(filter)
                .then(total_count => {
                    CountryModel
                        .find(filter)
                        .sort(sort)
                        .limit(limit)
                        .skip(skip)
                        .then(items => {
                            ajaxResponse.setDate({
                                items: items,
                                total_count: total_count
                            });
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public get(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            CountryModel
                .findOne({_id: req.params.id})
                .then(bank => {
                    if (bank == null) {
                        ajaxResponse.addErrorMessage("Item not found");
                        res.status(500);
                        return res.json(ajaxResponse.get());
                    }
                    ajaxResponse.setDate(bank);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public delete(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            CountryModel
                .findOne({_id: req.params.id})
                .then(bank => {
                    if (bank == null) {
                        ajaxResponse.addErrorMessage("Item not found");
                        res.status(500);
                        return res.json(ajaxResponse.get());
                    }
                    CountryModel
                        .remove({_id: req.params.id})
                        .then(r => {
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public save(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: CountyPost = Object.create(CountyPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                } else {
                    CountryModel
                        .findOne({_id: post._id})
                        .then(item => {
                            if (item == null) {
                                item = new CountryModel();
                            }
                            item.name = post.name;
                            item.code = post.code;
                            item.phone = "+1";
                            item
                                .save()
                                .then(item => {
                                    ajaxResponse.setDate(item);
                                    res.json(ajaxResponse.get());
                                })
                                .catch(err => {
                                    ajaxResponse.addError(err);
                                    res.status(500);
                                    res.json(ajaxResponse.get());
                                });
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                }
            });

        }

    }
}

export = Route;
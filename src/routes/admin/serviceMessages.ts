"use strict";

import * as express from "express";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {AjaxResponse} from "../../components/AjaxResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, IsBoolean} from "class-validator";
import {IServiceMessageModel, ServiceMessageModel} from "../../models/ServiceMessageModel";
import {IUserModel, UserModel} from "../../models/UserModel";
import {NotificationModule, NotificationTypes} from "../../components/NotificationModule";

module Route {

    class ServiceMessagesPost {
        @Length(5, 400, {
            message: "admin.validation.MessageMin5Max400"
        })
        text: string;

        @Length(20, 40, {
            message: "admin.validation.InvalidUserId"
        })
        user: string;
    }

    export class ServiceMessages {

        public async index(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            // sort
            let sort: any = {};
            if (req.query.sortKey) {
                sort[req.query.sortKey] = req.query.reverse;
            } else {
                sort = {created_at: -1};
            }

            // search
            let filter: any = {};

            if (!!req.query.user) {
                filter.recipient = req.query.user;
            }

            // paging
            let skip: number = 0;
            let limit: number = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }

            try {
                let total_count: number = await ServiceMessageModel
                    .count(filter);
                let items: Array<IServiceMessageModel> = await ServiceMessageModel
                    .find(filter)
                    .sort(sort)
                    .limit(limit)
                    .skip(skip);

                let user: IUserModel = await UserModel.findOne({_id: req.query.user});

                ajaxResponse.setDate({
                    items: items,
                    total_count: total_count,
                    user: user
                });
                res.json(ajaxResponse.get());

            } catch (err) {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            }
        }

        public save(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            let post: ServiceMessagesPost = Object.create(ServiceMessagesPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                } else {
                    UserModel
                        .findOne({_id: post.user})
                        .then(user => {
                            if (user === null) {
                                ajaxResponse.addError({message: "User not found"});
                                res.status(500);
                                res.json(ajaxResponse.get());
                            } else {
                                let model: IServiceMessageModel = new ServiceMessageModel();
                                model.sender = req.session.user._id;
                                model.recipient = user;
                                model.text = post.text;
                                model.save()
                                    .then(item => {

                                        NotificationModule.getInstance().send(
                                            [user._id.toString()],
                                            NotificationTypes.JabroolMessage,
                                            {text: post.text},
                                            model._id.toString()
                                        );

                                        ajaxResponse.setDate(item);
                                        res.json(ajaxResponse.get());
                                    })
                                    .catch(err => {
                                        ajaxResponse.addError(err);
                                        res.status(500);
                                        res.json(ajaxResponse.get());
                                    });
                            }
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                }
            });
        }
    }
}

export = Route;
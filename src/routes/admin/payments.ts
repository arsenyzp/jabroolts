"use strict";

import * as express from "express";
import * as escapeStringRegexp from "escape-string-regexp";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {AjaxResponse} from "../../components/AjaxResponse";
import {PromoCodModel} from "../../models/PromoCodModel";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {PromoCodHelper} from "../../components/jabrool/PromoCodHelper";
import {PaymentLogModel} from "../../models/PaymentLog";
import {IUserModel, UserModel} from "../../models/UserModel";

module Route {

    export class Payments {

        public async index(req: RequestInterface, res: express.Response, next: express.NextFunction): Promise<any> {
            let ajaxResponse: AjaxResponse = new AjaxResponse();

            // sort
            let sort: any = {};
            if (req.query.sortKey) {
                sort[req.query.sortKey] = req.query.reverse;
            } else {
                sort = {created_at: -1};
            }

            // search
            let filter: any = {};
            let search: string = "";
            if (req.query.q !== undefined && req.query.q !== "") {
                search = escapeStringRegexp(req.query.q);
                let filter2: any = {$or: [
                    {"email": new RegExp(".*" + search + ".*", "i")},
                    {"phone": new RegExp(".*" + search + ".*", "i")}
                ]};
                let users: Array<IUserModel> = await UserModel.find(filter2);
                let ids: Array<any> = [];
                users.map( (v, k, a) => {
                    ids.push(v._id.toString());
                });
                filter.user = {$in: ids};
            }

            // paging
            let skip: number = 0;
            let limit: number = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }

            PaymentLogModel
                .count(filter)
                .then(total_count => {
                    PaymentLogModel
                        .find(filter)
                        .populate({path: "user", model: UserModel})
                        .sort(sort)
                        .limit(limit)
                        .skip(skip)
                        .then(items => {
                            ajaxResponse.setDate({
                                items: items,
                                total_count: total_count
                            });
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public get(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            PaymentLogModel
                .findOne({_id: req.params.id})
                .populate({path: "user", model: UserModel})
                .then(bank => {
                    if (bank == null) {
                        ajaxResponse.addErrorMessage("Item not found");
                        res.status(500);
                        return res.json(ajaxResponse.get());
                    }
                    ajaxResponse.setDate(bank);
                    res.json(ajaxResponse.get());
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

        public delete(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
            let ajaxResponse: AjaxResponse = new AjaxResponse();
            PaymentLogModel
                .findOne({_id: req.params.id})
                .then(bank => {
                    if (bank == null) {
                        ajaxResponse.addErrorMessage("Item not found");
                        res.status(500);
                        return res.json(ajaxResponse.get());
                    }
                    PaymentLogModel
                        .remove({_id: req.params.id})
                        .then(r => {
                            res.json(ajaxResponse.get());
                        })
                        .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                })
                .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
        }

    }
}

export = Route;
"use strict";

import * as express from "express";
import * as createOrderMethods from "../../../api/methods/order/CreateOrderMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class Order {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let createOrder: createOrderMethods.CreateOrderMethods = new createOrderMethods.CreateOrderMethods();
            router.post("/calculate", ApiAuth.check, createOrder.calculate.bind(createOrder.calculate));
            router.post("/new_order", ApiAuth.check, createOrder.create.bind(createOrder.create));

            return router;
        }
    }
}

export = Route;
"use strict";

import * as express from "express";
import * as cardsMethods from "../../../api/methods/profile/CardsMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class Cards {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let cards: cardsMethods.CardsMethods = new cardsMethods.CardsMethods();
            router.post("/cards", ApiAuth.check, cards.cardsList.bind(cards.cardsList));
            router.post("/save_card", ApiAuth.check, cards.saveCard.bind(cards.saveCard));
            router.post("/remove_card", ApiAuth.check, cards.removeCard.bind(cards.removeCard));

            return router;
        }
    }
}

export = Route;
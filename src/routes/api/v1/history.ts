"use strict";

import * as express from "express";
import * as historyMethods from "../../../api/methods/HistoryOrdersMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class History {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let history: historyMethods.HistoryOrdersMethods = new historyMethods.HistoryOrdersMethods();
            router.post("/history", ApiAuth.check, history.index.bind(history.index));

            return router;
        }
    }
}

export = Route;
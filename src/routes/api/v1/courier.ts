"use strict";

import * as express from "express";
import * as courierMethods from "../../../api/methods/CourierMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class Courier {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let courier: courierMethods.CourierMethods = new courierMethods.CourierMethods();
            router.post("/check_courier_status", ApiAuth.check, courier.checkCourierStatus.bind(courier.checkCourierStatus));
            router.post("/get_courier_profile", ApiAuth.check, courier.getCourierProfile.bind(courier.getCourierProfile));
            router.post("/courier_requests", ApiAuth.check, courier.getCourierRequests.bind(courier.getCourierRequests));
            router.post("/accept_request", ApiAuth.check, courier.acceptRequests.bind(courier.acceptRequests));
            router.post("/decline_request", ApiAuth.check, courier.declineRequests.bind(courier.declineRequests));
            router.post("/request_update_number", ApiAuth.check, courier.requestUpdateNumber.bind(courier.requestUpdateNumber));

            return router;
        }
    }
}

export = Route;
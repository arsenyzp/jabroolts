"use strict";

import * as express from "express";
import * as earningsMethods from "../../../api/methods/EarningsMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class Earnings {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let earnings: earningsMethods.EarningsMethods = new earningsMethods.EarningsMethods();
            router.post("/earnings_week", ApiAuth.check, earnings.week.bind(earnings.week));
            router.post("/earnings_month", ApiAuth.check, earnings.month.bind(earnings.month));
            router.post("/earnings_year", ApiAuth.check, earnings.year.bind(earnings.year));

            return router;
        }
    }
}

export = Route;
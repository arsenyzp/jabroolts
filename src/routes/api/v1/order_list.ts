"use strict";

import * as express from "express";
import * as listOrderMethods from "../../../api/methods/order/OrdersListMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class OrderList {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let listOrder: listOrderMethods.OrdersListMethods = new listOrderMethods.OrdersListMethods();
            router.post("/my_order_courier", ApiAuth.check, listOrder.getMyOrdersCourier.bind(listOrder.getMyOrdersCourier));
            router.post("/my_order_customer", ApiAuth.check, listOrder.getMyOrdersCustomer.bind(listOrder.getMyOrdersCustomer));

            return router;
        }
    }
}

export = Route;
"use strict";

import * as express from "express";
import * as catalogMethods from "../../../api/methods/CatalogMethods";

module Route {

    export class Catalog {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let catalog: catalogMethods.CatalogMethods = new catalogMethods.CatalogMethods();
            router.post("/catalogs/banks", catalog.bank.bind(catalog.bank));
            router.post("/catalogs/countries", catalog.countries.bind(catalog.countries));
            router.post("/catalogs/catalogs", catalog.catalogs.bind(catalog.catalogs));
            router.post("/catalogs/car_types", catalog.сarTypes.bind(catalog.сarTypes));
            router.post("/catalogs/packages", catalog.packages.bind(catalog.packages));
            router.post("/catalogs/terms", catalog.terms.bind(catalog.terms));

            return router;
        }
    }
}

export = Route;
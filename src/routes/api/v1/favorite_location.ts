"use strict";

import * as express from "express";
import * as favoriteLocationMethods from "../../../api/methods/FavoriteLocationsMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class FavoriteLocation {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let confirm: favoriteLocationMethods.FavoriteLocationsMethods = new favoriteLocationMethods.FavoriteLocationsMethods();
            router.post("/favorites", ApiAuth.check, confirm.list.bind(confirm.list));
            router.post("/add_favorite_location", ApiAuth.check, confirm.save.bind(confirm.save));
            router.post("/remove_favorite_location", ApiAuth.check, confirm.delete.bind(confirm.delete));

            return router;
        }
    }
}

export = Route;
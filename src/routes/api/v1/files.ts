"use strict";

import * as express from "express";
import * as filesMethods from "../../../api/methods/FilesMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class Files {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let files: filesMethods.FilesMethods = new filesMethods.FilesMethods();
            router.post("/upload_image", ApiAuth.check, files.upload.bind(files.upload));
            //TODO убрать
            router.post("/upload", ApiAuth.check, files.upload.bind(files.upload));

            router.post("/delete_image", ApiAuth.check, files.delete.bind(files.delete));
            router.get("/resize_image", ApiAuth.check, files.resize.bind(files.resize));

            return router;
        }
    }
}

export = Route;
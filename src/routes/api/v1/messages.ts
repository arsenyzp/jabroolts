"use strict";

import * as express from "express";
import * as messagesMethods from "../../../api/methods/MessagesMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class Messages {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let confirm: messagesMethods.MessagesMethods = new messagesMethods.MessagesMethods();
            router.post("/messages", ApiAuth.check, confirm.index.bind(confirm.index));

            return router;
        }
    }
}

export = Route;
"use strict";

import * as express from "express";
import * as deliveryMethods from "../../../api/methods/order/DeliveryMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class Delivery {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let delivery: deliveryMethods.DeliveryMethods = new deliveryMethods.DeliveryMethods();
            router.post("/confirm_pickup", ApiAuth.check, delivery.confirmPickUp.bind(delivery.confirmPickUp));
            router.post("/confirm_delivery", ApiAuth.check, delivery.confirmDelivery.bind(delivery.confirmDelivery));
            router.post("/start_delivery", ApiAuth.check, delivery.startDelivery.bind(delivery.startDelivery));
            router.post("/courier_on_way", ApiAuth.check, delivery.courierOnWay.bind(delivery.courierOnWay));
            router.post("/at_destination", ApiAuth.check, delivery.atDestinition.bind(delivery.atDestinition));

            return router;
        }
    }
}

export = Route;
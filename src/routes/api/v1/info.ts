"use strict";

import * as express from "express";
import * as infoMethods from "../../../api/methods/order/InfoMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class Info {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let info: infoMethods.InfoMethods = new infoMethods.InfoMethods();
            router.post("/get_profile_by_jid", ApiAuth.check, info.getProfileByJid.bind(info.getProfileByJid));
            router.post("/get_order_info", ApiAuth.check, info.getOrderById.bind(info.getOrderById));

            router.post("/get_pickup_confirm_code", ApiAuth.check, info.getPickupConfirmCode.bind(info.getPickupConfirmCode));
            router.post("/get_delivery_confirm_code", ApiAuth.check, info.getDeliveryConfirmCode.bind(info.getDeliveryConfirmCode));

            router.post("/get_courier_profile_by_order", ApiAuth.check, info.getCourierProfileByOrder.bind(info.getCourierProfileByOrder));

            return router;
        }
    }
}

export = Route;
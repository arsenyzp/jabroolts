"use strict";

import * as express from "express";
import * as reviewsMethods from "../../../api/methods/ReviewMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class Reviews {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let review: reviewsMethods.ReviewMethods = new reviewsMethods.ReviewMethods();
            router.post("/save_review", ApiAuth.check, review.saveReview.bind(review.saveReview));

            return router;
        }
    }
}

export = Route;
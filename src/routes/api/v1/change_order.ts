"use strict";

import * as express from "express";
import * as changeMethods from "../../../api/methods/order/ChangeOrderMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class ChangeOrder {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let change: changeMethods.ChangeOrderMethods = new changeMethods.ChangeOrderMethods();
            router.post("/courier_change_order", ApiAuth.check, change.courierChangeOrder.bind(change.courierChangeOrder));
            router.post("/customer_accept_change", ApiAuth.check, change.customerAcceptChange.bind(change.customerAcceptChange));
            router.post("/update_receiver_number", ApiAuth.check, change.updateRecipientPhone.bind(change.updateRecipientPhone));

            return router;
        }
    }
}

export = Route;
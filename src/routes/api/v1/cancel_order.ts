"use strict";

import * as express from "express";
import * as cancelMethods from "../../../api/methods/order/CancelOrderMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class CancelOrder {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let cancel: cancelMethods.CancelOrderMethods = new cancelMethods.CancelOrderMethods();
            router.post("/cancel_cost", ApiAuth.check, cancel.cancelCost.bind(cancel.cancelCost));
            router.post("/cancel_request", ApiAuth.check, cancel.cancelRequest.bind(cancel.cancelRequest));
            router.post("/cancel_delivery", ApiAuth.check, cancel.cancelDelivery.bind(cancel.cancelDelivery));

            return router;
        }
    }
}

export = Route;
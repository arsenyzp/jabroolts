"use strict";

import * as express from "express";
import * as authMethods from "../../../api/methods/AuthMethods";

module Route {

    export class Auth {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let login: authMethods.AuthMethods = new authMethods.AuthMethods();
            router.post("/login", login.login.bind(login.login));
            router.post("/register", login.register.bind(login.register));
            router.post("/forgot", login.forgot.bind(login.forgot));

            return router;
        }
    }
}

export = Route;
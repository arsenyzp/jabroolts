"use strict";

import * as express from "express";
import * as openAppMethods from "../../../api/methods/reports/OpenAppMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class Reports {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let openApp: openAppMethods.OpenAppMethods = new openAppMethods.OpenAppMethods();
            router.post("/open_app", openApp.openApp.bind(openApp.openApp));

            return router;
        }
    }
}

export = Route;
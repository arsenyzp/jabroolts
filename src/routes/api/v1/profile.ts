"use strict";

import * as express from "express";
import * as confirmMethods from "../../../api/methods/profile/ConfirmCodeMethods";
import * as bankMethods from "../../../api/methods/profile/BankMethods";
import * as vehicleMethods from "../../../api/methods/profile/VehicleMethods";
import * as profileMethods from "../../../api/methods/profile/ProfileMethods";
import * as paymentSettingsMethods from "../../../api/methods/profile/ProfilePaymentSettings";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class Profile {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let confirm: confirmMethods.ConfirmCodeMethods = new confirmMethods.ConfirmCodeMethods();
            router.post("/get_confirm_code", ApiAuth.check, confirm.getCode.bind(confirm.getCode));
            router.post("/get_confirm_password_code", ApiAuth.check, confirm.getPasswordCode.bind(confirm.getPasswordCode));

            let bank: bankMethods.BankMethods = new bankMethods.BankMethods();
            router.post("/get_bank", ApiAuth.check, bank.get.bind(bank.get));
            router.post("/save_bank", ApiAuth.check, bank.save.bind(bank.save));

            let vehicle: vehicleMethods.VehicleMethods = new vehicleMethods.VehicleMethods();
            router.post("/save_license_info", ApiAuth.check, vehicle.saveLicense.bind(vehicle.saveLicense));
            router.post("/save_vehicle", ApiAuth.check, vehicle.saveVehicle.bind(vehicle.saveVehicle));

            let paymentsSettings: paymentSettingsMethods.ProfilePaymentSettings = new paymentSettingsMethods.ProfilePaymentSettings();
            router.post("/get_token", ApiAuth.check, paymentsSettings.getToken.bind(paymentsSettings.getToken));
            router.post("/set_pay_type", ApiAuth.check, paymentsSettings.setPayType.bind(paymentsSettings.setPayType));

            let profile: profileMethods.ProfileMethods = new profileMethods.ProfileMethods();
            router.post("/get_profile", ApiAuth.check, profile.get.bind(profile.get));
            router.post("/save_profile", ApiAuth.check, profile.save.bind(profile.save));
            router.post("/set_password", ApiAuth.check, profile.setPassword.bind(profile.setPassword));
            router.post("/confirm_change_password", ApiAuth.check, profile.confirmPassword.bind(profile.confirmPassword));
            router.post("/set_visible", ApiAuth.check, profile.setVisible.bind(profile.setVisible));
            router.post("/upload_avatar", ApiAuth.check, profile.uploadAvatar.bind(profile.uploadAvatar));

            return router;
        }
    }
}

export = Route;
"use strict";

import * as express from "express";
import * as promoMethods from "../../../api/methods/PromoMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class Promo {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let promo: promoMethods.PromoMethods = new promoMethods.PromoMethods();
            router.post("/check_promo", ApiAuth.check, promo.checkPromo.bind(promo.checkPromo));
            router.post("/save_promo", ApiAuth.check, promo.save.bind(promo.save));
            router.post("/day_bonus", ApiAuth.check, promo.dayPromo.bind(promo.dayPromo));
            router.post("/courier_limit", ApiAuth.check, promo.getCourierLimit.bind(promo.getCourierLimit));

            return router;
        }
    }
}

export = Route;
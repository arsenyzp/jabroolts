"use strict";

import * as express from "express";
import * as notificationsMethods from "../../../api/methods/NotificationMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class Notifications {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let confirm: notificationsMethods.NotificationMethods = new notificationsMethods.NotificationMethods();
            router.post("/notifications", ApiAuth.check, confirm.index.bind(confirm.index));

            return router;
        }
    }
}

export = Route;
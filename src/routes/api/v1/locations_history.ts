"use strict";

import * as express from "express";
import * as locationsMethods from "../../../api/methods/LocationHistoryMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class LocationsHistory {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let history: locationsMethods.LocationHistoryMethods = new locationsMethods.LocationHistoryMethods();
            router.post("/location_history", ApiAuth.check, history.index.bind(history.index));

            return router;
        }
    }
}

export = Route;
"use strict";

import * as express from "express";
import * as returnMethods from "../../../api/methods/order/ReturnOrderMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class Return {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let returnOrder: returnMethods.ReturnMethods = new returnMethods.ReturnMethods();

            router.post("/start_return", ApiAuth.check, returnOrder.startReturn.bind(returnOrder.startReturn));
            router.post("/cancel_return", ApiAuth.check, returnOrder.cancelReturn.bind(returnOrder.cancelReturn));
            router.post("/at_return_destination", ApiAuth.check, returnOrder.atReturnDestination.bind(returnOrder.atReturnDestination));
            router.post("/get_return_confirm_code", ApiAuth.check, returnOrder.getReturnConfirmCode.bind(returnOrder.getReturnConfirmCode));
            router.post("/confirm_return", ApiAuth.check, returnOrder.confirmReturn.bind(returnOrder.confirmReturn));
            router.post("/return_cost", ApiAuth.check, returnOrder.getReturnCost.bind(returnOrder.getReturnCost));

            return router;
        }
    }
}

export = Route;
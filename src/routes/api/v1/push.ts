"use strict";

import * as express from "express";
import * as pushMethods from "../../../api/methods/PushMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class Push {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let push: pushMethods.PushMethods = new pushMethods.PushMethods();
            router.post("/add_device", ApiAuth.check, push.save.bind(push.save));
            router.post("/remove_device", ApiAuth.check, push.remove.bind(push.remove));

            return router;
        }
    }
}

export = Route;
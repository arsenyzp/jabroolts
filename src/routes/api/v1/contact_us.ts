"use strict";

import * as express from "express";
import * as contactUsMethods from "../../../api/methods/ContactUsMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class ContactUs {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let contactUs: contactUsMethods.ContactUsMethods = new contactUsMethods.ContactUsMethods();
            router.post("/contact_us", ApiAuth.check, contactUs.contact.bind(contactUs.contact));

            return router;
        }
    }
}

export = Route;
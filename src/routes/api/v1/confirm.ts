"use strict";

import * as express from "express";
import * as confirmMethods from "../../../api/methods/ConfirmMethods";
import {ApiAuth} from "../../../components/ApiAuth";

module Route {

    export class Confirm {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let confirm: confirmMethods.ConfirmMethods = new confirmMethods.ConfirmMethods();
            router.post("/confirm", ApiAuth.check, confirm.confirm.bind(confirm.confirm));
            router.post("/resend", ApiAuth.check, confirm.resend.bind(confirm.resend));

            return router;
        }
    }
}

export = Route;
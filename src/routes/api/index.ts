"use strict";

import * as express from "express";
import * as authRoute from "./v1/auth";
import * as catalogRoute from "./v1/catalog";
import * as pushRoute from "./v1/push";
import * as confirmRoute from "./v1/confirm";
import * as favoriteLocationsRoute from "./v1/favorite_location";
import * as locationHistoryRoute from "./v1/locations_history";
import * as messagesRoute from "./v1/messages";
import * as filesRoute from "./v1/files";
import * as profileRoute from "./v1/profile";
import * as courierRoute from "./v1/courier";
import * as promoRoute from "./v1/promo";
import * as orderRoute from "./v1/order";
import * as infoRoute from "./v1/info";
import * as deliveryRoute from "./v1/delivery";
import * as orderListRoute from "./v1/order_list";
import * as cahgeOrderRoute from "./v1/change_order";
import * as returnOrder from "./v1/return_order";
import * as cardsRoute from "./v1/cards";
import * as reviewsRoute from "./v1/reviews";
import * as cancelOrderRoute from "./v1/cancel_order";
import * as historyRoute from "./v1/history";
import * as contactUsRoute from "./v1/contact_us";
import * as notificationsRoute from "./v1/notifications";
import * as earningsRoute from "./v1/earnings";
import * as reportsRoute from "./v1/reports";

module Route {

    export class Index {

        public index(): express.Router {

            //get router
            let router: express.Router;
            router = express.Router();

            let auth: authRoute.Auth = new authRoute.Auth();
            router.use(auth.index());

            let catalog: catalogRoute.Catalog = new catalogRoute.Catalog();
            router.use(catalog.index());

            let push: pushRoute.Push = new pushRoute.Push();
            router.use(push.index());

            let confirm: confirmRoute.Confirm = new confirmRoute.Confirm();
            router.use(confirm.index());

            let favoriteLocations: favoriteLocationsRoute.FavoriteLocation = new favoriteLocationsRoute.FavoriteLocation();
            router.use(favoriteLocations.index());

            let location: locationHistoryRoute.LocationsHistory = new locationHistoryRoute.LocationsHistory();
            router.use(location.index());

            let files: filesRoute.Files = new filesRoute.Files();
            router.use(files.index());

            let messages: messagesRoute.Messages = new messagesRoute.Messages();
            router.use(messages.index());

            let profile: profileRoute.Profile = new profileRoute.Profile();
            router.use(profile.index());

            let courier: courierRoute.Courier = new courierRoute.Courier();
            router.use(courier.index());

            let promo: promoRoute.Promo = new promoRoute.Promo();
            router.use(promo.index());

            let order: orderRoute.Order = new orderRoute.Order();
            router.use(order.index());

            let info: infoRoute.Info = new infoRoute.Info();
            router.use(info.index());

            let delivery: deliveryRoute.Delivery = new deliveryRoute.Delivery();
            router.use(delivery.index());

            let orderList: orderListRoute.OrderList = new orderListRoute.OrderList();
            router.use(orderList.index());

            let cahgeOrder: cahgeOrderRoute.ChangeOrder = new cahgeOrderRoute.ChangeOrder();
            router.use(cahgeOrder.index());

            let return_order: returnOrder.Return = new returnOrder.Return();
            router.use(return_order.index());

            let cards: cardsRoute.Cards = new cardsRoute.Cards();
            router.use(cards.index());

            let reviews: reviewsRoute.Reviews = new reviewsRoute.Reviews();
            router.use(reviews.index());

            let cancelOrder: cancelOrderRoute.CancelOrder = new cancelOrderRoute.CancelOrder();
            router.use(cancelOrder.index());

            let history: historyRoute.History = new historyRoute.History();
            router.use(history.index());

            let contact: contactUsRoute.ContactUs = new contactUsRoute.ContactUs();
            router.use(contact.index());

            let notifications: notificationsRoute.Notifications = new notificationsRoute.Notifications();
            router.use(notifications.index());

            let earnings: earningsRoute.Earnings = new earningsRoute.Earnings();
            router.use(earnings.index());

            let reports: reportsRoute.Reports = new reportsRoute.Reports();
            router.use(reports.index());

            return router;
        }
    }
}

export = Route;
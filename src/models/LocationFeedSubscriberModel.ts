import {Document, Schema, Model} from "mongoose";
import {ConnectionDb} from "../components/mongoose";
import {UserModel} from "./UserModel";

export interface ILocationFeedSubscriberModel extends Document {
    user?: string;
    location?: {
        type?: string;
        coordinates?: Array<number>;
    };
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;

    getPublicFields(): any;
}

export const LocationFeedSubscriberSchema: Schema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: UserModel},
    location: {
        type: {
            type: String,
            default: "Point"
        },
        coordinates: [Number]
    },
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
});

LocationFeedSubscriberSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

LocationFeedSubscriberSchema.index({location: "2dsphere"});

LocationFeedSubscriberSchema.methods.getPublicFields = function (): any {
    return {
        id: this._id,
        user: this.user,
        location: this.location
    };
};

export const LocationFeedSubscriberModel: Model<ILocationFeedSubscriberModel> =
    ConnectionDb.getInstanse().model<ILocationFeedSubscriberModel>("LocationFeedSubscriberModel", LocationFeedSubscriberSchema);
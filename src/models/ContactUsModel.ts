import {Document, Schema, Model} from "mongoose";
import {ConnectionDb} from "../components/mongoose";

export interface IContactUsModel extends Document {
    name?: string;
    email?: string;
    phone?: string;
    text?: string;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;

    getPublicFields(): any;
}

export const ContactUsSchema: Schema = new Schema({
    name: {type: String, trim: true},
    email: {type: String, trim: true},
    phone: {type: String, trim: true},
    text: {type: String, trim: true},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
});

ContactUsSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

ContactUsSchema.methods.getPublicFields = function (): any {
    return {
        id: this._id,
        name: this.name,
        email: this.email,
        phone: this.phone,
        text: this.text
    };
};

export const ContactUsModel: Model<IContactUsModel> = ConnectionDb.getInstanse().model<IContactUsModel>("ContactUsModel", ContactUsSchema);
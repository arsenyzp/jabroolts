import {Document, Schema, Model} from "mongoose";
import {ConnectionDb} from "../components/mongoose";

export interface IUserActivityLogModel extends Document {
    customers: number;
    couriers: number;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;

    getPublicFields(): any;
}

export const UserActivityLogSchema: Schema = new Schema({
    customers: {type: Number, default: 0},
    couriers: {type: Number, default: 0},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number}
});

UserActivityLogSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

UserActivityLogSchema.methods.getPublicFields = function (): any {
    return {
        id: this._id,
        customers: this.customers,
        couriers: this.couriers
    };
};

export const UserActivityLogModel: Model<IUserActivityLogModel> =
    ConnectionDb.getInstanse().model<IUserActivityLogModel>("UserActivityLogModel", UserActivityLogSchema);
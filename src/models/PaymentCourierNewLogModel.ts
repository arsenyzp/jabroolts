import {Document, Schema, Model} from "mongoose";
import {IUserModel, UserModel} from "./UserModel";
import {ConnectionDb} from "../components/mongoose";
import {IOrderModel, OrderModel} from "./OrderModel";

export const PaymentStatuses: any = {
    Init: "init",
    Success: "success",
    Fail: "fail",
};

export interface IPaymentCourierNewLogModel extends Document {
    courier?: IUserModel;
    order?: IOrderModel;
    status?: string;
    amount?: number;
    payment_type?: string;
    payment_method?: string;
    earnings?: number;
    jabroolFee?: number;
    courierDebt?: number;
    courierLimit?: number;
    courierTotal?: number;

    // date created
    created_at?: number;
    // date updated
    updated_at?: number;
}

export const PaymentCourierNewLogSchema: Schema = new Schema({
    courier: {type: Schema.Types.ObjectId, ref: UserModel},
    order: {type: Schema.Types.ObjectId, ref: OrderModel},
    status: {type: String, default: PaymentStatuses.Init},
    amount: {type: Number, default: 0},
    payment_type: {type: String, default: ""},
    payment_method: {type: String, default: ""},
    earnings: {type: Number, default: 0},
    jabroolFee: {type: Number, default: 0},
    courierDebt: {type: Number, default: 0},
    courierLimit: {type: Number, default: 0},
    courierTotal: {type: Number, default: 0},

    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
});

PaymentCourierNewLogSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

export const PaymentCourierNewLogModel: Model<IPaymentCourierNewLogModel> =
    ConnectionDb.getInstanse().model<IPaymentCourierNewLogModel>("PaymentCourierNewLogModel", PaymentCourierNewLogSchema);
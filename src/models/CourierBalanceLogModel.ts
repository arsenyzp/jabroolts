import {Document, Schema, Model} from "mongoose";
import {IUserModel, UserModel} from "./UserModel";
import {ConnectionDb} from "../components/mongoose";
import {IOrderModel, OrderModel} from "./OrderModel";

export const CourierBalanceTypes: any = {
    Promo: "promo",
    Order: "order",
    Admin: "admin",
    Jabrool: "jabrool"
};

export interface ICourierBalanceLogModel extends Document {
    user?: IUserModel;
    order?: IOrderModel;
    type?: string;
    amount?: number;

    // date created
    created_at?: number;
    // date updated
    updated_at?: number;
}

export const CourierBalanceLogModelSchema: Schema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: UserModel},
    order: {type: Schema.Types.ObjectId, ref: OrderModel},
    type: {type: String, default: CourierBalanceTypes.Order},
    amount: {type: Number, default: 0},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
});

CourierBalanceLogModelSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

export const CourierBalanceLogModel: Model<ICourierBalanceLogModel> =
    ConnectionDb.getInstanse().model<ICourierBalanceLogModel>("CourierBalanceLogModel", CourierBalanceLogModelSchema);
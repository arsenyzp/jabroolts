import {Document, Schema, Model} from "mongoose";
import {IUserModel, UserModel} from "./UserModel";
import {ConnectionDb} from "../components/mongoose";

export interface IOnlineLogModel extends Document {
    user?: IUserModel;
    duration?: number;
    type?: string;

    location?: {
        type?: string;
        coordinates?: Array<number>;
    };

    // date created
    created_at?: number;
    // date updated
    updated_at?: number;
}

export const OnlineLogSchema: Schema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: UserModel},
    duration: {type: Number, default: 0},
    type: {type: String, default: "no_orders"},

    location: {
        type: {
            type: String,
            default: "Point"
        },
        coordinates: [Number]
    },

    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
});

OnlineLogSchema.index({location: "2dsphere"}); // schema level

OnlineLogSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

export const OnlineLogModel: Model<IOnlineLogModel> = ConnectionDb.getInstanse().model<IOnlineLogModel>("OnlineLogModel", OnlineLogSchema);
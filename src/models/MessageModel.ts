import {Document, Schema, Model} from "mongoose";
import {ConnectionDb} from "../components/mongoose";
import {IUserModel, UserModel} from "./UserModel";
import {IOrderModel, OrderModel} from "./OrderModel";
import {AppConfig} from "../components/config";

export interface IMessageModel extends Document {
    mid?: string;
    sender: IUserModel;
    recipient: IUserModel;
    order?: IOrderModel;
    text?: string;
    image?: string;
    is_read?: boolean;
    is_view?: boolean;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;

    getPublicFields(): any;
}

export const MessageSchema: Schema = new Schema({
    mid: {type: String},
    sender: {type: Schema.Types.ObjectId, ref: UserModel},
    recipient: {type: Schema.Types.ObjectId, ref: UserModel},
    order: {type: Schema.Types.ObjectId, ref: OrderModel},
    text: {type: String},
    image: {type: String},
    is_read: {type: Boolean, default: false},
    is_view: {type: Boolean, default: false},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
});

MessageSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

MessageSchema.methods.getPublicFields = function (): any {
    return {
        id: this._id,
        mid: this.mid,
        is_read: this.is_read,
        is_view: this.is_view,
        sender: this.sender ? this.sender.toString() : "",
        recipient: this.recipient ? this.recipient.toString() : "",
        order: this.order ? this.order.toString() : "",
        text: this.text ? this.text : "",
        image: this.image ? AppConfig.getInstanse().get("base_url") + AppConfig.getInstanse().get("urls:chat") + "/" + this.image : "",
        created_at: this.created_at
    };
};

export const MessageModel: Model<IMessageModel> = ConnectionDb.getInstanse().model<IMessageModel>("MessageModel", MessageSchema);
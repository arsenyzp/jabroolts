import {Document, Schema, Model} from "mongoose";
import {ConnectionDb} from "../components/mongoose";

export interface ICarTypeModelModel extends Document {
    name?: string;
    size?: number;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;

    getPublicFields(): any;
}

export const CarTypeSchema: Schema = new Schema({
    name: {type: String, trim: true},
    size: {type: Number, default: 0},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
});

CarTypeSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

CarTypeSchema.methods.getPublicFields = function (): any {
    return {
        id: this._id,
        name: this.name,
        size: this.size
    };
};

export const CarTypeModel: Model<ICarTypeModelModel> = ConnectionDb.getInstanse().model<ICarTypeModelModel>("CarTypeModel", CarTypeSchema);
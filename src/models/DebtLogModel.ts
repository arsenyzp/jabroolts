import {Document, Schema, Model} from "mongoose";
import {ConnectionDb} from "../components/mongoose";

export interface IDebtLogModel extends Document {
    value: number;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;

    getPublicFields(): any;
}

export const DebtLogSchema: Schema = new Schema({
    value: {type: Number, default: 0},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number}
});

DebtLogSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

DebtLogSchema.methods.getPublicFields = function (): any {
    return {
        id: this._id,
        name: this.name
    };
};

export const DebtLogModel: Model<IDebtLogModel> = ConnectionDb.getInstanse().model<IDebtLogModel>("DebtLogModel", DebtLogSchema);
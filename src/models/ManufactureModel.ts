import {Document, Schema, Model} from "mongoose";
import {ConnectionDb} from "../components/mongoose";
import { ICarTypeModelModel, CarTypeModel } from "./CarTypeModel";

export interface IManufactureModel extends Document {
    name: string;
    types: Array<ICarTypeModelModel>;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;

    getPublicFields(): any;
}

export const ManufactureSchema: Schema = new Schema({
    name: {type: String, trim: true},
    types: [
        {type: Schema.Types.ObjectId, ref: CarTypeModel}
    ],
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number}
});

ManufactureSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

ManufactureSchema.methods.getPublicFields = function (): any {
    return {
        id: this._id,
        name: this.name,
        type: this.types
    };
};

export const ManufactureModel: Model<IManufactureModel> = ConnectionDb.getInstanse().model<IManufactureModel>("ManufactureModel", ManufactureSchema);
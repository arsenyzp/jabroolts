import {Document, Schema, Model} from "mongoose";
import {ConnectionDb} from "../components/mongoose";
import {BankModel} from "./BankModel";
import {OrderModel} from "./OrderModel";
import {BankTypeModel} from "./BankTypeModel";
import {CarTypeModel} from "./CarTypeModel";
import {ManufactureModel} from "./ManufactureModel";
import {AppConfig} from "../components/config";
import {JidGenerator} from "../components/jabrool/JidGenerator";

const md5: (str: string) => string = require("blueimp-md5");

export const UserTypes: any = {
    User: "user",
    Business: "business"
};

export const UserStatus: any = {
    Active: "active",
    Review: "review",
    Deleted: "deleted"
};

export const UserRoles: any = {
    dev: "dev",
    admin: "admin",
    manager: "manager",
    user: "user"
};

export interface ILicenseModel {
    image?: string;
    number?: string;
    name?: string;
    issue_date?: number;
    expiry_date?: number;
    country_code?: string;
}

export interface IUserBankModel {
    type: string;
    bank: string;
    country_code: string;
    branch: string;
    account_number: string;
    holder_name: string;
}

export interface IVehicleModel {
    year: string;
    model: string;
    type: string;
    number: string;

    image_front: string;
    image_back: string;
    image_side: string;
    fk_company: string;

    images_insurance: Array<string>;
}


export interface IUserModel extends Document {
    jabroolid?: string;
    // email
    email?: string;
    // first_name
    first_name?: string;
    // last_name
    last_name?: string;
    // phone
    phone?: string;
    // save phone before confirm
    tmp_phone?: string;
    // avatar
    // avatar
    avatar?: string;
    // password
    password?: string;
    // password
    new_password?: string;
    // password
    tmp_password?: string;
    // token
    token?: string;
    // status
    status?: string;
    // visible on map
    visible?: boolean;
    // balance
    balance?: number;
    // balance courier
    courierLimit?: number;
    // balance courier
    jabroolFee?: number;
    // role
    role?: string;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;
    // date last login
    login_at?: number;
    // push id
    push_device_ios?: Array<string>;
    push_device_android?: Array<string>;
    // socket id
    socket_ids?: Array<string>;
    web_socket_ids?: Array<string>;
    socket_admin_ids?: Array<string>;
    tz?: number;
    local?: string;
    duid?: string;
    confirm_sms_code?: string;
    confirm_phone?: boolean;
    confirm_profile?: boolean;
    confirm_email_code?: string;
    confirm_email?: boolean;
    license?: ILicenseModel;
    vehicle?: IVehicleModel;
    bank?: IUserBankModel;
    referrer?: string;
    referals?: Array<string>;
    current_orders?: Array<Schema.Types.ObjectId>;
    in_progress?: boolean;
    deleted?: boolean;
    accept_in_progress?: boolean;
    bt_customer_id?: string;
    qo_customer_id?: number;
    pay_type?: string;
    defaultUniqueNumberIdentifier?: string;
    tmp_code?: string;
    rating?: number;
    review_count?: number;
    unread_count?: number;
    unread_messages?: number;
    location?: {
        type?: string;
        coordinates?: Array<number>;
    };
    type?: string;
    couriers?: Array<string>;

    genToken(): string;

    comparePassword(password: string): boolean;

    setPassword(password: string): void;

    getApiFields(): any;

    getSiteFields(): any;

    getApiPublicFields(): any;

    generateConfirmSmsCode(): string;

    generateConfirmEmail(): string;

    getBank(): any;

    getVehicle(): IVehicleModel;

    getLicense(): ILicenseModel;

    generateJId(): Promise<{}>;
}

export const UserSchema: Schema = new Schema({
    jabroolid: {type: String, index: true, unique: [true, "This id already exists"], sparse: true},
    // email
    email: {type: String, index: true, unique: [true, "This email already exists"], sparse: true},
    // first_name
    first_name: {type: String, trim: true},
    // last_name
    last_name: {type: String, trim: true},
    // phone
    phone: {type: String, trim: true},
    // phone
    tmp_phone: {type: String, trim: true},
    // avatar
    avatar: {type: String, trim: true},
    // password
    password: {type: String, required: [true, "Password is require"]},
    // password
    tmp_password: {type: String},
    // token
    token: {type: String, trim: true, default: null},
    // status
    status: {type: String, default: UserStatus.Review, enum: [UserStatus.Active, UserStatus.Review, UserStatus.Deleted]},
    // visible on map
    visible: {type: Boolean, default: false},
    // balance
    balance: {type: Number, default: 0},
    // balance courier
    courierLimit: {type: Number, default: 0},
    // jabrool fee courier
    jabroolFee: {type: Number, default: 0},
    // role
    role: {
        type: String, default: UserRoles.user, enum: [
            UserRoles.admin,
            UserRoles.user,
            UserRoles.dev
        ]
    },
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
    // date last login
    login_at: {type: Number},
    // push id
    push_device_ios: [
        {type: String, default: null}
    ],
    push_device_android: [
        {type: String, default: null}
    ],
    // socket id
    socket_ids: [
        {type: String}
    ],
    web_socket_ids: [
        {type: String}
    ],
    socket_admin_ids: [
        {type: String}
    ],

    tz: {type: Number, default: 0},
    local: {type: String, default: "en"},
    duid: {type: String},

    confirm_sms_code: {type: String},
    confirm_phone: {type: Boolean, default: false},
    confirm_profile: {type: Boolean, default: false},

    confirm_email_code: {type: String},
    confirm_email: {type: Boolean, default: false},

    license: {
        image: {type: String, default: ""},
        number: {type: String, default: ""},
        name: {type: String, default: ""},
        issue_date: {type: Number},
        expiry_date: {type: Number},
        country_code: {type: String, default: ""}
    },

    vehicle: {
        year: {type: String, default: ""},
        model: {type: Schema.Types.ObjectId, ref: ManufactureModel},
        type: {type: Schema.Types.ObjectId, ref: CarTypeModel},
        number: {type: String, default: ""},

        image_front: {type: String, default: ""},
        image_back: {type: String, default: ""},
        image_side: {type: String, default: ""},
        fk_company: {type: String, default: ""},

        images_insurance: [
            {type: String}
        ]
    },

    bank: {
        type: {type: Schema.Types.ObjectId, ref: BankTypeModel},
        bank: {type: Schema.Types.ObjectId, ref: BankModel},
        country_code: {type: String},
        branch: {type: String},
        account_number: {type: String},
        holder_name: {type: String}
    },

    referrer: {type: String},

    referals: [
        {type: String}
    ],

    current_orders: [
        {type: Schema.Types.ObjectId, ref: OrderModel}
    ],

    in_progress: {type: Boolean, default: false},
    deleted: {type: Boolean, default: false},
    accept_in_progress: {type: Boolean, default: false},

    bt_customer_id: {type: String, default: ""},
    qo_customer_id: {type: Number, default: -1},
    pay_type: {type: String, default: "cash"},
    defaultUniqueNumberIdentifier: {type: String, default: ""},
    tmp_code: {type: String, default: "#@!@"},

    rating: {type: Number, default: 5},
    review_count: {type: Number, default: 0},
    unread_count: {type: Number, default: 0},
    unread_messages: {type: Number, default: 0},

    location: {
        type: {
            type: String,
            default: "Point"
        },
        coordinates: [Number]
    },
    type: {type: String, default: UserTypes.User},
    couriers: [
        {type: String}
    ]
});

UserSchema.index({location: "2dsphere"}); // schema level

/**
 * Generate new Jabrool id
 */
UserSchema.methods.generateJId = function (): Promise<{}> {
    return JidGenerator.gen(this);
};

/**
 * Generate token
 * @returns {*}
 */
UserSchema.methods.genToken = function (): string {
    let d: Date = new Date();
    this.token = md5((Math.random() * (999999 - 111111)) + "token") + md5(d.getTime() + this._id.toString()) + this._id.toString();
    return this.token;
};

/**
 * Generate password
 * @returns {number}
 */
UserSchema.methods.gen = function (): string {
    let rand: number = 111111 + Math.random() * (999999 - 111111);
    rand = Math.round(rand);
    this.setPassword(rand + "");
    return rand + "";
};

/**
 * Set password
 * @param password
 */
UserSchema.methods.setPassword = function (password: string): void {
    this.password = md5(password);
};

UserSchema.methods.comparePassword = function (password: string): boolean {
    return this.password === md5(password);
};

UserSchema.methods.generateConfirmSmsCode = function (): string {
    this.confirm_sms_code = Math.floor(111111 + Math.random() * (999999 - 111111));
    return this.confirm_sms_code;
};

UserSchema.methods.generateConfirmEmail = function (): string {
    this.confirm_email_code = Math.floor(111111 + Math.random() * (999999 - 111111)) + this.token;
    return this.confirm_email_code;
};

UserSchema.methods.getApiFields = function (): any {

    let complete_license: boolean = !!(this.license && this.license.number && this.license.number !== "");
    let complete_bank: boolean = !!(this.bank && this.bank.account_number && this.bank.account_number !== "");
    let complete_vehicle: boolean = !!(this.vehicle && this.vehicle.number && this.vehicle.number !== "");

    let avatar: string = this.avatar ?
        AppConfig.getInstanse().get("base_url") + AppConfig.getInstanse().get("urls:user_avatars") + "/" + this.avatar :
        AppConfig.getInstanse().get("base_url") + "/images/empty.png";

    return {
        id: this._id,
        token: this.token,
        email: this.email !== undefined ? this.email.toString() : "",
        phone: this.phone !== undefined ? this.phone.toString() : "",
        first_name: this.first_name !== undefined ? this.first_name.toString() : "",
        last_name: this.last_name !== undefined ? this.last_name.toString() : "",
        jabroolid: this.jabroolid !== undefined ? this.jabroolid.toString() : "",
        avatar: avatar,
        created_at: this.created_at,
        updated_at: this.updated_at,
        status: this.status,
        visible: this.visible,
        in_progress: this.in_progress,
        accept_in_progress: this.accept_in_progress,
        pay_type: this.pay_type,
        rating: this.rating,
        unread_count: this.unread_count,
        review_count: this.review_count,
        confirm_email: this.confirm_email,
        confirm_phone: this.confirm_phone,
        complete_license: complete_license,
        complete_bank: complete_bank,
        complete_vehicle: complete_vehicle,
        unread_messages: this.unread_messages
    };

};

UserSchema.methods.getSiteFields = function (): any {

    let complete_license: boolean = !!(this.license && this.license.number && this.license.number !== "");
    let complete_bank: boolean = !!(this.bank && this.bank.account_number && this.bank.account_number !== "");
    let complete_vehicle: boolean = !!(this.vehicle && this.vehicle.number && this.vehicle.number !== "");

    let avatar: string = this.avatar ?
        AppConfig.getInstanse().get("base_url") + AppConfig.getInstanse().get("urls:user_avatars") + "/" + this.avatar :
        AppConfig.getInstanse().get("base_url") + "/images/empty.png";

    return {
        id: this._id,
        token: this.token,
        email: this.email !== undefined ? this.email.toString() : "",
        phone: this.phone !== undefined ? this.phone.toString() : "",
        first_name: this.first_name !== undefined ? this.first_name.toString() : "",
        last_name: this.last_name !== undefined ? this.last_name.toString() : "",
        jabroolid: this.jabroolid !== undefined ? this.jabroolid.toString() : "",
        avatar: avatar,
        created_at: this.created_at,
        updated_at: this.updated_at,
        status: this.status,
        visible: this.visible,
        in_progress: this.in_progress,
        accept_in_progress: this.accept_in_progress,
        pay_type: this.pay_type,
        rating: this.rating,
        unread_count: this.unread_count,
        review_count: this.review_count,
        confirm_email: this.confirm_email,
        confirm_phone: this.confirm_phone,
        complete_license: complete_license,
        complete_bank: complete_bank,
        complete_vehicle: complete_vehicle,
        unread_messages: this.unread_messages,
        vehicle: this.vehicle
    };

};

UserSchema.methods.getApiPublicFields = function (): any {

    let complete_license: boolean = !!(this.license && this.license.number && this.license.number !== "");
    let complete_bank: boolean = !!(this.bank && this.bank.account_number && this.bank.account_number !== "");
    let complete_vehicle: boolean = !!(this.vehicle && this.vehicle.number && this.vehicle.number !== "");

    let avatar: string = this.avatar ?
        AppConfig.getInstanse().get("base_url") + AppConfig.getInstanse().get("urls:user_avatars") + "/" + this.avatar :
        AppConfig.getInstanse().get("base_url") + "/images/empty.png";

    return {
        id: this._id,
        email: this.email !== undefined ? this.email.toString() : "",
        phone: this.phone !== undefined ? this.phone.toString() : "",
        first_name: this.first_name !== undefined ? this.first_name.toString() : "",
        last_name: this.last_name !== undefined ? this.last_name.toString() : "",
        jabroolid: this.jabroolid !== undefined ? this.jabroolid.toString() : "",
        avatar: avatar,
        created_at: this.created_at,
        updated_at: this.updated_at,
        status: this.status,
        visible: this.visible,
        in_progress: this.in_progress,
        accept_in_progress: this.accept_in_progress,
        rating: this.rating,
        unread_count: this.unread_count,
        review_count: this.review_count,
        confirm_email: this.confirm_email,
        confirm_phone: this.confirm_phone,
        complete_license: complete_license,
        complete_bank: complete_bank,
        complete_vehicle: complete_vehicle,
        unread_messages: this.unread_messages
    };

};

UserSchema.methods.getBank = function (): any {
    return this.bank;
};

UserSchema.methods.getLicense = function (): any {
    return this.license;
};

UserSchema.methods.getVehicle = function (): IVehicleModel {

    let image_front: string = this.vehicle.image_front ?
        AppConfig.getInstanse().get("base_url") + AppConfig.getInstanse().get("urls:user_vehicle") + "/" + this.vehicle.image_front :
        AppConfig.getInstanse().get("base_url") + "/images/empty.png";

    let image_back: string = this.vehicle.image_back ?
        AppConfig.getInstanse().get("base_url") + AppConfig.getInstanse().get("urls:user_vehicle") + "/" + this.vehicle.image_back :
        AppConfig.getInstanse().get("base_url") + "/images/empty.png";

    let image_side: string = this.vehicle.image_side ?
        AppConfig.getInstanse().get("base_url") + AppConfig.getInstanse().get("urls:user_vehicle") + "/" + this.vehicle.image_side :
        AppConfig.getInstanse().get("base_url") + "/images/empty.png";

    let images_insurance: Array<string> = [];
    for (let i: number = 0; i < this.vehicle.images_insurance.length; i++) {
        images_insurance.push(AppConfig.getInstanse().get("base_url") +
            AppConfig.getInstanse().get("urls:user_vehicle") +
            "/" + this.vehicle.images_insurance[i]);
    }

    return {
        year: this.vehicle.year,
        model: this.vehicle.model,
        type: this.vehicle.type,
        number: this.vehicle.number,

        image_front: image_front,
        image_back: image_back,
        image_side: image_side,
        fk_company: this.vehicle.fk_company,

        images_insurance: images_insurance
    };
};

UserSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();

    if (!this.jabroolid) {
        JidGenerator.gen(this).then(() => {
            next();
        }).catch(err => {
            next(err);
        });
    } else {
        next();
    }
});

UserSchema.methods.fullName = function (): string {
    return (this.first_name.trim() + " " + this.last_name.trim());
};

export const UserModel: Model<IUserModel> = ConnectionDb.getInstanse().model<IUserModel>("UserModel", UserSchema);

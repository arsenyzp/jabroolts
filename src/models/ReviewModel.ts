import {Document, Schema, Model} from "mongoose";
import {ConnectionDb} from "../components/mongoose";
import {IUserModel, UserModel} from "./UserModel";
import {IOrderModel, OrderModel} from "./OrderModel";

export interface IReviewModel extends Document {
    owner?: IUserModel;
    user?: IUserModel;
    order?: IOrderModel;
    text?: string;
    rate?: number;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;

    getPublicFields(): any;
}

export const ReviewModelSchema: Schema = new Schema({
    owner: {type: Schema.Types.ObjectId, ref: UserModel},
    user: {type: Schema.Types.ObjectId, ref: UserModel},
    order: {type: Schema.Types.ObjectId, ref: OrderModel},
    text: {type: String},
    rate: {type: Number},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
});

ReviewModelSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

ReviewModelSchema.methods.getPublicFields = function (): any {
    return {
        id: this._id,
        data_id: this.ref_id,
        type: this.type,
        text: this.text,
        created_at: this.created_at
    };
};

export const ReviewModel: Model<IReviewModel> =
    ConnectionDb.getInstanse().model<IReviewModel>("ReviewModel", ReviewModelSchema);
import {Document, Schema, Model} from "mongoose";
import {UserModel} from "./UserModel";
import {ConnectionDb} from "../components/mongoose";

export interface IPromoCodModel extends Document {
    code?: string;
    date_expiry?: number;
    amount?: number;
    is_used?: boolean;
    is_blocked?: boolean;
    owner?: Schema.Types.ObjectId;
    forCourier?: boolean;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;
}

export const PromoCodSchema: Schema = new Schema({
    code: {type: String, trim: true},
    date_expiry: {type: Number},
    amount: {type: Number},
    is_used: {type: Boolean, default: false},
    is_blocked: {type: Boolean, default: false},
    owner: {type: Schema.Types.ObjectId, ref: UserModel},
    forCourier: {type: Boolean, default: false},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
});

PromoCodSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

export const PromoCodModel: Model<IPromoCodModel> = ConnectionDb.getInstanse().model<IPromoCodModel>("PromoCodModel", PromoCodSchema);
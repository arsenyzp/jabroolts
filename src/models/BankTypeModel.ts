import {Document, Schema, Model} from "mongoose";
import {ConnectionDb} from "../components/mongoose";


export interface IBankTypeModel extends Document {
    name?: string;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;

    getPublicFields(): any;
}

export const BankTypeSchema: Schema = new Schema({
    name: {type: String, trim: true},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number}
});

BankTypeSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

BankTypeSchema.methods.getPublicFields = function (): any {
    return {
        id: this._id,
        name: this.name
    };
};

export const BankTypeModel: Model<IBankTypeModel> = ConnectionDb.getInstanse().model<IBankTypeModel>("BankTypeModel", BankTypeSchema);
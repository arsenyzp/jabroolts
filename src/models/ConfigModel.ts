import {Document, Schema, Model} from "mongoose";
import {ConnectionDb} from "../components/mongoose";

export interface IConfigModel extends Document {
    name?: string;
    code?: string;
    value?: string;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;
}

export const ConfigSchema: Schema = new Schema({
    name: {type: String, trim: true},
    code: {type: String, trim: true},
    value: {type: String, trim: true},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
});

ConfigSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
export const ConfigModel: Model<IConfigModel> = ConnectionDb.getInstanse().model<IConfigModel>("ConfigModel", ConfigSchema);
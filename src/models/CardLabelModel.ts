import {Document, Schema, Model} from "mongoose";
import {ConnectionDb} from "../components/mongoose";
import {IUserModel, UserModel} from "./UserModel";

export interface ICardLabelModel extends Document {
    user?: IUserModel;
    uniqueNumberIdentifier?: string;
    label?: string;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;

    getPublicFields(): any;
}

export const CardLabelSchema: Schema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: UserModel},
    uniqueNumberIdentifier: {type: String, trim: true},
    label: {type: String, trim: true},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
});

CardLabelSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

CardLabelSchema.methods.getPublicFields = function (): any {
    return {
        id: this._id,
        label: this.label
    };
};

export const CardLabelModel: Model<ICardLabelModel> =
    ConnectionDb.getInstanse().model<ICardLabelModel>("CardLabelModel", CardLabelSchema);
import {Document, Schema, Model} from "mongoose";
import {ConnectionDb} from "../components/mongoose";
import {IUserModel, UserModel} from "./UserModel";

export interface IHistoryLocationModel extends Document {
    user: IUserModel;
    address: string;
    lat: number;
    lon: number;
    favorite?: boolean;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;

    getPublicFields(): any;
}

export const HistoryLocationSchema: Schema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: UserModel},
    address: {type: String, trim: true},
    lat: {type: Number},
    lon: {type: Number},
    favorite: {type: Boolean, default: false},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
});

HistoryLocationSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

HistoryLocationSchema.methods.getPublicFields = function (): any {
    return {
        id: this._id,
        address: this.address,
        lat: this.lat,
        lon: this.lon,
        favorite: this.favorite,
        created_at: this.created_at
    };
};

export const HistoryLocationModel: Model<IHistoryLocationModel> =
    ConnectionDb.getInstanse().model<IHistoryLocationModel>("HistoryLocationModel", HistoryLocationSchema);
import {Document, Schema, Model} from "mongoose";
import {IUserModel, UserModel} from "./UserModel";
import {PromoCodModel} from "./PromoCodModel";
import {ConnectionDb} from "../components/mongoose";
import {AppConfig} from "../components/config";
import {MessageModel} from "./MessageModel";
import {SocketServer} from "../components/SocketServer";

// order status
export const OrderStatuses: any = {
    New: "new", // только что созданная заявка - cost
    WaitPickUp: "wait_pick_up", // заявку принял курьер и она ожидает забора - cost
    WaitCustomerAccept: "wait_customer_accept", // курьер подтвердил данные зявки, но ждём подтверждения курьера - cost
    WaitCustomerPayment: "wait_customer_payment", // кастомер подтвердил но не оплатил - cost
    WaitCourierPickUpConfirmCode: "wait_courier_pick_up_confirm_code", // ожидание ввода курьером кода подтверждения pick up - cash и serviceFee
    WaitCourierDeliveryConfirmCode: "wait_courier_delivery_confirm_code", // ожидание ввода курьером кода подтверждения доставки
    WaitCourierReturnConfirmCode: "wait_courier_return_confirm_code", // ожидание ввода курьером кода подтверждения возврата
    InProgress: "in_progress",
    ReturnInProgress: "return_in_progress",
    WaitAcceptDelivery: "wait_accept_delivery", // return cost
    WaitAcceptReturn: "wait_accept_return", // курьер приехал, но кастомер ещё не оплатил возврат или не запросил код  - return cost
    Canceled: "canceled",
    Finished: "finished",
    Missed: "missed"
};

// order types
export const OrderTypes: any = {
    Jabrool: "jabrool",
    Express: "express"
};

// order sizes
export const OrderSizes: any = {
    small: "small",
    medium: "medium",
    large: "large"
};

export const OrderPayers: any = {
    Requester: "requester",
    Receiver: "receiver",
};

export const OrderPayTypes: any = {
    Cash: "cash",
    Card: "card",
    Bonus: "bonus"
};

export interface IOrderModel extends Document {
    orderId: string;
    owner: IUserModel;
    owner_name?: string;
    owner_contact?: string;
    courier?: IUserModel;
    recipient?: IUserModel;
    code?: string;
    weight?: number;
    small_package_count?: number;
    medium_package_count?: number;
    large_package_count?: number;
    route?: number;
    cost?: number;
    returnCost?: number;
    serviceFee?: number;
    userCredit?: number;
    card?: number;
    cash?: number;
    bonus?: number;
    recipient_name?: string;
    recipient_contact?: string;
    owner_address?: string;
    recipient_address?: string;
    owner_comment?: string;
    recipient_comment?: string;
    owner_map_url?: string;
    recipient_map_url?: string;
    size?: string;
    photos?: Array<string>;
    // status
    status?: string;
    type?: string;
    pay_type?: string;
    use_bonus?: boolean;
    paid?: boolean;
    paid_return?: boolean;
    owner_canceled?: boolean;
    owner_location?: {
        type?: string;
        coordinates?: Array<number>;
    };
    recipient_location?: {
        type?: string;
        coordinates?: Array<number>;
    };

    notified_couriers?: Array<Schema.Types.ObjectId>;
    canceled_couriers?: Array<Schema.Types.ObjectId>;
    declined_couriers?: Array<Schema.Types.ObjectId>;

    confirm_pickup_code?: string;
    confirm_delivery_code?: string;
    confirm_start_return_code?: string;
    confirm_return_code?: string;

    accept_at?: number;
    start_at?: number;
    end_at?: number;

    unread_messages_owner?: number;
    unread_messages_courier?: number;

    near_courier_notification?: number;

    bt_pay_id?: string;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;

    getPublicFields: (user?: IUserModel) => any;
    getApiFields: (user?: IUserModel) => any;
    getApiCourierFields: (user?: IUserModel) => any;
}

export const OrderSchema: Schema = new Schema({
    orderId: {type: String},
    owner: {type: Schema.Types.ObjectId, ref: UserModel},
    owner_name: {type: String},
    owner_contact: {type: String},
    courier: {type: Schema.Types.ObjectId, ref: UserModel},
    recipient: {type: Schema.Types.ObjectId, ref: UserModel},
    code: {type: Schema.Types.ObjectId, ref: PromoCodModel},
    weight: {type: Number, default: 0},
    small_package_count: {type: Number, default: 0},
    medium_package_count: {type: Number, default: 0},
    large_package_count: {type: Number, default: 0},
    route: {type: Number, default: 0},
    cost: {type: Number, default: 0},
    returnCost: {type: Number, default: 0},
    serviceFee: {type: Number, default: 0},
    userCredit: {type: Number, default: 0},
    card: {type: Number, default: 0},
    cash: {type: Number, default: 0},
    bonus: {type: Number, default: 0},
    recipient_name: {type: String},
    recipient_contact: {type: String},
    owner_address: {type: String},
    recipient_address: {type: String},
    owner_comment: {type: String},
    recipient_comment: {type: String},
    owner_map_url: {type: String},
    recipient_map_url: {type: String},
    size: {
        type: String, enum: [
            OrderSizes.small,
            OrderSizes.medium,
            OrderSizes.large
        ]
    },
    photos: [
        {type: String}
    ],
    // status
    status: {
        type: String, default: OrderStatuses.New, enum: [
            OrderStatuses.New,
            OrderStatuses.WaitPickUp,
            OrderStatuses.WaitCustomerAccept,
            OrderStatuses.WaitCustomerPayment,
            OrderStatuses.WaitCourierPickUpConfirmCode,
            OrderStatuses.WaitCourierDeliveryConfirmCode,
            OrderStatuses.WaitCourierReturnConfirmCode,
            OrderStatuses.InProgress,
            OrderStatuses.ReturnInProgress,
            OrderStatuses.WaitAcceptDelivery,
            OrderStatuses.WaitAcceptReturn,
            OrderStatuses.Canceled,
            OrderStatuses.Finished,
            OrderStatuses.Missed
        ]
    },
    // type
    type: {
        type: String, default: OrderTypes.Jabrool, enum: [
            OrderTypes.Jabrool,
            OrderTypes.Express
        ]
    },

    pay_type: {
        type: String, default: OrderPayTypes.Card, enum: [
            OrderPayTypes.Card,
            OrderPayTypes.Cash
        ]
    },

    use_bonus: {type: Boolean, default: false},

    paid: {type: Boolean, default: false},
    paid_return: {type: Boolean, default: false},
    owner_canceled: {type: Boolean, default: false},

    owner_location: {
        type: {
            type: String,
            default: "Point"
        },
        coordinates: [Number]
    },

    recipient_location: {
        type: {
            type: String,
            default: "Point"
        },
        coordinates: [Number]
    },

    notified_couriers: [
        {type: Schema.Types.ObjectId, ref: UserModel}
    ],
    declined_couriers: [
        {type: Schema.Types.ObjectId, ref: UserModel}
    ],
    canceled_couriers: [
        {type: Schema.Types.ObjectId, ref: UserModel}
    ],

    confirm_pickup_code: {type: String},
    confirm_delivery_code: {type: String},
    confirm_start_return_code: {type: String},
    confirm_return_code: {type: String},

    accept_at: {type: Number},
    start_at: {type: Number},
    end_at: {type: Number},

    unread_messages_owner: {type: Number, default: 0},
    unread_messages_courier: {type: Number, default: 0},

    near_courier_notification: {type: Number, default: 0},
    near_owner_notification: {type: Number, default: 0},

    bt_pay_id: {type: String, default: ""},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number}
});

OrderSchema.index({owner_location: "2dsphere"}); // schema level
OrderSchema.index({recipient_location: "2dsphere"}); // schema level

OrderSchema.pre("save", function (next: any): void {
    console.log("-------OrderSchema.pre save----------");
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();

    if ([OrderStatuses.Canceled, OrderStatuses.Finished].indexOf(this.status) >= 0) {
        MessageModel
            .remove({order: this._id})
            .then( _ =>  {
                console.log("Order messages update");
            })
            .catch(err => {
                console.log(err);
            });
    }
    next();
});

OrderSchema.post("findOneAndUpdate", function (doc: IOrderModel): void {
    if (doc !== null) {
        console.log("-------OrderSchema.post findOneAndUpdate---------- " + doc.status);

        if ([OrderStatuses.Canceled, OrderStatuses.Finished].indexOf(doc.status) >= 0) {
            MessageModel
                .remove({order: doc._id})
                .then( _ =>  {
                    console.log("Order messages update");

                    if (doc.courier) {
                        MessageModel.count( {recipient: doc.courier, is_view: false})
                            .then(c => {
                                UserModel
                                    .findOneAndUpdate(
                                        {_id: doc.courier},
                                        {unread_messages: c},
                                        {new: true}
                                    )
                                    .then(u => {
                                        SocketServer.emit([doc.courier.toString()], "unread_count", {unread_count: c});
                                    })
                                    .catch(err => {
                                        console.log(err);
                                    });
                            })
                            .catch(err => {
                                console.log(err);
                            });
                    }

                    MessageModel.count( {recipient: doc.owner, is_view: false})
                        .then(c => {
                            UserModel
                                .findOneAndUpdate(
                                    {_id: doc.owner},
                                    {unread_messages: c},
                                    {new: true}
                                )
                                .then(u => {
                                    SocketServer.emit([doc.owner.toString()], "unread_count", {unread_count: c});
                                })
                                .catch(err => {
                                    console.log(err);
                                });
                        })
                        .catch(err => {
                            console.log(err);
                        });
                })
                .catch(err => {
                    console.log(err);
                });
        }
    }
});

OrderSchema.methods.getPublicFields = function (user: IUserModel): any {
    let photos: Array<string> = [];
    if (this.photos && this.photos.length > 0) {
        for (let i: number = 0; i < this.photos.length; i++) {
            photos.push(AppConfig.getInstanse().get("base_url") + AppConfig.getInstanse().get("urls:order_files") + "/" + this.photos[i]);
        }
    }
    let unread_messages: number = 0;
    if (user && user._id) {
        if (this.owner && this.owner._id) {
            if (this.owner._id.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_owner;
            }
        } else if (this.owner) {
            if (this.owner.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_owner;
            }
        }

        if (this.courier && this.courier._id) {
            if (this.courier._id.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_courier;
            }
        } else if (this.courier) {
            if (this.courier.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_courier;
            }
        }
    }

    return {
        id: this._id,
        orderId: this.orderId,
        // owner: (!!this.owner && !!this.owner._id) ? this.owner.getApiPublicFields() : this.owner,
        // courier: (!!this.courier && !!this.courier._id) ? this.courier.getApiPublicFields() : this.courier,
        owner: (!!this.owner && !!this.owner._id) ? this.owner._id : this.owner,
        courier: (!!this.courier && !!this.courier._id) ? this.courier._id : this.courier,
        weight: this.weight,
        small_package_count: this.small_package_count,
        medium_package_count: this.medium_package_count,
        large_package_count: this.large_package_count,
        size: this.size,
        photos: photos,
        status: this.status,
        type: this.type,
        owner_name: this.owner_name,
        owner_contact: this.owner_contact,
        owner_address: this.owner_address,
        owner_comment: this.owner_comment,
        owner_location: this.owner_location.coordinates,
        recipient_address: this.recipient_address,
        recipient_comment: this.recipient_comment,
        recipient_location: this.recipient_location.coordinates,
        recipient_name: this.recipient_name,
        recipient_contact: this.recipient_contact,
        created_at: this.created_at,
        start_at: this.start_at,
        end_at: this.end_at,
        paid: this.paid,
        paid_return: this.paid_return,
        pay_type: this.pay_type,
        route: this.route,
        cost: this.cost,
        serviceFee: this.serviceFee,
        userCredit: this.userCredit * -1,
        returnCost: this.returnCost,
        card: this.card,
        cash: this.cash,
        bonus: this.bonus,
        updated_at: this.updated_at,
        use_bonus: this.use_bonus,
        owner_canceled: this.owner_canceled,
        unread_messages: unread_messages
    };
};

OrderSchema.methods.getApiFields = function (user: IUserModel): any {
    let photos: Array<string> = [];
    if (this.photos && this.photos.length > 0) {
        for (let i: number = 0; i < this.photos.length; i++) {
            photos.push(AppConfig.getInstanse().get("base_url") + AppConfig.getInstanse().get("urls:order_files") + "/" + this.photos[i]);
        }
    }
    let unread_messages: number = 0;
    if (user && user._id) {
        if (this.owner && this.owner._id) {
            if (this.owner._id.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_owner;
            }
        } else if (this.owner) {
            if (this.owner.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_owner;
            }
        }

        if (this.courier && this.courier._id) {
            if (this.courier._id.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_courier;
            }
        } else if (this.courier) {
            if (this.courier.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_courier;
            }
        }
    }

    return {
        order: {
            id: this._id,
            orderId: this.orderId,
            weight: this.weight,
            small_package_count: this.small_package_count,
            medium_package_count: this.medium_package_count,
            large_package_count: this.large_package_count,
            size: this.size,
            photos: photos,
            status: this.status,
            type: this.type,
            owner_name: this.owner_name,
            owner_contact: this.owner_contact,
            owner_address: this.owner_address,
            owner_comment: this.owner_comment,
            owner_location: this.owner_location.coordinates,
            recipient_address: this.recipient_address,
            recipient_comment: this.recipient_comment,
            recipient_location: this.recipient_location.coordinates,
            recipient_name: this.recipient_name,
            recipient_contact: this.recipient_contact,
            created_at: this.created_at,
            start_at: this.start_at,
            end_at: this.end_at,
            paid: this.paid,
            paid_return: this.paid_return,
            pay_type: this.pay_type,
            route: this.route,
            cost: this.cost,
            serviceFee: this.serviceFee,
            userCredit: this.userCredit * -1,
            returnCost: this.returnCost,
            card: this.card,
            cash: this.cash,
            bonus: this.bonus,
            updated_at: this.updated_at,
            use_bonus: this.use_bonus,
            owner_canceled: this.owner_canceled,
            unread_messages: unread_messages,
            owner: (!!this.owner && !!this.owner._id) ? this.owner._id.toString() : this.owner,
            courier: (!!this.courier && !!this.courier._id) ? this.courier._id.toString() : this.courier,
            recipient: (!!this.recipient && !!this.recipient._id) ? this.recipient._id.toString() : this.recipient
        },
        owner: (!!this.owner && !!this.owner._id) ? this.owner.getApiPublicFields() : this.owner,
        courier: (!!this.courier && !!this.courier._id) ? this.courier.getApiPublicFields() : this.courier,
        recipient: (!!this.recipient && !!this.recipient._id) ? this.recipient.getApiPublicFields() : this.recipient
    };
};

OrderSchema.methods.getApiCourierFields = function (user: IUserModel): any {
    let photos: Array<string> = [];
    if (this.photos && this.photos.length > 0) {
        for (let i: number = 0; i < this.photos.length; i++) {
            photos.push(AppConfig.getInstanse().get("base_url") + AppConfig.getInstanse().get("urls:order_files") + "/" + this.photos[i]);
        }
    }
    let unread_messages: number = 0;
    if (user && user._id) {
        if (this.owner && this.owner._id) {
            if (this.owner._id.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_owner;
            }
        } else if (this.owner) {
            if (this.owner.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_owner;
            }
        }

        if (this.courier && this.courier._id) {
            if (this.courier._id.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_courier;
            }
        } else if (this.courier) {
            if (this.courier.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_courier;
            }
        }
    }

    return {
        order: {
            id: this._id,
            orderId: this.orderId,
            weight: this.weight,
            small_package_count: this.small_package_count,
            medium_package_count: this.medium_package_count,
            large_package_count: this.large_package_count,
            size: this.size,
            photos: photos,
            status: this.status,
            type: this.type,
            owner_name: this.owner_name,
            owner_contact: this.owner_contact,
            owner_address: this.owner_address,
            owner_comment: this.owner_comment,
            owner_location: this.owner_location.coordinates,
            recipient_address: this.recipient_address,
            recipient_comment: this.recipient_comment,
            recipient_location: this.recipient_location.coordinates,
            recipient_name: this.recipient_name,
            recipient_contact: this.recipient_contact,
            created_at: this.created_at,
            start_at: this.start_at,
            end_at: this.end_at,
            paid: this.paid,
            paid_return: this.paid_return,
            pay_type: this.pay_type,
            route: this.route,
            cost: this.cost - this.serviceFee,
            serviceFee: this.serviceFee,
            userCredit: this.userCredit * -1,
            returnCost: this.returnCost,
            card: this.card,
            cash: this.cash,
            bonus: this.bonus,
            updated_at: this.updated_at,
            use_bonus: this.use_bonus,
            owner_canceled: this.owner_canceled,
            unread_messages: unread_messages,
            owner: (!!this.owner && !!this.owner._id) ? this.owner._id.toString() : this.owner,
            courier: (!!this.courier && !!this.courier._id) ? this.courier._id.toString() : this.courier,
            recipient: (!!this.recipient && !!this.recipient._id) ? this.recipient._id.toString() : this.recipient
        },
        owner: (!!this.owner && !!this.owner._id) ? this.owner.getApiPublicFields() : this.owner,
        courier: (!!this.courier && !!this.courier._id) ? this.courier.getApiPublicFields() : this.courier,
        recipient: (!!this.recipient && !!this.recipient._id) ? this.recipient.getApiPublicFields() : this.recipient
    };
};

export const OrderModel: Model<IOrderModel> = ConnectionDb.getInstanse().model<IOrderModel>("OrderModel", OrderSchema);
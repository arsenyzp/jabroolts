import {Document, Schema, Model} from "mongoose";
import {ConnectionDb} from "../components/mongoose";

export interface ICountryModel extends Document {
    name?: string;
    code?: string;
    phone?: string;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;

    getPublicFields(): any;
}

export const CountrySchema: Schema = new Schema({
    name: {type: String, trim: true},
    code: {type: String, trim: true},
    phone: {type: String, trim: true},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
});

CountrySchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

CountrySchema.methods.getPublicFields = function (): any {
    return {
        id: this._id,
        name: this.name,
        code: this.code,
        phone: this.phone
    };
};

export const CountryModel: Model<ICountryModel> = ConnectionDb.getInstanse().model<ICountryModel>("CountryModel", CountrySchema);
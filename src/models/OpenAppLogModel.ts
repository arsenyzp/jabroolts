import {Document, Schema, Model} from "mongoose";
import {IUserModel, UserModel} from "./UserModel";
import {ConnectionDb} from "../components/mongoose";

export interface IOpenAppLogModel extends Document {
    user?: IUserModel;
    duration?: number;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;
}

export const OpenAppLogSchema: Schema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: UserModel},
    duration: {type: Number, default: 0},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
});

OpenAppLogSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

export const OpenAppLogModel: Model<IOpenAppLogModel> = ConnectionDb.getInstanse().model<IOpenAppLogModel>("OpenAppLogModel", OpenAppLogSchema);
import {ConfigModel} from "../ConfigModel";
import * as cache from "memory-cache";

export class PackageRatioConfig {
    private _key: string = "PackageRatioConfig";
    private _name: string = "PackageRatioConfig";

    public small: number;
    public medium: number;
    public large: number;

    constructor() {
        this.small = 1;
        this.medium = 3;
        this.large = 7;
    }

    public save(): Promise<{}> {
        return new Promise((resolve, reject) => {
            ConfigModel
                .findOne({code: this._key})
                .then(config => {
                    if (config == null) {
                        config = new ConfigModel();
                        config.code = this._key;
                        config.name = this._name;
                    }
                    config.value = JSON.stringify({
                        small: this.small,
                        medium: this.medium,
                        large: this.large
                    });
                    config
                        .save()
                        .then(model => {
                            cache.put(this._key, config.value, 500000);
                            resolve(model);
                        })
                        .catch(err => {
                            reject(err);
                        });
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

    public getInstanse(): Promise<PackageRatioConfig> {
        return new Promise((resolve, reject) => {
            let value: string = cache.get(this._key);
            if (value) {
                let item: PackageRatioConfig = Object.create(PackageRatioConfig.prototype);
                Object.assign(item, JSON.parse(value), {});
                this.small = item.small;
                this.medium = item.medium;
                this.large = item.large;

                resolve(this);
            } else {
                ConfigModel
                    .findOne({code: this._key})
                    .then(config => {
                        if (config == null) {
                            config = new ConfigModel();
                            config.code = this._key;
                            config.name = this._name;
                            config.value = JSON.stringify({
                                small: this.small,
                                medium: this.medium,
                                large: this.large
                            });
                        }
                        cache.put(this._key, config.value, 500000);
                        let value: any = JSON.parse(config.value);

                        let item: PackageRatioConfig = Object.create(PackageRatioConfig.prototype);
                        Object.assign(item, value, {});
                        this.small = item.small;
                        this.medium = item.medium;
                        this.large = item.large;

                        resolve(this);
                    })
                    .catch(err => {
                        reject(err);
                    });
            }
        });
    }

}
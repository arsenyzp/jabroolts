import {ConfigModel, IConfigModel} from "../ConfigModel";

export class DeliveryConfig {
    private _key: string = "DeliveryConfig";
    private _name: string = "DeliveryConfig";

    public waiting_time: number;
    public average_speed: number;
    public missed_time: number;

    constructor() {
        this.waiting_time = 120;
        this.average_speed = 100;
        this.missed_time = 120;
    }

    public save(): Promise<{}> {
        return new Promise((resolve, reject) => {
            ConfigModel
                .findOne({code: this._key})
                .then(config => {
                    if (config == null) {
                        config = new ConfigModel();
                        config.code = this._key;
                        config.name = this._name;
                    }
                    config.value = JSON.stringify({
                        waiting_time: this.waiting_time,
                        average_speed: this.average_speed,
                        missed_time: this.missed_time
                    });
                    config
                        .save()
                        .then(model => {
                            resolve(model);
                        })
                        .catch(err => {
                            reject(err);
                        });
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

    public getInstanse(): Promise<DeliveryConfig> {
        return new Promise((resolve, reject) => {
            ConfigModel
                .findOne({code: this._key})
                .then(config => {
                    if (config == null) {
                        config = new ConfigModel();
                        config.code = this._key;
                        config.name = this._name;
                        config.value = JSON.stringify({
                            waiting_time: this.waiting_time,
                            average_speed: this.average_speed,
                            missed_time: this.missed_time
                        });
                    }
                    let value: any = JSON.parse(config.value);

                    let item: DeliveryConfig = Object.create(DeliveryConfig.prototype);
                    Object.assign(item, value, {});
                    this.waiting_time = item.waiting_time;
                    this.average_speed = item.average_speed;
                    this.missed_time = item.missed_time;

                    resolve(this);
                })
                .catch(err => {
                    reject(err);
                });
        });
    }
}
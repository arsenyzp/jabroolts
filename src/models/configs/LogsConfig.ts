import {ConfigModel} from "../ConfigModel";

export class LogsConfig {
    private _key: string = "LogsConfig";
    private _name: string = "LogsConfig";

    public api: boolean;
    public error: boolean;
    public sms: boolean;
    public email: boolean;

    constructor() {
        this.api = false;
        this.error = false;
        this.sms = false;
        this.email = false;
    }

    public save(): Promise<{}> {
        return new Promise((resolve, reject) => {
            ConfigModel
                .findOne({code: this._key})
                .then(config => {
                    if (config == null) {
                        config = new ConfigModel();
                        config.code = this._key;
                        config.name = this._name;
                    }
                    config.value = JSON.stringify({
                        api: this.api,
                        error: this.error,
                        sms: this.sms,
                        email: this.email
                    });
                    config
                        .save()
                        .then(model => {
                            resolve(model);
                        })
                        .catch(err => {
                            reject(err);
                        });
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

    public getInstanse(): Promise<{}> {
        return new Promise((resolve, reject) => {
            ConfigModel
                .findOne({code: this._key})
                .then(config => {
                    if (config == null) {
                        config = new ConfigModel();
                        config.code = this._key;
                        config.name = this._name;
                        config.value = JSON.stringify({
                            api: this.api,
                            error: this.error,
                            sms: this.sms,
                            email: this.email
                        });
                    }
                    let value: any = JSON.parse(config.value);

                    let model: LogsConfig = Object.create(LogsConfig.prototype);
                    Object.assign(model, value, {});
                    this.api = model.api;
                    this.error = model.error;
                    this.sms = model.sms;
                    this.email = model.email;

                    resolve(this);
                })
                .catch(err => {
                    reject(err);
                });
        });
    }
}
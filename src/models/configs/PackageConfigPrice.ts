import {PackagePrice} from "./PackagePrice";
import {ConfigModel} from "../ConfigModel";

export class PackageConfigPrice {
    private _key: string = "PackageConfigPrice";
    private _name: string = "PackageConfigPrice";

    public small: PackagePrice;
    public medium: PackagePrice;
    public large: PackagePrice;

    constructor() {
        this.small = new PackagePrice();
        this.medium = new PackagePrice();
        this.large = new PackagePrice();
    }

    public save(): Promise<{}> {
        return new Promise((resolve, reject) => {
            ConfigModel
                .findOne({code: this._key})
                .then(config => {
                    if (config == null) {
                        config = new ConfigModel();
                        config.code = this._key;
                        config.name = this._name;
                    }
                    config.value = JSON.stringify({
                        small: this.small,
                        medium: this.medium,
                        large: this.large
                    });
                    config
                        .save()
                        .then(model => {
                            resolve(model);
                        })
                        .catch(err => {
                            reject(err);
                        });
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

    public getInstanse(): Promise<PackageConfigPrice> {
        return new Promise((resolve, reject) => {
            ConfigModel
                .findOne({code: this._key})
                .then(config => {
                    if (config == null) {
                        config = new ConfigModel();
                        config.code = this._key;
                        config.name = this._name;
                        config.value = JSON.stringify({
                            small: this.small,
                            medium: this.medium,
                            large: this.large
                        });
                    }
                    let value: any = JSON.parse(config.value);

                    let small: PackagePrice = Object.create(PackagePrice.prototype);
                    Object.assign(small, value.small, {});
                    this.small = small;

                    let medium: PackagePrice = Object.create(PackagePrice.prototype);
                    Object.assign(medium, value.medium, {});
                    this.medium = medium;

                    let large: PackagePrice = Object.create(PackagePrice.prototype);
                    Object.assign(large, value.large, {});
                    this.large = large;

                    resolve(this);
                })
                .catch(err => {
                    reject(err);
                });
        });
    }
}
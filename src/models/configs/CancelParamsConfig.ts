import {ConfigModel, IConfigModel} from "../ConfigModel";

export class CancelParamsConfig {
    private _key: string = "CancelParamsConfig";
    private _name: string = "CancelParamsConfig";

    public cost: number;

    constructor() {
        this.cost = 5;
    }

    public save(): Promise<IConfigModel> {
        return new Promise((resolve, reject) => {
            ConfigModel
                .findOne({code: this._key})
                .then(config => {
                    if (config == null) {
                        config = new ConfigModel();
                        config.code = this._key;
                        config.name = this._name;
                    }
                    config.value = JSON.stringify({
                        cost: this.cost,
                    });
                    config
                        .save()
                        .then(model => {
                            resolve(model);
                        })
                        .catch(err => {
                            reject(err);
                        });
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

    public getInstanse(): Promise<CancelParamsConfig> {
        return new Promise((resolve, reject) => {
            ConfigModel
                .findOne({code: this._key})
                .then(config => {
                    if (config == null) {
                        config = new ConfigModel();
                        config.code = this._key;
                        config.name = this._name;
                        config.value = JSON.stringify({
                            cost: this.cost
                        });
                    }
                    let value: any = JSON.parse(config.value);

                    let item: CancelParamsConfig = Object.create(CancelParamsConfig.prototype);
                    Object.assign(item, value, {});
                    this.cost = item.cost;
                    resolve(this);
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

}
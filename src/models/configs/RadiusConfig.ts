import {ConfigModel} from "../ConfigModel";

export class RadiusConfig {
    private _key: string = "RadiusConfig";
    private _name: string = "RadiusConfig";

    public request: number;
    public feed: number;
    public drop_off: number;
    public close_radius: number;

    constructor() {
        this.request = 1000;
        this.feed = 1000;
        this.drop_off = 1000;
        this.close_radius = 1000;
    }

    public save(): Promise<{}> {
        return new Promise((resolve, reject) => {
            ConfigModel
                .findOne({code: this._key})
                .then(config => {
                    if (config == null) {
                        config = new ConfigModel();
                        config.code = this._key;
                        config.name = this._name;
                    }
                    config.value = JSON.stringify({
                        request: this.request,
                        feed: this.feed,
                        drop_off: this.drop_off,
                        close_radius: this.close_radius
                    });
                    config
                        .save()
                        .then(model => {
                            resolve(model);
                        })
                        .catch(err => {
                            reject(err);
                        });
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

    public getInstanse(): Promise<{}> {
        return new Promise((resolve, reject) => {
            ConfigModel
                .findOne({code: this._key})
                .then(config => {
                    if (config == null) {
                        config = new ConfigModel();
                        config.code = this._key;
                        config.name = this._name;
                        config.value = JSON.stringify({
                            request: this.request,
                            feed: this.feed,
                            drop_off: this.drop_off,
                            close_radius: this.close_radius
                        });
                    }
                    let value: any = JSON.parse(config.value);

                    let item: RadiusConfig = Object.create(RadiusConfig.prototype);
                    Object.assign(item, value, {});
                    this.request = item.request;
                    this.feed = item.feed;
                    this.drop_off = item.drop_off;
                    this.close_radius = item.close_radius;

                    resolve(this);
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

}
import {ConfigModel, IConfigModel} from "../ConfigModel";

export class BonusesConfig {
    private _key: string = "BonusesConfig";
    private _name: string = "BonusesConfig";

    public inviteBonuses: number;
    public defaultCourierLimit: number;

    constructor() {
        this.inviteBonuses = 5;
        this.defaultCourierLimit = 100;
    }

    public save(): Promise<{}> {
        return new Promise((resolve, reject) => {
            ConfigModel
                .findOne({code: this._key})
                .then(config => {
                    if (config == null) {
                        config = new ConfigModel();
                        config.code = this._key;
                        config.name = this._name;
                    }
                    config.value = JSON.stringify({
                        inviteBonuses: this.inviteBonuses,
                        defaultCourierLimit: this.defaultCourierLimit
                    });
                    config
                        .save()
                        .then(model => {
                            resolve(model);
                        })
                        .catch(err => {
                            reject(err);
                        });
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

    public getInstanse(): Promise<BonusesConfig> {
        return new Promise((resolve, reject) => {
            ConfigModel
                .findOne({code: this._key})
                .then(config => {
                    if (config == null) {
                        config = new ConfigModel();
                        config.code = this._key;
                        config.name = this._name;
                        config.value = JSON.stringify({
                            inviteBonuses: this.inviteBonuses,
                            defaultCourierLimit: this.defaultCourierLimit
                        });
                    }
                    let value: any = JSON.parse(config.value);

                    let item: BonusesConfig = Object.create(BonusesConfig.prototype);
                    Object.assign(item, value, {});
                    this.inviteBonuses = item.inviteBonuses;
                    this.defaultCourierLimit = item.defaultCourierLimit;

                    resolve(this);
                })
                .catch(err => {
                    reject(err);
                });
        });
    }
}
import {ConfigModel} from "../ConfigModel";

export class TermsConfig {
    private _key: string = "TermsConfig";
    private _name: string = "TermsConfig";

    public terms_courier_en: string;
    public terms_courier_ar: string;
    public terms_customer_en: string;
    public terms_customer_ar: string;
    public privacy_en: string;
    public privacy_ar: string;

    constructor() {
        this.terms_courier_en = "";
        this.terms_courier_ar = "";
        this.terms_customer_en = "";
        this.terms_customer_ar = "";
        this.privacy_en = "";
        this.privacy_ar = "";
    }

    public save(): Promise<{}> {
        return new Promise((resolve, reject) => {
            ConfigModel
                .findOne({code: this._key})
                .then(config => {
                    if (config == null) {
                        config = new ConfigModel();
                        config.code = this._key;
                        config.name = this._name;
                    }
                    config.value = JSON.stringify({
                        terms_courier_en: this.terms_courier_en,
                        terms_courier_ar: this.terms_courier_ar,
                        terms_customer_en: this.terms_customer_en,
                        terms_customer_ar: this.terms_customer_ar,
                        privacy_en: this.privacy_en,
                        privacy_ar: this.privacy_ar,
                    });
                    config
                        .save()
                        .then(model => {
                            resolve(model);
                        })
                        .catch(err => {
                            reject(err);
                        });
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

    public getInstanse(): Promise<TermsConfig> {
        return new Promise((resolve, reject) => {
            ConfigModel
                .findOne({code: this._key})
                .then(config => {
                    if (config == null) {
                        config = new ConfigModel();
                        config.code = this._key;
                        config.name = this._name;
                        config.value = JSON.stringify({
                            terms_courier_en: this.terms_courier_en,
                            terms_courier_ar: this.terms_courier_ar,
                            terms_customer_en: this.terms_customer_en,
                            terms_customer_ar: this.terms_customer_ar,
                            privacy_en: this.privacy_en,
                            privacy_ar: this.privacy_ar
                        });
                    }
                    let value: any = JSON.parse(config.value);

                    let item: TermsConfig = Object.create(TermsConfig.prototype);
                    Object.assign(item, value, {});
                    this.terms_courier_en = item.terms_courier_en;
                    this.terms_courier_ar = item.terms_courier_ar;
                    this.terms_customer_en = item.terms_customer_en;
                    this.terms_customer_ar = item.terms_customer_ar;
                    this.privacy_en = item.privacy_en;
                    this.privacy_ar = item.privacy_ar;

                    resolve(this);
                })
                .catch(err => {
                    reject(err);
                });
        });
    }
}
import {Document, Schema, Model} from "mongoose";
import {IUserModel, UserModel} from "./UserModel";
import {ConnectionDb} from "../components/mongoose";

export interface INotificationModel extends Document {
    user?: IUserModel;
    type?: string;
    ref_id?: string;
    text?: string;
    title?: string;
    is_view?: boolean;

    // date created
    created_at?: number;
    // date updated
    updated_at?: number;

    getPublicFields: () => any;
}

export const NotificationSchema: Schema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: UserModel},
    type: {type: String},
    ref_id: {type: String},
    text: {type: String},
    title: {type: String},
    is_view: {type: Boolean, default: false},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
});

NotificationSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

NotificationSchema.methods.getPublicFields = function (): any {
    return {
        id: this._id,
        data_id: this.ref_id,
        type: this.type,
        text: this.text,
        title: this.title,
        created_at: this.created_at,
        is_view: this.is_view
    };
};

export const NotificationModel: Model<INotificationModel> = ConnectionDb
    .getInstanse()
    .model<INotificationModel>("NotificationModel", NotificationSchema);
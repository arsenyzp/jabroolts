import { Document, Schema, Model, model} from "mongoose";
import {ConnectionDb} from "../../components/mongoose";

export interface ISmsLogModel extends Document {
    phone?: string;
    text?: string;
    mid?: string;
    cost?: string;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;
}

export const SmsLogSchema: Schema = new Schema({
    phone: { type: String , trim: true},
    text: { type: String , trim: true},
    mid: { type: String , trim: true},
    cost: { type: String , trim: true},
    // date created
    created_at: { type: Number },
    // date updated
    updated_at: { type: Number },
});

SmsLogSchema.pre("save",  function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

export const SmsLogModel: Model<ISmsLogModel> = ConnectionDb.getInstanse().model<ISmsLogModel>("SmsLogModel", SmsLogSchema);
import { Document, Schema, Model, model} from "mongoose";
import {ConnectionDb} from "../../components/mongoose";

export interface IEmailLogModel extends Document {
    email?: string;
    subject?: string;
    text?: string;
    html?: string;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;
}

export const EmailLogSchema: Schema = new Schema({
    email: { type: String , trim: true},
    subject: { type: String , trim: true},
    text: { type: String , trim: true},
    html: { type: String , trim: true},
    // date created
    created_at: { type: Number },
    // date updated
    updated_at: { type: Number },
});

EmailLogSchema.pre("save",  function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

export const EmailLogModel: Model<IEmailLogModel> = ConnectionDb.getInstanse().model<IEmailLogModel>("EmailLogModel", EmailLogSchema);
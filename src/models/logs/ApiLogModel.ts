import { Document, Schema, Model, model} from "mongoose";
import {ConnectionDb} from "../../components/mongoose";

export interface IApiLogModel extends Document {
    method?: string;
    params?: string;
    body?: string;
    headers?: string;
    url?: string;
    query?: string;
    rawHeaders?: string;
    path?: string;
    status?: string;
    response?: string;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;
}

export const ApiLogSchema: Schema = new Schema({
    method: { type: String , trim: true},
    params: { type: String , trim: true},
    body: { type: String , trim: true},
    headers: { type: String , trim: true},
    url: { type: String , trim: true},
    query: { type: String , trim: true},
    rawHeaders: { type: String , trim: true},
    path: { type: String , trim: true},
    status: { type: String , trim: true},
    response: { type: String , trim: true},
    // date created
    created_at: { type: Number },
    // date updated
    updated_at: { type: Number },
});

ApiLogSchema.pre("save",  function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

export const ApiLogModel: Model<IApiLogModel> = ConnectionDb.getInstanse().model<IApiLogModel>("ApiLogModel", ApiLogSchema);
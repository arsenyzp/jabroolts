import {Document, Schema, Model} from "mongoose";
import {IUserModel, UserModel} from "./UserModel";
import {ConnectionDb} from "../components/mongoose";
import {IOrderModel, OrderModel} from "./OrderModel";

export const PaymentTypes: any = {
    Invite: "invite",
    Promo: "promo",
    Refund: "refund",
    Fee: "fee",
    Payment: "payment",
    Refill: "refill",
    Order: "order",
    Admin: "admin"
};

export const PaymentStatuses: any = {
    Init: "init",
    Success: "success",
    Fail: "fail",
};

export interface IPaymentLogModel extends Document {
    user?: IUserModel;
    order?: IOrderModel;
    type?: string;
    status?: string;
    amount?: number;
    bt_pay_id?: string;
    bt_status?: string;
    bt_type?: string;
    bt_currencyIsoCode?: string;
    bt_amount?: string;
    bt_result_json?: string;

    // date created
    created_at?: number;
    // date updated
    updated_at?: number;
}

export const PaymentLogSchema: Schema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: UserModel},
    order: {type: Schema.Types.ObjectId, ref: OrderModel},
    type: {type: String, default: PaymentTypes.Payment},
    status: {type: String, default: PaymentStatuses.Init},
    amount: {type: Number, default: 0},
    bt_pay_id: {type: String, default: ""},
    bt_status: {type: String, default: ""},
    bt_type: {type: String, default: ""},
    bt_currencyIsoCode: {type: String, default: ""},
    bt_amount: {type: String, default: ""},
    bt_result_json: {type: String, default: "{}"},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
});

PaymentLogSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

export const PaymentLogModel: Model<IPaymentLogModel> = ConnectionDb.getInstanse().model<IPaymentLogModel>("PaymentLogModel", PaymentLogSchema);
import {Document, Schema, Model} from "mongoose";
import {ConnectionDb} from "../components/mongoose";
import {HistoryLocationModel, IHistoryLocationModel} from "./HistoryLocationModel";
import {IUserModel, UserModel} from "./UserModel";

export interface IFavoriteLocationModel extends Document {
    user: IUserModel;
    history_id?: IHistoryLocationModel;
    address: string;
    name: string;
    lat: number;
    lon: number;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;

    getPublicFields(): any;
}

export const FavoriteLocationSchema: Schema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: UserModel},
    history_id: {type: Schema.Types.ObjectId, ref: HistoryLocationModel},
    address: {type: String, trim: true},
    name: {type: String, trim: true},
    lat: {type: Number},
    lon: {type: Number},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number}
});

FavoriteLocationSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

FavoriteLocationSchema.methods.getPublicFields = function (): any {
    return {
        id: this._id,
        history_id: this.history_id,
        address: this.address,
        lat: this.lat,
        lon: this.lon,
        name: this.name,
        created_at: this.created_at
    };
};

export const FavoriteLocationModel: Model<IFavoriteLocationModel> =
    ConnectionDb.getInstanse().model<IFavoriteLocationModel>("FavoriteLocationModel", FavoriteLocationSchema);
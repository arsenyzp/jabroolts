import {Document, Schema, Model} from "mongoose";
import {ConnectionDb} from "../components/mongoose";
import {IUserModel, UserModel} from "./UserModel";

export interface IServiceMessageModel extends Document {
    mid?: string;
    sender: IUserModel;
    recipient: IUserModel;
    text?: string;
    is_read?: boolean;
    is_view?: boolean;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;

    getPublicFields(): any;
}

export const ServiceMessageSchema: Schema = new Schema({
    mid: {type: String},
    sender: {type: Schema.Types.ObjectId, ref: UserModel},
    recipient: {type: Schema.Types.ObjectId, ref: UserModel},
    text: {type: String},
    is_read: {type: Boolean, default: false},
    is_view: {type: Boolean, default: false},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
});

ServiceMessageSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

ServiceMessageSchema.methods.getPublicFields = function (): any {
    return {
        id: this._id,
        mid: this.mid,
        is_read: this.is_read,
        is_view: this.is_view,
        sender: this.sender ? this.sender.toString() : "",
        recipient: this.recipient ? this.recipient.toString() : "",
        text: this.text ? this.text : "",
        created_at: this.created_at
    };
};

export const ServiceMessageModel: Model<IServiceMessageModel> =
    ConnectionDb.getInstanse().model<IServiceMessageModel>("ServiceMessageModel", ServiceMessageSchema);
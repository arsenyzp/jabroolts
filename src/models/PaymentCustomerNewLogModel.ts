import {Document, Schema, Model} from "mongoose";
import {IUserModel, UserModel} from "./UserModel";
import {ConnectionDb} from "../components/mongoose";
import {IOrderModel, OrderModel} from "./OrderModel";

export const PaymentStatuses: any = {
    Init: "init",
    Success: "success",
    Fail: "fail",
};

export interface IPaymentCustomerNewLogModel extends Document {
    customer?: IUserModel;
    order?: IOrderModel;
    status?: string;
    amount?: number;
    payment_type?: string;
    payment_method?: string;
    customerDebt?: number;
    customerBonus?: number;

    // date created
    created_at?: number;
    // date updated
    updated_at?: number;
}

export const PaymentCustomerNewLogSchema: Schema = new Schema({
    customer: {type: Schema.Types.ObjectId, ref: UserModel},
    order: {type: Schema.Types.ObjectId, ref: OrderModel},
    status: {type: String, default: PaymentStatuses.Init},
    amount: {type: Number, default: 0},
    payment_type: {type: String, default: ""},
    payment_method: {type: String, default: ""},
    customerDebt: {type: Number, default: 0},
    customerBonus: {type: Number, default: 0},

    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number},
});

PaymentCustomerNewLogSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

export const PaymentCustomerNewLogModel: Model<IPaymentCustomerNewLogModel> =
    ConnectionDb.getInstanse().model<IPaymentCustomerNewLogModel>("PaymentCustomerNewLogModel", PaymentCustomerNewLogSchema);
import {Document, Schema, Model} from "mongoose";
import {ConnectionDb} from "../components/mongoose";

export interface IBankModel extends Document {
    name?: string;
    country_code?: string;
    // date created
    created_at?: number;
    // date updated
    updated_at?: number;

    getPublicFields(): any;
}

export const BankSchema: Schema = new Schema({
    name: {type: String, trim: true},
    country_code: {type: String, trim: true},
    // date created
    created_at: {type: Number},
    // date updated
    updated_at: {type: Number}
});

BankSchema.pre("save", function (next: any): void {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});

BankSchema.methods.getPublicFields = function (): any {
    return {
        id: this._id,
        name: this.name
    };
};

export const BankModel: Model<IBankModel> = ConnectionDb.getInstanse().model<IBankModel>("BankModel", BankSchema);
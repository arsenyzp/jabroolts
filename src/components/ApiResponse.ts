import {RequestInterface} from "../interfaces/RequestInterface";
import {Dictionary, MappedError} from "express-validator";
import {ValidationError} from "class-validator";

class ApiError {

    public code: string;
    public message: string;

    constructor(code: string, message: string) {
        this.code = code;
        this.message = message;
    }
}

export class ApiResponse {
    private data: any = {};
    private errors: Array<any> = [];

    public addError (error: Error): void {
        let item: ApiError = new ApiError("server", error.message);
        this.errors.push(item);
    }

    public addErrorMessage (key: string, error: string): void {
        let item: ApiError = new ApiError(key, error);
        this.errors.push(item);
    }

    public addErrors (errors: Array<Error>): void {
        errors.forEach( (error, key, array) => {
            let item: ApiError = new ApiError("server", error.message);
            this.errors.push(item);
        });
    }

    public setDate (data: any): void {
        this.data = data;
    }

    public addValidationErrors (req: RequestInterface, errors: Array<ValidationError>): void {
        for (let i of Object.keys(errors)) {
            for (let j of Object.keys(errors[i].constraints)) {
                let item: ApiError = new ApiError(errors[i].property, req.__(errors[i].constraints[j]));
                this.errors.push(item);
            }
        }
    }

    public get(message?: string): ApiResponseObject {
        let r: ApiResponseObject = new ApiResponseObject(this.data, this.errors);
        if (message) {
            r.message = message;
        }
        if (this.errors.length > 0) {
            r.status = 0;
        }
        return r;
    }
}

class ApiResultObject {
    public error: Array<string>;
    public data: any;
}

class ApiResponseObject {
    public status: number = 1;
    public message: string = "OK";
    public result: ApiResultObject;

    constructor(data: any, errors: any) {
        this.result = new ApiResultObject();
        this.result.data = data;
        this.result.error = errors;
    }
}
import {UserModel} from "../models/UserModel";
import * as FCM from "fcm-push";
import {Log} from "./Log";
import {AppConfig} from "./config";

const log: any = Log.getInstanse()(module);

const fcm: any = new FCM(AppConfig.getInstanse().get("push:gcm_sender"));

export class PushService {

    public static send(ids: Array<string>, title: string, message: string, payload: any): void {
        log.debug("fcm push " + title + " " + message);
        if (!ids || !ids.length || ids.length === 0) {
            log.error("Empty ids");
            return;
        }

        /**
         * Update counter
         * @type {{key: String, cert: String, production: boolean}}
         */
        if (!payload || (payload.type !== "message")) {
            UserModel
                .update({$or: [{push_device_ios: {$in: ids}}, {push_device_android: {$in: ids}}]}, {
                $inc: { unread_count: 1 }
            }, {multi: true})
                .catch(err => {
                    log.error(err);
                });
        }

        if (!payload) {
            payload = {};
        }

        /**
         * Find user and send
         * @type {Array}
         */
        let users_notified: Array<string> = [];
        UserModel
            .find({$or: [{push_device_ios: {$in: ids}}, {push_device_android: {$in: ids}}]})
            .then( (users) => {
                for (let i: number = 0; i < users.length; i++) {
                    if (users_notified.indexOf(users[i]._id.toString()) < 0) {
                        users_notified.push(users[i]._id.toString());
                        let push_ids: Array<string> = users[i].push_device_ios.concat(users[i].push_device_android);
                        for (let j: number = 0; j < push_ids.length; j++) {
                            payload.to = push_ids[j];
                            payload.notification = {
                                title: title,
                                body: message,
                                icon: "ic_logo_notification",
                                expiry: Math.floor(Date.now() / 1000) + 3600,
                                badge: users[i].unread_count + users[i].unread_messages,
                                sound: "ping.aiff",
                                alert: title,
                                click_action: "OPEN_NOTIFICATION"
                            };
                            // for android
                            // payload.data = {
                            //     title: title,
                            //     body: message,
                            //     icon: 'ic_launcher',
                            //     expiry: Math.floor(Date.now() / 1000) + 3600,
                            //     badge: users[i].unread_count + users[i].unread_messages,
                            //     sound: "ping.aiff",
                            //     alert: title,
                            // };
                            payload.priority = "high";
                            // let mes = {
                            //     to: push_ids[j], // required fill with device token or topics
                            //     data: payload,
                            //     notification: {
                            //         title: title,
                            //         body: message,
                            //         icon: 'ic_launcher',
                            //         expiry: Math.floor(Date.now() / 1000) + 3600,
                            //         badge: users[i].unread_count + users[i].unread_messages,
                            //         sound: "ping.aiff",
                            //         alert: title,
                            //     },
                            //     priority: 'high'
                            // };

                            fcm.send(payload)
                                .then((response) => {
                                    log.info("Successfully sent with response: ", response);
                                })
                                .catch((err) => {
                                    log.info("Something has gone wrong!");
                                    log.error(err);
                                });
                        }
                    }
                }

            })
            .catch( (err) => {
                log.error(err);
            });
    }
}
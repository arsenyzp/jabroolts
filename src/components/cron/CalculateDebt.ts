import * as EventBus from "eventbusjs";
import {Log} from "../Log";
import {IUserModel, UserModel} from "../../models/UserModel";
import {IDebtLogModel, DebtLogModel} from "../../models/DebtLogModel";

const log: any = Log.getInstanse()(module);

export class CalculateDebt {
    public async run(): Promise<any> {
        try {
            let users: Array<IUserModel> =  await UserModel.find({balance: {$lt: 0}});
            let total: number = 0;
            users.map((v, k, a) => {
                total -= v.balance;
            });
            let m: IDebtLogModel = new DebtLogModel();
            m.value = total * -1;
            await m.save();
        } catch (e) {
            log.error(e);
        }
    }
}
import * as EventBus from "eventbusjs";
import {Log} from "../Log";
import {IOrderModel, OrderModel, OrderStatuses} from "../../models/OrderModel";
import {DeliveryConfig} from "../../models/configs/DeliveryConfig";
import {EVENT_ORDER_MISSED_TIMEOUT, EVENT_ORDER_REQUEST_CANCELED} from "../events/Events";

const log: any = Log.getInstanse()(module);

export class MissedOrder {
    public async run(): Promise<any> {
        try {
            let model: DeliveryConfig = await new DeliveryConfig().getInstanse();
            let maxTime: number = new Date().getTime() - model.missed_time * 60 * 1000;
            let missedOrders: Array<IOrderModel> = await OrderModel
                .find({status : OrderStatuses.New, created_at: {$lte: maxTime}}).lean() as Array<any>;
            missedOrders.forEach(async (order, key, array) => {
                let model: IOrderModel = await OrderModel.findById(order._id);
                EventBus.dispatch(EVENT_ORDER_REQUEST_CANCELED, this, {
                    order: model
                });
                EventBus.dispatch(EVENT_ORDER_MISSED_TIMEOUT, this, {
                    order: model
                });
                try {
                    await OrderModel.findOneAndUpdate(
                        {_id: order._id},
                        {
                            status: OrderStatuses.Missed
                        },
                        {new: true}
                        );
                } catch (e) {
                    log.error(e);
                }
            });
        } catch (e) {
            log.error(e);
        }
    }
}
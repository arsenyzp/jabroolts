import * as EventBus from "eventbusjs";
import {Log} from "../Log";
import {IUserModel, UserModel, UserStatus} from "../../models/UserModel";
import {IUserActivityLogModel, UserActivityLogModel} from "../../models/UserActivityLogModel";
import {SocketServer} from "../SocketServer";

const log: any = Log.getInstanse()(module);

export class ClearSocket {
    public async run(): Promise<any> {
        try {
            let users: Array<IUserModel> =  await UserModel.find({$or: [
                    {web_socket_ids: { $exists: true, $not: {$size: 0} } },
                    {socket_ids: { $exists: true, $not: {$size: 0} } }
                ]
            });
            let wrong_socket_ids: Array<string> = [];
            let wrong_web_socket_ids: Array<string> = [];
            let wrong_admin_socket_ids: Array<string> = [];
            users.map((v, k, a) => {
                v.socket_ids.map((id, key, array) => {
                    if (!SocketServer.getInstance().sockets.sockets[id] ||
                        !SocketServer.getInstance().sockets.sockets[id].emit) {
                        wrong_socket_ids.push(id);
                    }
                });
                v.web_socket_ids.map((id, key, array) => {
                    if (!SocketServer.getInstance().sockets.sockets[id] ||
                        !SocketServer.getInstance().sockets.sockets[id].emit) {
                        wrong_web_socket_ids.push(id);
                    }
                });
                v.socket_admin_ids.map((id, key, array) => {
                    if (!SocketServer.getInstance().sockets.sockets[id] ||
                        !SocketServer.getInstance().sockets.sockets[id].emit) {
                        wrong_admin_socket_ids.push(id);
                    }
                });
            });
            wrong_socket_ids.map((v, k, a) => {
                UserModel.findOneAndUpdate({socket_ids: v}, {
                    $pull: {socket_ids: v}
                });
            });
            wrong_web_socket_ids.map((v, k, a) => {
                UserModel.findOneAndUpdate({socket_ids: v}, {
                    $pull: {socket_ids: v}
                });
            });
            wrong_admin_socket_ids.map((v, k, a) => {
                UserModel.findOneAndUpdate({socket_ids: v}, {
                    $pull: {socket_ids: v}
                });
            });
        } catch (e) {
            log.error(e);
        }
    }
}
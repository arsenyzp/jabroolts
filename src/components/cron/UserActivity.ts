import * as EventBus from "eventbusjs";
import {Log} from "../Log";
import {IUserModel, UserModel, UserStatus} from "../../models/UserModel";
import {IUserActivityLogModel, UserActivityLogModel} from "../../models/UserActivityLogModel";

const log: any = Log.getInstanse()(module);

export class UserActivity {
    public async run(): Promise<any> {
        try {
            let couriers: number =  await UserModel.count({$or: [
                    {web_socket_ids: { $exists: true, $not: {$size: 0} } },
                    {socket_ids: { $exists: true, $not: {$size: 0} } }
                ],
                status: UserStatus.Active
            });
            let customers: number =  await UserModel.count({$or: [
                    {web_socket_ids: { $exists: true, $not: {$size: 0} } },
                    {socket_ids: { $exists: true, $not: {$size: 0} } }
                ],
                status: {$nin: [UserStatus.Active]}
            });

            let m: IUserActivityLogModel = new UserActivityLogModel();
            m.customers = customers;
            m.couriers = couriers;
            await m.save();
        } catch (e) {
            log.error(e);
        }
    }
}
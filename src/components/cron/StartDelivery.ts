
import {IUserModel, UserModel, UserStatus} from "../../models/UserModel";
import {CheckCourierStatus} from "../jabrool/CheckCourierStatus";
import {Log} from "../Log";
import * as delay from "delay";

const log: any = Log.getInstanse()(module);

export class StartDelivery {
    // public run(): void {
    //     UserModel
    //         .find({in_progress: false, status : UserStatus.Active})
    //         .lean()
    //         .then( (users: Array<any>) => {
    //             // users.forEach((user, key, array) => {
    //             //     if (user.current_orders.length > 0) {
    //             //         CheckCourierStatus
    //             //             .getStatus(user._id)
    //             //             .then(result => {
    //             //                 log.info(result);
    //             //             })
    //             //             .catch(err => {
    //             //                 log.error(err);
    //             //             });
    //             //     }
    //             // });
    //
    //             users.map((user, key, array) => {
    //                 if (user.current_orders.length > 0) {
    //                     CheckCourierStatus
    //                         .getStatus(user._id)
    //                         .then(result => {
    //                             log.info(result);
    //                         })
    //                         .catch(err => {
    //                             log.error(err);
    //                         });
    //                 }
    //             });
    //         })
    //         .catch(err => {
    //             log.error(err);
    //         });
    // }

    public async run(): Promise<any> {
        try {
            let users: Array<any> = await UserModel.find({in_progress: false, status : UserStatus.Active})
                .lean() as Array<any>;
            for (let i: number = 0; i < users.length; i++) {
                if (i % 100 === 0) {
                    await delay(100);
                }
                if (users[i].current_orders.length > 0) {
                    await CheckCourierStatus
                        .getStatus(users[i]._id);
                }
            }
        } catch (err) {
            log.error(err);
        }
    }
}
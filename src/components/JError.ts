export class JError {
    public name: string = "";
    public message: string = "";

    constructor(message: string) {
        this.message = message;
    }
}
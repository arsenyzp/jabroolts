
import {RequestInterface} from "../interfaces/RequestInterface";
import {ResponseInterface} from "../interfaces/ResponseInterface";
import * as express from "express";

const gm: any = require("gm").subClass({imageMagick: true});

export class ImageResizer {

    public static runtime (file_orig: string, w: number, h: number, req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
        gm(file_orig)
            .resize(w, h)
            .noProfile()
            .crop(w, h, 0, 0)
            .stream((err, stdout, stderr) => {
                if (err) {
                    return next(err);
                }
                stdout.pipe(res); //pipe to response

                // the following line gave me an error compaining for already sent headers
                //stdout.on('end', function(){res.writeHead(200, { 'Content-Type': 'ima    ge/jpeg' });});

                stdout.on("error", next);
            });
    }
}
/**
 * Application database connection
 */

import {AppConfig} from "./config";
import {Log} from "./Log";
import * as mongoose from "mongoose";
const log: any = Log.getInstanse()(module);

export class ConnectionDb {
    private static connect: mongoose.Connection;
    private static init(): void {
        if (!ConnectionDb.connect) {
            (<any>mongoose).Promise = global.Promise;
            ConnectionDb.connect = mongoose.createConnection(AppConfig.getInstanse().get("mongoose:uri"));
            ConnectionDb.connect.on("error", err => {
                log.error("connection error:", err.message);
            });
            ConnectionDb.connect.once("open", () => {
                log.info("Connected to DB!");
            });
        }
    }

    public static getInstanse(): mongoose.Connection {
        ConnectionDb.init();
        return ConnectionDb.connect;
    }
}


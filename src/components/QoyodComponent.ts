import {AxiosInstanse} from "./AxiosInstanse";
import moment = require("moment");

export const jid: number = 1;
export const eid: number = 2;

export class QoyodComponent {
    private static auth_token: string = "";

    public static async init (): Promise<any> {
        try {
            let result: any = await AxiosInstanse.get().post("login", {
                email: "hamadsj@hotmail.com",
                password: "Jabrool@123"
            });
            console.log(result.data);
            console.log(result.data.messages[0].api_key);
            if (result.data.status === "Success") {
                QoyodComponent.auth_token = result.data.messages[0].auth_token;
            }
        } catch (e) {
            console.log(e);
        }
    }

    public static async createCustomer(customer_name: string, primary_contact_number: string, primary_email: string): Promise<number> {
        let result: any = await AxiosInstanse.get().post("customers", {
            customer_name: customer_name,
            primary_contact_number: primary_contact_number,
            primary_email: primary_email
        });
        console.log(result.data);
        if (result.data.status === "Success") {
            return result.data.messages[0].id;
        } else if (result.data.status === "Failed") {
            console.log(result.data);
            throw new Error(result.data.messages);
        }
    }

    public static async createVendor(customer_name: string, primary_contact_number: string, primary_email: string): Promise<any> {
        let result: any = await AxiosInstanse.get().post("vendors", {
            vendor_name: customer_name,
            primary_contact_number: primary_contact_number,
            primary_email: primary_email
        });
        console.log(result.data);
        if (result.data.status === "Success") {
            return result.data.messages[0].id;
        } else if (result.data.status === "Failed") {
            console.log(result.data);
            throw new Error(result.data.messages);
        }
    }

    public static async createAccount(
        name: string,
        code: string,
        arabic_name: string,
        account_type: string,
        type_of_account: string, recieve_payments: number): Promise<any> {
        try {
            let result: any = await AxiosInstanse.get().post("accounts", {
                "name": "Cost Of Goods Sold 33",
                "code": "5403",
                "arabic_name": "",
                "account_type": "CurrentAsset",
                "type_of_account": "Credit33",
                "recieve_payments": 23
            });
            console.log(result.data);
            if (result.data.status === "Success") {
                //QoyodComponent.auth_token = result.data.messages[0].id;
            } else if (result.data.status === "Failed") {
                console.log(result.data.messages);
            }
        } catch (e) {
            console.log(e.request.data.errors);
        }
    }

    public static async createInvoice(
        customer_id: string,
        invoice_number: string,
        status: string,
        product_id: number,
        price: number,
    ): Promise<any> {
        let result: any = await AxiosInstanse.get().post("invoices", {
            "customer_id": customer_id,
            "invoiced_date": moment().format("YYYY-MM-DD"),
            "due_date": moment().format("YYYY-MM-DD"),
            "issue_date": moment().format("YYYY-MM-DD"),
            "invoice_number": invoice_number,
            "status": status,
            "products": {
                "1": {
                    "product_id": product_id,
                    "quantity": "1",
                    "unit_price": price,
                    "discount_percent": "0",
                    "description": ""
                }
            }
        });
        console.log(result.data);
        if (result.data.status === "Success") {
            return result.data.messages[0].id;
        } else if (result.data.status === "Failed") {
            console.log(result.data.messages);
            throw new Error(result.data.messages);
        }
    }

    public static async test(): Promise<any> {
        try {
            let id: any = await QoyodComponent.createInvoice("1", "teste33wdew", "Approved", 1, 123);
            console.log( id);
        } catch (e) {
            if (e.response && e.response.data) {
                console.log(e.response.data);
            } else {
                console.log(e.message);
            }
        }
    }
}
import {RequestInterface} from "../interfaces/RequestInterface";
import * as express from "express";
import {ApiResponse} from "./ApiResponse";
import {UserModel} from "../models/UserModel";

export class ApiAuth {
    public static check(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
        if (!req.header("authorization")) {
            let apiResponse: ApiResponse = new ApiResponse();
            apiResponse.addErrorMessage("token", "Error authorization. Empty token");
            res.status(403);
            res.json(apiResponse.get());
        } else {
            UserModel
                .findOneAndUpdate(
                    {token: req.header("authorization"), deleted: false},
                    {local: req.header("Local")},
                    {new: true}
                    )
                .then(user => {
                    if (user == null) {
                        let apiResponse: ApiResponse = new ApiResponse();
                        apiResponse.addErrorMessage("token", "Error authorization. Empty token");
                        res.status(403);
                        res.json(apiResponse.get());
                    } else {
                        // set local
                        if (user.local && user.local !== "") {
                            req.setLocale(user.local);
                        }

                        req.user = user;
                        next();
                    }
                })
                .catch(err => {
                    let apiResponse: ApiResponse = new ApiResponse();
                    apiResponse.addError(err);
                    res.status(500);
                    res.json(apiResponse.get());
                });
        }
    }
}
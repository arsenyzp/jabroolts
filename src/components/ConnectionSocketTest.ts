import {UserModel} from "../models/UserModel";

export class ConnectionSocketTest {
    public static test(socket: any, data: any): void {
        UserModel
            .findOne({socket_ids: socket.id})
            .then( (user) => {
                if (user === null) {
                    socket.emit("connection_error", {message: "Profile not found"});
                } else {
                    socket.emit("connection_ok", {message: "OK"});
                }
            })
            .catch( (err) => {
                socket.emit("connection_error", {message: "Profile not found"});
            });
    }
}
export class SocketResponse {
    public status: number = 200;
    public message: string = "";
    public data: any = {};
    public errors: any = {};
}
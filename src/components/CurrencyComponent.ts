import axios, {AxiosInstance} from "axios";
import * as cache from "memory-cache";

export class CurrencyComponent {
    // http://apilayer.net/api/live?access_key=14d42d8ba79b702b5c878e7b644a33ee&currencies=SAR&source=USD&format=1
    public static async updateData (): Promise<any> {
        let _axios: any = axios.create({
            baseURL: "http://apilayer.net/api",
        });
        _axios.defaults.timeout = 2500;

        let result: any = await _axios.get("live?access_key=14d42d8ba79b702b5c878e7b644a33ee&currencies=SAR&source=USD&format=1");
        console.log(result);
        cache.put("currency", result.data.quotes.USDSAR, 5000000);
    }
}

import {ConnectionSocket} from "../api/socket/ConnectionSocket";
import {LocationsSocket} from "../api/socket/LocationsSocket";
import * as async from "async";
import {Log} from "./Log";
import {IUserModel, UserModel, UserRoles} from "../models/UserModel";
import {MessagesSocket} from "../api/socket/MessagesSocket";
import {ConnectionSocketTest} from "./ConnectionSocketTest";

const log: any = Log.getInstanse()(module);

export class SocketServer {

    private static io: any;

    public static init(server: any): void {
        const connection: ConnectionSocket = new ConnectionSocket();
        const location: LocationsSocket = new LocationsSocket();
        const messages: MessagesSocket = new MessagesSocket();

        SocketServer.io = require("socket.io")(server);
        SocketServer.io.set("authorization",  (handshakeData, accept) => {
            accept(null, true);
        });
        SocketServer.io.sockets.on("connection", (socket) => {

            setTimeout(() => {
                socket.emit("init", {message: "connect ok"});
            }, 1000);

            socket.on("subscribe", data => {
                connection.onSubscribe(socket, data);
            });

            socket.on("admin_auth", data => {
                connection.onAdminAuth(socket, data);
            });

            socket.on("web_auth", data => {
                connection.onWebAuth(socket, data);
            });

            socket.on("disconnect", () => {
                connection.onDisconnect(socket);
            });

            socket.on("my_location", data => {
                location.onMyLocation(socket, data);
            });

            socket.on("subscribe_location_feed", data => {
                location.onSubscribeLocationFeed(socket, data);
            });

            socket.on("unsubscribe_location_feed", data => {
                location.onUnSubscribeLocationFeed(socket, data);
            });

            socket.on("send_message", data => {
                messages.onSendMessage(socket, data);
            });

            socket.on("read_message", data => {
                messages.onReadMessage(socket, data);
            });

            socket.on("connection_test", data => {
                ConnectionSocketTest.test(socket, data);
            });

        });
    }

    public static getInstance(): any {
        return SocketServer.io;
    }

    public static emit(user_ids: Array<string>, event: string, data: any, exclude?: Array<string>): void {
        if (user_ids.length === 0) {
            log.error("empty ids list");
            return;
        }

        let socket_ids: Array<string> = [];
        async.forEachOfSeries(user_ids,
             (user_id, key, callback) => {
                UserModel
                    .findById(user_id)
                    .then(user => {
                        if (user === null) {
                            log.error("Notification module -> find user Not found");
                        } else {
                            // socket ids
                            if (user.socket_ids && user.socket_ids.length > 0) {
                                socket_ids = socket_ids.concat(user.socket_ids);
                            }
                            if (user.web_socket_ids && user.web_socket_ids.length > 0) {
                                socket_ids = socket_ids.concat(user.web_socket_ids);
                            }
                        }
                        callback();
                    })
                    .catch(err => {
                        log.error("Socket emit -> find user " + err);
                    });
            },
            (err) => {
                if (err) {
                    log.error("Socket module -> forEachOfSeries " + err);
                }
                let wrong_socket_ids: Array<string> = [];
                for (let i: number = 0; i < socket_ids.length; i++) {
                    if (exclude && exclude.indexOf(socket_ids[i]) > 0) {
                        continue;
                    }
                    if (SocketServer.io.sockets.sockets[socket_ids[i]] && SocketServer.io.sockets.sockets[socket_ids[i]].emit) {
                        SocketServer.io.sockets.sockets[socket_ids[i]].emit(event, data);
                        log.debug("===============================");
                        log.debug("Socket module -> Emit " + event );
                        log.debug(data);
                        log.debug("===============================");
                    } else {
                        wrong_socket_ids.push(socket_ids[i]);
                    }
                }
                if (wrong_socket_ids.length > 0) {
                    for (let i: number = 0; i < wrong_socket_ids.length; i++) {
                        UserModel.findOneAndUpdate({socket_ids: wrong_socket_ids[i]}, {
                            $pull: {socket_ids: wrong_socket_ids[i]}
                        })
                        .catch(err => {
                            log.error("Socket emit, Error remove wrong socket ids");
                        });
                    }
                }
            }
        );
    }

    public static async adminEmit(event: string, data: any): Promise<any> {

        let admins: Array<IUserModel> = await UserModel.find({role: {$in: [UserRoles.admin, UserRoles.dev]}});
        let socket_ids: Array<string> = [];
        admins.map((v, k, a) => {
            socket_ids = socket_ids.concat(v.socket_admin_ids);
        });
        let wrong_socket_ids: Array<string> = [];
        for (let i: number = 0; i < socket_ids.length; i++) {
            if (SocketServer.io.sockets.sockets[socket_ids[i]]) {
                SocketServer.io.sockets.sockets[socket_ids[i]].emit(event, data);
                log.debug("===============================");
                log.debug("Socket module -> Emit " + event );
                log.debug(data);
                log.debug("===============================");
            } else {
                wrong_socket_ids.push(socket_ids[i]);
            }
        }
        if (wrong_socket_ids.length > 0) {
            for (let i: number = 0; i < wrong_socket_ids.length; i++) {
                UserModel.findOneAndUpdate({socket_ids: wrong_socket_ids[i]}, {
                    $pull: {socket_ids: wrong_socket_ids[i]}
                })
                    .catch(err => {
                        log.error("Socket emit, Error remove wrong socket ids");
                    });
            }
        }
    }

    public static async webEmit(event: string, data: any, user: any): Promise<any> {

        let users: Array<IUserModel> = await UserModel.find({_id: {$in: [user]}});
        let socket_ids: Array<string> = [];
        users.map((v, k, a) => {
            socket_ids = socket_ids.concat(v.web_socket_ids);
        });
        let wrong_socket_ids: Array<string> = [];
        for (let i: number = 0; i < socket_ids.length; i++) {
            if (SocketServer.io.sockets.sockets[socket_ids[i]]) {
                SocketServer.io.sockets.sockets[socket_ids[i]].emit(event, data);
                log.debug(data);
            } else {
                wrong_socket_ids.push(socket_ids[i]);
            }
        }
        if (wrong_socket_ids.length > 0) {
            for (let i: number = 0; i < wrong_socket_ids.length; i++) {
                UserModel.findOneAndUpdate({socket_ids: wrong_socket_ids[i]}, {
                    $pull: {socket_ids: wrong_socket_ids[i]}
                })
                    .catch(err => {
                        log.error("Socket emit, Error remove wrong socket ids");
                    });
            }
        }
    }

}

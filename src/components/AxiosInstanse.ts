import axios, {AxiosInstance} from "axios";

export class AxiosInstanse {
    private static _axios: AxiosInstance;

    private static init(): void {
        AxiosInstanse._axios = axios.create({
            baseURL: "https://qoyod.com/api/1.0/",
            headers: {
                "API-KEY": "6b9a7f1b8cae40fb71c732af0",
                "Content-Type": "application/json"
            }
        });
        AxiosInstanse._axios.defaults.timeout = 2500;
    }

    public static get(): AxiosInstance {
        if (!AxiosInstanse._axios) {
            AxiosInstanse.init();
        }
        return AxiosInstanse._axios;
    }
}
/**
 * Application config module
 */

export class AppConfig {
    private static _conf: any;
    private static init(): void {
        if (!AppConfig._conf) {
            let conf_prefix: string = "";
            switch (process.env.NODE_ENV) {
                case "production" :
                    conf_prefix = "_prod";
                    break;
                case "dev" :
                    conf_prefix = "_dev";
                    break;
                case "devst" :
                    conf_prefix = "_devst";
                    break;
            }
            const nconf: any = require("nconf");
            AppConfig._conf = nconf.argv()
                .env()
                .file({ file: "./config/config" + conf_prefix + ".json" });
        }
    }
    public static getInstanse(): any {
        AppConfig.init();
        return AppConfig._conf;
    }
}

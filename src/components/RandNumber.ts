export class RandNumber {
    public static ri(min: number, max: number): number {
        let rand: number = min + Math.random() * (max - min);
        rand = Math.round(rand);
        return rand;
    }
}
import {RequestInterface} from "../interfaces/RequestInterface";
import * as express from "express";
import {ApiResponse} from "./ApiResponse";
import {ApiLogModel, IApiLogModel} from "../models/logs/ApiLogModel";
import {ResponseInterface} from "../interfaces/ResponseInterface";
import {LogsConfig} from "../models/configs/LogsConfig";
import {Log} from "./Log";

const log: any = Log.getInstanse()(module);

export class ApiParser {
    public static parser(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {

        // set local
        if (req.header("Local") && req.header("Local") !== "") {
            req.setLocale(req.header("Local"));
        }

        if (req.body.params) {

            let model: IApiLogModel = new ApiLogModel();
            let logConfig: LogsConfig = new LogsConfig();
            logConfig
                .getInstanse()
                .then((conf: LogsConfig) => {
                    if (conf.api) {
                        model.params = JSON.stringify(req.params);
                        let body: string = JSON.stringify(req.body);
                        if (body && body.length > 2000) {
                            model.body = body.substring(0, 2000);
                        } else {
                            model.body = body;
                        }
                        model.headers = JSON.stringify(req.headers);
                        model.rawHeaders = JSON.stringify(req.rawHeaders);
                        model.url = req.url;
                        model.method = req.method;
                        model.query = JSON.stringify(req.query);
                        model
                            .save();
                    }
                })
                .catch(err => {
                    log.error(err);
                });


            res.apiJson = (data: any) => {
                logConfig
                    .getInstanse()
                    .then((conf: LogsConfig) => {
                        if (conf.api) {
                            model.response = JSON.stringify(data);
                            model
                                .save();
                        }
                    })
                    .catch(err => {
                        log.error(err);
                    });

                return res.json(data);
            };

            req.body = req.body.params;
            next();
        } else {
            let apiResponse: ApiResponse = new ApiResponse();
            apiResponse.addErrorMessage("request", "Error parse request");
            res.status(400);
            res.json(apiResponse.get());
        }
    }
}
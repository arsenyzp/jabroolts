/**
 * Authentication on admin panel
 */

import {RequestInterface} from "../interfaces/RequestInterface";
import * as express from "express";
import {UserRoles} from "../models/UserModel";
import {AppConfig} from "./config";

export class AdminAuth {
    public static check(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
        if (req.session.user) {
            if (req.session.user.role === UserRoles.admin
                || req.session.user.role === UserRoles.dev) {
                return next();
            }
        }
        res.redirect(AppConfig.getInstanse().get("urls:admin_login"));
    }
}
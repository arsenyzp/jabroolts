import {IUserModel, UserModel} from "../../models/UserModel";
import {IPaymentLogModel, PaymentLogModel, PaymentStatuses, PaymentTypes} from "../../models/PaymentLog";
import {Log} from "../Log";
import {JError} from "../JError";
import {IOrderModel} from "../../models/OrderModel";
import {CourierBalanceTypes, ICourierBalanceLogModel, CourierBalanceLogModel} from "../../models/CourierBalanceLogModel";
import {SmsSender} from "./SmsSender";
import {EmailSender} from "./EmailSender";
import * as cache from "memory-cache";

const log: any = Log.getInstanse()(module);

const braintree: any = require("braintree");


export class PaymentModule {
    private static _gateway: any;

    private static init(): void {
        PaymentModule._gateway = braintree.connect({
            environment: braintree.Environment.Sandbox,
            merchantId: "k2k7v3y7j8cjxbwf",
            publicKey: "cshd2zhwsb58t445",
            privateKey: "dc8f9088901f09167b1ec78253cc6dab"
        });
    }

    public static getGateway(): any {
        if (!PaymentModule._gateway) {
            PaymentModule.init();
        }
        return PaymentModule._gateway;
    }

    public static getToken(): Promise<any> {
        return new Promise((resolve, reject) => {
            PaymentModule.getGateway().clientToken.generate({}, (err, response) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(response.clientToken);
                }
            });
        });
    }

    private static findUser(bt_customer_id: string): Promise<any> {
        return new Promise((resolve, reject) => {
            PaymentModule
                .getGateway()
                .customer
                .find(bt_customer_id, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    public static listCards(bt_customer_id: string): Promise<any> {
        return PaymentModule.findUser(bt_customer_id);
    }

    public static paymentFormDefaultCard(amount: number, user: IUserModel): Promise<any> {
        return new Promise((resolve, reject) => {
            if (amount <= 0) {
                reject(new JError("Invalid amount"));
            } else {
                // save to log
                let paymentLog: IPaymentLogModel = new PaymentLogModel({
                    user: user._id,
                    type: PaymentTypes.Payment,
                    amount: amount * -1
                });

                paymentLog
                    .save()
                    .then(pLog => {
                        PaymentModule
                            .findUser(user.bt_customer_id)
                            .then(result => {

                                let found: boolean = false;
                                for (let i: number = 0; i < result.creditCards.length; i++) {

                                    if (result.creditCards[i].default) {
                                        found = true;

                                        let currency: number = parseFloat(cache.get("currency"));
                                        let usd_amount: number = amount / currency;
                                        PaymentModule
                                            .getGateway()
                                            .transaction.sale({
                                                amount: usd_amount.toFixed(2),
                                                paymentMethodToken: result.creditCards[i].token,
                                                customerId: user.bt_customer_id,
                                                options: {
                                                    submitForSettlement: true
                                                }
                                        }, (err, result) => {
                                            console.log("transaction.sale");
                                            console.log(result);
                                            if (err) {
                                                log.error(err);
                                                // update status in payment log
                                                pLog.status = PaymentStatuses.Fail;
                                                pLog.bt_result_json = JSON.stringify(result);
                                                pLog.save()
                                                    .then(l => {
                                                        reject(err);
                                                    })
                                                    .catch(e => {
                                                        log.error(e);
                                                        reject(err);
                                                    });
                                            } else if (!result.success) {
                                                log.error(result);
                                                pLog.status = PaymentStatuses.Fail;
                                                pLog.bt_result_json = JSON.stringify(result);
                                                pLog.save()
                                                    .then(l => {
                                                        reject(new Error(result.message));
                                                    })
                                                    .catch(e => {
                                                        log.error(e);
                                                        reject(err);
                                                    });
                                            } else {
                                                // update status in payment log
                                                pLog.status = PaymentStatuses.Success;
                                                pLog.bt_pay_id = result.transaction.id;
                                                pLog.bt_status = result.transaction.status;
                                                pLog.bt_type = result.transaction.bt_type;
                                                pLog.bt_currencyIsoCode = result.transaction.currencyIsoCode;
                                                pLog.bt_amount = result.transaction.amount;
                                                pLog.bt_result_json = JSON.stringify(result);
                                                pLog.save()
                                                    .then(l => {
                                                        resolve(result);
                                                    })
                                                    .catch(e => {
                                                        log.error(e);
                                                        resolve(result);
                                                    });
                                            }
                                        });
                                    }
                                }
                                if (!found) {
                                    reject(new JError("Card not found"));
                                }
                            })
                            .catch(err => {
                                reject(err);
                            });
                    })
                    .catch(err => {
                        reject(err);
                    });
            }
        });
    }

    /**
     * Pay from card
     * @param {string} uniqueNumberIdentifier
     * @param {number} amount
     * @param {IUserModel} user
     * @returns {Promise}
     */
    public static paymentFormCard(uniqueNumberIdentifier: string, amount: number, user: IUserModel): Promise<any> {
        return new Promise((resolve, reject) => {
            if (amount <= 0) {
                reject("err");
            } else {
                // save to log
                let paymentLog: IPaymentLogModel = new PaymentLogModel({
                    user: user._id,
                    type: PaymentTypes.Payment,
                    amount: amount * -1
                });

                paymentLog
                    .save()
                    .then(pLog => {
                        PaymentModule
                            .findUser(user.bt_customer_id)
                            .then(result => {
                                let found: boolean = false;
                                for (let i: number = 0; i < result.length; i++) {
                                    if (result[i].uniqueNumberIdentifier === uniqueNumberIdentifier) {
                                        found = true;
                                        found = true;
                                        PaymentModule
                                            .getGateway()
                                            .transaction.sale({
                                            amount: amount,
                                            paymentMethodToken: result[i].token,
                                            customerId: user.bt_customer_id,
                                            options: {
                                                submitForSettlement: true
                                            }
                                        }, (err, result) => {
                                            if (err) {
                                                log.error(err);
                                                // update status in payment log
                                                pLog.status = PaymentStatuses.Fail;
                                                pLog.save()
                                                    .then(l => {
                                                        reject(err);
                                                    })
                                                    .catch(e => {
                                                        log.error(e);
                                                        reject(err);
                                                    });
                                            } else if (!result.status) {
                                                log.error(err);
                                                log.error(result);
                                                pLog.status = PaymentStatuses.Fail;
                                                pLog.save()
                                                    .then(l => {
                                                        reject(err);
                                                    })
                                                    .catch(e => {
                                                        log.error(e);
                                                        reject(err);
                                                    });
                                            } else {
                                                // update status in payment log
                                                pLog.status = PaymentStatuses.Success;
                                                pLog.bt_pay_id = result.id;
                                                pLog.save()
                                                    .then(l => {
                                                        resolve(result);
                                                    })
                                                    .catch(e => {
                                                        log.error(e);
                                                        resolve(result);
                                                    });
                                            }
                                        });
                                    }
                                }
                                if (!found) {
                                    reject(new Error("Card not found"));
                                }
                            })
                            .catch(err => {
                                reject(err);
                            });
                    })
                    .catch(err => {
                        reject(err);
                    });
            }
        });
    }

    /**
     * Pay from balance
     * @param {number} amount
     * @param {IUserModel} user
     * @returns {Promise}
     */
    public static paymentFromBalance(amount: number, user: IUserModel): Promise<IUserModel> {
        return new Promise((resolve, reject) => {
            if (amount <= 0) {
                reject("err");
            } else {
                // save to log
                let paymentLog: IPaymentLogModel = new PaymentLogModel({
                    user: user._id,
                    type: PaymentTypes.Payment,
                    amount: amount * -1
                });

                paymentLog
                    .save()
                    .then(pLog => {

                        // update user balance
                        UserModel.findOneAndUpdate(
                            {_id: user._id},
                            {$inc: {balance: (-1 * amount)}},
                            {new: true})
                            .then(user => {
                                // update status in payment log
                                pLog.status = PaymentStatuses.Success;
                                pLog.save()
                                    .then(l => {
                                        resolve(user);
                                    })
                                    .catch(e => {
                                        log.error(e);
                                        resolve(user);
                                    });
                            })
                            .catch(err => {
                                log.error(err);
                                pLog.status = PaymentStatuses.Fail;
                                pLog.save()
                                    .then(l => {
                                        reject(err);
                                    })
                                    .catch(e => {
                                        log.error(e);
                                        reject(err);
                                    });
                            });
                    })
                    .catch(err => {
                        reject(err);
                    });
            }
        });
    }

    /**
     * Create customer
     * @param {string} firstName
     * @param {string} lastName
     * @param {string} email
     * @param {string} phone
     * @returns {Promise}
     */
    public static createCustomer(firstName: string, lastName: string, email: string, phone: string): Promise<any> {
        return new Promise((resolve, reject) => {
            PaymentModule
                .getGateway()
                .customer.create({
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    phone: phone
                }, (err, result) => {
                    if (err) {
                        log.error(err);
                        reject(err);
                    } else {
                        resolve(result);
                    }
                });
        });
    }

    public static createCustomerAndPayment(
        firstName: string,
        lastName: string,
        email: string,
        phone: string,
        nonceFromTheClient: string
    ): Promise<any> {
        return new Promise((resolve, reject) => {
            PaymentModule
                .getGateway()
                .customer.create({
                firstName: firstName,
                lastName: lastName,
                email: email,
                phone: phone,
                paymentMethodNonce: nonceFromTheClient
            }, (err, result) => {
                if (err) {
                    log.error(err);
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    public static createPayment(
        customerId: string,
        nonceFromTheClient: string
    ): Promise<any> {
        return new Promise((resolve, reject) => {
            PaymentModule
                .getGateway()
                .paymentMethod.create({
                customerId: customerId,
                paymentMethodNonce: nonceFromTheClient
            }, (err, result) => {
                if (err) {
                    log.error(err);
                    reject(err);
                } else {
                    PaymentModule
                        .findUser(customerId)
                        .then(r => {
                            resolve({
                                success: true,
                                customer: r
                            });
                        })
                        .catch(reject);
                }
            });
        });
    }

    /**
     * Remove user card
     * @param {string} customer_id
     * @param {string} uniqueNumberIdentifier
     * @returns {Promise}
     */
    public static removeCard(customer_id: string , uniqueNumberIdentifier: string): Promise<any> {
        return new Promise((resolve, reject) => {
            PaymentModule
                .findUser(customer_id)
                .then(result => {
                    let deleted: boolean = false;
                    for (let i: number = 0; i < result.creditCards.length; i++) {
                        if (result.creditCards[i].uniqueNumberIdentifier === uniqueNumberIdentifier) {
                            deleted = true;
                            PaymentModule
                                .getGateway()
                                .paymentMethod.delete(result.creditCards[i].token, (err) => {
                                    if (err) {
                                        log.error(err);
                                        reject(err);
                                    } else {
                                        resolve(result);
                                    }
                                });
                        }
                    }
                    if (!deleted) {
                        resolve(0);
                    }
                })
                .catch(err => {
                    log.error(err);
                    reject(err);
                });
        });
    }

    /**
     * Set default card
     * @param {string} customer_id
     * @param {string} uniqueNumberIdentifier
     * @returns {Promise}
     */
    public static setDefault(customer_id: string, uniqueNumberIdentifier: string): Promise<any> {
        console.log("PaymentModule::setDefault");
        return new Promise((resolve, reject) => {
            console.log("PaymentModule::findUser");
            PaymentModule
                .findUser(customer_id)
                .then(result => {
                    let found: boolean = false;
                    for (let i: number = 0; i < result.creditCards.length; i++) {
                        if (result.creditCards[i].uniqueNumberIdentifier === uniqueNumberIdentifier) {
                            found = true;
                            console.log("PaymentModule::getGateway.paymentMethod.update");
                            PaymentModule
                                .getGateway()
                                .paymentMethod
                                .update(
                                    result.creditCards[i].token,
                                    {
                                        options: {
                                            makeDefault: true
                                        }
                                    },
                                    (err) => {
                                        if (err) {
                                            log.error(err);
                                            reject(err);
                                        } else {
                                            resolve(1);
                                        }
                                    });
                            break;
                        }
                    }
                    if (!found) {
                        reject(new JError("Card not found"));
                    }
                })
                .catch(err => {
                    log.error(err);
                    reject(err);
                });
        });
    }

    private static correctBalance(type: string, customer_id: string, amount: number): Promise<IUserModel> {
        return new Promise((resolve, reject) => {
            // save to log
            let paymentLog: IPaymentLogModel = new PaymentLogModel({
                user: customer_id,
                type: type,
                amount: amount
            });

            paymentLog
                .save()
                .then(pLog => {
                    // update user balance
                    UserModel.findOneAndUpdate(
                        {_id: customer_id},
                        {$inc: {balance: amount}},
                        {new: true})
                        .then(user => {
                            // update status in payment log
                            pLog.status = PaymentStatuses.Success;
                            pLog.save()
                                .then(l => {
                                    resolve(user);
                                })
                                .catch(e => {
                                    log.error(e);
                                    resolve(user);
                                });
                        })
                        .catch(err => {
                            log.error(err);
                            pLog.status = PaymentStatuses.Fail;
                            pLog.save()
                                .then(l => {
                                    reject(err);
                                })
                                .catch(e => {
                                    log.error(e);
                                    reject(err);
                                });
                        });
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

    public static moneyBack(customer_id: string, amount: number): Promise<IUserModel> {
        return PaymentModule.correctBalance(PaymentTypes.Refund, customer_id, amount);
    }

    public static promoBonusAdd(customer_id: string, amount: number): Promise<IUserModel> {
        return PaymentModule.correctBalance(PaymentTypes.Promo, customer_id, amount);
    }

    public static promoCourierAdd(customer_id: string, amount: number): Promise<IUserModel> {
        return PaymentModule.correctCourierLimit(CourierBalanceTypes.Promo, customer_id, amount);
    }

    public static courierTakeUserDebt(customer_id: string, amount: number): Promise<IUserModel> {
        return PaymentModule.correctCourierJabroolFee(CourierBalanceTypes.Jabrool, customer_id, amount);
    }

    public static correctBalanceFromAdmin(customer_id: string, amount: number): Promise<IUserModel> {
        return PaymentModule.correctBalance(PaymentTypes.Admin, customer_id, amount);
    }

    public static invaiteBonus(customer_id: string, amount: number): Promise<IUserModel> {
        return PaymentModule.correctBalance(PaymentTypes.Invite, customer_id, amount);
    }

    public static async courierOrder(courier: IUserModel, order: IOrderModel): Promise<IUserModel> {
        console.log("courierOrder");
        let earning: number = order.cost - order.serviceFee;
        // save to log
        let paymentLog: IPaymentLogModel = new PaymentLogModel({
            user: courier._id,
            type: PaymentTypes.Order,
            amount: earning,
            order: order._id
        });

        await paymentLog
            .save();
        // update courier jabrool fee
        console.log("courierOrder jabrool fee " + order.serviceFee);

        switch (order.pay_type) {
            case "card":
                return await PaymentModule
                    .correctCourierJabroolFee(CourierBalanceTypes.Order, courier._id, (order.cost - order.serviceFee) * -1);
            case "cash":
                return await PaymentModule
                    .correctCourierJabroolFee(CourierBalanceTypes.Order, courier._id, (order.serviceFee - order.bonus));
        }
    }

    private static correctCourierLimit(type: string, customer_id: string, amount: number): Promise<IUserModel> {
        return new Promise((resolve, reject) => {
            // save to log
            let model: ICourierBalanceLogModel = new CourierBalanceLogModel({
                user: customer_id,
                amount: amount,
                type: type
            });
            model
                .save()
                .then(pLog => {
                    // update user balance
                    UserModel.findOneAndUpdate(
                        {_id: customer_id},
                        {$inc: {courierLimit: amount}},
                        {new: true})
                        .then(user => {
                            // TODO ?????
                            // NotificationModule.getInstance().send(
                            //     [user._id],
                            //     NotificationTypes.ReachedCashLimits,
                            //     {},
                            //     user._id
                            // );
                            // // Send Notification
                            // SmsSender.send(user.phone, "You have reached cash limits, " +
                            //     "you can not get anymore deliveries. Please, pay off Jabrool debt by" +
                            //     " creditcard or depost it to Jabrool Office.");
                            // EmailSender.sendTemplateToUser(user, "Courier balance", {}, "courier_limit_notification");
                            resolve(user);
                        })
                        .catch(err => {
                            reject(err);
                        });
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

    private static correctCourierJabroolFee(type: string, customer_id: string, amount: number): Promise<IUserModel> {
        //log.info("correctCourierJabroolFee " + type + " " + customer_id + " " + amount);
        return new Promise((resolve, reject) => {
            // save to log
            let model: ICourierBalanceLogModel = new CourierBalanceLogModel({
                user: customer_id,
                amount: amount,
                type: type
            });
            model
                .save()
                .then(pLog => {
                    // update user balance
                    UserModel.findOneAndUpdate(
                        {_id: customer_id},
                        {$inc: {jabroolFee: amount}},
                        {new: true})
                        .then(user => {

                            if (user.courierLimit <= user.jabroolFee) {
                                // Send Notification
                                SmsSender.send(user.phone, "Limit is less than 0");
                                EmailSender.sendTemplateToUser(user, "Courier limit", {}, "courier_limit_notification");
                            }

                            resolve(user);
                        })
                        .catch(err => {
                            reject(err);
                        });
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

}

import {IPromoCodModel, PromoCodModel} from "../../models/PromoCodModel";
import {RandNumber} from "../RandNumber";
import {IUserModel, UserModel} from "../../models/UserModel";
import {PaymentModule} from "./PaymentModule";

export const PromoCodeStatus: any = {
    NotFound: -10,
    Used: -20,
    Ready: 10
};

export class PromoCodHelper {

    private promo: IPromoCodModel;

    constructor(promo: IPromoCodModel) {
        this.promo = promo;
    }

    public save(): Promise<{}> {
        return new Promise((resolve, reject) => {
            if (!this.promo.code || this.promo.code === "") {
                this.generate((err, code) => {
                    if (err) {
                        reject(err);
                    } else {
                        this.promo.code = code;
                        this.promo.save()
                            .then(model => {
                                resolve(model);
                            })
                            .catch(reject);
                    }
                });
            } else {
                this.promo
                    .save()
                    .then(model => {
                        resolve(model);
                    })
                    .catch(reject);
            }
        });
    }

    private generate (callback: (err: Error, code?: string) => void): void {

        function fi(cb : (err: Error, code?: string) => void): void {
            let code: string = "PROMO" + RandNumber.ri(10000, 99999);
            PromoCodModel
                .findOne({ code: code })
                .then(item => {
                    if (item != null) {
                        fi(cb);
                    } else {
                        callback(null, code);
                    }
                })
                .catch(err => {
                    callback(err);
                });
        }

        fi(callback);
    }

    public static check(code: string, type: string): Promise<any> {
        return new Promise((resolve, reject) => {
            PromoCodModel
                .findOne({code: code})
                .then( (promo) => {
                if (promo == null) {
                    resolve({
                        status: PromoCodeStatus.NotFound,
                        promo: promo
                    });
                } else if (promo.forCourier && type !== "courier") {
                    resolve({
                        status: PromoCodeStatus.NotFound,
                        promo: promo
                    });
                } else if (!promo.forCourier && type !== "client") {
                    resolve({
                        status: PromoCodeStatus.NotFound,
                        promo: promo
                    });
                } else if (promo.is_used) {
                    resolve({
                        status: PromoCodeStatus.Used,
                        promo: promo
                    });
                } else {
                    resolve({
                        status: PromoCodeStatus.Ready,
                        promo: promo
                    });
                }
            }).catch( (err) => {
                reject(err);
            });
        });
    }

    public static block(user: IUserModel, code: string): Promise<any> {
        return new Promise((resolve, reject) => {
            PromoCodModel
                .findOne({code: code})
                .then( (promo) => {
                    if (promo == null) {
                        resolve({
                            status: PromoCodeStatus.NotFound,
                            promo: promo
                        });
                    } else if (promo.is_used) {
                        resolve({
                            status: PromoCodeStatus.Used,
                            promo: promo
                        });
                    } else {
                        // set block
                        PromoCodModel.findOneAndUpdate(
                            {code: promo.code, is_used: false},
                            {is_used: true, owner: user._id, is_blocked: true},
                            {new: true})
                            .then(promo => {
                                // update account
                                resolve({
                                    status: PromoCodeStatus.Ready,
                                    promo: promo
                                });
                            })
                            .catch(err => {
                                reject(err);
                            });
                    }
                }).catch( (err) => {
                reject(err);
            });
        });
    }

    public static unblock(user: IUserModel, code: string): Promise<any> {
        return new Promise((resolve, reject) => {
            PromoCodModel
                .findOne({_id: code})
                .then( (promo) => {
                    if (promo == null) {
                        resolve({
                            status: PromoCodeStatus.NotFound,
                            promo: promo
                        });
                    } else {
                        // set unblock
                        PromoCodModel.findOneAndUpdate(
                            {_id: promo._id, is_blocked: true, owner: user._id},
                            {is_used: false, owner: null, is_blocked: false},
                            {new: true})
                            .then(promo => {
                                console.log(promo);
                                // update account
                                resolve({
                                    status: PromoCodeStatus.Ready,
                                    promo: promo
                                });
                            })
                            .catch(err => {
                                reject(err);
                            });
                    }
                }).catch( (err) => {
                reject(err);
            });
        });
    }

    public static use(user: IUserModel, code: string, type: string): Promise<any> {
        return new Promise((resolve, reject) => {
            PromoCodModel
                .findOne({code: code})
                .then( (promo) => {
                    if (promo == null) {
                        resolve({
                            status: PromoCodeStatus.NotFound,
                            promo: promo
                        });
                    } else if (promo.forCourier && type !== "courier") {
                        resolve({
                            status: PromoCodeStatus.NotFound,
                            promo: promo
                        });
                    } else if (!promo.forCourier && type !== "client") {
                        resolve({
                            status: PromoCodeStatus.NotFound,
                            promo: promo
                        });
                    } else if (promo.is_used) {
                        resolve({
                            status: PromoCodeStatus.Used,
                            promo: promo
                        });
                    } else {
                        // set used
                        PromoCodModel.findOneAndUpdate(
                            {code: promo.code, is_used: false},
                            {is_used: true, owner: user._id},
                            {new: true})
                            .then(promo => {
                                // update account
                                if (promo.forCourier) {
                                    PaymentModule
                                        .promoCourierAdd(user._id, promo.amount)
                                        .then(user => {
                                            resolve({
                                                status: PromoCodeStatus.Ready,
                                                promo: promo
                                            });
                                        })
                                        .catch(err => {
                                            reject(err);
                                        });
                                } else {
                                    PaymentModule
                                        .promoBonusAdd(user._id, promo.amount)
                                        .then(user => {
                                            resolve({
                                                status: PromoCodeStatus.Ready,
                                                promo: promo
                                            });
                                        })
                                        .catch(err => {
                                            reject(err);
                                        });
                                }
                            })
                            .catch(err => {
                                reject(err);
                            });
                    }
                }).catch( (err) => {
                reject(err);
            });
        });
    }
}
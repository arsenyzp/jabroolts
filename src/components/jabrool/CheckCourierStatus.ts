import {UserModel} from "../../models/UserModel";
import {IOrderModel, OrderModel, OrderTypes} from "../../models/OrderModel";
import {DeliveryConfig} from "../../models/configs/DeliveryConfig";
import {SocketServer} from "../SocketServer";
import {NotificationModule, NotificationTypes} from "../NotificationModule";

export class CheckCourierStatus {
    public static getStatus(user_id: string): Promise<any> {
        return new Promise((resolve, reject) => {
            UserModel
                .findOne({_id: user_id})
                .then( (user) => {
                    if (user === null) {
                        reject(new Error("Not found"));
                    } else {
                        let d: Date = new Date();
                        new DeliveryConfig()
                            .getInstanse()
                            .then(config => {
                                let delivery_time: number = config.waiting_time * 60 * 1000;
                                let items: Array<IOrderModel> = [];
                                if (user.current_orders && user.current_orders.length > 0) {
                                    OrderModel
                                        .find({_id: {$in: user.current_orders}})
                                        .then( (orders) => {

                                            function compare(a: any, b: any): number {
                                                if (a.start_at < b.start_at) {
                                                    return -1;
                                                } else if (a.start_at > b.start_at) {
                                                    return 1;
                                                } else {
                                                    return 0;
                                                }
                                            }

                                            if (orders.length > 0) {
                                                // find first courier order
                                                let sorted_current_orders: Array<IOrderModel> = orders.sort(compare);
                                                let first_order: IOrderModel = sorted_current_orders[0];
                                                delivery_time = delivery_time - (d.getTime() - first_order.start_at);
                                                delivery_time = delivery_time < 0 ? 0 : delivery_time;

                                                /**
                                                 * Check express
                                                 */
                                                let has_express: boolean = false;
                                                for (let i: number = 0; i < orders.length; i++) {
                                                    if (orders[i].type === OrderTypes.Express) {
                                                        has_express = true;
                                                    }
                                                }

                                                /**
                                                 * Start delivery for this courier
                                                 */
                                                /**
                                                 * Check count orders and time
                                                 */
                                                if ((delivery_time < 1 || user.current_orders.length >= 10 || has_express) && !user.in_progress) {
                                                    user.in_progress = true;
                                                    UserModel.findOneAndUpdate(
                                                        {_id: user._id},
                                                        {in_progress: true},
                                                        {new : true})
                                                        .then(us => {
                                                            if (us != null) {
                                                                // send notification to courier
                                                                SocketServer.emit([us._id], "start_delivery", {

                                                                });

                                                                /**
                                                                 * Send notification to courier
                                                                 */
                                                                NotificationModule.getInstance().send(
                                                                    [us._id],
                                                                    NotificationTypes.Start,
                                                                    {
                                                                        delivery_time: delivery_time < 1,
                                                                        current_orders: user.current_orders.length >= 10,
                                                                        has_express: has_express
                                                                    },
                                                                    us._id
                                                                );
                                                            }
                                                        })
                                                        .catch(err => {
                                                            reject(err);
                                                        });
                                                }

                                                for (let i: number = 0 ; i < orders.length; i++) {
                                                    items.push(orders[i].getPublicFields());
                                                }
                                            }
                                            resolve({
                                                in_progress: user.in_progress,
                                                accept_in_progress: user.accept_in_progress,
                                                delivery_time: delivery_time,
                                                orders: items,
                                                courier: user.getApiPublicFields()
                                            });
                                        })
                                        .catch( (err) => {
                                            reject(err);
                                        });
                                } else {

                                    UserModel.findOneAndUpdate({_id: user._id}, {
                                        in_progress: false,
                                        accept_in_progress: false
                                    }, {new: true})
                                        .then(updatedUser => {
                                            resolve({
                                                in_progress: updatedUser.in_progress,
                                                accept_in_progress: updatedUser.accept_in_progress,
                                                delivery_time: delivery_time,
                                                orders: [],
                                                courier: updatedUser
                                            });
                                        })
                                        .catch(err => {
                                            reject(err);
                                        });

                                }
                            })
                            .catch(err => {
                                reject(err);
                            });
                    }
                })
                .catch( (err) => {
                    reject(err);
                });
        });
    }
}
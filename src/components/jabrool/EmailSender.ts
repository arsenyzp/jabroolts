import {AppConfig} from "../config";
import * as nodemailer from "nodemailer";
import * as path from "path";
import {IUserModel} from "../../models/UserModel";
import {Log} from "../Log";
import {IEmailLogModel, EmailLogModel} from "../../models/logs/EmailLogModel";
import {LogsConfig} from "../../models/configs/LogsConfig";

const transporter: any = nodemailer.createTransport(AppConfig.getInstanse().get("mail_config"));
const EmailTemplate: any = require("email-templates").EmailTemplate;
const log: any = Log.getInstanse()(module);


export class EmailSender {

    public static sendTemplateToUser(user: IUserModel, subject: string, data: any, template: string): void {
        let lang: string = user.local;
        if (["en", "ar"].indexOf(lang) < 0) {
            lang = "en";
        }
        const templateDir: string = path.join(__dirname, "../../../src/views/mail", template + "_" + lang);
        const tmpl: any = new EmailTemplate(templateDir);

        const variables: any = {user: user, data: data};
        tmpl.render(variables, (err, result) => {
            if (err) {
                log.error(err);
            } else {
                this.send(user.email, subject, result.html, result.text);
            }
        });
    }

    private static send(email: string, subject: string, html: string, text: string): void {
        log.debug("send email " + text + " to " + email);

        let logConfig: LogsConfig = new LogsConfig();
        logConfig
            .getInstanse()
            .then((conf: LogsConfig) => {
                if (conf.email) {
                    let logModel: IEmailLogModel = new EmailLogModel();
                    logModel.email = email;
                    logModel.subject = subject;
                    logModel.html = html;
                    logModel.text = text;
                    logModel.save();
                }
            })
            .catch(err => {
                log.error(err);
            });

        const mailOptions: any = {
            from: AppConfig.getInstanse().get("mail_sender"), // sender address
            to: email, // list of receivers
            subject: subject, // Subject line
            text: text ? text : html, // plaintext body
            html: html // html body
        };

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            } else {
                log.debug("Message sent: " + info.response);
            }
        });
    }
}
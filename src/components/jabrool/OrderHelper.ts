import {IOrderModel, OrderModel, OrderStatuses, OrderTypes} from "../../models/OrderModel";
import {IUserModel, UserModel} from "../../models/UserModel";
import {SocketServer} from "../SocketServer";
import {PaymentModule} from "./PaymentModule";
import {NotificationModule, NotificationTypes} from "../NotificationModule";
import {CheckCourierStatus} from "./CheckCourierStatus";
import {CarOverloadError, CarOverloadTypes} from "../CarOverloadError";
import {PackageRatioConfig} from "../../models/configs/PackageRatioConfig";
import {CarTypeModel} from "../../models/CarTypeModel";
import {CostParts} from "../CostParts";
import {IPaymentLogModel, PaymentTypes, PaymentLogModel, PaymentStatuses} from "../../models/PaymentLog";
import {PriceConfig} from "../../models/configs/PriceConfig";
import {NotificationModel} from "../../models/NotificationModel";
import {OnlineLogModel} from "../../models/OnlineLogModel";
import {Log} from "../Log";
import {EVENT_MOB_USER_REGISTER, EVENT_ORDER_PAID} from "../events/Events";
import * as EventBus from "eventbusjs";

const log: any = Log.getInstanse()(module);

export interface ResultOrder {
    order: IOrderModel;
    cost_parts: CostParts;
}

export class OrderHelper {
    private static EventBus: any;

    public static getConfirmCode(): string {
        return Math.floor(111111 + Math.random() * (999999 - 111111)) + "";
    }

    public static payOrder(user: IUserModel, order: IOrderModel, cost_parts: CostParts): Promise<ResultOrder> {
        return new Promise((resolve, reject) => {
            OrderModel.findOneAndUpdate(
                {_id: order._id},
                {
                    status: order.status === OrderStatuses.WaitCourierReturnConfirmCode ?
                        OrderStatuses.WaitCourierReturnConfirmCode :
                        OrderStatuses.WaitCourierPickUpConfirmCode,
                    paid: true,
                    bt_pay_id: user.bt_customer_id,
                    cash: cost_parts.cash,
                    card: cost_parts.card,
                    bonus: cost_parts.balance
                },
                {new: true})
                .then(doc => {
                    let price: number = cost_parts.cash + cost_parts.card + cost_parts.balance;
                    EventBus.dispatch(EVENT_ORDER_PAID, this, {user: user, order: doc, price: price});
                    // save to log
                    let paymentLog: IPaymentLogModel = new PaymentLogModel({
                        user: order.courier,
                        type: PaymentTypes.Order,
                        amount: cost_parts.courier,
                        status: PaymentStatuses.Success
                    });
                    paymentLog
                        .save()
                        .then(_ => {
                            resolve({
                                order: doc,
                                cost_parts: cost_parts
                            });
                        })
                        .catch(err => {
                            reject(err);
                        });
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

    public static async paymentProcessNew(user: IUserModel, order: IOrderModel, cost: number): Promise<IOrderModel> {

        let cost_parts: CostParts = new CostParts();
        let userCredit: number = 0;

        // Check balance
        if (user.balance < 0) {
            // add to order cost
            userCredit = user.balance;
        }

        if (order.use_bonus) {
            if (user.pay_type === "card") {
                if (user.balance > 0) {
                    if (user.balance >= cost) {
                        cost_parts.balance =  cost;
                    } else {
                        cost_parts.balance = user.balance;
                        cost_parts.card = cost - user.balance;
                    }
                } else {
                    cost_parts.card = cost;
                }
            }
            if (user.pay_type === "cash") {
                if (user.balance > 0) {
                    if (user.balance >= cost) {
                        cost_parts.balance =  cost;
                    } else {
                        cost_parts.balance = user.balance;
                        cost_parts.cash = cost - user.balance;
                    }
                } else {
                    cost_parts.cash = cost;
                }
            }
        } else {
            if (user.pay_type === "card") {
                cost_parts.card = cost;
            }
            if (user.pay_type === "cash") {
                cost_parts.cash = cost;
            }
        }

        /**
         * !!!!!!! 3,5%
         */
        // Card percent
        if (cost_parts.card > 0) {
            cost_parts.card += 0.035 * cost_parts.card;
            if (userCredit !== 0) {
                cost_parts.card += userCredit * -1;
            }
        }
        let configPrice: PriceConfig = await new PriceConfig().getInstanse();
        if (order.type === OrderTypes.Jabrool) {
            cost_parts.courier = (cost_parts.cash + cost_parts.balance + cost_parts.card) -
                ((cost_parts.cash + cost_parts.balance + cost_parts.card) * configPrice.j_percent) / 100;
        } else {
            cost_parts.courier = (cost_parts.cash + cost_parts.balance + cost_parts.card) -
                ((cost_parts.cash + cost_parts.balance + cost_parts.card) * configPrice.e_percent) / 100;
        }

        if (order.use_bonus && user.balance > 0) {
            user = await PaymentModule
                .paymentFromBalance(cost_parts.balance, user);
        } else {
            // check user Debt
            if (userCredit !== 0) {

                await PaymentModule
                    .courierTakeUserDebt(order.courier.toString(), (-1 * userCredit));

                // reset user debt
                user = await UserModel
                    .findOneAndUpdate(
                        {_id: user._id},
                        {$inc: {balance: (-1 * userCredit)}
                        });

                await NotificationModel.update(
                    {user: order.owner, type: {$in: [
                        NotificationTypes.JabroolDebt
                    ]}}, {is_view: true},
                    {multi: true});
            }

            order = await OrderModel
                .findOneAndUpdate(
                    {_id: order._id},
                    {userCredit: userCredit},
                    {new: true}
                );
        }

        switch (order.pay_type) {
            case "card":
                await PaymentModule.paymentFormDefaultCard(cost_parts.card, user);
                break;
            case "cash":
                console.log("Pay from cash");
                break;
            default :
                throw new Error("Pay error");
        }

        let result: ResultOrder = await OrderHelper
            .payOrder(user, order, cost_parts);
        return result.order;
    }

    public static removeOrderFromCourier(orderCanceled: IOrderModel): Promise<IOrderModel> {
        console.log("removeOrderFromCourier", orderCanceled._id);
        return new Promise((resolve, reject) => {
            UserModel.findOneAndUpdate(
                {current_orders: orderCanceled._id},
                {$pull: {current_orders: orderCanceled._id}},
                {new: true})
                .then( us => {
                    if (us && us.current_orders.length < 1) {
                        UserModel.findOneAndUpdate({_id: us._id}, {
                            in_progress: false,
                            accept_in_progress: false
                        }).then(_ => {
                            resolve(orderCanceled);
                        }).catch(err => {
                            reject(err);
                        });
                    } else {
                        resolve(orderCanceled);
                    }
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

    public static compare(a: IOrderModel, b: IOrderModel): number {
        if (a.start_at < b.start_at) {
            return -1;
        } else  if (a.start_at > b.start_at) {
            return 1;
        } else {
            return 0;
        }
    }

    public static acceptToCourier(courier: IUserModel, doc: IOrderModel): Promise<any> {
        return new Promise((resolve, reject) => {
            UserModel
                .findOneAndUpdate(
                    {_id: courier._id},
                    {$push: {current_orders: doc._id}},
                    {new: true}
                )
                .then(user => {
                    // send notification to customer

                    NotificationModule.getInstance().send(
                        [doc.owner.toString()],
                        NotificationTypes.RequestAccepted,
                        {},
                        doc._id.toString()
                    );

                    OnlineLogModel
                        .findOne({user: user._id})
                        .sort({created_at: -1})
                        .then(l => {
                            l.type = "order";
                            l.save().then(_ => {
                                log.info("saved in online log");
                            }).catch(err => {
                                log.error(err);
                            });
                        })
                        .catch(err => {
                            log.error(err);
                        });

                    CheckCourierStatus.getStatus(user.id)
                        .then(result => {
                            let owner_id: string = doc.owner._id ? doc.owner._id.toString() : doc.owner.toString();
                            SocketServer.emit([owner_id], "request_accept", {
                                order: doc.getPublicFields(),
                                courier: user.getApiFields(),
                                pickup_time: result.delivery_time
                            });
                            resolve(result);
                        })
                        .catch( (err) => {
                           reject(err);
                        });
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

    public static checkOverloadCar(courier: IUserModel, order: IOrderModel): Promise<CarOverloadError> {
        return new Promise((resolve, reject) => {
            if (courier.current_orders && courier.current_orders.length > 0) {
                // find first courier order
                OrderModel
                    .find({_id: {$in: courier.current_orders}})
                    .then( (orders) => {
                        /**
                         * Check load car
                         */
                        let load_small: number = 0;
                        let load_medium: number = 0;
                        let load_large: number = 0;

                        let load_small_in_small: number = 0;
                        let load_medium_in_small: number = 0;
                        let load_large_in_small: number = 0;
                        for (let i: number = 0; i < orders.length; i++) {
                            load_small += orders[i].small_package_count;
                            load_medium += orders[i].medium_package_count;
                            load_large += orders[i].large_package_count;
                        }

                        let packageConfigModel: PackageRatioConfig = new PackageRatioConfig();
                        packageConfigModel
                            .getInstanse()
                            .then(packageConfig => {
                                load_small_in_small = load_small * packageConfig.small;
                                load_medium_in_small = load_medium * packageConfig.medium;
                                load_large_in_small = load_large * packageConfig.large;
                                CarTypeModel
                                    .findOne({_id: courier.vehicle.type})
                                    .then( (car_type) => {
                                        if (car_type != null) {
                                            if (order.small_package_count +
                                                load_medium_in_small +
                                                load_large_in_small > car_type.size) {
                                                resolve(new CarOverloadError(CarOverloadTypes.TOTAL, "Сar overload"));
                                            } else {
                                                resolve(new CarOverloadError(CarOverloadTypes.NONE, "OK"));
                                            }
                                        } else {
                                            resolve(new CarOverloadError(CarOverloadTypes.NONE, "OK"));
                                        }
                                    })
                                    .catch(err => {
                                        reject(err);
                                    });
                            })
                            .catch(err => {
                               reject(err);
                            });
                    })
                    .catch( (err) => {
                        reject(err);
                    });
            } else {
                resolve(new CarOverloadError(CarOverloadTypes.NONE, "OK"));
            }
        });
    }

    public static getRandCharset(): string {
        let text: string = "";
        let possible: string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        for (let i: number = 0; i < 3; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    }

    private static getRandCode(): string {
        return Math.floor(111 + Math.random() * (999 - 111)) + "";
    }

    public static async generateOrderId(type: string = "jabrool"): Promise<string> {
        let count: number = await OrderModel.count({});
        let OrderID: string = "";
        switch (type) {
            case "jabrool":
                OrderID += "J";
                break;
            case "express":
                OrderID += "E";
                break;
        }
        OrderID += count;
        OrderID += OrderHelper.getRandCode();
        return OrderID;
    }
}
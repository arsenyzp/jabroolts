import {IUserModel, UserModel} from "../../models/UserModel";
import {ILocationFeedSubscriberModel, LocationFeedSubscriberModel} from "../../models/LocationFeedSubscriberModel";
import {IOrderModel, OrderModel, OrderStatuses} from "../../models/OrderModel";
import {CarTypeModel, ICarTypeModelModel} from "../../models/CarTypeModel";
import {SocketServer} from "../SocketServer";
import * as cache from "memory-cache";
import {Log} from "../Log";
import {NotificationModule, NotificationTypes} from "../NotificationModule";
import {PackageRatioConfig} from "../../models/configs/PackageRatioConfig";

const log: any = Log.getInstanse()(module);

const test: boolean = false;

export class CourierLocationHandler {

    public static async process(courier: IUserModel, lat: number, lon: number):  Promise<any> {

        let query: any = {};
        query.location = {
            $near: {
                $geometry: {
                    type: "Point",
                    coordinates: [lat, lon]
                },
                $maxDistance: 1000
            }
        };

        /**
         * Only if courier visible and courier not in progress
         */
        if (courier.visible && !courier.in_progress && !test) {

            /**
             * Send location to subscribers
             */
            let users: Array<ILocationFeedSubscriberModel> = await LocationFeedSubscriberModel.find(query).lean() as Array<any>;
            let users_ids: Array<string> = [];
            for (let i: number = 0; i < users.length; i++) {
                users_ids.push(users[i].user);
            }

            /**
             * Get latest subscribers
             */
            let last_users_ids: Array<string> = cache.get("subscriber_ids_" + courier._id.toString());
            if (last_users_ids && last_users_ids.length > 0) {
                let difference_users_ids: Array<string> = last_users_ids
                    .filter(x => users_ids.indexOf(x) === -1);
                let subscribers: Array<ILocationFeedSubscriberModel> = await LocationFeedSubscriberModel
                    .find({user: {$in: difference_users_ids}}).lean() as Array<any>;

                let notify_ids: Array<string> = [];
                for (let j: number = 0; j < subscribers.length; j++) {
                    notify_ids.push(subscribers[j].user);
                }
                if (notify_ids.length > 0) {
                    SocketServer.emit(notify_ids, "courier_disconnect", {
                        name: courier.first_name + "_" + courier.last_name,
                        jabroolid: courier.jabroolid,
                        courier: courier.getApiFields(),
                        courier_remove: "radius"
                    });
                }
            }

            if (users_ids.length > 0) {
                /**
                 * Save subscribers list to cache
                 */
                cache.put("subscriber_ids_" + courier._id.toString(), users_ids, 5000);
                SocketServer.emit(users_ids, "locations", {
                    name: courier.first_name + "_" + courier.last_name,
                    jabroolid: courier.jabroolid,
                    courier: courier.getApiFields(),
                    lat: lat,
                    lon: lon
                });
            } else {
                cache.put("subscriber_ids_" + courier._id.toString(), [], 5000);
            }

            /**
             * Find available new orders
             */
            let query_orders: any = {};
            query_orders.owner_location = {
                $near: {
                    $geometry: {
                        type: "Point",
                        coordinates: [lon, lat]
                    },
                    $maxDistance: 10000
                }
            };
            query_orders.status = OrderStatuses.New;
            query_orders.notified_couriers = {$ne: courier._id};
            query_orders.canceled_couriers = {$ne: courier._id};
            let orders: Array<IOrderModel> = await OrderModel
                .find(query_orders).lean() as Array<any>;

            log.debug("Courier " + courier.first_name);
            log.debug("New orders " + orders.length);

            let type: ICarTypeModelModel = cache.get("car_etype_" + courier.vehicle.type.toString());
            if (!type) {
                //check courier vehicle
                type = await CarTypeModel
                    .findOne({_id: courier.vehicle.type}).lean() as any;
                cache.put("car_etype_" + courier.vehicle.type.toString(), type, 500000);
            }

            let packageRatioConfig: PackageRatioConfig = await new PackageRatioConfig().getInstanse();
            let totalPackages: number = 0;

            const courierOrders: Array<IOrderModel> = await OrderModel
                .find({courier: courier._id, status: {$nin: [OrderStatuses.Finished, OrderStatuses.Canceled]}})
                .lean() as Array<any>;
            courierOrders.map((v, k, a) => {
                totalPackages += v.small_package_count * packageRatioConfig.small +
                    v.medium_package_count * packageRatioConfig.medium +
                    v.large_package_count * packageRatioConfig.large;
            });

            log.debug("Courier car loading " + totalPackages);

            if (type !== null) {
                let data: Array<any> = [];
                for (let i: number = 0; i < orders.length; i++) {
                    let v: IOrderModel = orders[i];
                    /***
                     *
                     * CHECK LOADING COURIER CAR
                     *
                     */
                    let totalPackagesC: number = v.small_package_count * packageRatioConfig.small +
                        v.medium_package_count * packageRatioConfig.medium +
                        v.large_package_count * packageRatioConfig.large;

                    log.debug("order packages " + totalPackagesC);
                    log.debug("type size " + type.size);

                    if (type.size >= totalPackagesC + totalPackages) {
                        // add courier id to order
                       let cc: IOrderModel = await OrderModel.findOneAndUpdate({
                            _id: v._id
                        }, {
                            $push: {
                                notified_couriers: courier._id
                            }
                        }, {new: true});
                        data.push(cc.getPublicFields());
                    }

                    if (data.length >= 10 ) {
                       log.debug("Over 10 orders available");
                       break;
                    }
                }
                // send event
                if (data.length > 0) {
                    SocketServer.emit([courier._id], "new_requests", data);
                }
            } else {
                SocketServer.emit([courier._id], "socket_error", {"message": "Type vehicle not selected"});
            }
        }

        /**
         * Send location courier to him customers
         */
        if (courier.in_progress && !test) {
            let orders: Array<IOrderModel> = await OrderModel
                .find({_id: {$in: courier.current_orders}})
                .lean() as Array<any>;

            let users_ids: Array<string> = [];
            for (let i: number = 0; i < orders.length; i++) {
                if (users_ids.indexOf(orders[i].owner.toString()) < 0) {
                    users_ids.push(orders[i].owner.toString());
                }
            }
            if (users_ids.length > 0) {
                SocketServer.emit(users_ids, "locations", {
                    name: courier.first_name + "_" + courier.last_name,
                    jabroolid: courier.jabroolid,
                    courier: courier.getApiFields(),
                    lat: lat,
                    lon: lon
                });
            }

            /**
             * Check distance to recipient
             */
            let query_orders_in_progress: any = {};
            query_orders_in_progress.recipient_location = {
                $near: {
                    $geometry: {
                        type: "Point",
                        coordinates: [lon, lat]
                    },
                    $maxDistance: 1000
                }
            };
            query_orders_in_progress.status = OrderStatuses.InProgress;
            query_orders_in_progress.courier = courier._id;
            query_orders_in_progress.near_courier_notification = {$in: [0, null]};
            let ordersForNotifications: Array<IOrderModel> = await OrderModel
                .find(query_orders_in_progress).lean() as Array<any>;
            if (ordersForNotifications.length > 0) {
                ordersForNotifications.map(async (v, k, a) => {
                    let order: IOrderModel = await OrderModel.findOneAndUpdate({
                            _id: v._id
                        },
                        {
                            $inc: {near_courier_notification: 1}
                        },
                        {new: true}
                    );
                    if (order.recipient) {
                        NotificationModule.getInstance().send(
                            [order.recipient.toString()],
                            NotificationTypes.CourierIsNearDestination,
                            {},
                            order._id.toString());
                    }
                });
            } else {
                log.info(courier.phone + " Order near recipient - 0");
            }

            /**
             * Check distance to owner
             */
            let query_orders_wait_pick_up: any = {};
            query_orders_wait_pick_up.owner_location = {
                $near: {
                    $geometry: {
                        type: "Point",
                        coordinates: [lon, lat]
                    },
                    $maxDistance: 1000
                }
            };
            query_orders_wait_pick_up.status = OrderStatuses.WaitPickUp;
            query_orders_wait_pick_up.courier = courier._id;
            query_orders_wait_pick_up.near_owner_notification = {$in: [0, null]};
            let ordersNotificationOwner: Array<IOrderModel> = await OrderModel
                .find(query_orders_wait_pick_up).lean() as Array<any>;
            if (ordersNotificationOwner.length > 0) {
                ordersNotificationOwner.map(async (v, k, a) => {
                    let order: IOrderModel = await OrderModel.findOneAndUpdate({
                            _id: v._id
                        },
                        {
                            $inc: {near_owner_notification: 1}
                        },
                        {new: true}
                    );
                    NotificationModule.getInstance().send(
                        [order.owner.toString()],
                        NotificationTypes.CourierIsNearAtPickUp,
                        {},
                        order._id.toString());
                });
            } else {
                log.info(courier.phone + " Order near recipient - 0");
            }
        }

    }
}

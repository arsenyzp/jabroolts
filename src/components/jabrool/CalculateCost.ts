import {PackageConfigPrice} from "../../models/configs/PackageConfigPrice";
import {PriceConfig} from "../../models/configs/PriceConfig";

export class CalculateCost {

    public static process(
        route: number,
        small_package_count: number,
        medium_package_count: number,
        large_package_count: number
    ): Promise<any> {
        return new Promise((resolve, reject) => {
            let configModel: PackageConfigPrice = new PackageConfigPrice();
            configModel
                .getInstanse()
                .then(config => {

                    let configPrice: PriceConfig = new PriceConfig();
                    configPrice
                        .getInstanse()
                        .then(price => {
                            let j_cost: number = (route  * small_package_count * config.small.j_cost) +
                                (route  * medium_package_count * config.medium.j_cost + (route  * large_package_count * config.large.j_cost));
                            j_cost += price.j_fix_cost;
                            let j_serviceFee: number = j_cost * price.j_percent / 100;
                            j_cost += j_serviceFee;


                            if (j_cost < price.j_min_cost) {
                                j_cost = price.j_min_cost;
                                j_serviceFee = j_cost * price.j_percent / 100;
                            }

                            let e_cost: number = (route  * small_package_count * config.small.e_cost) +
                                (route  * medium_package_count * config.medium.e_cost + (route  * large_package_count * config.large.e_cost));
                            e_cost += price.e_fix_cost;
                            let e_serviceFee: number = e_cost * price.e_percent / 100;
                            e_cost += e_serviceFee;

                            if (e_cost < price.e_min_cost) {
                                e_cost = price.e_min_cost;
                                e_serviceFee = e_cost * price.e_percent / 100;
                            }

                            resolve({
                                e_cost: e_cost.toFixed(2),
                                j_cost: j_cost.toFixed(2),
                                e_serviceFee: e_serviceFee.toFixed(2),
                                j_serviceFee: j_serviceFee.toFixed(2)
                            });
                        })
                        .catch(err => {
                            reject(err);
                        });

                })
                .catch(err => {
                    reject(err);
                });
        });
    }

}
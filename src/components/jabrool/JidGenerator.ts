import {IUserModel, UserModel} from "../../models/UserModel";
import {RandNumber} from "../RandNumber";

export class JidGenerator {

    private static getRendCharset(): string {
        let text: string = "";
        let possible: string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        for (let i: number = 0; i < 3; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    }

    public static gen(user: IUserModel): Promise<{}> {
        return new Promise((resolve, reject) => {

            let name: string = JidGenerator.getRendCharset();
            let fi: () => void  = () => {
                let jabroolid: string = RandNumber.ri(1000, 9999) + "";
                jabroolid = name + RandNumber.ri(10000, 99999);
                UserModel.findOne({ jabroolid: jabroolid })
                    .then(item => {
                        if (item != null) {
                            fi();
                        } else {
                            user.jabroolid = jabroolid;
                            resolve();
                        }
                    })
                    .catch(err => {
                        reject(err);
                    });
            };

            if (name.length > 4) {
                name = name.substring(0, 3);
            }
            name = name.toString().toUpperCase();

            fi();
        });
    }
}
/**
 * Send sms
 * experttexting.com
 *
 * Sample URL

 https://www.experttexting.com/ExptRestApi/sms/json/Message/Send?
 username=starcity&password=StarCity123&api_key=sswmp8r7l63y&from=BRANDSMS&to=123456789&text=Hello&type=text

 JSON Response (Success)

 {
    "Response": {
       "message_id": "671729375",
       "message_count": 1,
       "price": 0.0085
    },
    "ErrorMessage": "",
    "Status": 0
 }
 */
import {Log} from "../Log";
import * as request from "request";
import * as querystring from "querystring";
import {ISmsLogModel, SmsLogModel} from "../../models/logs/SmsLogModel";
import {LogsConfig} from "../../models/configs/LogsConfig";
import {AppConfig} from "../config";

const log: any = Log.getInstanse()(module);

export class SmsSender {

    public static send (phone: string, text: string): void {
        let url: string = "https://www.experttexting.com/ExptRestApi/sms/json/Message/Send?username=jabrool&";
        let form: any = {
            password: "ABC12345",
            api_key: "w84ccyknuehi7g6",
            from: "jabrool",
            to: "+" + parseInt(phone),
            text: text
        };

        let logModel: ISmsLogModel = new SmsLogModel();
        let logConfig: LogsConfig = new LogsConfig();
        logConfig
            .getInstanse()
            .then((conf: LogsConfig) => {
                if (conf.sms) {
                    logModel.text = text;
                    logModel.phone = phone;
                    logModel.save();
                }
            })
            .catch(err => {
                log.error(err);
            });

        if (!AppConfig.getInstanse().get("enable_sms")) {
            return;
        }

        let formData: string = querystring.stringify(form);
        request(
            {
                uri: url + formData,
                method: "GET"
            },  (error, res, body) => {
                if (!error) {
                    try {
                        let json: any = JSON.parse(body);
                        if (json.Status !== 0) {
                            log.error("Sms error send to " + phone);
                        } else {
                            log.debug("Sms success send to " + phone);

                            let logConfig: LogsConfig = new LogsConfig();
                            logConfig
                                .getInstanse()
                                .then((conf: LogsConfig) => {
                                    if (conf.sms) {
                                        logModel.phone = phone;
                                        logModel.mid = json.message_id;
                                        logModel.cost = json.price;
                                        logModel.save();
                                    }
                                })
                                .catch(err => {
                                    log.error(err);
                                });
                        }
                    } catch (e) {
                        logModel.phone = phone;
                        logModel.mid = e.message;
                        logModel.save();
                    }
                } else {
                    logModel.phone = phone;
                    logModel.mid = "error";
                    logModel.save();
                }
            }
        );
    }

}
export const CarOverloadTypes: any = {
  SMALL: "small",
  MEDIUM: "medium",
  LARGE: "large",
  TOTAL: "total",
  NONE: "none"
};

export class CarOverloadError {

    public type: string;
    public message: string;

    constructor(type: string, message: string) {
        this.type = type;
        this.message = message;
    }
}
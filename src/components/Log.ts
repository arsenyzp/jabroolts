
export class Log {
    private static _log: any;
    private static init(): void {
        if (!Log._log) {
            const winston: any = require("winston");
            Log._log = module => {
                const path: string = module.filename.split("/").slice(-2).join("/"); //file name when created message
                return new winston.Logger({
                    transports : [
                        new winston.transports.Console({
                            colorize:   true,
                            level:      "debug",
                            label:      path
                        })
                    ]
                });
            };
        }
    }
    public static getInstanse(): any {
        Log.init();
        return Log._log;
    }
}
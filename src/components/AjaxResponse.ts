import {Dictionary, MappedError} from "express-validator";
import {RequestInterface} from "../interfaces/RequestInterface";
import {ValidationError} from "class-validator";

export class AjaxResponse {
    private data: any = {};
    private errors: Array<string> = [];

    public addError (error: any): void {
        this.errors.push(error.message);
    }

    public addErrorMessage (error: string): void {
        this.errors.push(error);
    }

    public addErrors (errors: Array<Error>): void {
        errors.forEach( (error, key, array) => {
            this.errors.push(error.message);
        });
    }

    public setDate (data: any): void {
        this.data = data;
    }

    public validation (req: RequestInterface): boolean {
        let mappedErrors: Dictionary<MappedError> = req.validationErrors(true);
        if (mappedErrors) {
            for (let i of Object.keys(mappedErrors)) {
                this.addError(new Error(mappedErrors[i].msg));
            }
            return false;
        }
        return true;
    }

    public addValidationErrors (req: RequestInterface, errors: Array<ValidationError>): void {
        for (let i of Object.keys(errors)) {
            for (let j of Object.keys(errors[i].constraints)) {
                let item: Error = new Error(req.__(errors[i].constraints[j]));
                this.addError(item);
            }
        }
    }

    public get(): AjaxResponseObject {
        return new AjaxResponseObject(this.data, this.errors);
    }
}

export class AjaxResponseObject {
    public data: any;
    public errors: any;

    constructor(data: any, errors: any) {
        this.data = data;
        this.errors = errors;
    }
}
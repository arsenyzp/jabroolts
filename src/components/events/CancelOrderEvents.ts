import * as EventBus from "eventbusjs";
import {
    EVENT_ORDER_CANCELED, EVENT_ORDER_REQUEST_CANCELED
} from "./Events";
import {Log} from "../Log";
import {SocketServer} from "../SocketServer";
import {IOrderModel} from "../../models/OrderModel";
const log: any = Log.getInstanse()(module);

export class CancelOrderEvents {
    public static init(): void {
        EventBus.addEventListener(EVENT_ORDER_REQUEST_CANCELED, (params, arg) => {
            log.info(EVENT_ORDER_REQUEST_CANCELED);
            // console.log(params);
            // console.log(arg);

            let orderCanceled: IOrderModel = arg.order;
            //send notification to couriers
            let notified_couriers: Array<string> = [];
            for (let i: number = 0; i < orderCanceled.notified_couriers.length; i++) {
                notified_couriers.push(orderCanceled.notified_couriers[i].toString());
            }
            if (orderCanceled.courier) {
                notified_couriers = notified_couriers.concat([orderCanceled.courier.toString()]);
            }
            SocketServer.emit(notified_couriers, "request_canceled", {
                order: orderCanceled.getPublicFields()
            });

        });
    }
}
import * as EventBus from "eventbusjs";
import {
    EVENT_ORDER_PAID
} from "./Events";
import {Log} from "../Log";
import {eid, jid, QoyodComponent} from "../QoyodComponent";
const log: any = Log.getInstanse()(module);

export class PayOrderEvent {
    public static init(): void {
        EventBus.addEventListener(EVENT_ORDER_PAID, async (params, arg) => {
            log.info(EVENT_ORDER_PAID);
            // console.log(params);
            // console.log(arg);

        });
    }
}
import * as EventBus from "eventbusjs";
import {
    EVENT_ORDER_FINISHED,
    EVENT_ORDER_PAID
} from "./Events";
import {Log} from "../Log";
import {eid, jid, QoyodComponent} from "../QoyodComponent";
const log: any = Log.getInstanse()(module);

export class FinishOrderEvent {
    public static init(): void {
        EventBus.addEventListener(EVENT_ORDER_FINISHED, async (params, arg) => {
            log.info(EVENT_ORDER_FINISHED);
            // console.log(params);
            // console.log(arg);

            let invoice: number = await QoyodComponent.createInvoice(
                arg.user.qo_customer_id,
                arg.order.orderId,
                "Draft",
                (arg.order.type === "jabrool" ? jid : eid),
                arg.price
            );

        });
    }
}
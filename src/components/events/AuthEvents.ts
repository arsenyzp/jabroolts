import * as EventBus from "eventbusjs";
import * as i18n from "i18n";
import {EVENT_MOB_USER_CONFIRM_PHONE, EVENT_MOB_USER_LOGIN, EVENT_MOB_USER_REGISTER, EVENT_MOB_USER_RESTORE_PASSWORD} from "./Events";
import {IUserModel, UserModel} from "../../models/UserModel";
import {SmsSender} from "../jabrool/SmsSender";
import {EmailSender} from "../jabrool/EmailSender";
import {Log} from "../Log";
import {AppConfig} from "../config";
import {QoyodComponent} from "../QoyodComponent";
const log: any = Log.getInstanse()(module);

export class AuthEvents {
    public static init(): void {
        EventBus.addEventListener(EVENT_MOB_USER_LOGIN, (params, arg) => {
            log.info(EVENT_MOB_USER_LOGIN);
            // console.log(params);
            // console.log(arg);
            let user: IUserModel = arg.user;
            UserModel
                .findOneAndUpdate({_id: user._id}, {login_at: new Date().getTime()})
                .then(user => {
                    log.info(EVENT_MOB_USER_LOGIN + " set user last login");
                })
                .catch(err => {
                    log.error(err);
                });
        });
        EventBus.addEventListener(EVENT_MOB_USER_REGISTER, async (params, arg) => {
            log.info(EVENT_MOB_USER_REGISTER);
            // console.log(params);
            // console.log(arg);

            try {
                let customer: number = await QoyodComponent.createCustomer(
                    arg.user.first_name + " " + arg.user.last_name,
                    arg.user.phone,
                    arg.user.email
                );
                await UserModel.findOneAndUpdate({
                    _id: arg.user._id
                }, {
                    qo_customer_id: customer
                });
            } catch (e) {
                console.log(e);
            }

            let user: IUserModel = arg.user;
            // send confirm sms
            if (user.phone !== "") {
                SmsSender.send(user.phone, i18n.__("api.sms.ConfirmCode", user.confirm_sms_code));
            }
            // send email
            EmailSender.sendTemplateToUser(
                user,
                i18n.__("api.email.SubjectSuccessRegister"),
                {url: AppConfig.getInstanse().get("base_url")},
                "registration"
            );
        });
        EventBus.addEventListener(EVENT_MOB_USER_CONFIRM_PHONE, async (params, arg) => {
            log.info(EVENT_MOB_USER_CONFIRM_PHONE);
            // console.log(params);
            // console.log(arg);

            // try {
            //     let customer: any = await QoyodComponent.createCustomer(
            //         arg.user.first_name + " " + arg.user.last_name,
            //         arg.user.phone,
            //         arg.user.email);
            // } catch (e) {
            //     console.log(e);
            // }
        });

        EventBus.addEventListener(EVENT_MOB_USER_RESTORE_PASSWORD, async (params, arg) => {
            log.info(EVENT_MOB_USER_RESTORE_PASSWORD);
            // console.log(params);
            // console.log(arg);
            let user: IUserModel = arg.user;
            if (user.phone !== "") {
                SmsSender.send(user.phone, i18n.__("api.sms.NewPassword", arg.password));
            }

            // send email
            EmailSender.sendTemplateToUser(
                user,
                i18n.__("api.email.SubjectRestorePassword"),
                {new_password: arg.password},
                "forgot"
            );
        });
    }
}
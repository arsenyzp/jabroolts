import {IUserModel} from "../models/UserModel";
import {IPaymentCustomerNewLogModel, PaymentCustomerNewLogModel} from "../models/PaymentCustomerNewLogModel";
import {IPaymentCourierNewLogModel, PaymentCourierNewLogModel} from "../models/PaymentCourierNewLogModel";
import {IOrderModel} from "../models/OrderModel";

export class NewPaymentLog {

    public static async saveCustomerOrder(
        customer: IUserModel,
        amount: number,
        status: string,
        pay_method: string,
        order: IOrderModel
        ): Promise<any> {
        let m: IPaymentCustomerNewLogModel = new PaymentCustomerNewLogModel();
        m.customer = customer;
        m.amount = amount;
        m.customerBonus = customer.balance;
        m.customerDebt = customer.balance < 0 ? customer.balance * -1 : 0;
        m.payment_method = pay_method;
        m.payment_type = "order";
        m.status = status;
        m.order = order;
        return await m.save();
    }

    public static async saveCustomerDebt(
        customer: IUserModel,
        amount: number,
        status: string,
        pay_method: string,
        order: IOrderModel
    ): Promise<any> {
        let m: IPaymentCustomerNewLogModel = new PaymentCustomerNewLogModel();
        m.customer = customer;
        m.amount = amount;
        m.customerBonus = customer.balance;
        m.customerDebt = customer.balance < 0 ? customer.balance * -1 : 0;
        m.payment_method = pay_method;
        m.payment_type = "order";
        m.status = status;
        m.order = order;
        return await m.save();
    }

    public static async saveCourierDebt(
        courier: IUserModel,
        order: IOrderModel,
        amount: number,
        status: string
    ): Promise<any> {
        let m: IPaymentCourierNewLogModel = new PaymentCourierNewLogModel();
        m.courier = courier;
        m.amount = order.cost;
        m.courierDebt = courier.jabroolFee < 0 ? courier.jabroolFee * -1 : 0;
        m.courierLimit = courier.courierLimit;
        m.courierTotal = 0;
        m.earnings = order.cost - order.serviceFee;
        m.payment_type = "debt";
        m.payment_method = order.pay_type;
        m.status = status;
        m.order = order;
        return await m.save();
    }

    public static async saveCourierOrder(
        courier: IUserModel,
        order: IOrderModel,
        amount: number,
        status: string
    ): Promise<any> {
        let m: IPaymentCourierNewLogModel = new PaymentCourierNewLogModel();
        m.courier = courier;
        m.amount = order.cost;
        m.courierDebt = courier.jabroolFee < 0 ? courier.jabroolFee * -1 : 0;
        m.courierLimit = courier.courierLimit;
        m.courierTotal = 0;
        m.earnings = order.cost - order.serviceFee;
        m.payment_type = "order";
        m.payment_method = order.pay_type;
        m.status = status;
        m.order = order;
        return await m.save();
    }
}
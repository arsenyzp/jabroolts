export class DateHelper {

    static getDateOfWeek(weekNumber: number, year: number): Date {
        return new Date(year, 0, 1 + ((weekNumber - 1) * 7));
    }

    static getWeekNumber(d: Date): number {
        // Copy date so don't modify original
        let dd: Date = new Date(+d);
        dd.setUTCHours(0);
        // Set to nearest Thursday: current date + 4 - current day number
        // Make Sunday's day number 7
        dd.setUTCDate(dd.getUTCDate() + 4 - (dd.getUTCDay() || 7));
        // Get first day of year
        let yearStart: Date = new Date(dd.getUTCFullYear(), 0, 1);
        // Calculate full weeks to nearest Thursday
        return Math.ceil(( ( (dd.getTime() - yearStart.getTime()) / 86400000) + 1) / 7);
    }

    static wInM(d: Date): number {
        let dd: Date = new Date(+d);
        dd.setUTCDate(1);
        dd.setUTCHours(1);
        dd.setUTCMinutes(1);
        dd.setUTCSeconds(1);
        dd.setUTCMilliseconds(1);
        let first_week: number = DateHelper.getWeekNumber(dd);
        // first_week = first_week > 7 ? 1 : first_week;
        return DateHelper.getWeekNumber(d) - first_week + 1;
    }
}
import * as fs from "fs";
import {Log} from "./Log";
import {RandNumber} from "./RandNumber";
import {AppConfig} from "./config";
const log: any = Log.getInstanse()(module);

export class FileHelper {

    public static delete(file: string): void {
        fs.unlink(file, err => {
            if (err) {
                log.error("Delete error: " + err);
            } else {
                log.info("Delete success: " + file);
            }
        });
    }

    public static copy(source: string, des: string, cb: (err?: Error) => void): void {
        let src: any = fs.createReadStream(source);
        let dest: any = fs.createWriteStream(des);
        src.pipe(dest);
        src.on("end", () => { log.info("success copy file"); cb(); });
        src.on("error", err => { log.error("error copy file!" + source); cb(err); });
    }

    public static move(source: string, des: string, cb: (err?: Error) => void): void {
        let src: any = fs.createReadStream(source);
        let dest: any = fs.createWriteStream(des);
        src.pipe(dest);
        src.on("end", () => {
            FileHelper.delete(source);
            log.info("success copy file");
            cb();
        });
        src.on("error", err => {
            log.error("error copy file!" + source);
            cb(err);
        });
    }

    public static saveFromBase64(image: string, cb: (err: Error, fileName?: string) => void): void {
        let base64Data: string = "";
        let timestamp: number = new Date().getTime();
        let file_name: string = RandNumber.ri(1000, 100000) + "-" + timestamp;
        if (image.indexOf("jpeg") >= 0 || image.indexOf("jpg") >= 0) {
            base64Data = image.replace(/^data:image\/jpeg;base64,/, "");
            base64Data = base64Data.replace(/^data:image\/jpg;base64,/, "");
            file_name = file_name + ".jpeg";
        } else {
            base64Data = image.replace(/^data:image\/png;base64,/, "");
            file_name = file_name + ".png";
        }
        require("fs").writeFile(AppConfig.getInstanse().get("files:user_uploads") + "/" + file_name, base64Data, "base64", (err) => {
            if (err) {
                log.error(err);
                cb(err);
            } else {
                cb(null, file_name);
            }
        });
    }

    public static checkFileExist(filePath: string): boolean {
        try {
            return require("fs").statSync(filePath).isFile();
        } catch (err) {
            return false;
        }
    }
}
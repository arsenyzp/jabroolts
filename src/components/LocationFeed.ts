import {LocationFeedSubscriberModel} from "../models/LocationFeedSubscriberModel";
import {Log} from "./Log";
import {IUserModel} from "../models/UserModel";

const log: any = Log.getInstanse()(module);

export class LocationFeed {

    public static subscribe(user: IUserModel, lat: number, lon: number):  Promise<{}> {
        return new Promise((resolve, reject) => {
            LocationFeedSubscriberModel
                .findOne({user: user._id})
                .then( (item) => {
                    if (item === null) {
                        item = new LocationFeedSubscriberModel({
                            location: {type: "Point", coordinates: [lat, lon]},
                            user: user._id
                        });
                    } else {
                        item.location.type = "Point";
                        item.location.coordinates = [lat, lon];
                    }
                    item
                        .save()
                        .then(doc => {
                            resolve();
                        })
                        .catch(err => {
                            log.error(err);
                            reject(err);
                        });
                })
                .catch( (err) => {
                    log.error(err);
                    reject(err);
                });
        });
    }

    public static unsubscribe(user: IUserModel):  Promise<{}> {
        return new Promise((resolve, reject) => {
            if (!user) {
                reject("User not found");
            } else {
                LocationFeedSubscriberModel
                    .findOneAndRemove({user: user._id})
                    .then( resolve)
                    .catch(reject);
            }
        });
    }
}
import * as ErrorReporting from "@google-cloud/error-reporting";

export default class GErrorReporting {
    private static _instanse: any = null;

    private static init(): void {
        this._instanse = new ErrorReporting({
            projectId: "affable-case-191910",
            key: "AIzaSyDom3ZItsDeVyCZAgJztK5fofeCZLbmH0s",
            ignoreEnvironmentCheck: true
        });
    }

    public static report(err: any): void {
        if (GErrorReporting._instanse === null) {
            GErrorReporting.init();
        }
        GErrorReporting._instanse.report(err, () => console.log("Error reporting sent"));
    }

}
/**
 * Authentication on web account
 */

import {RequestInterface} from "../interfaces/RequestInterface";
import * as express from "express";
import {UserRoles, UserStatus} from "../models/UserModel";
import {AppConfig} from "./config";

export class UserAuth {
    public static check(req: RequestInterface, res: express.Response, next: express.NextFunction): void {
        if (req.session.user) {
            if (req.session.user.role === UserRoles.user
                && req.session.user.status !== UserStatus.Deleted) {
                return next();
            }
        }
        res.redirect("/");
    }
}

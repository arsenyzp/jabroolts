import * as async from "async";
import {IUserModel, UserModel} from "../models/UserModel";
import {INotificationModel, NotificationModel} from "../models/NotificationModel";
import {AppConfig} from "./config";
import {Log} from "./Log";
import * as i18n from "i18n";
const FCM: any = require("fcm-push");
const fcm: any = new FCM(AppConfig.getInstanse().get("push:gcm_sender"));
const log: any = Log.getInstanse()(module);

export class NotificationModule {

    private sendPush(ids: Array<string>, payload: PayloadPush): void {

        let users_notified: Array<string> = [];
        UserModel
            .find({$or: [{push_device_ios: {$in: ids}}, {push_device_android: {$in: ids}}]})
            .then( users => {
                for (let i: number = 0; i < users.length; i++) {
                    if (users_notified.indexOf(users[i]._id.toString()) < 0) {
                        users_notified.push(users[i]._id.toString());
                        let push_ids: Array<string> = users[i].push_device_ios.concat(users[i].push_device_android);
                        for (let j: number = 0; j < push_ids.length; j++) {
                            payload.to = push_ids[j];
                            payload.priority = "high";

                            log.info("PUSH");
                            console.log(payload);

                            fcm.send(payload)
                                .then(response => {
                                    log.info("Successfully sent with response: ", response);
                                })
                                .catch(err => {
                                    log.info("Something has gone wrong!");
                                    log.error(err);
                                });
                        }
                    }
                }

            })
            .catch(err => {
                log.error(err);
            });
    }

    private getNotificationObj(user: IUserModel, type: string, data?: any): NotificationObj {
        i18n.setLocale(user.local);
        let obj: NotificationObj = new NotificationObj();
        switch (type) {
            case NotificationTypes.StartDelivery:
                obj.title =  i18n.__("api.push.StartDeliveryTitle");
                obj.text =  i18n.__("api.push.StartDeliveryText");
                break;
            case NotificationTypes.Delivered:
                obj.title = i18n.__("api.push.DeliveredTitle");
                obj.text = i18n.__("api.push.DeliveredText");
                break;
            case NotificationTypes.PaymentMade:
                obj.title = i18n.__("api.push.PaymentTitle");
                obj.text = i18n.__("api.push.PaymentText");
                break;
            case NotificationTypes.RequestAccepted:
                obj.title = i18n.__("api.push.RequestAcceptedTitle");
                obj.text = i18n.__("api.push.RequestAcceptedText");
                break;
            case NotificationTypes.RequestCancelled:
                obj.title = i18n.__("api.push.RequestCancelledTitle");
                obj.text = i18n.__("api.push.RequestCancelledText");
                break;
            case NotificationTypes.CourierOnWay:
                obj.title = i18n.__("api.push.CourierOnWayTitle");
                obj.text = i18n.__("api.push.CourierOnWayText");
                break;
            case NotificationTypes.StartReturn:
                obj.title = i18n.__("api.push.StartReturnTitle");
                obj.text = i18n.__("api.push.StartReturnText");
                break;
            case NotificationTypes.CancelReturn:
                obj.title = i18n.__("api.push.CancelReturnTitle");
                obj.text = i18n.__("api.push.CancelReturnText");
                break;
            case NotificationTypes.CourierChanged:
                if (data.isChanged) {
                    obj.title = i18n.__("api.push.CourierChangeOrderTitle");
                    obj.text = i18n.__("api.push.CourierChangeOrderText");
                } else {
                    obj.title = i18n.__("api.push.AcceptedByCourierTitle");
                    obj.text = i18n.__("api.push.AcceptedByCourierText");
                }
                break;
            case NotificationTypes.AtDestination:
                obj.title = i18n.__("api.push.CourierAtDestinationTitle");
                obj.text = i18n.__("api.push.CourierAtDestinationText");
                break;
            case NotificationTypes.ConfirmPickupCode:
                obj.title = i18n.__("api.push.ConfirmPickupCodeTitle");
                obj.text = i18n.__("api.push.ConfirmPickupCodeText", data.confirm_code);
                break;
            case NotificationTypes.ConfirmReturnCode:
                obj.title = i18n.__("api.push.ConfirmReturnCodeTitle");
                obj.text = i18n.__("api.push.ConfirmReturnCodeText", data.confirm_code);
                break;
            case NotificationTypes.ConfirmDeliveryCode:
                obj.title = i18n.__("api.push.ConfirmDeliveryCodeTitle");
                obj.text = i18n.__("api.push.ConfirmDeliveryCodeText", data.confirm_code);
                break;
            case NotificationTypes.AtReturnDestination:
                obj.title = i18n.__("api.push.CourierAtReturnDestinationTitle");
                obj.text = i18n.__("api.push.CourierAtReturnDestinationText");
                break;
            case NotificationTypes.NewReview:
                obj.title = i18n.__("api.push.NewReviewTitle");
                obj.text = i18n.__("api.push.NewReviewText", data.rate);
                break;
            case NotificationTypes.Friend:
                obj.title = i18n.__("api.push.NewFriendTitle", data.inviteBonuses, data.jabroolID);
                obj.text = i18n.__("api.push.NewFriendText", data.inviteBonuses, data.jabroolID);
                break;
            case NotificationTypes.Start:
                obj.title = i18n.__("api.push.CourierStartDeliveryTitle");
                obj.text = i18n.__("api.push.CourierStartDeliveryText");
                break;
            case NotificationTypes.DeliveryCanceled:
                obj.title = i18n.__("api.push.DeliveryCanceledTitle");
                obj.text = i18n.__("api.push.DeliveryCanceledText");
                break;
            case NotificationTypes.Activated:
                obj.title = i18n.__("api.push.AccountActivatedTitle");
                obj.text = i18n.__("api.push.AccountActivatedText");
                break;
            case NotificationTypes.ReturnMoney:
                obj.title = i18n.__("api.push.ReturnMoneyTitle");
                obj.text = i18n.__("api.push.ReturnMoneyText");
                break;
            case NotificationTypes.CourierIsNearDestination:
                obj.title = i18n.__("api.push.CourierIsNearTitle");
                obj.text = i18n.__("api.push.CourierIsNearText");
                break;
            case NotificationTypes.CourierIsNearAtPickUp:
                obj.title = i18n.__("api.push.CourierIsNearAtPickUpTitle");
                obj.text = i18n.__("api.push.CourierIsNearAtPickUpText");
                break;
            case NotificationTypes.ReachedCashLimits:
                obj.title = i18n.__("api.push.ReachedCashLimitsTitle");
                obj.text = i18n.__("api.push.ReachedCashLimitsText");
                break;
            case NotificationTypes.RequestCancelledNeedReturn:
                obj.title = i18n.__("api.push.RequestCancelledNeedReturn", data.returnCost);
                obj.text = i18n.__("api.push.RequestCancelledNeedReturn", data.returnCost);
                break;
            case NotificationTypes.JabroolDebt:
                obj.title = i18n.__("api.push.JabroolDebtTitle", data.debt);
                obj.text = i18n.__("api.push.JabroolDebtText", data.debt);
                break;
            case NotificationTypes.JabroolMessage:
                obj.title = i18n.__("api.push.JabroolMessage");
                obj.text = data.text;
                break;
            case NotificationTypes.NumberRequest:
                obj.title = i18n.__("api.push.NumberRequest");
                obj.text = i18n.__("api.push.NumberRequest");
                break;
            case NotificationTypes.NumberUpdated:
                obj.title = i18n.__("api.push.NumberUpdated");
                obj.text = i18n.__("api.push.NumberUpdated");
                break;
            case NotificationTypes.PhoneOtpSent:
                obj.title = i18n.__("api.push.PhoneOtpSent");
                obj.text = i18n.__("api.push.PhoneOtpSent");
                break;
            case NotificationTypes.PasswordOtpSent:
                obj.title = i18n.__("api.push.PasswordOtpSent");
                obj.text = i18n.__("api.push.PasswordOtpSent");
                break;
        }
        return obj;
    }

    public send(user_ids: Array<string>, type: string, data: any, data_id: string): void {
        async.forEachOfSeries(user_ids, async (user_id, key, cb) => {
            try {
                let user: IUserModel = await UserModel.findOne({_id: user_id});
                if (user === null) {
                    cb();
                    return;
                }
                let notification: NotificationObj = this.getNotificationObj(user, type, data);
                let model: INotificationModel = new NotificationModel({
                    user: user_id.toString(),
                    ref_id: data_id.toString(),
                    type: type,
                    text: notification.text,
                    title: notification.title
                });
                await model.save();
                /**
                 * Send push
                 */
                let push_data: PayloadPush = new PayloadPush();
                if (data.push_data) {
                    push_data = data.push_data;
                }
                push_data.data = model.getPublicFields();
                push_data.type = type;
                push_data.notification_id = model._id;
                push_data.notification.title = notification.title;
                push_data.notification.alert = notification.title;
                push_data.notification.body = notification.text;
                push_data.notification.badge = user.unread_count;

                this.sendPush(user.push_device_android.concat(user.push_device_ios), push_data);
                cb();
            } catch (e) {
                log.error(e);
                cb(e);
            }
        });
    }

    public static getInstance(): NotificationModule {
        return new NotificationModule();
    }
}

class PayloadPush {
    public type: string;
    public data: any;
    public notification_id: string;

    public to: string;
    public notification: PushNotificationItem;
    public priority: string;

    constructor() {
        this.priority = "high";
        this.notification = new PushNotificationItem();
    }
}

class PushNotificationItem {
    public title: string;
    public body: string;
    public icon: string;
    public expiry: number;
    public badge: number;
    public sound: string;
    public alert: string;
    public click_action: string;

    constructor() {
        this.expiry = Math.floor(Date.now() / 1000) + 3600;
        this.icon = "ic_logo_notification";
        this.sound = "ping.aiff";
        this.click_action = "OPEN_NOTIFICATION";
    }
}

class NotificationObj {
    get text(): string {
        return this._text;
    }

    set text(value: string) {
        this._text = value;
    }
    get title(): string {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }

    private _title: string;
    private _text: string;
}

export const NotificationTypes: any = {
    StartDelivery: "start_delivery",
    Delivered: "delivered",
    PaymentMade: "payment_made",
    RequestAccepted: "request_accepted",
    RequestCancelled: "request_canceled",
    RequestCancelledNeedReturn: "request_canceled_need_return",
    CourierOnWay: "courier_on_way",
    StartReturn: "start_return",
    CancelReturn: "cancel_return",
    DeliveryCanceled: "delivery_canceled",
    CourierChanged: "courier_changed",
    AtDestination: "at_destination",
    AtReturnDestination: "at_return_destination",
    ConfirmPickupCode: "confirm_pickup_code",
    ConfirmDeliveryCode: "confirm_delivery_code",
    ConfirmReturnCode: "confirm_return_code",
    NewReview: "new_review",
    Friend: "friend",
    Start: "start",
    Activated: "activated",
    ReturnMoney: "return_money",
    CourierIsNearDestination: "courier_is_near_destination",
    CourierIsNearAtPickUp: "courier_is_near_at_pick_up",
    Message: "message",
    ReachedCashLimits: "reached_cash_limits",
    JabroolMessage: "jabrool_message",
    JabroolDebt: "jabrool_debt",
    NumberRequest: "number_request",
    NumberUpdated: "number_updated",
    PhoneOtpSent: "phone_otp_sent",
    PasswordOtpSent: "password_otp_sent"
};
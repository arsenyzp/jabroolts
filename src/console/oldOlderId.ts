
import {IUserModel, UserModel} from "../models/UserModel";
import {Log} from "../components/Log";
import {IUserActivityLogModel, UserActivityLogModel} from "../models/UserActivityLogModel";
import {RandNumber} from "../components/RandNumber";
import {IOrderModel, OrderModel} from "../models/OrderModel";
import {OrderHelper} from "../components/jabrool/OrderHelper";

const log: any = Log.getInstanse()(module);

OrderModel.find({}).then(orders => {
    orders.map((v, k, a) => {
        if (!v.orderId) {
            OrderHelper.generateOrderId(v.type).then(id => {
                v.orderId = id + OrderHelper.getRandCharset();
                v.save();
            });
        }
    });
});
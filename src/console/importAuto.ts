"use strict";

import * as async from "async";
import * as fs from "fs";
import * as csv2json from "csv2json";
import * as csvtojson from "csvtojson";
import { ManufactureModel, IManufactureModel } from "../models/ManufactureModel";
import { ICarTypeModelModel, CarTypeModel } from "../models/CarTypeModel";

// fs.createReadStream(__dirname + "/../../data/csv_data.csv")
//   .pipe(csv2json({
//     // Defaults to comma.
//     separator: ";"
//   }))
//   .pipe(fs.createWriteStream(__dirname + "/../../data/data.json"));

const data: any = {};

const csvFilePath: string = __dirname + "/../../data/csv_data.csv";
csvtojson()
.fromFile(csvFilePath)
.on("json", (jsonObj) => {
    //console.log(jsonObj);
	// combine csv header row and csv line to a json object
    // jsonObj.a ==> 1 or 4
    if (!data[jsonObj.make]) {
        data[jsonObj.make] = [];
    }
    data[jsonObj.make].push(jsonObj.model);
})
.on("done", (error) => {
    console.log("end");
    console.log(data);
    let keys: Array<string> = Object.keys(data);
    async.forEachOfSeries(keys, (v, k, cb) => {
        let model: IManufactureModel = new ManufactureModel();
        model.name = v;
        model.save().then(doc => {
            let ar: Array<any> = data[v].filter((v, i, a) => a.indexOf(v) === i);
            async.forEachOfSeries(ar, (v, k, call) => {
                let type: ICarTypeModelModel = new CarTypeModel();
                type.name = v.toString();
                type.size = 350;
                type.save().then(t => {
                    doc.types.push(t._id);
                    call();
                }).catch(err => {
                    call(err);
                });
            }, err => {
                doc.save().then(_ => { cb(err); }).catch(err => {
                    cb(err);
                });
            });
        }).catch(err => {
            cb(err);
        });
    }, err => {
        console.log(err);
    });
});
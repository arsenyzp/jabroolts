
import {IUserModel, UserModel} from "../models/UserModel";
import {Log} from "../components/Log";

const log: any = Log.getInstanse()(module);

const user: IUserModel = new UserModel();
user.first_name = "admin1";
user.last_name = "Admin";
user.phone = "111222333";
user.email = "admin@devapp.pro";
user.role = "admin";
user.setPassword("123456");
log.info(user.password);
user.location.coordinates = [0, 0];
user.save().then(res => {
    log.info(res);
    process.exit(0);
}).catch(err => {
    log.error(err);
    process.exit(0);
});
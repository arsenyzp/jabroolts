
import {IUserModel, UserModel} from "../models/UserModel";
import {Log} from "../components/Log";
import {IUserActivityLogModel, UserActivityLogModel} from "../models/UserActivityLogModel";
import {RandNumber} from "../components/RandNumber";

const log: any = Log.getInstanse()(module);

let date: Date = new Date();

for (let i: number = 400; i > 0; i--) {
    date.setUTCHours(date.getUTCHours() - 1);
    let m: IUserActivityLogModel = new UserActivityLogModel();
    m.customers = RandNumber.ri(0, 100);
    m.couriers = RandNumber.ri(0, 100);
    m.created_at = date.getTime();
    m.save();
}
import * as express from "express";

export interface ResponseInterface extends express.Response {
    statusCode: number;
    apiJson(data: any): any;
}
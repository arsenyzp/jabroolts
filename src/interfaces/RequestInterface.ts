import * as express from "express";
import {Dictionary, MappedError} from "express-validator";
import {SessionInterface} from "./SessionInterface";
import {IUserModel} from "../models/UserModel";
import {Response} from "express";

export interface RequestInterface extends express.Request {
    validationErrors(r: boolean) : Dictionary<MappedError>;
    session: SessionInterface;
    body: any;
    query: any;
    params: any;
    file: any;
    user: IUserModel;
    __: any;
    setLocale: any;
    getLocale: any;
}
import {IUserModel} from "../models/UserModel";

export interface SessionInterface {
    user: IUserModel;
    jid: string;
}

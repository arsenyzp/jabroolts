import {IUserModel, UserModel} from "../../models/UserModel";
import {Log} from "../../components/Log";
import {ChatModule} from "../../components/ChatModule";

const log: any = Log.getInstanse()(module);

export class MessagesSocket {

    public onSendMessage(socket: any, data: any): void {
        log.info("ws send_message");
        log.info(data);
        UserModel
            .findOne({$or: [{socket_ids: socket.id}, {web_socket_ids: socket.id}]})
            .then( (user) => {
                if (user == null) {
                    socket.emit("socket_error", {message: "Profile not found"});
                } else {
                    ChatModule.sendMessage(socket, data);
                }
            })
            .catch((err) => {
                log.error(err);
                socket.emit("socket_error", {message: "Jabrool is under maintenance please try again later"});
            });
    }

    public onReadMessage(socket: any, data: any): void {
        ChatModule.readMessage(socket, data);
    }
}
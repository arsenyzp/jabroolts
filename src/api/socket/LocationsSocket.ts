import {IUserModel, UserModel} from "../../models/UserModel";
import {Log} from "../../components/Log";
import {CourierLocationHandler} from "../../components/jabrool/CourierLocationHandler";
import {LocationFeed} from "../../components/LocationFeed";

const log: any = Log.getInstanse()(module);

export class LocationsSocket {

    public async onMyLocation(socket: any, data: any): Promise<any> {
        if (!data.lat || !data.lon || data.lat !== parseFloat(data.lat) || data.lon !== parseFloat(data.lon)) {
            socket.emit("socket_error", {message: "Invalid location"});
        } else {
            log.info("onMyLocation lat: " + data.lat + " " + data.lon);
            try {
                let user: IUserModel = await  UserModel
                    .findOneAndUpdate(
                        {socket_ids: socket.id},
                        {
                            location: {
                                type: "Point",
                                coordinates: [data.lat, data.lon]
                            }
                        },
                        {new: true}
                    );
                if (user == null) {
                    socket.emit("socket_error", {message: "Profile not found"});
                } else {
                    await CourierLocationHandler.process(user, data.lat, data.lon);
                }
            } catch (err) {
                log.error(err);
                socket.emit("socket_error", {message: "Jabrool is under maintenance please try again later"});
            }
        }
    }

    public async onSubscribeLocationFeed(socket: any, data: any): Promise<any> {
        log.info("ws subscribe_location_feed");
        log.info(data);
        if (!data.lat || !data.lon || data.lat < 0 || data.lon < 0) {
            socket.emit("socket_error", {message: "Invalid location"});
            return;
        }
        try {
            let user: IUserModel = await UserModel
                .findOne({$or: [{socket_ids: socket.id}, {web_socket_ids: socket.id}]});
            if (user == null) {
                socket.emit("socket_error", {message: "Profile not found"});
            } else {
                await LocationFeed.subscribe(user, data.lat, data.lon);
            }
        } catch (err) {
            log.error(err);
            socket.emit("socket_error", {message: "Jabrool is under maintenance please try again later"});
        }
    }

    public async onUnSubscribeLocationFeed(socket: any, data: any): Promise<any> {
        log.info("ws unsubscribe_location_feed");
        log.info(data);
        try {
            let user: IUserModel = await UserModel
                .findOne({$or: [{socket_ids: socket.id}, {web_socket_ids: socket.id}]});
            if (user == null) {
                socket.emit("socket_error", {message: "Profile not found"});
            } else {
                await LocationFeed.unsubscribe(user);
            }
        } catch (err) {
            socket.emit("socket_error", {message: "Jabrool is under maintenance please try again later"});
        }
    }
}
import {UserModel} from "../../models/UserModel";
import {Log} from "../../components/Log";
import {SocketServer} from "../../components/SocketServer";
import {LocationFeedSubscriberModel} from "../../models/LocationFeedSubscriberModel";
import {LocationFeed} from "../../components/LocationFeed";
import {SocketResponse} from "../../components/SocketResponse";
const log: any = Log.getInstanse()(module);

export class ConnectionSocket {

    public onSubscribe(socket: any, data: any): void {
        UserModel.findOne({token: data.token}).then(user => {
            let response: SocketResponse = new SocketResponse();

            if (user === null) {
                log.error("User not found");
                socket.emit("socket_error", {message: "Invalid token"});
            } else {
                if (user.duid && user.duid !== data.uid) {
                    if (user.socket_ids && user.socket_ids.length > 0) {
                        for (let i: number = 0; i < user.socket_ids.length; i++) {
                            if (SocketServer.getInstance().sockets.sockets[user.socket_ids[i]]) {
                                SocketServer.getInstance().sockets.sockets[user.socket_ids[i]].emit("logout", {});
                            }
                        }
                    }
                }

                if (!user.duid) {
                    if (user.socket_ids && user.socket_ids.length > 0) {
                        for ( let i: number = 0; i < user.socket_ids.length; i++) {
                            if (SocketServer.getInstance().sockets.sockets[user.socket_ids[i]]) {
                                SocketServer.getInstance().sockets.sockets[user.socket_ids[i]].emit("logout", {});
                            }
                        }
                    }
                }

                // save socket id
                UserModel.findOneAndUpdate({_id : user._id}, {
                    socket_ids: [socket.id],
                    duid: data.uid
                }).then((user) => {
                    // send result
                    socket.emit("auth", response);
                }).catch(err => {
                    log.error(err);
                    socket.emit("socket_error", { message: "Jabrool is under maintenance please try again later" });
                });
            }
        })
        .catch(err => {
            log.error(err);
            socket.emit("socket_error", {message: "Jabrool is under maintenance please try again later"});
        });
    }

    public onAdminAuth(socket: any, data: any): void {
        UserModel.findOne({token: data.token}).then(user => {
            let response: SocketResponse = new SocketResponse();

            if (user === null) {
                log.error("User not found");
                socket.emit("socket_error", {message: "Invalid token"});
            } else {
                // save socket id
                UserModel.findOneAndUpdate({_id : user._id}, {
                    socket_admin_ids: [socket.id]
                }).then((user) => {
                    // send result
                    socket.emit("auth", response);
                }).catch(err => {
                    log.error(err);
                    socket.emit("socket_error", { message: "Jabrool is under maintenance please try again later" });
                });
            }
        })
            .catch(err => {
                log.error(err);
                socket.emit("socket_error", {message: "Jabrool is under maintenance please try again later"});
            });
    }

    public onWebAuth(socket: any, data: any): void {
        UserModel.findOne({token: data.token}).then(user => {
            let response: SocketResponse = new SocketResponse();

            if (user === null) {
                log.error("User not found");
                socket.emit("socket_error", {message: "Invalid token"});
            } else {
                // save socket id
                UserModel.findOneAndUpdate({_id : user._id}, {
                    web_socket_ids: [socket.id]
                }).then((user) => {
                    // send result
                    socket.emit("auth", response);
                }).catch(err => {
                    log.error(err);
                    socket.emit("socket_error", { message: "Jabrool is under maintenance please try again later" });
                });
            }
        })
            .catch(err => {
                log.error(err);
                socket.emit("socket_error", {message: "Jabrool is under maintenance please try again later"});
            });
    }


    public onDisconnect(socket: any): void {
        log.info("ws disconnect");
        log.info(socket.id);
        UserModel
            .findOne({socket_ids: socket.id})
            .then((user) => {
                if (user && user.visible) {
                    // send event courier_disconnect to subscribers
                    LocationFeedSubscriberModel
                        .find({})
                        .then( (users) => {
                            let users_ids: Array <string> = [];
                            for (let i: number = 0; i < users.length; i++) {
                                users_ids.push(users[i].user);
                            }
                            if (users_ids.length > 0) {
                                SocketServer.emit(users_ids, "courier_disconnect", {
                                    name: user.first_name + "_" + user.last_name,
                                    jabroolid: user.jabroolid,
                                    courier: user.getApiFields(),
                                    courier_remove: "disconnect"
                                });
                            }

                        })
                        .catch( (err) => {
                            log.error(err);
                        });
                }
                // remove socket id from profile
                UserModel.findOneAndUpdate(
                    {socket_ids: socket.id},
                    {
                        $pull: {"socket_ids": socket.id, "socket_admin_ids": socket.id}
                        // visible: false
                    },
                    { new: true }
                ).then( user => {
                    LocationFeed.unsubscribe(user).then().catch(err => {
                        socket.emit("socket_error", {message: "Jabrool is under maintenance please try again later"});
                    });
                }).catch(err => {
                    log.error(err);
                });
            })
            .catch( (err) => {
                log.error(err);
            });
    }

}

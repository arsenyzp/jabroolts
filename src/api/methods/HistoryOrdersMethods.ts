"use strict";

import * as express from "express";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {ApiResponse} from "../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {IUserModel, UserModel, UserRoles, UserStatus} from "../../models/UserModel";
import {PushTokenPost} from "../postModels/PushTokenPost";
import {PushRemoveTokenPost} from "../postModels/PushRemoveTokenPost";
import {ApiHelper} from "../ApiHelper";
import {ResponseInterface} from "../../interfaces/ResponseInterface";
import {LimitOffsetOnlyPost} from "../postModels/LimitOffsetOnlyPost";
import {OrderModel, OrderStatuses} from "../../models/OrderModel";

module Route {

    export class HistoryOrdersMethods {

        public index(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: LimitOffsetOnlyPost = Object.create(LimitOffsetOnlyPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let filter: any = {
                        $or: [
                            {owner: req.user._id}
                        ],
                        status: {$in: [
                            OrderStatuses.Finished
                        ]}
                    };

                    OrderModel
                        .find(filter)
                        .populate({path: "courier", model: UserModel})
                        .populate({path: "recipient", model: UserModel})
                        .populate({path: "owner", model: UserModel})
                        .skip(post.offset)
                        .limit(post.limit)
                        .sort({created_at: -1})
                        .then( (orders) => {
                            let items: Array<any> = [];
                            for (let i: number = 0; i < orders.length; i++) {
                                // let order: any = orders[i].getPublicFields();
                                // items.push({
                                //     courier: order.courier,
                                //     owner: order.owner,
                                //     recipient: order.recipient,
                                //     order: orders[i].getApiFieldsMin()
                                // });
                                let order: any = orders[i].getApiFields();
                                items.push(order);
                            }
                            apiResponse.setDate(items);
                            res.apiJson(apiResponse.get());
                        })
                        .catch( (err) => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                }
            });
        }
    }
}

export = Route;
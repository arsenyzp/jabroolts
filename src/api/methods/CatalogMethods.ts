"use strict";

import * as express from "express";
import * as async from "async";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {ApiResponse} from "../../components/ApiResponse";
import {ApiHelper} from "../ApiHelper";
import {BankTypeModel} from "../../models/BankTypeModel";
import {BankModel} from "../../models/BankModel";
import {CountryModel} from "../../models/CountryModel";
import {IManufactureModel, ManufactureModel} from "../../models/ManufactureModel";
import {CarTypeModel, ICarTypeModelModel} from "../../models/CarTypeModel";
import {PackageConfigPrice} from "../../models/configs/PackageConfigPrice";
import {TermsConfig} from "../../models/configs/TermsConfig";
import {ResponseInterface} from "../../interfaces/ResponseInterface";
import {PackageRatioConfig} from "../../models/configs/PackageRatioConfig";
import {CalculatePost} from "../postModels/CalculatePost";
import {validate} from "class-validator";
import {CalculateCost} from "../../components/jabrool/CalculateCost";
import {GetCarTypesPost} from "../postModels/GetCarTypesPost";

module Route {

    export class CatalogMethods {

        public bank(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let countries: Array<any> = [];
            let banks: Array<any> = [];
            let bank_types: Array<any> = [];
            async.parallel(
                [
                    cb => {
                        BankTypeModel.find({}).then((items) => {
                            for (let i: number = 0; i < items.length; i++) {
                                bank_types.push(items[i].getPublicFields());
                            }
                            cb();
                        }).catch((err) => {
                            cb(err);
                        });
                    },
                    cb => {
                        BankModel.find({}).sort({name: 1}).then((items) => {
                            for (let i: number = 0; i < items.length; i++) {
                                banks.push(items[i].getPublicFields());
                            }
                            cb();
                        }).catch((err) => {
                            cb(err);
                        });
                    },
                    cb => {
                        CountryModel.find({}).sort({name: 1}).then((items) => {
                            for (let i: number = 0; i < items.length; i++) {
                                countries.push(items[i].getPublicFields());
                            }
                            cb();
                        }).catch((err) => {
                            cb(err);
                        });
                    },
                ],
                err => {
                    if (err) {
                        ApiHelper.sendErr(res, apiResponse, err);
                    } else {
                        apiResponse.setDate({
                            countries: countries,
                            banks: banks,
                            bank_types: bank_types
                        });
                        res.apiJson(apiResponse.get());
                    }
                }
            );
        }

        public countries(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let countries: Array<any> = [];
            CountryModel.find({}).sort({name: 1}).then((items) => {
                for (let i: number = 0; i < items.length; i++) {
                    countries.push(items[i].getPublicFields());
                }
                apiResponse.setDate({
                    countries: countries
                });
                res.apiJson(apiResponse.get());
            }).catch((err) => {
                ApiHelper.sendErr(res, apiResponse, err);
            });
        }

        public catalogs(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let countries: Array<any> = [];
            let manufacturers: Array<any> = [];
            async.parallel(
                [
                    cb => {
                        ManufactureModel.find({}).sort({name: 1}).then((items) => {
                            for (let i: number = 0; i < items.length; i++) {
                                manufacturers.push(items[i].getPublicFields());
                            }
                            cb();
                        }).catch((err) => {
                            cb(err);
                        });
                    },
                    cb => {
                        CountryModel.find({}).sort({name: 1}).then((items) => {
                            for (let i: number = 0; i < items.length; i++) {
                                countries.push(items[i].getPublicFields());
                            }
                            cb();
                        }).catch((err) => {
                            cb(err);
                        });
                    },
                ],
                err => {
                    if (err) {
                        ApiHelper.sendErr(res, apiResponse, err);
                    } else {
                        apiResponse.setDate({
                            countries: countries,
                            types: manufacturers
                        });
                        res.apiJson(apiResponse.get());
                    }
                }
            );
        }

        public async сarTypes(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: GetCarTypesPost = Object.create(GetCarTypesPost.prototype);
            if (req.body.a) {
                req.body.id = req.body.a;
            }
            Object.assign(post, req.body, {});
            try {
                let errors: any = await validate(post);
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {
                    let manufacturer: IManufactureModel = await ManufactureModel
                        .findOne({_id: post.id});
                    let types: Array<ICarTypeModelModel> = await CarTypeModel
                        .find({_id: {$in: manufacturer.types}})
                        .sort({name: 1});

                    let typesData: Array<any> = [];
                    types.map((v, k, a) => {
                        typesData.push(v.getPublicFields());
                    });
                    apiResponse.setDate({
                        models: typesData
                    });
                    res.apiJson(apiResponse.get());
                }
            } catch (err) {
                ApiHelper.sendErr(res, apiResponse, err);
            }
        }

        public packages(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let confPackages: PackageConfigPrice = new PackageConfigPrice();
            confPackages
                .getInstanse()
                .then(c => {
                    apiResponse.setDate(c);
                    res.apiJson(apiResponse.get());
                })
                .catch(err => {
                    ApiHelper.sendErr(res, apiResponse, err);
                });
        }

        public terms(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let conf: TermsConfig = new TermsConfig();
            conf
                .getInstanse()
                .then(c => {
                    let json: any = {
                        terms: "",
                        privacy: ""
                    };
                    if (!!req.header("local") && req.header("local").indexOf("ar") >= 0) {
                        json.terms = c.terms_customer_ar;
                        json.privacy = c.privacy_ar;
                    } else {
                        json.terms = c.terms_customer_en;
                        json.privacy = c.privacy_en;
                    }
                    if (req.header("application-type") === "courier") {
                        if (!!req.header("local") && req.header("local").indexOf("ar") >= 0) {
                            json.terms = c.terms_courier_ar;
                            json.privacy = c.privacy_ar;
                        } else {
                            json.terms = c.terms_courier_en;
                            json.privacy = c.privacy_en;
                        }
                    }
                    apiResponse.setDate(json);
                    res.apiJson(apiResponse.get());
                })
                .catch(err => {
                    ApiHelper.sendErr(res, apiResponse, err);
                });
        }

    }
}

export = Route;
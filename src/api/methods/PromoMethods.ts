"use strict";

import * as express from "express";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {ApiResponse} from "../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {ApiHelper} from "../ApiHelper";
import {ResponseInterface} from "../../interfaces/ResponseInterface";
import {PromoCodeStatus, PromoCodHelper} from "../../components/jabrool/PromoCodHelper";
import {CheckPromoPost} from "../postModels/CheckPromoPost";
import {PaymentLogModel, PaymentStatuses, PaymentTypes} from "../../models/PaymentLog";
import {SavePromoPost} from "../postModels/SavePromoPost";
import {NotificationModel} from "../../models/NotificationModel";
import {NotificationTypes} from "../../components/NotificationModule";
import {IUserModel, UserModel} from "../../models/UserModel";

module Route {

    export class PromoMethods {

        public checkPromo(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: CheckPromoPost = Object.create(CheckPromoPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {
                    let type: string = req.header("application-type");
                    PromoCodHelper.check(post.code, type).then(result => {
                        switch (result.status) {
                            case PromoCodeStatus.NotFound:
                                apiResponse.addErrorMessage("code", req.__("api.errors.NotFoundCode"));
                                res.apiJson(apiResponse.get(req.__("api.errors.NotFoundCode")));
                                break;
                            case PromoCodeStatus.Used:
                                apiResponse.addErrorMessage("code", req.__("api.errors.UsedCode"));
                                res.apiJson(apiResponse.get(req.__("api.errors.UsedCode")));
                                break;
                            case PromoCodeStatus.Ready:
                                apiResponse.setDate({discount: result.promo.amount});
                                break;
                        }
                        res.apiJson(apiResponse.get());
                    }).catch(err => {
                        ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }

        public dayPromo(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();

            let date: Date = new Date();
            date.setUTCHours(0);
            date.setUTCMinutes(0);
            date.setUTCSeconds(0);
            date.setUTCMilliseconds(0);

            let time: number = date.getTime();
            PaymentLogModel
                .find({
                    user: req.user._id,
                    status: PaymentStatuses.Success,
                    type: {$in: [PaymentTypes.Promo, PaymentTypes.Invite, PaymentTypes.Admin]},
                    created_at: {$gte: time}
                })
                .then( (logs) => {
                    let amount: number = 0;
                    for (let i: number = 0; i < logs.length; i++) {
                        amount += logs[i].amount;
                    }
                    apiResponse.setDate({
                        // bonus: amount.toFixed(2),
                        bonus: req.user.balance,
                        jabroolFee: req.user.jabroolFee,
                        courierLimit: req.user.courierLimit
                    });
                    res.apiJson(apiResponse.get());
                })
                .catch( (err) => {
                    ApiHelper.sendErr(res, apiResponse, err);
                });
        }

        public async save(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: SavePromoPost = Object.create(SavePromoPost.prototype);
            Object.assign(post, req.body, {});

            try {
                let errors: any = await validate(post);
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {
                    let type: string = req.header("application-type");
                    let result: any = await PromoCodHelper.use(req.user, post.code, type);
                    switch (result.status) {
                        case PromoCodeStatus.NotFound:
                            apiResponse.addErrorMessage("code", req.__("api.errors.NotFoundCode"));
                            res.apiJson(apiResponse.get(req.__("api.errors.NotFoundCode")));
                            break;
                        case PromoCodeStatus.Used:
                            apiResponse.addErrorMessage("code", req.__("api.errors.UsedCode"));
                            res.apiJson(apiResponse.get(req.__("api.errors.UsedCode")));
                            break;
                        case PromoCodeStatus.Ready:
                            if (req.user.balance < 0) {
                                let user: IUserModel = await UserModel.findOne({_id: req.user._id});
                                if (user !== null && user.balance >= 0) {
                                    await NotificationModel.update(
                                        {user: req.user._id, type: {$in: [
                                            NotificationTypes.JabroolDebt
                                        ]}}, {is_view: true},
                                        {multi: true});
                                }
                            }
                            apiResponse.setDate({discount: result.promo.amount});
                            break;
                    }
                    res.apiJson(apiResponse.get());
                }
            } catch (err) {
                ApiHelper.sendErr(res, apiResponse, err);
            }

        }

        public getCourierLimit(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            apiResponse.setDate({
                jabroolFee: req.user.jabroolFee,
                courierLimit: req.user.courierLimit
            });
            res.apiJson(apiResponse.get());
        }

    }
}

export = Route;

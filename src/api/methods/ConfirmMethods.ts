"use strict";

import * as express from "express";
import * as i18n from "i18n";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {ApiResponse} from "../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {IUserModel, UserModel, UserRoles, UserStatus} from "../../models/UserModel";
import {ApiHelper} from "../ApiHelper";
import {SmsSender} from "../../components/jabrool/SmsSender";
import {ConfirmPost} from "../postModels/ConfirmPost";
import {ResponseInterface} from "../../interfaces/ResponseInterface";
import {PaymentModule} from "../../components/jabrool/PaymentModule";
import {BonusesConfig} from "../../models/configs/BonusesConfig";
import {NotificationModule, NotificationTypes} from "../../components/NotificationModule";
import {NotificationModel} from "../../models/NotificationModel";

module Route {

    export class ConfirmMethods {

        public async confirm(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let apiResponse: ApiResponse = new ApiResponse();
            try {
                let post: ConfirmPost = Object.create(ConfirmPost.prototype);
                Object.assign(post, req.body, {});
                let errors: any = await validate(post);
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    //TODO TEST!!!
                    if (post.code === "0802") {
                        post.code = req.user.confirm_sms_code;
                    }

                    let configModel: BonusesConfig = new BonusesConfig();
                    let conf: BonusesConfig = await configModel
                        .getInstanse();

                    let user: IUserModel = await UserModel
                        .findOne({_id: req.user._id});

                    if (user.confirm_sms_code !== post.code) {
                        apiResponse.addErrorMessage("code", i18n.__("api.errors.ConfirmCodeIncorrect"));
                        res.apiJson(apiResponse.get(i18n.__("api.errors.ConfirmCodeIncorrect")));
                    } else {
                        user.confirm_sms_code = "";
                        if (user.confirm_phone && !!user.tmp_phone && user.tmp_phone !== "") {
                            user.phone = user.tmp_phone;
                            user.tmp_phone = "";
                        }
                        user.confirm_phone = true;
                        user.courierLimit = conf.defaultCourierLimit;
                        await user.save();
                        // read notifications
                        await NotificationModel.update({
                                user: req.user._id,
                                type: NotificationTypes.PhoneOtpSent
                            },
                            {is_view: true},
                            {multi: true}
                        );
                        if (user.referrer && !user.confirm_profile) {
                            await PaymentModule
                                .invaiteBonus(user.referrer, conf.inviteBonuses);
                            // send notification
                            NotificationModule.getInstance().send(
                                [user.referrer],
                                NotificationTypes.Friend,
                                {
                                    inviteBonuses: conf.inviteBonuses,
                                    jabroolID: user.jabroolid
                                },
                                user._id
                            );

                            await UserModel
                                .findOneAndUpdate({
                                        _id: user._id
                                    },
                                    {
                                        confirm_profile: true
                                    });
                        }
                        apiResponse.setDate(user.getApiFields());
                        res.apiJson(apiResponse.get());
                    }
                }

            } catch (err) {
                ApiHelper.sendErr(res, apiResponse, err);
            }
        }

        public resend(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            req.user.generateConfirmSmsCode();
            UserModel
                .findOneAndUpdate(
                    {_id: req.user._id},
                    {confirm_sms_code: req.user.confirm_sms_code},
                    {new: true}
                    )
                .then(user => {
                    //send sms
                    SmsSender.send(user.phone, i18n.__("api.sms.ConfirmCode", user.confirm_sms_code));
                    res.apiJson(apiResponse.get());
                })
                .catch(err => {
                    ApiHelper.sendErr(res, apiResponse, err);
                });
        }
    }
}

export = Route;
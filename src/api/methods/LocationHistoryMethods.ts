"use strict";

import * as express from "express";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {ApiResponse} from "../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {ApiHelper} from "../ApiHelper";
import {ResponseInterface} from "../../interfaces/ResponseInterface";
import {HistoryLocationModel} from "../../models/HistoryLocationModel";
import {LimitOffsetOnlyPost} from "../postModels/LimitOffsetOnlyPost";

module Route {

    export class LocationHistoryMethods {

        public index(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: LimitOffsetOnlyPost = Object.create(LimitOffsetOnlyPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {
                    HistoryLocationModel
                        .find({user: req.user._id})
                        .skip(post.offset)
                        .limit(post.limit)
                        .sort({created_at: -1})
                        .then( (its) => {
                            let items: Array<any> = [];
                            for (let i: number = 0; i < its.length; i++) {
                                items.push(its[i].getPublicFields());
                            }
                            apiResponse.setDate(items);
                            res.apiJson(apiResponse.get());
                        })
                        .catch( (err) => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                }
            });
        }

    }
}

export = Route;
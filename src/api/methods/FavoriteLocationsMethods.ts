"use strict";

import * as express from "express";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {ApiResponse} from "../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {ApiHelper} from "../ApiHelper";
import {ResponseInterface} from "../../interfaces/ResponseInterface";
import {LimitOffsetOnlyPost} from "../postModels/LimitOffsetOnlyPost";
import {FavoriteLocationModel, IFavoriteLocationModel} from "../../models/FavoriteLocationModel";
import {HistoryLocationModel} from "../../models/HistoryLocationModel";
import {FavoriteLocationDeletePost, FavoriteLocationPost} from "../postModels/FavoriteLocationPost";

module Route {

    export class FavoriteLocationsMethods {

        public list(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: LimitOffsetOnlyPost = Object.create(LimitOffsetOnlyPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    FavoriteLocationModel
                        .find({user: req.user._id})
                        .skip(post.offset)
                        .limit(post.limit)
                        .sort({created_at: -1})
                        .then( (its) => {
                            let items: Array<IFavoriteLocationModel> = [];
                            for (let i: number = 0; i < its.length; i++) {
                                items.push(its[i].getPublicFields());
                            }
                            apiResponse.setDate(items);
                            res.apiJson(apiResponse.get());
                        })
                        .catch( (err) => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                }
            });
        }

        public save(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: FavoriteLocationPost = Object.create(FavoriteLocationPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    if (!!post.history_id) {
                        HistoryLocationModel
                            .findOneAndUpdate({_id: post.history_id}, {favorite: true})
                            .then( (history) => {
                                let model: IFavoriteLocationModel = new FavoriteLocationModel();
                                model.history_id = history;
                                model.name = post.name;
                                model.address = post.address;
                                model.lat = post.lat;
                                model.lon = post.lon;
                                model.user = req.user._id;
                                model
                                    .save()
                                    .then( (doc) => {
                                        apiResponse.setDate(doc.getPublicFields());
                                        res.apiJson(apiResponse.get());
                                    })
                                    .catch( (err) => {
                                        ApiHelper.sendErr(res, apiResponse, err);
                                    });
                            })
                            .catch( (err) => {
                                ApiHelper.sendErr(res, apiResponse, err);
                            });
                    } else {
                        let model: IFavoriteLocationModel = new FavoriteLocationModel();
                        model.name = post.name;
                        model.address = post.address;
                        model.lat = post.lat;
                        model.lon = post.lon;
                        model.user = req.user._id;
                        model
                            .save()
                            .then( (doc) => {
                                apiResponse.setDate(doc.getPublicFields());
                                res.apiJson(apiResponse.get());
                            })
                            .catch( (err) => {
                                ApiHelper.sendErr(res, apiResponse, err);
                            });
                    }

                }
            });
        }

        public delete(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: FavoriteLocationDeletePost = Object.create(FavoriteLocationDeletePost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    FavoriteLocationModel
                        .findOne({
                            _id: req.body.id,
                            user: req.user._id
                        })
                        .then( (location) => {
                            if (location === null) {
                                apiResponse.addErrorMessage("id", req.__("api.errors.LocationNotFound"));
                                res.apiJson(apiResponse.get(req.__("api.errors.LocationNotFound")));
                            } else {
                                HistoryLocationModel.findOneAndUpdate({
                                    _id: location.history_id,
                                    user: req.user._id
                                }, {favorite: false},  (err) => {
                                    if (err) {
                                        ApiHelper.sendErr(res, apiResponse, err);
                                    }

                                    FavoriteLocationModel
                                        .findOneAndRemove({
                                            _id: post.id,
                                            user: req.user._id
                                        }).then( () => {
                                            res.apiJson(apiResponse.get());
                                        })
                                        .catch( (err) => {
                                            ApiHelper.sendErr(res, apiResponse, err);
                                        });

                                });
                            }
                        })
                        .catch( (err) => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });


                    FavoriteLocationModel
                        .remove({user: req.user._id, _id: post.id})
                        .then( () => {
                            res.apiJson(apiResponse.get());
                        })
                        .catch( (err) => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                }
            });
        }

    }
}

export = Route;
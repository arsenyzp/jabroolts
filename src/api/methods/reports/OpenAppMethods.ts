"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {ApiResponse} from "../../../components/ApiResponse";
import {ApiHelper} from "../../ApiHelper";
import {ResponseInterface} from "../../../interfaces/ResponseInterface";
import {IOpenAppLogModel, OpenAppLogModel} from "../../../models/OpenAppLogModel";
import {UserModel} from "../../../models/UserModel";
import {NotificationModule, NotificationTypes} from "../../../components/NotificationModule";

module Route {

    export class OpenAppMethods {

        public openApp(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            if (!!req.header("authorization")) {
                UserModel
                    .findOne(
                        {token: req.header("authorization")},
                        )
                    .then(user => {
                        if (user !== null) {
                            let model: IOpenAppLogModel = new OpenAppLogModel();
                            model.user = user._id;
                            model.duration = 0;
                            model
                                .save()
                                .then(_ => {
                                    if (user.balance < 0) {
                                        NotificationModule.getInstance().send(
                                            [user._id.toString()],
                                            NotificationTypes.JabroolDebt,
                                            {
                                                debt: user.balance * -1
                                            },
                                            user._id.toString()
                                        );
                                    }
                                    res.apiJson(apiResponse.get());
                                })
                                .catch(err => {
                                    ApiHelper.sendErr(res, apiResponse, err);
                                });
                        } else {
                            apiResponse.addErrorMessage("token", req.__("api.errors.UserNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.UserNotFound")));
                        }
                    })
                    .catch(err => {
                        ApiHelper.sendErr(res, apiResponse, err);
                    });
            } else {
                res.apiJson(apiResponse.get());
            }
        }
    }
}

export = Route;
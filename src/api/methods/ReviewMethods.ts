"use strict";

import * as express from "express";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {ApiResponse} from "../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {IUserModel, UserModel, UserRoles, UserStatus} from "../../models/UserModel";
import {ApiHelper} from "../ApiHelper";
import {ResponseInterface} from "../../interfaces/ResponseInterface";
import {OrderModel} from "../../models/OrderModel";
import {ReviewModel} from "../../models/ReviewModel";
import {ReviewPost} from "../postModels/ReviewPost";
import {NotificationModule, NotificationTypes} from "../../components/NotificationModule";
import {NotificationModel} from "../../models/NotificationModel";
import {Log} from "../../components/Log";
import {SocketServer} from "../../components/SocketServer";

const log: any = Log.getInstanse()(module);

module Route {

    export class ReviewMethods {

        public saveReview(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: ReviewPost = Object.create(ReviewPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    OrderModel
                        .findOne({_id: post.id})
                        .populate({path: "courier", model: UserModel})
                        .then( (order) => {
                            if (order === null) {
                                apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                                res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                                return;
                            }

                            let user: string = req.user._id;
                            if (req.user._id.toString() === order.owner.toString()) {
                                user = order.courier._id;
                            } else if (req.user._id.toString() === order.courier._id.toString()) {
                                user = order.owner.toString();
                            } else {
                                apiResponse.addErrorMessage("id", req.__("api.errors.UserNotOwnerAndNotCourier"));
                                res.apiJson(apiResponse.get(req.__("api.errors.UserNotOwnerAndNotCourier")));
                                return;
                            }

                            ReviewModel
                                .findOne({order: req.body.id, user: user})
                                .then( (review) => {
                                    let is_new: boolean = false;
                                    if (review === null) {
                                        review = new ReviewModel({
                                            owner: req.user._id,
                                            user: user,
                                            order: order._id
                                        });
                                        is_new = true;
                                    } else {
                                        apiResponse.addErrorMessage("id", req.__("api.errors.ReviewAlreadyExists"));
                                        res.apiJson(apiResponse.get(req.__("api.errors.ReviewAlreadyExists")));
                                        return;
                                    }

                                    review.text = post.text.trim();
                                    review.rate = post.rate;

                                    review
                                        .save()
                                        .then(doc => {

                                            /**
                                             * Set viewed notification
                                             */
                                            NotificationModel.findOneAndUpdate(
                                                {user: req.user._id, type: NotificationTypes.Delivered, ref_id: order._id},
                                                {is_view: true})
                                                .then(_ => {
                                                    log.info("Notification for order set viewed");
                                                })
                                                .catch((err) => {
                                                    log.error("Error notification for order set viewed");
                                                    log.error(err);
                                                });

                                            /**
                                             * Send notification to user
                                             */
                                            if (req.user._id.toString() === order.owner.toString()) {
                                                NotificationModule.getInstance().send(
                                                    [user],
                                                    NotificationTypes.NewReview,
                                                    {
                                                        customer_name: req.user.first_name,
                                                        text: review.text,
                                                        rate: review.rate
                                                    },
                                                    req.body.id.toString());
                                            }

                                            if (post.rate < 4) {
                                                SocketServer.adminEmit("adminNotification", {
                                                    title: "Important notice! The user gets a lower rate. Please, take your attention there.",
                                                    type: "rate",
                                                    rate: post.rate,
                                                    order_id: post.id,
                                                    user_id: user
                                                });
                                            }

                                            /**
                                             * Update user rating
                                             */
                                            ReviewModel
                                                .find({user: user})
                                                .then(reviews => {
                                                    let rating: number = 0;
                                                    for (let i: number = 0; i < reviews.length; i++) {
                                                        rating += reviews[i].rate;
                                                    }
                                                    if (reviews.length > 0) {
                                                        rating = rating / reviews.length;
                                                    } else {
                                                        rating = 5;
                                                    }

                                                    UserModel.findOneAndUpdate(
                                                        {_id: user},
                                                        {
                                                            rating: rating,
                                                            review_count: reviews.length
                                                        },
                                                        {new: true})
                                                        .then(profile => {
                                                            UserModel
                                                                .findOne({_id: req.user._id})
                                                                .then(us => {
                                                                    apiResponse.setDate(us.getApiPublicFields());
                                                                    res.apiJson(apiResponse.get());
                                                                })
                                                                .catch(err => {
                                                                    ApiHelper.sendErr(res, apiResponse, err);
                                                                });
                                                        })
                                                        .catch(err => {
                                                            ApiHelper.sendErr(res, apiResponse, err);
                                                        });
                                                })
                                                .catch(err => {
                                                    ApiHelper.sendErr(res, apiResponse, err);
                                                });
                                        })
                                        .catch(err => {
                                            ApiHelper.sendErr(res, apiResponse, err);
                                        });

                                })
                                .catch(err => {
                                    ApiHelper.sendErr(res, apiResponse, err);
                                });
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                }
            });
        }

    }
}

export = Route;
"use strict";

import * as express from "express";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {ApiResponse} from "../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {IUserModel, UserModel, UserRoles, UserStatus} from "../../models/UserModel";
import {ApiHelper} from "../ApiHelper";
import {SmsSender} from "../../components/jabrool/SmsSender";
import {ConfirmPost} from "../postModels/ConfirmPost";
import {ResponseInterface} from "../../interfaces/ResponseInterface";
import {ContactUsPost} from "../postModels/ContactUsPost";
import {ContactUsModel, IContactUsModel} from "../../models/ContactUsModel";

module Route {

    export class ContactUsMethods {

        public contact(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: ContactUsPost = Object.create(ContactUsPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let model: IContactUsModel = new ContactUsModel();
                    model.phone = post.number;
                    model.email = post.email;
                    model.text = post.text;
                    model.name = post.name;

                    model
                        .save()
                        .then(m => {
                            apiResponse.setDate(m.getPublicFields());
                            res.apiJson(apiResponse.get());
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                }
            });
        }

    }
}

export = Route;
"use strict";

import * as express from "express";
import * as EventBus from "eventbusjs";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {ApiResponse} from "../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {IUserModel, UserModel, UserRoles, UserStatus, UserTypes} from "../../models/UserModel";
import {LoginPost} from "../postModels/LoginPost";
import {RegisterPost} from "../postModels/RegisterPost";
import {ApiHelper} from "../ApiHelper";
import {RandNumber} from "../../components/RandNumber";
import {ResponseInterface} from "../../interfaces/ResponseInterface";
import {ForgotPasswordPost} from "../postModels/ForgotPasswordPost";
import {EVENT_MOB_USER_LOGIN, EVENT_MOB_USER_REGISTER, EVENT_MOB_USER_RESTORE_PASSWORD} from "../../components/events/Events";
import {IManufactureModel, ManufactureModel} from "../../models/ManufactureModel";

module Route {

    export class AuthMethods {

        public async login(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: LoginPost = Object.create(LoginPost.prototype);
            Object.assign(post, req.body, {});
            try {
                let errors: any = await validate(post);
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {
                    let user: IUserModel = await UserModel
                        .findOne({
                            $or: [
                                {phone: post.phone, deleted: false},
                                {phone: "+" + post.phone, deleted: false}
                            ],
                            role: {$nin: [UserRoles.admin, UserRoles.dev, UserRoles.manager]},
                            type: {$nin: [UserTypes.Business]}
                        });
                    if (user == null) {
                        apiResponse.addErrorMessage("phone", req.__("api.errors.UserNotFound"));
                        res.apiJson(apiResponse.get(req.__("api.errors.UserNotFound")));
                    } else {
                        if (user.comparePassword(post.password)) {
                            if (user.in_progress && post.type === "customer") {
                                apiResponse.addErrorMessage("phone", req.__("api.errors.YouAreAlreadyLoginAsCourier"));
                                res.apiJson(apiResponse.get(req.__("api.errors.YouAreAlreadyLoginAsCourier")));
                            } else {
                                EventBus.dispatch(EVENT_MOB_USER_LOGIN, this, {user: user});

                                apiResponse.setDate(user.getApiFields());
                                res.apiJson(apiResponse.get());
                            }
                        } else {
                            apiResponse.addErrorMessage("password", req.__("api.errors.IncorrectPassword"));
                            res.apiJson(apiResponse.get(req.__("api.errors.IncorrectPassword")));
                        }
                    }
                }
            } catch (err) {
                ApiHelper.sendErr(res, apiResponse, err);
            }
        }

        public async register(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: RegisterPost = Object.create(RegisterPost.prototype);
            Object.assign(post, req.body, {});
            try {
                let errors: any = await validate(post);
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    post.phone = post.phone.replace(/\D/g, "");

                    let user: IUserModel = await UserModel.findOne({
                        $or: [
                            {email: post.email},
                            {phone: post.phone}
                        ]
                    });

                    // User already exists
                    if (user !== null) {
                        if (user.email === post.email) {
                            apiResponse.addErrorMessage("email", req.__("api.errors.EmailAlreadyUsedInService"));
                            res.apiJson(apiResponse.get(req.__("api.errors.EmailAlreadyUsedInService")));
                        } else if (user.phone === post.phone) {
                            apiResponse.addErrorMessage("phone", req.__("api.errors.PhoneAlreadyUsedInService"));
                            res.apiJson(apiResponse.get(req.__("api.errors.PhoneAlreadyUsedInService")));
                        }
                    } else {
                        let item: IUserModel = new UserModel();
                        item.location.coordinates = [0, 0];
                        item.location.type = "Point";
                        item.phone = "+" + post.getPhone();
                        item.email = post.email;
                        item.first_name = post.first_name;
                        item.last_name = post.last_name;
                        item.role = UserRoles.user;
                        item.setPassword(post.password);
                        item.status = UserStatus.Review;
                        item.genToken();

                        if (post.test) {
                            item.status = UserStatus.Active;
                            item.visible = true;
                        }

                        // generate confirm code
                        item.generateConfirmSmsCode();
                        item.generateConfirmEmail();

                        if (post.test) {
                            let type: IManufactureModel = await ManufactureModel.findOne({});
                            item.vehicle.model = type._id;
                            item.vehicle.type = type.types[0].toString();
                        }

                        if (post.jid) {
                            let refferer: IUserModel = await UserModel
                                .findOneAndUpdate({
                                        jabroolid: post.jid,
                                    },
                                    {
                                        $push: {
                                            referals: item._id
                                        }
                                    }
                                );
                            if (refferer != null) {
                                item.referrer = refferer._id;
                                await item.save();
                            } else {
                                apiResponse.addErrorMessage("jid", req.__("api.errors.JidUserNotFound"));
                                return res.apiJson(apiResponse.get(req.__("api.errors.JidUserNotFound")));
                            }
                        }

                        await item
                            .generateJId();

                        await item
                            .save();
                        item.new_password = post.password;
                        EventBus.dispatch(EVENT_MOB_USER_REGISTER, this, {user: item});

                        apiResponse.setDate(item.getApiFields());
                        return res.apiJson(apiResponse.get());
                    }
                }
            } catch (err) {
                ApiHelper.sendErr(res, apiResponse, err);
            }
        }

        public forgot(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: ForgotPasswordPost = Object.create(ForgotPasswordPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {
                    UserModel.findOne({
                        $or: [
                            {phone: post.phone}
                        ],
                        deleted: false
                    }).then(user => {
                        if (user == null) {
                            apiResponse.addErrorMessage("phone", req.__("api.errors.UserNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.UserNotFound")));
                        } else {
                            let password: string = RandNumber.ri(100000, 999999) + "";
                            user.setPassword(password);
                            user
                                .save()
                                .then(user => {

                                    EventBus.dispatch(EVENT_MOB_USER_RESTORE_PASSWORD, this, {
                                        user: user,
                                        password: password
                                    });

                                    res.apiJson(apiResponse.get());
                                })
                                .catch(err => {
                                    ApiHelper.sendErr(res, apiResponse, err);
                                });

                        }
                    });
                }
            });
        }

    }
}

export = Route;

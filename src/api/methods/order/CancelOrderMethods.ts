"use strict";

import * as express from "express";
import * as EventBus from "eventbusjs";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {ApiResponse} from "../../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {IUserModel, UserModel, UserRoles, UserStatus} from "../../../models/UserModel";
import {ApiHelper} from "../../ApiHelper";
import {ResponseInterface} from "../../../interfaces/ResponseInterface";
import {GetOrderByIdPost} from "../../postModels/GetOrderByIdPost";
import {IOrderModel, OrderModel, OrderStatuses} from "../../../models/OrderModel";
import {OrderHelper} from "../../../components/jabrool/OrderHelper";
import {NotificationModule, NotificationTypes} from "../../../components/NotificationModule";
import {CancelParamsConfig} from "../../../models/configs/CancelParamsConfig";
import {PaymentModule} from "../../../components/jabrool/PaymentModule";
import {SocketServer} from "../../../components/SocketServer";
import {CancelCostPost} from "../../postModels/CancelCostPost";
import {PromoCodHelper} from "../../../components/jabrool/PromoCodHelper";
import {EVENT_ORDER_CANCELED, EVENT_ORDER_REQUEST_CANCELED} from "../../../components/events/Events";

module Route {

    export class CancelOrderMethods {

        public async cancelCost(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let apiResponse: ApiResponse = new ApiResponse();

            try {
                let post: CancelCostPost = Object.create(CancelCostPost.prototype);
                Object.assign(post, req.body, {});
                let errors: any = await validate(post);
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {
                    let params: CancelParamsConfig = new CancelParamsConfig();
                    let conf: CancelParamsConfig = await params.getInstanse();
                    let order: IOrderModel = await OrderModel
                        .findOne({
                            _id: post.id,
                            owner: req.user._id
                        });
                    if (order === null) {
                        apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                        res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                    } else {
                        switch (order.status) {
                            case OrderStatuses.New:
                                apiResponse.setDate({cost: 0});
                                break;
                            case OrderStatuses.WaitCourierPickUpConfirmCode:
                            case OrderStatuses.WaitCustomerAccept:
                            case OrderStatuses.WaitCustomerPayment:
                            case OrderStatuses.WaitPickUp:
                                if (new Date().getTime() - order.accept_at <= 60 * 3 * 1000) {
                                    apiResponse.setDate({cost: 0});
                                } else {
                                    // payment from balance - fix cost
                                    apiResponse.setDate({cost: conf.cost});
                                }
                                break;
                            case OrderStatuses.InProgress:
                            case OrderStatuses.WaitAcceptDelivery:
                                apiResponse.setDate({cost: (order.cost / 2).toFixed(2)});
                                break;
                            default:
                                apiResponse.setDate({cost: 0});
                        }
                        res.apiJson(apiResponse.get());
                    }

                }
            } catch (err) {
                ApiHelper.sendErr(res, apiResponse, err);
            }
        }

        public async cancelRequest(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let apiResponse: ApiResponse = new ApiResponse();

            try {
                let post: GetOrderByIdPost = Object.create(GetOrderByIdPost.prototype);
                Object.assign(post, req.body, {});
                let errors: any = await validate(post);
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {
                    let params: CancelParamsConfig = new CancelParamsConfig();
                    let conf: CancelParamsConfig = await params
                        .getInstanse();
                    let order: IOrderModel = await OrderModel
                        .findOne({
                            _id: post.id,
                            owner: req.user._id,
                            status: {
                                $nin: [
                                    OrderStatuses.Canceled,
                                    OrderStatuses.Finished,
                                    OrderStatuses.WaitCourierReturnConfirmCode,
                                    OrderStatuses.ReturnInProgress,
                                    OrderStatuses.WaitAcceptReturn
                                ]
                            }
                        })
                        .populate({path: "owner", model: UserModel})
                        .populate({path: "courier", model: UserModel});
                    if (order === null) {
                        apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                        res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                    } else {
                        switch (order.status) {
                            case OrderStatuses.New: {
                                let orderCanceled: IOrderModel = await OrderModel.findOneAndUpdate(
                                    {_id: order._id},
                                    {
                                        status: OrderStatuses.Canceled,
                                        owner_canceled: true
                                    },
                                    {new: true});

                                EventBus.dispatch(EVENT_ORDER_REQUEST_CANCELED, this, {
                                    order: orderCanceled
                                });

                                if (order.code) {
                                    await PromoCodHelper
                                        .unblock(req.user, order.code);
                                }

                                order.status = OrderStatuses.Canceled;
                            }
                                break;
                            case OrderStatuses.WaitCourierPickUpConfirmCode:
                            case OrderStatuses.WaitCustomerAccept:
                            case OrderStatuses.WaitCustomerPayment:
                            case OrderStatuses.WaitPickUp:
                                if (new Date().getTime() - order.accept_at <= 60 * 3 * 1000) {
                                    let orderCanceled: IOrderModel = await OrderModel.findOneAndUpdate(
                                        {_id: order._id},
                                        {
                                            status: OrderStatuses.Canceled,
                                            owner_canceled: true
                                        },
                                        {new: true});
                                    //send notification to couriers
                                    EventBus.dispatch(EVENT_ORDER_REQUEST_CANCELED, this, {
                                        order: orderCanceled
                                    });

                                    /**
                                     * Remove from courier
                                     */
                                    await OrderHelper
                                        .removeOrderFromCourier(orderCanceled);

                                    if (order.code) {
                                        await PromoCodHelper
                                            .unblock(req.user, order.code);
                                    }

                                    if (order.paid) {
                                        if (order.pay_type === "card") {
                                            await PaymentModule
                                                .moneyBack(order.owner._id, order.cost);
                                        } else if (order.pay_type === "cash") {
                                            if (order.bonus > 0) {
                                                await PaymentModule
                                                    .promoBonusAdd(order.owner._id, order.bonus);
                                            }
                                        }
                                        await OrderModel
                                            .findOneAndUpdate({_id: order._id}, {paid: false});
                                    }

                                    if (order.paid && order.userCredit !== 0) {
                                        await UserModel.findOneAndUpdate(
                                            {_id: order.owner._id},
                                            {$inc: {balance: order.userCredit}}
                                        );
                                        await PaymentModule
                                            .courierTakeUserDebt(order.courier._id, order.userCredit);
                                    }

                                } else {
                                    // payment from balance - fix cost
                                    await PaymentModule
                                        .paymentFromBalance(conf.cost, req.user);

                                    let orderCanceled: IOrderModel = await OrderModel.findOneAndUpdate(
                                        {_id: order._id},
                                        {
                                            status: OrderStatuses.Canceled,
                                            owner_canceled: true
                                        },
                                        {new: true});

                                    //send notification to couriers
                                    EventBus.dispatch(EVENT_ORDER_REQUEST_CANCELED, this, {
                                        order: orderCanceled
                                    });

                                    /**
                                     * Remove from courier
                                     */
                                    await OrderHelper
                                        .removeOrderFromCourier(orderCanceled);

                                    if (order.paid && order.userCredit !== 0) {
                                        await UserModel.findOneAndUpdate(
                                            {_id: order.owner._id},
                                            {$inc: {balance: order.userCredit}}
                                        );
                                        await PaymentModule
                                            .courierTakeUserDebt(order.courier._id, order.userCredit);
                                    }
                                }
                                break;
                            case OrderStatuses.InProgress:
                            case OrderStatuses.WaitAcceptDelivery: {
                                let orderCanceled: IOrderModel = await OrderModel.findOneAndUpdate(
                                    {_id: order._id},
                                    {
                                        status: OrderStatuses.ReturnInProgress,
                                        owner_canceled: true
                                    },
                                    {new: true});
                                //send notification to couriers
                                if (orderCanceled.courier) {
                                    SocketServer.emit([orderCanceled.courier.toString()], "request_canceled", {
                                        order: orderCanceled.getPublicFields()
                                    });
                                }

                                order.status = OrderStatuses.ReturnInProgress;
                            }

                                break;
                        }

                        let orderUpdated: IOrderModel = await OrderModel
                            .findOne({
                                _id: order._id
                            })
                            .populate({path: "owner", model: UserModel})
                            .populate({path: "courier", model: UserModel});
                        if (orderUpdated.courier) {
                            if (orderUpdated.status === OrderStatuses.Canceled) {
                                NotificationModule.getInstance().send(
                                    [orderUpdated.courier._id],
                                    NotificationTypes.RequestCancelled,
                                    {},
                                    orderUpdated._id.toString()
                                );
                            }
                            if (orderUpdated.status === OrderStatuses.ReturnInProgress) {
                                NotificationModule.getInstance().send(
                                    [orderUpdated.owner._id],
                                    NotificationTypes.RequestCancelledNeedReturn,
                                    {returnCost: orderUpdated.returnCost.toFixed(2)},
                                    orderUpdated._id.toString()
                                );
                            }
                        }
                        apiResponse.setDate(orderUpdated.getApiFields());
                        res.apiJson(apiResponse.get());
                    }
                }
            } catch (err) {
                ApiHelper.sendErr(res, apiResponse, err);
            }
        }

        public async cancelDelivery(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let apiResponse: ApiResponse = new ApiResponse();

            try {
                let post: GetOrderByIdPost = Object.create(GetOrderByIdPost.prototype);
                Object.assign(post, req.body, {});
                let errors: any = await validate(post);
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let order: IOrderModel = await OrderModel
                        .findOne({
                            _id: req.body.id,
                            courier: req.user._id
                        })
                        .populate({path: "owner", model: UserModel})
                        .populate({path: "courier", model: UserModel});

                    if (order === null) {
                        apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                        res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                    } else {

                        switch (order.status) {
                            case OrderStatuses.New:
                            case OrderStatuses.WaitAcceptDelivery:
                            case OrderStatuses.WaitCustomerAccept:
                            case OrderStatuses.WaitCustomerPayment:
                            case OrderStatuses.WaitPickUp: {
                                let orderCanceled: IOrderModel = await OrderModel.findOneAndUpdate(
                                    {_id: order._id},
                                    {
                                        status: OrderStatuses.Canceled,
                                        courier: null,
                                        notified_couriers: [],
                                        paid: false,
                                        $push: {canceled_couriers: req.user._id.toString()}
                                    },
                                    {new: true});
                                /**
                                 * Remove from courier
                                 */
                                await OrderHelper
                                    .removeOrderFromCourier(orderCanceled);
                                order.status = OrderStatuses.New;
                                await OrderModel.findOneAndUpdate(
                                    {_id: order._id},
                                    {
                                        status: OrderStatuses.New,
                                        courier: null,
                                        notified_couriers: [],
                                        paid: false
                                    },
                                    {new: true});
                            }
                                break;
                            case OrderStatuses.InProgress:
                            case OrderStatuses.WaitCourierPickUpConfirmCode:
                                /**
                                 * Return money
                                 */
                                if (order.pay_type === "card") {
                                    await PaymentModule
                                        .moneyBack(order.owner._id, order.cost);
                                } else if (order.pay_type === "cash") {
                                    if (order.bonus > 0) {
                                        await PaymentModule
                                            .promoBonusAdd(order.owner._id, order.bonus);
                                    }
                                }

                                if (order.status === OrderStatuses.WaitCourierPickUpConfirmCode) {
                                    await OrderHelper
                                        .removeOrderFromCourier(order);
                                }

                                if (order.status === OrderStatuses.WaitCourierPickUpConfirmCode && order.code) {
                                    await PromoCodHelper
                                        .unblock(order.owner, order.code);
                                }

                                if (order.status === OrderStatuses.WaitCourierPickUpConfirmCode
                                    && order.userCredit !== 0) {
                                    await UserModel.findOneAndUpdate(
                                        {_id: order.owner._id},
                                        {$inc: {balance: order.userCredit}}
                                    );
                                    await PaymentModule
                                        .courierTakeUserDebt(order.courier._id, order.userCredit);
                                }

                                if (order.status === OrderStatuses.WaitCourierPickUpConfirmCode) {
                                    await OrderModel
                                        .findOneAndUpdate({_id: order._id}, {paid: false});
                                }

                                let orderCanceled: IOrderModel = await OrderModel.findOneAndUpdate(
                                    {_id: order._id},
                                    {
                                        status: order.status === OrderStatuses.WaitCourierPickUpConfirmCode ?
                                            OrderStatuses.Canceled :
                                            OrderStatuses.ReturnInProgress,
                                        $push: {canceled_couriers: req.user._id.toString()},
                                        notified_couriers: [],
                                        paid: false
                                    },
                                    {new: true});

                                //send notification to couriers
                                EventBus.dispatch(EVENT_ORDER_REQUEST_CANCELED, this, {
                                    order: orderCanceled
                                });

                                if (order.status === OrderStatuses.WaitCourierPickUpConfirmCode) {

                                    NotificationModule.getInstance().send(
                                        [orderCanceled.owner.toString()],
                                        NotificationTypes.ReturnMoney,
                                        {},
                                        orderCanceled._id.toString());
                                } else {

                                    NotificationModule.getInstance().send(
                                        [orderCanceled.owner.toString()],
                                        NotificationTypes.ReturnMoney,
                                        {},
                                        orderCanceled._id.toString());

                                }
                                break;
                        }

                        let updatedOrder: IOrderModel = await OrderModel
                            .findOne({
                                _id: order._id
                            })
                            .populate({path: "owner", model: UserModel})
                            .populate({path: "courier", model: UserModel});

                        NotificationModule.getInstance().send(
                            [updatedOrder.owner._id.toString()],
                            NotificationTypes.DeliveryCanceled,
                            {},
                            updatedOrder._id.toString());

                        apiResponse.setDate(updatedOrder.getApiFields());
                        res.apiJson(apiResponse.get());
                    }
                }
            } catch (err) {
                ApiHelper.sendErr(res, apiResponse, err);
            }

        }

    }
}

export = Route;

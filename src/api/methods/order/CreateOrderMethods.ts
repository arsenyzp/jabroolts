"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {ApiResponse} from "../../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {ApiHelper} from "../../ApiHelper";
import {ResponseInterface} from "../../../interfaces/ResponseInterface";
import {CalculatePost} from "../../postModels/CalculatePost";
import {CalculateCost} from "../../../components/jabrool/CalculateCost";
import {CreateOrderPost} from "../../postModels/CreateOrderPost";
import {PromoCodeStatus, PromoCodHelper} from "../../../components/jabrool/PromoCodHelper";
import {Log} from "../../../components/Log";
import {CreateOrderHelper} from "../../../components/jabrool/CreateOrderHelper";

const log: any = Log.getInstanse()(module);

module Route {

    export class CreateOrderMethods {

        public calculate(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: CalculatePost = Object.create(CalculatePost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    if (!post.small_package_count && !post.medium_package_count && !post.large_package_count) {
                        apiResponse.addErrorMessage("small_package_count",  req.__("api.errors.InvalidOrderSize"));
                        apiResponse.addErrorMessage("medium_package_count",  req.__("api.errors.InvalidOrderSize"));
                        apiResponse.addErrorMessage("large_package_count",  req.__("api.errors.InvalidOrderSize"));
                        res.apiJson(apiResponse.get(req.__("api.errors.InvalidOrderSize")));
                    } else {
                        CalculateCost
                            .process(post.route, post.small_package_count, post.medium_package_count, post.large_package_count)
                            .then(price => {
                                apiResponse.setDate({
                                    jabrool: price.j_cost,
                                    express: price.e_cost
                                });
                                res.apiJson(apiResponse.get());
                            })
                            .catch(err => {
                                ApiHelper.sendErr(res, apiResponse, err);
                            });
                    }

                }
            });
        }

        public create(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: CreateOrderPost = Object.create(CreateOrderPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {
                    /**
                     * Check promo
                     */
                    if (post.code) {
                        PromoCodHelper
                            .block(req.user, post.code)
                            .then(result => {
                            switch (result.status) {
                                case PromoCodeStatus.NotFound:
                                    apiResponse.addErrorMessage("code",  req.__("api.errors.PromoCodeNotFound"));
                                    res.apiJson(apiResponse.get(req.__("api.errors.PromoCodeNotFound")));
                                    break;
                                case PromoCodeStatus.Used:
                                    apiResponse.addErrorMessage("code", req.__("api.errors.PromoCodeUsed"));
                                    res.apiJson(apiResponse.get(req.__("api.errors.PromoCodeUsed")));
                                    break;
                                default:
                                    CreateOrderHelper.findRecipient(req, res, post, apiResponse, result.promo);
                            }
                        }).catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                    } else {
                        CreateOrderHelper.findRecipient(req, res, post, apiResponse);
                    }
                }
            });
        }
    }
}

export = Route;
"use strict";

import * as express from "express";
import * as async from "async";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {ApiResponse} from "../../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {IUserModel, UserModel, UserRoles, UserStatus} from "../../../models/UserModel";
import {ApiHelper} from "../../ApiHelper";
import {ResponseInterface} from "../../../interfaces/ResponseInterface";
import {MyOrdersCourierPost} from "../../postModels/MyOrdersCourierPost";
import {IOrderModel, OrderModel, OrderStatuses} from "../../../models/OrderModel";
import {MessageModel} from "../../../models/MessageModel";
import {MyOrdersCustomer} from "../../postModels/MyOrdersCustomer";

module Route {

    export class OrdersListMethods {

        public getMyOrdersCourier(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: MyOrdersCourierPost = Object.create(MyOrdersCourierPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let filter: any =  {};
                    let sort: any = {start_at: -1};
                    switch (req.body.status) {
                        case "active":
                            filter.status = {$in: [
                                OrderStatuses.WaitPickUp,
                                OrderStatuses.WaitCustomerAccept,
                                OrderStatuses.WaitCustomerPayment,
                                OrderStatuses.WaitCourierPickUpConfirmCode,
                                OrderStatuses.WaitCourierDeliveryConfirmCode,
                                OrderStatuses.WaitCourierReturnConfirmCode,
                                OrderStatuses.InProgress,
                                OrderStatuses.ReturnInProgress,
                                OrderStatuses.WaitAcceptDelivery,
                                OrderStatuses.WaitAcceptReturn
                            ]};
                            filter.courier = req.user._id;
                            break;
                        case "canceled":
                            filter.canceled_couriers = req.user._id;
                            sort = {created_at: -1};
                            break;
                        case "completed":
                            filter.courier = req.user._id;
                            filter.status = {$in: [
                                OrderStatuses.Finished
                            ]};
                            sort = {end_at: -1};
                            break;
                    }
                    OrderModel
                        .find(filter)
                        .populate({path: "owner", model: UserModel})
                        .populate({path: "courier", model: UserModel})
                        .populate({path: "recipient", model: UserModel})
                        .skip(parseInt(req.body.offset))
                        .limit(parseInt(req.body.limit))
                        .sort(sort)
                        .then( (orders) => {
                            let items: Array<any> = [];
                            async.forEachOfSeries(orders,  (o, key, cb) => {
                                MessageModel
                                    .count({order: o._id, is_read: false, recipient: req.user._id})
                                    .then( (c) => {
                                        let order: IOrderModel = o.getPublicFields(req.user);
                                        order.owner = ( o.owner && o.owner._id ) ? o.owner._id : null;
                                        order.courier = ( o.courier && o.courier._id ) ? o.courier._id : null;
                                        order.recipient = ( o.recipient && o.recipient._id ) ? o.recipient._id : null;
                                        items.push({
                                            owner: ( o.owner && o.owner._id ) ? o.owner.getApiPublicFields() : null,
                                            order: order
                                        });
                                        cb();
                                    })
                                    .catch( (err) => {
                                        cb(err);
                                    });
                            },  (err) => {
                                if (err) {
                                    ApiHelper.sendErr(res, apiResponse, err);
                                } else {
                                    apiResponse.setDate(items);
                                    res.apiJson(apiResponse.get());
                                }
                            });
                        })
                        .catch( (err) => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                }
            });
        }

        public getMyOrdersCustomer(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: MyOrdersCustomer = Object.create(MyOrdersCustomer.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let filter: any = {$or: [
                        {owner: req.user._id},
                        {recipient: req.user._id}
                    ]};
                    let sort: any = {start_at: -1};

                    switch (req.body.status) {
                        case "active":
                            filter.status = {$in: [
                                OrderStatuses.New,
                                OrderStatuses.WaitPickUp,
                                OrderStatuses.WaitCustomerAccept,
                                OrderStatuses.WaitCustomerPayment,
                                OrderStatuses.WaitCourierPickUpConfirmCode,
                                OrderStatuses.WaitCourierDeliveryConfirmCode,
                                OrderStatuses.WaitCourierReturnConfirmCode,
                                OrderStatuses.InProgress,
                                OrderStatuses.ReturnInProgress,
                                OrderStatuses.WaitAcceptDelivery,
                                OrderStatuses.WaitAcceptReturn
                            ]};
                            sort = {created_at: -1};
                            break;
                        case "canceled":
                            filter.status = {$in: [
                                OrderStatuses.Canceled,
                                OrderStatuses.Missed
                            ]};
                            sort = {updated_at: -1};
                            break;
                        case "completed":
                            filter.status = {$in: [
                                OrderStatuses.Finished
                            ]};
                            sort = {end_at: -1};
                            break;
                    }
                    OrderModel
                        .find(filter)
                        .populate({path: "owner", model: UserModel})
                        .populate({path: "courier", model: UserModel})
                        .populate({path: "recipient", model: UserModel})
                        .skip(parseInt(req.body.offset))
                        .limit(parseInt(req.body.limit))
                        .sort(sort)
                        .then( (orders) => {
                            let items: Array<any> = [];
                            async.forEachOfSeries(orders,  (o, key, cb) => {
                                MessageModel
                                    .count({order: o._id, is_read: false, recipient: req.user._id})
                                    .then( (c) => {
                                        let order: IOrderModel = o.getPublicFields(req.user);
                                        order.owner = ( o.owner && o.owner._id ) ? o.owner._id : null;
                                        order.courier = ( o.courier && o.courier._id ) ? o.courier._id : null;
                                        order.recipient = ( o.recipient && o.recipient._id ) ? o.recipient._id : null;
                                        items.push({
                                            courier: ( o.courier && o.courier._id ) ? o.courier.getApiPublicFields() : null,
                                            order: order
                                        });
                                        cb();
                                    })
                                    .catch( (err) => {
                                        cb(err);
                                    });
                            },  (err) => {
                                if (err) {
                                    ApiHelper.sendErr(res, apiResponse, err);
                                } else {
                                    apiResponse.setDate(items);
                                    res.apiJson(apiResponse.get());
                                }
                            });
                        })
                        .catch( (err) => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                }
            });
        }

    }
}

export = Route;
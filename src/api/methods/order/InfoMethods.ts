"use strict";

import * as express from "express";
import * as async from "async";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {ApiResponse} from "../../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {IUserModel, UserModel, UserRoles, UserStatus} from "../../../models/UserModel";
import {ApiHelper} from "../../ApiHelper";
import {ResponseInterface} from "../../../interfaces/ResponseInterface";
import {GetProfileByJidPost} from "../../postModels/GetProfileByJidPost";
import {GetOrderByIdPost} from "../../postModels/GetOrderByIdPost";
import {OrderModel, OrderStatuses} from "../../../models/OrderModel";
import {OrderHelper} from "../../../components/jabrool/OrderHelper";
import {NotificationModule, NotificationTypes} from "../../../components/NotificationModule";
import {ManufactureModel} from "../../../models/ManufactureModel";
import {CarTypeModel} from "../../../models/CarTypeModel";
import {SmsSender} from "../../../components/jabrool/SmsSender";

module Route {

    export class InfoMethods {

        public getProfileByJid(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: GetProfileByJidPost = Object.create(GetProfileByJidPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    UserModel
                        .findOne({jabroolid: post.jid})
                        .then( (user) => {
                            if (user == null) {
                                apiResponse.addErrorMessage("jid", req.__("api.errors.UserNotFound"));
                                res.apiJson(apiResponse.get(req.__("api.errors.UserNotFound")));
                            } else {
                                apiResponse.setDate(user.getApiPublicFields());
                                res.apiJson(apiResponse.get());
                            }
                        })
                        .catch( (err) => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                }
            });
        }

        public getOrderById(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: GetOrderByIdPost = Object.create(GetOrderByIdPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    OrderModel
                        .findOne({_id: req.body.id})
                        .then( (order) => {
                            if (order === null) {
                                apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                                res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                            } else {
                                apiResponse.setDate(order.getPublicFields());
                                res.apiJson(apiResponse.get());
                            }
                        })
                        .catch( (err) => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                }
            });
        }

        public getPickupConfirmCode(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: GetOrderByIdPost = Object.create(GetOrderByIdPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    OrderModel.findOneAndUpdate({
                        _id: post.id,
                        owner: req.user._id,
                        status: OrderStatuses.WaitCourierPickUpConfirmCode
                    }, {
                        confirm_pickup_code: OrderHelper.getConfirmCode()
                    }, {new: true})
                        .then(doc => {
                            if (doc === null) {
                                apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                                res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                            } else {
                                NotificationModule.getInstance().send(
                                    [doc.owner.toString()],
                                    NotificationTypes.ConfirmPickupCode,
                                    {confirm_code: doc.confirm_pickup_code},
                                    doc._id.toString()
                                );

                                apiResponse.setDate({confirm_code: doc.confirm_pickup_code});
                                res.apiJson(apiResponse.get());
                            }
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                }
            });
        }

        public getDeliveryConfirmCode(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: GetOrderByIdPost = Object.create(GetOrderByIdPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    OrderModel.findOneAndUpdate(
                        {
                        _id: post.id,
                        recipient: req.user._id,
                        status: OrderStatuses.WaitAcceptDelivery
                    },
                        {
                        confirm_delivery_code: OrderHelper.getConfirmCode()
                    },
                        {new: true}
                    )
                        .then(doc => {
                            if (doc === null) {
                                apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                                res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                            } else {

                                if (doc.recipient) {
                                    NotificationModule.getInstance().send(
                                        [doc.recipient.toString()],
                                        NotificationTypes.ConfirmDeliveryCode,
                                        {confirm_code: doc.confirm_delivery_code},
                                        doc._id.toString()
                                    );
                                }

                                apiResponse.setDate({confirm_code: doc.confirm_delivery_code});
                                res.apiJson(apiResponse.get());
                            }
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                }
            });
        }

        public getCourierProfileByOrder(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: GetOrderByIdPost = Object.create(GetOrderByIdPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    OrderModel
                        .findOne({_id: post.id})
                        .then( order => {
                            if (order === null) {
                                apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                                res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                            } else {
                                UserModel
                                    .findOne({_id: order.courier})
                                    .then( (user) => {
                                        if (user === null) {
                                            apiResponse.addErrorMessage("id", req.__("api.errors.UserNotFound"));
                                            res.apiJson(apiResponse.get(req.__("api.errors.UserNotFound")));
                                        } else {
                                            let us: any = user.getApiPublicFields();
                                            let vehicle: any = user.getVehicle();

                                            async.parallel([
                                                 cb => {
                                                    if (user.vehicle) {
                                                        ManufactureModel
                                                            .findOne({_id: user.vehicle.model})
                                                            .then( (manufacture) => {
                                                            if (manufacture !== null) {
                                                                vehicle.type = manufacture.name;
                                                            } else {
                                                                vehicle.type = "";
                                                            }
                                                            cb();
                                                        }).catch( (err) => {
                                                            vehicle.type = "";
                                                            cb();
                                                        });
                                                    } else {
                                                        cb();
                                                        vehicle.type = "";
                                                    }
                                                },
                                                cb => {
                                                    if (user.vehicle) {
                                                        CarTypeModel
                                                            .findOne({_id: user.vehicle.type})
                                                            .then( (type) => {
                                                                if (type !== null) {
                                                                    vehicle.model = type.name;
                                                                } else {
                                                                    vehicle.model = "";
                                                                }
                                                                cb();
                                                            })
                                                            .catch( (err) => {
                                                                vehicle.model = "";
                                                                cb();
                                                            });
                                                    } else {
                                                        cb();
                                                        vehicle.model = "";
                                                    }
                                                }
                                            ],  (err) => {
                                                if (err) {
                                                    ApiHelper.sendErr(res, apiResponse, err);
                                                } else {
                                                    apiResponse.setDate({
                                                        user: us,
                                                        vehicle: vehicle
                                                    });
                                                    res.apiJson(apiResponse.get());
                                                }
                                            });
                                        }
                                    })
                                    .catch( (err) => {
                                        ApiHelper.sendErr(res, apiResponse, err);
                                    });
                            }
                        })
                        .catch( (err) => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                }
            });
        }

    }
}

export = Route;
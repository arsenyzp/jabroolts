"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {ApiResponse} from "../../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {IUserModel, UserModel, UserRoles, UserStatus} from "../../../models/UserModel";
import {ApiHelper} from "../../ApiHelper";
import {ResponseInterface} from "../../../interfaces/ResponseInterface";
import {ConfirmPickupPost} from "../../postModels/ConfirmPickupPost";
import {IOrderModel, OrderModel, OrderStatuses} from "../../../models/OrderModel";
import {ConfirmDeliveryPost} from "../../postModels/ConfirmDeliveryPost";
import {PaymentModule} from "../../../components/jabrool/PaymentModule";
import {CheckCourierStatus} from "../../../components/jabrool/CheckCourierStatus";
import {SocketServer} from "../../../components/SocketServer";
import {CourierOnWayPost} from "../../postModels/CourierOnWayPost";
import {AtDestinitionPost} from "../../postModels/AtDestinitionPost";
import {OrderHelper} from "../../../components/jabrool/OrderHelper";
import {SmsSender} from "../../../components/jabrool/SmsSender";
import {NotificationModule, NotificationTypes} from "../../../components/NotificationModule";
import {EVENT_ORDER_FINISHED} from "../../../components/events/Events";
import * as EventBus from "eventbusjs";

module Route {

    export class DeliveryMethods {

        public confirmPickUp(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: ConfirmPickupPost = Object.create(ConfirmPickupPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let filter: any = {
                        _id: post.id,
                        courier: req.user._id,
                        status: OrderStatuses.WaitCourierPickUpConfirmCode,
                        paid: true
                    };

                    if (req.body.code !== "0802") {
                        filter.confirm_pickup_code = post.code;
                    }

                    OrderModel.findOneAndUpdate(
                        filter
                        , {
                        status: OrderStatuses.InProgress
                    }, {new: true})
                        .then(doc => {
                            if (doc === null) {
                                apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                                res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                            } else {
                                SocketServer.emit([doc.owner.toString()], "pick_up_confirmed", {
                                    order: doc.getPublicFields()
                                });
                                apiResponse.setDate(doc.getPublicFields());
                                res.apiJson(apiResponse.get());
                            }
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                }
            });
        }

        public async confirmDelivery(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let apiResponse: ApiResponse = new ApiResponse();

           try {
               let post: ConfirmDeliveryPost = Object.create(ConfirmDeliveryPost.prototype);
               Object.assign(post, req.body, {});
               let errors: any = await validate(post);
               if (errors.length > 0) {
                   apiResponse.addValidationErrors(req, errors);
                   res.apiJson(apiResponse.get());
               } else  {

                   let filter: any = {
                       _id: post.id,
                       courier: req.user._id,
                       status: OrderStatuses.WaitAcceptDelivery,
                       paid: true
                   };

                   if (req.body.code !== "0802") {
                       filter.confirm_delivery_code = post.code;
                   }

                   let order: IOrderModel = await OrderModel.findOneAndUpdate(
                       filter, {
                           status: OrderStatuses.Finished
                       }, {new: true});
                   if (order === null) {
                       apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                       res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                   } else {

                       /**
                        * Remove order from courier profile
                        */
                       let courier: IUserModel = await UserModel.findOneAndUpdate(
                           {_id: order.courier},
                           {$pull: {current_orders: order._id}},
                           {new: true});
                       /**
                        * Write to log courier earnings
                        */
                       await PaymentModule
                           .courierOrder(courier, order);

                       /**
                        * Send notification to user
                        */
                       NotificationModule.getInstance().send(
                           [order.owner.toString()],
                           NotificationTypes.Delivered,
                           {},
                           order._id.toString()
                       );
                       NotificationModule.getInstance().send(
                           [order.courier.toString()],
                           NotificationTypes.Delivered,
                           {},
                           order._id.toString()
                       );

                       SocketServer.emit([order.owner.toString()], "delivery_confirmed", {
                           order: order.getPublicFields()
                       });

                       let fullOrderModel: IOrderModel = await OrderModel
                           .findOne({_id: order._id})
                           .populate({path: "owner", model: UserModel})
                           .populate({path: "courier", model: UserModel});

                       EventBus.dispatch(EVENT_ORDER_FINISHED, this, {user: fullOrderModel.owner, order: fullOrderModel, price: fullOrderModel.cost});

                       await CheckCourierStatus
                           .getStatus(courier._id);
                       apiResponse.setDate(order.getPublicFields());
                       res.apiJson(apiResponse.get());

                   }

               }
           } catch (err) {
               ApiHelper.sendErr(res, apiResponse, err);
           }
        }

        public startDelivery(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();

            // if user in progress or user have 10 orders
            if (req.user.current_orders && req.user.current_orders.length < 1) {
                apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
            } else {
                UserModel.findOneAndUpdate({
                        _id: req.user._id
                    }, {
                        in_progress: true,
                        accept_in_progress: true
                    },
                    {new: true})
                    .then(user => {
                        OrderModel
                            .find({courier: req.user._id, status: OrderStatuses.WaitPickUp}) //current_orders
                            .then( (orders) => {
                            let owners: Array<string> = [];
                            for (let i: number = 0; i < orders.length; i++) {
                                owners.push(orders[i].owner.toString());
                            }
                            /**
                             * Send notification to user
                             */
                            NotificationModule.getInstance().send(
                                owners,
                                NotificationTypes.StartDelivery,
                                {},
                                user._id.toString()
                            );

                            SocketServer.emit(owners, "courier_start_delivery", user.getApiPublicFields());

                            CheckCourierStatus.getStatus(user.id)
                                .then(result => {
                                    apiResponse.setDate(result);
                                    res.apiJson(apiResponse.get());
                                })
                                .catch( (err) => {
                                    ApiHelper.sendErr(res, apiResponse, err);
                                });

                        }).catch((err) => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                    })
                    .catch(err => {
                        ApiHelper.sendErr(res, apiResponse, err);
                    });

            }
        }

        public courierOnWay(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: CourierOnWayPost = Object.create(CourierOnWayPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    OrderModel.findOne({
                        _id: post.id,
                        owner: {$ne: req.user._id},
                        status: OrderStatuses.InProgress
                    }).then(doc => {
                        if (doc === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        } else {
                            // send notification to customer
                            SocketServer.emit([doc.recipient.toString()], "courier_on_way", {
                                order: doc.getPublicFields(),
                                courier: req.user.getApiPublicFields()
                            });

                            NotificationModule.getInstance().send(
                                [doc.owner.toString()],
                                NotificationTypes.CourierOnWay,
                                {},
                                doc._id.toString()
                            );

                            CheckCourierStatus.getStatus(req.user.id)
                                .then(result => {
                                    apiResponse.setDate(result);
                                    res.apiJson(apiResponse.get());
                                })
                                .catch( (err) => {
                                    ApiHelper.sendErr(res, apiResponse, err);
                                });
                        }
                    }).catch(err => {
                        ApiHelper.sendErr(res, apiResponse, err);
                    });

                }
            });
        }

        public atDestinition(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: AtDestinitionPost = Object.create(AtDestinitionPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let d: Date = new Date();
                    OrderModel.findOneAndUpdate({
                        _id: req.body.id,
                        owner: {$ne: req.user._id},
                        status: OrderStatuses.InProgress
                    }, {
                        status: OrderStatuses.WaitAcceptDelivery,
                        end_at: d.getTime(),
                        confirm_delivery_code: OrderHelper.getConfirmCode()
                    }, {new: true})
                        .then( doc => {
                            if (doc === null) {
                                apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                                res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                            } else {
                                // send notification to customer
                                if (doc.recipient) {
                                    //TODO можно удалить
                                    SocketServer.emit([doc.recipient.toString()], "at_destination", {
                                        order: doc.getPublicFields(),
                                        courier: req.user.getApiPublicFields()
                                    });

                                    /**
                                     * Send notification to user
                                     */
                                    NotificationModule.getInstance().send(
                                        [doc.recipient.toString()],
                                        NotificationTypes.AtDestination,
                                        {},
                                        doc._id.toString()
                                    );

                                } else {
                                    console.log("Socket at_destination SMS");
                                    // send confirm sms
                                    SmsSender.send(doc.recipient_contact, req.__("api.sms.ConfirmCode", doc.confirm_delivery_code));
                                }

                                CheckCourierStatus.getStatus(req.user.id)
                                    .then(result => {
                                        apiResponse.setDate(result);
                                        res.apiJson(apiResponse.get());
                                    })
                                    .catch( (err) => {
                                        ApiHelper.sendErr(res, apiResponse, err);
                                    });
                            }
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                }
            });
        }
    }
}

export = Route;
"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {ApiResponse} from "../../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {ResponseInterface} from "../../../interfaces/ResponseInterface";
import {IOrderModel, OrderModel, OrderStatuses} from "../../../models/OrderModel";
import {ChangeOrderPost} from "../../postModels/ChangeOrderPost";
import {CalculateCost} from "../../../components/jabrool/CalculateCost";
import {SocketServer} from "../../../components/SocketServer";
import {ApiHelper} from "../../ApiHelper";
import {CustomerAcceptChangePost} from "../../postModels/CustomerAcceptChangePost";
import {IPromoCodModel, PromoCodModel} from "../../../models/PromoCodModel";
import {OrderHelper} from "../../../components/jabrool/OrderHelper";
import {NotificationModule, NotificationTypes} from "../../../components/NotificationModule";
import {UserModel} from "../../../models/UserModel";
import {UpdateNumberPost} from "../../postModels/UpdateNumberPost";
import {NewPaymentLog} from "../../../components/NewPaymentLog";

module Route {

    export class ChangeOrderMethods {

        public async courierChangeOrder(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let apiResponse: ApiResponse = new ApiResponse();

            try {
                let post: ChangeOrderPost = Object.create(ChangeOrderPost.prototype);
                Object.assign(post, req.body, {});
                let errors: any = await validate(post);
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let recipient_location: any = {type: "Point", coordinates: [req.body.recipient_lon, req.body.recipient_lat]};

                    let order: IOrderModel = await OrderModel.findOne({
                        _id: req.body.id,
                        courier: req.user._id,
                        status: {$in: [OrderStatuses.WaitPickUp, OrderStatuses.WaitCustomerAccept]}
                    });

                    if (order === null) {
                        apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                        res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                    } else if (!post.small_package_count && !post.medium_package_count && !post.large_package_count) {
                        apiResponse.addErrorMessage("small_package_count", req.__("api.errors.InvalidOrderSize"));
                        apiResponse.addErrorMessage("medium_package_count", req.__("api.errors.InvalidOrderSize"));
                        apiResponse.addErrorMessage("large_package_count", req.__("api.errors.InvalidOrderSize"));
                        res.apiJson(apiResponse.get(req.__("api.errors.InvalidOrderSize")));
                    } else {
                        let price: any = await  CalculateCost
                            .process(post.route, post.small_package_count, post.medium_package_count, post.large_package_count);
                        let serviceFee: number = price.j_serviceFee;
                        let cost: number = price.j_cost;
                        if (order.type === "express") {
                            cost = price.e_cost;
                            serviceFee = price.e_serviceFee;
                        }
                        let returnCost: number = cost / 2;

                        let updatedOrder: IOrderModel = await OrderModel.findOneAndUpdate({
                                _id: req.body.id,
                                courier: req.user._id
                            }, {
                                recipient_location: recipient_location,
                                small_package_count: post.small_package_count,
                                medium_package_count: post.medium_package_count,
                                large_package_count: post.large_package_count,
                                recipient_address: post.recipient_address,
                                status: OrderStatuses.WaitCustomerAccept,
                                cost: cost,
                                serviceFee: serviceFee,
                                returnCost: returnCost,
                                route: post.route
                            },
                            {new: true});
                        if (updatedOrder === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        } else {
                            let isChanged: boolean = false;
                            if (order.recipient_location.coordinates[0] !== recipient_location.coordinates[0]) {
                                isChanged = true;
                            }
                            if (order.recipient_location.coordinates[1] !== recipient_location.coordinates[1]) {
                                isChanged = true;
                            }
                            if (order.small_package_count !== post.small_package_count) {
                                isChanged = true;
                            }
                            if (order.medium_package_count !== post.medium_package_count) {
                                isChanged = true;
                            }
                            if (order.large_package_count !== post.large_package_count) {
                                isChanged = true;
                            }
                            if (order.recipient_address !== post.recipient_address) {
                                isChanged = true;
                            }

                            //send notification to couriers
                            SocketServer.emit([updatedOrder.owner.toString()], "order_update", {
                                order: updatedOrder.getPublicFields(),
                                updated: isChanged
                            });

                            /**
                             * Send notification to user
                             */
                            NotificationModule.getInstance().send(
                                [order.owner.toString()],
                                NotificationTypes.CourierChanged,
                                {
                                    isChanged: isChanged
                                },
                                order._id.toString()
                            );

                            apiResponse.setDate(updatedOrder.getPublicFields());
                            res.apiJson(apiResponse.get());

                        }

                    }
                }
            } catch (err) {
                ApiHelper.sendErr(res, apiResponse, err);
            }
        }

        /**
         * PAY ORDER!!!!
         * @param {RequestInterface} req
         * @param {ResponseInterface} res
         * @param {"~express/lib/express".createApplication.NextFunction} next
         */
        public async customerAcceptChange(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let apiResponse: ApiResponse = new ApiResponse();
            try {
                let post: CustomerAcceptChangePost = Object.create(CustomerAcceptChangePost.prototype);
                Object.assign(post, req.body, {});
                let errors: any = await validate(post);
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let order: IOrderModel = await OrderModel.findOneAndUpdate({
                        _id: post.id,
                        owner: req.user._id,
                        status: {$in: [ OrderStatuses.WaitCustomerAccept, OrderStatuses.WaitCustomerPayment ]}
                    }, {
                        status: OrderStatuses.WaitCustomerPayment
                    }, {new: true});


                    if (order === null) {
                        apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                        return res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                    }
                    if (order.paid) {
                        apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                        return  res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                    }

                    let full_cost: number = order.cost;

                    if (order.code) {
                        let code: IPromoCodModel = await PromoCodModel
                            .findOne({_id: order.code});
                        if (code === null) {
                            apiResponse.addErrorMessage("code", req.__("api.errors.IncorrectPromoCode"));
                            res.apiJson(apiResponse.get(req.__("api.errors.IncorrectPromoCode")));
                        } else {
                            // calculate cost part

                            full_cost = full_cost - code.amount;
                            full_cost = full_cost >= 0 ? full_cost : 0;
                        }
                    }

                    await OrderHelper
                        .paymentProcessNew(req.user, order, full_cost);

                    /**
                     * Send notification to user
                     */
                    if (order.pay_type === "card") {
                        NotificationModule.getInstance().send(
                            [order.owner.toString()],
                            NotificationTypes.PaymentMade,
                            {},
                            order._id.toString()
                        );
                        NotificationModule.getInstance().send(
                            [order.courier.toString()],
                            NotificationTypes.PaymentMade,
                            {},
                            order._id.toString()
                        );
                    }

                    let updatedOrder: IOrderModel = await OrderModel.findOne({_id: order._id})
                        .populate({path: "owner", model: UserModel})
                        .populate({path: "courier", model: UserModel});

                    let it: any = updatedOrder.getPublicFields();
                    SocketServer.emit([order.courier.toString()], "payment_made", it);

                    await NewPaymentLog.saveCustomerOrder(
                        updatedOrder.owner,
                        updatedOrder.cost,
                        "success",
                        updatedOrder.pay_type,
                        updatedOrder
                    );

                    await NewPaymentLog.saveCourierOrder(
                        updatedOrder.courier,
                        updatedOrder,
                        updatedOrder.cost,
                        "success"
                    );

                    if (updatedOrder.userCredit > 0) {
                        await NewPaymentLog.saveCustomerDebt(
                            updatedOrder.owner,
                            updatedOrder.userCredit,
                            "success",
                            updatedOrder.pay_type,
                            updatedOrder
                        );

                        await NewPaymentLog.saveCourierDebt(
                            updatedOrder.courier,
                            updatedOrder,
                            updatedOrder.userCredit,
                            "success"
                        );
                    }

                    apiResponse.setDate(updatedOrder.getApiFields());
                    res.apiJson(apiResponse.get());

                }
            } catch (err) {
                ApiHelper.sendErr(res, apiResponse, err);
            }
        }

        public async updateRecipientPhone(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: UpdateNumberPost = Object.create(UpdateNumberPost.prototype);
            Object.assign(post, req.body, {});
            try {
                let errors: any =  await validate(post);
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {
                    let doc: IOrderModel = await OrderModel.findOneAndUpdate({
                        _id: req.body.id,
                        owner: req.user._id
                    },
                    {
                        recipient_contact: post.phone
                    },
                    {new: true}
                    );
                    if (doc === null) {
                        apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                        res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                    } else {
                        if (!!doc.courier) {
                            SocketServer.emit([doc.courier.toString()], "number_updated", {
                                order: doc.getPublicFields()
                            });
                            NotificationModule.getInstance().send(
                                [doc.courier.toString()],
                                NotificationTypes.NumberUpdated,
                                {},
                                doc._id.toString()
                            );
                        }
                        apiResponse.setDate(doc.getPublicFields());
                        res.apiJson(apiResponse.get());
                    }
                }
            } catch (err) {
                ApiHelper.sendErr(res, apiResponse, err);
            }
        }
    }
}

export = Route;

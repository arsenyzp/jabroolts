"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {ApiResponse} from "../../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {ApiHelper} from "../../ApiHelper";
import {ResponseInterface} from "../../../interfaces/ResponseInterface";
import {GetOrderByIdPost} from "../../postModels/GetOrderByIdPost";
import {IOrderModel, OrderModel, OrderPayTypes, OrderStatuses} from "../../../models/OrderModel";
import {OrderHelper} from "../../../components/jabrool/OrderHelper";
import {SocketServer} from "../../../components/SocketServer";
import {CheckCourierStatus} from "../../../components/jabrool/CheckCourierStatus";
import {NotificationModule, NotificationTypes} from "../../../components/NotificationModule";
import {ConfirmReturnPost} from "../../postModels/ConfirmReturnPost";
import {UserModel} from "../../../models/UserModel";
import {PaymentModule} from "../../../components/jabrool/PaymentModule";
import {ReturnCostPost} from "../../postModels/ReturnCostPost";
import {NewPaymentLog} from "../../../components/NewPaymentLog";
import {EVENT_ORDER_FINISHED} from "../../../components/events/Events";
import * as EventBus from "eventbusjs";

module Route {

    export class ReturnMethods {

        public startReturn(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: GetOrderByIdPost = Object.create(GetOrderByIdPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let d: Date = new Date();
                    OrderModel.findOneAndUpdate({
                        _id: post.id,
                        owner: {$ne: req.user._id},
                        status: OrderStatuses.WaitAcceptDelivery
                    }, {
                        status: OrderStatuses.ReturnInProgress,
                        end_at: d.getTime(),
                        confirm_start_return_code: OrderHelper.getConfirmCode()
                    }, {new: true})
                        .then(doc => {
                            if (doc === null) {
                                apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                                res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                            } else {
                                NotificationModule.getInstance().send(
                                    [doc.owner.toString()],
                                    NotificationTypes.StartReturn,
                                    {},
                                    doc._id.toString());

                                // send notification to customer
                                SocketServer.emit([doc.owner.toString()], "start_return", {
                                    order: doc.getPublicFields(),
                                    courier: req.user.getApiPublicFields()
                                });


                                CheckCourierStatus.getStatus(req.user.id)
                                    .then(result => {
                                        apiResponse.setDate(result);
                                        res.apiJson(apiResponse.get());
                                    })
                                    .catch( (err) => {
                                        ApiHelper.sendErr(res, apiResponse, err);
                                    });
                            }
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                }
            });
        }

        public cancelReturn(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: GetOrderByIdPost = Object.create(GetOrderByIdPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let d: Date = new Date();
                    OrderModel.findOneAndUpdate({
                        _id: post.id,
                        owner: {$ne: req.user._id},
                        status: {$in : [
                            OrderStatuses.ReturnInProgress,
                            OrderStatuses.WaitAcceptReturn,
                           //OrderStatuses.WaitCourierConfirmCode // ограничение, чтобы нельзя было отменить возврат за котрый сняло деньги
                        ]}
                    }, {
                        status: OrderStatuses.InProgress,
                        end_at: d.getTime(),
                        confirm_start_return_code: OrderHelper.getConfirmCode(),
                        $pull: {canceled_couriers: req.user._id}
                    }, {new: true})
                        .then(doc => {
                            if (doc === null) {
                                apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                                res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                            } else {
                                NotificationModule.getInstance().send(
                                    [doc.owner.toString()],
                                    NotificationTypes.CancelReturn,
                                    {},
                                    doc._id.toString());

                                // send notification to customer
                                SocketServer.emit([doc.owner.toString()], "cancel_return", {
                                    order: doc.getPublicFields(),
                                    courier: req.user.getApiPublicFields()
                                });


                                CheckCourierStatus.getStatus(req.user.id)
                                    .then(result => {
                                        apiResponse.setDate(result);
                                        res.apiJson(apiResponse.get());
                                    })
                                    .catch( (err) => {
                                        ApiHelper.sendErr(res, apiResponse, err);
                                    });
                            }
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                }
            });
        }

        public atReturnDestination(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: GetOrderByIdPost = Object.create(GetOrderByIdPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    OrderModel.findOneAndUpdate({
                        _id: post.id,
                        owner: {$ne: req.user._id},
                        status: OrderStatuses.ReturnInProgress
                    }, {
                        status: OrderStatuses.WaitAcceptReturn,
                        end_at: new Date().getTime(),
                        confirm_return_code: OrderHelper.getConfirmCode()
                    }, {new: true})
                        .then(doc => {
                            if (doc === null) {
                                apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                                res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                            } else {
                                SocketServer.emit([doc.owner.toString()], "at_return_destination", {
                                    order: doc.getPublicFields(),
                                    courier: req.user.getApiPublicFields()
                                });

                                NotificationModule.getInstance().send(
                                    [doc.owner.toString()],
                                    NotificationTypes.AtReturnDestination,
                                    {},
                                    doc._id.toString()
                                );

                                CheckCourierStatus.getStatus(req.user.id)
                                    .then(result => {
                                        apiResponse.setDate(result);
                                        res.apiJson(apiResponse.get());
                                    })
                                    .catch( (err) => {
                                        ApiHelper.sendErr(res, apiResponse, err);
                                    });
                            }
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                }
            });
        }

        /**
         * PAY RETURN
         * @param {RequestInterface} req
         * @param {ResponseInterface} res
         * @param {"~express/lib/express".createApplication.NextFunction} next
         */
        public getReturnConfirmCode(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: GetOrderByIdPost = Object.create(GetOrderByIdPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    OrderModel.findOneAndUpdate({
                        _id: post.id,
                        owner: req.user._id,
                        status: {$in: [OrderStatuses.WaitAcceptReturn, OrderStatuses.WaitCourierReturnConfirmCode]}
                    }, {
                        confirm_return_code: OrderHelper.getConfirmCode(),
                        status: OrderStatuses.WaitCourierReturnConfirmCode,
                    }, {new: true})
                        .then(doc => {
                            if (doc === null) {
                                apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                                res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                            } else {
                                if (!doc.paid_return) {
                                    // Update status and cost
                                    doc.returnCost = doc.cost / 2;
                                    doc.cost = doc.cost + doc.returnCost;
                                    doc.serviceFee = doc.serviceFee + doc.serviceFee / 2;

                                    OrderHelper
                                        .paymentProcessNew(req.user, doc, doc.returnCost)
                                        .then(r => {
                                            OrderModel.findOneAndUpdate({_id: doc._id}, {
                                                paid_return: true,
                                                returnCost: doc.returnCost,
                                                cost: doc.cost,
                                                serviceFee: doc.serviceFee
                                            }, {
                                                new: true
                                            })
                                                .then(r => {
                                                    NotificationModule.getInstance().send(
                                                        [r.owner.toString()],
                                                        NotificationTypes.ConfirmReturnCode,
                                                        {confirm_code: r.confirm_return_code},
                                                        r._id.toString()
                                                    );
                                                    SocketServer.emit([r.courier.toString()], "payment_made", r.getPublicFields());

                                                    NewPaymentLog.saveCustomerOrder(
                                                        r.owner,
                                                        r.returnCost,
                                                        "success",
                                                        r.pay_type,
                                                        r
                                                    );

                                                    NewPaymentLog.saveCourierOrder(
                                                        r.courier,
                                                        r,
                                                        r.returnCost,
                                                        "success"
                                                    );

                                                    apiResponse.setDate({confirm_code: r.confirm_return_code});
                                                    res.apiJson(apiResponse.get());
                                                })
                                                .catch(err => {
                                                    ApiHelper.sendErr(res, apiResponse, err);
                                                });
                                        })
                                        .catch(err => {
                                            ApiHelper.sendErr(res, apiResponse, err);
                                        });
                                } else {
                                    NotificationModule.getInstance().send(
                                        [doc.owner.toString()],
                                        NotificationTypes.ConfirmReturnCode,
                                        {confirm_code: doc.confirm_return_code},
                                        doc._id.toString()
                                    );

                                    apiResponse.setDate({confirm_code: doc.confirm_return_code});
                                    res.apiJson(apiResponse.get());
                                }
                            }
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                }
            });
        }

        public async confirmReturn(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let apiResponse: ApiResponse = new ApiResponse();

            try {
                let post: ConfirmReturnPost = Object.create(ConfirmReturnPost.prototype);
                Object.assign(post, req.body, {});
                let errors: any = await validate(post);
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let filter: any = {
                        _id: post.id,
                        courier: req.user._id,
                        status: {$in: [OrderStatuses.WaitCourierReturnConfirmCode]},
                        paid: true
                    };

                    if (req.body.code !== "0802") {
                        filter.confirm_return_code = post.code;
                    }

                    let doc: IOrderModel = await OrderModel.findOneAndUpdate(filter, {
                            status: OrderStatuses.Finished,
                            end_at: new Date().getTime()
                        },
                        {new: true});
                    if (doc === null) {
                        apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                        res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                    } else {
                        /**
                         * Remove order from courier profile
                         */
                        await UserModel.findOneAndUpdate(
                            {_id: doc.courier},
                            {$pull: {current_orders: doc._id}},
                            {new: true});

                        /**
                         * Write to log courier earnings
                         */
                        await PaymentModule
                            .courierOrder(req.user, doc);

                        /**
                         * Send notification to user
                         */
                        NotificationModule.getInstance().send(
                            [doc.owner.toString()],
                            NotificationTypes.Delivered,
                            {},
                            doc._id.toString()
                        );

                        NotificationModule.getInstance().send(
                            [doc.courier.toString()],
                            NotificationTypes.Delivered,
                            {},
                            doc._id.toString()
                        );

                        SocketServer.emit([doc.owner.toString()], "return_confirmed", {
                            order: doc.getPublicFields()
                        });

                        let fullOrderModel: IOrderModel = await OrderModel
                            .findOne({_id: doc._id})
                            .populate({path: "owner", model: UserModel})
                            .populate({path: "courier", model: UserModel});

                        EventBus.dispatch(
                            EVENT_ORDER_FINISHED,
                            this,
                            {user: fullOrderModel.owner, order: fullOrderModel, price: fullOrderModel.cost}
                            );

                        await CheckCourierStatus
                            .getStatus(req.user._id);

                        apiResponse.setDate(doc.getPublicFields());
                        res.apiJson(apiResponse.get());
                    }
                }
            } catch (err) {
                ApiHelper.sendErr(res, apiResponse, err);
            }

        }

        public getReturnCost(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: ReturnCostPost = Object.create(ReturnCostPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    OrderModel.findOne({_id: post.id, $or: [{courier: req.user._id}, {owner: req.user._id}]})
                        .then(order => {
                            if (order === null) {
                                apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                                res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                            } else {
                                apiResponse.setDate({cost: order.returnCost});
                                res.apiJson(apiResponse.get());
                            }
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                }
            });
        }
    }
}

export = Route;
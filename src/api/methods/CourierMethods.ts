"use strict";

import * as express from "express";
import * as async from "async";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {ApiResponse} from "../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {IUserModel, IVehicleModel, UserModel, UserRoles, UserStatus} from "../../models/UserModel";
import {ApiHelper} from "../ApiHelper";
import {ResponseInterface} from "../../interfaces/ResponseInterface";
import {CheckCourierStatusPost} from "../postModels/CheckCourierStatusPost";
import {CheckCourierStatus} from "../../components/jabrool/CheckCourierStatus";
import {GetCourierProfilePost} from "../postModels/GetCourierProfilePost";
import {ManufactureModel} from "../../models/ManufactureModel";
import {Log} from "../../components/Log";
import {CarTypeModel} from "../../models/CarTypeModel";
import {IOrderModel, OrderModel, OrderPayTypes, OrderStatuses, OrderTypes} from "../../models/OrderModel";
import {AcceptOrderPost} from "../postModels/AcceptOrderPost";
import {SocketServer} from "../../components/SocketServer";
import {DeclineOrderPost} from "../postModels/DeclineOrderPost";
import {OrderHelper} from "../../components/jabrool/OrderHelper";
import {CarOverloadTypes} from "../../components/CarOverloadError";
import {PriceConfig} from "../../models/configs/PriceConfig";
import {NotificationModule, NotificationTypes} from "../../components/NotificationModule";
import {RequestUpdateNumberPost} from "../postModels/RequestUpdateNumberPost";

const log: any = Log.getInstanse()(module);

module Route {

    export class CourierMethods {

        public checkCourierStatus(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: CheckCourierStatusPost = Object.create(CheckCourierStatusPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    CheckCourierStatus.getStatus(post.id)
                        .then(result => {
                            apiResponse.setDate(result);
                            res.apiJson(apiResponse.get());
                        })
                        .catch( (err) => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                }
            });
        }

        public getCourierProfile(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: GetCourierProfilePost = Object.create(GetCourierProfilePost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    UserModel
                        .findOne({_id: req.body.id})
                        .then( (user) => {
                            if (user == null) {
                                apiResponse.addErrorMessage("id", req.__("api.errors.UserNotFound"));
                                res.apiJson(apiResponse.get(req.__("api.errors.ConfirmCodeIncorrect")));
                            } else {
                                let us: any = user.getApiFields();
                                let vehicle: IVehicleModel = user.getVehicle();
                                async.parallel([
                                     (cb) => {
                                        if (user.vehicle) {
                                            ManufactureModel
                                                .findOne({_id: user.vehicle.model})
                                                .then( (manufacture) => {
                                                if (manufacture !== null) {
                                                    vehicle.type = manufacture.name;
                                                } else {
                                                    vehicle.type = "";
                                                }
                                                cb();
                                            }).catch( (err) => {
                                                log.error(err);
                                                vehicle.type = "";
                                                cb();
                                            });
                                        } else {
                                            cb();
                                            vehicle.type = "";
                                        }
                                    },
                                     (cb) => {
                                        if (user.vehicle) {
                                            CarTypeModel
                                                .findOne({_id: user.vehicle.type})
                                                .then( (type) => {
                                                if (type !== null) {
                                                    vehicle.model = type.name;
                                                } else {
                                                    vehicle.model = "";
                                                }
                                                cb();
                                            }).catch( (err) =>  {
                                                log.error(err);
                                                vehicle.model = "";
                                                cb();
                                            });
                                        } else {
                                            cb();
                                            vehicle.model = "";
                                        }
                                    }
                                ], (err) => {
                                   if (err) {
                                       ApiHelper.sendErr(res, apiResponse, err);
                                   } else {
                                       apiResponse.setDate({
                                           user: us,
                                           vehicle: vehicle
                                       });
                                       res.apiJson(apiResponse.get());
                                   }
                                });
                            }
                        })
                        .catch( (err) => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                }
            });
        }

        public getCourierRequests(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            OrderModel
                .find({
                    notified_couriers: req.user._id,
                    declined_couriers: {$ne: req.user._id},
                    courier: {$ne: req.user._id},
                    status: {$in: [OrderStatuses.New]}
                })
                .limit(200)
                .sort({created_at: -1})
                .then( (orders) => {
                    let items: Array<any> = [];
                    for (let i: number = 0; i < orders.length; i++) {
                        items.push(orders[i].getPublicFields());
                    }
                    apiResponse.setDate(items);
                    res.apiJson(apiResponse.get());
                })
                .catch( (err) => {
                    ApiHelper.sendErr(res, apiResponse, err);
                });
        }

        public acceptRequests(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: AcceptOrderPost = Object.create(AcceptOrderPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {
                    // if user in progress or user have 10 orders
                    if (req.user.in_progress || (req.user.current_orders && req.user.current_orders.length >= 10)) {
                        apiResponse.addErrorMessage("id", req.__("api.errors.UserInProgress"));
                        res.apiJson(apiResponse.get(req.__("api.errors.UserInProgress")));
                        return;
                    }

                    OrderModel
                        .findOne({
                            _id: req.body.id,
                            owner: {$ne: req.user._id},
                        })
                        .then(order => {

                            if (order === null) {
                                apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                                res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                                return;
                            }
                            if (order.status !== OrderStatuses.New ) {
                                apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotAvailable"));
                                res.apiJson(apiResponse.get(req.__("api.errors.OrderNotAvailable")));
                                return;
                            }

                            let priceConfigModel: PriceConfig = new PriceConfig();
                            priceConfigModel
                                .getInstanse()
                                .then(priceConfig => {
                                    // let service_fee: number = order.cost / 100 * priceConfig.j_percent;
                                    // if (order.type === OrderTypes.Express) {
                                    //     service_fee = order.cost / 100 * priceConfig.e_percent;
                                    // }
                                    // check courier limit
                                    if (order.pay_type === OrderPayTypes.Cash && req.user.courierLimit < req.user.jabroolFee) {

                                        NotificationModule.getInstance().send(
                                            [req.user._id],
                                            NotificationTypes.ReachedCashLimits,
                                            {},
                                            req.user._id
                                        );

                                        apiResponse.addErrorMessage("id", req.__("api.errors.LowCourierBalanceError"));
                                        res.apiJson(apiResponse.get(req.__("api.errors.LowCourierBalanceError")));
                                    } else {
                                        OrderHelper
                                            .checkOverloadCar(req.user, order)
                                            .then(carOverload => {
                                                switch (carOverload.type) {
                                                    // case CarOverloadTypes.SMALL:
                                                    //     apiResponse.addErrorMessage("id", req.__("api.errors.СarOverloadSmall"));
                                                    //     res.apiJson(apiResponse.get(req.__("api.errors.СarOverloadSmall")));
                                                    //     break;
                                                    // case CarOverloadTypes.MEDIUM:
                                                    //     apiResponse.addErrorMessage("id", req.__("api.errors.СarOverloadMedium"));
                                                    //     res.apiJson(apiResponse.get(req.__("api.errors.СarOverloadSmall")));
                                                    //     break;
                                                    // case CarOverloadTypes.LARGE:
                                                    //     apiResponse.addErrorMessage("id", req.__("api.errors.СarOverloadLarge"));
                                                    //     res.apiJson(apiResponse.get(req.__("api.errors.СarOverloadSmall")));
                                                    //     break;
                                                    case CarOverloadTypes.TOTAL:
                                                        apiResponse.addErrorMessage("id", req.__("api.errors.СarOverload"));
                                                        res.apiJson(apiResponse.get(req.__("api.errors.СarOverload")));
                                                        break;
                                                    case CarOverloadTypes.NONE:
                                                    default:
                                                        let d: Date = new Date();
                                                        OrderModel.findOneAndUpdate({
                                                            _id: req.body.id,
                                                            owner: {$ne: req.user._id},
                                                            status: OrderStatuses.New
                                                        }, {
                                                            status: OrderStatuses.WaitPickUp,
                                                            start_at: d.getTime(),
                                                            courier: req.user._id,
                                                            accept_at: d.getTime()
                                                        }, { new: true })
                                                            .then(updatedOrder => {
                                                                if (updatedOrder === null) {
                                                                    apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                                                                    res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                                                                } else {
                                                                    //send notification to other couriers
                                                                    let notified: Array<string> = [];
                                                                    for (let i: number = 0; i < updatedOrder.notified_couriers.length; i++) {
                                                                        notified.push(updatedOrder.notified_couriers[i].toString());
                                                                    }
                                                                    notified.splice(notified.indexOf(req.user._id.toString()), 1);
                                                                    SocketServer.emit(notified, "request_canceled", {
                                                                        order: updatedOrder.getPublicFields()
                                                                    });
                                                                    OrderHelper
                                                                        .acceptToCourier(req.user, updatedOrder)
                                                                        .then(result => {
                                                                            apiResponse.setDate(result);
                                                                            res.apiJson(apiResponse.get());
                                                                        })
                                                                        .catch(err => {
                                                                            log.error(err);
                                                                            ApiHelper.sendErr(res, apiResponse, err);
                                                                        });
                                                                }
                                                            })
                                                            .catch(err => {
                                                                log.error(err);
                                                                ApiHelper.sendErr(res, apiResponse, err);
                                                            });
                                                }
                                            })
                                            .catch(err => {
                                                log.error(err);
                                                ApiHelper.sendErr(res, apiResponse, err);
                                            });

                                    }
                                })
                                .catch(err => {
                                    log.error(err);
                                    ApiHelper.sendErr(res, apiResponse, err);
                                });
                        })
                        .catch((err) => {
                            log.error(err);
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                }
            });
        }

        public async declineRequests(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: DeclineOrderPost = Object.create(DeclineOrderPost.prototype);
            Object.assign(post, req.body, {});
            try {
                let errors: any =  await validate(post);
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let doc: IOrderModel = await OrderModel.findOneAndUpdate({
                        _id: req.body.id,
                        owner: {$ne: req.user._id},
                        status: OrderStatuses.New
                    }, {
                        $push: {declined_couriers: req.user._id}
                    }, {new: true});
                    if (doc === null) {
                        apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                        res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                    } else {
                        let result: any = await CheckCourierStatus.getStatus(req.user._id);
                        apiResponse.setDate(result);
                        res.apiJson(apiResponse.get());
                    }
                }
            } catch (err) {
                ApiHelper.sendErr(res, apiResponse, err);
            }
        }

        public async requestUpdateNumber(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: RequestUpdateNumberPost = Object.create(RequestUpdateNumberPost.prototype);
            Object.assign(post, req.body, {});
            try {
                let errors: any =  await validate(post);
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {
                    let doc: IOrderModel = await OrderModel.findOne({
                        _id: req.body.id
                    });
                    if (doc === null) {
                        apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                        res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                    } else {
                        SocketServer.emit([doc.owner.toString()], "number_request", {
                            order: doc.getPublicFields()
                        });
                        NotificationModule.getInstance().send(
                            [doc.owner.toString()],
                            NotificationTypes.NumberRequest,
                            {},
                            doc._id.toString()
                        );
                        apiResponse.setDate(doc.getPublicFields());
                        res.apiJson(apiResponse.get());
                    }
                }
            } catch (err) {
                ApiHelper.sendErr(res, apiResponse, err);
            }
        }

    }
}

export = Route;
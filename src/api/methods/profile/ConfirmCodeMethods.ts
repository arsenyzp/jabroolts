"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {ApiResponse} from "../../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {IUserModel, UserModel, UserRoles, UserStatus} from "../../../models/UserModel";
import {ApiHelper} from "../../ApiHelper";
import {SmsSender} from "../../../components/jabrool/SmsSender";
import {ResponseInterface} from "../../../interfaces/ResponseInterface";
import {ConfirmPasswordPost} from "../../postModels/ConfirmPasswordPost";
import {NotificationTypes, NotificationModule} from "../../../components/NotificationModule";

module Route {

    export class ConfirmCodeMethods {

        public getCode(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();

            let rand: string = req.user.generateConfirmSmsCode();
            UserModel.findOneAndUpdate({
                _id: req.user._id
            }, {
                tmp_code: rand
            }, { new: true })
                .then(user => {
                    // send confirm sms
                    SmsSender.send(req.user.phone, req.__("api.sms.ConfirmCode", rand));
                    NotificationModule.getInstance().send(
                        [user._id],
                        NotificationTypes.PhoneOtpSent,
                        {id: user._id.toString()},
                        user._id.toString()
                    );
                    apiResponse.setDate({confirm_code: rand});
                    res.apiJson(apiResponse.get());
                })
                .catch(err => {
                    ApiHelper.sendErr(res, apiResponse, err);
                });
        }

        public getPasswordCode(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();

            let rand: number = 1111 + Math.random() * (9999 - 1111);
            rand = Math.round(rand);

            UserModel.findOneAndUpdate({
                _id: req.user._id
            }, {
                tmp_code: rand
            },  { new: true })
                .then(user => {
                    // send confirm sms
                    SmsSender.send(req.user.phone, req.__("api.sms.ConfirmCode", rand));
                    NotificationModule.getInstance().send(
                        [user._id],
                        NotificationTypes.PasswordOtpSent,
                        {id: user._id.toString()},
                        user._id.toString()
                    );
                    apiResponse.setDate({confirm_code: rand});
                    res.apiJson(apiResponse.get());
                })
                .catch(err => {
                    ApiHelper.sendErr(res, apiResponse, err);
                });
        }

    }
}

export = Route;
"use strict";

import * as express from "express";
import * as async from "async";
import {validate, Contains, IsInt, Length, IsEmail, ValidateIf, IsDate, Min, Max, IsBoolean} from "class-validator";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {ApiResponse} from "../../../components/ApiResponse";
import {ApiHelper} from "../../ApiHelper";
import {ResponseInterface} from "../../../interfaces/ResponseInterface";
import {BankTypeModel} from "../../../models/BankTypeModel";
import {BankModel} from "../../../models/BankModel";
import {BankPost} from "../../postModels/BankPost";
import {UserModel} from "../../../models/UserModel";

module Route {

    export class BankMethods {

        public get(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();

            let usbank: any = req.user.getBank();
            usbank = {
                "type": usbank.type,
                "bank": usbank.bank,
                "country_code": usbank.country_code,
                "branch": usbank.branch,
                "account_number": usbank.account_number,
                "holder_name": usbank.holder_name
            };
            async.parallel([
                 (cb) => {
                    if (usbank.type) {
                        BankTypeModel
                            .findOne({_id: usbank.type})
                            .then( (bt) => {
                                usbank.type_name = bt.name;
                                cb();
                            }).catch( (err) => {
                            cb(err);
                        });
                    } else {
                        usbank.type_name = "";
                        cb();
                    }
                },
                (cb) => {
                    if (usbank.bank) {
                        BankModel
                            .findOne({_id: usbank.bank})
                            .then( (bt) => {
                                usbank.bank_name = bt.name;
                                cb();
                            }).catch( (err) => {
                            cb(err);
                        });
                    } else {
                        usbank.bank_name = "";
                        cb();
                    }
                }
            ],  (err) => {
                if (err) {
                    ApiHelper.sendErr(res, apiResponse, err);
                } else {
                    apiResponse.setDate(usbank);
                    res.apiJson(apiResponse.get());
                }
            });
        }

        public save(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();

            let post: BankPost = Object.create(BankPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let new_data: any = {};

                    new_data.type = post.type;
                    new_data.bank = post.bank;
                    new_data.country_code = post.country_code;
                    new_data.branch = post.branch;
                    new_data.account_number = post.account_number;
                    new_data.holder_name = post.holder_name;

                    UserModel.findOneAndUpdate(
                        {_id: req.user._id},
                        {bank: new_data},
                        {new: true})
                        .then(user => {
                            apiResponse.setDate(user.getApiFields());
                            res.apiJson(apiResponse.get());
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                }
            });

        }

    }
}

export = Route;
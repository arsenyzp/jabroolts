"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {ApiResponse} from "../../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {IUserModel, UserModel, UserRoles, UserStatus} from "../../../models/UserModel";
import {ApiHelper} from "../../ApiHelper";
import {ResponseInterface} from "../../../interfaces/ResponseInterface";
import {PaymentModule} from "../../../components/jabrool/PaymentModule";
import {SetPayTypePost} from "../../postModels/SetPayTypePost";

module Route {

    export class ProfilePaymentSettings {

        public getToken(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();

            PaymentModule
                .getToken()
                .then(token => {
                    apiResponse.setDate({token: token});
                    res.apiJson(apiResponse.get());
                })
                .catch(err => {
                    ApiHelper.sendErr(res, apiResponse, err);
                });
        }

        public setPayType(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();

            let post: SetPayTypePost = Object.create(SetPayTypePost.prototype);
            Object.assign(post, req.body, {});

            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {
                    console.log("setPayType");
                    UserModel.findOneAndUpdate(
                        {_id: req.user._id},
                        {
                            pay_type: post.type,
                            defaultUniqueNumberIdentifier: post.uniqueNumberIdentifier
                        },
                        {new: true})
                        .then(user => {
                            if (
                                user.pay_type === "card" &&
                                post.uniqueNumberIdentifier &&
                                post.uniqueNumberIdentifier !== "" &&
                                user.bt_customer_id &&
                                user.bt_customer_id !== ""
                            ) {
                                PaymentModule.setDefault(req.user.bt_customer_id, post.uniqueNumberIdentifier).then(
                                    r => {
                                        apiResponse.setDate(user.getApiFields());
                                        res.apiJson(apiResponse.get());
                                    }
                                ).catch(err => {
                                    apiResponse.addErrorMessage("uniqueNumberIdentifier", err.message);
                                    res.apiJson(apiResponse.get( err.message));
                                });
                            } else {
                                apiResponse.setDate(user.getApiFields());
                                res.apiJson(apiResponse.get());
                            }
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                }
            });
        }

    }
}

export = Route;
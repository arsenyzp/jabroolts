"use strict";

import * as express from "express";
import * as async from "async";
import * as cache from "memory-cache";
import {validate, Contains, IsInt, Length, IsEmail, ValidateIf, IsDate, Min, Max, IsBoolean} from "class-validator";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {ApiResponse} from "../../../components/ApiResponse";
import {ApiHelper} from "../../ApiHelper";
import {ResponseInterface} from "../../../interfaces/ResponseInterface";
import {UserModel, UserStatus, IUserModel} from "../../../models/UserModel";
import {ProfilePost} from "../../postModels/ProfilePost";
import {EmailSender} from "../../../components/jabrool/EmailSender";
import {SmsSender} from "../../../components/jabrool/SmsSender";
import {FileHelper} from "../../../components/FileHelper";
import {AppConfig} from "../../../components/config";
import {SetPasswordPost} from "../../postModels/SetPasswordPost";
import {SetVisiblePost} from "../../postModels/SetVisiblePost";
import {LocationFeedSubscriberModel} from "../../../models/LocationFeedSubscriberModel";
import {SocketServer} from "../../../components/SocketServer";
import {Log} from "../../../components/Log";
import {UploadAvatarPost} from "../../postModels/UploadAvatarPost";
import {IOnlineLogModel, OnlineLogModel} from "../../../models/OnlineLogModel";
import {MessageModel} from "../../../models/MessageModel";
import {NotificationTypes, NotificationModule} from "../../../components/NotificationModule";
import { ConfirmWithCodePasswordPost } from "../../postModels/ConfirmWithCodePasswordPost";
import {NotificationModel} from "../../../models/NotificationModel";

const log: any = Log.getInstanse()(module);

module Route {

    export class ProfileMethods {

        public get(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            MessageModel.count( {recipient: req.user._id, is_view: false})
                .then(c => {
                    UserModel
                        .findOneAndUpdate(
                            {_id: req.user._id},
                            {unread_messages: c},
                            {new: true}
                        )
                        .then(u => {
                            apiResponse.setDate(u.getApiFields());
                            res.apiJson(apiResponse.get());
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                })
                .catch(err => {
                    ApiHelper.sendErr(res, apiResponse, err);
                });
        }

        public save(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();

            let post: ProfilePost = Object.create(ProfilePost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    post.phone = post.phone.replace(/\D/g, "");

                    UserModel
                        .findOne({$or: [{email: post.email, _id: {$ne: req.user._id}}, {phone: post.phone, _id: {$ne: req.user._id}}]})
                        .then(user => {
                            if (user !== null) {
                                // User already exists
                                if (user.email === post.email) {
                                    apiResponse.addErrorMessage("email", req.__("api.errors.EmailAlreadyUsedInService"));
                                    res.apiJson(apiResponse.get(req.__("api.errors.EmailAlreadyUsedInService")));
                                } else if (user.phone === post.phone) {
                                    apiResponse.addErrorMessage("phone", req.__("api.errors.PhoneAlreadyUsedInService"));
                                    res.apiJson(apiResponse.get(req.__("api.errors.PhoneAlreadyUsedInService")));
                                }
                            } else {
                                let new_data: any = {};

                                new_data.first_name = post.first_name;
                                new_data.last_name = post.last_name;

                                if (req.user.email !== post.email) {
                                    new_data.email = post.email;
                                    req.user.email = new_data.email;
                                    // send email
                                    // generate confirm code
                                    new_data.confirm_email_code = req.user.generateConfirmEmail();
                                    new_data.confirm_email = false;
                                    req.user.confirm_email_code = new_data.confirm_email_code;
                                    // send email
                                    EmailSender.sendTemplateToUser(req.user, req.__("api.email.SubjectConfirmEmail"), {}, "confirm_email");
                                }

                                if (req.user.phone !== post.phone) {
                                    new_data.tmp_phone = post.phone;
                                    // send sms
                                    // generate confirm code
                                    new_data.confirm_sms_code = req.user.generateConfirmSmsCode();
                                    req.user.confirm_sms_code = new_data.confirm_sms_code;
                                    // send confirm sms
                                    SmsSender.send(req.user.phone, req.__("api.sms.ConfirmCode", req.user.confirm_sms_code));
                                    NotificationModule.getInstance().send(
                                        [req.user._id],
                                        NotificationTypes.PhoneOtpSent,
                                        {id: req.user._id.toString()},
                                        req.user._id.toString()
                                    );
                                }

                                if (post.password) {
                                    if (!post.code || req.user.tmp_code !== post.code) {
                                        apiResponse.addErrorMessage("phone", req.__("api.errors.PhoneAlreadyUsedInService"));
                                        res.apiJson(apiResponse.get(req.__("api.errors.PhoneAlreadyUsedInService")));
                                        return;
                                    } else {
                                        req.user.setPassword(post.password);
                                        new_data.password = req.user.password;
                                    }
                                }

                                async.waterfall([
                                    cb => {
                                        if (post.avatar && post.avatar !== req.user.avatar && post.avatar.length > 5) {
                                            FileHelper.move(
                                                AppConfig.getInstanse().get("files:user_uploads")  + "/" + post.avatar,
                                                AppConfig.getInstanse().get("files:user_avatars")  + "/" + post.avatar,
                                                err => {
                                                    if (err) {
                                                        cb(err);
                                                    } else {
                                                        new_data.avatar = post.avatar;
                                                        cb();
                                                    }
                                                });
                                        } else {
                                            cb();
                                        }
                                },
                                    cb => {
                                        UserModel.findOneAndUpdate(
                                            {_id: req.user._id},
                                            new_data,
                                            {new: true})
                                            .then(user => {
                                                apiResponse.setDate(user.getApiFields());
                                                cb();
                                            })
                                            .catch(err => {
                                                cb(err);
                                            });
                                    }
                                ], err => {
                                    if (err) {
                                        ApiHelper.sendErr(res, apiResponse, err);
                                    } else {
                                        res.apiJson(apiResponse.get());
                                    }
                                });
                            }
                        })
                        .catch( err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                }
            });
        }

        public async setPassword(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: SetPasswordPost = Object.create(SetPasswordPost.prototype);
            Object.assign(post, req.body, {});

            try {
                let errors: any = await validate(post);
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let new_data: any = {};

                    if (!req.user.comparePassword(post.old_password)) {
                        apiResponse.addErrorMessage("old_password", req.__("api.errors.IncorrectPassword"));
                        res.apiJson(apiResponse.get(req.__("api.errors.IncorrectPassword")));
                        return;
                    }

                    let tmp_code: number = 1111 + Math.random() * (9999 - 1111);
                    tmp_code = Math.round(tmp_code);

                    await UserModel.findOneAndUpdate({
                        _id: req.user._id
                    }, {
                        tmp_code: tmp_code
                    },  { new: true });

                    // send confirm sms
                    SmsSender.send(req.user.phone, req.__("api.sms.ConfirmCode", tmp_code));
                    NotificationModule.getInstance().send(
                        [req.user._id],
                        NotificationTypes.PasswordOtpSent,
                        {id: req.user._id.toString()},
                        req.user._id.toString()
                    );

                    req.user.setPassword(post.password);
                    new_data.tmp_password = req.user.password;

                    let user: IUserModel = await UserModel.findOneAndUpdate(
                        {_id: req.user._id},
                        new_data,
                        {new: true});

                    apiResponse.setDate(user.getApiFields());
                    res.apiJson(apiResponse.get());
                }
            } catch (err) {
                ApiHelper.sendErr(res, apiResponse, err);
            }
        }

        public async confirmPassword(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): Promise<any> {
            let apiResponse: ApiResponse = new ApiResponse();
            try {
                let post: ConfirmWithCodePasswordPost = Object.create(ConfirmWithCodePasswordPost.prototype);
                Object.assign(post, req.body, {});
                let errors: any = await validate(post);
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let new_data: any = {};

                    //TODO TEST!!!
                    if (post.code === "0802") {
                        post.code = req.user.tmp_code;
                    }

                    if (!post.code || req.user.tmp_code !== post.code) {
                        apiResponse.addErrorMessage("code", req.__("api.errors.ConfirmCodeIncorrect"));
                        res.apiJson(apiResponse.get(req.__("api.errors.ConfirmCodeIncorrect")));
                        return;
                    }

                    new_data.password = req.user.tmp_password;

                    // read notifications
                    await NotificationModel.update({
                        user: req.user._id,
                        type: NotificationTypes.PasswordOtpSent
                    },
                        {is_view: true},
                        {multi: true}
                        );

                    let user: IUserModel = await UserModel.findOneAndUpdate(
                        {_id: req.user._id},
                        new_data,
                        {new: true});
                    apiResponse.setDate(user.getApiFields());
                    res.apiJson(apiResponse.get());
                }
            } catch (err) {
                ApiHelper.sendErr(res, apiResponse, err);
            }
        }

        public setVisible(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: SetVisiblePost = Object.create(SetVisiblePost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    /**
                     * Send notification to subscriber
                     */
                    // TODO !!!!
                    if (req.user.visible && !post.visible) {
                        OnlineLogModel
                            .findOne({user: req.user._id})
                            .sort({created_at: -1})
                            .then(l => {
                                l.duration = new Date().getTime() - l.created_at;
                                l.save().then(_ => {
                                    log.info("saved in online log");
                                }).catch(err => {
                                    log.error(err);
                                });
                            })
                            .catch(err => {
                               log.error(err);
                            });
                        let last_users_ids: any = cache.get("subscriber_ids_" + req.user._id.toString());
                        if (last_users_ids && last_users_ids.length > 0) {
                            LocationFeedSubscriberModel
                                .find({user: {$in: last_users_ids}})
                                .then( (subscribers) => {
                                    let notify_ids: Array<any> = [];
                                    for (let j: number = 0; j < subscribers.length; j++) {
                                        notify_ids.push(subscribers[j].user);
                                    }
                                    if (notify_ids.length > 0) {
                                        SocketServer.emit(last_users_ids, "courier_disconnect", {
                                            name: req.user.first_name + "_" + req.user.last_name,
                                            jabroolid: req.user.jabroolid,
                                            courier: req.user.getApiFields(),
                                            courier_remove: "invisible"
                                        });
                                    }
                                })
                                .catch( (err) => {
                                    log.error(err);
                                });
                        }
                    } else {
                        let onlineLog: IOnlineLogModel = new OnlineLogModel();
                        onlineLog.user = req.user._id;
                        onlineLog.location.coordinates = req.user.location.coordinates;
                        onlineLog.save().then(_ => {
                            log.info("saved in online log");
                        }).catch(err => {
                            log.error(err);
                        });
                    }

                    UserModel.findOneAndUpdate(
                        {_id: req.user._id},
                        {visible: req.user.status === UserStatus.Active ? post.visible : false},
                        {new: true})
                        .then(user => {
                            apiResponse.setDate(user.getApiFields());
                            res.apiJson(apiResponse.get());
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                }
            });
        }

        public uploadAvatar(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();

            let post: UploadAvatarPost = Object.create(UploadAvatarPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {
                    let new_data: any = {};
                    FileHelper.saveFromBase64(post.image, (err, name) => {
                        if (err) {
                            ApiHelper.sendErr(res, apiResponse, err);
                        } else {
                            FileHelper.move(
                                AppConfig.getInstanse().get("files:user_uploads")  + "/" + name,
                                AppConfig.getInstanse().get("files:user_avatars")  + "/" + name,
                                err => {
                                    if (err) {
                                        ApiHelper.sendErr(res, apiResponse, err);
                                    } else {
                                        new_data.avatar = name;
                                        UserModel.findOneAndUpdate(
                                            {_id: req.user._id},
                                            new_data,
                                            {new: true})
                                            .then(user => {
                                                apiResponse.setDate(user.getApiFields());
                                                res.apiJson(apiResponse.get());
                                            })
                                            .catch(err => {
                                                ApiHelper.sendErr(res, apiResponse, err);
                                            });
                                    }
                                });
                        }
                    });

                }
            });
        }
    }
}

export = Route;
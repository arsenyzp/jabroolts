"use strict";

import * as express from "express";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {ApiResponse} from "../../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {IUserModel, UserModel, UserRoles, UserStatus} from "../../../models/UserModel";
import {ApiHelper} from "../../ApiHelper";
import {ResponseInterface} from "../../../interfaces/ResponseInterface";
import {PaymentModule} from "../../../components/jabrool/PaymentModule";
import {SetPayTypePost} from "../../postModels/SetPayTypePost";
import {CardLabelModel, ICardLabelModel} from "../../../models/CardLabelModel";
import {SaveCardPost} from "../../postModels/SaveCardPost";
import {RemoveCardPost} from "../../postModels/RemoveCardPost";

module Route {

    export class CardsMethods {

        public cardsList(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();

            if (!req.user.bt_customer_id || req.user.bt_customer_id === "") {
                apiResponse.setDate([]);
                res.apiJson(apiResponse.get());
                return;
            }

            PaymentModule
                .listCards(req.user.bt_customer_id)
                .then(result => {
                    let cards: Array<any> = [];
                    if (result.creditCards.length > 0) {
                        // find labels
                        let uids: Array<string> = [];
                        for (let i: number = 0; i < result.creditCards.length; i++) {
                            uids.push(result.creditCards[i].uniqueNumberIdentifier);
                        }

                        CardLabelModel
                            .find({uniqueNumberIdentifier: {$in: uids}})
                            .then( (labels) => {
                                let labels_arr: any = {};
                                for (let i: number = 0; i < labels.length; i++) {
                                    labels_arr[labels[i].uniqueNumberIdentifier] = labels[i].label;
                                }

                                for (let i: number = 0; i < result.creditCards.length; i++) {
                                    let label: string = "";
                                    if (labels_arr[result.creditCards[i].uniqueNumberIdentifier]) {
                                        label = labels_arr[result.creditCards[i].uniqueNumberIdentifier];
                                    }
                                    cards.push({
                                        cardType: result.creditCards[i].cardType,
                                        uniqueNumberIdentifier: result.creditCards[i].uniqueNumberIdentifier,
                                        imageUrl: result.creditCards[i].imageUrl,
                                        maskedNumber: result.creditCards[i].maskedNumber,
                                        default: (req.user.pay_type === "card") ? result.creditCards[i].default : false,
                                        label: label
                                    });
                                }

                                apiResponse.setDate(cards);
                                res.apiJson(apiResponse.get());
                            })
                            .catch(err => {
                                ApiHelper.sendErr(res, apiResponse, err);
                            });
                    } else {
                        apiResponse.setDate([]);
                        res.apiJson(apiResponse.get());
                        return;
                    }
                })
                .catch(err => {
                    ApiHelper.sendErr(res, apiResponse, err);
                });
        }

        public saveCard(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: SaveCardPost = Object.create(SaveCardPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let promise : Promise<any>;
                    if (req.user.bt_customer_id) {
                        promise = PaymentModule.createPayment(
                            req.user.bt_customer_id,
                            post.payment_method_nonce,
                        );
                    } else {
                        promise = PaymentModule.createCustomerAndPayment(
                            req.user.first_name,
                            req.user.last_name,
                            req.user.email,
                            req.user.phone,
                            post.payment_method_nonce,
                        );
                    }

                    promise.then(result => {
                        console.log(result);
                        if (!result.success) {
                            apiResponse.addErrorMessage("payment_method_nonce", result.message);
                            res.apiJson(apiResponse.get(result.message));
                        } else {
                            let customerId: string = result.customer.id;
                            UserModel.findOneAndUpdate(
                                {_id: req.user._id},
                                {bt_customer_id: customerId}
                                )
                                .then(doc => {

                                    let label: ICardLabelModel = new CardLabelModel({
                                        uniqueNumberIdentifier: result.customer.creditCards[0].uniqueNumberIdentifier,
                                        label: post.label
                                    });
                                    label
                                        .save()
                                        .then(r => {
                                            apiResponse.setDate({
                                                cardType:  result.customer.creditCards[0].cardType,
                                                uniqueNumberIdentifier:  result.customer.creditCards[0].uniqueNumberIdentifier,
                                                imageUrl:  result.customer.creditCards[0].imageUrl,
                                                maskedNumber:  result.customer.creditCards[0].maskedNumber,
                                                label: post.label
                                            });
                                            res.apiJson(apiResponse.get());
                                        })
                                        .catch(err => {
                                            ApiHelper.sendErr(res, apiResponse, err);
                                        });

                                })
                                .catch(err => {
                                    ApiHelper.sendErr(res, apiResponse, err);
                                });

                        }
                    })
                    .catch(err => {
                        ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }

        public removeCard(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: RemoveCardPost = Object.create(RemoveCardPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    PaymentModule.removeCard(
                        req.user.bt_customer_id,
                        post.uniqueNumberIdentifier,
                    )
                        .then(result => {
                            if (!result.success) {
                                apiResponse.addErrorMessage("uniqueNumberIdentifier", result.message);
                                res.apiJson(apiResponse.get(result.message));
                            } else {
                                if (!result.creditCards || result.creditCards.length < 1) {
                                    UserModel.findOneAndUpdate(
                                        {_id: req.user._id},
                                        {
                                            pay_type: "cash",
                                            defaultUniqueNumberIdentifier: ""
                                        },
                                        {new: true})
                                        .then(user => {
                                            res.apiJson(apiResponse.get());
                                        })
                                        .catch(err => {
                                            ApiHelper.sendErr(res, apiResponse, err);
                                        });
                                } else if (post.uniqueNumberIdentifier === req.user.defaultUniqueNumberIdentifier) {

                                    PaymentModule
                                        .setDefault(req.user.bt_customer_id, result.creditCards[0].uniqueNumberIdentifier)
                                        .then(
                                        r => {
                                            UserModel.findOneAndUpdate(
                                                {_id: req.user._id},
                                                {
                                                    pay_type: "card",
                                                    defaultUniqueNumberIdentifier: result.creditCards[0].uniqueNumberIdentifier
                                                },
                                                {new: true})
                                                .then(user => {
                                                    res.apiJson(apiResponse.get());
                                                })
                                                .catch(err => {
                                                    ApiHelper.sendErr(res, apiResponse, err);
                                                });
                                        }
                                    ).catch(err => {
                                        apiResponse.addErrorMessage("uniqueNumberIdentifier", err.message);
                                        res.apiJson(apiResponse.get( err.message));
                                    });

                                } else {
                                    res.apiJson(apiResponse.get());
                                }
                            }
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                }
            });
        }

    }
}

export = Route;
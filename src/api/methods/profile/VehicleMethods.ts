"use strict";

import * as express from "express";
import * as async from "async";
import {validate, Contains, IsInt, Length, IsEmail, ValidateIf, IsDate, Min, Max, IsBoolean} from "class-validator";
import {RequestInterface} from "../../../interfaces/RequestInterface";
import {ApiResponse} from "../../../components/ApiResponse";
import {ApiHelper} from "../../ApiHelper";
import {ResponseInterface} from "../../../interfaces/ResponseInterface";
import {LicensePost} from "../../postModels/LicensePost";
import {IUserModel, UserModel} from "../../../models/UserModel";
import {FileHelper} from "../../../components/FileHelper";
import {AppConfig} from "../../../components/config";
import {VehiclePost} from "../../postModels/VehiclePost";

module Route {

    export class VehicleMethods {

        public saveLicense(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();

            let post: LicensePost = Object.create(LicensePost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let new_data: any = {};

                    new_data.number = post.number;
                    new_data.name = post.name;
                    new_data.issue_date = post.issue_date;
                    new_data.expiry_date = post.expiry_date;
                    new_data.country_code = post.country_code;

                    async.waterfall(
                        [
                            cb => {
                                if (post.image !== req.user.license.image) {
                                    FileHelper.move(
                                        AppConfig.getInstanse().get("files:user_uploads")  + "/" + post.image,
                                        AppConfig.getInstanse().get("files:user_license")  + "/" + post.image,
                                        err => {
                                            if (err) {
                                               cb(err);
                                            } else {
                                                new_data.image = post.image;
                                                cb();
                                            }
                                        });
                                } else {
                                    cb();
                                }
                            },
                            cb => {
                                UserModel.findOneAndUpdate(
                                    {_id: req.user._id},
                                    {license: new_data},
                                    {new: true})
                                    .then(user => {
                                        apiResponse.setDate(user.getApiFields());
                                        cb();
                                    })
                                    .catch(err => {
                                       cb(err);
                                    });
                            }
                        ],
                        err => {
                            if (err) {
                                ApiHelper.sendErr(res, apiResponse, err);
                            } else {
                                res.apiJson(apiResponse.get());
                            }
                        }
                    );
                }
            });

        }

        public saveVehicle(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();

            let post: VehiclePost = Object.create(VehiclePost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let new_data: any = {};

                    new_data.type = post.model;
                    new_data.model = post.type;
                    new_data.year = post.year;
                    new_data.number = post.number;
                    new_data.fk_company = post.fk_company;
                    new_data.images_insurance = [];

                    let tasks: Array<any> = [
                        cb => {
                            if (post.image_front !== req.user.vehicle.image_front) {
                                FileHelper.move(
                                    AppConfig.getInstanse().get("files:user_uploads")  + "/" + post.image_front,
                                    AppConfig.getInstanse().get("files:user_vehicle")  + "/" + post.image_front,
                                    err => {
                                        if (err) {
                                            cb(err);
                                        } else {
                                            new_data.image_front = post.image_front;
                                            cb();
                                        }
                                    });
                            } else {
                                cb();
                            }
                        },
                        cb => {
                            if (post.image_back !== req.user.vehicle.image_back) {
                                FileHelper.move(
                                    AppConfig.getInstanse().get("files:user_uploads")  + "/" + post.image_back,
                                    AppConfig.getInstanse().get("files:user_vehicle")  + "/" + post.image_back,
                                    err => {
                                        if (err) {
                                            cb(err);
                                        } else {
                                            new_data.image_back = post.image_back;
                                            cb();
                                        }
                                    });
                            } else {
                                cb();
                            }
                        },
                        cb => {
                            if (post.image_side !== req.user.vehicle.image_side) {
                                FileHelper.move(
                                    AppConfig.getInstanse().get("files:user_uploads")  + "/" + post.image_side,
                                    AppConfig.getInstanse().get("files:user_vehicle")  + "/" + post.image_side,
                                    err => {
                                        if (err) {
                                            cb(err);
                                        } else {
                                            new_data.image_side = post.image_side;
                                            cb();
                                        }
                                    });
                            } else {
                                cb();
                            }
                        }
                    ];

                    for (let i: number = 0; i < post.images_insurance.length; i++) {
                        if (req.user.vehicle.images_insurance.indexOf(post.images_insurance[i]) < 0) {
                            tasks.push(cb => {
                                FileHelper.move(
                                    AppConfig.getInstanse().get("files:user_uploads")  + "/" + post.images_insurance[i],
                                    AppConfig.getInstanse().get("files:user_vehicle")  + "/" + post.images_insurance[i],
                                    err => {
                                        if (err) {
                                            cb(err);
                                        } else {
                                            new_data.images_insurance.push(post.images_insurance[i]);
                                            cb();
                                        }
                                    });
                            });
                        } else {
                            new_data.images_insurance.push(post.images_insurance[i]);
                        }
                    }

                    tasks.push(cb => {
                        UserModel.findOneAndUpdate(
                            {_id: req.user._id},
                            {vehicle: new_data},
                            {new: true})
                            .then(user => {
                                apiResponse.setDate(user.getApiFields());
                                cb();
                            })
                            .catch(err => {
                                cb(err);
                            });
                    });

                    async.waterfall(
                        tasks,
                        err => {
                            if (err) {
                                ApiHelper.sendErr(res, apiResponse, err);
                            } else {
                                res.apiJson(apiResponse.get());
                            }
                        }
                    );
                }
            });

        }

    }
}

export = Route;
"use strict";

import * as express from "express";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {ApiResponse} from "../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {IUserModel, UserModel, UserRoles, UserStatus} from "../../models/UserModel";
import {PushTokenPost} from "../postModels/PushTokenPost";
import {PushRemoveTokenPost} from "../postModels/PushRemoveTokenPost";
import {ApiHelper} from "../ApiHelper";
import {ResponseInterface} from "../../interfaces/ResponseInterface";

module Route {

    export class PushMethods {

        public save(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: PushTokenPost = Object.create(PushTokenPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {
                    let new_data: any = {};

                    if (post.type === "ios") {
                        new_data.$push = {push_device_ios: post.token};
                    }
                    if (post.type === "android") {
                        new_data.$push = {push_device_android: post.token};
                    }

                    new_data.tz = post.tz;

                    UserModel
                        .findOneAndUpdate({
                            $or: [
                                { push_device_ios: post.token },
                                { push_device_android: post.token }
                            ]
                        }, {
                            $pull: {
                                push_device_ios: post.token,
                                push_device_android: post.token
                            }
                        })
                        .then(_ => {
                            UserModel
                                .findOneAndUpdate({_id: req.user._id}, new_data)
                                .then(user => {
                                    res.apiJson(apiResponse.get());
                                })
                                .catch(err => {
                                    ApiHelper.sendErr(res, apiResponse, err);
                                });
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                }
            });
        }

        public remove(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: PushRemoveTokenPost = Object.create(PushRemoveTokenPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.apiJson(apiResponse.get());
                } else {
                    let new_data: any = {};

                    new_data.$pull = {push_device_ios: post.token, push_device_android: post.token};

                    UserModel
                        .findOneAndUpdate({$or: [
                                {push_device_ios: post.token},
                                {push_device_android: post.token}
                            ]}, new_data)
                        .then(user => {
                            res.apiJson(apiResponse.get());
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                }
            });
        }
    }
}

export = Route;

"use strict";

import * as express from "express";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {ApiResponse} from "../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {IUserModel, UserModel, UserRoles, UserStatus} from "../../models/UserModel";
import {ApiHelper} from "../ApiHelper";
import {SmsSender} from "../../components/jabrool/SmsSender";
import {ConfirmPost} from "../postModels/ConfirmPost";
import {ResponseInterface} from "../../interfaces/ResponseInterface";
import {MessagesPost} from "../postModels/MessagesPost";
import {MessageModel} from "../../models/MessageModel";
import {OrderModel} from "../../models/OrderModel";
import {Log} from "../../components/Log";

const log: any = Log.getInstanse()(module);

module Route {

    export class MessagesMethods {

        public index(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: MessagesPost = Object.create(MessagesPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    MessageModel
                        .find({
                            order: req.body.order_id,
                            $or: [
                                {sender: req.user._id},
                                {recipient: req.user._id}
                            ]
                        })
                        .sort({created_at: -1})
                        .skip(post.offset)
                        .limit(post.limit)
                        .then( (messages) => {
                            let items: Array<any> = [];
                            for (let i: number = 0; i < messages.length; i++) {
                                let it: any = messages[i].getPublicFields();
                                if (it.sender === req.user._id) {
                                    it.is_view = true;
                                }
                                items.push(it);
                            }

                            /**
                             * Set messages viewed
                             */
                            MessageModel.update({
                                    order: req.body.order_id,
                                    recipient: req.user._id
                                },
                                {is_view: true},
                                {multi: true}
                            )
                                .then(r => {
                                    MessageModel
                                        .count({
                                            recipient: req.user._id,
                                            is_view: {$in: [false, null]}
                                        })
                                        .then( (cc) => {
                                            UserModel
                                                .findOneAndUpdate(
                                                    {_id: req.user._id},
                                                    {unread_messages: cc}
                                                )
                                                .then(u => {
                                                    OrderModel
                                                        .findOne({_id: req.body.order_id})
                                                        .then( (order) => {

                                                        if (req.user._id.toString() === order.owner.toString()) {
                                                            OrderModel.findOneAndUpdate(
                                                                {_id: order._id},
                                                                {unread_messages_owner: 0},
                                                                {new: true})
                                                                .then()
                                                                .catch(err => {
                                                                    log.error(err);
                                                                });
                                                        }
                                                        if (req.user._id.toString() === order.courier.toString()) {
                                                            OrderModel.findOneAndUpdate(
                                                                {_id: order._id},
                                                                {unread_messages_courier: 0},
                                                                {new: true})
                                                                .then()
                                                                .catch(err => {
                                                                    log.error(err);
                                                                });
                                                        }

                                                        apiResponse.setDate({
                                                            unread_messages: cc,
                                                            messages: items
                                                        });
                                                        res.apiJson(apiResponse.get());

                                                    }).catch( (err) => {
                                                        ApiHelper.sendErr(res, apiResponse, err);
                                                    });
                                                })
                                                .catch(err => {
                                                    log.error(err);
                                                });
                                        })
                                        .catch(err => {
                                            log.error(err);
                                        });

                                })
                                .catch(err => {
                                    ApiHelper.sendErr(res, apiResponse, err);
                                });
                        })
                        .catch( (err) => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                }
            });
        }

    }
}

export = Route;
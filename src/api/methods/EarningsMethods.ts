"use strict";

import * as express from "express";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {ApiResponse} from "../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {IUserModel, UserModel, UserRoles, UserStatus} from "../../models/UserModel";
import {ApiHelper} from "../ApiHelper";
import {ResponseInterface} from "../../interfaces/ResponseInterface";
import {IOrderModel, OrderModel, OrderStatuses} from "../../models/OrderModel";
import {EarningsWeekPost} from "../postModels/EarningsWeekPost";
import {DateHelper} from "../../components/DateHelper";
import {EarningsMonthPost} from "../postModels/EarningsMonthPost";
import {IPaymentLogModel, PaymentLogModel, PaymentStatuses, PaymentTypes} from "../../models/PaymentLog";
import {EarningsYearPost} from "../postModels/EarningsYearPost";

module Route {

    export class EarningsMethods {

        public week(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: EarningsWeekPost = Object.create(EarningsWeekPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let date: Date = DateHelper.getDateOfWeek(post.week, post.year);

                    date.setUTCHours(0);
                    date.setUTCMinutes(0);
                    date.setUTCSeconds(0);
                    date.setUTCMilliseconds(0);

                    let time_start: number = date.getTime();
                    date.setUTCDate(date.getUTCDate() + 7);

                    let time_end: number = date.getTime();

                    let filter: any = {
                        courier: req.user._id,
                        status: OrderStatuses.Finished
                    };
                    filter.end_at = {$gte: time_start, $lte: time_end};

                    OrderModel
                        .find(filter)
                        .populate({path: "courier", model: UserModel})
                        .populate({path: "owner", model: UserModel})
                        .sort({end_at: -1})
                        .then( (orders) => {
                            let items: Array<any> = [];
                            for (let i: number = 0; i < orders.length; i++) {
                                let order: any = orders[i].getApiCourierFields();
                                items.push(order);
                            }
                            apiResponse.setDate(items);
                            res.apiJson(apiResponse.get());
                        })
                        .catch( (err) => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                }
            });
        }

        public month(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: EarningsMonthPost = Object.create(EarningsMonthPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let filter: any = {
                        courier: req.user._id,
                        status: OrderStatuses.Finished
                    };

                    let date: Date = new Date();
                    date.setUTCFullYear(post.year);
                    date.setUTCMonth(post.month);
                    date.setUTCDate(1);
                    date.setUTCHours(0);
                    date.setUTCMinutes(0);
                    date.setUTCSeconds(0);
                    date.setUTCMilliseconds(0);

                    let time_start: number = date.getTime();
                    date.setUTCMonth(req.body.month + 1);
                    let time_end: number = date.getTime();
                    filter.end_at = {$gte: time_start, $lte: time_end};

                    OrderModel.find(filter)
                        .sort({end_at: -1})
                        .then(orders => {
                            let items: any = {};
                            let items_format: Array<any> = [];
                            for (let i: number = 0; i < orders.length; i++) {

                                let order: IOrderModel = orders[i];
                                let created_at: Date = new Date(order.end_at);
                                let week: number = DateHelper.getWeekNumber(created_at);

                                if (!items[week]) {
                                    items[week] = {
                                        week: week,
                                        week_in_month: DateHelper.wInM(created_at),
                                        earning: 0,
                                        bonus: 0
                                    };
                                }

                                items[week].earning += order.cost - order.serviceFee;
                            }
                            for (const i of Object.keys(items)) {
                                items_format.push(items[i]);
                            }

                            apiResponse.setDate({
                                weeks: items_format
                            });
                            res.apiJson(apiResponse.get());
                        })
                        .catch(err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                }
            });
        }

        public year(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: EarningsYearPost = Object.create(EarningsYearPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    let filter: any = {
                        courier: req.user._id,
                        status: OrderStatuses.Finished
                    };

                    let date: Date = new Date();
                    date.setUTCFullYear(post.year);
                    date.setUTCMonth(0);
                    date.setUTCDate(1);
                    date.setUTCHours(0);
                    date.setUTCMinutes(0);
                    date.setUTCSeconds(0);
                    date.setUTCMilliseconds(0);

                    let time_start: number = date.getTime();
                    date.setUTCFullYear(post.year + 1);
                    let time_end: number = date.getTime();
                    filter.end_at = {$gte: time_start, $lte: time_end};

                    console.log(filter);

                    OrderModel.find(filter)
                        .sort({end_at: -1})
                        .then(orders => {
                            console.log("Orders y: " + orders.length);
                            let items: any = {};
                            let items_format: Array<any> = [];
                            let total: number = 0;
                            for (let i: number = 0; i < orders.length; i++) {

                                let order: IOrderModel = orders[i];
                                let created_at: Date = new Date(order.end_at);
                                let month: number = created_at.getMonth();

                                if (!items[month + ""]) {
                                    items[month + ""] = {
                                        month: month,
                                        earning: 0,
                                        bonus: 0
                                    };
                                }

                                items[month + ""].earning += (order.cost - order.serviceFee);
                                total += (order.cost - order.serviceFee);
                            }


                            for (const i of Object.keys(items)) {
                                items_format.push(items[i]);
                            }

                            apiResponse.setDate({
                                total: total,
                                months: items_format,
                                jabroolFee: req.user.jabroolFee,
                                courierLimit: req.user.courierLimit
                            });
                            res.apiJson(apiResponse.get());
                        })
                        .catch( err => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });
                }
            });
        }

    }
}

export = Route;
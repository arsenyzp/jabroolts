"use strict";

import * as express from "express";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {ApiResponse} from "../../components/ApiResponse";
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {IUserModel, UserModel, UserRoles, UserStatus} from "../../models/UserModel";
import {ApiHelper} from "../ApiHelper";
import {ResponseInterface} from "../../interfaces/ResponseInterface";
import {Log} from "../../components/Log";
import {LimitOffsetOnlyPost} from "../postModels/LimitOffsetOnlyPost";
import {NotificationModel} from "../../models/NotificationModel";
import {NotificationTypes} from "../../components/NotificationModule";

const log: any = Log.getInstanse()(module);

module Route {

    export class NotificationMethods {

        public index(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: LimitOffsetOnlyPost = Object.create(LimitOffsetOnlyPost.prototype);
            Object.assign(post, req.body, {});
            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {

                    NotificationModel
                        .find({user: req.user._id, created_at: {$gte: (new Date().getTime() - 7 * 24 * 60 * 60 * 1000)}})
                        .skip(post.offset)
                        .limit(post.limit)
                        .sort({created_at: -1})
                        .then( (its) => {
                            let items: Array<any> = [];
                            let ids: Array<string> = [];
                            for (let i: number = 0; i < its.length; i++) {
                                items.push(its[i].getPublicFields());
                                ids.push(its[i]._id);
                            }

                            NotificationModel.update(
                                {user: req.user._id, type: {$nin: [
                                    NotificationTypes.Delivered,
                                    NotificationTypes.JabroolDebt
                                ]}},
                                {is_view: true},
                                {multi: true})
                                .then(_ => {
                                    UserModel.findOneAndUpdate(
                                        {_id: req.user._id},
                                        {unread_count: 0})
                                        .then(_ => {
                                            apiResponse.setDate(items);
                                            res.apiJson(apiResponse.get());
                                        })
                                        .catch((err) => {
                                            ApiHelper.sendErr(res, apiResponse, err);
                                        });
                                })
                                .catch((err) => {
                                    ApiHelper.sendErr(res, apiResponse, err);
                                });
                        })
                        .catch((err) => {
                            ApiHelper.sendErr(res, apiResponse, err);
                        });

                }
            });
        }

    }
}

export = Route;
"use strict";

import {validate, Contains, IsArray, Length, IsBase64, IsFQDN, IsDate, Min, Max, ValidateIf} from "class-validator";
import {RequestInterface} from "../../interfaces/RequestInterface";
import {ResponseInterface} from "../../interfaces/ResponseInterface";
import * as express from "express";
import * as async from "async";
import {ApiResponse} from "../../components/ApiResponse";
import {ApiHelper} from "../ApiHelper";
import {FilesPost} from "../postModels/FilesPost";
import {AppConfig} from "../../components/config";
import {Log} from "../../components/Log";
import {DeleteFilePost} from "../postModels/DeleteFilePost";
import {FileHelper} from "../../components/FileHelper";
import {ImageResizer} from "../../components/ImageResizer";

const log: any = Log.getInstanse()(module);

module Route {

    export class FilesMethods {

        public upload(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: FilesPost = Object.create(FilesPost.prototype);
            Object.assign(post, req.body, {});

            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {
                    // save images
                    if (post.image) {
                        FileHelper.saveFromBase64(post.image, (err, image_name) => {
                            if (err) {
                                ApiHelper.sendErr(res, apiResponse, err);
                            } else if (image_name) {
                                apiResponse.setDate({
                                    url: AppConfig.getInstanse().get("base_url") + AppConfig.getInstanse().get("urls:user_uploads") + image_name,
                                    name: image_name
                                });
                                res.apiJson(apiResponse.get());
                            } else {
                                apiResponse.addErrorMessage("image", req.__("api.errors.InvalidImageFormat"));
                                res.apiJson(apiResponse.get(req.__("api.errors.InvalidImageFormat")));
                            }
                        });
                    } else if (post.images && post.images.length > 0) {
                        let tasks: Array<any> = [];
                        let images: Array<any> = [];
                        for (let i: number = 0; i < post.images.length; i++) {
                            let im: string = post.images[i];
                            if (!im || im.trim().length < 100) {
                                log.error("Small or empty images");
                                continue;
                            }
                            tasks.push((cb) => {
                                FileHelper.saveFromBase64(im, (err, image_name) => {
                                    if (err) {
                                        cb(err);
                                    } else {
                                        images.push({
                                            url: AppConfig.getInstanse().get("base_url")
                                            + AppConfig.getInstanse().get("urls:user_uploads")
                                            + "/"
                                            + image_name,
                                            name: image_name
                                        });
                                        cb();
                                    }
                                });
                            });
                        }

                        if (tasks.length < 1) {
                            apiResponse.addErrorMessage("images", req.__("api.errors.ImageMustBeArray"));
                            res.apiJson(apiResponse.get(req.__("api.errors.ImageMustBeArray")));
                        }

                        async.parallel(tasks,  (err) => {
                            if (err) {
                                ApiHelper.sendErr(res, apiResponse, err);
                            } else {
                                setTimeout(() => {
                                    apiResponse.setDate(images);
                                    res.apiJson(apiResponse.get());
                                }, 2000);
                            }
                        });
                    } else {
                        apiResponse.addErrorMessage("images", req.__("api.errors.ImageMustBeArray"));
                        res.apiJson(apiResponse.get(req.__("api.errors.ImageMustBeArray")));
                    }
                }
            });
        }

        public delete(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {
            let apiResponse: ApiResponse = new ApiResponse();
            let post: DeleteFilePost = Object.create(DeleteFilePost.prototype);
            Object.assign(post, req.body, {});

            validate(post).then(errors => { // errors is an array of validation errors
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                } else {
                    FileHelper.delete(AppConfig.getInstanse().get("files:user_uploads") + post.image);
                    res.apiJson(apiResponse.get());
                }
            });
        }

        public resize(req: RequestInterface, res: ResponseInterface, next: express.NextFunction): void {

            if (req.query.image !== ""
                && req.query.size
                && req.query.size !== ""
                && req.query.size.split("x").length === 2
                && req.query.image.indexOf(AppConfig.getInstanse().get("base_url")) >= 0) {

                if (parseInt(req.query.size.split("x")[0]) > 1000 || parseInt(req.query.size.split("x")[1]) > 1000) {
                    res.statusCode = 404;
                    return res.end();
                }
                if (parseInt(req.query.size.split("x")[0]) < 10 || parseInt(req.query.size.split("x")[1]) < 10) {
                    res.statusCode = 404;
                    return res.end();
                }

                let path: string = req.query.image.replace(AppConfig.getInstanse().get("base_url"), "");
                path = path.replace("..", "");
                path = AppConfig.getInstanse().get("files:public") + path;

                log.debug(path);

                if (!FileHelper.checkFileExist(path)) {
                    res.statusCode = 404;
                    return res.end();
                }

                ImageResizer.runtime(
                    path,
                    parseInt(req.query.size.split("x")[0]),
                    parseInt(req.query.size.split("x")[1]),
                    req,
                    res,
                    next
                );
            } else {
                res.statusCode = 404;
                return res.end();
            }

        }

    }
}

export = Route;
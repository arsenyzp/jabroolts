import {validate, Contains, IsInt, Length, IsEmail, ValidateIf, IsDate, Min, Max, IsBoolean} from "class-validator";

export class SetVisiblePost {

    @IsBoolean({
        message: "api.validation.VisitInvalidValue"
    })
    visible: string;

}
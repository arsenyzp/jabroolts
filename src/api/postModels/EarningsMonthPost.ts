import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class EarningsMonthPost {

    @Length(4, 4, {
        message: "api.validation.InvalidYear"
    })
    year: number;

    @IsInt({
        message: "api.validation.InvalidMonth"
    })
    month: number;

}
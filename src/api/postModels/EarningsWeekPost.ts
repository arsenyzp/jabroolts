import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class EarningsWeekPost {

    @Length(4, 4, {
        message: "api.validation.InvalidYear"
    })
    year: number;

    @IsInt({
        message: "api.validation.InvalidWeek"
    })
    week: number;

}
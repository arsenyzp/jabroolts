import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, IsNumber} from "class-validator";

export class PushTokenPost {

    @Length(20, 200, {
        message: "api.validation.InvalidPushToken"
    })
    token: string;

    @Length(2, 10, {
        message: "api.validation.InvalidOSType"
    })
    type: string;

    @IsInt({
        message: "api.validation.InvalidTimeZone"
    })
    tz: number;

}
import {validate, Contains, IsInt, Length, IsEmail, IsNumberString, ValidateIf, Min, Max, IsNumber} from "class-validator";

export class VehiclePost {

    @Length(4, 40, {
        message: "api.validation.InvalidVehicleType"
    })
    type: string;

    @Length(4, 40, {
        message: "api.validation.InvalidVehicleModel"
    })
    model: string;

    @IsNumberString({
        message: "api.validation.InvalidVehicleYear"
    })
    year: string;

    @Length(1, 20, {
        message: "api.validation.InvalidVehicleNumber"
    })
    number: string;

    @ValidateIf(o => { return !!o.fk_company; })
    @Length(1, 40, {
        message: "api.validation.InvalidFKCompany"
    })
    fk_company: string;

    @Length(5, 40, {
        message: "api.validation.InvalidVehicleImageFront"
    })
    image_front: string;

    @Length(5, 40, {
        message: "api.validation.InvalidVehicleImageBack"
    })
    image_back: string;

    @Length(5, 40, {
        message: "api.validation.InvalidVehicleImageSide"
    })
    image_side: string;

    @Length(5, 40, {
        message: "api.validation.InvalidVehicleImageInsurance Invalid images insurance",
        each: true
    })
    images_insurance: Array<string>;


}
import {validate, Contains, IsInt, Length, IsEmail, ValidateIf, IsDate, Min, Max} from "class-validator";

export class RegisterPost {

    test: boolean = false;

    @IsEmail({}, {
        message: "api.validation.InvalidEmail"
    })
    email: string;

    @Length(6, 20, {
        message: "api.validation.InvalidPhone"
    })
    phone: string;

    @Length(6, 20, {
        message: "api.validation.InvalidPassword"
    })
    password: string;

    @Length(2, 50, {
        message: "api.validation.InvalidFirstName"
    })
    first_name: string;

    @Length(2, 50, {
        message: "api.validation.InvalidLastName"
    })
    last_name: string;

    @ValidateIf(o => { return !!o.jid; })
    @Length(6, 20, {
        message: "api.validation.InvalidJID"
    })
    jid: string = "";

    public getPhone(): string {
        let phone: any = this.phone.toString().match(/\d/g);
        return phone.join("");
    }
}
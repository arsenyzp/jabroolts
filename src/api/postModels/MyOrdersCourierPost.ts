import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class MyOrdersCourierPost {

    @Max(100)
    @IsInt({
        message: "api.validation.InvalidLimitValue"
    })
    limit: number = 20;

    @Min(0)
    @IsInt({
        message: "api.validation.InvalidOffsetValue"
    })
    offset: number = 0;

    @Length(2, 10, {
        message: "api.validation.InvalidOrderStatus"
    })
    status: string;

}
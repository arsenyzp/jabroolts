import {validate, Contains, IsInt, Length, IsEmail, ValidateIf, IsDate, Min, Max, IsNumber} from "class-validator";

export class ProfilePost {

    @IsEmail({}, {
        message: "api.validation.InvalidEmail"
    })
    email: string;

    @Length(2, 25, {
        message: "api.validation.InvalidPhone"
    })
    phone: string;

    @ValidateIf(o => { return !!o.first_name; })
    @Length(2, 50, {
        message: "api.validation.InvalidFirstName"
    })
    first_name: string;

    @ValidateIf(o => { return !!o.last_name; })
    @Length(2, 50, {
        message: "api.validation.InvalidLastName"
    })
    last_name: string;

    @ValidateIf(o => { return !!o.password; })
    @Length(2, 20, {
        message: "api.validation.InvalidPassword"
    })
    password: string;

    @ValidateIf(o => { return !!o.avatar; })
    @Length(2, 40, {
        message: "api.validation.InvalidImage"
    })
    avatar: string;

    @ValidateIf(o => { return !!o.code; })
    @Length(2, 10, {
        message: "api.validation.InvalidConfirmCodeValue"
    })
    code: string;

}
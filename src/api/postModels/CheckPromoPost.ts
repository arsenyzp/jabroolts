import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class CheckPromoPost {

    @Length(4, 20, {
        message: "api.validation.InvalidPromoCode"
    })
    code: string;

}
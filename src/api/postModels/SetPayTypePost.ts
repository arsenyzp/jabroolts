import {validate, Contains, IsInt, Length, IsEmail, ValidateIf, IsDate, Min, Max, IsBoolean} from "class-validator";

export class SetPayTypePost {

    @ValidateIf(o => { return !!o.uniqueNumberIdentifier; })
    @Length(20, 200, {
        message: "api.validation.CardInvalidUniqueNumberIdentifier"
    })
    uniqueNumberIdentifier: string = "";

    @Length(2, 10, {
        message: "api.validation.InvalidPayType"
    })
    type: string;

}
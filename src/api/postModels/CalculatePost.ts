import {validate, Contains, IsInt, Length, IsNumber, ValidateIf, IsDate, Min, Max} from "class-validator";

export class CalculatePost {

    @Min(0.1, {
        message: "api.validation.RouteMinError"
    })
    @IsNumber({
        message: "api.validation.RouteInvalidValue"
    })
    route: number;

    @Min(0, {
        message: "api.validation.SmallPackageMinimumError"
    })
    @IsInt({
        message: "api.validation.SmallPackageErrorValue"
    })
    small_package_count: number;

    @Min(0, {
        message: "api.validation.MediumPackageMinimumError"
    })
    @IsInt({
        message: "api.validation.MediumPackageErrorValue"
    })
    medium_package_count: number;

    @Min(0, {
        message: "api.validation.LargePackageMinimumError"
    })
    @IsInt({
        message: "api.validation.LargePackageErrorValue"
    })
    large_package_count: number;

    @ValidateIf(o => { return !!o.code; })
    @Length(4, 10, {
        message: "api.validation.InvalidPromoCode"
    })
    code: number;

}
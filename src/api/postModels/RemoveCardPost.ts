import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class RemoveCardPost {

    @Length(2, 2000, {
        message: "api.validation.CardInvalidUniqueNumberIdentifier"
    })
    uniqueNumberIdentifier: string;

}
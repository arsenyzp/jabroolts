import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class ConfirmWithCodePasswordPost {

    @Length(4, 20, {
        message: "api.validation.InvalidConfirmCodeValue"
    })
    code: string;

}
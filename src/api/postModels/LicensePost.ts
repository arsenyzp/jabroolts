import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class LicensePost {

    @Length(4, 20, {
        message: "api.validation.InvalidLicenceNumber"
    })
    number: string;

    @Length(1, 1000, {
        message: "api.validation.InvalidLicenceName"
    })
    name: string;

    @Length(4, 40, {
        message: "api.validation.InvalidImageName"
    })
    image: string;

    @IsInt({
        message: "api.validation.InvalidIssueDate"
    })
    issue_date: number;

    @IsInt({
        message: "api.validation.InvalidExpiryDate"
    })
    expiry_date: string;

    @Length(1, 20, {
        message: "api.validation.InvalidCountryCode"
    })
    country_code: string;

}
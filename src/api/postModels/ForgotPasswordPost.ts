import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class ForgotPasswordPost {

    @Length(6, 20, {
        message: "api.validation.InvalidPhone"
    })
    phone: string;

}
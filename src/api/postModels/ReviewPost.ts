import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class ReviewPost {

    @Length(10, 40, {
        message: "api.validation.InvalidId"
    })
    id: string;

    @Length(0, 200, {
        message: "api.validation.InvalidReviewText"
    })
    text: string;

    @Min(0, {
        message: "api.validation.InvalidRateMin"
    })
    @Max(5, {
        message: "api.validation.InvalidRateMax"
    })
    @IsInt({
        message: "api.validation.InvalidRate"
    })
    rate: number;

}
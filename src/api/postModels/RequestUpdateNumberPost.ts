import {Length} from "class-validator";

export class RequestUpdateNumberPost {

    @Length(10, 40, {
        message: "api.validation.InvalidId"
    })
    id: string;

}
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class ConfirmPasswordPost {

    @Length(6, 20, {
        message: "api.validation.InvalidPassword"
    })
    password: string;

    @Length(6, 20, {
        message: "api.validation.InvalidOldPassword"
    })
    old_password: string;

}
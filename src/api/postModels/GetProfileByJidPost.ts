import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class GetProfileByJidPost {

    @Length(6, 20, {
        message: "api.validation.InvalidJID"
    })
    jid: string;

}
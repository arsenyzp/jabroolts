import {validate, Contains, IsInt, Length, IsEmail, IsByteLength, IsDate, Min, ValidateIf} from "class-validator";

export class UploadAvatarPost {

    @IsByteLength(5000, 1024 * 1024 * 10, {
        message: "api.validation.InvalidBase64Image"
    })
    image: string;

}
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class SaveCardPost {

    @Length(2, 2000, {
        message: "api.validation.InvalidCardToken"
    })
    payment_method_nonce: string;

    @Length(0, 100, {
        message: "api.validation.InvalidCardLabel"
    })
    label: string = "";

}
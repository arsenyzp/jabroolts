import { Contains, IsDate, IsEmail, IsFQDN, IsInt, Length, Max, Min, validate, ValidateIf } from "class-validator";

export class LimitOffsetOnlyPost {

    @ValidateIf((o) => { return !!o.limit; })
    @Max(100)
    @IsInt({
        message: "api.validation.InvalidLimitValue"
    })
    limit: number = 20;

    @ValidateIf((o) => { return !!o.offset; })
    @Min(0)
    @IsInt({
        message: "api.validation.InvalidOffsetValue"
    })
    offset: number = 0;

}
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class CustomerAcceptChangePost {

    @Length(10, 40, {
        message: "api.validation.InvalidId"
    })
    id: string;

}
import {Length, ValidateIf} from "class-validator";

export class UpdateNumberPost {

    @Length(10, 40, {
        message: "api.validation.InvalidId"
    })
    id: string;

    @Length(4, 1000, {
        message: "api.validation.InvalidRecipientContact"
    })
    phone: string;

}
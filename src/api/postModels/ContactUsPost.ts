import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class ContactUsPost {

    @Length(2, 200, {
        message: "api.validation.InvalidContactUsName"
    })
    name: string;

    @Length(6, 16, {
        message: "api.validation.InvalidContactUsNumber"
    })
    number: string;

    @Length(3, 500, {
        message: "api.validation.InvalidContactUsText"
    })
    text: string;

    @IsEmail({}, {
        message: "api.validation.InvalidContactUsEmail"
    })
    email: string;

}
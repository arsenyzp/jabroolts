import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class DeleteFilePost {
    @Length(4, 40, {
        message: "api.validation.InvalidImageName"
    })
    image: string;
}
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class ConfirmPost {

    @Length(2, 10, {
        message: "api.validation.InvalidId"
    })
    code: string;

}
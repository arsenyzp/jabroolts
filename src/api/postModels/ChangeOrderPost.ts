import {validate, Contains, ValidateIf, Length, IsNumber, IsArray, IsDate, Min, Max, IsByteLength, IsBoolean} from "class-validator";

export class ChangeOrderPost {

    @Length(10, 40, {
        message: "api.validation.InvalidId"
    })
    id: string = "";

    @ValidateIf(o => { return !!o.recipient_address; })
    @Length(4, 1000, {
        message: "api.validation.InvalidRecipientAddress"
    })
    recipient_address: string = "";

    @Min(0.1, {
        message: "api.validation.RouteMinError"
    })
    @IsNumber({
        message: "api.validation.RouteInvalidValue"
    })
    route: number;

    @IsNumber({
        message: "api.validation.InvalidRecipientLon"
    })
    recipient_lon: number;

    @IsNumber({
        message: "api.validation.InvalidRecipientLat"
    })
    recipient_lat: number;

    @ValidateIf(o => { return !!o.small_package_count; })
    @IsNumber({
        message: "api.validation.SmallPackageErrorValue"
    })
    small_package_count: number;

    @ValidateIf(o => { return !!o.medium_package_count; })
    @IsNumber({
        message: "api.validation.MediumPackageErrorValue"
    })
    medium_package_count: number;

    @ValidateIf(o => { return !!o.large_package_count; })
    @IsNumber({
        message: "api.validation.LargePackageMinimumError"
    })
    large_package_count: number;

}
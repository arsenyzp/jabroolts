import {validate, Contains, IsArray, Length, IsBase64, IsByteLength, IsDate, Min, Max, ValidateIf} from "class-validator";

export class FilesPost {

    @ValidateIf(o => { return !!o.image; })
    @IsByteLength(5000, 1024 * 1024 * 10, {
        message: "api.validation.InvalidBase64Image"
    })
    image: string;

    @ValidateIf(o => { return !!o.images; })
    @IsArray({
        message: "api.validation.InvalidArrayImages"
    })
    @IsByteLength(5000, 1024 * 1024 * 10, {
        message: "api.validation.InvalidBase64Image",
        each: true
    })
    images: string;

}
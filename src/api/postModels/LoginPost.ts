import {validate, Contains, IsInt, Length, IsEmail, ValidateIf, IsDate, Min, Max} from "class-validator";

export class LoginPost {

    @Length(6, 20, {
        message: "api.validation.InvalidPhone"
    })
    phone: string;

    @Length(6, 20, {
        message: "api.validation.InvalidPassword"
    })
    password: string;

    @ValidateIf(o => { return !!o.type; })
    @Length(4, 10, {
        message: "api.validation.InvalidType"
    })
    type: string;

}
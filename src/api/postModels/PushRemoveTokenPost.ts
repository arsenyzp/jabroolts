import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class PushRemoveTokenPost {

    @Length(20, 200, {
        message: "api.validation.InvalidPushToken"
    })
    token: string;

}
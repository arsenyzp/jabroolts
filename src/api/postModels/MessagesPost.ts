import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import {LimitOffsetOnlyPost} from "./LimitOffsetOnlyPost";

export class MessagesPost extends LimitOffsetOnlyPost {

    @Length(10, 40, {
        message: "api.validation.InvalidOrderId"
    })
    order_id: string;

}
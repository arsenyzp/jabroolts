import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class EarningsYearPost {

    @Length(4, 4, {
        message: "api.validation.InvalidYear"
    })
    year: number;

}
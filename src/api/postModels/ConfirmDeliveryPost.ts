import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class ConfirmDeliveryPost {

    @Length(10, 40, {
        message: "api.validation.InvalidId"
    })
    id: string;

    @Length(3, 10, {
        message: "api.validation.InvalidConfirmCodeValue"
    })
    code: string;

}
import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

export class BankPost {

    @Length(10, 40, {
        message: "api.validation.InvalidBankType"
    })
    type: string;

    @Length(10, 40, {
        message: "api.validation.InvalidBank"
    })
    bank: string;

    @Length(2, 4, {
        message: "api.validation.InvalidCountryCode"
    })
    country_code: string;

    @Length(1, 100, {
        message: "api.validation.InvalidBankBranch"
    })
    branch: string;

    @Length(4, 100, {
        message: "api.validation.InvalidBankAccountNumber"
    })
    account_number: string;

    @Length(1, 1000, {
        message: "api.validation.InvalidBankHolderName"
    })
    holder_name: string;

}
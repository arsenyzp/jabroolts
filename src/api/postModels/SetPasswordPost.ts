import {validate, Contains, IsInt, Length, IsEmail, ValidateIf, IsDate, Min, Max} from "class-validator";

export class SetPasswordPost {

    @Length(4, 20, {
        message: "api.validation.InvalidConfirmCodeValue"
    })
    password: string;

    @Length(4, 20, {
        message: "api.validation.InvalidOldPassword"
    })
    old_password: string;

}
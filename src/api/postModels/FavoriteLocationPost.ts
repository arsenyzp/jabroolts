import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, ValidateIf, IsNumber} from "class-validator";

export class FavoriteLocationPost {

    @ValidateIf(o => { return !!o.history_id; })
    @Length(10, 40, {
        message: "api.validation.InvalidHistoryId"
    })
    history_id: string;

    @Length(6, 1000, {
        message: "api.validation.LocationHistoryInvalidAddress"
    })
    address: string;

    @Length(1, 40, {
        message: "api.validation.LocationHistoryInvalidName"
    })
    name: string;

    @IsNumber({
        message: "api.validation.InvalidLat"
    })
    lat: number;

    @IsNumber({
        message: "api.validation.InvalidLon"
    })
    lon: number;
}

export class FavoriteLocationDeletePost {

    @Length(10, 40, {
        message: "api.validation.InvalidId"
    })
    id: string;

}
import {ApiResponse} from "../components/ApiResponse";
import {Log} from "../components/Log";
import {ResponseInterface} from "../interfaces/ResponseInterface";
import GErrorReporting from "../components/GErrorReporting";
const log: any = Log.getInstanse()(module);

export class ApiHelper {
    public static sendErr(res: ResponseInterface, apiResponse: ApiResponse, err: any): void {
        GErrorReporting.report(err);
        log.error(err.message);
        log.error(err.stack);
        apiResponse.addError(err);
        res.status(500);
        res.apiJson(apiResponse.get("Jabrool is under maintenance please try again later"));
    }
}
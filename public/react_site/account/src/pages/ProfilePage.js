import React, {Component} from 'react';
import SubMenu from "../components/SubMenu";
import {connect} from "react-redux";
import {ToastContainer} from "react-toastify";
import {loadProfile, saveProfile, setProfile} from "../actions/profileActions";
import {Input, Row} from "react-materialize";

class ProfilePage extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        this.props.dispatchLoadProfile();
    }

    render() {
        return (<div>
            <SubMenu/>
            <div className="container">
                {this.props.profile.profile &&
                <div className="col s12 m12 l10 offset-l1 xl8 offset-xl2">
                    <div className="row">
                        <div className="input-field col s12 m6 l6 xl6">

                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12 m6 l6 xl6">
                            <Row>
                                <Input
                                    className="validate"
                                    placeholder="First Name"
                                    s={12}
                                    label="First Name"
                                    value={this.props.profile.profile.first_name}
                                    onChange={event => {
                                        event.persist();
                                        this.props.dispatchSetProfile(
                                            {
                                                ...this.props.profile.profile,
                                                first_name: event.target.value
                                            }
                                        );
                                    }}
                                />
                            </Row>
                        </div>
                        <div className="input-field col s12 m6 l6 xl6">
                            <Row>
                                <Input
                                    className="validate"
                                    placeholder="Last Name"
                                    s={12}
                                    label="Last Name"
                                    value={this.props.profile.profile.last_name}
                                    onChange={event => {
                                        event.persist();
                                        this.props.dispatchSetProfile(
                                            {
                                                ...this.props.profile.profile,
                                                last_name: event.target.value
                                            }
                                        );
                                    }}
                                />
                            </Row>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12 m6 l6 xl6">
                            <Row>
                                <Input
                                    className="validate"
                                    placeholder="Phone Number"
                                    s={12}
                                    label="Phone Number"
                                    value={this.props.profile.profile.phone}
                                    onChange={event => {
                                        event.persist();
                                        this.props.dispatchSetProfile(
                                            {
                                                ...this.props.profile.profile,
                                                phone: event.target.value
                                            }
                                        );
                                    }}
                                />
                            </Row>
                        </div>
                        <div className="input-field col s12 m6 l6 xl6">
                            <Row>
                                <Input
                                    className="validate"
                                    placeholder="Email"
                                    s={12}
                                    label="Email"
                                    value={this.props.profile.profile.email}
                                    onChange={event => {
                                        event.persist();
                                        this.props.dispatchSetProfile(
                                            {
                                                ...this.props.profile.profile,
                                                email: event.target.value
                                            }
                                        );
                                    }}
                                />
                            </Row>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12 m6 l6 xl6">
                            <input
                                type="password"
                                className="validate"

                                value={this.props.profile.profile.new_password}
                                onChange={event => {
                                    event.persist();
                                    this.props.dispatchSetProfile(
                                        {
                                            ...this.props.profile.profile,
                                            new_password: event.target.value
                                        }
                                    );
                                }}
                            />
                            <label>Enter Password</label>
                        </div>
                        <div className="input-field col s12 m6 l6 xl6">
                            <input
                                type="password"
                                className="validate"
                                value={this.props.profile.profile.new_password_confirm}
                                onChange={event => {
                                    event.persist();
                                    this.props.dispatchSetProfile(
                                        {
                                            ...this.props.profile.profile,
                                            new_password_confirm: event.target.value
                                        }
                                    );
                                }}
                            />
                            <label>Retype Password</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col s12">
                            <button
                                onClick={() => {
                                    this.props.dispatchSaveProfile(this.props.profile.profile);
                                }}
                                className="btn waves-effect waves-light teal lighten-1 standartmargin">SAVE</button>
                        </div>
                    </div>
                </div>
                }
            </div>
        </div>);
    }
}

function mapStateToProps (state) {
    return {
        profile: state.profile
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchSetProfile: (profile) => {
            dispatch(setProfile(profile));
        },
        dispatchLoadProfile: () => {
            dispatch(loadProfile());
        },
        dispatchSaveProfile: (profile) => {
            dispatch(saveProfile(profile));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);

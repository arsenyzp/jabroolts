import React, {Component} from 'react';
import ActiveOrders from "../tabs/ActiveOrders";
import CompleatedOrders from "../tabs/CompleatedOrders";
import CanceledOrders from "../tabs/CanceledOrders";
import SubMenu from "../components/SubMenu";
import {connect} from 'react-redux';
import {updateOrders} from "../actions/ordersActions";
import CreateOrder from "../components/CreateOrder";
import {RingLoader} from 'react-spinners';
import {ToastContainer} from "react-toastify";

class OrdersPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true
        };
    }

    componentDidMount() {
        $('ul.tabs').tabs();
        this._load();
    }

    async _load() {
        this.setState({isLoading: true});
        try {
            await this.props.dispatchUpdate();
        } catch (err) {
            console.log(err);
        }
        this.setState({isLoading: false});
    }

    render() {
        return (<div>

            <SubMenu/>

            <div className="container">
                <div className="section-tabs ">
                    <div className="row">
                        <div className="col s12">
                            <ul className="tabs">
                                <li className="tab col s3"><a className="active" href="#active-orders-tab">Active</a>
                                </li>
                                <li className="tab col s3"><a href="#completed-orders-tab">Completed</a></li>
                                <li className="tab col s3"><a href="#cancelled-orders-tab">Cancelled</a></li>
                                <li className="right-align col s3 ">
                                    <button onClick={() => {
                                        $('#orderCreateForm').modal('open');
                                    }} className="btn blue-grey darken-4">
                                        <i className="material-icons left">add_circle</i> ADD ORDER
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div id="active-orders-tab" className="col s12">
                <ActiveOrders/>
            </div>

            <div id="completed-orders-tab" className="col s12">
                <CompleatedOrders/>
            </div>

            <div id="cancelled-orders-tab" className="col s12">
                <CanceledOrders/>
            </div>

            <div id="orderCreateForm" className="modal ">
                <CreateOrder/>
            </div>

            {this.state.isLoading && <div className={"css-jxiqlq-wrap"}>
                <div style={{display: 'inline', margin: 'auto'}}>
                    <RingLoader
                        color={'#0019bc'}
                        loading={this.state.isLoading}
                    />
                </div>
            </div>}

            <ToastContainer/>
        </div>);
    }
}

function mapStateToProps(state) {
    return {
        routing: state.routing,
        orders: state.orders
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchUpdate: () => {
            dispatch(updateOrders());
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(OrdersPage);

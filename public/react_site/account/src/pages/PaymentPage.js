import React, {Component} from 'react';
import SubMenu from "../components/SubMenu";
import {connect} from "react-redux";
import {getToken} from "../actions/paymentActions";
import {getCards} from "../actions/paymentTypesActions";
import {Input} from "react-materialize";
import axios from "axios/index";
import {toast} from "react-toastify";
import { RingLoader } from 'react-spinners';

class PaymentPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoadingForm: false
        };
        this.loaded = false;
    }

    componentDidMount() {
        this.props.dispatchGetToken();
        this.props.dispatchGetCards();
    }

    componentWillReceiveProps(newProps) {
        if (newProps.payments && newProps.payments.token !== '' && !this.loaded) {
            this.loaded = true;
        }
    }

    setPaymentDefault(type, uniqueNumberIdentifier) {
        axios
            .post(`/account/saveDefaultPayment`, {
                type: type,
                uniqueNumberIdentifier: uniqueNumberIdentifier
            })
            .then(response => {
                this.props.dispatchGetCards();
            })
            .catch(err => {
                console.log(err);
            });
    }

    openAddCardDialog = () => {
        $('#addCardForm').modal('open');
        setTimeout(() => {
            braintree.client.create({
                authorization: this.props.payments.token
            }, (err, clientInstance) => {
                if (err) {
                    console.error(err);
                    return;
                }

                braintree.hostedFields.create({
                    client: clientInstance,
                    styles: {
                        'input': {
                            'font-size': '16px',
                            'font-family': 'roboto, verdana, sans-serif',
                            'font-weight': 'lighter',
                            'color': 'black'
                        },
                        ':focus': {
                            'color': 'black'
                        },
                        '.valid': {
                            'color': 'black'
                        },
                        '.invalid': {
                            'color': 'black'
                        }
                    },
                    fields: {
                        number: {
                            selector: '#card-number',
                            placeholder: '1111 1111 1111 1111'
                        },
                        cvv: {
                            selector: '#cvv',
                            placeholder: '111'
                        },
                        expirationDate: {
                            selector: '#expiration-date',
                            placeholder: 'MM/YY'
                        },
                        postalCode: {
                            selector: '#postal-code',
                            placeholder: '11111'
                        }
                    }
                }, (err, hostedFieldsInstance) => {
                    if (err) {
                        console.error(err);
                        return;
                    }

                    function findLabel(field) {
                        return $('.hosted-field--label[htmlFor="' + field.container.id + '"]');
                    }

                    hostedFieldsInstance.on('focus', (event) => {
                        let field = event.fields[event.emittedBy];

                        findLabel(field).addClass('label-float').removeClass('filled');
                    });

                    // Emulates floating label pattern
                    hostedFieldsInstance.on('blur', (event) => {
                        let field = event.fields[event.emittedBy];
                        let label = findLabel(field);

                        if (field.isEmpty) {
                            label.removeClass('label-float');
                        } else if (field.isValid) {
                            label.addClass('filled');
                        } else {
                            label.addClass('invalid');
                        }
                    });

                    hostedFieldsInstance.on('empty', (event) => {
                        let field = event.fields[event.emittedBy];

                        findLabel(field).removeClass('filled').removeClass('invalid');
                    });

                    hostedFieldsInstance.on('validityChange', (event) => {
                        let field = event.fields[event.emittedBy];
                        let label = findLabel(field);

                        if (field.isPotentiallyValid) {
                            label.removeClass('invalid');
                        } else {
                            label.addClass('invalid');
                        }
                    });

                    $('#cardForm').submit( (event) => {
                        event.preventDefault();

                        hostedFieldsInstance.tokenize( (err, payload) => {
                            if (err) {
                                console.error(err);
                                toast.error(err.message);
                                return;
                            }
                            console.log(payload);
                            // This is where you would submit payload.nonce to your server

                            axios
                                .post(`/account/saveCard`, {
                                    cardType: payload.details.cardType,
                                    name: payload.details.lastFour,
                                    payment_method_nonce: payload.nonce
                                })
                                .then(response => {
                                    this.props.dispatchGetCards();
                                    toast.success("Card saved!");
                                    $('#addCardForm').modal('close');
                                    this.setState({isLoadingForm: false});
                                })
                                .catch(err => {
                                    console.log(err);
                                });
                        });
                    });

                    this.setState({isLoadingForm: true});

                });
            });

        }, 500);
    };

    render() {
        return (<div>
            <SubMenu/>

            <div className="container mywhitesection">

                <div className="section">

                    <div className="row">
                        <div className="col s12">
                            <h2 className="header center my-blue-text">Select How You Want To Pay</h2>
                        </div>

                        <div className="col s12 m12 l10 offset-l1 xl8 offset-xl2 valign-wrapper">
                            <table className="bordered highlight responsive-table paymentmethod">
                                <tbody>
                                {this.props.paymentTypes.types.map((v, k, a) => {
                                    return (
                                        <tr key={k}>
                                            <td><img src={v.imageUrl} alt="Visa" /></td>
                                            <td>{v.maskedNumber}</td>
                                            <td>
                                                <Input
                                                    name='deliverytype'
                                                    type='radio'
                                                    value={v.uniqueNumberIdentifier}
                                                    label=''
                                                    checked={v.default}
                                                    onChange={event => {
                                                        event.persist();
                                                        this.setPaymentDefault('card', event.target.value);
                                                    }
                                                    }
                                                />
                                            </td>
                                        </tr>
                                    );
                                })}

                                <tr>
                                    <td><img src="/site/img/cash-icon.png" alt="Cash" /></td>
                                    <td>Cash</td>
                                    <td>
                                        <Input
                                            name='deliverytype'
                                            type='radio'
                                            value='cash'
                                            label=''
                                            checked={this.props.profile.profile && this.props.profile.profile.pay_type === 'cash'}
                                            onChange={event => {
                                                event.persist();
                                                this.setPaymentDefault('cash');
                                            }
                                            }
                                        />
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div className="col s12 m12 l10 offset-l1 xl8 offset-xl2">
                            <a className="btn teal standartmargin" onClick={this.openAddCardDialog}>ADD CARD</a>
                        </div>
                    </div>
                </div>
            </div>

            <div id="addCardForm" className="modal ">

                <div className="col s12 m12 l8 xl8 offset-xl2 offset-l2">
                    <div className="row formheader">
                        <div className="col s8 offset-s2"><h4 style={{marginTop: 20}}>Add card</h4> </div>
                        <div className="col s2"><a className=" modal-action modal-close" ><img src="/site/img/close.png" alt="×" /></a></div>
                    </div>
                    <div>
                        <form id="cardForm" className="col s12 m12 l10 offset-l1 xl8 offset-xl2" style={!this.state.isLoadingForm ? {
                            position: 'absolute',
                            left: -10000
                        } : {}}>

                            <div className="row">
                                <div className="input-field col  s12 m12 l6 xl6">
                                    <div id="card-number" type="text" className="validate" />
                                    <label htmlFor="card-number">Credit Card Number</label>
                                </div>
                                <div className="input-field col  s12 m12 l6 xl6">
                                    <div id="postal-code" type="text" className="validate" />
                                    <label htmlFor="postal-code">Postal code</label>
                                </div>
                            </div>

                            <div className="row ">
                                <div className="input-field col  s12 m12 l4 xl4">
                                    <div id="expiration-date" type="text" className="validate" />
                                    <label htmlFor="expiration-date">Expires MM/YY</label>
                                </div>
                                <div className="input-field col  s12 m12 l4 xl4">
                                    <div id="cvv" type="number" className="validate" />
                                        <label htmlFor="cvv">Code (CVV)</label>
                                </div>
                            </div>


                            <div className="row center">
                                <div className="col s12 m12 l6 xl6">
                                    <button type="submit" className="btn teal lighten-1 standartmargin fullwidth">SAVE</button>
                                </div>
                                <div className="col s12 m12 l6 xl6">
                                    <button className="btn whitebutton standartmargin fullwidth modal-action modal-close" style={{textAlign: 'center'}} onClick={() => {
                                        $('#addCardForm').modal('close');
                                        this.setState({isLoadingForm: false});
                                    }}>CANCEL</button>
                                </div>
                            </div>

                        </form>

                        {!this.state.isLoadingForm && <div className={"css-jxiqlq-wrap-r"}>
                            <RingLoader
                                style={{margin: 'auto'}}
                                color={'#0019bc'}
                                loading={!this.state.isLoadingForm}
                            />
                        </div>}

                    </div>
                </div>

            </div>
        </div>);
    }
}

function mapStateToProps (state) {
    return {
        routing: state.routing,
        payments: state.payments,
        paymentTypes: state.paymentTypes,
        profile: state.profile
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchGetToken: () => {
            dispatch(getToken());
        },
        dispatchGetCards: () => {
            dispatch(getCards());
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentPage);

import React, {Component} from 'react';
import SubMenu from "../components/SubMenu";
import { ToastContainer, toast } from 'react-toastify';
import {connect} from "react-redux";
import {setProfile} from "../actions/profileActions";
import Clipboard from 'clipboard';

class ReferFriendPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount(){
        new Clipboard('#copyBtn');
    }

    render() {
        return (
            <div>
                <SubMenu/>
                <div className="container mywhitesection">
                    {this.props.profile.profile && <div className="section">
                        <div className="row center-align">
                            <div className="col s12">
                                <h2 className="header  my-blue-text">Reffer a Friend</h2>

                                <p className="blue-grey-text darken-4">
                                    You can see your referral link in field below. Copy it to clipboard and share with friends. </p>
                                <p>Your Jabrool ID is <span className="teal-text ">{this.props.profile.profile.jabroolid}</span>.
                                </p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col  s12 m12 l6 offset-l3 xl6 offset-xl3">
                                <input id="code" type="text" className="validate" defaultValue={'http://jabrool.com/jid?' + this.props.profile.profile.jabroolid} readOnly />
                                    <label>Referral Link</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col  s12 m12 l6 offset-l3 xl6 offset-xl3">
                                <button
                                    id={"copyBtn"}
                                    className="btn teal lighten-1 standartmargin fullwidth"
                                    data-clipboard-target="#code"
                                    onClick={() => {
                                        toast.success("Copied!");
                                    }}>COPY LINK</button>
                            </div>
                            <div className="col  s12 m12 l6 offset-l3 xl6 offset-xl3">
                                <button className="btn whitebutton  halfmargin fullwidth" onClick={() => {
                                    window.open('https://plus.google.com/share?url=http://jabrool.com/jid?' + this.props.profile.profile.jabroolid,
                                        '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
                                }}>SHARE TO GOOGLE PLUS</button>
                            </div>
                            <div className="col  s12 m12 l6 offset-l3 xl6 offset-xl3">
                                <button className="btn whitebutton  halfmargin fullwidth" onClick={() => {
                                    let url = 'http://jabrool.com/jid?' + this.props.profile.profile.jabroolid;
                                    window.open('http://www.linkedin.com/shareArticle?mini=true&url='+encodeURIComponent(url), '', 'left=0,top=0,width=650,height=420,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
                                }}>SHARE TO LINKEDIN</button>
                            </div>
                            <div className="col  s12 m12 l6 offset-l3 xl6 offset-xl3">
                                <button className="btn whitebutton  halfmargin fullwidth" onClick={() => {
                                    let url = 'http://jabrool.com/jid?' + this.props.profile.profile.jabroolid;
                                    let text = "Jabrool web site";
                                    window.open('http://twitter.com/share?url='+encodeURIComponent(url)+'&text='+encodeURIComponent(text), '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
                                }}>SHARE TO TWITTER</button>
                            </div>
                        </div>
                    </div>}
                </div>
                <ToastContainer />
            </div>
      );
    }
}

function mapStateToProps(state) {
    return {
        profile: state.profile
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchUpdate: (profile) => {
            dispatch(setProfile(profile));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ReferFriendPage);

import React, {Component} from 'react';
import SubMenu from "../components/SubMenu";
import {connect} from "react-redux";
import {updateOrdersLocations} from "../actions/historyOrdersActions";
import moment from "moment";

class MyHistoryPage extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        this.props.dispatchUpdateHistoryLocations()
    }

    render() {
        return (<div>
            <SubMenu/>
            <div className="container g-container">
                <div className="section">
                    <div className="row">

                        {this.props.historyOrders.orders.map((v, k, a) => {
                            return (
                                <div className=" col s12 m6 l6 xl4 ">
                                    <div className="card card-corder">
                                        <div className="card-content">
                             <span className="card-title activator grey-text text-darken-4">You → {v.order.recipient_name}
                              <i className="material-icons right teal-text">remove_red_eye</i>
                            </span>
                                            <div className="collection mycollection">
                                                <a href="#!" className="collection-item"><span className="badge">{v.order.owner_name}</span>Sender</a>
                                                <a href="#!" className="collection-item"><span className="badge">{v.order.recipient_name}</span>Receiver</a>
                                                <a href="#!" className="collection-item"><span className="badge">{v.order.owner_address}</span>Pick
                                                    Up</a>
                                                <a href="#!" className="collection-item"><span className="badge">{v.order.recipient_address}</span>Drop
                                                    Up</a>
                                                <a href="#!" className="collection-item"><span className="badge">{v.courier.first_name} {v.courier.last_name}</span>Courier</a>
                                                <a href="#!" className="collection-item"><span className="badge">{v.order.small_package_count + v.order.medium_package_count + v.order.large_package_count}</span>Number
                                                    of Packages</a>
                                                <a href="#!" className="collection-item"><span
                                                    className="badge">{v.courier.weight}kg</span>Weight</a>
                                                <a href="#!" className="collection-item"><span className="badge">{moment(v.courier.created_at).format("DD/MM/YYYY")}</span>Date</a>
                                                <a href="#!" className="collection-item"><span
                                                    className="badge">{v.order.cost}SAR</span>Amount</a>
                                            </div>
                                        </div>
                                        <div className="card-reveal">
                                            <span className="card-title grey-text text-darken-4">View Receipt<i
                                                className="material-icons right">close</i></span>
                                            <div className="collection mycollection">
                                                <a href="#!" className="collection-item"><span className="badge">{moment(v.courier.start_at).format("DD/MM/YYYY")}</span>Start
                                                    at</a>
                                                <a href="#!" className="collection-item"><span className="badge">{moment(v.courier.end_at).format("DD/MM/YYYY HH:mm")}</span>End
                                                    at</a>
                                                <a href="#!" className="collection-item"><span className="badge">{v.order.type}</span>Delivery
                                                    Type</a>
                                                <a href="#!" className="collection-item"><span
                                                    className="badge">{v.order.route} Mi</span>Distance</a>
                                                <a href="#!" className="collection-item"><span
                                                    className="badge">{v.order.cost}SAR</span>Total charge</a>
                                                <a href="#!" className="collection-item"><span
                                                    className="badge">{v.order.pay_type}</span>Payment Method</a>
                                                <a href="#!" className="collection-item"><span
                                                    className="badge">Payed</span>Payment Status</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            );
                        })}

                    </div>
                </div>
            </div>
        </div>);
    }
}

function mapStateToProps(state) {
    return {
        historyOrders: state.historyOrders
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchUpdateHistoryLocations: () => {
            dispatch(updateOrdersLocations());
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(MyHistoryPage);

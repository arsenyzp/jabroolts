import React, {Component} from 'react';
import SubMenu from "../components/SubMenu";
import {connect} from "react-redux";
import {applyCode, getBonusToday, successCodeApply} from "../actions/promoActions";

class RedeemPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            code: ""
        };
    }

    componentDidMount() {
        this.props.dispatchBonusToday();
        this.props.dispatchCodeApply(0);
    }

    _send() {
        this.props.dispatchApplyCode(this.state.code);
    }

    render() {
        return (<div>
            <SubMenu/>
            <div className="container mywhitesection">
                <div className="section">
                    <div className="row center-align">
                        <div className="col s12">
                            <h2 className="header  my-blue-text">Redeem Code</h2>
                            {this.props.promo.apply > 0 && <p className="center-align">You get <span className="teal-text "> ${this.props.promo.apply}</span> by using Promo Code</p>}
                            <p className="blue-grey-text darken-4">Total bonus till today: ${this.props.promo.amount}</p>
                            <p className="blue-grey-text darken-4">You can use the bonus in your next order.</p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col  s12 m12 l6 offset-l3 xl6 offset-xl3">
                            <input
                                type="text"
                                className="validate"
                                value={this.state.code}
                                onChange={event => {
                                    event.persist();
                                    this.setState(previousState => {
                                        return {
                                            ...previousState,
                                            code: event.target.value
                                        };
                                    })}}
                            />
                                <label>Enter Code</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col  s12 m12 l6 offset-l3 xl6 offset-xl3">
                            <button
                                className="btn teal lighten-1 standartmargin fullwidth"
                                onClick={() => {
                                    this._send();
                                }}
                            >APPLY</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>);
    }
}

function mapStateToProps (state) {
    return {
        routing: state.routing,
        promo: state.promo
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchBonusToday: () => {
            dispatch(getBonusToday());
        },
        dispatchApplyCode: (code) => {
            dispatch(applyCode(code));
        },
        dispatchCodeApply: (amount) => {
            dispatch(successCodeApply(amount));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(RedeemPage);

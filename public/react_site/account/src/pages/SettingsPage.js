import React, {Component} from 'react';
import SubMenu from "../components/SubMenu";
import {connect} from "react-redux";
import {updateFavoriteLocations} from "../actions/favoriteLocationsActions";
import {updateHistoryLocations} from "../actions/historyLocationsActions";

class SettingsPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        this.props.dispatchUpdate();
        this.props.dispatchUpdateHistory();
    }

    render() {
        return (<div>
            <SubMenu/>
            <div className="container mywhitesection">
                <div className="section">
                    <div className="row center-align">
                        <div className="col s12">
                            <h2 className="header  my-blue-text">Settings</h2>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col s12 m12 l4 xl4 card-panel min500">
                            <h5>Favourite Locations</h5>
                            {this.props.favoriteLocation.locations.map((v, k, a) => {
                                return (
                                    <div className=" settingsblock" key={k}>
                                        <p> {v.name} <i className="material-icons right">clear</i></p>
                                        <p className="blue-grey-text darken-4">{v.address}</p>
                                    </div>
                                );
                            })}
                        </div>

                        <div className="col s12 m12 l4 xl4 card-panel min500">
                            <h5>History of Searches</h5>
                            {this.props.historyLocation.locations.map((v, k, a) => {
                                return (
                                    <div className=" settingsblock2" key={k}>
                                        <p className="blue-grey-text darken-4">{v.address}</p>
                                    </div>
                                );
                            })}

                        </div>
                        <div className="col s12 m12 l4 xl4 card-panel min500">
                            <h5>Notifications Settings</h5>
                            <p><input type="checkbox" id="setting1" className="filled-in" checked="checked"  /><label for="setting1" className="sch">Package Received</label></p>
                            <p><input type="checkbox" id="setting2" className="filled-in" checked="checked"  /><label for="setting2" className="sch">Package Delivered</label></p>
                            <p><input type="checkbox" id="setting3" className="filled-in" checked="checked"  /><label for="setting3" className="sch">Order Cancelled</label></p>
                        </div>

                    </div>

                </div>
            </div>

        </div>);
    }
}

function mapStateToProps (state) {
    return {
        routing: state.routing,
        favoriteLocation: state.favoriteLocation,
        historyLocation: state.historyLocation
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchUpdate: () => {
            dispatch(updateFavoriteLocations());
        },
        dispatchUpdateHistory: () => {
            dispatch(updateHistoryLocations());
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsPage);

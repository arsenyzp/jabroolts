import React, {Component} from 'react';
import {render} from 'react-dom';
import {ToastContainer} from 'react-toastify';
import {Router, Route, hashHistory} from 'react-router';
import {syncHistoryWithStore} from 'react-router-redux';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import reducer from './reducers';
import MyHistoryPage from "./pages/MyHistoryPage";
import OrdersPage from "./pages/OrdersPage";
import PaymentPage from "./pages/PaymentPage";
import RedeemPage from "./pages/RedeemPage";
import ReferFriendPage from "./pages/ReferFriendPage";
import SettingsPage from "./pages/SettingsPage";
import {SocketService} from "./components/SocketService";
import ProfilePage from "./pages/ProfilePage";

export const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));
export const history = syncHistoryWithStore(hashHistory, store);

import './styles/main.css'

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {};
        SocketService.init();
    }

    render() {
        return (
            <div className="App">
                <div className={"right_col"} role="main">
                    <Provider store={store}>
                        <Router history={history}>
                            <Route path="/" component={OrdersPage}/>
                            <Route path="/history" component={MyHistoryPage}/>
                            <Route path="/payments" component={PaymentPage}/>
                            <Route path="/redeem" component={RedeemPage}/>
                            <Route path="/refer" component={ReferFriendPage}/>
                            <Route path="/settings" component={SettingsPage}/>
                            <Route path="/profile" component={ProfilePage}/>
                        </Router>
                    </Provider>
                </div>
                <ToastContainer/>
            </div>
        );
    }

}

render(
    <App/>,
    document.getElementById('account_home')
);

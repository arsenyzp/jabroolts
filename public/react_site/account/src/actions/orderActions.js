export function setOrderStep(step) {
    return {
        type: 'SET_STEP',
        payload: {
            step: step
        }
    };
}

export function setOwnerLocation(address, lat, lon) {
    return {
        type: 'SET_OWNER_LOCATION',
        payload: {
            address: address,
            lat: lat,
            lon: lon
        }
    };
}

export function setRecipientLocation(address, lat, lon) {
    return {
        type: 'SET_RECIPIENT_LOCATION',
        payload: {
            address: address,
            lat: lat,
            lon: lon
        }
    };
}

export function setPackages(small_count, medium_count, large_count) {
    return {
        type: 'SET_PACKAGES',
        payload: {
            small_count: small_count,
            medium_count: medium_count,
            large_count: large_count
        }
    };
}

export function setPromo(code) {
    return {
        type: 'SET_PROMO',
        payload: {
            code: code
        }
    };
}

export function setReceiveName(receiver_name) {
    return {
        type: 'SET_RECEIVE_NAME',
        payload: {
            receiver_name: receiver_name
        }
    };
}

export function setReceivePhone(receiver_phone) {
    return {
        type: 'SET_RECEIVE_PHONE',
        payload: {
            receiver_phone: receiver_phone
        }
    };
}

export function setReceiveJid(receiver_jid) {
    return {
        type: 'SET_RECEIVE_JID',
        payload: {
            receiver_jid: receiver_jid
        }
    };
}

export function setWeight(weight) {
    return {
        type: 'SET_WEIGHT',
        payload: {
            weight: weight
        }
    };
}

export function setType(type) {
    return {
        type: 'SET_TYPE',
        payload: {
            type: type
        }
    };
}

export function setPhotos(photos) {
    return {
        type: 'SET_PHOTOS',
        payload: {
            photos: photos
        }
    };
}

export function addPhoto(photo) {
    return {
        type: 'ADD_PHOTO',
        payload: {
            photo: photo
        }
    };
}

export function removePhoto(photo) {
    return {
        type: 'REMOVE_PHOTO',
        payload: {
            photo: photo
        }
    };
}

export function setDistance(distance) {
    return {
        type: 'SET_DISTANCE',
        payload: {
            distance: distance
        }
    };
}

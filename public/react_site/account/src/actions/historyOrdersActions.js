import axios from 'axios';

export function updateOrdersLocations() {
    return (dispatch) => {
        axios
            .get(`/account/ordersHistory/`, {})
            .then(response => {
                dispatch(_updateList(
                    response.data.data
                ));
            })
            .catch(err => {
               console.log(err);
            });
    };
}


export function _updateList(orders) {
    return {
        type: 'UPDATE_HISTORY_ORDERS_LIST',
        payload: {
            orders: orders
        }
    };
}

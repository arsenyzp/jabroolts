import axios from 'axios';

export function getToken() {
    return (dispatch) => {
        axios
            .get(`/account/getPaymentToken`, {})
            .then(response => {
                dispatch(_setPaymentToken(
                    response.data.data.token
                ));
            })
            .catch(err => {
                console.log(err);
            });
    };
}


export function _setPaymentToken(token) {
    return {
        type: 'SET_PAY_TOKEN',
        payload: {
            token: token
        }
    };
}

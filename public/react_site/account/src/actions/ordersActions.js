import axios from 'axios';

export function updateOrders() {
    return (dispatch) => {
        axios
            .get(`/account/orders/`, {})
            .then(response => {
                dispatch(_updateList(
                    response.data.data.ordersActive,
                    response.data.data.ordersCompleted,
                    response.data.data.ordersCanceled
                ));
            })
            .catch(err => {
               console.log(err);
            });
    };
}


export function _updateList(ordersActive, ordersCompleted, ordersCanceled) {
    return {
        type: 'UPDATE_LIST',
        payload: {
            ordersActive: ordersActive,
            ordersCompleted: ordersCompleted,
            ordersCanceled: ordersCanceled
        }
    };
}

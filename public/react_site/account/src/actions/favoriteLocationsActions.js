import axios from 'axios';

export function updateFavoriteLocations() {
    return (dispatch) => {
        axios
            .get(`/account/favoriteLocation/`, {})
            .then(response => {
                dispatch(_updateList(
                    response.data.data
                ));
            })
            .catch(err => {
               console.log(err);
            });
    };
}


export function _updateList(locations) {
    return {
        type: 'UPDATE_FAVORITE_LIST',
        payload: {
            locations: locations
        }
    };
}

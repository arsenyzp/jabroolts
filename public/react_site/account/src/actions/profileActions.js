import axios from "axios";

export function setProfile(profile) {
    return {
        type: 'PROFILE_UPDATED',
        payload: {
            profile: profile
        }
    };
}

export function loadProfile() {
    return (dispatch) => {
        axios
            .get(`/account/getProfile`, {})
            .then(response => {
                dispatch(setProfile(
                    response.data.data
                ));
            })
            .catch(err => {
                console.log(err);
            });
    };
}

export function saveProfile(profile) {
    return (dispatch) => {
        let data = {
            email: profile.email,
            phone: profile.phone,
            first_name: profile.first_name,
            last_name: profile.last_name
        };
        if(profile.new_password){
            data.profile = profile.new_password;
        }
        axios
            .post(`/account/saveProfile`, data)
            .then(response => {
                dispatch(setProfile(
                    response.data.data
                ));
            })
            .catch(err => {
                console.log(err);
            });
    };
}

import axios from 'axios';

export function updateHistoryLocations() {
    return (dispatch) => {
        axios
            .get(`/account/getHistory/`, {})
            .then(response => {
                dispatch(_updateList(
                    response.data.data
                ));
            })
            .catch(err => {
               console.log(err);
            });
    };
}


export function _updateList(locations) {
    return {
        type: 'UPDATE_HISTORY_LIST',
        payload: {
            locations: locations
        }
    };
}

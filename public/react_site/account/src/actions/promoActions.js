import axios from 'axios';

export function getBonusToday() {
    return (dispatch) => {
        axios
            .get(`/account/day_bonus/`, {})
            .then(response => {
                dispatch(_updateBonusToday(
                    response.data.data.bonus
                ));
            })
            .catch(err => {
                console.log(err);
            });
    };
}

export function applyCode(code) {
    return (dispatch) => {
        axios
            .get(`/account/apply_code/?code=${code}`, {})
            .then(response => {
                dispatch(getBonusToday());
                dispatch(successCodeApply(response.data.data.discount));
            })
            .catch(err => {
                console.log(err);
            });
    };
}


export function _updateBonusToday(amount) {
    return {
        type: 'UPDATE_DAY_PROMO',
        payload: {
            amount: amount
        }
    };
}

export function successCodeApply(amount) {
    return {
        type: 'APPLY_PROMO',
        payload: {
            amount: amount
        }
    };
}

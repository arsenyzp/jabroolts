import axios from 'axios';

export function getCards() {
    return (dispatch) => {
        axios
            .get(`/account/getProfileCards`, {})
            .then(response => {
                dispatch(_setPaymentTypes(
                    response.data.data
                ));
            })
            .catch(err => {
                console.log(err);
            });
    };
}


export function _setPaymentTypes(types) {
    return {
        type: 'SET_PAY_TYPES',
        payload: {
            types: types
        }
    };
}

const initialState = {
    orders: []
};

export default function historyLocations(state = initialState, action) {
    switch (action.type) {
        case 'UPDATE_HISTORY_ORDERS_LIST':
            return {
                ...state,
                orders: action.payload.orders
            };
        default:
            return state;
    }
}

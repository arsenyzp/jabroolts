const initialState = {
    token: ''
};

export default function notifications(state = initialState, action) {
    switch (action.type) {
        case 'SET_PAY_TOKEN':
            return {
                ...state,
                token: action.payload.token
            };
        default:
            return state;
    }
}

const initialState = {
    types: []
};

export default function paymentTypes(state = initialState, action) {
    switch (action.type) {
        case 'SET_PAY_TYPES':
            return {
                ...state,
                types: action.payload.types
            };
        default:
            return state;
    }
}

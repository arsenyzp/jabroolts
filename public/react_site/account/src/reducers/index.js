import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import notifications from "./notifications";
import socket from "./socket";
import orders from "./orders";
import order from "./order";
import promo from "./promo";
import profile from "./profile";
import favoriteLocation from "./favoriteLocation";
import historyLocation from "./historyLocation";
import historyOrders from "./historyOrders";
import payments from "./payments";
import paymentTypes from "./paymentTypes";

export default combineReducers({
    routing: routerReducer,
    notifications: notifications,
    socket: socket,
    orders: orders,
    promo: promo,
    profile: profile,
    order: order,
    favoriteLocation: favoriteLocation,
    historyLocation: historyLocation,
    historyOrders: historyOrders,
    payments: payments,
    paymentTypes: paymentTypes
});

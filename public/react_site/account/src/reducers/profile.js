const initialState = {
    profile: null
};

export default function profile(state = initialState, action) {
    switch (action.type) {
        case 'PROFILE_UPDATED':
            return {
                ...state,
                profile: action.payload.profile
            };
        default:
            return state;
    }
}

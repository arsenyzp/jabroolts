const initialState = {
    locations: []
};

export default function favoriteLocations(state = initialState, action) {
    switch (action.type) {
        case 'UPDATE_FAVORITE_LIST':
            return {
                ...state,
                locations: action.payload.locations
            };
        default:
            return state;
    }
}

const initialState = {
    step: 1,
    owner: {},
    recipient: {},
    small_count: 0,
    medium_count: 0,
    large_count: 0,
    code: '',
    receiver_name: '',
    receiver_phone: '',
    receiver_jid: '',
    weight: 0,
    type: 'jabrool',
    photos: [],
    distance: 0,

    owner_first_name: '',
    owner_last_name: '',
    owner_phone_number: '',
    owner_email: '',
    password: '',
    password_confirm: '',
    referall_id: '',
    access: ''
};

export default function order(state = initialState, action) {
    switch (action.type) {
        case 'SET_STEP':
            return {
                ...state,
                step: action.payload.step
            };
        case 'SET_OWNER_LOCATION':
            return {
                ...state,
                owner: {
                    lat: action.payload.lat,
                    lon: action.payload.lon,
                    address: action.payload.address
                }
            };
        case 'SET_RECIPIENT_LOCATION':
            return {
                ...state,
                recipient: {
                    lat: action.payload.lat,
                    lon: action.payload.lon,
                    address: action.payload.address
                }
            };
        case 'SET_PACKAGES':
            return {
                ...state,
                small_count: action.payload.small_count,
                medium_count: action.payload.medium_count,
                large_count: action.payload.large_count
            };
        case 'SET_PROMO':
            return {
                ...state,
                code: action.payload.code
            };
        case 'SET_RECEIVE_NAME':
            return {
                ...state,
                receiver_name: action.payload.receiver_name
            };
        case 'SET_RECEIVE_PHONE':
            return {
                ...state,
                receiver_phone: action.payload.receiver_phone
            };
        case 'SET_RECEIVE_JID':
            return {
                ...state,
                receiver_jid: action.payload.receiver_jid
            };
        case 'SET_WEIGHT':
            return {
                ...state,
                weight: action.payload.weight
            };
        case 'SET_TYPE':
            return {
                ...state,
                type: action.payload.type
            };
        case 'SET_PHOTOS':
            return {
                ...state,
                photos: action.payload.photos
            };
        case 'ADD_PHOTO':
            return {
                ...state,
                photos: state.photos.concat(action.payload.photo)
            };
        case 'REMOVE_PHOTO':
            return {
                ...state,
                photos: state.photos.filter(v => {
                    return v !== action.payload.photo;
                })
            };
        case 'SET_DISTANCE':
            return {
                ...state,
                distance: action.payload.distance
            };
        default:
            return state;
    }
}

const initialState = {
    ordersActive: [],
    ordersCompleted: [],
    ordersCanceled: [],
};

export default function notifications(state = initialState, action) {
    switch (action.type) {
        case 'UPDATE_LIST':
            return {
                ...state,
                ordersActive: action.payload.ordersActive,
                ordersCompleted: action.payload.ordersCompleted,
                ordersCanceled: action.payload.ordersCanceled,
            };
        default:
            return state;
    }
}

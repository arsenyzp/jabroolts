const initialState = {
    amount: 0,
    apply: 0
};

export default function notifications(state = initialState, action) {
    switch (action.type) {
        case 'UPDATE_DAY_PROMO':
            return {
                ...state,
                amount: action.payload.amount
            };
        case 'APPLY_PROMO':
            return {
                ...state,
                apply: action.payload.amount
            };
        default:
            return state;
    }
}

const initialState = {
    locations: []
};

export default function historyLocations(state = initialState, action) {
    switch (action.type) {
        case 'UPDATE_HISTORY_LIST':
            return {
                ...state,
                locations: action.payload.locations
            };
        default:
            return state;
    }
}

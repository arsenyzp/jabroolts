import React, {Component} from 'react';
import CreateRequest from "./CreateRequest";
import RequestDetails from "./RequestDetails";
import {connect} from "react-redux";
import {setOrderStep} from "../actions/orderActions";

class CreateOrder extends Component {
    render() {
        return (<div className="modal-content">
            {this.props.order.step === 1 && <CreateRequest/>}
            {this.props.order.step === 2 && <RequestDetails/>}
        </div>);
    }
}

function mapStateToProps(state) {
    return {
        order: state.order
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setOrderStep: (step) => {
            dispatch(setOrderStep(step));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateOrder);

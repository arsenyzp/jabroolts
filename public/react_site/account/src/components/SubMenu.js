import React, {Component} from 'react';
import {connect} from "react-redux";

class SubMenu extends Component {

    constructor(props) {
        super(props);
        console.log(props);
        this.state = {
        };
    }

    render() {
        const {pathname} = this.props.routing.locationBeforeTransitions;
        return (
            <div className="submenu">
                <div className="container">
                    <div className="row">
                        <div className="col s12">
                            <ul className="hide-on-med-and-down">
                                <li className={ pathname === '/' ? "active-element" : ""}>
                                    <a href="#/"><i className="material-icons">layers</i>My Orders</a>
                                </li>
                                <li className={ pathname === '/history' ? "active-element" : ""}>
                                    <a href="#/history"><i className="material-icons">attach_money</i>My History</a>
                                </li>
                                <li className={ pathname === '/payments' ? "active-element" : ""}>
                                    <a href="#/payments"><i className="material-icons">payment</i>Payment</a>
                                </li>
                                <li className={ pathname === '/redeem' ? "active-element" : ""}>
                                    <a href="#/redeem"><i className="material-icons">redeem</i>Redeem</a>
                                </li>
                                <li className={ pathname === '/refer' ? "active-element" : ""}>
                                    <a href="#/refer"><i className="material-icons">link</i>Refer a Friend</a>
                                </li>
                                <li className={ pathname === '/settings' ? "active-element" : ""}>
                                    <a href="#/settings"><i className="material-icons">settings</i>Settings</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps (state) {
    return {
        routing: state.routing
    }
}

export default connect(mapStateToProps)(SubMenu)

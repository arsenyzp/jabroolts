import React, {Component} from 'react';
import { render } from 'react-dom';
import {connect} from "react-redux";
import {
    setDistance, setOrderStep, setOwnerLocation, setPackages, setPromo,
    setRecipientLocation
} from "../actions/orderActions";
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete';
import axios from "axios/index";
import { ToastContainer, toast } from 'react-toastify';

class CreateRequest extends Component {

    constructor(props) {
        super(props);
        this.state = {
            owner_address: '',
            owner_place: null,
            recipient_address: '',
            recipient_place: null
        };
        this.onChangeOwner = (owner_address) => this.setState({ owner_address });
        this.onChangeRecipient = (recipient_address) => this.setState({ recipient_address });
        this.onSelectOwner = (owner_place) => {
            this.setState({ owner_place })
            geocodeByAddress(owner_place)
                .then(results => getLatLng(results[0]))
                .then(({ lat, lng }) => {
                    this.props.dispatchSetOwnerLocation(this.state.owner_place, lat, lng);
                    this.setState({owner_address: this.state.owner_place});
                })
                .catch(error => toast.error(error.message));
        };
        this.onSelectRecipient = (recipient_place) => {
            this.setState({ recipient_place });
            geocodeByAddress(recipient_place)
                .then(results => getLatLng(results[0]))
                .then(({ lat, lng }) => {
                    this.props.dispatchSetRecipientLocation(this.state.recipient_place, lat, lng);
                    this.setState({recipient_address: this.state.recipient_place})
                })
                .catch(error => toast.error(error.message));
        }
    }

    async calculateDistance() {
        if(!this.props.order.owner.lat || !this.props.order.owner.lon){
            toast.error("Pickup location required");
            return;
        }
        if(!this.props.order.recipient.lat || !this.props.order.recipient.lon){
            toast.error("Pickup location required");
            return;
        }
        if(!this.props.order.small_count & !this.props.order.medium_count & !this.props.order.large_count){
            toast.error("Package count required");
            return;
        }
        try {
            let response = await axios.post("/ajax/calculateDistance", this.props.order);
            console.log(response.data.data.rows);
            this.props.dispatchSetDistance(response.data.data.rows[0].elements[0].distance.value);
            this.props.dispatchSetOrderStep(2);
        } catch (error) {
            toast.error(error.message);
        }
    }

    render() {
        const inputPropsOwner = {
            value: this.state.owner_address,
            onChange: this.onChangeOwner
        };
        const inputPropsRecipient = {
            value: this.state.recipient_address,
            onChange: this.onChangeRecipient
        };
        const cssClasses = {
            root: 'input-field',
            input: 'form-control',
            autocompleteContainer: 'gautocompleate z-depth-4'
        };
        return (
            <div className="col s12 m12 l8 xl8 offset-xl2 offset-l2">
                <div className="row formheader">
                    <div className="col s8 offset-s2"><h4 style={{marginTop: 20}}>Create Request</h4> </div>
                    <div className="col s2"><a href="#" className=" modal-action modal-close" ><img src="/site/img/close.png" alt="×" /></a></div>
                </div>

                <div className="row">
                    <div className="input-field col s12 m12 l6 xl6">
                        <i className="material-icons prefix dropdownicon">location_on</i>
                        <PlacesAutocomplete
                            inputProps={inputPropsOwner}
                            classNames={cssClasses}
                            onSelect={this.onSelectOwner}
                        />
                        <label>Pickup Location </label>
                    </div>
                    <div className="input-field col s12 m12 l6 xl6">
                        <i className="material-icons prefix dropofficon">location_on</i>
                        <PlacesAutocomplete
                            inputProps={inputPropsRecipient}
                            classNames={cssClasses}
                            onSelect={this.onSelectRecipient}
                        />
                        <label>Drop Off Location</label>
                    </div>
                </div>

                <div className="row">
                    <div className="input-field col s12 m4 l4 xl4">
                        <input
                            placeholder="0"
                            type="number"
                            min="0"
                            max="3"
                            className="validate"
                            value={this.props.order.small_count}
                            onChange={event => {
                                event.persist();
                                this.props.dispatchSetPackages(event.target.value, this.props.order.medium_count, this.props.order.large_count)
                            }
                            }
                        />
                        <label>Small Package (4&Prime;-6&Prime;) </label>
                    </div>
                    <div className="input-field col s12 m4 l4 xl4">
                        <input
                            placeholder="0"
                            type="number"
                            min="0"
                            max="3"
                            className="validate"
                            value={this.props.order.medium_count}
                            onChange={event => {
                                event.persist();
                                this.props.dispatchSetPackages(this.props.order.small_count, event.target.value, this.props.order.large_count)
                            }
                            }
                        />
                        <label>Medium Package (4&Prime;-6&Prime;) </label>
                    </div>
                    <div className="input-field col s12 m4 l4 xl4">
                        <input
                            placeholder="0"
                            type="number"
                            min="0"
                            max="3"
                            className="validate"
                            value={this.props.order.large_count}
                            onChange={event => {
                                event.persist();
                                this.props.dispatchSetPackages(this.props.order.small_count, this.props.order.medium_count, event.target.value)
                            }
                            }
                        />
                        <label>Big Package (4&Prime;-6&Prime;) </label>
                    </div>
                </div>

                <div className="row">
                    <div className="input-field col s12">
                        <input
                            placeholder="Enter promo code"
                            type="text"
                            className="validate"
                            value={this.props.order.code}
                            onChange={event => {
                                event.persist();
                                this.props.dispatchSetPromo(event.target.value)
                            }
                            }
                        />
                        <label>Have a Promo Code?</label>
                    </div>
                </div>
                <div className="row center">
                    <div className="col s12">
                        <button
                            className="btn waves-effect waves-light teal lighten-1 standartmargin"
                            onClick={() => {
                                this.calculateDistance();
                            }}
                        >ADD PACKAGE DETAIL</button>
                        <div style={{height: 20}}/>
                    </div>
                </div>
                <ToastContainer />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        order: state.order
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchSetOrderStep: (step) => {
            dispatch(setOrderStep(step));
        },
        dispatchSetOwnerLocation: (address, lat, lon) => {
            dispatch(setOwnerLocation(address, lat, lon));
        },
        dispatchSetRecipientLocation: (address, lat, lon) => {
            dispatch(setRecipientLocation(address, lat, lon));
        },
        dispatchSetPackages: (small_count, medium_count, large_count) => {
            dispatch(setPackages(small_count, medium_count, large_count));
        },
        dispatchSetPromo: (code) => {
            dispatch(setPromo(code));
        },
        dispatchSetDistance: (distance) => {
            dispatch(setDistance(distance));
        }
    };
}

export default  connect(mapStateToProps, mapDispatchToProps)(CreateRequest);

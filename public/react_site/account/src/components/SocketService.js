import openSocket from 'socket.io-client';
import axios from "axios/index";
import {store} from "../index";
import {setConnect, setToken} from "../actions/socketActions";
import {updateOrders} from "../actions/ordersActions";
import {setProfile} from "../actions/profileActions";


export class SocketService {

    static socket = null;

    static init() {
        if(SocketService.socket === null){
            SocketService.socket = openSocket('/');
            SocketService._init();
        }
    }

    static _init() {

        SocketService.socket.on('connect', () => {
            console.log('socket connect');
        });

        SocketService.socket.on('init', async () => {
            console.log('socket init');
            try {
                let response = await axios.get(`/account/getToken/`, {
                    params: {
                    }
                });
                if(response.data.data.token){
                    store.dispatch(setToken(response.data.data.token));
                    store.dispatch(setProfile(response.data.data));
                    SocketService.socket.emit('web_auth', {token: response.data.data.token});
                }
            } catch (e) {
                console.log(e);
            }
        });

        SocketService.socket.on('auth', () => {
            console.log('socket auth');
            store.dispatch(setConnect(true));
        });

        SocketService.socket.on('orderUpdate', (data) => {
            console.log('orderUpdate');
            store.dispatch(updateOrders());
        });

        SocketService.socket.on('disconnect', () => {
            console.log('socket disconnect');
            store.dispatch(setConnect(false));
        });
    }

    static getSocket() {
        SocketService.init();
        return SocketService.socket;
    }
}

import React, {Component} from 'react';
import {connect} from "react-redux";

class CanceledOrders extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    renderItem(order, key) {
        return (
            <div className=" col s12 m6 l6 xl4" key={key}>
                <div className="card card-corder">
                    <div className="card-image waves-effect waves-block waves-light">
                        <img className="activator" src={order.map}/>
                    </div>
                    <div className="card-content">
                                <span
                                    className="card-title activator grey-text text-darken-4">You → {order.order.recipient_name}<i
                                    className="material-icons right teal-text">info</i></span>
                        <p><i className="material-icons prefix dropdownicon">location_on</i>
                            {order.order.owner_address}
                        </p>
                        <p><i className="material-icons prefix dropofficon">location_on</i>
                            {order.order.recipient_address}
                        </p>
                    </div>
                    <div className="card-reveal">
                                <span
                                    className="card-title grey-text text-darken-4">You → {order.order.recipient_name}<i
                                    className="material-icons right">close</i></span>

                        {order.courier && <div className="row corder-courier">
                            <div className="col s3 center-align">
                                <img className=" circle" src={order.courier.avatar} width="60" height="60"
                                     alt="courier"/>
                            </div>
                            <div className="col s9 left-align">
                                <p>{order.courier.first_name} {order.courier.last_name}</p>
                                <p>
                                    {Math.round(order.courier.rating) >= 1 &&
                                    <span className="fa fa-star starchecked"/>}
                                    {Math.round(order.courier.rating) >= 2 &&
                                    <span className="fa fa-star starchecked"/>}
                                    {Math.round(order.courier.rating) >= 3 &&
                                    <span className="fa fa-star starchecked"/>}
                                    {Math.round(order.courier.rating) >= 4 &&
                                    <span className="fa fa-star starchecked"/>}
                                    {Math.round(order.courier.rating) >= 5 &&
                                    <span className="fa fa-star starchecked"/>}

                                    {Math.round(order.courier.rating) < 1 &&
                                    <span className="fa fa-star starunchecked"/>}
                                    {Math.round(order.courier.rating) < 2 &&
                                    <span className="fa fa-star starunchecked"/>}
                                    {Math.round(order.courier.rating) < 3 &&
                                    <span className="fa fa-star starunchecked"/>}
                                    {Math.round(order.courier.rating) < 4 &&
                                    <span className="fa fa-star starunchecked"/>}
                                    {Math.round(order.courier.rating) < 5 &&
                                    <span className="fa fa-star starunchecked"/>}
                                </p>
                                <p className="blue-grey-text">{order.vehicle.model}</p>
                            </div>
                        </div>}

                        <div className="collection mycollection">
                            {order.order.type === 'jabrool' &&
                            <a href="#!" className="collection-item"><span className="badge">Jabrool Budget</span>Delivery
                                Type </a>}
                            {order.order.type === 'express' &&
                            <a href="#!" className="collection-item"><span className="badge">Express</span>Delivery Type
                            </a>}
                            <a href="#!" className="collection-item">
                            <span className="badge">
                                {order.order.small_package_count + order.order.medium_package_count + order.order.large_package_count}pck / {order.order.weight}kg</span>Size
                            </a>
                            <a href="#!" className="collection-item">
                                <span className="badge">{order.order.cost} SAR</span>Price
                            </a>
                            <a href="#!" className="collection-item">
                                <span className="badge">{order.order.owner_comment}</span>Description
                            </a>
                        </div>
                        <div className="row">
                            {order.order.photos.map((v, k, a) => {
                                return (
                                    <div className="col s3">
                                        <a data-fancybox={"gallery" + key} href={v}><img
                                            className="materialboxed  responsive-img" width="80" src={v}/></a>
                                        {/*<img className="materialboxed  responsive-img" width="80" src={v} />*/}
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div className="container g-container">
                <div className="section">
                    <div className="row">

                        {this.props.orders.ordersCanceled.map((v, k, a) => this.renderItem(v, k))}

                    </div>
                </div>
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        orders: state.orders
    }
}

export default connect(mapStateToProps)(CanceledOrders);

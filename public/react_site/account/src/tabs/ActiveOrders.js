import React, {Component} from 'react';
import {connect} from "react-redux";
import axios from 'axios';
import { toast } from 'react-toastify';
import {updateOrders} from "../actions/ordersActions";

class ActiveOrders extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loadingCancelInformation: false,
            orderForCanceled: null,
            orderCancelInformation: null
        };
    }

    getCancelCost(id) {
        this.setState({loadingCancelInformation: true});
        axios
            .get(`/account/cancel_cost/?id=${id}`, {})
            .then(response => {
                console.log(response.data.data);
                this.setState({
                    loadingCancelInformation: false,
                    orderCancelInformation: response.data.data
                });
            })
            .catch(err => {
                console.log(err);
                this.setState({loadingCancelInformation: false});
                toast.error("Server error");
            });
    }

    cancelOrder(id) {
        this.setState({loadingCancelInformation: true});
        axios
            .get(`/account/cancel/?id=${id}`, {})
            .then(response => {
                this.setState({loadingCancelInformation: false});
                this.props.dispatchUpdate();
                toast.success("Order canceled");
            })
            .catch(err => {
                console.log(err);
                toast.error("Server error");
                this.setState({loadingCancelInformation: false});
            });
    }

    renderItem(order, key) {
        return (
            <div className=" col s12 m6 l6 xl4" key={key}>
                <div className="card card-corder">
                    <div className="card-image waves-effect waves-block waves-light">
                        <img className="activator" src={order.map}/>
                    </div>
                    <div className="card-content">
                                <span
                                    className="card-title activator grey-text text-darken-4">You → {order.order.recipient_name}<i
                                    className="material-icons right teal-text">info</i></span>
                        <p><i className="material-icons prefix dropdownicon">location_on</i>
                            {order.order.owner_address}
                        </p>
                        <p><i className="material-icons prefix dropofficon">location_on</i>
                            {order.order.recipient_address}
                        </p>
                    </div>
                    <div className="card-reveal">
                                <span
                                    className="card-title grey-text text-darken-4">You → {order.order.recipient_name}<i
                                    className="material-icons right">close</i></span>

                        {order.courier && <div className="row corder-courier">
                            <div className="col s3 center-align">
                                <img className=" circle" src={order.courier.avatar} width="60" height="60"
                                     alt="courier"/>
                            </div>
                            <div className="col s9 left-align">
                                <p>{order.courier.first_name} {order.courier.last_name}</p>
                                <p>
                                    {Math.round(order.courier.rating) >= 1 && <span className="fa fa-star starchecked"/>}
                                    {Math.round(order.courier.rating) >= 2 && <span className="fa fa-star starchecked"/>}
                                    {Math.round(order.courier.rating) >= 3 && <span className="fa fa-star starchecked"/>}
                                    {Math.round(order.courier.rating) >= 4 && <span className="fa fa-star starchecked"/>}
                                    {Math.round(order.courier.rating) >= 5 && <span className="fa fa-star starchecked"/>}

                                    {Math.round(order.courier.rating) < 1 && <span className="fa fa-star starunchecked"/>}
                                    {Math.round(order.courier.rating) < 2 && <span className="fa fa-star starunchecked"/>}
                                    {Math.round(order.courier.rating) < 3 && <span className="fa fa-star starunchecked"/>}
                                    {Math.round(order.courier.rating) < 4 && <span className="fa fa-star starunchecked"/>}
                                    {Math.round(order.courier.rating) < 5 && <span className="fa fa-star starunchecked"/>}
                                </p>
                                <p className="blue-grey-text">{order.vehicle.model}</p>
                            </div>
                        </div>}

                        <div className="collection mycollection">
                            {order.order.type === 'jabrool' &&
                            <a href="#!" className="collection-item"><span className="badge">Jabrool Budget</span>Delivery
                                Type </a>}
                            {order.order.type === 'express' &&
                            <a href="#!" className="collection-item"><span className="badge">Express</span>Delivery Type
                            </a>}
                            <a href="#!" className="collection-item">
                            <span className="badge">
                                {order.order.small_package_count + order.order.medium_package_count + order.order.large_package_count}pck / {order.order.weight}kg</span>Size
                            </a>
                            <a href="#!" className="collection-item">
                                <span className="badge">{order.order.cost} SAR</span>Price
                            </a>
                            <a href="#!" className="collection-item">
                                <span className="badge">{order.order.owner_comment}</span>Description
                            </a>
                        </div>
                        <div className="row">
                            {order.order.photos.map((v, k, a) => {
                                return (
                                    <div className="col s3">
                                        <a data-fancybox={"gallery" + key} href={v}><img
                                            className="materialboxed  responsive-img" width="80" src={v}/></a>
                                        {/*<img className="materialboxed  responsive-img" width="80" src={v} />*/}
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                    <div className="card-action">
                        <a className="teal-text loginlink modal-trigger" data-target="loginpopup">CHAT</a>
                        <a data-target="cancelpopup" className="modal-trigger" onClick={() => {
                            this.setState(previousState => {
                                return {
                                    ...previousState,
                                    orderForCanceled: order.order
                                };
                            }, () => {
                                this.getCancelCost(order.order.id);
                            });
                        }}>CANCEL</a>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div className="container g-container">
                <div className="section">
                    <div className="row">

                        {this.props.orders.ordersActive.map((v, k, a) => this.renderItem(v, k))}

                    </div>
                </div>

                <div id="cancelpopup" className="modal ">
                    <div className="modal-content">
                        <div className="row header valign-wrapper">
                            <div className="col s8 offset-s2">Cancel order?</div>
                            <div className="col s2">
                                <a href="#!" className="modal-action modal-close">
                                    <img src="/site/img/close.png" alt="×"/>
                                </a>
                            </div>
                        </div>
                    </div>
                    {this.state.orderCancelInformation && <div>
                        <div className={"row"}>
                            <div className={"input-field col col s10 offset-s1"} style={{
                                flex: 1,
                                justifyContent: 'center',
                                display: 'flex'
                            }}>
                                <h2>Cancel cost {this.state.orderCancelInformation.cost} SAR</h2>
                                <div style={{height: 10}}/>
                            </div>
                        </div>
                        <div className={"row center"}>
                            <div className={"col s6"}>
                                <button
                                    className={"btn waves-effect waves-light teal lighten-1 standartmargin modal-action modal-close"}
                                >CANCEL
                                </button>
                            </div>
                            <div className={"col s6"}>
                                <button
                                    className={"btn waves-effect waves-light teal lighten-1 standartmargin"}
                                    onClick={() => {
                                        this.cancelOrder(this.state.orderForCanceled.id);
                                    }}
                                >CONFIRM
                                </button>
                            </div>
                        </div>
                        <div style={{height: 10}}/>
                    </div>}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        orders: state.orders
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchUpdate: () => {
            dispatch(updateOrders());
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ActiveOrders);

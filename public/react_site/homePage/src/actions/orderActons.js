export function setOrderStep(step) {
    return {
        type: 'SET_STEP',
        payload: {
            step: step
        }
    };
}

export function setOwnerLocation(address, lat, lon) {
    return {
        type: 'SET_OWNER_LOCATION',
        payload: {
            address: address,
            lat: lat,
            lon: lon
        }
    };
}

export function setRecipientLocation(address, lat, lon) {
    return {
        type: 'SET_RECIPIENT_LOCATION',
        payload: {
            address: address,
            lat: lat,
            lon: lon
        }
    };
}

export function setPackages(small_count, medium_count, large_count) {
    return {
        type: 'SET_PACKAGES',
        payload: {
            small_count: small_count,
            medium_count: medium_count,
            large_count: large_count
        }
    };
}

export function setPromo(code) {
    return {
        type: 'SET_PROMO',
        payload: {
            code: code
        }
    };
}

export function setReceiveName(receiver_name) {
    return {
        type: 'SET_RECEIVE_NAME',
        payload: {
            receiver_name: receiver_name
        }
    };
}

export function setReceivePhone(receiver_phone) {
    return {
        type: 'SET_RECEIVE_PHONE',
        payload: {
            receiver_phone: receiver_phone
        }
    };
}

export function setReceiveJid(receiver_jid) {
    return {
        type: 'SET_RECEIVE_JID',
        payload: {
            receiver_jid: receiver_jid
        }
    };
}

export function setWeight(weight) {
    return {
        type: 'SET_WEIGHT',
        payload: {
            weight: weight
        }
    };
}

export function setType(type) {
    return {
        type: 'SET_TYPE',
        payload: {
            type: type
        }
    };
}

export function setPhotos(photos) {
    return {
        type: 'SET_PHOTOS',
        payload: {
            photos: photos
        }
    };
}

export function addPhoto(photo) {
    return {
        type: 'ADD_PHOTO',
        payload: {
            photo: photo
        }
    };
}

export function removePhoto(photo) {
    return {
        type: 'REMOVE_PHOTO',
        payload: {
            photo: photo
        }
    };
}

export function setRefelall(referall_id) {
    return {
        type: 'SET_REFERAL_ID',
        payload: {
            referall_id: referall_id
        }
    };
}

export function setPasswordConfirm(password_confirm) {
    return {
        type: 'SET_PASSWORD_CONFIRM',
        payload: {
            password_confirm: password_confirm
        }
    };
}

export function setPassword(password) {
    return {
        type: 'SET_PASSWORD',
        payload: {
            password: password
        }
    };
}

export function setEmail(owner_email) {
    return {
        type: 'SET_EMAIL',
        payload: {
            owner_email: owner_email
        }
    };
}

export function setPhone(owner_phone_number) {
    return {
        type: 'SET_PHONE',
        payload: {
            owner_phone_number: owner_phone_number
        }
    };
}

export function setLastName(owner_last_name) {
    return {
        type: 'SET_OWNER_LAST_NAME',
        payload: {
            owner_last_name: owner_last_name
        }
    };
}

export function setFirstName(owner_first_name) {
    return {
        type: 'SET_OWNER_FIRST_NAME',
        payload: {
            owner_first_name: owner_first_name
        }
    };
}

export function setDistance(distance) {
    return {
        type: 'SET_DISTANCE',
        payload: {
            distance: distance
        }
    };
}

import axios from 'axios';
import {ITEMS_PER_PAGE} from "../components/Constants";

export function updateData() {
    return async dispatch => {
        try {
            let response = await axios.get(`/admin/statistics`, {
                params: {
                    sortKey: "",
                    reverse: ""
                }
            });
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    console.log(err);
                });
            } else {
                dispatch({
                    type: 'UPDATE_STATISTIC_DATE',
                    payload: {
                        data: response.data.data
                    }
                });
            }
        } catch (e) {
            console.log(e);
        }
    };
}

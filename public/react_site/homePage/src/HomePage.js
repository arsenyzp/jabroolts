import React, {Component} from 'react';
import { render } from 'react-dom';
import { ToastContainer, toast } from 'react-toastify';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { Router, Route, hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import reducer from './reducers';
import axios from "axios/index";

export const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));
export const history = syncHistoryWithStore(hashHistory, store);

export default class HomePage extends Component {

    constructor(props){
        super(props);
        this.state = {
            loading: true,
            form: {
                name: "",
                number: ""
            }
        };
    }

    async _send() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.post(`/site/contact_us`, this.state.form);
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                this.setState(previousState => {
                    return {
                        ...previousState,
                        loading: false,
                        form: {
                            name: "",
                            number: ""
                        }
                    };
                }, () => {
                    toast.success("Message sent!");
                });
            }
        } catch (e) {
            console.log(e.response);
            if(!!e.response && !!e.response.data.errors && e.response.data.errors.length > 0) {
                e.response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                toast.error(e.message);
            }
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
        }
    }

    render() {
        return (
            <form className={"col s12"}>
                <div className={"row"}>
                    <div className={"input-field col s12 m12 l5 xl5"}>
                        <input
                            type="text"
                            className={"validate"}
                            value={this.state.form.name}
                            onChange={event => {
                                event.persist();
                                this.setState(previousState => {
                                    return {
                                        ...previousState,
                                        form: {
                                            ...previousState.form,
                                            name: event.target.value
                                        }
                                    };
                                })}}
                        />
                        <label>Your Name</label>
                    </div>
                    <div className="input-field col s12 m12 l4 xl4">
                        <input
                            type="tel"
                            className={"validate"}
                            value={this.state.form.number}
                            onChange={event => {
                                event.persist();
                                this.setState(previousState => {
                                    return {
                                        ...previousState,
                                        form: {
                                            ...previousState.form,
                                            number: event.target.value
                                        }
                                    };
                                })}}
                        />
                        <label>Contact Number</label>
                    </div>
                    <div className={"col s12 m12 l3 xl3 button-w"}>
                        <button onClick={() => {
                            this._send();
                        }} className={"btn waves-effect waves-light teal lighten-1 standartmargin"}>CALL ME</button>
                    </div>
                </div>
                <ToastContainer />
            </form>
        );
    }

}

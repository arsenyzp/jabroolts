import React, {Component} from 'react';
import {render} from 'react-dom';
import {connect} from "react-redux";
import {
    addPhoto, removePhoto,
    setOrderStep, setPhotos, setReceiveJid, setReceiveName, setReceivePhone, setType,
    setWeight
} from "../actions/orderActons";
import {Input} from "react-materialize";
import axios from "axios";
import {toast} from "react-toastify";

class RequestDetails extends Component {

    async _preUpload(event) {
        if(this.fileInput.files.length === 0) {
            return;
        }
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        let formdata = new FormData();
        formdata.append('image', this.fileInput.files[0]);
        try {
            let response = await axios.post(`/ajax/uploads_img`, formdata, {headers: {
                    'Content-Type': 'multipart/form-data'
                }});
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                this.fileInput.value = '';
                this.props.dispatchAddPhoto(response.data.data.image)
            }
        } catch (e) {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
            toast.error(e.message);
        }
    }

    render() {
        return (
            <div className="col s12 m12 l8 xl8 offset-xl2 offset-l2 formstep1">
                <div className="row  formheader">
                    <div className="col s2">
                        <a onClick={() => {
                            this.props.dispatchSetOrderStep(1);
                        }}><img src="/site/img/back.png" alt="back" className="backarrow valign-wrapper" /></a>
                    </div>
                    <div className="col s8">
                        <h4 className="formheader">Request Details</h4>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12 m6 l6 xl6">
                        <input
                            id="receiver_name"
                            type="text"
                            className="validate"
                            value={this.props.order.receiver_name}
                            onChange={event => {
                                event.persist();
                                this.props.dispatchSetReceiveName(event.target.value)
                            }
                            }
                        />
                            <label>Receiver's Name</label>
                    </div>
                    <div className="input-field col s12 m6 l6 xl6">
                        <input
                            id="receiver_cnumber"
                            type="text"
                            className="validate"
                            value={this.props.order.receiver_phone}
                            onChange={event => {
                                event.persist();
                                this.props.dispatchSetReceivePhone(event.target.value)
                            }
                            }
                        />
                            <label>Receiver's Contact Number</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12 m6 l6 xl6">
                        <input
                            id="jabrool_id"
                            type="text"
                            className="validate"
                            value={this.props.order.receiver_jid}
                            onChange={event => {
                                event.persist();
                                this.props.dispatchSetReceiveJid(event.target.value)
                            }
                            }
                        />
                            <label>Jabrool ID (Receiver)</label>
                    </div>
                    <div className="input-field col s12 m6 l6 xl6">
                        <input
                            id="weight"
                            type="number"
                            className="validate"
                            value={this.props.order.weight}
                            onChange={event => {
                                event.persist();
                                this.props.dispatchSetWeight(event.target.value);
                            }
                            }
                        />
                            <label>Weight (kg) </label>
                    </div>
                </div>
                <div className="row ">
                    <div className="col s12 radiomargin">
                        <p className="rbtn-label">Delivery Type</p>
                        <Input
                            name='deliverytype'
                            type='radio'
                            value='jabrool'
                            label='JABROOL'
                            checked={this.props.order.type}
                            onChange={event => {
                                event.persist();
                                this.props.dispatchSetType(event.target.value);
                            }
                            }
                        />
                        <Input
                            name='deliverytype'
                            type='radio'
                            value='express'
                            label='EXPRESS'
                            checked={this.props.order.type}
                            onChange={event => {
                                event.persist();
                                this.props.dispatchSetType(event.target.value);
                            }
                            }
                        />
                    </div>
                </div>

                <div className="row">
                    <div className="file-field input-field col s12">
                        <div className="btn whitebutton">
                            <span>File</span>
                            <input
                                type="file"
                                ref={input => {
                                    this.fileInput = input;
                                }}
                                onChange={this._preUpload.bind(this)}
                            />
                        </div>
                        <div className="file-path-wrapper">
                            {this.props.order.photos.map((v, k, a) => {
                               return (
                                   <div className="col s2 m2" style={{position: 'relative'}}>
                                       <img src={'/uploads/tmp/' + v} className="z-depth-2 preview"/>
                                       <a onClick={() => {
                                           this.props.dispatchRemovePhoto(v);
                                       }}
                                          style={{position: 'absolute', top: 5, right: 10, padding: 5, color: 'red'}}
                                       >X</a>
                                   </div>
                               );
                            })}
                        </div>
                    </div>

                </div>

                <div className="row center">
                    <div className="col s12"><a onClick={() => {
                        this.props.dispatchSetOrderStep(3);
                    }}
                                            className="btn waves-effect waves-light teal lighten-1 standartmargin">ENTER
                        PERSONAL DATA</a></div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        order: state.order
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchSetOrderStep: (step) => {
            dispatch(setOrderStep(step));
        },
        dispatchSetReceiveName: (receiver_name) => {
            dispatch(setReceiveName(receiver_name));
        },
        dispatchSetReceivePhone: (receiver_phone) => {
            dispatch(setReceivePhone(receiver_phone));
        },
        dispatchSetReceiveJid: (receiver_jid) => {
            dispatch(setReceiveJid(receiver_jid));
        },
        dispatchSetWeight: (weight) => {
            dispatch(setWeight(weight));
        },
        dispatchSetType: (type) => {
            dispatch(setType(type));
        },
        dispatchAddPhoto: (photo) => {
            dispatch(addPhoto(photo));
        },
        dispatchRemovePhoto: (photo) => {
            dispatch(removePhoto(photo));
        }
    };
}

export default  connect(mapStateToProps, mapDispatchToProps)(RequestDetails);

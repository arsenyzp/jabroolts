import React, {Component} from 'react';
import {render} from 'react-dom';
import {connect} from "react-redux";
import {
    setEmail, setFirstName, setLastName, setOrderStep, setPassword, setPasswordConfirm, setPhone,
    setRefelall
} from "../actions/orderActons";
import axios from "axios";
import {toast} from "react-toastify";

class PersonalData extends Component {

    async send() {
        try {
            let response = await axios.post("/ajax/saveTmpOrder", {
                jid: this.props.order.receiver_jid,
                recipient_name: this.props.order.receiver_name,
                recipient_contact: this.props.order.receiver_phone,
                owner_address: this.props.order.owner.address,
                recipient_address: this.props.order.recipient.address,
                route: this.props.order.distance,
                type: this.props.order.type,
                weight: this.props.order.weight,
                photos: this.props.order.photos,
                owner_lon: this.props.order.owner.lon,
                owner_lat: this.props.order.owner.lat,
                recipient_lon: this.props.order.recipient.lon,
                recipient_lat: this.props.order.recipient.lat,
                small_package_count: parseInt(this.props.order.small_count),
                medium_package_count: parseInt(this.props.order.medium_count),
                large_package_count: parseInt(this.props.order.large_count),
                code: this.props.order.code,

                phone: this.props.order.owner_phone_number,
                email: this.props.order.owner_email,
                first_name: this.props.order.owner_first_name,
                last_name: this.props.order.owner_last_name,
                password: this.props.order.password,
                rjid: this.props.order.referall_id,
            });
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                toast.success("Request created");
                setTimeout(() => {
                    document.location.href = "/account";
                }, 1000);
            }
        } catch (e) {
            console.log(e)
        }
    }

    render() {
        return (
            <div className="col s12 m12 l8 xl8 offset-xl2 offset-l2 formstep1">

                <div className="row  formheader">
                    <div className="col s2">
                        <a onClick={() => {
                            this.props.dispatchSetOrderStep(2);
                        }}><img src="/site/img/back.png" alt="back"
                                className="backarrow valign-wrapper"/></a>
                    </div>
                    <div className="col s8">
                        <h4 className="formheader">Personal Data</h4>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12 m6 l6 xl6">
                        <input
                            id="first_name"
                            type="text"
                            className="validate"
                            value={this.props.order.owner_first_name}
                            onChange={event => {
                                event.persist();
                                this.props.dispatchSetFirstName(event.target.value);
                            }
                            }
                        />
                        <label>First Name</label>
                    </div>
                    <div className="input-field col s12 m6 l6 xl6">
                        <input
                            id="last_name"
                            type="text"
                            className="validate"
                            value={this.props.order.owner_last_name}
                            onChange={event => {
                                event.persist();
                                this.props.dispatchSetLastName(event.target.value);
                            }
                            }
                        />
                        <label>Last Name</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12 m6 l6 xl6">
                        <input
                            id="phone_number"
                            type="tel"
                            className="validate"
                            value={this.props.order.owner_phone_number}
                            onChange={event => {
                                event.persist();
                                this.props.dispatchSetPhone(event.target.value);
                            }
                            }
                        />
                        <label>Phone Number</label>
                    </div>
                    <div className="input-field col s12 m6 l6 xl6">
                        <input
                            id="email"
                            type="email"
                            className="validate"
                            value={this.props.order.owner_email}
                            onChange={event => {
                                event.persist();
                                this.props.dispatchSetEmail(event.target.value);
                            }
                            }
                        />
                        <label>Email</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12 m6 l6 xl6">
                        <input
                            id="pass"
                            type="password"
                            className="validate"
                            value={this.props.order.password}
                            onChange={event => {
                                event.persist();
                                this.props.dispatchSetPassword(event.target.value);
                            }
                            }
                        />
                        <label>Choose Password</label>
                    </div>
                    <div className="input-field col s12 m6 l6 xl6">
                        <input
                            id="retype_pass"
                            type="password"
                            className="validate"
                            value={this.props.order.password_confirm}
                            onChange={event => {
                                event.persist();
                                this.props.dispatchSetPasswordConfirm(event.target.value);
                            }
                            }
                        />
                        <label>Confirm Password</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                        <input
                            id="ref"
                            type="text"
                            className="validate"
                            value={this.props.order.referall_id}
                            onChange={event => {
                                event.persist();
                                this.props.dispatchSetRefelall(event.target.value);
                            }
                            }
                        />
                        <label>Referral Person Jabrool ID</label>
                    </div>
                </div>
                <div className="row">
                    <div className="col s12">
                        <p>
                            <input
                                type="checkbox"
                                id="agree"
                                className="filled-in"
                            />
                            <label className=" blue-grey-text">I Agree to Jabrool's <a href="terms.html"
                                                                                       className="green-text">Terms &amp; Conditions</a> and <a
                                href="privacypolicy.html" className="green-text">Privacy Policy</a></label></p>
                        <br/>
                    </div>
                </div>
                <div className="row">
                    <div className="col s6 m6 l4 offset-l2 xl3 offset-xl3"><a data-target="loginpopup"
                                                                              className="btn whitebutton fullwidth standartmargin"
                                                                              onClick={() => {
                                                                                  $('#loginpopup').modal('open');
                                                                              }}>LOGIN</a>
                    </div>
                    <div className="col s6 m6 l4 xl3"><a onClick={() => {
                        this.send()
                    }} className="btn fullwidth waves-effect waves-light teal lighten-1 standartmargin">DONE</a>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        order: state.order
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchSetOrderStep: (step) => {
            dispatch(setOrderStep(step));
        },
        dispatchSetRefelall: (referall_id) => {
            dispatch(setRefelall(referall_id));
        },
        dispatchSetPasswordConfirm: (password_confirm) => {
            dispatch(setPasswordConfirm(password_confirm));
        },
        dispatchSetPassword: (password) => {
            dispatch(setPassword(password));
        },
        dispatchSetEmail: (email) => {
            dispatch(setEmail(email));
        },
        dispatchSetPhone: (owner_phone_number) => {
            dispatch(setPhone(owner_phone_number));
        },
        dispatchSetLastName: (owner_last_name) => {
            dispatch(setLastName(owner_last_name));
        },
        dispatchSetFirstName: (owner_last_name) => {
            dispatch(setFirstName(owner_last_name));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(PersonalData);

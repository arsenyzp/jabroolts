import React, {Component} from 'react';
import {render} from 'react-dom';
import {ToastContainer, toast} from 'react-toastify';
import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import {Router, Route, hashHistory} from 'react-router';
import {syncHistoryWithStore} from 'react-router-redux';

import reducer from './reducers';
import axios from "axios/index";
import {Button, Icon, Input} from 'react-materialize';

export const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));
export const history = syncHistoryWithStore(hashHistory, store);

export default class RegisterPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            form: {
                email: "",
                phone: "",
                password: "",
                repeat_password: "",
                first_name: "",
                last_name: "",
                jid: "",
                accept: false,
                business: false,
                customer: true
            }
        };
    }

    async _send() {
        if (this.state.form.password !== this.state.form.repeat_password) {
            toast.error("Password does not match");
            return;
        }
        if (!this.state.form.accept) {
            toast.error("Accept terms is required");
            return;
        }
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.post(`/site/register`, this.state.form);
            if (!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                this.setState(previousState => {
                    return {
                        ...previousState,
                        loading: false,
                        form: {
                            email: "",
                            phone: "",
                            password: "",
                            repeat_password: "",
                            first_name: "",
                            last_name: "",
                            jid: "",
                            accept: false,
                            business: false,
                            customer: true
                        }
                    };
                }, () => {
                    toast.success("Register success");
                    setTimeout(() => {
                        document.location.href = "/account";
                    }, 1000);
                });
            }
        } catch (e) {
            console.log(e.response);
            if (!!e.response && !!e.response.data.errors && e.response.data.errors.length > 0) {
                e.response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                toast.error(e.message);
            }
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
        }
    }

    render() {
        return (
            <div className="col s12 m12 l10 offset-l1 xl8 offset-xl2">

                <div className="row">
                    <div className="input-field col s12 m6 l6 xl6">
                        <input
                            type="text" className="validate"
                            value={this.state.form.first_name}
                            onChange={event => {
                                event.persist();
                                this.setState(previousState => {
                                    return {
                                        ...previousState,
                                        form: {
                                            ...previousState.form,
                                            first_name: event.target.value
                                        }
                                    };
                                })}}
                        />
                        <label>First Name</label>
                    </div>
                    <div className="input-field col s12 m6 l6 xl6">
                        <input
                            type="text"
                            className="validate"
                            value={this.state.form.last_name}
                            onChange={event => {
                                event.persist();
                                this.setState(previousState => {
                                    return {
                                        ...previousState,
                                        form: {
                                            ...previousState.form,
                                            last_name: event.target.value
                                        }
                                    };
                                })}}
                        />
                        <label>Last Name</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12 m6 l6 xl6">
                        <input
                            type="tel"
                            className="validate"
                            value={this.state.form.phone}
                            onChange={event => {
                                event.persist();
                                this.setState(previousState => {
                                    return {
                                        ...previousState,
                                        form: {
                                            ...previousState.form,
                                            phone: event.target.value
                                        }
                                    };
                                })}}
                        />
                        <label>Phone Number</label>
                    </div>
                    <div className="input-field col s12 m6 l6 xl6">
                        <input
                            type="email"
                            className="validate"
                            value={this.state.form.email}
                            onChange={event => {
                                event.persist();
                                this.setState(previousState => {
                                    return {
                                        ...previousState,
                                        form: {
                                            ...previousState.form,
                                            email: event.target.value
                                        }
                                    };
                                })}}
                        />
                        <label>Email</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12 m6 l6 xl6">
                        <input
                            type="password"
                            className="validate"
                            value={this.state.form.password}
                            onChange={event => {
                                event.persist();
                                this.setState(previousState => {
                                    return {
                                        ...previousState,
                                        form: {
                                            ...previousState.form,
                                            password: event.target.value
                                        }
                                    };
                                })}}
                        />
                        <label>Enter Password</label>
                    </div>
                    <div className="input-field col s12 m6 l6 xl6">
                        <input
                            type="password"
                            className="validate"
                            value={this.state.form.repeat_password}
                            onChange={event => {
                                event.persist();
                                this.setState(previousState => {
                                    return {
                                        ...previousState,
                                        form: {
                                            ...previousState.form,
                                            repeat_password: event.target.value
                                        }
                                    };
                                })}}
                        />
                        <label>Retype Password</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12 m6 l6 xl6">
                        <input
                            type="text"
                            className="validate"
                            value={this.state.form.jid}
                            onChange={event => {
                                event.persist();
                                this.setState(previousState => {
                                    return {
                                        ...previousState,
                                        form: {
                                            ...previousState.form,
                                            jid: event.target.value
                                        }
                                    };
                                })}}
                        />
                        <label>Refferal Person Jabrol ID (optional)</label>
                    </div>
                    <div className="col s12 m6 l6 xl6 radiomargin">
                        <p className="rbtn-label">Account Type</p>

                        <Input
                            name='user_type'
                            type='radio'
                            value='customer'
                            label='Customer'
                            checked={this.state.form.customer}
                            onChange={event => {
                                this.setState(previousState => {
                                    return {
                                        ...previousState,
                                        form: {
                                            ...previousState.form,
                                            customer: !this.state.form.customer,
                                            business: this.state.form.customer
                                        }
                                    };
                                })}}
                        />

                        <Input
                            name='user_type'
                            type='radio'
                            value='business'
                            label='Business Account'
                            checked={this.state.form.business}
                            onChange={event => {
                                this.setState(previousState => {
                                    return {
                                        ...previousState,
                                        form: {
                                            ...previousState.form,
                                            customer: this.state.form.business,
                                            business: !this.state.form.business
                                        }
                                    };
                                })}}
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col s12">
                        <div className="section">
                            <Input
                                name='accept'
                                type='checkbox'
                                value='green'
                                label={'I Agree to Jabrool '}
                                className='filled-in'
                                checked={this.state.form.accept}
                                onClick={event => {
                                    this.setState(previousState => {
                                        return {
                                            ...previousState,
                                            form: {
                                                ...previousState.form,
                                                accept: !this.state.form.accept
                                            }
                                        };
                                    })}}
                            />
                            <a href="terms.html" className="green-text">Terms &amp; Conditions</a> and
                            <a href="privacypolicy.html" className="green-text"> Privacy Policy</a>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col s12">
                        <button
                            onClick={() => {
                                this._send();
                            }}
                            className="btn waves-effect waves-light teal lighten-1 standartmargin">REGISTER</button>
                    </div>
                </div>
                <div className="row">
                    <div className="col s12">
                        <p className="blue-grey-text center">INFO: If you want register as couries, please
                            <a href="/download" className="green-text">Download the App</a>
                        </p>
                    </div>
                </div>
                <ToastContainer />
            </div>
        );
    }

}

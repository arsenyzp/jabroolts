import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import socket from "./socket";
import order from "./order";

export default combineReducers({
    routing: routerReducer,
    socket: socket,
    order: order
});

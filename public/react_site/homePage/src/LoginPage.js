import React, {Component} from 'react';
import { render } from 'react-dom';
import { ToastContainer, toast } from 'react-toastify';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { Router, Route, hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import reducer from './reducers';
import axios from "axios/index";

export const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));
export const history = syncHistoryWithStore(hashHistory, store);

export default class LoginPage extends Component {

    constructor(props){
        super(props);
        this.state = {
            loading: true,
            form: {
                email: "",
                password: ""
            }
        };
    }

    async _send() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.post(`/site/login`, this.state.form);
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                this.setState(previousState => {
                    return {
                        ...previousState,
                        loading: false,
                        form: {
                            phone: "",
                            password: ""
                        }
                    };
                }, () => {
                    toast.success("Login success!");
                    setTimeout(() => {
                        document.location.href = "/account";
                    }, 1000);
                });
            }
        } catch (e) {
            console.log(e.response);
            if(!!e.response && !!e.response.data.errors && e.response.data.errors.length > 0) {
                e.response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                toast.error(e.message);
            }
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
        }
    }

    render() {
        return (
            <div>
                <div className={"row"}>
                    <div className={"input-field col col s10 offset-s1"}>
                        <input
                            type="email"
                            className={"validate"}
                            value={this.state.form.email}
                            onChange={event => {
                                event.persist();
                                this.setState(previousState => {
                                    return {
                                        ...previousState,
                                        form: {
                                            ...previousState.form,
                                            email: event.target.value
                                        }
                                    };
                                })}}
                        />
                        <label >Email</label>
                    </div>
                    <div className={"input-field col s10 offset-s1"}>
                        <input
                            type="password"
                            className={"validate"}
                            value={this.state.form.password}
                            onChange={event => {
                                event.persist();
                                this.setState(previousState => {
                                    return {
                                        ...previousState,
                                        form: {
                                            ...previousState.form,
                                            password: event.target.value
                                        }
                                    };
                                })}}
                        />
                        <label>Password</label>
                    </div>
                </div>
                <div className={"row center"}>
                    <div className={"col s12"}>
                        <button
                            className={"btn waves-effect waves-light teal lighten-1 standartmargin"}
                            onClick={() => {
                                this._send();
                            }}
                        >LOGIN</button>
                    </div>
                </div>
            </div>
        );
    }

}

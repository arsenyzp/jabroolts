import React, {Component} from 'react';
import { render } from 'react-dom';
import { ToastContainer, toast } from 'react-toastify';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { Router, Route, hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import reducer from './reducers';
import CreateRequest from "./components/CreateRequest";
import RequestDetails from "./components/RequestDetails";
import {connect} from "react-redux";
import PersonalData from "./components/PersonalData";
import {setOrderStep} from "./actions/orderActons";

export const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));
export const history = syncHistoryWithStore(hashHistory, store);

class CreateOrderPage extends Component {

    constructor(props){
        super(props);
        this.state = {
            loading: true,
        };
    }

    render() {
        return (
            <div className="row">
                {this.props.order.step === 1 && <CreateRequest/>}
                {this.props.order.step === 2 && <RequestDetails/>}
                {this.props.order.step === 3 && <PersonalData/>}
            </div>
        );
    }

}

function mapStateToProps(state) {
    return {
        order: state.order
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setOrderStep: (step) => {
            dispatch(setOrderStep(step));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateOrderPage);

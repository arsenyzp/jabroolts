import React, {Component} from 'react';
import { render } from 'react-dom';
import { ToastContainer, toast } from 'react-toastify';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { Router, Route, hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import reducer from './reducers';
import HomePage from "./HomePage";
import LoginPage from "./LoginPage";
import RegisterPage from "./RegisterPage";
import CreateOrderPage from "./CreateOrderPage";

export const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));
export const history = syncHistoryWithStore(hashHistory, store);

if (document.getElementById('contact_us')) {
    render(
        <HomePage/>,
        document.getElementById('contact_us')
    );
}

if (document.getElementById('login_form')) {
    render(
        <LoginPage/>,
        document.getElementById('login_form')
    );
}

if (document.getElementById('register_form')) {
    render(
        <RegisterPage/>,
        document.getElementById('register_form')
    );
}

if (document.getElementById('order_form')) {
    render(
        <Provider store={store}>
            <CreateOrderPage/>
        </Provider>,
        document.getElementById('order_form')
    );
}

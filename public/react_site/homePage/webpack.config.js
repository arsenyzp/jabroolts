const path = require('path');
module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve('../../dist_site'),
        filename: 'site_home_bundle.js'
    },
    module: {
        loaders: [
            { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/, query:
                {
                    presets:['es2015', 'react', 'stage-0'],
                    plugins: [
                        'transform-runtime',
                    ],
                }
            },
            { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/, query:
                {
                    presets:['es2015', 'react', 'stage-0'],
                    plugins: [
                        'transform-runtime',
                    ],
                }
            },
            { test: /\.css$/, use: [ 'style-loader', 'css-loader' ] },
            {
                test: /\.scss$/,
                use: [{
                    loader: "style-loader" // creates style nodes from JS strings
                    }, {
                        loader: "css-loader" // translates CSS into CommonJS
                    }, {
                        loader: "sass-loader" // compiles Sass to CSS
                    }
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    {
                        loader: 'file-loader',
                        options: {
                            query: {
                                name:'assets/[name].[ext]'
                            },
                            publicPath: '/dist_site/'
                        }
                    },
                    {
                        loader: 'image-webpack-loader',
                        options: {
                            query: {
                                mozjpeg: {
                                    progressive: true,
                                },
                                gifsicle: {
                                    interlaced: true,
                                },
                                optipng: {
                                    optimizationLevel: 7,
                                }
                            },
                            publicPath: '/dist_site/'
                        }
                    }]
            },
            {
                test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            publicPath: '/dist_site/'
                        }
                    }
                ]
            }
        ]
    },
    devtool: "source-map",
};

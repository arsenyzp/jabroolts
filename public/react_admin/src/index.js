import 'jquery';
import 'moment';

import 'gentelella/vendors/fastclick/lib/fastclick.js';
import 'gentelella/vendors/nprogress/nprogress.js';
import 'gentelella/vendors/Chart.js/dist/Chart.min.js';
import 'gentelella/vendors/gauge.js/dist/gauge.min.js';
import 'gentelella/vendors/iCheck/icheck.min.js';

import 'gentelella/vendors/Flot/jquery.flot.js';
import 'gentelella/vendors/Flot/jquery.flot.pie.js';
import 'gentelella/vendors/Flot/jquery.flot.time.js';
import 'gentelella/vendors/Flot/jquery.flot.stack.js';
import 'gentelella/vendors/Flot/jquery.flot.resize.js';
import 'gentelella/vendors/flot.orderbars/js/jquery.flot.orderBars.js';
import 'gentelella/vendors/flot-spline/js/jquery.flot.spline.min.js';

import '../../js/admin/ui/theme.js';

import React, {Component} from 'react';
import { render } from 'react-dom';
import { ToastContainer, toast } from 'react-toastify';

import 'gentelella/vendors/bootstrap/dist/css/bootstrap.min.css';
import 'gentelella/vendors/font-awesome/css/font-awesome.min.css';
import 'gentelella/vendors/nprogress/nprogress.css';
import 'gentelella/vendors/iCheck/skins/flat/green.css';
import 'gentelella/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css';
import 'gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css';
import 'gentelella/build/css/custom.min.css';

import 'react-select/dist/react-select.css';
import './styles/main.css';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { Router, Route, hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import reducer from './reducers';

export const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));
export const history = syncHistoryWithStore(hashHistory, store);

import MainPage from "./pages/MainPage";
import LeftMenu from "./components/LeftMenu";
import TopMenu from "./components/TopMenu";
import UserPage from "./pages/UserPage";
import UserFormPage from "./pages/UserFormPage";
import UserBank from "./pages/UserBank";
import UserVehicle from "./pages/UserVehicle";
import UserLicense from "./pages/UserLicense";
import UserMessages from "./pages/UserMessages";
import NewCouriers from "./pages/NewCouriers";
import PromoPage from "./pages/PromoPage";
import ConfigPackageSizeRatioPage from "./pages/configs/ConfigPackageSizeRatioPage";
import ManufacturerPage from './pages/ManufacturerPage';
import ManufacturerFormPage from "./pages/ManufacturerFormPage";
import PromoFormPage from "./pages/PromoFormPage";
import ContactUsPage from "./pages/ContactUsPage";
import ContactUsViewPage from "./pages/ContactUsViewPage";
import PaymentLogPage from "./pages/PaymentLogPage";
import ConfigPackagePricePage from "./pages/configs/ConfigPackagePricePage";
import ConfigRadiusPage from "./pages/configs/ConfigRadiusPage";
import PaymentLogViewPage from "./pages/PaymentLogViewPage";
import DeliveryParamsPage from "./pages/configs/DeliveryParamsPage";
import CancelConfigPage from "./pages/configs/CancelConfigPage";
import ConfigLogsPage from './pages/configs/ConfigLogsPage';
import ConfigPricePage from './pages/configs/ConfigPricePage';
import ConfigTermsPage from './pages/configs/ConfigTermsPage';
import ConfigBonusPage from './pages/configs/ConfigBonusPage';
import CountriesPage from './pages/CountriesPage';
import CountryFormPage from './pages/CountryFormPage';
import BanksPage from './pages/BanksPage';
import BankFormPage from './pages/BankFormPage';

import ApiLogsPage from './pages/logs/ApiLogsPage';
import ApiLogViewPage from './pages/logs/ApiLogViewPage';
import EmailLogsPage from './pages/logs/EmailLogsPage';
import SmsLogsPage from './pages/logs/SmsLogsPage';
import EmailLogViewPage from "./pages/logs/EmailLogViewPage";
import SmsLogViewPage from "./pages/logs/SmsLogsViewPage";
import BanksTypesPage from "./pages/BankTypesPage";
import BankTypesFormPage from "./pages/BankTypesFormPage";
import CouriersRPage from "./pages/reports/CouriersRPage";
import NewUsersPage from "./pages/NewUsersPage";
import SubscriptionsPage from "./pages/SubscriptionsPage";
import AdminProfilePage from "./pages/AdminProfilePage";
import CouriorClustersPage from "./pages/reports/CouriorClustersPage";
import CustomerClustersPage from "./pages/reports/CustomerClustersPage";
import CustomersRPage from "./pages/reports/CustomersRPage";
import HoursReportPage from "./pages/reports/HoursReportPage";
import OrdersReportPage from "./pages/reports/OrdersReportPage";
import UserOrders from "./pages/UserOrders";
import OrderDetailPage from "./pages/OrderDetailPage";
import {SocketService} from "./components/SocketService";
import {removeNotifications} from "./actions/notificationActions";
import PaymentListCourierPage from "./pages/PaymentListCourierPage";
import PaymentListCustomerPage from "./pages/PaymentListCustomerPage";
import NotificationBar from "./components/NotificationBar";
import ActiveUsersPage from "./pages/stat/ActiveUsersPage";
import ActiveOrdersPage from "./pages/stat/ActiveOrdersPage";
import MissedOrdersPage from "./pages/stat/MissedOrdersPage";
import BusinessCouriersPage from "./pages/BusinessCouriersPage";
import UserBusinessPage from "./pages/UserBusinessPage";
import AdminLogin from "./components/AdminLogin";

class App extends Component {

    constructor(props){
        super(props);
        this.state = {
            loading: true,
            show: false,
            selectedUser: null,
            lightboxIsOpen: false,
            notifications: []
        };
        SocketService.init();
    }

    componentDidMount(){
        $(document).trigger('TTinit');
        $("#preload").fadeOut('slow');
    }

    componentWillReceiveProps(newProps){
        console.log("App");
        console.log(newProps);
    }

    render() {
        return (
            <div className="App">
                <LeftMenu/>
                <TopMenu/>
                <div className={"right_col"} role="main">

                    <NotificationBar/>

                    <Provider store={store}>
                        <Router history={history}>
                            <Route path="/" component={MainPage}/>
                            <Route path="/users" component={UserPage}/>
                            <Route path="/business" component={UserBusinessPage}/>
                            <Route path="/admin/profile" component={AdminProfilePage}/>
                            <Route path="/newUsers" component={NewUsersPage}/>
                            <Route path="/users/:id" component={UserFormPage}/>
                            <Route path="/businessCouriers/:id" component={BusinessCouriersPage}/>
                            <Route path="/users/:id/:n" component={UserFormPage}/>
                            <Route path="/userBank/:id" component={UserBank}/>
                            <Route path="/userVehicle/:id" component={UserVehicle}/>
                            <Route path="/userLicense/:id" component={UserLicense}/>
                            <Route path="/userMessages/:id" component={UserMessages}/>
                            <Route path="/userOrders/:id/:courier" component={UserOrders}/>

                            <Route path="/newCouriers" component={NewCouriers}/>

                            <Route path="/subscriptions" component={SubscriptionsPage}/>

                            <Route path="/promo" component={PromoPage}/>
                            <Route path="/promo/:id" component={PromoFormPage}/>

                            <Route path="/manufacturers" component={ManufacturerPage}/>
                            <Route path="/manufacturers/:id" component={ManufacturerFormPage}/>

                            <Route path="/contact_us" component={ContactUsPage}/>
                            <Route path="/contact_us/:id" component={ContactUsViewPage}/>

                            <Route path="/payments" component={PaymentLogPage}/>
                            <Route path="/payments/:id" component={PaymentLogViewPage}/>

                            <Route path="/countries" component={CountriesPage}/>
                            <Route path="/countries/:id" component={CountryFormPage}/>

                            <Route path="/banks" component={BanksPage}/>
                            <Route path="/banks/:id" component={BankFormPage}/>

                            <Route path="/bank_types" component={BanksTypesPage}/>
                            <Route path="/bank_types/:id" component={BankTypesFormPage}/>

                            <Route path="/configPackageSizeRatio" component={ConfigPackageSizeRatioPage}/>
                            <Route path="/configPackages" component={ConfigPackagePricePage}/>
                            <Route path="/configRadius" component={ConfigRadiusPage}/>
                            <Route path="/configDelivery" component={DeliveryParamsPage}/>
                            <Route path="/configCancel" component={CancelConfigPage}/>
                            <Route path="/configPrice" component={ConfigPricePage}/>
                            <Route path="/configLogs" component={ConfigLogsPage}/>
                            <Route path="/configTerms" component={ConfigTermsPage}/>
                            <Route path="/configBonuses" component={ConfigBonusPage}/>

                            <Route path="/logs/api" component={ApiLogsPage}/>
                            <Route path="/logs/api/:id" component={ApiLogViewPage}/>

                            <Route path="/logs/email" component={EmailLogsPage}/>
                            <Route path="/logs/email/:id" component={EmailLogViewPage}/>

                            <Route path="/logs/sms" component={SmsLogsPage}/>
                            <Route path="/logs/sms/:id" component={SmsLogViewPage}/>

                            <Route path="/reports/couriers" component={CouriersRPage}/>
                            <Route path="/reports/couriers_clusters" component={CouriorClustersPage}/>
                            <Route path="/reports/customers_clusters" component={CustomerClustersPage}/>
                            <Route path="/reports/customers" component={CustomersRPage}/>
                            <Route path="/reports/hours" component={HoursReportPage}/>
                            <Route path="/reports/orders" component={OrdersReportPage}/>

                            <Route path="/order/:id" component={OrderDetailPage}/>

                            <Route path="/payments_courier" component={PaymentListCourierPage}/>
                            <Route path="/payments_customer" component={PaymentListCustomerPage}/>

                            <Route path="/stat/users" component={ActiveUsersPage}/>
                            <Route path="/stat/active_orders" component={ActiveOrdersPage}/>
                            <Route path="/stat/missed_orders" component={MissedOrdersPage}/>

                        </Router>
                    </Provider>
                </div>
                <ToastContainer />
            </div>
    );
    }

}

if (document.getElementById('root')) {
    render(
        <App />,
        document.getElementById('root')
    );
}

if (document.getElementById('adminLogin')) {
    render(
        <AdminLogin />,
        document.getElementById('adminLogin')
    );
}

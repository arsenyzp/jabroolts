import React, {Component} from 'react';
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';

const style = {
    width: '100%',
    height: '500px'
};

export class MyMapComponent extends Component {

    componentWillReceiveProps(newProps){
        console.log(newProps);
    }

    render() {
        return (
            <Map
                google={window.google}
                style={style}
                initialCenter={{
                    lat: parseFloat(this.props.lat),
                    lng: parseFloat(this.props.lng)
                }}
                zoom={8}
            >
                {this.props.items.map((v, k, a) => {
                    return (
                        <Marker
                            key={k}
                            title={'The marker`s title will appear as a tooltip.'}
                            name={'SOMA'}
                            position={{lat: parseFloat(v.lat), lng: parseFloat(v.lon)}} />
                    );
                })}
            </Map>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: ("AIzaSyD4eP_ajOi49HNd_GYGOfMu1qUvElw5DnU")
})(MyMapComponent)
import React, {Component} from 'react';

export default class LeftMenu extends Component{

    render() {
        return (
            <div className={"col-md-3 left_col"}>
                <div className={"left_col scroll-view"}>
                    <div className={"navbar nav_title"} style={{border: 0}}>
                        <a href="#/" className={"site_title"}><i className={"fa fa-paw"}></i> <span>Jabrool</span></a>
                    </div>

                    <div className={"clearfix"}></div>

                    <div className={"profile clearfix"}>
                        <div className={"profile_info"}>
                            <h2></h2>
                        </div>
                    </div>

                    <br />

                    <div id="sidebar-menu" className={"main_menu_side hidden-print main_menu"}>
                        <div className={"menu_section"}>
                            <h3>General</h3>
                            <ul className={"nav side-menu"}>
                                <li><a><i className={"fa fa-home"}></i> Users <span className={"fa fa-chevron-down"}/></a>
                                    <ul className={"nav child_menu"}>
                                        <li><a href="#/newUsers">New users</a></li>
                                        <li><a href="#/users">All users</a></li>
                                        <li><a href="#/stat/users">Active users</a></li>
                                        <li><a href="#/business">Business users</a></li>
                                    </ul>
                                </li>
                                <li><a><i className={"fa fa-home"}></i> Orders <span className={"fa fa-chevron-down"}/></a>
                                    <ul className={"nav child_menu"}>
                                        <li><a href="#/stat/active_orders">Active orders</a></li>
                                        <li><a href="#/stat/missed_orders">Unanswered requests</a></li>
                                    </ul>
                                </li>
                                <li><a><i className={"fa fa-edit"}/> Configs <span className={"fa fa-chevron-down"}/></a>
                                    <ul className={"nav child_menu"}>
                                        <li><a href="#/configPackages">Package configurations</a></li>
                                        <li><a href="#/configPackageSizeRatio">Package size ratio</a></li>
                                        <li><a href="#/configRadius">Radius</a></li>
                                        <li><a href="#/configDelivery">Delivery Parametrs</a></li>
                                        <li><a href="#/configCancel">Cancel request params</a></li>
                                        <li><a href="#/configPrice">Price configurations</a></li>
                                        <li><a href="#/configBonuses">Bonuses</a></li>
                                        <li><a href="#/configLogs">Logs</a></li>
                                        <li><a href="#/configTerms">Terms and conditions</a></li>
                                    </ul>
                                </li>
                                <li><a><i className={"fa fa-desktop"}/> Service <span className={"fa fa-chevron-down"}/></a>
                                    <ul className={"nav child_menu"}>
                                        <li><a href="#/promo">Promo</a></li>
                                        <li><a href="#/subscriptions">Subscribers</a></li>
                                        <li><a href="#/contact_us">Contact us</a></li>
                                    </ul>
                                </li>
                                <li><a><i className={"fa fa-desktop"}/> Payments <span className={"fa fa-chevron-down"}/></a>
                                    <ul className={"nav child_menu"}>
                                        <li><a href="#/payments_customer">Customer</a></li>
                                        <li><a href="#/payments_courier">Courier</a></li>
                                    </ul>
                                </li>

                                <li><a><i className={"fa fa-table"}/> Catalogs <span className={"fa fa-chevron-down"}/></a>
                                    <ul className={"nav child_menu"}>
                                        <li><a href="#/manufacturers">Car manufacturers</a></li>
                                        <li><a href="#/countries">Countries</a></li>
                                        <li><a href="#/bank_types">Bank types</a></li>
                                        <li><a href="#/banks">Banks</a></li>
                                    </ul>
                                </li>
                                <li><a><i className={"fa fa-clone"}/>Reports <span className={"fa fa-chevron-down"}/></a>
                                    <ul className={"nav child_menu"}>
                                        <li><a href="#/reports/couriers">Couriers</a></li>
                                        <li><a href="#/reports/customers">Customers</a></li>
                                        <li><a href="#/reports/hours">Hours</a></li>
                                        <li><a href="#/reports/orders">Orders</a></li>
                                        <li><a href="#/reports/couriers_clusters">Courier clusters</a></li>
                                        <li><a href="#/reports/customers_clusters">Customers clusters</a></li>
                                    </ul>
                                </li>
                                <li><a><i className={"fa fa-bar-chart-o"}/> Logs <span className={"fa fa-chevron-down"}/></a>
                                    <ul className={"nav child_menu"}>
                                        <li><a href="#/logs/api">Api</a></li>
                                        <li><a href="#/logs/email">Email</a></li>
                                        <li><a href="#/logs/sms">Sms</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        {/*<div className={"menu_section"}>*/}
                            {/*<h3>Live On</h3>*/}
                            {/*<ul className={"nav side-menu"}>*/}
                                {/*<li><a><i className={"fa fa-bug"}/> Additional Pages <span className={"fa fa-chevron-down"}/></a>*/}
                                    {/*<ul className={"nav child_menu"}>*/}
                                        {/*<li><a href="e_commerce.html">E-commerce</a></li>*/}
                                        {/*<li><a href="projects.html">Projects</a></li>*/}
                                        {/*<li><a href="project_detail.html">Project Detail</a></li>*/}
                                        {/*<li><a href="contacts.html">Contacts</a></li>*/}
                                        {/*<li><a href="profile.html">Profile</a></li>*/}
                                    {/*</ul>*/}
                                {/*</li>*/}
                                {/*<li><a><i className={"fa fa-windows"}/> Extras <span className={"fa fa-chevron-down"}/></a>*/}
                                    {/*<ul className={"nav child_menu"}>*/}
                                        {/*<li><a href="page_403.html">403 Error</a></li>*/}
                                        {/*<li><a href="page_404.html">404 Error</a></li>*/}
                                        {/*<li><a href="page_500.html">500 Error</a></li>*/}
                                        {/*<li><a href="plain_page.html">Plain Page</a></li>*/}
                                        {/*<li><a href="login.html">Login Page</a></li>*/}
                                        {/*<li><a href="pricing_tables.html">Pricing Tables</a></li>*/}
                                    {/*</ul>*/}
                                {/*</li>*/}
                                {/*<li><a><i className={"fa fa-sitemap"}/> Multilevel Menu <span className={"fa fa-chevron-down"}/></a>*/}
                                    {/*<ul className={"nav child_menu"}>*/}
                                        {/*<li><a href="#level1_1">Level One</a>*/}
                                            {/*<ul>*/}
                                                {/*<li><a>Level One<span className={"fa fa-chevron-down"}/></a>*/}
                                                    {/*<ul className={"nav child_menu"}>*/}
                                                        {/*<li className={"sub_menu"}><a href="level2.html">Level Two</a>*/}
                                                        {/*</li>*/}
                                                        {/*<li><a href="#level2_1">Level Two</a>*/}
                                                        {/*</li>*/}
                                                        {/*<li><a href="#level2_2">Level Two</a>*/}
                                                        {/*</li>*/}
                                                    {/*</ul>*/}
                                                {/*</li>*/}
                                                {/*<li><a href="#level1_2">Level One</a></li>*/}
                                            {/*</ul>*/}
                                        {/*</li>*/}
                                    {/*</ul>*/}
                                {/*</li>*/}
                                {/*<li><a href="javascript:void(0)"><i className={"fa fa-laptop"}/> Landing Page <span className={"label label-success pull-right"}>Coming Soon</span></a></li>*/}
                            {/*</ul>*/}
                        {/*</div>*/}

                    </div>

                    <div className={"sidebar-footer hidden-small"}>
                        {/*<a data-toggle="tooltip" data-placement="top" title="Settings" href="#/admin/profile">*/}
                            {/*<span className={"glyphicon glyphicon-cog"} aria-hidden="true"/>*/}
                        {/*</a>*/}
                        {/*<a data-toggle="tooltip" data-placement="top" title="FullScreen">*/}
                            {/*<span className={"glyphicon glyphicon-fullscreen"} aria-hidden="true"/>*/}
                        {/*</a>*/}
                        {/*<a data-toggle="tooltip" data-placement="top" title="Lock">*/}
                            {/*<span className={"glyphicon glyphicon-eye-close"} aria-hidden="true"/>*/}
                        {/*</a>*/}
                        {/*<a data-toggle="tooltip" data-placement="top" title="Logout" href="/admin/logout">*/}
                            {/*<span className={"glyphicon glyphicon-off"} aria-hidden="true"/>*/}
                        {/*</a>*/}
                    </div>

                </div>
            </div>
        );
    }

}

import React, {Component} from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { RingLoader } from 'react-spinners';
import axios from 'axios';

const chart_plot_01_settings = {
    series: {
        lines: {
            show: false,
            fill: true
        },
        splines: {
            show: true,
            tension: 0.4,
            lineWidth: 1,
            fill: 0.4
        },
        points: {
            radius: 0,
            show: true
        },
        shadowSize: 2
    },
    grid: {
        verticalLines: true,
        hoverable: true,
        clickable: true,
        tickColor: "#d5d5d5",
        borderWidth: 1,
        color: '#fff'
    },
    colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
    xaxis: {
        tickColor: "rgba(51, 51, 51, 0.06)",
        mode: "time",
        tickSize: [1, "day"],
        //tickLength: 10,
        axisLabel: "Date",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 12,
        axisLabelFontFamily: 'Verdana, Arial',
        axisLabelPadding: 10
    },
    yaxis: {
        ticks: 8,
        tickColor: "rgba(51, 51, 51, 0.06)",
    },
    tooltip: true
};

export default class ActivityUsers extends Component{

    constructor(props) {
        super(props);
        this.state = {
            loading: true
        };
    }

    componentDidMount() {
        this._loadList();
    }

    async _loadList() {
        this.setState({
            loading: true,
        });
        try {
            let response = await axios.get(`/admin/statistics/users`, {
                params: {}
            });
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            }

            this.setState({
                loading: false,
            });

            let a1 = [];
            let a2 = [];
            response.data.data.map((v, k, a) => {
                a1.push([v.created_at, v.customers]);
                a2.push([v.created_at, v.couriers]);
            });

            if ($("#chart_plot_01").length){
                console.log('Plot1');

                $.plot( $("#chart_plot_01"), [ a1, a2 ],  chart_plot_01_settings );
            }

        } catch (e) {
            this.setState({
                loading: false,
            });
            toast.error(e.message);
        }
    }

    render() {
        return (
            <div className={"dashboard_graph"}>

                <div className={"row x_title"}>
                    <div className={"col-md-6"}>
                        <h3>Active Users</h3>
                    </div>
                    <div className={"col-md-6"}>

                    </div>
                </div>

                <div className={"col-lg-12"}>
                    <div id="chart_plot_01" className={"demo-placeholder"}/>
                </div>

                {this.state.loading && <div className={"css-jxiqlq-wrap"}>
                    <RingLoader
                        style={{margin: 'auto'}}
                        color={'#0019bc'}
                        loading={this.state.loading}
                    />
                </div>}

                <div className={"clearfix"}/>
            </div>
        );
    }
}
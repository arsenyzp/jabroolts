import React, {Component} from 'react';
import { render } from 'react-dom';
import {Button, Modal, Alert} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';

export default class TopMenu extends Component{

    render() {
        return (
            <div className={"top_nav"}>
                <div className={"nav_menu"}>
                    <nav>
                        <div className={"nav toggle"}>
                            <a id={"menu_toggle"}><i className={"fa fa-bars"}></i></a>
                        </div>

                        <ul className={"nav navbar-nav navbar-right"}>
                            <li className={""}>
                                <a href="javascript:;" className={"user-profile dropdown-toggle"} data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/img.jpg" alt="" />
                                    <span className={" fa fa-angle-down"}></span>
                                </a>
                                <ul className={"dropdown-menu dropdown-usermenu pull-right"}>
                                    <li><a href="#/admin/profile"><i className={"fa fa-sign-out pull-right"}></i> My profile</a></li>
                                    <li><a href="/admin/logout"><i className={"fa fa-sign-out pull-right"}></i> Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        );
    }

}
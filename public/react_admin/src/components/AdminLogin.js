import React, {Component} from 'react';
import {render} from 'react-dom';
import {ToastContainer, toast} from 'react-toastify';
import axios from "axios/index";

export default class AdminLogin extends Component{

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        };
    }

    async submit () {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });

        try {
            let response = await axios.post(`/admin/login`, this.state);
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                this.setState(previousState => {
                    return {
                        ...previousState,
                        loading: false,
                        email: '',
                        password: ''
                    };
                }, () => {
                    toast.success("Login success");
                    setTimeout(() => {
                        document.location.href = '/admin';
                    }, 1000);
                });
            }
        } catch (e) {
            console.log(e.response);
            if(!!e.response && !!e.response.data.errors && e.response.data.errors.length > 0) {
                e.response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                toast.error(e.message);
            }
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
        }
    }

    render() {
        return (
            <div className="form-signin">
                <h2 className="form-signin-heading">Please sign in</h2>
                <label className="sr-only">Email address</label>
                <input
                    type="email"
                    className="form-control"
                    placeholder="Email address"
                    required
                    autoFocus
                    value={this.state.email}
                    onChange={event => {
                        event.persist();
                        this.setState(previousState => {
                            return {
                                ...previousState,
                                email: event.target.value
                            };
                        })}}
                />
                    <label className="sr-only">Password</label>
                    <input
                        type="password"
                        className="form-control"
                        placeholder="Password"
                        required
                        value={this.state.password}
                        onChange={event => {
                            event.persist();
                            this.setState(previousState => {
                                return {
                                    ...previousState,
                                    password: event.target.value
                                };
                            })}}
                    />
                        <button className="btn btn-lg btn-primary btn-block" onClick={() => {
                            this.submit();
                        }}>Sign in</button>
                <ToastContainer />
            </div>
    );
    }

    }

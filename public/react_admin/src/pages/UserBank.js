import React, {Component} from 'react';
import { render } from 'react-dom';
import {Button, Modal, Alert, Pagination} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { RingLoader } from 'react-spinners';
import axios from 'axios';
import { Link } from 'react-router';
import 'icheck/skins/all.css';
import {Checkbox, Radio} from 'react-icheck';
import {history} from '../index';

export default class UserBank extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            id: props.params.id,
            user: {jabroolid: ""},
            banks: [],
            countries: [],
            types: [],
            ready: false
        };
    }

    componentDidMount() {
        this._load();
    }

    async _load() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.get(`/admin/user/bank/${this.state.id}`, {
                params: {}
            });
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            }

            this.state.user = response.data.data.user;
            if(!this.state.user.bank){
                this.state.user.bank = {};
            }
            if(!this.state.user.bank.bank || this.state.user.bank.bank === ''){
                this.state.user.bank.bank =  response.data.data.banks[0]._id;
            }
            if(!this.state.user.bank.country_code || this.state.user.bank.country_code === ''){
                this.state.user.bank.country_code =  response.data.data.countries[0].code;
            }
            if(!this.state.user.bank.type || this.state.user.bank.type === ''){
                this.state.user.bank.type =  response.data.data.types[0]._id;
            }

            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false,
                    user: this.state.user,
                    banks: response.data.data.banks,
                    countries: response.data.data.countries,
                    types: response.data.data.types,
                    ready: true
                };
            });

        } catch (e) {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
            toast.error(e.message);
        }
    }

    async _save() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.post(`/admin/user/bank/${this.state.id}`, this.state.user.bank);
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                this.setState(previousState => {
                    return {
                        ...previousState,
                        loading: false,
                        user: {
                            ...previousState.user,
                            ...response.data.data
                        }
                    };
                }, () => {
                    toast.success("Item updated!");
                });
            }
        } catch (e) {
            console.log(e.response);
            if(!!e.response && !!e.response.data.errors && e.response.data.errors.length > 0) {
                e.response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                toast.error(e.message);
            }
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
        }
    }

    render() {
        return (
            <div>
                <div className={"row"}>
                    <div className={"col-md-12 col-sm-12 col-xs-12"}>
                        <div className={"x_panel"}>
                            <div className={"x_title"}>
                                <h2>User Bank <small>{this.state.user.jabroolid} (Payment default from: {this.state.user.pay_type})</small></h2>
                                <div className={"clearfix"}></div>
                            </div>
                            <div className={"x_content"}>
                                <br />
                                <form data-parsley-validate className={"form-horizontal form-label-left"}>

                                    {this.state.ready && !!this.state.user.bank && <div className={"row"}>

                                        <div className={"col-md-6 col-sm-6 col-xs-6"}>
                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Branch <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        type="text"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.user.bank.branch}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.user,
                                                                        bank: {
                                                                            ...previousState.user.bank,
                                                                            branch: event.target.value
                                                                        }
                                                                    }
                                                                };
                                                            })}}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Account number <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        type="text"
                                                        name="last-name"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.user.bank.account_number}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.user,
                                                                        bank: {
                                                                            ...previousState.user.bank,
                                                                            account_number: event.target.value
                                                                        }
                                                                    }
                                                                };
                                                            })}}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Holder name</label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        type="text"
                                                        name="email"
                                                        value={this.state.user.bank.holder_name}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.user,
                                                                        bank: {
                                                                            ...previousState.user.bank,
                                                                            holder_name: event.target.value
                                                                        }
                                                                    }
                                                                };
                                                            })}}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Account type <span className={"required"}>*</span></label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <select
                                                        className={"form-control"}
                                                        value={this.state.user.bank.type}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.user,
                                                                        bank: {
                                                                            ...previousState.user.bank,
                                                                            type: event.target.value
                                                                        }
                                                                    }
                                                                };
                                                            })}}
                                                    >
                                                        { this.state.types.map((v, k, a) => {
                                                            return (
                                                                <option value={v._id} key={k}>{v.name}</option>
                                                            );
                                                        })}
                                                    </select>
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Bank <span className={"required"}>*</span></label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <select
                                                        className={"form-control"}
                                                        value={this.state.user.bank.bank}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.user,
                                                                        bank: {
                                                                            ...previousState.user.bank,
                                                                            bank: event.target.value
                                                                        }
                                                                    }
                                                                };
                                                            })}}
                                                    >
                                                        { this.state.banks.map((v, k, a) => {
                                                            return (
                                                                <option value={v._id} key={k}>{v.name}</option>
                                                            );
                                                        })}
                                                    </select>
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Country <span className={"required"}>*</span></label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <select
                                                        className={"form-control"}
                                                        value={this.state.user.bank.country_code}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.user,
                                                                        bank: {
                                                                            ...previousState.user.bank,
                                                                            country_code: event.target.value
                                                                        }
                                                                    }
                                                                };
                                                            })}}
                                                    >
                                                        { this.state.countries.map((v, k, a) => {
                                                            return (
                                                                <option value={v.code} key={k}>{v.name}</option>
                                                            );
                                                        })}
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        <div className={"col-md-6 col-sm-6 col-xs-6"}>

                                        </div>

                                    </div>}

                                    {this.state.ready && !this.state.user.bank && <h3>Bank info not found</h3> }

                                    <div className={"ln_solid"}></div>
                                    <div className={"form-group"}>
                                        <div className={"col-lg-6"}>
                                            <a className={"btn btn-primary"} href={"#/users/" + this.state.id}>Cancel</a>
                                            <Button onClick={this._save.bind(this)} bsStyle="success">Submit</Button>
                                        </div>
                                        <div className={"col-lg-6"}>

                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.loading && <div className={"css-jxiqlq-wrap"}>
                    <RingLoader
                        style={{margin: 'auto'}}
                        color={'#0019bc'}
                        loading={this.state.loading}
                    />
                </div>}
            </div>
        );
    }

}
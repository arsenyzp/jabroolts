import React, {Component} from 'react';
import { render } from 'react-dom';
import {Button, Modal, Alert, Pagination} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { RingLoader } from 'react-spinners';
import axios from 'axios';
import { Link } from 'react-router';
import 'icheck/skins/all.css';
import {Checkbox, Radio} from 'react-icheck';
import {history} from '../index';

export default class UserFormPage extends Component{
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            id: props.params.id,
            n: !!props.params.n,
            user: {
                first_name: "",
                last_name: "",
                email: "",
                pay_type: "",
                phone: "",
                status: "review",
                role: "user",
                type: "user",
                new_password: "",
                new_balance: 0,
                new_courierLimit: 0,
                visible: false,
                in_progress: false,
                jabroolFee: 0,
                avatar_url: ""
            }
        };
    }

    componentDidMount() {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

        if(!!this.state.id && this.state.id !== "new"){
            this._load();
        } else {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
        }
    }

    async _load() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.get(`/admin/user/${this.state.id}`, {
                params: {}
            });
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            }

            if(!!response.data.data.avatar && response.data.data.avatar !== "") {
                response.data.data.avatar_url = "/uploads/user_avatars/" + response.data.data.avatar;
            }
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false,
                    user: {
                        ...previousState.user,
                        ...response.data.data
                    }
                };
            });

        } catch (e) {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
            toast.error(e.message);
        }
    }

    async _preUpload(event) {
        if(this.fileInput.files.length === 0) {
            return;
        }
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        let formdata = new FormData();
        formdata.append('image', this.fileInput.files[0]);
        try {
            let response = await axios.post(`/admin/ajax/uploads_img`, formdata, {headers: {
                'Content-Type': 'multipart/form-data'
            }});
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                this.fileInput.value = '';
                this.setState(previousState => {
                    return {
                        ...previousState,
                        loading: false,
                        user: {
                            ...previousState.user,
                            avatar_url: response.data.data.image_url,
                            avatar: response.data.data.image
                        }
                    };
                });
            }
        } catch (e) {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
            toast.error(e.message);
        }
    }

    async _save() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            this.state.user.new_courierLimit = parseFloat(this.state.user.new_courierLimit);
            this.state.user.new_balance = parseFloat(this.state.user.new_balance);

            let response = await axios.post(`/admin/user/`, this.state.user);
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                if(this.state.id === "new") {
                    this.setState(previousState => {
                        return {
                            ...previousState,
                            loading: false,
                            user: {
                                ...previousState.user,
                                ...response.data.data
                            },
                            id: response.data.data._id
                        };
                    }, () => {
                        toast.success("Item created!");
                        history.push('/users/' + response.data.data._id);
                    });
                } else {
                    if(!!response.data.data.avatar && response.data.data.avatar !== "") {
                        response.data.data.avatar_url = "/uploads/user_avatars/" + response.data.data.avatar;
                    }
                    response.data.data.visible =  !!response.data.data.visible;
                    response.data.data.in_progress =  !!response.data.data.in_progress;
                    this.setState(previousState => {
                        return {
                            ...previousState,
                            loading: false,
                            user: {
                                ...previousState.user,
                                ...response.data.data
                            },
                            id: response.data.data._id
                        };
                    }, () => {
                        toast.success("Item updated!");
                    });
                }
            }
        } catch (e) {
            console.log(e.response);
            if(!!e.response && !!e.response.data.errors && e.response.data.errors.length > 0) {
                e.response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                toast.error(e.message);
            }
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
        }
    }

    async _resetFee() {
        if(confirm("Reset jabrool fee ?")){
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: true
                };
            });
            try {
                let response = await axios.post(`/admin/user/reset_jabrool_fee/${this.state.id}`, {});
                if(!!response.data.errors && response.data.errors.length > 0) {
                    response.data.errors.map((err, i) => {
                        toast.error(err);
                    });
                } else {
                    this.setState(previousState => {
                        return {
                            ...previousState,
                            loading: false,
                            user: {
                                ...previousState.user,
                                ...response.data.data
                            }
                        };
                    }, () => {
                        toast.success("Jabrool fee reset!");
                    });
                }
            } catch (e) {
                console.log(e.response);
                if(!!e.response.data.errors && e.response.data.errors.length > 0) {
                    e.response.data.errors.map((err, i) => {
                        toast.error(err);
                    });
                } else {
                    toast.error(e.message);
                }
                this.setState(previousState => {
                    return {
                        ...previousState,
                        loading: false
                    };
                });
            }
        }
    }

    render() {
        return (
            <div>
                <div className={"row"}>
                    <div className={"col-md-12 col-sm-12 col-xs-12"}>
                        <div className={"x_panel"}>
                            <div className={"x_title"}>
                                <h2>User
                                    { this.state.user._id &&  <b> {this.state.user.jabroolid} </b>}
                                    { this.state.user._id && <small> (Payment default from: {this.state.user.pay_type})</small>}
                                    { !this.state.user._id && <small>New</small>}
                                </h2>
                                <div className={"clearfix"}></div>
                            </div>
                            <div className={"x_content"}>
                                <br />
                                <form data-parsley-validate className={"form-horizontal form-label-left"}>

                                    <div className={"row"}>

                                        <div className={"col-md-6 col-sm-6 col-xs-6"}>
                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>First Name <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        type="text"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.user.first_name}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.user,
                                                                        first_name: event.target.value
                                                                    }
                                                                };
                                                            })}}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Last Name <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        type="text"
                                                        name="last-name"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.user.last_name}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.user,
                                                                        last_name: event.target.value
                                                                    }
                                                                };
                                                            })}}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Email</label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        type="text"
                                                        name="email"
                                                        value={this.state.user.email}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.user,
                                                                        email: event.target.value
                                                                    }
                                                                };
                                                            })}}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Phone</label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        type="text"
                                                        name="phone"
                                                        value={this.state.user.phone}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.user,
                                                                        phone: event.target.value
                                                                    }
                                                                };
                                                            })}}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Role <span className={"required"}>*</span></label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <select
                                                        className={"form-control"}
                                                        value={this.state.user.role}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.user,
                                                                        role: event.target.value
                                                                    }
                                                                };
                                                            })}}
                                                    >
                                                        <option>user</option>
                                                        <option>admin</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Type <span className={"required"}>*</span></label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <select
                                                        className={"form-control"}
                                                        value={this.state.user.type}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.user,
                                                                        type: event.target.value
                                                                    }
                                                                };
                                                            })}}
                                                    >
                                                        <option>user</option>
                                                        <option>business</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Status <span className={"required"}>*</span></label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <select
                                                        className={"form-control"}
                                                        value={this.state.user.status}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.user,
                                                                        status: event.target.value
                                                                    }
                                                                };
                                                            })}}
                                                    >
                                                        <option>review</option>
                                                        <option>active</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>New password
                                                </label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        className={"date-picker form-control col-md-7 col-xs-12"}
                                                        type="text"
                                                        value={this.state.user.new_password}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.user,
                                                                        new_password: event.target.value
                                                                    }
                                                                };
                                                            })}}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className={"col-md-6 col-sm-6 col-xs-6"}>

                                            <div className={"form-group"}>
                                                <div className={"text-center"}>
                                                    {this.state.user.avatar !=="" && <img className={"img-circle profile_img"} style={{maxHeight: 100, width: "auto", margin: 20}} src={this.state.user.avatar_url}/>}
                                                    {this.state.user.avatar ==="" && <img className={"img-circle profile_img"} style={{maxHeight: 100, width: "auto", margin: 20}} src="/images/empty.png"/>}
                                                </div>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Photo</label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        type="file"
                                                        name="avatar"
                                                        ref={input => {
                                                            this.fileInput = input;
                                                        }}
                                                        onChange={this._preUpload.bind(this)}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"ln_solid"}></div>
                                            <div className={"form-group"}>
                                                <div className={"row"}>
                                                    <div className={"col-lg-6"}>
                                                        <div>Balance: {this.state.user.balance}</div>
                                                        <div className={"form-group"}>
                                                            <label className={"control-label col-lg-6"}>Add balance (SAR)</label>
                                                            <div className={"col-lg-6"}>
                                                                <input
                                                                    className={"form-control col-md-7 col-xs-12"}
                                                                    type="text"
                                                                    name="avatar"
                                                                    value={this.state.user.new_balance}
                                                                    onChange={event => {
                                                                        event.persist();
                                                                        this.setState(previousState => {
                                                                        return {
                                                                            ...previousState,
                                                                            user: {
                                                                                ...previousState.user,
                                                                                new_balance: event.target.value
                                                                            }
                                                                        };
                                                                    })}}
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    { this.state.user.type === 'user' && <div className={"col-lg-6"}>
                                                        <div>Courier limit: {this.state.user.courierLimit}</div>
                                                        <div className={"form-group"}>
                                                            <label className={"control-label col-lg-6"}>Add courier limit (SAR)</label>
                                                            <div className={"col-lg-6"}>
                                                                <input
                                                                    className={"form-control col-md-7 col-xs-12"}
                                                                    type="text"
                                                                    name="avatar"
                                                                    value={this.state.user.new_courierLimit}
                                                                    onChange={event => {
                                                                        event.persist();
                                                                        this.setState(previousState => {
                                                                        return {
                                                                            ...previousState,
                                                                            user: {
                                                                                ...previousState.user,
                                                                                new_courierLimit: event.target.value
                                                                            }
                                                                        };
                                                                    })}}
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>}
                                                </div>
                                            </div>
                                            <div className={"ln_solid"}></div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-lg-6"}>Jabrool fee: {this.state.user.jabroolFee.toFixed(2)} (SAR)</label>
                                                <div className={"col-lg-6"}>
                                                    <button className={"btn btn-warning"} type="button" onClick={this._resetFee.bind(this)}>Reset jabrool fee</button>
                                                </div>
                                            </div>
                                            <div className={"ln_solid"}></div>

                                            { this.state.user.type === 'user' && <div className={"row"}>
                                                <div className={"col-lg-6"}>
                                                    <div className={"form-group"}>
                                                        <Checkbox
                                                            checkboxClass="icheckbox_flat-green"
                                                            increaseArea="20%"
                                                            label=" In Progress"
                                                            disabled
                                                            checked={this.state.user.in_progress}
                                                            onChange={event => {
                                                                event.persist();
                                                                console.log(event.target.checked);
                                                                this.setState(previousState => {
                                                                    return {
                                                                        ...previousState,
                                                                        user: {
                                                                            ...previousState.user,
                                                                            in_progress: !!event.target.checked
                                                                        }
                                                                    };
                                                                })}}
                                                        />
                                                    </div>
                                                </div>
                                                <div className={"col-lg-6"}>
                                                    <div className={"form-group"}>
                                                        <Checkbox
                                                            checkboxClass="icheckbox_flat-green"
                                                            increaseArea="20%"
                                                            label=" Visible"
                                                            disabled
                                                            checked={this.state.user.visible}
                                                            onChange={event => {
                                                                event.persist();
                                                                console.log(event.target.checked);
                                                                this.setState(previousState => {
                                                                    return {
                                                                        ...previousState,
                                                                        user: {
                                                                            ...previousState.user,
                                                                            visible: !!event.target.checked
                                                                        }
                                                                    };
                                                                })}}
                                                        />
                                                    </div>
                                                </div>
                                            </div>}
                                        </div>

                                    </div>

                                    <div className={"ln_solid"}></div>
                                    <div className={"form-group"}>
                                        <div className={"col-lg-6"}>
                                            <Button onClick={() => {
                                                history.go(-1);
                                            }} bsStyle="success">Cancel</Button>
                                            <Button onClick={this._save.bind(this)} bsStyle="success">Submit</Button>
                                        </div>
                                        <div className={"col-lg-6"}>
                                            {this.state.id !== 'new' && <div className={"form-group"}>
                                                <a className={"btn btn-warning"} href={"#/userBank/" + this.state.id}>Bank</a>
                                                { this.state.user.type === 'user' && <a className={"btn btn-warning"} href={"#/userVehicle/" + this.state.id}>Vehicle</a> }
                                                <a className={"btn btn-warning"} href={"#/userLicense/" + this.state.id}>License</a>
                                                <a className={"btn btn-warning"} href={"#/userMessages/" + this.state.id}>Messages</a>
                                                { this.state.user.type === 'business' && <a className={"btn btn-warning"} href={"#/businessCouriers/" + this.state.id }>Couriers</a> }
                                            </div>}
                                            {this.state.id !== 'new' && <div className={"form-group"}>
                                                { this.state.user.type === 'user' && <a className={"btn btn-warning"} href={"#/userOrders/" + this.state.id + "/false"}>Customer orders</a> }
                                                { this.state.user.type === 'user' && <a className={"btn btn-warning"} href={"#/userOrders/" + this.state.id + "/true"}>Courier orders</a> }
                                            </div>}
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.loading && <div className={"css-jxiqlq-wrap"}>
                    <RingLoader
                        style={{margin: 'auto'}}
                        color={'#0019bc'}
                        loading={this.state.loading}
                    />
                </div>}
            </div>
        );
    }

}

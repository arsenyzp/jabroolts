import React, {Component} from 'react';
import { render } from 'react-dom';
import {Button, Modal, Alert, Pagination} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { RingLoader } from 'react-spinners';
import axios from 'axios';
import { Link } from 'react-router';
import 'icheck/skins/all.css';
import {Checkbox, Radio} from 'react-icheck';
import {history} from '../index';
import moment from "moment/moment";

export default class  OrderDetailPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            id: props.params.id,
            model: {},
            ready: false
        };
    }

    componentDidMount() {
        this._load();
    }

    async _load() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.get(`/admin/orders/${this.state.id}`, {
                params: {}
            });
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            }

            this.state.model = response.data.data;

            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false,
                    model: this.state.model,
                    ready: true
                };
            });

        } catch (e) {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
            toast.error(e.message);
        }
    }

    render() {
        return (
            <div>
                <div className={"row"}>
                    <div className={"col-md-12 col-sm-12 col-xs-12"}>
                        <div className={"x_panel"}>
                            <div className={"x_title"}>
                                <h2>Order details</h2>
                                <div className={"clearfix"}></div>
                            </div>
                            <div className={"x_content"}>
                                <br />
                                <form data-parsley-validate className={"form-horizontal form-label-left"}>

                                    {this.state.ready && !!this.state.model && <div className={"row"}>

                                        <table className={"table table-striped table-bordered detail-view"}>
                                            <tbody>

                                            <tr>
                                                <th>Order id</th>
                                                <td>{this.state.model.orderId}</td>
                                            </tr>

                                            <tr>
                                                <th>Invoice id</th>
                                                <td>{this.state.model.orderId}</td>
                                            </tr>

                                            <tr>
                                                <th>Start date & time</th>
                                                <td>{moment(new Date(this.state.model.start_at)).format("DD.MM.YYYY HH:mm")}</td>
                                            </tr>

                                            <tr>
                                                <th>Finish date & time</th>
                                                <td>{moment(new Date(this.state.model.end_at)).format("DD.MM.YYYY HH:mm")}</td>
                                            </tr>

                                            <tr>
                                                <th>Owner</th>
                                                <td>{this.state.model.owner.jabroolid}</td>
                                            </tr>

                                            <tr>
                                                <th>Receiver</th>
                                                <td>
                                                    {this.state.model.receiver && <div>{this.state.model.receiver.jabroolid}</div>}
                                                    {!this.state.model.receiver && <div>{this.state.model.recipient_name} {this.state.model.recipient_contact}</div>}
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>Courier</th>
                                                <td>{this.state.model.courier.jabroolid}</td>
                                            </tr>

                                            <tr>
                                                <th>Pick up</th>
                                                <td>{this.state.model.owner_address}</td>
                                            </tr>

                                            <tr>
                                                <th>Drop off location</th>
                                                <td>{this.state.model.recipient_address}</td>
                                            </tr>

                                            <tr>
                                                <th>Distance in km</th>
                                                <td>{this.state.model.route}</td>
                                            </tr>

                                            <tr>
                                                <th>Number of parcels</th>
                                                <td>{
                                            this.state.model.small_package_count +
                                            this.state.model.medium_package_count +
                                            this.state.model.large_package_count
                                                }</td>
                                            </tr>

                                            <tr>
                                                <th>Accepted time (min)</th>
                                                {this.state.model.accept_at !== null && <td>{((this.state.model.accept_at - this.state.model.created_at)/60000).toFixed(2)}</td>}
                                                {this.state.model.accept_at === null && <td>--</td>}
                                            </tr>

                                            <tr>
                                                <th>Payment method</th>
                                                <td>{this.state.model.pay_type}</td>
                                            </tr>

                                            <tr>
                                                <th>Amount (SAR)</th>
                                                <td>{this.state.model.cost}</td>
                                            </tr>

                                            <tr>
                                                <th>Status</th>
                                                <td>{this.state.model.status}</td>
                                            </tr>

                                            <tr>
                                                <th>Delivery type</th>
                                                <td>{this.state.model.type}</td>
                                            </tr>

                                            <tr>
                                                <th>Trip duration (min)</th>
                                                {this.state.model.start_at !== null && <td>{((this.state.model.end_at - this.state.model.start_at)/60000).toFixed(2)}</td>}
                                                {this.state.model.start_at === null && <td>---</td>}
                                            </tr>

                                            <tr>
                                                <th>Courier earning (SAR)</th>
                                                <td>{(this.state.model.cost - this.state.model.serviceFee).toFixed(2)}</td>
                                            </tr>

                                            <tr>
                                                <th>Jabrool earning (SAR)</th>
                                                <td>{this.state.model.serviceFee.toFixed(2)}</td>
                                            </tr>

                                            </tbody>
                                        </table>

                                    </div>}

                                    {this.state.ready && !this.state.model && <h3>Info not found</h3> }

                                    <div className={"ln_solid"}></div>
                                    <div className={"form-group"}>
                                        <div className={"col-lg-6"}>
                                            <Button onClick={() => {
                                                history.go(-1);
                                            }} bsStyle="success">Cancel</Button>
                                        </div>
                                        <div className={"col-lg-6"}>

                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.loading && <div className={"css-jxiqlq-wrap"}>
                    <RingLoader
                        style={{margin: 'auto'}}
                        color={'#0019bc'}
                        loading={this.state.loading}
                    />
                </div>}
            </div>
        );
    }

}
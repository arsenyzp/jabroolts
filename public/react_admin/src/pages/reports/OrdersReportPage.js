import React, {Component} from 'react';
import { render } from 'react-dom';
import {Button, Modal, Alert, Pagination} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { RingLoader } from 'react-spinners';
import axios from 'axios';
import { Link } from 'react-router';
import 'icheck/skins/all.css';
import {Checkbox, Radio} from 'react-icheck';
import {history} from '../../index';
import {ITEMS_PER_PAGE} from "../../components/Constants";
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { formatDate, parseDate } from 'react-day-picker/moment';
import 'react-day-picker/lib/style.css';
import moment from 'moment';

export default class OrdersReportPage extends Component  {

    constructor(props){
        super(props);
        this.state = {
            loading: true,
            activePage: 1,
            total: 0,
            q: "",
            items: [],
            filter: {
                start_date: new Date(),
                end_date: new Date(),
                start_date_str:  moment().format("DD/MM/YYYY"),
                end_date_str:  moment().format("DD/MM/YYYY")
            }
        };
    }

    componentWillMount(){
        this._loadList();
    }

    async _loadList() {
        this.setState({
            loading: true
        });
        try {
            let response = await axios.get(`/admin/reports/orders/${ITEMS_PER_PAGE}/${this.state.activePage}`, {
                params: {
                    q: this.state.q,
                    sortKey: "",
                    reverse: ""
                }
            });
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            }

            this.setState({
                loading: false,
                items: response.data.data.items,
                total: response.data.data.total_count
            });

        } catch (e) {
            this.setState({
                loading: false
            });
            toast.error(e.message);
        }
    }


    handlePagination(eventKey) {
        this.setState({
            activePage: eventKey
        }, () => {
            this._loadList()
        });
    }

    _renderRow(item, i) {
        return (
            <tr key={i}>
                <td>{ item.tripID }</td>
                <td>{moment(item.date).format("DD.MM.YYYY HH:mm")}</td>
                <td><a href={ item.city } target={"_blank"}>View on map</a></td>
                <td>{ item.deliveryType }</td>
                <td>{ item.courierJID }</td>
                <td>{ Math.round(item.tripDuration/60000) }</td>
                <td>{ item.tripDistance.toFixed(2) }</td>
                <td>{ (item.tripWaitTime/60000).toFixed(2) }</td>
                <td>{ item.tripPrice.toFixed(2) }</td>
                <td>{ Math.round(item.tripPrice - item.jabroolEarnings) }</td>
                <td>{ item.paymentMethod }</td>
                <td>{ item.jabroolEarnings.toFixed(2)}</td>
                <td>{ item.roundDown.toFixed(2) }</td>
                <td className={"text-center"}>
                    <Link to={`/order/${item._id}`}><i className={"fa fa-eye"} /></Link>
                </td>
            </tr>
        );
    }

    _renderTitle() {
        return (
            <h2>Orders</h2>
        );
    }

    render() {
        return (
            <div className={"row"} style={{position: "relative"}}>
                <div className={"col-md-12 col-sm-12 col-xs-12"}>
                    <div className={"x_panel"}>
                        <div className={"x_title"}>

                            {this._renderTitle()}

                            <div className={"title_right"}>
                                <div className={"col-lg-6 form-group pull-right"}>
                                    <div className={"row"}>
                                        <div className={"col-md-4"}>
                                            <div className={"form-group"}>
                                                <span className="InputFromTo-to">
                                                      <DayPickerInput
                                                          className={"form-control col-md-7 col-xs-12"}
                                                          value={this.state.filter.start_date_str}
                                                          placeholder="Issue date"
                                                          locale={'en'}
                                                          formatDate={formatDate}
                                                          parseDate={parseDate}
                                                          format={"DD/MM/YYYY"}
                                                          dayPickerProps={{
                                                              numberOfMonths: 1,
                                                              className: "form-control col-md-7 col-xs-12"
                                                          }}
                                                          inputProps={{
                                                              className: "form-control col-md-7 col-xs-12"
                                                          }}
                                                          onDayChange={(from) => {
                                                              console.log(from);
                                                              this.setState(previousState => {
                                                                  return {
                                                                      ...previousState,
                                                                      filter: {
                                                                          ...previousState.filter,
                                                                          start_date: from,
                                                                          start_date_str:  moment(from).format("DD-MM-YYYY")
                                                                      }
                                                                  };
                                                              });
                                                          }}
                                                      />
                                                </span>
                                            </div>
                                        </div>
                                        <div className={"col-md-4"}>
                                            <div className={"form-group"}>
                                                <span className="InputFromTo-to">
                                                     <DayPickerInput
                                                         className={"form-control col-md-7 col-xs-12"}
                                                         value={this.state.filter.end_date_str}
                                                         placeholder="Issue date"
                                                         locale={'en'}
                                                         formatDate={formatDate}
                                                         parseDate={parseDate}
                                                         format={"DD/MM/YYYY"}
                                                         dayPickerProps={{
                                                             numberOfMonths: 1,
                                                             className: "form-control col-md-7 col-xs-12"
                                                         }}
                                                         inputProps={{
                                                             className: "form-control col-md-7 col-xs-12"
                                                         }}
                                                         onDayChange={(from) => {
                                                             console.log(from);
                                                             this.setState(previousState => {
                                                                 return {
                                                                     ...previousState,
                                                                     filter: {
                                                                         ...previousState.filter,
                                                                         end_date: from,
                                                                         end_date_str:  moment(from).format("DD-MM-YYYY")
                                                                     }
                                                                 };
                                                             });
                                                         }}
                                                     />
                                                </span>
                                            </div>
                                        </div>
                                        <div className={"col-md-4"}>
                                            <div className={"form-group"}>
                                                <a target="_blank" href={"/admin/reports/orders/export/?exportDateFrom=" + moment(this.state.filter.start_date).format("DD-MM-YYYY") + "&exportDateTo="+ moment(this.state.filter.end_date).format("DD-MM-YYYY")} className={"btn btn-success float-lg-right"}> Export</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className={"clearfix"}></div>
                        </div>
                        <div className={"x_content"}>

                            <table id="datatable-responsive" className={"table table-striped table-bordered dt-responsive nowrap"} cellSpacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Trip ID</th>
                                    <th>Trip date and time</th>
                                    <th>City</th>
                                    <th>Delivery type</th>
                                    <th>Courior ID</th>
                                    <th>Total trip duration (min)</th>
                                    <th>Distance travelled (km)</th>
                                    <th>Courior wait time for customer at pickup (min)</th>
                                    <th>Trip price (SAR)</th>
                                    <th>Courior earnings (SAR)</th>
                                    <th>Payment method</th>
                                    <th>Jabrool ernings</th>
                                    <th>Round down</th>
                                    <th>View</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.state.items.map((item, i) => {
                                    return this._renderRow(item, i);
                                })}
                                </tbody>
                            </table>


                        </div>
                    </div>

                    {!this.state.loading && <div>
                        <Pagination
                            prev
                            next
                            first
                            last
                            ellipsis
                            boundaryLinks
                            items={Math.round(this.state.total/ITEMS_PER_PAGE)}
                            maxButtons={5}
                            activePage={this.state.activePage}
                            onSelect={this.handlePagination.bind(this)}
                        />
                    </div>}

                </div>

                {this.state.loading && <div className={"css-jxiqlq-wrap"}>
                    <RingLoader
                        style={{margin: 'auto'}}
                        color={'#0019bc'}
                        loading={this.state.loading}
                    />
                </div>}
            </div>
        );
    }

}
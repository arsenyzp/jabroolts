import React, {Component} from 'react';
import { render } from 'react-dom';
import {Button, Modal, Alert, Pagination} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { RingLoader } from 'react-spinners';
import axios from 'axios';
import { Link } from 'react-router';
import 'icheck/skins/all.css';
import {Checkbox, Radio} from 'react-icheck';
import {history} from '../../index';
import {ITEMS_PER_PAGE} from "../../components/Constants";
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { formatDate, parseDate } from 'react-day-picker/moment';
import 'react-day-picker/lib/style.css';
import moment from 'moment';
import MyMapComponent from "../../components/MyMapComponent";

export default class CouriorClustersPage extends Component  {

    constructor(props){
        super(props);
        let d = new Date();
        let end = new Date().setDate(d.getDate()-7);
        this.state = {
            loading: true,
            activePage: 1,
            total: 0,
            q: "",
            items: [],
            filter: {
                start_date: end,
                end_date: d,
                start_date_str:  moment(end).format("DD/MM/YYYY"),
                end_date_str:  moment(d).format("DD/MM/YYYY")
            },
            lat: 0,
            lng: 0
        };
    }

    componentWillMount(){
        this._loadList();
    }

    async _loadList() {
        this.setState({
            loading: true
        });
        try {
            let response = await axios.get(`/admin/reports/courior_clusters/${ITEMS_PER_PAGE}/${this.state.activePage}`, {
                params: {
                    q: this.state.q,
                    sortKey: "",
                    reverse: "",
                    exportDateFrom: moment(this.state.filter.start_date).format("DD-MM-YYYY"),
                    exportDateTo: moment(this.state.filter.end_date).format("DD-MM-YYYY")
                }
            });
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            }

            let lat = 0;
            let lng = 0;

            response.data.data.items.map((v, k, a) => {
                lat += v.lat*1;
                lng += v.lon*1;
            });

            lat = lat/response.data.data.total_count;
            lng = lng/response.data.data.total_count;

            this.setState({
                loading: false,
                items: response.data.data.items,
                total: response.data.data.total_count,
                lat: lat,
                lng: lng
            });

        } catch (e) {
            this.setState({
                loading: false
            });
            toast.error(e.message);
        }
    }


    handlePagination(eventKey) {
        this.setState({
            activePage: eventKey
        }, () => {
            this._loadList()
        });
    }

    _renderRow(item, i) {
        return (
            <tr key={i}>
                <td><a href={ item.map } target="_blank">View on map</a></td>
                <td>{item.count}</td>
                <td>{moment(item.date).format("DD/MM/YYYY HH:mm")}</td>
            </tr>
        );
    }

    _renderTitle() {
        return (
            <h2>Couriers clusters</h2>
        );
    }

    render() {
        return (
            <div className={"row"} style={{position: "relative"}}>
                <div className={"col-md-12 col-sm-12 col-xs-12"}>
                    <div className={"x_panel"}>
                        <div className={"x_title"}>

                            {this._renderTitle()}

                            <div className={"title_right"}>
                                <div className={"col-lg-6 form-group pull-right"}>
                                    <div className={"row"}>
                                        <div className={"col-md-4"}>
                                            <div className={"form-group"}>
                                                <span className="InputFromTo-to">
                                                      <DayPickerInput
                                                          className={"form-control col-md-7 col-xs-12"}
                                                          value={this.state.filter.start_date_str}
                                                          placeholder="Issue date"
                                                          locale={'en'}
                                                          formatDate={formatDate}
                                                          parseDate={parseDate}
                                                          format={"DD/MM/YYYY"}
                                                          dayPickerProps={{
                                                              numberOfMonths: 1,
                                                              className: "form-control col-md-7 col-xs-12"
                                                          }}
                                                          inputProps={{
                                                              className: "form-control col-md-7 col-xs-12"
                                                          }}
                                                          onDayChange={(from) => {
                                                              console.log(from);
                                                              this.setState(previousState => {
                                                                  return {
                                                                      ...previousState,
                                                                      filter: {
                                                                          ...previousState.filter,
                                                                          start_date: from,
                                                                          start_date_str:  moment(from).format("DD-MM-YYYY")
                                                                      }
                                                                  };
                                                              }, () => {
                                                                  this._loadList();
                                                              });
                                                          }}
                                                      />
                                                </span>
                                            </div>
                                        </div>
                                        <div className={"col-md-4"}>
                                            <div className={"form-group"}>
                                                <span className="InputFromTo-to">
                                                     <DayPickerInput
                                                         className={"form-control col-md-7 col-xs-12"}
                                                         value={this.state.filter.end_date_str}
                                                         placeholder="Issue date"
                                                         locale={'en'}
                                                         formatDate={formatDate}
                                                         parseDate={parseDate}
                                                         format={"DD/MM/YYYY"}
                                                         dayPickerProps={{
                                                             numberOfMonths: 1,
                                                             className: "form-control col-md-7 col-xs-12"
                                                         }}
                                                         inputProps={{
                                                             className: "form-control col-md-7 col-xs-12"
                                                         }}
                                                         onDayChange={(from) => {
                                                             console.log(from);
                                                             this.setState(previousState => {
                                                                 return {
                                                                     ...previousState,
                                                                     filter: {
                                                                         ...previousState.filter,
                                                                         end_date: from,
                                                                         end_date_str:  moment(from).format("DD-MM-YYYY")
                                                                     }
                                                                 };
                                                             }, () => {
                                                                 this._loadList();
                                                             });
                                                         }}
                                                     />
                                                </span>
                                            </div>
                                        </div>
                                        <div className={"col-md-4"}>
                                            <div className={"form-group"}>
                                                <a target="_blank" href={"/admin/reports/courior_clusters/export/?exportDateFrom=" + moment(this.state.filter.start_date).format("DD-MM-YYYY") + "&exportDateTo="+ moment(this.state.filter.end_date).format("DD-MM-YYYY")} className={"btn btn-success float-lg-right"}> Export</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className={"clearfix"}></div>
                        </div>

                        <div className={"x_content"} style={{height: 500}}>
                            {(this.state.lat !== 0 && this.state.lng !== 0) && <MyMapComponent
                                items={this.state.items}
                                lat={this.state.lat}
                                lng={this.state.lng}
                            />}
                        </div>

                        <div className={"x_content"}>

                            <table id="datatable-responsive" className={"table table-striped table-bordered dt-responsive nowrap"} cellSpacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Locations where no couriors avalabile for requests</th>
                                    <th>Number of times no Couriors around at this location</th>
                                    <th>Timestamp</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.state.items.map((item, i) => {
                                    return this._renderRow(item, i);
                                })}
                                </tbody>
                            </table>


                        </div>
                    </div>

                    {!this.state.loading && <div>
                        <Pagination
                            prev
                            next
                            first
                            last
                            ellipsis
                            boundaryLinks
                            items={Math.round(this.state.total/ITEMS_PER_PAGE)}
                            maxButtons={5}
                            activePage={this.state.activePage}
                            onSelect={this.handlePagination.bind(this)}
                        />
                    </div>}

                </div>

                {this.state.loading && <div className={"css-jxiqlq-wrap"}>
                    <RingLoader
                        style={{margin: 'auto'}}
                        color={'#0019bc'}
                        loading={this.state.loading}
                    />
                </div>}
            </div>
        );
    }

}
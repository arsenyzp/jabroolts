import React, {Component} from 'react';
import { render } from 'react-dom';
import {Button, Modal, Alert, Pagination} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { RingLoader } from 'react-spinners';
import axios from 'axios';
import { Link } from 'react-router';
import 'icheck/skins/all.css';
import {Checkbox, Radio} from 'react-icheck';
import {history} from '../../index';

export default class ConfigRadiusPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            config: {close_radius: 0},
            ready: false
        };
    }

    componentDidMount() {
        this._load();
    }

    async _load() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.get(`/admin/config/radius/`, {
                params: {}
            });
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            }

            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false,
                    config: response.data.data,
                    ready: true
                };
            });

        } catch (e) {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
            toast.error(e.message);
        }
    }

    async _save() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            this.state.config.close_radius = parseInt(this.state.config.close_radius);
            this.state.config.request = parseInt(this.state.config.request);
            this.state.config.feed = parseInt(this.state.config.feed);
            let response = await axios.post(`/admin/config/radius/`, this.state.config);
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                this.setState(previousState => {
                    return {
                        ...previousState,
                        loading: false,
                        config: response.data.data
                    };
                }, () => {
                    toast.success("Item updated!");
                });
            }
        } catch (e) {
            console.log(e.response);
            if(!!e.response && !!e.response.data.errors && e.response.data.errors.length > 0) {
                e.response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                toast.error(e.message);
            }
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
        }
    }

    render() {
        return (
            <div>
                <div className={"row"}>
                    <div className={"col-md-12 col-sm-12 col-xs-12"}>
                        <div className={"x_panel"}>
                            <div className={"x_title"}>
                                <h2>Radius configuration </h2>
                                <div className={"clearfix"}></div>
                            </div>
                            <div className={"x_content"}>
                                <br />
                                <form data-parsley-validate className={"form-horizontal form-label-left"}>

                                    {this.state.ready && !!this.state.config && <div className={"row"}>

                                        <div className={"col-lg-3"}>
                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-12"}>New request <br/> for Couriers (m)
                                                </label>
                                                <div className={"col-md-12"}>
                                                    <input
                                                        type="number"
                                                        required="required"
                                                        className={"form-control col-md-12"}
                                                        value={this.state.config.request}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    config: {
                                                                        ...previousState.config,
                                                                        request: parseInt(event.target.value)
                                                                    }
                                                                };
                                                            })}}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className={"col-lg-3"}>
                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-12"}>Available couriers<br/> around Customer (m)
                                                </label>
                                                <div className={"col-md-12"}>
                                                    <input
                                                        type="number"
                                                        required="required"
                                                        className={"form-control col-md-12"}
                                                        value={this.state.config.feed}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    config: {
                                                                        ...previousState.config,
                                                                        feed: parseInt(event.target.value)
                                                                    }
                                                                };
                                                            })}}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className={"col-lg-3"}>
                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-12"}>Notification:<br/> Courier get closely to Pick Up (m)
                                                </label>
                                                <div className={"col-md-12"}>
                                                    <input
                                                        type="number"
                                                        required="required"
                                                        className={"form-control col-md-12"}
                                                        value={this.state.config.drop_off}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    config: {
                                                                        ...previousState.config,
                                                                        drop_off: parseInt(event.target.value)
                                                                    }
                                                                };
                                                            })}}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className={"col-lg-3"}>
                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-12"}>Notification:<br/> Courier get closely to Drop Off (m)
                                                </label>
                                                <div className={"col-md-12"}>
                                                    <input
                                                        type="number"
                                                        required="required"
                                                        className={"form-control col-md-12"}
                                                        value={this.state.config.close_radius}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    config: {
                                                                        ...previousState.config,
                                                                        close_radius: parseInt(event.target.value)
                                                                    }
                                                                };
                                                            })}}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                    </div>}

                                    {this.state.ready && !this.state.config && <h3>Config info not found</h3> }

                                    <div className={"ln_solid"}></div>
                                    <div className={"form-group"}>
                                        <div className={"col-lg-6"}>
                                            <Button onClick={this._save.bind(this)} bsStyle="success">Submit</Button>
                                        </div>
                                        <div className={"col-lg-6"}>

                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.loading && <div className={"css-jxiqlq-wrap"}>
                    <RingLoader
                        style={{margin: 'auto'}}
                        color={'#0019bc'}
                        loading={this.state.loading}
                    />
                </div>}
            </div>
        );
    }

}
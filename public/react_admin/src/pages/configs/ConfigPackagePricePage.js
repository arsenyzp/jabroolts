import React, {Component} from 'react';
import {render} from 'react-dom';
import {Button, Modal, Alert, Pagination} from 'react-bootstrap';
import {ToastContainer, toast} from 'react-toastify';
import {RingLoader} from 'react-spinners';
import axios from 'axios';
import {Link} from 'react-router';
import 'icheck/skins/all.css';
import {Checkbox, Radio} from 'react-icheck';
import {history} from '../../index';

export default class ConfigPackagePricePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            config: {},
            ready: false
        };
    }

    componentDidMount() {
        this._load();
    }

    async _load() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.get(`/admin/config/packages/`, {
                params: {}
            });
            if (!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            }

            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false,
                    config: response.data.data,
                    ready: true
                };
            });

        } catch (e) {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
            toast.error(e.message);
        }
    }

    async _save() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.post(`/admin/config/packages/`, this.state.config);
            if (!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                this.setState(previousState => {
                    return {
                        ...previousState,
                        loading: false,
                        config: response.data.data
                    };
                }, () => {
                    toast.success("Item updated!");
                });
            }
        } catch (e) {
            console.log(e.response);
            if (!!e.response && !!e.response.data.errors && e.response.data.errors.length > 0) {
                e.response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                toast.error(e.message);
            }
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
        }
    }

    render() {
        return (
            <div>
                <div className={"row m-f"}>
                    <div className={"col-md-12 col-sm-12 col-xs-12"}>
                        <div className={"x_panel"}>
                            <div className={"x_title"}>
                                <h2>Package configurations </h2>
                                <div className={"clearfix"}></div>
                            </div>
                            <div className={"x_content"}>
                                <br/>
                                {this.state.ready && !!this.state.config &&
                                <form data-parsley-validate className={"form-horizontal form-label-left"}>

                                    <div className={"row"}>

                                        <div className={"col-lg-4"}>
                                            <h2>Small </h2>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-lg-8"}>Jabrool
                                                    price <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-lg-4"}>
                                                    <input
                                                        type="number"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.config.small.j_cost}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    config: {
                                                                        ...previousState.config,
                                                                        small: {
                                                                            ...previousState.config.small,
                                                                            j_cost: parseFloat(event.target.value)
                                                                        }
                                                                    }
                                                                };
                                                            })
                                                        }}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-lg-8"}>Express
                                                    price <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-lg-4"}>
                                                    <input
                                                        type="number"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.config.small.e_cost}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    config: {
                                                                        ...previousState.config,
                                                                        small: {
                                                                            ...previousState.config.small,
                                                                            e_cost: event.target.value
                                                                        }
                                                                    }
                                                                };
                                                            })
                                                        }}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-lg-8"}>Max
                                                    count <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-lg-4"}>
                                                    <input
                                                        type="number"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.config.small.max_count}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    config: {
                                                                        ...previousState.config,
                                                                        small: {
                                                                            ...previousState.config.small,
                                                                            max_count: event.target.value
                                                                        }
                                                                    }
                                                                };
                                                            })
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className={"col-lg-4"}>
                                            <h2>Medium </h2>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-lg-8"}>Jabrool
                                                    price <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-lg-4"}>
                                                    <input
                                                        type="number"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.config.medium.j_cost}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    config: {
                                                                        ...previousState.config,
                                                                        medium: {
                                                                            ...previousState.config.medium,
                                                                            j_cost: parseFloat(event.target.value)
                                                                        }
                                                                    }
                                                                };
                                                            })
                                                        }}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-lg-8"}>Express
                                                    price <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-lg-4"}>
                                                    <input
                                                        type="number"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.config.medium.e_cost}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    config: {
                                                                        ...previousState.config,
                                                                        medium: {
                                                                            ...previousState.config.medium,
                                                                            e_cost: parseFloat(event.target.value)
                                                                        }
                                                                    }
                                                                };
                                                            })
                                                        }}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-lg-8"}>Max
                                                    count <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-lg-4"}>
                                                    <input
                                                        type="number"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.config.medium.max_count}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    config: {
                                                                        ...previousState.config,
                                                                        medium: {
                                                                            ...previousState.config.medium,
                                                                            max_count: parseFloat(event.target.value)
                                                                        }
                                                                    }
                                                                };
                                                            })
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className={"col-lg-4"}>
                                            <h2>Large </h2>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-lg-8"}>Jabrool
                                                    price <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-lg-4"}>
                                                    <input
                                                        type="number"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.config.large.j_cost}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    config: {
                                                                        ...previousState.config,
                                                                        large: {
                                                                            ...previousState.config.large,
                                                                            j_cost: parseFloat(event.target.value)
                                                                        }
                                                                    }
                                                                };
                                                            })
                                                        }}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-lg-8"}>Express
                                                    price <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-lg-4"}>
                                                    <input
                                                        type="number"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.config.large.e_cost}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    config: {
                                                                        ...previousState.config,
                                                                        large: {
                                                                            ...previousState.config.large,
                                                                            e_cost: parseFloat(event.target.value)
                                                                        }
                                                                    }
                                                                };
                                                            })
                                                        }}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-lg-8"}>Max
                                                    count <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-lg-4"}>
                                                    <input
                                                        type="number"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.config.large.max_count}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    config: {
                                                                        ...previousState.config,
                                                                        large: {
                                                                            ...previousState.config.large,
                                                                            max_count: parseInt(event.target.value)
                                                                        }
                                                                    }
                                                                };
                                                            })
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div className={"ln_solid"}></div>

                                    {this.state.ready && !!this.state.config && <div className={"row"}>
                                        <div className={"col-lg-12"}>
                                            <h2>Package dimensions </h2>
                                            <div className={"row"}>

                                                <div className={"col-lg-4"}>
                                                    <div className={"col-lg-12"}>
                                                        <div className={"form-group text-right"}>
                                                            <label
                                                                className={"control-label col-lg-12 text-right"}
                                                                style={{textAlign: {
                                                                        value: 'right',
                                                                        important: 'true',
                                                                    }}}
                                                                ref={(node) => {
                                                                    if (node) {
                                                                        node.style.setProperty("text-align", "right", "important");
                                                                    }
                                                                }}
                                                            >Small (side size
                                                                cm)<span className={"required"}>*</span>
                                                            </label>
                                                            <div className={"col-lg-12 text-right"}>
                                                                <div style={{height: "30px"}}></div>
                                                                Up to {this.state.config.medium.from_dimension}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className={"col-lg-4"}>
                                                    <div className={"form-group"}>
                                                        <label className={"control-label col-lg-12 text-center"}>Medium (side size
                                                            cm)<span className={"required"}>*</span>
                                                        </label>
                                                        <div className={"col-lg-12"}>
                                                            <div className={"row"}>

                                                                <div className={"col-lg-6"}>
                                                                    <div className={"form-group"}>
                                                                        <label
                                                                            className={"control-label col-lg-12"}>From
                                                                            dimension <span
                                                                                className={"required"}>*</span>
                                                                        </label>
                                                                        <div className={"col-lg-12"}>
                                                                            <input
                                                                                type="number"
                                                                                required="required"
                                                                                className={"form-control col-md-7 col-xs-12"}
                                                                                value={this.state.config.medium.from_dimension}
                                                                                onChange={event => {
                                                                                    event.persist();
                                                                                    this.setState(previousState => {
                                                                                        return {
                                                                                            ...previousState,
                                                                                            config: {
                                                                                                ...previousState.config,
                                                                                                medium: {
                                                                                                    ...previousState.config.medium,
                                                                                                    from_dimension: parseInt(event.target.value)
                                                                                                }
                                                                                            }
                                                                                        };
                                                                                    })
                                                                                }}
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className={"col-lg-6"}>
                                                                    <div className={"form-group"}>
                                                                        <label
                                                                            className={"control-label col-lg-12"}>To
                                                                            dimension <span
                                                                                className={"required"}>*</span>
                                                                        </label>
                                                                        <div className={"col-lg-12"}>
                                                                            <input
                                                                                type="number"
                                                                                required="required"
                                                                                className={"form-control col-md-7 col-xs-12"}
                                                                                value={this.state.config.medium.to_dimension}
                                                                                onChange={event => {
                                                                                    event.persist();
                                                                                    this.setState(previousState => {
                                                                                        return {
                                                                                            ...previousState,
                                                                                            config: {
                                                                                                ...previousState.config,
                                                                                                medium: {
                                                                                                    ...previousState.config.medium,
                                                                                                    to_dimension: parseInt(event.target.value)
                                                                                                }
                                                                                            }
                                                                                        };
                                                                                    })
                                                                                }}
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className={"col-lg-4"}>
                                                    <div className={"form-group"}>
                                                        <label className={"control-label col-lg-12 text-center"}>Large (side
                                                            size cm)<span className={"required"}>*</span>
                                                        </label>
                                                        <div className={"col-lg-12"}>
                                                            <div style={{height: "30px"}}></div>
                                                            More then {this.state.config.medium.to_dimension}
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>}

                                    <div className={"ln_solid"}></div>

                                    <div className={"form-group"}>
                                        <div className={"col-lg-6"}>
                                            <Button onClick={this._save.bind(this)} bsStyle="success">Submit</Button>
                                        </div>
                                        <div className={"col-lg-6"}>

                                        </div>
                                    </div>

                                </form>}

                                {this.state.ready && !this.state.config && <h3>Config info not found</h3>}
                                <div>
                                    <div className={"ln_solid"}></div>
                                    Note:<br/>
                                    Packege volume = A side size, cm x B side size x C side size, cm = N cm3<br/>
                                    A side size, cm = B side size, cm = C side size, cm<br/>
                                    Units: 1000 cm3 = 1 dm3 = 1 liter<br/>
                                    <div className={"ln_solid"}></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.loading && <div className={"css-jxiqlq-wrap"}>
                    <RingLoader
                        style={{margin: 'auto'}}
                        color={'#0019bc'}
                        loading={this.state.loading}
                    />
                </div>}
            </div>
        );
    }

}
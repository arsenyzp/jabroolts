import React, {Component} from 'react';
import { render } from 'react-dom';
import {Button, Modal, Alert, Pagination} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { RingLoader } from 'react-spinners';
import axios from 'axios';
import { Link } from 'react-router';
import 'icheck/skins/all.css';
import {Checkbox, Radio} from 'react-icheck';
import {history} from '../../index';
import MyStatefulEditor from '../../components/MyStatefulEditor';

export default class ConfigTermsPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            config: {
                terms_courier_en: "",
                terms_courier_ar: "",
                terms_customer_ar: "",
                terms_customer_en: "",
                privacy_ar: "",
                privacy_en: ""
            },
            ready: false
        };
    }

    componentDidMount() {
        this._load();
    }

    async _load() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.get(`/admin/config/terms/`, {
                params: {}
            });
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            }

            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false,
                    config: response.data.data,
                    ready: true
                };
            });

        } catch (e) {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
            toast.error(e.message);
        }
    }

    async _save() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.post(`/admin/config/terms/`, this.state.config);
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                this.setState(previousState => {
                    return {
                        ...previousState,
                        loading: false,
                        config: response.data.data
                    };
                }, () => {
                    toast.success("Item updated!");
                });
            }
        } catch (e) {
            console.log(e.response);
            if(!!e.response && !!e.response.data.errors && e.response.data.errors.length > 0) {
                e.response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                toast.error(e.message);
            }
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
        }
    }

    render() {
        

        return (
            <div>

                {this.state.ready && <div>
                        <div className={"row"}>
                            <div className={"col-md-12 col-sm-12 col-xs-12"}>
                                <div className={"x_panel"}>
                                    <div className={"x_title"}>
                                        <h2>Terms and conditions courier <small>(EN)</small></h2>
                                        <div className={"clearfix"}></div>
                                    </div>
                                    <div className={"x_content"}>
                                        <MyStatefulEditor 
                                        value={this.state.config.terms_courier_en}
                                        onChange={value => {
                                            this.setState(previousState => {
                                                return {
                                                    ...previousState,
                                                    config: {
                                                        ...previousState.config,
                                                        terms_courier_en: value
                                                    }
                                                };
                                            });
                                        }}
                                        />
                                        <div className={"ln_solid"}></div>
                                    </div>
                                </div>
                            </div>   
                        </div>

                        <div className={"row"}>
                            <div className={"col-md-12 col-sm-12 col-xs-12"}>
                                <div className={"x_panel"}>
                                    <div className={"x_title"}>
                                        <h2>Terms and conditions courier <small>(AR)</small></h2>
                                        <div className={"clearfix"}></div>
                                    </div>
                                    <div className={"x_content"}>
                                        <MyStatefulEditor 
                                        value={this.state.config.terms_courier_ar}
                                        onChange={value => {
                                            this.setState(previousState => {
                                                return {
                                                    ...previousState,
                                                    config: {
                                                        ...previousState.config,
                                                        terms_courier_ar: value
                                                    }
                                                };
                                            });
                                        }}
                                        />
                                        <div className={"ln_solid"}></div>
                                    </div>
                                </div>
                            </div>   
                        </div>

                        <div className={"row"}>
                            <div className={"col-md-12 col-sm-12 col-xs-12"}>
                                <div className={"x_panel"}>
                                    <div className={"x_title"}>
                                        <h2>Terms and conditions customer <small>(EN)</small></h2>
                                        <div className={"clearfix"}></div>
                                    </div>
                                    <div className={"x_content"}>
                                        <MyStatefulEditor 
                                        value={this.state.config.terms_customer_en}
                                        onChange={value => {
                                            this.setState(previousState => {
                                                return {
                                                    ...previousState,
                                                    config: {
                                                        ...previousState.config,
                                                        terms_customer_en: value
                                                    }
                                                };
                                            });
                                        }}
                                        />
                                        <div className={"ln_solid"}></div>
                                    </div>
                                </div>
                            </div>   
                        </div>

                        <div className={"row"}>
                            <div className={"col-md-12 col-sm-12 col-xs-12"}>
                                <div className={"x_panel"}>
                                    <div className={"x_title"}>
                                        <h2>Terms and conditions customer <small>(AR)</small></h2>
                                        <div className={"clearfix"}></div>
                                    </div>
                                    <div className={"x_content"}>
                                        <MyStatefulEditor 
                                        value={this.state.config.terms_customer_ar}
                                        onChange={value => {
                                            this.setState(previousState => {
                                                return {
                                                    ...previousState,
                                                    config: {
                                                        ...previousState.config,
                                                        terms_customer_ar: value
                                                    }
                                                };
                                            });
                                        }}
                                        />
                                        <div className={"ln_solid"}></div>
                                    </div>
                                </div>
                            </div>   
                        </div>

                        <div className={"row"}>
                            <div className={"col-md-12 col-sm-12 col-xs-12"}>
                                <div className={"x_panel"}>
                                    <div className={"x_title"}>
                                        <h2>Privacy <small>(EN)</small></h2>
                                        <div className={"clearfix"}></div>
                                    </div>
                                    <div className={"x_content"}>
                                        <MyStatefulEditor 
                                        value={this.state.config.privacy_en}
                                        onChange={value => {
                                            this.setState(previousState => {
                                                return {
                                                    ...previousState,
                                                    config: {
                                                        ...previousState.config,
                                                        privacy_en: value
                                                    }
                                                };
                                            });
                                        }}
                                        />
                                        <div className={"ln_solid"}></div>
                                    </div>
                                </div>
                            </div>   
                        </div>

                        <div className={"row"}>
                            <div className={"col-md-12 col-sm-12 col-xs-12"}>
                                <div className={"x_panel"}>
                                    <div className={"x_title"}>
                                        <h2>Privacy <small>(AR)</small></h2>
                                        <div className={"clearfix"}></div>
                                    </div>
                                    <div className={"x_content"}>
                                        <MyStatefulEditor 
                                        value={this.state.config.privacy_ar}
                                        onChange={value => {
                                            this.setState(previousState => {
                                                return {
                                                    ...previousState,
                                                    config: {
                                                        ...previousState.config,
                                                        privacy_ar: value
                                                    }
                                                };
                                            });
                                        }}
                                        />
                                        <div className={"ln_solid"}></div>
                                    </div>
                                </div>
                            </div>   
                        </div>

                        <div className={"row"}>
                            <div className={"col-md-12 col-sm-12 col-xs-12"}>
                            <div className={"ln_solid"}></div>
                            <div className={"form-group"}>
                                <div className={"col-lg-6"}>
                                    <Button onClick={this._save.bind(this)} bsStyle="success">Submit</Button>
                                </div>
                                <div className={"col-lg-6"}>

                                </div>
                            </div>
                            </div>   
                        </div>
                    </div>}

                {this.state.loading && <div className={"css-jxiqlq-wrap"}>
                    <RingLoader
                        style={{margin: 'auto'}}
                        color={'#0019bc'}
                        loading={this.state.loading}
                    />
                </div>}
            </div>
        );
    }

}
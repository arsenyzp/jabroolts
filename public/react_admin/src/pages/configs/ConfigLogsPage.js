import React, {Component} from 'react';
import { render } from 'react-dom';
import {Button, Modal, Alert, Pagination} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { RingLoader } from 'react-spinners';
import axios from 'axios';
import { Link } from 'react-router';
import 'icheck/skins/all.css';
import {Checkbox, Radio} from 'react-icheck';
import {history} from '../../index';

export default class ConfigLogsPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            config: {},
            ready: false
        };
    }

    componentDidMount() {
        this._load();
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    }

    async _load() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.get(`/admin/config/logs/`, {
                params: {}
            });
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            }

            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false,
                    config: response.data.data,
                    ready: true
                };
            });

        } catch (e) {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
            toast.error(e.message);
        }
    }

    async _save() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.post(`/admin/config/logs/`, this.state.config);
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                this.setState(previousState => {
                    return {
                        ...previousState,
                        loading: false,
                        config: response.data.data
                    };
                }, () => {
                    toast.success("Item updated!");
                });
            }
        } catch (e) {
            console.log(e.response);
            if(!!e.response && !!e.response.data.errors && e.response.data.errors.length > 0) {
                e.response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                toast.error(e.message);
            }
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
        }
    }

    render() {
        return (
            <div>
                <div className={"row"}>
                    <div className={"col-md-12 col-sm-12 col-xs-12"}>
                        <div className={"x_panel"}>
                            <div className={"x_title"}>
                                <h2>Logs </h2>
                                <div className={"clearfix"}></div>
                            </div>
                            <div className={"x_content"}>
                                <br />
                                <form data-parsley-validate className={"form-horizontal form-label-left"}>

                                    {this.state.ready && !!this.state.config && <div className={"row"}>

                                        <div className={"col-lg-4"}>
                                            <div className={"form-group"}>
                                                <Checkbox
                                                    checkboxClass="icheckbox_flat-green"
                                                    increaseArea="20%"
                                                    label=" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Api log"
                                                    checked={this.state.config.api}
                                                    onChange={event => {
                                                        event.persist();
                                                        console.log(event.target.checked);
                                                        this.setState(previousState => {
                                                            return {
                                                                ...previousState,
                                                                config: {
                                                                    ...previousState.config,
                                                                    api: !!event.target.checked
                                                                }
                                                            };
                                                        })}}
                                                />
                                            </div>
                                        </div>

                                        <div className={"col-lg-4"}>
                                            <div className={"form-group"}>
                                                <Checkbox
                                                    checkboxClass="icheckbox_flat-green"
                                                    increaseArea="20%"
                                                    label=" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email log"
                                                    checked={this.state.config.email}
                                                    onChange={event => {
                                                        event.persist();
                                                        console.log(event.target.checked);
                                                        this.setState(previousState => {
                                                            return {
                                                                ...previousState,
                                                                config: {
                                                                    ...previousState.config,
                                                                    email: !!event.target.checked
                                                                }
                                                            };
                                                        })}}
                                                />
                                            </div>
                                        </div>

                                        <div className={"col-lg-4"}>
                                            <div className={"form-group"}>
                                                <Checkbox
                                                    checkboxClass="icheckbox_flat-green"
                                                    increaseArea="20%"
                                                    label=" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sms log"
                                                    checked={this.state.config.sms}
                                                    onChange={event => {
                                                        event.persist();
                                                        console.log(event.target.checked);
                                                        this.setState(previousState => {
                                                            return {
                                                                ...previousState,
                                                                config: {
                                                                    ...previousState.config,
                                                                    sms: !!event.target.checked
                                                                }
                                                            };
                                                        })}}
                                                />
                                            </div>
                                        </div>

                                    </div>}

                                    {this.state.ready && !this.state.config && <h3>Config info not found</h3> }

                                    <div className={"ln_solid"}></div>
                                    <div className={"form-group"}>
                                        <div className={"col-lg-6"}>
                                            <Button onClick={this._save.bind(this)} bsStyle="success">Submit</Button>
                                        </div>
                                        <div className={"col-lg-6"}>

                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.loading && <div className={"css-jxiqlq-wrap"}>
                    <RingLoader
                        style={{margin: 'auto'}}
                        color={'#0019bc'}
                        loading={this.state.loading}
                    />
                </div>}
            </div>
        );
    }

}
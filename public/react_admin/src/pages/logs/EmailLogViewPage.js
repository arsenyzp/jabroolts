import React, {Component} from 'react';
import { render } from 'react-dom';
import {Button, Modal, Alert, Pagination} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { RingLoader } from 'react-spinners';
import axios from 'axios';
import { Link } from 'react-router';
import 'icheck/skins/all.css';
import {Checkbox, Radio} from 'react-icheck';
import {history} from '../../index';
import moment from "moment/moment";

export default class  EmailLogViewPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            id: props.params.id,
            model: {},
            ready: false
        };
    }

    componentDidMount() {
        this._load();
    }

    async _load() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.get(`/admin/logs/email/${this.state.id}`, {
                params: {}
            });
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            }

            this.state.model = response.data.data;

            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false,
                    model: this.state.model,
                    ready: true
                };
            });

        } catch (e) {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
            toast.error(e.message);
        }
    }

    render() {
        return (
            <div>
                <div className={"row"}>
                    <div className={"col-md-12 col-sm-12 col-xs-12"}>
                        <div className={"x_panel"}>
                            <div className={"x_title"}>
                                <h2>Email {this.state.model.email}</h2>
                                <div className={"clearfix"}></div>
                            </div>
                            <div className={"x_content"}>
                                <br />
                                <form data-parsley-validate className={"form-horizontal form-label-left"}>

                                    {this.state.ready && !!this.state.model && <div className={"row"}>

                                        <table className={"table table-striped table-bordered detail-view"}>
                                            <tbody>

                                            <tr>
                                                <th>email</th>
                                                <td>{this.state.model.email}</td>
                                            </tr>

                                            <tr>
                                                <th>subject</th>
                                                <td>{this.state.model.subject}</td>
                                            </tr>

                                            <tr>
                                                <th>text</th>
                                                <td>
                                                    <code>
                                                        {this.state.model.text}
                                                    </code>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>Created at</th>
                                                <td>{moment(new Date(this.state.model.created_at)).format("DD-MM-YYYY hh:mm")}</td>
                                            </tr>

                                            </tbody>
                                        </table>

                                    </div>}

                                    {this.state.ready && !this.state.model && <h3>Info not found</h3> }

                                    <div className={"ln_solid"}></div>
                                    <div className={"form-group"}>
                                        <div className={"col-lg-6"}>
                                            <a className={"btn btn-primary"} href={"#/contact_us/"}>Cancel</a>
                                        </div>
                                        <div className={"col-lg-6"}>

                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.loading && <div className={"css-jxiqlq-wrap"}>
                    <RingLoader
                        style={{margin: 'auto'}}
                        color={'#0019bc'}
                        loading={this.state.loading}
                    />
                </div>}
            </div>
        );
    }

}
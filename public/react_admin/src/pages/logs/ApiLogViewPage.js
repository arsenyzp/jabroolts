import React, {Component} from 'react';
import { render } from 'react-dom';
import {Button, Modal, Alert, Pagination} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { RingLoader } from 'react-spinners';
import axios from 'axios';
import { Link } from 'react-router';
import 'icheck/skins/all.css';
import {Checkbox, Radio} from 'react-icheck';
import {history} from '../../index';
import moment from "moment/moment";

export default class  ApiLogViewPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            id: props.params.id,
            model: {},
            ready: false
        };
    }

    componentDidMount() {
        this._load();
    }

    async _load() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.get(`/admin/logs/api/${this.state.id}`, {
                params: {}
            });
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            }

            this.state.model = response.data.data;

            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false,
                    model: this.state.model,
                    ready: true
                };
            });

        } catch (e) {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
            toast.error(e.message);
        }
    }

    render() {
        return (
            <div>
                <div className={"row"}>
                    <div className={"col-md-12 col-sm-12 col-xs-12"}>
                        <div className={"x_panel"}>
                            <div className={"x_title"}>
                                <h2>Api {this.state.model.path}</h2>
                                <div className={"clearfix"}></div>
                            </div>
                            <div className={"x_content"}>
                                <br />
                                <form data-parsley-validate className={"form-horizontal form-label-left"}>

                                    {this.state.ready && !!this.state.model && <div className={"row"}>

                                        <table className={"table table-striped table-bordered detail-view"}>
                                            <tbody>

                                            <tr>
                                                <th>method</th>
                                                <td>
                                                    {this.state.model.method}
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>params</th>
                                                <td>
                                                    <p className={"logs"}>
                                                        {this.state.model.params}
                                                    </p>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>body</th>
                                                <td>
                                                    <p className={"logs"}>
                                                        {this.state.model.body}
                                                    </p>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>headers</th>
                                                <td>
                                                    <p className={"logs"}>
                                                        {this.state.model.headers}
                                                    </p>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>url</th>
                                                <td>
                                                    {this.state.model.url}
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>query</th>
                                                <td>
                                                    <p className={"logs"}>
                                                        {this.state.model.query}
                                                    </p>
                                                 </td>
                                            </tr>

                                            <tr>
                                                <th>rawHeaders</th>
                                                <td>
                                                    <p className={"logs"}>
                                                        {this.state.model.rawHeaders}
                                                    </p>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>path</th>
                                                <td>{this.state.model.path}</td>
                                            </tr>

                                            <tr>
                                                <th>status</th>
                                                <td>{this.state.model.status}</td>
                                            </tr>

                                            <tr>
                                                <th>response</th>
                                                <td>
                                                    <p className={"logs"}>
                                                        {this.state.model.response}
                                                    </p>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>Created at</th>
                                                <td>{moment(new Date(this.state.model.created_at)).format("DD-MM-YYYY hh:mm")}</td>
                                            </tr>

                                            </tbody>
                                        </table>

                                    </div>}

                                    {this.state.ready && !this.state.model && <h3>Info not found</h3> }

                                    <div className={"ln_solid"}></div>
                                    <div className={"form-group"}>
                                        <div className={"col-lg-6"}>
                                            <a className={"btn btn-primary"} href={"#/contact_us/"}>Cancel</a>
                                        </div>
                                        <div className={"col-lg-6"}>

                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.loading && <div className={"css-jxiqlq-wrap"}>
                    <RingLoader
                        style={{margin: 'auto'}}
                        color={'#0019bc'}
                        loading={this.state.loading}
                    />
                </div>}
            </div>
        );
    }

}
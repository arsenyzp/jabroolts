import React, {Component} from 'react';
import { render } from 'react-dom';
import {Button, Modal, Alert, Pagination} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { RingLoader } from 'react-spinners';
import axios from 'axios';
import { Link } from 'react-router';
import 'icheck/skins/all.css';
import {Checkbox, Radio} from 'react-icheck';
import {history} from '../index';

export default class CountryFormPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            id: props.params.id,
            country: {name: "", code: "", phone: ""},
            ready: false
        };
    }

    componentDidMount() {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
        if(!!this.state.id && this.state.id !== "new"){
            this._load();
        } else {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false,
                    ready: true
                };
            });
        }
    }

    async _load() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.get(`/admin/country/${this.state.id}`, {
                params: {}
            });
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            }

            this.state.country = response.data.data;

            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false,
                    country: this.state.country,
                    ready: true
                };
            });

        } catch (e) {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
            toast.error(e.message);
        }
    }

    async _save() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.post(`/admin/country`, this.state.country);
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                this.setState(previousState => {
                    return {
                        ...previousState,
                        loading: false,
                        country: {
                            ...previousState.country,
                            ...response.data.data
                        }
                    };
                }, () => {
                    toast.success("Item updated!");
                });
            }
        } catch (e) {
            console.log(e.response);
            if(!!e.response && !!e.response.data.errors && e.response.data.errors.length > 0) {
                e.response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                toast.error(e.message);
            }
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
        }
    }

    render() {
        return (
            <div>
                <div className={"row"}>
                    <div className={"col-md-12 col-sm-12 col-xs-12"}>
                        <div className={"x_panel"}>
                            <div className={"x_title"}>
                                {this.state.id !== 'new' &&  <h2>Edit country</h2>}
                                {this.state.id === 'new' &&  <h2>Add country</h2>}
                                <div className={"clearfix"}></div>
                            </div>
                            <div className={"x_content"}>
                                <br />
                                <form data-parsley-validate className={"form-horizontal form-label-left"}>

                                    {this.state.ready && !!this.state.country && <div className={"row"}>

                                        <div className={"col-lg-4"}>
                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Name <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        type="text"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.country.name}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    country: {
                                                                        ...previousState.country,
                                                                        name: event.target.value
                                                                    }
                                                                };
                                                            })}}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className={"col-lg-4"}>
                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Code <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        type="text"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.country.code}
                                                        onChange={event => {
                                                            event.persist();
                                                            var letters = /^[A-Za-z]+$/;
                                                            if(event.target.value.match(letters))
                                                            {
                                                                this.setState(previousState => {
                                                                    return {
                                                                        ...previousState,
                                                                        country: {
                                                                            ...previousState.country,
                                                                            code: event.target.value
                                                                        }
                                                                    };
                                                                })
                                                            }
                                                           }}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        {/*<div className={"col-lg-4"}>*/}
                                            {/*<div className={"form-group"}>*/}
                                                {/*<label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Phone code <span className={"required"}>*</span>*/}
                                                {/*</label>*/}
                                                {/*<div className={"col-md-6 col-sm-6 col-xs-12"}>*/}
                                                    {/*<input*/}
                                                        {/*type="text"*/}
                                                        {/*required="required"*/}
                                                        {/*className={"form-control col-md-7 col-xs-12"}*/}
                                                        {/*value={this.state.country.phone}*/}
                                                        {/*onChange={event => {*/}
                                                            {/*event.persist();*/}
                                                            {/*this.setState(previousState => {*/}
                                                                {/*return {*/}
                                                                    {/*...previousState,*/}
                                                                    {/*country: {*/}
                                                                        {/*...previousState.country,*/}
                                                                        {/*phone: event.target.value*/}
                                                                    {/*}*/}
                                                                {/*};*/}
                                                            {/*})}}*/}
                                                    {/*/>*/}
                                                {/*</div>*/}
                                            {/*</div>*/}
                                        {/*</div>*/}

                                    </div>}

                                    {this.state.ready && !this.state.country && <h3>Info not found</h3> }

                                    <div className={"ln_solid"}></div>
                                    <div className={"form-group"}>
                                        <div className={"col-lg-6"}>
                                            <a className={"btn btn-primary"} href={"#/countries/"}>Cancel</a>
                                            <Button onClick={this._save.bind(this)} bsStyle="success">Submit</Button>
                                        </div>
                                        <div className={"col-lg-6"}>

                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.loading && <div className={"css-jxiqlq-wrap"}>
                    <RingLoader
                        style={{margin: 'auto'}}
                        color={'#0019bc'}
                        loading={this.state.loading}
                    />
                </div>}
            </div>
        );
    }
    
}
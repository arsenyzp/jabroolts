import React, {Component} from 'react';
import { render } from 'react-dom';
import {Button, Modal, Alert, Pagination} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { RingLoader } from 'react-spinners';
import axios from 'axios';
import { Link } from 'react-router';
import 'icheck/skins/all.css';
import {Checkbox, Radio} from 'react-icheck';
import {history} from '../index';

export default class UserVehicle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            id: props.params.id,
            user: {jabroolid: ""},
            models: [],
            years: [],
            types: [],
            ready: false
        };
        this.fileInputs = {};
    }

    componentDidMount() {
        this._load();
    }

    _prepareModel() {
        if(!this.state.user.vehicle.type || this.state.user.vehicle.type === ''){
            if(this.state.types.length > 0){
                this.state.user.vehicle.type =  this.state.types[0]._id;
            }
        }
        if(!this.state.user.vehicle.year || this.state.user.vehicle.year === ''){
            this.state.user.vehicle.year =  this.state.years[0];
        }
        if(!this.state.user.vehicle.model || this.state.user.vehicle.model === ''){
            this.state.user.vehicle.model =  this.state.models[0]._id;
        }

        if(!this.state.user.vehicle.image_front || this.state.user.vehicle.image_front !== ''){
            this.state.user.vehicle.image_front_url =  "/uploads/user_vehicle/" + this.state.user.vehicle.image_front;
        }
        if(!this.state.user.vehicle.image_back || this.state.user.vehicle.image_back !== ''){
            this.state.user.vehicle.image_back_url =  "/uploads/user_vehicle/" + this.state.user.vehicle.image_back;
        }
        if(!this.state.user.vehicle.image_side || this.state.user.vehicle.image_side !== ''){
            this.state.user.vehicle.image_side_url =  "/uploads/user_vehicle/" + this.state.user.vehicle.image_side;
        }

        this.state.user.vehicle.images_insurance_url = [];
        this.state.user.vehicle.images_insurance_delete = [];
        for(let i = 0; i < this.state.user.vehicle.images_insurance.length; i++){
            this.state.user.vehicle.images_insurance_url.push("/uploads/user_vehicle/" + this.state.user.vehicle.images_insurance[i]);
        }
    }

    async _load() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.get(`/admin/user/vehicle/${this.state.id}`, {
                params: {}
            });
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            }

            this.state.user = response.data.data.user;
            this.state.models = response.data.data.models;
            this.state.types = response.data.data.types;
            this.state.years = response.data.data.years;

            this._prepareModel();

            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false,
                    user: this.state.user,
                    models: this.state.models,
                    types: this.state.types,
                    years: this.state.years,
                    ready: true
                };
            });

        } catch (e) {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
            toast.error(e.message);
        }
    }

    async _loadCarTypes() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.get(`/admin/manufacture/getTypes/${this.state.user.vehicle.model}`, {
                params: {}
            });
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            }

            this.state.types = response.data.data.types;

            this._prepareModel();

            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false,
                    types: this.state.types
                };
            });

        } catch (e) {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
            toast.error(e.message);
        }
    }

    async _save() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.post(`/admin/user/vehicle/${this.state.id}`, this.state.user.vehicle);
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                this.state.user = response.data.data;
                this._prepareModel();
                this.setState(previousState => {
                    return {
                        ...previousState,
                        loading: false,
                        user: this.state.user
                    };
                }, () => {
                    toast.success("Item updated!");
                });
            }
        } catch (e) {
            console.log(e.response);
            if(!!e.response && !!e.response.data.errors && e.response.data.errors.length > 0) {
                e.response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                toast.error(e.message);
            }
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
        }
    }

    async _preUpload(event, type) {
        if(this.fileInputs[type].files.length === 0) {
            return;
        }
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        let formdata = new FormData();
        formdata.append('image', this.fileInputs[type].files[0]);
        try {
            let response = await axios.post(`/admin/ajax/uploads_img`, formdata, {headers: {
                'Content-Type': 'multipart/form-data'
            }});
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                if(!!this.state.user.vehicle[type].forEach){
                    this.state.user.vehicle[type].push(response.data.data.image);
                    this.state.user.vehicle[type + '_url'].push(response.data.data.image_url);
                } else {
                    this.state.user.vehicle[type] = response.data.data.image;
                    this.state.user.vehicle[type + '_url'] = response.data.data.image_url;
                }
                this.fileInputs[type].value = '';
                this.setState(previousState => {
                    return {
                        ...previousState,
                        loading: false,
                        user: {
                            ...previousState.user,
                            vehicle: this.state.user.vehicle
                        }
                    };
                });
            }
        } catch (e) {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
            toast.error(e.message);
        }
    }

    render() {
        return (
            <div>
                <div className={"row"}>
                    <div className={"col-md-12 col-sm-12 col-xs-12"}>
                        <div className={"x_panel"}>
                            <div className={"x_title"}>
                                <h2>User Vehicle <small>{this.state.user.jabroolid} (Payment default from: {this.state.user.pay_type})</small></h2>
                                <div className={"clearfix"}></div>
                            </div>
                            <div className={"x_content"}>
                                <br />
                                <form data-parsley-validate className={"form-horizontal form-label-left"}>

                                    {this.state.ready && !!this.state.user.vehicle && <div className={"row"}>

                                        <div className={"col-md-6 col-sm-6 col-xs-6"}>

                                            <div className={"form-group"}>
                                                <div className={"text-center"}>
                                                    {this.state.user.vehicle.image_front !=="" && <img className={"img-circle profile_img"} style={{maxHeight: 100, width: "auto", margin: 20}} src={this.state.user.vehicle.image_front_url}/>}
                                                    {this.state.user.vehicle.image_front ==="" && <img className={"img-circle profile_img"} style={{maxHeight: 100, width: "auto", margin: 20}} src="/images/empty.png"/>}
                                                </div>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Image front</label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        type="file"
                                                        name="image_front"
                                                        ref={input => {
                                                            this.fileInputs.image_front = input;
                                                        }}
                                                        onChange={(event) => {
                                                            this._preUpload(event, 'image_front');
                                                        }}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"ln_solid"}></div>

                                            <div className={"form-group"}>
                                                <div className={"text-center"}>
                                                    {this.state.user.vehicle.image_back !=="" && <img className={"img-circle profile_img"} style={{maxHeight: 100, width: "auto", margin: 20}} src={this.state.user.vehicle.image_back_url}/>}
                                                    {this.state.user.vehicle.image_back ==="" && <img className={"img-circle profile_img"} style={{maxHeight: 100, width: "auto", margin: 20}} src="/images/empty.png"/>}
                                                </div>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Image back</label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        type="file"
                                                        name="image_back"
                                                        ref={input => {
                                                            this.fileInputs.image_back = input;
                                                        }}
                                                        onChange={(event) => {
                                                            this._preUpload(event, 'image_back');
                                                        }}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"ln_solid"}></div>

                                            <div className={"form-group"}>
                                                <div className={"text-center"}>
                                                    {this.state.user.vehicle.image_side !=="" && <img className={"img-circle profile_img"} style={{maxHeight: 100, width: "auto", margin: 20}} src={this.state.user.vehicle.image_side_url}/>}
                                                    {this.state.user.vehicle.image_side ==="" && <img className={"img-circle profile_img"} style={{maxHeight: 100, width: "auto", margin: 20}} src="/images/empty.png"/>}
                                                </div>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Image side</label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        type="file"
                                                        name="image_side"
                                                        ref={input => {
                                                            this.fileInputs.image_side = input;
                                                        }}
                                                        onChange={(event) => {
                                                            this._preUpload(event, 'image_side');
                                                        }}
                                                    />
                                                </div>
                                            </div>

                                        </div>

                                        <div className={"col-md-6 col-sm-6 col-xs-6"}>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Number <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        type="text"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.user.vehicle.number}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.vehicle,
                                                                        vehicle: {
                                                                            ...previousState.user.vehicle,
                                                                            number: event.target.value
                                                                        }
                                                                    }
                                                                };
                                                            })}}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Year <span className={"required"}>*</span></label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <select
                                                        className={"form-control"}
                                                        value={this.state.user.vehicle.year}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.vehicle,
                                                                        vehicle: {
                                                                            ...previousState.user.vehicle,
                                                                            year: event.target.value
                                                                        }
                                                                    }
                                                                };
                                                            })}}
                                                    >
                                                        { this.state.years.map((v, k, a) => {
                                                            return (
                                                                <option value={v} key={k}>{v}</option>
                                                            );
                                                        })}
                                                    </select>
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Model <span className={"required"}>*</span></label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <select
                                                        className={"form-control"}
                                                        value={this.state.user.vehicle.model}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.vehicle,
                                                                        vehicle: {
                                                                            ...previousState.user.vehicle,
                                                                            model: event.target.value
                                                                        }
                                                                    }
                                                                };
                                                            }, () => {
                                                                this._loadCarTypes();
                                                            })}}
                                                    >
                                                        { this.state.models.map((v, k, a) => {
                                                            return (
                                                                <option value={v._id} key={k}>{v.name}</option>
                                                            );
                                                        })}
                                                    </select>
                                                </div>
                                            </div>

                                            {this.state.types.length > 0 && <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Type <span className={"required"}>*</span></label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <select
                                                        className={"form-control"}
                                                        value={this.state.user.vehicle.type}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.vehicle,
                                                                        vehicle: {
                                                                            ...previousState.user.vehicle,
                                                                            type: event.target.value
                                                                        }
                                                                    }
                                                                };
                                                            })}}
                                                    >
                                                        { this.state.types.map((v, k, a) => {
                                                            return (
                                                                <option value={v._id} key={k}>{v.name}</option>
                                                            );
                                                        })}
                                                    </select>
                                                </div>
                                            </div>}

                                            <div className={"ln_solid"}></div>

                                            <div className={"form-group"}>
                                                <div className={"text-center"}>

                                                    { this.state.user.vehicle.images_insurance_url.map((v, k, a) => {
                                                        return (
                                                            <img className={"img-circle profile_img"} style={{maxHeight: 100, width: "auto", margin: 20}} key={k} src={v}/>
                                                        );
                                                    })}

                                                </div>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Image insurance</label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        type="file"
                                                        name="avatar"
                                                        ref={input => {
                                                            this.fileInputs.images_insurance = input;
                                                        }}
                                                        onChange={(event) => {
                                                            this._preUpload(event, 'images_insurance');
                                                        }}
                                                    />
                                                </div>
                                            </div>

                                        </div>

                                    </div>}

                                    {this.state.ready && !this.state.user.vehicle && <h3>Vehicle info not found</h3> }

                                    <div className={"ln_solid"}></div>
                                    <div className={"form-group"}>
                                        <div className={"col-lg-6"}>
                                            <a className={"btn btn-primary"} href={"#/users/" + this.state.id}>Cancel</a>
                                            <Button onClick={this._save.bind(this)} bsStyle="success">Submit</Button>
                                        </div>
                                        <div className={"col-lg-6"}>

                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.loading && <div className={"css-jxiqlq-wrap"}>
                    <RingLoader
                        style={{margin: 'auto'}}
                        color={'#0019bc'}
                        loading={this.state.loading}
                    />
                </div>}
            </div>
        );
    }

}
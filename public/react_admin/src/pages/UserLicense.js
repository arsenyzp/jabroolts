import React, {Component} from 'react';
import { render } from 'react-dom';
import {Button, Modal, Alert, Pagination} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { RingLoader } from 'react-spinners';
import axios from 'axios';
import { Link } from 'react-router';
import 'icheck/skins/all.css';
import {Checkbox, Radio} from 'react-icheck';
import {history} from '../index';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { formatDate, parseDate } from 'react-day-picker/moment';
import 'react-day-picker/lib/style.css';
import moment from 'moment';

export default class UserLicense extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            id: props.params.id,
            user: {jabroolid: ""},
            countries: [],
            ready: false
        };
        this.fileInputs = {};
    }

    componentDidMount() {
        this._load();
    }

    _prepareModel() {
        if(!this.state.user.license.country_code || this.state.user.license.country_code === ''){
            this.state.user.license.country_code =  this.state.countries[0].country_code;
        }

        if(!this.state.user.license.issue_date || this.state.user.license.issue_date === ''){
            this.state.user.license.issue_date =  new Date();
        }
        this.state.user.license.issue_date = new Date(this.state.user.license.issue_date);
        this.state.user.license.issue_date_str =  moment(this.state.user.license.issue_date).format("DD/MM/YYYY");

        if(!this.state.user.license.expiry_date || this.state.user.license.expiry_date === ''){
            this.state.user.license.expiry_date = new Date();
        }
        this.state.user.license.expiry_date = new Date(this.state.user.license.expiry_date);
        this.state.user.license.expiry_date_str =  moment(this.state.user.license.expiry_date).format("DD/MM/YYYY");

        if(!this.state.user.license.image || this.state.user.license.image !== ''){
            this.state.user.license.image_url =  "/uploads/user_license/" + this.state.user.license.image;
        }
    }

    async _load() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.get(`/admin/user/license/${this.state.id}`, {
                params: {}
            });
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            }

            this.state.user = response.data.data.user;
            this.state.countries = response.data.data.countries;

            this._prepareModel();

            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false,
                    user: this.state.user,
                    countries: this.state.countries,
                    ready: true
                };
            });

        } catch (e) {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
            toast.error(e.message);
        }
    }

    async _save() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        // this.state.user.license.expiry_date = moment(this.state.user.license.expiry_date_str, 'DD/MM/YYYY').format("X");
        // this.state.user.license.issue_date = moment(this.state.user.license.issue_date_str, 'DD/MM/YYYY').format("X");
        try {
            let response = await axios.post(`/admin/user/license/${this.state.id}`, this.state.user.license);
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                this.state.user = response.data.data;
                this._prepareModel();
                this.setState(previousState => {
                    return {
                        ...previousState,
                        loading: false,
                        user: this.state.user
                    };
                }, () => {
                    toast.success("Item updated!");
                });
            }
        } catch (e) {
            console.log(e.response);
            if(!!e.response && !!e.response.data.errors && e.response.data.errors.length > 0) {
                e.response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                toast.error(e.message);
            }
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
        }
    }

    async _preUpload(event, type) {
        if(this.fileInputs[type].files.length === 0) {
            return;
        }
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        let formdata = new FormData();
        formdata.append('image', this.fileInputs[type].files[0]);
        try {
            let response = await axios.post(`/admin/ajax/uploads_img`, formdata, {headers: {
                'Content-Type': 'multipart/form-data'
            }});
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                if(!!this.state.user.license[type].forEach){
                    this.state.user.license[type].push(response.data.data.image);
                    this.state.user.license[type + '_url'].push(response.data.data.image_url);
                } else {
                    this.state.user.license[type] = response.data.data.image;
                    this.state.user.license[type + '_url'] = response.data.data.image_url;
                }
                this.fileInputs[type].value = '';
                this.setState(previousState => {
                    return {
                        ...previousState,
                        loading: false,
                        user: {
                            ...previousState.user,
                            license: this.state.user.license
                        }
                    };
                });
            }
        } catch (e) {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
            toast.error(e.message);
        }
    }

    render() {
        return (
            <div>
                <div className={"row"}>
                    <div className={"col-md-12 col-sm-12 col-xs-12"}>
                        <div className={"x_panel"}>
                            <div className={"x_title"}>
                                <h2>User License <small>{this.state.user.jabroolid} (Payment default from: {this.state.user.pay_type})</small></h2>
                                <div className={"clearfix"}></div>
                            </div>
                            <div className={"x_content"}>
                                <br />
                                <form data-parsley-validate className={"form-horizontal form-label-left"}>

                                    {this.state.ready && !!this.state.user.license && <div className={"row"}>

                                        <div className={"col-md-6 col-sm-6 col-xs-6"}>

                                            <div className={"form-group"}>
                                                <div className={"text-center"}>
                                                    {this.state.user.license.image !=="" && <img className={"img-circle profile_img"} style={{maxHeight: 100, width: "auto", margin: 20}} src={this.state.user.license.image_url}/>}
                                                    {this.state.user.license.image ==="" && <img className={"img-circle profile_img"} style={{maxHeight: 100, width: "auto", margin: 20}} src="/images/empty.png"/>}
                                                </div>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Image side</label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        type="file"
                                                        name="image_side"
                                                        ref={input => {
                                                            this.fileInputs.image = input;
                                                        }}
                                                        onChange={(event) => {
                                                            this._preUpload(event, 'image');
                                                        }}
                                                    />
                                                </div>
                                            </div>

                                        </div>

                                        <div className={"col-md-6 col-sm-6 col-xs-6"}>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Number <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        type="text"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.user.license.number}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.license,
                                                                        license: {
                                                                            ...previousState.user.license,
                                                                            number: event.target.value
                                                                        }
                                                                    }
                                                                };
                                                            })}}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Name <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        type="text"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.user.license.name}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.license,
                                                                        license: {
                                                                            ...previousState.user.license,
                                                                            name: event.target.value
                                                                        }
                                                                    }
                                                                };
                                                            })}}
                                                    />
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Country <span className={"required"}>*</span></label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <select
                                                        className={"form-control"}
                                                        value={this.state.user.license.country_code}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    user: {
                                                                        ...previousState.license,
                                                                        license: {
                                                                            ...previousState.user.license,
                                                                            country_code: event.target.value
                                                                        }
                                                                    }
                                                                };
                                                            })}}
                                                    >
                                                        { this.state.countries.map((v, k, a) => {
                                                            return (
                                                                <option value={v.code} key={k}>{v.name}</option>
                                                            );
                                                        })}
                                                    </select>
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Issue date <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <span className="InputFromTo-to">
                                                        <DayPickerInput
                                                            className={"form-control col-md-7 col-xs-12"}
                                                            value={this.state.user.license.issue_date_str}
                                                            placeholder="Issue date"
                                                            locale={'en'}
                                                            formatDate={formatDate}
                                                            parseDate={parseDate}
                                                            format={"DD/MM/YYYY"}
                                                            dayPickerProps={{
                                                                numberOfMonths: 1,
                                                                className: "form-control col-md-7 col-xs-12"
                                                            }}
                                                            inputProps={{
                                                                className: "form-control col-md-7 col-xs-12"
                                                            }}
                                                            onDayChange={(from) => {
                                                                console.log(from);
                                                                this.setState(previousState => {
                                                                    return {
                                                                        ...previousState,
                                                                        user: {
                                                                            ...previousState.user,
                                                                            license: {
                                                                                ...previousState.user.license,
                                                                                issue_date: from
                                                                            }
                                                                        }
                                                                    };
                                                                });
                                                            }}
                                                        />
                                                    </span>
                                                </div>
                                            </div>

                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Expiry date <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <span className="InputFromTo-to">
                                                        <DayPickerInput
                                                            className={"form-control col-md-7 col-xs-12"}
                                                            value={this.state.user.license.expiry_date_str}
                                                            placeholder="Expiry date"
                                                            locale={'en'}
                                                            formatDate={formatDate}
                                                            parseDate={parseDate}
                                                            format={"DD/MM/YYYY"}
                                                            dayPickerProps={{
                                                                numberOfMonths: 1,
                                                                className: "form-control col-md-7 col-xs-12"
                                                            }}
                                                            inputProps={{
                                                                className: "form-control col-md-7 col-xs-12"
                                                            }}
                                                            onDayChange={(from) => {
                                                                console.log(from);
                                                                this.setState(previousState => {
                                                                    return {
                                                                        ...previousState,
                                                                        user: {
                                                                            ...previousState.license,
                                                                            license: {
                                                                                ...previousState.user.license,
                                                                                expiry_date: from
                                                                            }
                                                                        }
                                                                    };
                                                                });
                                                            }}
                                                        />
                                                    </span>
                                                </div>
                                            </div>

                                        </div>

                                    </div>}

                                    {this.state.ready && !this.state.user.license && <h3>License info not found</h3> }

                                    <div className={"ln_solid"}></div>
                                    <div className={"form-group"}>
                                        <div className={"col-lg-6"}>
                                            <a className={"btn btn-primary"} href={"#/users/" + this.state.id}>Cancel</a>
                                            <Button onClick={this._save.bind(this)} bsStyle="success">Submit</Button>
                                        </div>
                                        <div className={"col-lg-6"}>

                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.loading && <div className={"css-jxiqlq-wrap"}>
                    <RingLoader
                        style={{margin: 'auto'}}
                        color={'#0019bc'}
                        loading={this.state.loading}
                    />
                </div>}

            </div>
        );
    }

}
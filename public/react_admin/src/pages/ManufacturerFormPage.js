import React, {Component} from 'react';
import { render } from 'react-dom';
import {Button, Modal, Alert, Pagination} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { RingLoader } from 'react-spinners';
import axios from 'axios';
import { Link } from 'react-router';
import 'icheck/skins/all.css';
import {Checkbox, Radio} from 'react-icheck';
import {history} from '../index';
import theme from 'react-images/lib/theme';

export default class ManufacturerFormPage extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            id: props.params.id,
            manufacture: {
                name: '',
                types: []
            }
        };
    }

    componentDidMount() {
        if(!!this.state.id && this.state.id !== "new"){
            this._load();
        } else {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
        }
    }

    async _load() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.get(`/admin/manufacture/${this.state.id}`, {
                params: {}
            });
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            }

            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false,
                    manufacture: response.data.data
                };
            });

        } catch (e) {
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
            toast.error(e.message);
        }
    }

    async _save() {
        this.setState(previousState => {
            return {
                ...previousState,
                loading: true
            };
        });
        try {
            let response = await axios.post(`/admin/manufacture`, this.state.manufacture);
            if(!!response.data.errors && response.data.errors.length > 0) {
                response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                if(this.state.id === "new") {
                    this.setState(previousState => {
                        return {
                            ...previousState,
                            manufacture: response.data.data,
                            id: response.data.data._id,
                            loading: false
                        };
                    }, () => {
                        toast.success("Item created!");
                        history.push('/manufacturers/' + response.data.data._id);
                    });
                } else {
                    this.setState(previousState => {
                        return {
                            ...previousState,
                            loading: false,
                            manufacture: response.data.data,
                            id: response.data.data._id
                        };
                    }, () => {
                        toast.success("Item updated!");
                    });
                }
            }
        } catch (e) {
            console.log(e.response);
            if(!!e.response && !!e.response.data.errors && e.response.data.errors.length > 0) {
                e.response.data.errors.map((err, i) => {
                    toast.error(err);
                });
            } else {
                toast.error(e.message);
            }
            this.setState(previousState => {
                return {
                    ...previousState,
                    loading: false
                };
            });
        }
    }

    _addType() {
        this.state.manufacture.types.map((v, k, a) => {
            this.state.manufacture.types[k].editable = false;
        });
        this.state.manufacture.types.push({
            name: '',
            size: 1,
            editable: true
        });
        this.setState(previousState => {
            return {
                ...previousState,
                manufacture: {
                    ...previousState.manufacture,
                    types: this.state.manufacture.types
                }
            };
        });
    }

    _renderRow(item, i) {
        return (
            <tr key={i}>
                <td>
                    {!item.editable && item.name}
                    {item.editable &&
                    <input
                        type="text"
                        required="required"
                        className={"form-control col-md-7 col-xs-12"}
                        value={ this.state.manufacture.types[i].name }
                        onChange={event => {
                            event.persist();
                            this.state.manufacture.types[i].name = event.target.value;
                            this.setState(previousState => {
                                return {
                                    ...previousState,
                                    manufacture: {
                                        ...previousState.manufacture,
                                        types: this.state.manufacture.types
                                    }
                                };
                            })}}
                    />}
                </td>
                <td>
                    {!item.editable &&  item.size}
                    {item.editable &&  <input
                        type="number"
                        required="required"
                        className={"form-control col-md-7 col-xs-12"}
                        value={ this.state.manufacture.types[i].size }
                        onChange={event => {
                            event.persist();
                            this.state.manufacture.types[i].size = event.target.value;
                            this.setState(previousState => {
                                return {
                                    ...previousState,
                                    manufacture: {
                                        ...previousState.manufacture,
                                        types: this.state.manufacture.types
                                    }
                                };
                            })}}
                    />}
                </td>
                <td className={"text-center"} style={{width: 120}}>
                    <Button bsStyle="link" onClick={() => {
                        this.state.manufacture.types[i].editable = !this.state.manufacture.types[i].editable;
                        this.setState(previousState => {
                            return {
                                ...previousState,
                                manufacture: {
                                    ...previousState.manufacture,
                                    types: this.state.manufacture.types
                                }
                            };
                        });
                    }} >
                        {!item.editable && <i className={"fa fa-pencil"} />}
                        {item.editable && <i className={"fa fa-check"} />}
                    </Button>
                    <Button bsStyle="link" onClick={() => {
                        this.state.manufacture.types.splice(i, 1);
                        this.setState(previousState => {
                            return {
                                ...previousState,
                                manufacture: {
                                    ...previousState.manufacture,
                                    types: this.state.manufacture.types
                                }
                            };
                        });
                    }} ><i className={"fa fa-trash"} /></Button>
                </td>
            </tr>
        );
    }

    render() {
        return (
            <div>
                <div className={"row"}>
                    <div className={"col-md-12 col-sm-12 col-xs-12"}>
                        <div className={"x_panel"}>
                            <div className={"x_title"}>
                               {this.state.id !== 'new' &&  <h2>Edit car type</h2>}
                               {this.state.id === 'new' &&  <h2>Add car type</h2>}
                                <div className={"clearfix"}></div>
                            </div>
                            <div className={"x_content"}>
                                <br />
                                <form data-parsley-validate className={"form-horizontal form-label-left"}>

                                    <div className={"row"}>

                                        <div className={"col-md-6 col-sm-6 col-xs-6"}>
                                            <h3>Car type</h3>
                                            <div className={"form-group"}>
                                                <label className={"control-label col-md-3 col-sm-3 col-xs-12"}>Name <span className={"required"}>*</span>
                                                </label>
                                                <div className={"col-md-6 col-sm-6 col-xs-12"}>
                                                    <input
                                                        type="text"
                                                        required="required"
                                                        className={"form-control col-md-7 col-xs-12"}
                                                        value={this.state.manufacture.name}
                                                        onChange={event => {
                                                            event.persist();
                                                            this.setState(previousState => {
                                                                return {
                                                                    ...previousState,
                                                                    manufacture: {
                                                                        ...previousState.manufacture,
                                                                        name: event.target.value
                                                                    }
                                                                };
                                                            })}}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className={"col-md-6 col-sm-6 col-xs-6"}>
                                            <h3>Car models</h3>
                                            <table id="datatable-responsive" className={"table table-striped table-bordered dt-responsive nowrap"} cellSpacing="0" width="100%">
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Size</th>
                                                    <th style={{width: 120}}></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {this.state.manufacture.types.map((item, i) => {
                                                    return this._renderRow(item, i);
                                                })}
                                                </tbody>
                                            </table>
                                            <Button onClick={this._addType.bind(this)} bsStyle="success">Add type</Button>
                                        </div>

                                    </div>

                                    <div className={"ln_solid"}></div>
                                    <div className={"form-group"}>
                                        <div className={"col-lg-6"}>
                                            <a className={"btn btn-primary"} href={"#/manufacturers/"}>Cancel</a>
                                            <Button onClick={this._save.bind(this)} bsStyle="success">Submit</Button>
                                        </div>
                                        <div className={"col-lg-6"}>

                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.loading && <div className={"css-jxiqlq-wrap"}>
                    <RingLoader
                        style={{margin: 'auto'}}
                        color={'#0019bc'}
                        loading={this.state.loading}
                    />
                </div>}
            </div>
        );
    }

}
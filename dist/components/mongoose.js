"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("./config");
const Log_1 = require("./Log");
const mongoose = require("mongoose");
const log = Log_1.Log.getInstanse()(module);
class ConnectionDb {
    static init() {
        if (!ConnectionDb.connect) {
            mongoose.Promise = global.Promise;
            ConnectionDb.connect = mongoose.createConnection(config_1.AppConfig.getInstanse().get("mongoose:uri"));
            ConnectionDb.connect.on("error", err => {
                log.error("connection error:", err.message);
            });
            ConnectionDb.connect.once("open", () => {
                log.info("Connected to DB!");
            });
        }
    }
    static getInstanse() {
        ConnectionDb.init();
        return ConnectionDb.connect;
    }
}
exports.ConnectionDb = ConnectionDb;

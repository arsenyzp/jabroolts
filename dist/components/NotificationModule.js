"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const async = require("async");
const UserModel_1 = require("../models/UserModel");
const NotificationModel_1 = require("../models/NotificationModel");
const config_1 = require("./config");
const Log_1 = require("./Log");
const i18n = require("i18n");
const FCM = require("fcm-push");
const fcm = new FCM(config_1.AppConfig.getInstanse().get("push:gcm_sender"));
const log = Log_1.Log.getInstanse()(module);
class NotificationModule {
    sendPush(ids, payload) {
        let users_notified = [];
        UserModel_1.UserModel
            .find({ $or: [{ push_device_ios: { $in: ids } }, { push_device_android: { $in: ids } }] })
            .then(users => {
            for (let i = 0; i < users.length; i++) {
                if (users_notified.indexOf(users[i]._id.toString()) < 0) {
                    users_notified.push(users[i]._id.toString());
                    let push_ids = users[i].push_device_ios.concat(users[i].push_device_android);
                    for (let j = 0; j < push_ids.length; j++) {
                        payload.to = push_ids[j];
                        payload.priority = "high";
                        log.info("PUSH");
                        console.log(payload);
                        fcm.send(payload)
                            .then(response => {
                            log.info("Successfully sent with response: ", response);
                        })
                            .catch(err => {
                            log.info("Something has gone wrong!");
                            log.error(err);
                        });
                    }
                }
            }
        })
            .catch(err => {
            log.error(err);
        });
    }
    getNotificationObj(user, type, data) {
        i18n.setLocale(user.local);
        let obj = new NotificationObj();
        switch (type) {
            case exports.NotificationTypes.StartDelivery:
                obj.title = i18n.__("api.push.StartDeliveryTitle");
                obj.text = i18n.__("api.push.StartDeliveryText");
                break;
            case exports.NotificationTypes.Delivered:
                obj.title = i18n.__("api.push.DeliveredTitle");
                obj.text = i18n.__("api.push.DeliveredText");
                break;
            case exports.NotificationTypes.PaymentMade:
                obj.title = i18n.__("api.push.PaymentTitle");
                obj.text = i18n.__("api.push.PaymentText");
                break;
            case exports.NotificationTypes.RequestAccepted:
                obj.title = i18n.__("api.push.RequestAcceptedTitle");
                obj.text = i18n.__("api.push.RequestAcceptedText");
                break;
            case exports.NotificationTypes.RequestCancelled:
                obj.title = i18n.__("api.push.RequestCancelledTitle");
                obj.text = i18n.__("api.push.RequestCancelledText");
                break;
            case exports.NotificationTypes.CourierOnWay:
                obj.title = i18n.__("api.push.CourierOnWayTitle");
                obj.text = i18n.__("api.push.CourierOnWayText");
                break;
            case exports.NotificationTypes.StartReturn:
                obj.title = i18n.__("api.push.StartReturnTitle");
                obj.text = i18n.__("api.push.StartReturnText");
                break;
            case exports.NotificationTypes.CancelReturn:
                obj.title = i18n.__("api.push.CancelReturnTitle");
                obj.text = i18n.__("api.push.CancelReturnText");
                break;
            case exports.NotificationTypes.CourierChanged:
                if (data.isChanged) {
                    obj.title = i18n.__("api.push.CourierChangeOrderTitle");
                    obj.text = i18n.__("api.push.CourierChangeOrderText");
                }
                else {
                    obj.title = i18n.__("api.push.AcceptedByCourierTitle");
                    obj.text = i18n.__("api.push.AcceptedByCourierText");
                }
                break;
            case exports.NotificationTypes.AtDestination:
                obj.title = i18n.__("api.push.CourierAtDestinationTitle");
                obj.text = i18n.__("api.push.CourierAtDestinationText");
                break;
            case exports.NotificationTypes.ConfirmPickupCode:
                obj.title = i18n.__("api.push.ConfirmPickupCodeTitle");
                obj.text = i18n.__("api.push.ConfirmPickupCodeText", data.confirm_code);
                break;
            case exports.NotificationTypes.ConfirmReturnCode:
                obj.title = i18n.__("api.push.ConfirmReturnCodeTitle");
                obj.text = i18n.__("api.push.ConfirmReturnCodeText", data.confirm_code);
                break;
            case exports.NotificationTypes.ConfirmDeliveryCode:
                obj.title = i18n.__("api.push.ConfirmDeliveryCodeTitle");
                obj.text = i18n.__("api.push.ConfirmDeliveryCodeText", data.confirm_code);
                break;
            case exports.NotificationTypes.AtReturnDestination:
                obj.title = i18n.__("api.push.CourierAtReturnDestinationTitle");
                obj.text = i18n.__("api.push.CourierAtReturnDestinationText");
                break;
            case exports.NotificationTypes.NewReview:
                obj.title = i18n.__("api.push.NewReviewTitle");
                obj.text = i18n.__("api.push.NewReviewText", data.rate);
                break;
            case exports.NotificationTypes.Friend:
                obj.title = i18n.__("api.push.NewFriendTitle", data.inviteBonuses, data.jabroolID);
                obj.text = i18n.__("api.push.NewFriendText", data.inviteBonuses, data.jabroolID);
                break;
            case exports.NotificationTypes.Start:
                obj.title = i18n.__("api.push.CourierStartDeliveryTitle");
                obj.text = i18n.__("api.push.CourierStartDeliveryText");
                break;
            case exports.NotificationTypes.DeliveryCanceled:
                obj.title = i18n.__("api.push.DeliveryCanceledTitle");
                obj.text = i18n.__("api.push.DeliveryCanceledText");
                break;
            case exports.NotificationTypes.Activated:
                obj.title = i18n.__("api.push.AccountActivatedTitle");
                obj.text = i18n.__("api.push.AccountActivatedText");
                break;
            case exports.NotificationTypes.ReturnMoney:
                obj.title = i18n.__("api.push.ReturnMoneyTitle");
                obj.text = i18n.__("api.push.ReturnMoneyText");
                break;
            case exports.NotificationTypes.CourierIsNearDestination:
                obj.title = i18n.__("api.push.CourierIsNearTitle");
                obj.text = i18n.__("api.push.CourierIsNearText");
                break;
            case exports.NotificationTypes.CourierIsNearAtPickUp:
                obj.title = i18n.__("api.push.CourierIsNearAtPickUpTitle");
                obj.text = i18n.__("api.push.CourierIsNearAtPickUpText");
                break;
            case exports.NotificationTypes.ReachedCashLimits:
                obj.title = i18n.__("api.push.ReachedCashLimitsTitle");
                obj.text = i18n.__("api.push.ReachedCashLimitsText");
                break;
            case exports.NotificationTypes.RequestCancelledNeedReturn:
                obj.title = i18n.__("api.push.RequestCancelledNeedReturn", data.returnCost);
                obj.text = i18n.__("api.push.RequestCancelledNeedReturn", data.returnCost);
                break;
            case exports.NotificationTypes.JabroolDebt:
                obj.title = i18n.__("api.push.JabroolDebtTitle", data.debt);
                obj.text = i18n.__("api.push.JabroolDebtText", data.debt);
                break;
            case exports.NotificationTypes.JabroolMessage:
                obj.title = i18n.__("api.push.JabroolMessage");
                obj.text = data.text;
                break;
            case exports.NotificationTypes.NumberRequest:
                obj.title = i18n.__("api.push.NumberRequest");
                obj.text = i18n.__("api.push.NumberRequest");
                break;
            case exports.NotificationTypes.NumberUpdated:
                obj.title = i18n.__("api.push.NumberUpdated");
                obj.text = i18n.__("api.push.NumberUpdated");
                break;
            case exports.NotificationTypes.PhoneOtpSent:
                obj.title = i18n.__("api.push.PhoneOtpSent");
                obj.text = i18n.__("api.push.PhoneOtpSent");
                break;
            case exports.NotificationTypes.PasswordOtpSent:
                obj.title = i18n.__("api.push.PasswordOtpSent");
                obj.text = i18n.__("api.push.PasswordOtpSent");
                break;
        }
        return obj;
    }
    send(user_ids, type, data, data_id) {
        async.forEachOfSeries(user_ids, (user_id, key, cb) => __awaiter(this, void 0, void 0, function* () {
            try {
                let user = yield UserModel_1.UserModel.findOne({ _id: user_id });
                if (user === null) {
                    cb();
                    return;
                }
                let notification = this.getNotificationObj(user, type, data);
                let model = new NotificationModel_1.NotificationModel({
                    user: user_id.toString(),
                    ref_id: data_id.toString(),
                    type: type,
                    text: notification.text,
                    title: notification.title
                });
                yield model.save();
                let push_data = new PayloadPush();
                if (data.push_data) {
                    push_data = data.push_data;
                }
                push_data.data = model.getPublicFields();
                push_data.type = type;
                push_data.notification_id = model._id;
                push_data.notification.title = notification.title;
                push_data.notification.alert = notification.title;
                push_data.notification.body = notification.text;
                push_data.notification.badge = user.unread_count;
                this.sendPush(user.push_device_android.concat(user.push_device_ios), push_data);
                cb();
            }
            catch (e) {
                log.error(e);
                cb(e);
            }
        }));
    }
    static getInstance() {
        return new NotificationModule();
    }
}
exports.NotificationModule = NotificationModule;
class PayloadPush {
    constructor() {
        this.priority = "high";
        this.notification = new PushNotificationItem();
    }
}
class PushNotificationItem {
    constructor() {
        this.expiry = Math.floor(Date.now() / 1000) + 3600;
        this.icon = "ic_logo_notification";
        this.sound = "ping.aiff";
        this.click_action = "OPEN_NOTIFICATION";
    }
}
class NotificationObj {
    get text() {
        return this._text;
    }
    set text(value) {
        this._text = value;
    }
    get title() {
        return this._title;
    }
    set title(value) {
        this._title = value;
    }
}
exports.NotificationTypes = {
    StartDelivery: "start_delivery",
    Delivered: "delivered",
    PaymentMade: "payment_made",
    RequestAccepted: "request_accepted",
    RequestCancelled: "request_canceled",
    RequestCancelledNeedReturn: "request_canceled_need_return",
    CourierOnWay: "courier_on_way",
    StartReturn: "start_return",
    CancelReturn: "cancel_return",
    DeliveryCanceled: "delivery_canceled",
    CourierChanged: "courier_changed",
    AtDestination: "at_destination",
    AtReturnDestination: "at_return_destination",
    ConfirmPickupCode: "confirm_pickup_code",
    ConfirmDeliveryCode: "confirm_delivery_code",
    ConfirmReturnCode: "confirm_return_code",
    NewReview: "new_review",
    Friend: "friend",
    Start: "start",
    Activated: "activated",
    ReturnMoney: "return_money",
    CourierIsNearDestination: "courier_is_near_destination",
    CourierIsNearAtPickUp: "courier_is_near_at_pick_up",
    Message: "message",
    ReachedCashLimits: "reached_cash_limits",
    JabroolMessage: "jabrool_message",
    JabroolDebt: "jabrool_debt",
    NumberRequest: "number_request",
    NumberUpdated: "number_updated",
    PhoneOtpSent: "phone_otp_sent",
    PasswordOtpSent: "password_otp_sent"
};

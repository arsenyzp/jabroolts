"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const UserModel_1 = require("../models/UserModel");
class UserAuth {
    static check(req, res, next) {
        if (req.session.user) {
            if (req.session.user.role === UserModel_1.UserRoles.user
                && req.session.user.status !== UserModel_1.UserStatus.Deleted) {
                return next();
            }
        }
        res.redirect("/");
    }
}
exports.UserAuth = UserAuth;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ApiResponse_1 = require("./ApiResponse");
const ApiLogModel_1 = require("../models/logs/ApiLogModel");
const LogsConfig_1 = require("../models/configs/LogsConfig");
const Log_1 = require("./Log");
const log = Log_1.Log.getInstanse()(module);
class ApiParser {
    static parser(req, res, next) {
        if (req.header("Local") && req.header("Local") !== "") {
            req.setLocale(req.header("Local"));
        }
        if (req.body.params) {
            let model = new ApiLogModel_1.ApiLogModel();
            let logConfig = new LogsConfig_1.LogsConfig();
            logConfig
                .getInstanse()
                .then((conf) => {
                if (conf.api) {
                    model.params = JSON.stringify(req.params);
                    let body = JSON.stringify(req.body);
                    if (body && body.length > 2000) {
                        model.body = body.substring(0, 2000);
                    }
                    else {
                        model.body = body;
                    }
                    model.headers = JSON.stringify(req.headers);
                    model.rawHeaders = JSON.stringify(req.rawHeaders);
                    model.url = req.url;
                    model.method = req.method;
                    model.query = JSON.stringify(req.query);
                    model
                        .save();
                }
            })
                .catch(err => {
                log.error(err);
            });
            res.apiJson = (data) => {
                logConfig
                    .getInstanse()
                    .then((conf) => {
                    if (conf.api) {
                        model.response = JSON.stringify(data);
                        model
                            .save();
                    }
                })
                    .catch(err => {
                    log.error(err);
                });
                return res.json(data);
            };
            req.body = req.body.params;
            next();
        }
        else {
            let apiResponse = new ApiResponse_1.ApiResponse();
            apiResponse.addErrorMessage("request", "Error parse request");
            res.status(400);
            res.json(apiResponse.get());
        }
    }
}
exports.ApiParser = ApiParser;

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const AxiosInstanse_1 = require("./AxiosInstanse");
const moment = require("moment");
exports.jid = 1;
exports.eid = 2;
class QoyodComponent {
    static init() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result = yield AxiosInstanse_1.AxiosInstanse.get().post("login", {
                    email: "hamadsj@hotmail.com",
                    password: "Jabrool@123"
                });
                console.log(result.data);
                console.log(result.data.messages[0].api_key);
                if (result.data.status === "Success") {
                    QoyodComponent.auth_token = result.data.messages[0].auth_token;
                }
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    static createCustomer(customer_name, primary_contact_number, primary_email) {
        return __awaiter(this, void 0, void 0, function* () {
            let result = yield AxiosInstanse_1.AxiosInstanse.get().post("customers", {
                customer_name: customer_name,
                primary_contact_number: primary_contact_number,
                primary_email: primary_email
            });
            console.log(result.data);
            if (result.data.status === "Success") {
                return result.data.messages[0].id;
            }
            else if (result.data.status === "Failed") {
                console.log(result.data);
                throw new Error(result.data.messages);
            }
        });
    }
    static createVendor(customer_name, primary_contact_number, primary_email) {
        return __awaiter(this, void 0, void 0, function* () {
            let result = yield AxiosInstanse_1.AxiosInstanse.get().post("vendors", {
                vendor_name: customer_name,
                primary_contact_number: primary_contact_number,
                primary_email: primary_email
            });
            console.log(result.data);
            if (result.data.status === "Success") {
                return result.data.messages[0].id;
            }
            else if (result.data.status === "Failed") {
                console.log(result.data);
                throw new Error(result.data.messages);
            }
        });
    }
    static createAccount(name, code, arabic_name, account_type, type_of_account, recieve_payments) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result = yield AxiosInstanse_1.AxiosInstanse.get().post("accounts", {
                    "name": "Cost Of Goods Sold 33",
                    "code": "5403",
                    "arabic_name": "",
                    "account_type": "CurrentAsset",
                    "type_of_account": "Credit33",
                    "recieve_payments": 23
                });
                console.log(result.data);
                if (result.data.status === "Success") {
                }
                else if (result.data.status === "Failed") {
                    console.log(result.data.messages);
                }
            }
            catch (e) {
                console.log(e.request.data.errors);
            }
        });
    }
    static createInvoice(customer_id, invoice_number, status, product_id, price) {
        return __awaiter(this, void 0, void 0, function* () {
            let result = yield AxiosInstanse_1.AxiosInstanse.get().post("invoices", {
                "customer_id": customer_id,
                "invoiced_date": moment().format("YYYY-MM-DD"),
                "due_date": moment().format("YYYY-MM-DD"),
                "issue_date": moment().format("YYYY-MM-DD"),
                "invoice_number": invoice_number,
                "status": status,
                "products": {
                    "1": {
                        "product_id": product_id,
                        "quantity": "1",
                        "unit_price": price,
                        "discount_percent": "0",
                        "description": ""
                    }
                }
            });
            console.log(result.data);
            if (result.data.status === "Success") {
                return result.data.messages[0].id;
            }
            else if (result.data.status === "Failed") {
                console.log(result.data.messages);
                throw new Error(result.data.messages);
            }
        });
    }
    static test() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let id = yield QoyodComponent.createInvoice("1", "teste33wdew", "Approved", 1, 123);
                console.log(id);
            }
            catch (e) {
                if (e.response && e.response.data) {
                    console.log(e.response.data);
                }
                else {
                    console.log(e.message);
                }
            }
        });
    }
}
QoyodComponent.auth_token = "";
exports.QoyodComponent = QoyodComponent;

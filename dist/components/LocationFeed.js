"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const LocationFeedSubscriberModel_1 = require("../models/LocationFeedSubscriberModel");
const Log_1 = require("./Log");
const log = Log_1.Log.getInstanse()(module);
class LocationFeed {
    static subscribe(user, lat, lon) {
        return new Promise((resolve, reject) => {
            LocationFeedSubscriberModel_1.LocationFeedSubscriberModel
                .findOne({ user: user._id })
                .then((item) => {
                if (item === null) {
                    item = new LocationFeedSubscriberModel_1.LocationFeedSubscriberModel({
                        location: { type: "Point", coordinates: [lat, lon] },
                        user: user._id
                    });
                }
                else {
                    item.location.type = "Point";
                    item.location.coordinates = [lat, lon];
                }
                item
                    .save()
                    .then(doc => {
                    resolve();
                })
                    .catch(err => {
                    log.error(err);
                    reject(err);
                });
            })
                .catch((err) => {
                log.error(err);
                reject(err);
            });
        });
    }
    static unsubscribe(user) {
        return new Promise((resolve, reject) => {
            if (!user) {
                reject("User not found");
            }
            else {
                LocationFeedSubscriberModel_1.LocationFeedSubscriberModel
                    .findOneAndRemove({ user: user._id })
                    .then(resolve)
                    .catch(reject);
            }
        });
    }
}
exports.LocationFeed = LocationFeed;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
class AxiosInstanse {
    static init() {
        AxiosInstanse._axios = axios_1.default.create({
            baseURL: "https://qoyod.com/api/1.0/",
            headers: {
                "API-KEY": "6b9a7f1b8cae40fb71c732af0",
                "Content-Type": "application/json"
            }
        });
        AxiosInstanse._axios.defaults.timeout = 2500;
    }
    static get() {
        if (!AxiosInstanse._axios) {
            AxiosInstanse.init();
        }
        return AxiosInstanse._axios;
    }
}
exports.AxiosInstanse = AxiosInstanse;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const Log_1 = require("./Log");
const RandNumber_1 = require("./RandNumber");
const config_1 = require("./config");
const log = Log_1.Log.getInstanse()(module);
class FileHelper {
    static delete(file) {
        fs.unlink(file, err => {
            if (err) {
                log.error("Delete error: " + err);
            }
            else {
                log.info("Delete success: " + file);
            }
        });
    }
    static copy(source, des, cb) {
        let src = fs.createReadStream(source);
        let dest = fs.createWriteStream(des);
        src.pipe(dest);
        src.on("end", () => { log.info("success copy file"); cb(); });
        src.on("error", err => { log.error("error copy file!" + source); cb(err); });
    }
    static move(source, des, cb) {
        let src = fs.createReadStream(source);
        let dest = fs.createWriteStream(des);
        src.pipe(dest);
        src.on("end", () => {
            FileHelper.delete(source);
            log.info("success copy file");
            cb();
        });
        src.on("error", err => {
            log.error("error copy file!" + source);
            cb(err);
        });
    }
    static saveFromBase64(image, cb) {
        let base64Data = "";
        let timestamp = new Date().getTime();
        let file_name = RandNumber_1.RandNumber.ri(1000, 100000) + "-" + timestamp;
        if (image.indexOf("jpeg") >= 0 || image.indexOf("jpg") >= 0) {
            base64Data = image.replace(/^data:image\/jpeg;base64,/, "");
            base64Data = base64Data.replace(/^data:image\/jpg;base64,/, "");
            file_name = file_name + ".jpeg";
        }
        else {
            base64Data = image.replace(/^data:image\/png;base64,/, "");
            file_name = file_name + ".png";
        }
        require("fs").writeFile(config_1.AppConfig.getInstanse().get("files:user_uploads") + "/" + file_name, base64Data, "base64", (err) => {
            if (err) {
                log.error(err);
                cb(err);
            }
            else {
                cb(null, file_name);
            }
        });
    }
    static checkFileExist(filePath) {
        try {
            return require("fs").statSync(filePath).isFile();
        }
        catch (err) {
            return false;
        }
    }
}
exports.FileHelper = FileHelper;

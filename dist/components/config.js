"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AppConfig {
    static init() {
        if (!AppConfig._conf) {
            let conf_prefix = "";
            switch (process.env.NODE_ENV) {
                case "production":
                    conf_prefix = "_prod";
                    break;
                case "dev":
                    conf_prefix = "_dev";
                    break;
                case "devst":
                    conf_prefix = "_devst";
                    break;
            }
            const nconf = require("nconf");
            AppConfig._conf = nconf.argv()
                .env()
                .file({ file: "./config/config" + conf_prefix + ".json" });
        }
    }
    static getInstanse() {
        AppConfig.init();
        return AppConfig._conf;
    }
}
exports.AppConfig = AppConfig;

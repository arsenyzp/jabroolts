"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const PaymentCustomerNewLogModel_1 = require("../models/PaymentCustomerNewLogModel");
const PaymentCourierNewLogModel_1 = require("../models/PaymentCourierNewLogModel");
class NewPaymentLog {
    static saveCustomerOrder(customer, amount, status, pay_method, order) {
        return __awaiter(this, void 0, void 0, function* () {
            let m = new PaymentCustomerNewLogModel_1.PaymentCustomerNewLogModel();
            m.customer = customer;
            m.amount = amount;
            m.customerBonus = customer.balance;
            m.customerDebt = customer.balance < 0 ? customer.balance * -1 : 0;
            m.payment_method = pay_method;
            m.payment_type = "order";
            m.status = status;
            m.order = order;
            return yield m.save();
        });
    }
    static saveCustomerDebt(customer, amount, status, pay_method, order) {
        return __awaiter(this, void 0, void 0, function* () {
            let m = new PaymentCustomerNewLogModel_1.PaymentCustomerNewLogModel();
            m.customer = customer;
            m.amount = amount;
            m.customerBonus = customer.balance;
            m.customerDebt = customer.balance < 0 ? customer.balance * -1 : 0;
            m.payment_method = pay_method;
            m.payment_type = "order";
            m.status = status;
            m.order = order;
            return yield m.save();
        });
    }
    static saveCourierDebt(courier, order, amount, status) {
        return __awaiter(this, void 0, void 0, function* () {
            let m = new PaymentCourierNewLogModel_1.PaymentCourierNewLogModel();
            m.courier = courier;
            m.amount = order.cost;
            m.courierDebt = courier.jabroolFee < 0 ? courier.jabroolFee * -1 : 0;
            m.courierLimit = courier.courierLimit;
            m.courierTotal = 0;
            m.earnings = order.cost - order.serviceFee;
            m.payment_type = "debt";
            m.payment_method = order.pay_type;
            m.status = status;
            m.order = order;
            return yield m.save();
        });
    }
    static saveCourierOrder(courier, order, amount, status) {
        return __awaiter(this, void 0, void 0, function* () {
            let m = new PaymentCourierNewLogModel_1.PaymentCourierNewLogModel();
            m.courier = courier;
            m.amount = order.cost;
            m.courierDebt = courier.jabroolFee < 0 ? courier.jabroolFee * -1 : 0;
            m.courierLimit = courier.courierLimit;
            m.courierTotal = 0;
            m.earnings = order.cost - order.serviceFee;
            m.payment_type = "order";
            m.payment_method = order.pay_type;
            m.status = status;
            m.order = order;
            return yield m.save();
        });
    }
}
exports.NewPaymentLog = NewPaymentLog;

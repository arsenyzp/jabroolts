"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const UserModel_1 = require("../models/UserModel");
class ConnectionSocketTest {
    static test(socket, data) {
        UserModel_1.UserModel
            .findOne({ socket_ids: socket.id })
            .then((user) => {
            if (user === null) {
                socket.emit("connection_error", { message: "Profile not found" });
            }
            else {
                socket.emit("connection_ok", { message: "OK" });
            }
        })
            .catch((err) => {
            socket.emit("connection_error", { message: "Profile not found" });
        });
    }
}
exports.ConnectionSocketTest = ConnectionSocketTest;

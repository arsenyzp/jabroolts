"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class DateHelper {
    static getDateOfWeek(weekNumber, year) {
        return new Date(year, 0, 1 + ((weekNumber - 1) * 7));
    }
    static getWeekNumber(d) {
        let dd = new Date(+d);
        dd.setUTCHours(0);
        dd.setUTCDate(dd.getUTCDate() + 4 - (dd.getUTCDay() || 7));
        let yearStart = new Date(dd.getUTCFullYear(), 0, 1);
        return Math.ceil((((dd.getTime() - yearStart.getTime()) / 86400000) + 1) / 7);
    }
    static wInM(d) {
        let dd = new Date(+d);
        dd.setUTCDate(1);
        dd.setUTCHours(1);
        dd.setUTCMinutes(1);
        dd.setUTCSeconds(1);
        dd.setUTCMilliseconds(1);
        let first_week = DateHelper.getWeekNumber(dd);
        return DateHelper.getWeekNumber(d) - first_week + 1;
    }
}
exports.DateHelper = DateHelper;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Log {
    static init() {
        if (!Log._log) {
            const winston = require("winston");
            Log._log = module => {
                const path = module.filename.split("/").slice(-2).join("/");
                return new winston.Logger({
                    transports: [
                        new winston.transports.Console({
                            colorize: true,
                            level: "debug",
                            label: path
                        })
                    ]
                });
            };
        }
    }
    static getInstanse() {
        Log.init();
        return Log._log;
    }
}
exports.Log = Log;

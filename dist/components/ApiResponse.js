"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ApiError {
    constructor(code, message) {
        this.code = code;
        this.message = message;
    }
}
class ApiResponse {
    constructor() {
        this.data = {};
        this.errors = [];
    }
    addError(error) {
        let item = new ApiError("server", error.message);
        this.errors.push(item);
    }
    addErrorMessage(key, error) {
        let item = new ApiError(key, error);
        this.errors.push(item);
    }
    addErrors(errors) {
        errors.forEach((error, key, array) => {
            let item = new ApiError("server", error.message);
            this.errors.push(item);
        });
    }
    setDate(data) {
        this.data = data;
    }
    addValidationErrors(req, errors) {
        for (let i of Object.keys(errors)) {
            for (let j of Object.keys(errors[i].constraints)) {
                let item = new ApiError(errors[i].property, req.__(errors[i].constraints[j]));
                this.errors.push(item);
            }
        }
    }
    get(message) {
        let r = new ApiResponseObject(this.data, this.errors);
        if (message) {
            r.message = message;
        }
        if (this.errors.length > 0) {
            r.status = 0;
        }
        return r;
    }
}
exports.ApiResponse = ApiResponse;
class ApiResultObject {
}
class ApiResponseObject {
    constructor(data, errors) {
        this.status = 1;
        this.message = "OK";
        this.result = new ApiResultObject();
        this.result.data = data;
        this.result.error = errors;
    }
}

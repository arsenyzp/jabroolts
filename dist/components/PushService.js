"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const UserModel_1 = require("../models/UserModel");
const FCM = require("fcm-push");
const Log_1 = require("./Log");
const config_1 = require("./config");
const log = Log_1.Log.getInstanse()(module);
const fcm = new FCM(config_1.AppConfig.getInstanse().get("push:gcm_sender"));
class PushService {
    static send(ids, title, message, payload) {
        log.debug("fcm push " + title + " " + message);
        if (!ids || !ids.length || ids.length === 0) {
            log.error("Empty ids");
            return;
        }
        if (!payload || (payload.type !== "message")) {
            UserModel_1.UserModel
                .update({ $or: [{ push_device_ios: { $in: ids } }, { push_device_android: { $in: ids } }] }, {
                $inc: { unread_count: 1 }
            }, { multi: true })
                .catch(err => {
                log.error(err);
            });
        }
        if (!payload) {
            payload = {};
        }
        let users_notified = [];
        UserModel_1.UserModel
            .find({ $or: [{ push_device_ios: { $in: ids } }, { push_device_android: { $in: ids } }] })
            .then((users) => {
            for (let i = 0; i < users.length; i++) {
                if (users_notified.indexOf(users[i]._id.toString()) < 0) {
                    users_notified.push(users[i]._id.toString());
                    let push_ids = users[i].push_device_ios.concat(users[i].push_device_android);
                    for (let j = 0; j < push_ids.length; j++) {
                        payload.to = push_ids[j];
                        payload.notification = {
                            title: title,
                            body: message,
                            icon: "ic_logo_notification",
                            expiry: Math.floor(Date.now() / 1000) + 3600,
                            badge: users[i].unread_count + users[i].unread_messages,
                            sound: "ping.aiff",
                            alert: title,
                            click_action: "OPEN_NOTIFICATION"
                        };
                        payload.priority = "high";
                        fcm.send(payload)
                            .then((response) => {
                            log.info("Successfully sent with response: ", response);
                        })
                            .catch((err) => {
                            log.info("Something has gone wrong!");
                            log.error(err);
                        });
                    }
                }
            }
        })
            .catch((err) => {
            log.error(err);
        });
    }
}
exports.PushService = PushService;

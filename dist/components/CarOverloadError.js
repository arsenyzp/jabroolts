"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CarOverloadTypes = {
    SMALL: "small",
    MEDIUM: "medium",
    LARGE: "large",
    TOTAL: "total",
    NONE: "none"
};
class CarOverloadError {
    constructor(type, message) {
        this.type = type;
        this.message = message;
    }
}
exports.CarOverloadError = CarOverloadError;

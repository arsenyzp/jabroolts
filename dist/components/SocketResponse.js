"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class SocketResponse {
    constructor() {
        this.status = 200;
        this.message = "";
        this.data = {};
        this.errors = {};
    }
}
exports.SocketResponse = SocketResponse;

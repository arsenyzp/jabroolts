"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const cache = require("memory-cache");
class CurrencyComponent {
    static updateData() {
        return __awaiter(this, void 0, void 0, function* () {
            let _axios = axios_1.default.create({
                baseURL: "http://apilayer.net/api",
            });
            _axios.defaults.timeout = 2500;
            let result = yield _axios.get("live?access_key=14d42d8ba79b702b5c878e7b644a33ee&currencies=SAR&source=USD&format=1");
            console.log(result);
            cache.put("currency", result.data.quotes.USDSAR, 5000000);
        });
    }
}
exports.CurrencyComponent = CurrencyComponent;

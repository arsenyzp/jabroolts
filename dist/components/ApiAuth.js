"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ApiResponse_1 = require("./ApiResponse");
const UserModel_1 = require("../models/UserModel");
class ApiAuth {
    static check(req, res, next) {
        if (!req.header("authorization")) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            apiResponse.addErrorMessage("token", "Error authorization. Empty token");
            res.status(403);
            res.json(apiResponse.get());
        }
        else {
            UserModel_1.UserModel
                .findOneAndUpdate({ token: req.header("authorization"), deleted: false }, { local: req.header("Local") }, { new: true })
                .then(user => {
                if (user == null) {
                    let apiResponse = new ApiResponse_1.ApiResponse();
                    apiResponse.addErrorMessage("token", "Error authorization. Empty token");
                    res.status(403);
                    res.json(apiResponse.get());
                }
                else {
                    if (user.local && user.local !== "") {
                        req.setLocale(user.local);
                    }
                    req.user = user;
                    next();
                }
            })
                .catch(err => {
                let apiResponse = new ApiResponse_1.ApiResponse();
                apiResponse.addError(err);
                res.status(500);
                res.json(apiResponse.get());
            });
        }
    }
}
exports.ApiAuth = ApiAuth;

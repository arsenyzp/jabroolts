"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ConnectionSocket_1 = require("../api/socket/ConnectionSocket");
const LocationsSocket_1 = require("../api/socket/LocationsSocket");
const async = require("async");
const Log_1 = require("./Log");
const UserModel_1 = require("../models/UserModel");
const MessagesSocket_1 = require("../api/socket/MessagesSocket");
const ConnectionSocketTest_1 = require("./ConnectionSocketTest");
const log = Log_1.Log.getInstanse()(module);
class SocketServer {
    static init(server) {
        const connection = new ConnectionSocket_1.ConnectionSocket();
        const location = new LocationsSocket_1.LocationsSocket();
        const messages = new MessagesSocket_1.MessagesSocket();
        SocketServer.io = require("socket.io")(server);
        SocketServer.io.set("authorization", (handshakeData, accept) => {
            accept(null, true);
        });
        SocketServer.io.sockets.on("connection", (socket) => {
            setTimeout(() => {
                socket.emit("init", { message: "connect ok" });
            }, 1000);
            socket.on("subscribe", data => {
                connection.onSubscribe(socket, data);
            });
            socket.on("admin_auth", data => {
                connection.onAdminAuth(socket, data);
            });
            socket.on("web_auth", data => {
                connection.onWebAuth(socket, data);
            });
            socket.on("disconnect", () => {
                connection.onDisconnect(socket);
            });
            socket.on("my_location", data => {
                location.onMyLocation(socket, data);
            });
            socket.on("subscribe_location_feed", data => {
                location.onSubscribeLocationFeed(socket, data);
            });
            socket.on("unsubscribe_location_feed", data => {
                location.onUnSubscribeLocationFeed(socket, data);
            });
            socket.on("send_message", data => {
                messages.onSendMessage(socket, data);
            });
            socket.on("read_message", data => {
                messages.onReadMessage(socket, data);
            });
            socket.on("connection_test", data => {
                ConnectionSocketTest_1.ConnectionSocketTest.test(socket, data);
            });
        });
    }
    static getInstance() {
        return SocketServer.io;
    }
    static emit(user_ids, event, data, exclude) {
        if (user_ids.length === 0) {
            log.error("empty ids list");
            return;
        }
        let socket_ids = [];
        async.forEachOfSeries(user_ids, (user_id, key, callback) => {
            UserModel_1.UserModel
                .findById(user_id)
                .then(user => {
                if (user === null) {
                    log.error("Notification module -> find user Not found");
                }
                else {
                    if (user.socket_ids && user.socket_ids.length > 0) {
                        socket_ids = socket_ids.concat(user.socket_ids);
                    }
                    if (user.web_socket_ids && user.web_socket_ids.length > 0) {
                        socket_ids = socket_ids.concat(user.web_socket_ids);
                    }
                }
                callback();
            })
                .catch(err => {
                log.error("Socket emit -> find user " + err);
            });
        }, (err) => {
            if (err) {
                log.error("Socket module -> forEachOfSeries " + err);
            }
            let wrong_socket_ids = [];
            for (let i = 0; i < socket_ids.length; i++) {
                if (exclude && exclude.indexOf(socket_ids[i]) > 0) {
                    continue;
                }
                if (SocketServer.io.sockets.sockets[socket_ids[i]] && SocketServer.io.sockets.sockets[socket_ids[i]].emit) {
                    SocketServer.io.sockets.sockets[socket_ids[i]].emit(event, data);
                    log.debug("===============================");
                    log.debug("Socket module -> Emit " + event);
                    log.debug(data);
                    log.debug("===============================");
                }
                else {
                    wrong_socket_ids.push(socket_ids[i]);
                }
            }
            if (wrong_socket_ids.length > 0) {
                for (let i = 0; i < wrong_socket_ids.length; i++) {
                    UserModel_1.UserModel.findOneAndUpdate({ socket_ids: wrong_socket_ids[i] }, {
                        $pull: { socket_ids: wrong_socket_ids[i] }
                    })
                        .catch(err => {
                        log.error("Socket emit, Error remove wrong socket ids");
                    });
                }
            }
        });
    }
    static adminEmit(event, data) {
        return __awaiter(this, void 0, void 0, function* () {
            let admins = yield UserModel_1.UserModel.find({ role: { $in: [UserModel_1.UserRoles.admin, UserModel_1.UserRoles.dev] } });
            let socket_ids = [];
            admins.map((v, k, a) => {
                socket_ids = socket_ids.concat(v.socket_admin_ids);
            });
            let wrong_socket_ids = [];
            for (let i = 0; i < socket_ids.length; i++) {
                if (SocketServer.io.sockets.sockets[socket_ids[i]]) {
                    SocketServer.io.sockets.sockets[socket_ids[i]].emit(event, data);
                    log.debug("===============================");
                    log.debug("Socket module -> Emit " + event);
                    log.debug(data);
                    log.debug("===============================");
                }
                else {
                    wrong_socket_ids.push(socket_ids[i]);
                }
            }
            if (wrong_socket_ids.length > 0) {
                for (let i = 0; i < wrong_socket_ids.length; i++) {
                    UserModel_1.UserModel.findOneAndUpdate({ socket_ids: wrong_socket_ids[i] }, {
                        $pull: { socket_ids: wrong_socket_ids[i] }
                    })
                        .catch(err => {
                        log.error("Socket emit, Error remove wrong socket ids");
                    });
                }
            }
        });
    }
    static webEmit(event, data, user) {
        return __awaiter(this, void 0, void 0, function* () {
            let users = yield UserModel_1.UserModel.find({ _id: { $in: [user] } });
            let socket_ids = [];
            users.map((v, k, a) => {
                socket_ids = socket_ids.concat(v.web_socket_ids);
            });
            let wrong_socket_ids = [];
            for (let i = 0; i < socket_ids.length; i++) {
                if (SocketServer.io.sockets.sockets[socket_ids[i]]) {
                    SocketServer.io.sockets.sockets[socket_ids[i]].emit(event, data);
                    log.debug(data);
                }
                else {
                    wrong_socket_ids.push(socket_ids[i]);
                }
            }
            if (wrong_socket_ids.length > 0) {
                for (let i = 0; i < wrong_socket_ids.length; i++) {
                    UserModel_1.UserModel.findOneAndUpdate({ socket_ids: wrong_socket_ids[i] }, {
                        $pull: { socket_ids: wrong_socket_ids[i] }
                    })
                        .catch(err => {
                        log.error("Socket emit, Error remove wrong socket ids");
                    });
                }
            }
        });
    }
}
exports.SocketServer = SocketServer;

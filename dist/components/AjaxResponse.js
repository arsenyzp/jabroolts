"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AjaxResponse {
    constructor() {
        this.data = {};
        this.errors = [];
    }
    addError(error) {
        this.errors.push(error.message);
    }
    addErrorMessage(error) {
        this.errors.push(error);
    }
    addErrors(errors) {
        errors.forEach((error, key, array) => {
            this.errors.push(error.message);
        });
    }
    setDate(data) {
        this.data = data;
    }
    validation(req) {
        let mappedErrors = req.validationErrors(true);
        if (mappedErrors) {
            for (let i of Object.keys(mappedErrors)) {
                this.addError(new Error(mappedErrors[i].msg));
            }
            return false;
        }
        return true;
    }
    addValidationErrors(req, errors) {
        for (let i of Object.keys(errors)) {
            for (let j of Object.keys(errors[i].constraints)) {
                let item = new Error(req.__(errors[i].constraints[j]));
                this.addError(item);
            }
        }
    }
    get() {
        return new AjaxResponseObject(this.data, this.errors);
    }
}
exports.AjaxResponse = AjaxResponse;
class AjaxResponseObject {
    constructor(data, errors) {
        this.data = data;
        this.errors = errors;
    }
}
exports.AjaxResponseObject = AjaxResponseObject;

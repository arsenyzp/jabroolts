"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const gm = require("gm").subClass({ imageMagick: true });
class ImageResizer {
    static runtime(file_orig, w, h, req, res, next) {
        gm(file_orig)
            .resize(w, h)
            .noProfile()
            .crop(w, h, 0, 0)
            .stream((err, stdout, stderr) => {
            if (err) {
                return next(err);
            }
            stdout.pipe(res);
            stdout.on("error", next);
        });
    }
}
exports.ImageResizer = ImageResizer;

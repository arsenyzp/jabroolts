"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const PromoCodModel_1 = require("../../models/PromoCodModel");
const RandNumber_1 = require("../RandNumber");
const PaymentModule_1 = require("./PaymentModule");
exports.PromoCodeStatus = {
    NotFound: -10,
    Used: -20,
    Ready: 10
};
class PromoCodHelper {
    constructor(promo) {
        this.promo = promo;
    }
    save() {
        return new Promise((resolve, reject) => {
            if (!this.promo.code || this.promo.code === "") {
                this.generate((err, code) => {
                    if (err) {
                        reject(err);
                    }
                    else {
                        this.promo.code = code;
                        this.promo.save()
                            .then(model => {
                            resolve(model);
                        })
                            .catch(reject);
                    }
                });
            }
            else {
                this.promo
                    .save()
                    .then(model => {
                    resolve(model);
                })
                    .catch(reject);
            }
        });
    }
    generate(callback) {
        function fi(cb) {
            let code = "PROMO" + RandNumber_1.RandNumber.ri(10000, 99999);
            PromoCodModel_1.PromoCodModel
                .findOne({ code: code })
                .then(item => {
                if (item != null) {
                    fi(cb);
                }
                else {
                    callback(null, code);
                }
            })
                .catch(err => {
                callback(err);
            });
        }
        fi(callback);
    }
    static check(code, type) {
        return new Promise((resolve, reject) => {
            PromoCodModel_1.PromoCodModel
                .findOne({ code: code })
                .then((promo) => {
                if (promo == null) {
                    resolve({
                        status: exports.PromoCodeStatus.NotFound,
                        promo: promo
                    });
                }
                else if (promo.forCourier && type !== "courier") {
                    resolve({
                        status: exports.PromoCodeStatus.NotFound,
                        promo: promo
                    });
                }
                else if (!promo.forCourier && type !== "client") {
                    resolve({
                        status: exports.PromoCodeStatus.NotFound,
                        promo: promo
                    });
                }
                else if (promo.is_used) {
                    resolve({
                        status: exports.PromoCodeStatus.Used,
                        promo: promo
                    });
                }
                else {
                    resolve({
                        status: exports.PromoCodeStatus.Ready,
                        promo: promo
                    });
                }
            }).catch((err) => {
                reject(err);
            });
        });
    }
    static block(user, code) {
        return new Promise((resolve, reject) => {
            PromoCodModel_1.PromoCodModel
                .findOne({ code: code })
                .then((promo) => {
                if (promo == null) {
                    resolve({
                        status: exports.PromoCodeStatus.NotFound,
                        promo: promo
                    });
                }
                else if (promo.is_used) {
                    resolve({
                        status: exports.PromoCodeStatus.Used,
                        promo: promo
                    });
                }
                else {
                    PromoCodModel_1.PromoCodModel.findOneAndUpdate({ code: promo.code, is_used: false }, { is_used: true, owner: user._id, is_blocked: true }, { new: true })
                        .then(promo => {
                        resolve({
                            status: exports.PromoCodeStatus.Ready,
                            promo: promo
                        });
                    })
                        .catch(err => {
                        reject(err);
                    });
                }
            }).catch((err) => {
                reject(err);
            });
        });
    }
    static unblock(user, code) {
        return new Promise((resolve, reject) => {
            PromoCodModel_1.PromoCodModel
                .findOne({ _id: code })
                .then((promo) => {
                if (promo == null) {
                    resolve({
                        status: exports.PromoCodeStatus.NotFound,
                        promo: promo
                    });
                }
                else {
                    PromoCodModel_1.PromoCodModel.findOneAndUpdate({ _id: promo._id, is_blocked: true, owner: user._id }, { is_used: false, owner: null, is_blocked: false }, { new: true })
                        .then(promo => {
                        console.log(promo);
                        resolve({
                            status: exports.PromoCodeStatus.Ready,
                            promo: promo
                        });
                    })
                        .catch(err => {
                        reject(err);
                    });
                }
            }).catch((err) => {
                reject(err);
            });
        });
    }
    static use(user, code, type) {
        return new Promise((resolve, reject) => {
            PromoCodModel_1.PromoCodModel
                .findOne({ code: code })
                .then((promo) => {
                if (promo == null) {
                    resolve({
                        status: exports.PromoCodeStatus.NotFound,
                        promo: promo
                    });
                }
                else if (promo.forCourier && type !== "courier") {
                    resolve({
                        status: exports.PromoCodeStatus.NotFound,
                        promo: promo
                    });
                }
                else if (!promo.forCourier && type !== "client") {
                    resolve({
                        status: exports.PromoCodeStatus.NotFound,
                        promo: promo
                    });
                }
                else if (promo.is_used) {
                    resolve({
                        status: exports.PromoCodeStatus.Used,
                        promo: promo
                    });
                }
                else {
                    PromoCodModel_1.PromoCodModel.findOneAndUpdate({ code: promo.code, is_used: false }, { is_used: true, owner: user._id }, { new: true })
                        .then(promo => {
                        if (promo.forCourier) {
                            PaymentModule_1.PaymentModule
                                .promoCourierAdd(user._id, promo.amount)
                                .then(user => {
                                resolve({
                                    status: exports.PromoCodeStatus.Ready,
                                    promo: promo
                                });
                            })
                                .catch(err => {
                                reject(err);
                            });
                        }
                        else {
                            PaymentModule_1.PaymentModule
                                .promoBonusAdd(user._id, promo.amount)
                                .then(user => {
                                resolve({
                                    status: exports.PromoCodeStatus.Ready,
                                    promo: promo
                                });
                            })
                                .catch(err => {
                                reject(err);
                            });
                        }
                    })
                        .catch(err => {
                        reject(err);
                    });
                }
            }).catch((err) => {
                reject(err);
            });
        });
    }
}
exports.PromoCodHelper = PromoCodHelper;

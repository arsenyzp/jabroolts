"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const async = require("async");
const UserModel_1 = require("../../models/UserModel");
const Log_1 = require("../Log");
const ApiHelper_1 = require("../../api/ApiHelper");
const OrderModel_1 = require("../../models/OrderModel");
const CalculateCost_1 = require("./CalculateCost");
const FileHelper_1 = require("../FileHelper");
const config_1 = require("../config");
const HistoryLocationModel_1 = require("../../models/HistoryLocationModel");
const OrderHelper_1 = require("./OrderHelper");
const SocketServer_1 = require("../SocketServer");
const log = Log_1.Log.getInstanse()(module);
class CreateOrderHelper {
    static findRecipient(req, res, post, apiResponse, code) {
        log.info("findRecipient start");
        if (post.jid && post.jid !== "") {
            UserModel_1.UserModel
                .findOne({ jabroolid: req.body.jid })
                .then((recipient) => {
                if (recipient === null) {
                    apiResponse.addErrorMessage("jid", "Jabrool id not found");
                    res.apiJson(apiResponse.get("Jabrool id not found"));
                }
                else {
                    this.createOrder(req, res, post, apiResponse, code, recipient);
                }
            })
                .catch((err) => {
                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
            });
        }
        else {
            this.createOrder(req, res, post, apiResponse, code);
        }
    }
    static createOrder(req, res, post, apiResponse, code, recipient) {
        log.info("createOrder start");
        let order = new OrderModel_1.OrderModel({
            owner_name: req.user.first_name + " " + req.user.last_name,
            owner_contact: req.user.phone,
            owner_comment: post.owner_comment,
            owner_address: post.owner_address,
            owner_map_url: "https://www.google.com/maps/search/?api=1&query=" + post.owner_lat + "," + post.owner_lat,
            recipient_name: post.recipient_name,
            recipient_contact: post.recipient_contact,
            recipient_address: post.recipient_address,
            recipient_comment: post.recipient_comment,
            recipient_map_url: "https://www.google.com/maps/search/?api=1&query=" + post.recipient_lat + "," + post.recipient_lon,
            type: post.type,
            use_bonus: post.use_bonus,
            pay_type: req.user.pay_type,
            size: post.size,
            route: post.route,
            weight: post.weight,
            small_package_count: post.small_package_count,
            medium_package_count: post.medium_package_count,
            large_package_count: post.large_package_count,
            owner: req.user._id
        });
        if (recipient && recipient._id) {
            order.recipient = recipient._id;
        }
        if (code) {
            order.code = code._id;
        }
        CalculateCost_1.CalculateCost.process(post.route, post.small_package_count, post.medium_package_count, post.large_package_count)
            .then(price => {
            if (order.type === "express") {
                order.cost = price.e_cost;
            }
            else {
                order.cost = price.j_cost;
            }
            order.returnCost = order.cost / 2;
            if (order.type === "express") {
                order.serviceFee = price.e_serviceFee;
            }
            else {
                order.serviceFee = price.j_serviceFee;
            }
            console.log("price.e_serviceFee " + price.e_serviceFee);
            console.log("price.j_serviceFee " + price.j_serviceFee);
            console.log("order.serviceFee " + order.serviceFee);
            order.owner_location = { type: "Point", coordinates: [post.owner_lon, post.owner_lat] };
            order.recipient_location = { type: "Point", coordinates: [post.recipient_lon, post.recipient_lat] };
            let tasks = [];
            let has_error = false;
            let photos = [];
            function upload(img, cb) {
                FileHelper_1.FileHelper.move(config_1.AppConfig.getInstanse().get("files:user_uploads") + "/" + img, config_1.AppConfig.getInstanse().get("files:order_files") + "/" + img, (err) => {
                    if (err) {
                        has_error = true;
                    }
                    else {
                        photos.push(img);
                    }
                    cb();
                });
            }
            if (post.photos) {
                for (let i = 0; i < post.photos.length; i++) {
                    tasks.push((cb) => {
                        let img = post.photos[i];
                        upload(img, cb);
                    });
                }
            }
            tasks.push((cb) => {
                let model = new HistoryLocationModel_1.HistoryLocationModel({
                    address: post.recipient_address,
                    lon: post.recipient_lon,
                    lat: post.recipient_lat
                });
                model.user = req.user._id;
                model
                    .save()
                    .then((doc) => {
                    cb();
                })
                    .catch((err) => {
                    cb(err);
                });
            });
            tasks.push((cb) => {
                let model = new HistoryLocationModel_1.HistoryLocationModel({
                    address: post.owner_address,
                    lon: post.owner_lon,
                    lat: post.owner_lat
                });
                model.user = req.user._id;
                model
                    .save()
                    .then((doc) => {
                    cb();
                })
                    .catch((err) => {
                    cb(err);
                });
            });
            tasks.push((cb) => {
                OrderHelper_1.OrderHelper
                    .generateOrderId(order.type)
                    .then(OrderId => {
                    order.orderId = OrderId;
                    cb();
                })
                    .catch((err) => {
                    cb(err);
                });
            });
            async.parallel(tasks, (err) => {
                if (err || has_error) {
                    apiResponse.addErrorMessage("photos", "Error save images");
                    res.apiJson(apiResponse.get("Error save images"));
                }
                else {
                    order.photos = photos;
                    order
                        .save()
                        .then(doc => {
                        let cost_parts = {
                            card: 0,
                            balance: 0,
                            cash: 0
                        };
                        let full_cost = doc.cost;
                        if (code) {
                            full_cost = full_cost - code.amount;
                        }
                        if (doc.use_bonus) {
                            if (req.user.pay_type === "card") {
                                if (req.user.balance > 0) {
                                    if (req.user.balance >= full_cost) {
                                        cost_parts.balance = full_cost;
                                    }
                                    else {
                                        cost_parts.balance = req.user.balance;
                                        cost_parts.card = full_cost - req.user.balance;
                                    }
                                }
                                else {
                                    cost_parts.card = full_cost;
                                }
                            }
                            if (req.user.pay_type === "cash") {
                                if (req.user.balance > 0) {
                                    if (req.user.balance >= full_cost) {
                                        cost_parts.balance = full_cost;
                                    }
                                    else {
                                        cost_parts.balance = req.user.balance;
                                        cost_parts.cash = full_cost - req.user.balance;
                                    }
                                }
                                else {
                                    cost_parts.cash = full_cost;
                                }
                            }
                        }
                        else {
                            if (req.user.pay_type === "card") {
                                cost_parts.card = full_cost;
                            }
                            if (req.user.pay_type === "cash") {
                                cost_parts.cash = full_cost;
                            }
                        }
                        SocketServer_1.SocketServer.webEmit("orderUpdate", doc.getPublicFields(), order.owner);
                        apiResponse.setDate({
                            cost: cost_parts,
                            order: doc.getPublicFields()
                        });
                        res.apiJson(apiResponse.get());
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        })
            .catch(err => {
            ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
        });
    }
    static findRecipientWeb(req, res, post, ajaxResponse, code) {
        log.info("findRecipient start");
        if (post.jid && post.jid !== "") {
            UserModel_1.UserModel
                .findOne({ jabroolid: req.body.jid })
                .then((recipient) => {
                if (recipient === null) {
                    ajaxResponse.addErrorMessage("Jabrool id not found");
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
                else {
                    this.createOrderWeb(req, res, post, ajaxResponse, code, recipient);
                }
            })
                .catch((err) => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        else {
            this.createOrderWeb(req, res, post, ajaxResponse, code);
        }
    }
    static createOrderWeb(req, res, post, ajaxResponse, code, recipient) {
        log.info("createOrder start");
        let order = new OrderModel_1.OrderModel({
            owner_name: req.session.user.first_name + " " + req.session.user.last_name,
            owner_contact: req.session.user.phone,
            owner_comment: "",
            owner_address: post.owner_address,
            owner_map_url: "https://www.google.com/maps/search/?api=1&query=" + post.owner_lat + "," + post.owner_lat,
            recipient_name: post.recipient_name,
            recipient_contact: post.recipient_contact,
            recipient_address: post.recipient_address,
            recipient_comment: "",
            recipient_map_url: "https://www.google.com/maps/search/?api=1&query=" + post.recipient_lat + "," + post.recipient_lon,
            type: post.type,
            use_bonus: false,
            pay_type: req.session.user.pay_type,
            size: "medium",
            route: post.route,
            weight: post.weight,
            small_package_count: post.small_package_count,
            medium_package_count: post.medium_package_count,
            large_package_count: post.large_package_count,
            owner: req.session.user._id
        });
        if (recipient && recipient._id) {
            order.recipient = recipient._id;
        }
        if (code) {
            order.code = code._id;
        }
        CalculateCost_1.CalculateCost.process(post.route, post.small_package_count, post.medium_package_count, post.large_package_count)
            .then(price => {
            if (order.type === "express") {
                order.cost = price.e_cost;
            }
            else {
                order.cost = price.j_cost;
            }
            order.returnCost = order.cost / 2;
            if (order.type === "express") {
                order.serviceFee = price.e_serviceFee;
            }
            else {
                order.serviceFee = price.j_serviceFee;
            }
            console.log("price.e_serviceFee " + price.e_serviceFee);
            console.log("price.j_serviceFee " + price.j_serviceFee);
            console.log("order.serviceFee " + order.serviceFee);
            order.owner_location = { type: "Point", coordinates: [post.owner_lon, post.owner_lat] };
            order.recipient_location = { type: "Point", coordinates: [post.recipient_lon, post.recipient_lat] };
            let tasks = [];
            let has_error = false;
            let photos = [];
            function upload(img, cb) {
                FileHelper_1.FileHelper.move(config_1.AppConfig.getInstanse().get("files:user_uploads") + "/" + img, config_1.AppConfig.getInstanse().get("files:order_files") + "/" + img, (err) => {
                    if (err) {
                        has_error = true;
                    }
                    else {
                        photos.push(img);
                    }
                    cb();
                });
            }
            if (post.photos) {
                for (let i = 0; i < post.photos.length; i++) {
                    tasks.push((cb) => {
                        let img = post.photos[i];
                        upload(img, cb);
                    });
                }
            }
            tasks.push((cb) => {
                let model = new HistoryLocationModel_1.HistoryLocationModel({
                    address: post.recipient_address,
                    lon: post.recipient_lon,
                    lat: post.recipient_lat
                });
                model.user = req.session.user._id;
                model
                    .save()
                    .then((doc) => {
                    cb();
                })
                    .catch((err) => {
                    cb(err);
                });
            });
            tasks.push((cb) => {
                let model = new HistoryLocationModel_1.HistoryLocationModel({
                    address: post.owner_address,
                    lon: post.owner_lon,
                    lat: post.owner_lat
                });
                model.user = req.session.user._id;
                model
                    .save()
                    .then((doc) => {
                    cb();
                })
                    .catch((err) => {
                    cb(err);
                });
            });
            tasks.push((cb) => {
                OrderHelper_1.OrderHelper
                    .generateOrderId(order.type)
                    .then(OrderId => {
                    order.orderId = OrderId;
                    cb();
                })
                    .catch((err) => {
                    cb(err);
                });
            });
            async.parallel(tasks, (err) => {
                if (err || has_error) {
                    ajaxResponse.addErrorMessage("Error save images");
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
                else {
                    order.photos = photos;
                    order
                        .save()
                        .then(doc => {
                        let cost_parts = {
                            card: 0,
                            balance: 0,
                            cash: 0
                        };
                        let full_cost = doc.cost;
                        if (code) {
                            full_cost = full_cost - code.amount;
                        }
                        if (doc.use_bonus) {
                            if (req.session.user.pay_type === "card") {
                                if (req.session.user.balance > 0) {
                                    if (req.session.user.balance >= full_cost) {
                                        cost_parts.balance = full_cost;
                                    }
                                    else {
                                        cost_parts.balance = req.session.user.balance;
                                        cost_parts.card = full_cost - req.session.user.balance;
                                    }
                                }
                                else {
                                    cost_parts.card = full_cost;
                                }
                            }
                            if (req.session.user.pay_type === "cash") {
                                if (req.session.user.balance > 0) {
                                    if (req.session.user.balance >= full_cost) {
                                        cost_parts.balance = full_cost;
                                    }
                                    else {
                                        cost_parts.balance = req.session.user.balance;
                                        cost_parts.cash = full_cost - req.session.user.balance;
                                    }
                                }
                                else {
                                    cost_parts.cash = full_cost;
                                }
                            }
                        }
                        else {
                            if (req.session.user.pay_type === "card") {
                                cost_parts.card = full_cost;
                            }
                            if (req.session.user.pay_type === "cash") {
                                cost_parts.cash = full_cost;
                            }
                        }
                        SocketServer_1.SocketServer.webEmit("orderUpdate", doc.getPublicFields(), order.owner);
                        ajaxResponse.setDate({
                            cost: cost_parts,
                            order: doc.getPublicFields()
                        });
                        res.json(ajaxResponse.get());
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                }
            });
        })
            .catch(err => {
            ajaxResponse.addError(err);
            res.status(500);
            res.json(ajaxResponse.get());
        });
    }
}
exports.CreateOrderHelper = CreateOrderHelper;

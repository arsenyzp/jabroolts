"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const LocationFeedSubscriberModel_1 = require("../../models/LocationFeedSubscriberModel");
const OrderModel_1 = require("../../models/OrderModel");
const CarTypeModel_1 = require("../../models/CarTypeModel");
const SocketServer_1 = require("../SocketServer");
const cache = require("memory-cache");
const Log_1 = require("../Log");
const NotificationModule_1 = require("../NotificationModule");
const PackageRatioConfig_1 = require("../../models/configs/PackageRatioConfig");
const log = Log_1.Log.getInstanse()(module);
const test = false;
class CourierLocationHandler {
    static process(courier, lat, lon) {
        return __awaiter(this, void 0, void 0, function* () {
            let query = {};
            query.location = {
                $near: {
                    $geometry: {
                        type: "Point",
                        coordinates: [lat, lon]
                    },
                    $maxDistance: 1000
                }
            };
            if (courier.visible && !courier.in_progress && !test) {
                let users = yield LocationFeedSubscriberModel_1.LocationFeedSubscriberModel.find(query).lean();
                let users_ids = [];
                for (let i = 0; i < users.length; i++) {
                    users_ids.push(users[i].user);
                }
                let last_users_ids = cache.get("subscriber_ids_" + courier._id.toString());
                if (last_users_ids && last_users_ids.length > 0) {
                    let difference_users_ids = last_users_ids
                        .filter(x => users_ids.indexOf(x) === -1);
                    let subscribers = yield LocationFeedSubscriberModel_1.LocationFeedSubscriberModel
                        .find({ user: { $in: difference_users_ids } }).lean();
                    let notify_ids = [];
                    for (let j = 0; j < subscribers.length; j++) {
                        notify_ids.push(subscribers[j].user);
                    }
                    if (notify_ids.length > 0) {
                        SocketServer_1.SocketServer.emit(notify_ids, "courier_disconnect", {
                            name: courier.first_name + "_" + courier.last_name,
                            jabroolid: courier.jabroolid,
                            courier: courier.getApiFields(),
                            courier_remove: "radius"
                        });
                    }
                }
                if (users_ids.length > 0) {
                    cache.put("subscriber_ids_" + courier._id.toString(), users_ids, 5000);
                    SocketServer_1.SocketServer.emit(users_ids, "locations", {
                        name: courier.first_name + "_" + courier.last_name,
                        jabroolid: courier.jabroolid,
                        courier: courier.getApiFields(),
                        lat: lat,
                        lon: lon
                    });
                }
                else {
                    cache.put("subscriber_ids_" + courier._id.toString(), [], 5000);
                }
                let query_orders = {};
                query_orders.owner_location = {
                    $near: {
                        $geometry: {
                            type: "Point",
                            coordinates: [lon, lat]
                        },
                        $maxDistance: 10000
                    }
                };
                query_orders.status = OrderModel_1.OrderStatuses.New;
                query_orders.notified_couriers = { $ne: courier._id };
                query_orders.canceled_couriers = { $ne: courier._id };
                let orders = yield OrderModel_1.OrderModel
                    .find(query_orders).lean();
                log.debug("Courier " + courier.first_name);
                log.debug("New orders " + orders.length);
                let type = cache.get("car_etype_" + courier.vehicle.type.toString());
                if (!type) {
                    type = (yield CarTypeModel_1.CarTypeModel
                        .findOne({ _id: courier.vehicle.type }).lean());
                    cache.put("car_etype_" + courier.vehicle.type.toString(), type, 500000);
                }
                let packageRatioConfig = yield new PackageRatioConfig_1.PackageRatioConfig().getInstanse();
                let totalPackages = 0;
                const courierOrders = yield OrderModel_1.OrderModel
                    .find({ courier: courier._id, status: { $nin: [OrderModel_1.OrderStatuses.Finished, OrderModel_1.OrderStatuses.Canceled] } })
                    .lean();
                courierOrders.map((v, k, a) => {
                    totalPackages += v.small_package_count * packageRatioConfig.small +
                        v.medium_package_count * packageRatioConfig.medium +
                        v.large_package_count * packageRatioConfig.large;
                });
                log.debug("Courier car loading " + totalPackages);
                if (type !== null) {
                    let data = [];
                    for (let i = 0; i < orders.length; i++) {
                        let v = orders[i];
                        let totalPackagesC = v.small_package_count * packageRatioConfig.small +
                            v.medium_package_count * packageRatioConfig.medium +
                            v.large_package_count * packageRatioConfig.large;
                        log.debug("order packages " + totalPackagesC);
                        log.debug("type size " + type.size);
                        if (type.size >= totalPackagesC + totalPackages) {
                            let cc = yield OrderModel_1.OrderModel.findOneAndUpdate({
                                _id: v._id
                            }, {
                                $push: {
                                    notified_couriers: courier._id
                                }
                            }, { new: true });
                            data.push(cc.getPublicFields());
                        }
                        if (data.length >= 10) {
                            log.debug("Over 10 orders available");
                            break;
                        }
                    }
                    if (data.length > 0) {
                        SocketServer_1.SocketServer.emit([courier._id], "new_requests", data);
                    }
                }
                else {
                    SocketServer_1.SocketServer.emit([courier._id], "socket_error", { "message": "Type vehicle not selected" });
                }
            }
            if (courier.in_progress && !test) {
                let orders = yield OrderModel_1.OrderModel
                    .find({ _id: { $in: courier.current_orders } })
                    .lean();
                let users_ids = [];
                for (let i = 0; i < orders.length; i++) {
                    if (users_ids.indexOf(orders[i].owner.toString()) < 0) {
                        users_ids.push(orders[i].owner.toString());
                    }
                }
                if (users_ids.length > 0) {
                    SocketServer_1.SocketServer.emit(users_ids, "locations", {
                        name: courier.first_name + "_" + courier.last_name,
                        jabroolid: courier.jabroolid,
                        courier: courier.getApiFields(),
                        lat: lat,
                        lon: lon
                    });
                }
                let query_orders_in_progress = {};
                query_orders_in_progress.recipient_location = {
                    $near: {
                        $geometry: {
                            type: "Point",
                            coordinates: [lon, lat]
                        },
                        $maxDistance: 1000
                    }
                };
                query_orders_in_progress.status = OrderModel_1.OrderStatuses.InProgress;
                query_orders_in_progress.courier = courier._id;
                query_orders_in_progress.near_courier_notification = { $in: [0, null] };
                let ordersForNotifications = yield OrderModel_1.OrderModel
                    .find(query_orders_in_progress).lean();
                if (ordersForNotifications.length > 0) {
                    ordersForNotifications.map((v, k, a) => __awaiter(this, void 0, void 0, function* () {
                        let order = yield OrderModel_1.OrderModel.findOneAndUpdate({
                            _id: v._id
                        }, {
                            $inc: { near_courier_notification: 1 }
                        }, { new: true });
                        if (order.recipient) {
                            NotificationModule_1.NotificationModule.getInstance().send([order.recipient.toString()], NotificationModule_1.NotificationTypes.CourierIsNearDestination, {}, order._id.toString());
                        }
                    }));
                }
                else {
                    log.info(courier.phone + " Order near recipient - 0");
                }
                let query_orders_wait_pick_up = {};
                query_orders_wait_pick_up.owner_location = {
                    $near: {
                        $geometry: {
                            type: "Point",
                            coordinates: [lon, lat]
                        },
                        $maxDistance: 1000
                    }
                };
                query_orders_wait_pick_up.status = OrderModel_1.OrderStatuses.WaitPickUp;
                query_orders_wait_pick_up.courier = courier._id;
                query_orders_wait_pick_up.near_owner_notification = { $in: [0, null] };
                let ordersNotificationOwner = yield OrderModel_1.OrderModel
                    .find(query_orders_wait_pick_up).lean();
                if (ordersNotificationOwner.length > 0) {
                    ordersNotificationOwner.map((v, k, a) => __awaiter(this, void 0, void 0, function* () {
                        let order = yield OrderModel_1.OrderModel.findOneAndUpdate({
                            _id: v._id
                        }, {
                            $inc: { near_owner_notification: 1 }
                        }, { new: true });
                        NotificationModule_1.NotificationModule.getInstance().send([order.owner.toString()], NotificationModule_1.NotificationTypes.CourierIsNearAtPickUp, {}, order._id.toString());
                    }));
                }
                else {
                    log.info(courier.phone + " Order near recipient - 0");
                }
            }
        });
    }
}
exports.CourierLocationHandler = CourierLocationHandler;

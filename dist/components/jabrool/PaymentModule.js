"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const UserModel_1 = require("../../models/UserModel");
const PaymentLog_1 = require("../../models/PaymentLog");
const Log_1 = require("../Log");
const JError_1 = require("../JError");
const CourierBalanceLogModel_1 = require("../../models/CourierBalanceLogModel");
const SmsSender_1 = require("./SmsSender");
const EmailSender_1 = require("./EmailSender");
const cache = require("memory-cache");
const log = Log_1.Log.getInstanse()(module);
const braintree = require("braintree");
class PaymentModule {
    static init() {
        PaymentModule._gateway = braintree.connect({
            environment: braintree.Environment.Sandbox,
            merchantId: "k2k7v3y7j8cjxbwf",
            publicKey: "cshd2zhwsb58t445",
            privateKey: "dc8f9088901f09167b1ec78253cc6dab"
        });
    }
    static getGateway() {
        if (!PaymentModule._gateway) {
            PaymentModule.init();
        }
        return PaymentModule._gateway;
    }
    static getToken() {
        return new Promise((resolve, reject) => {
            PaymentModule.getGateway().clientToken.generate({}, (err, response) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(response.clientToken);
                }
            });
        });
    }
    static findUser(bt_customer_id) {
        return new Promise((resolve, reject) => {
            PaymentModule
                .getGateway()
                .customer
                .find(bt_customer_id, (err, result) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(result);
                }
            });
        });
    }
    static listCards(bt_customer_id) {
        return PaymentModule.findUser(bt_customer_id);
    }
    static paymentFormDefaultCard(amount, user) {
        return new Promise((resolve, reject) => {
            if (amount <= 0) {
                reject(new JError_1.JError("Invalid amount"));
            }
            else {
                let paymentLog = new PaymentLog_1.PaymentLogModel({
                    user: user._id,
                    type: PaymentLog_1.PaymentTypes.Payment,
                    amount: amount * -1
                });
                paymentLog
                    .save()
                    .then(pLog => {
                    PaymentModule
                        .findUser(user.bt_customer_id)
                        .then(result => {
                        let found = false;
                        for (let i = 0; i < result.creditCards.length; i++) {
                            if (result.creditCards[i].default) {
                                found = true;
                                let currency = parseFloat(cache.get("currency"));
                                let usd_amount = amount / currency;
                                PaymentModule
                                    .getGateway()
                                    .transaction.sale({
                                    amount: usd_amount.toFixed(2),
                                    paymentMethodToken: result.creditCards[i].token,
                                    customerId: user.bt_customer_id,
                                    options: {
                                        submitForSettlement: true
                                    }
                                }, (err, result) => {
                                    console.log("transaction.sale");
                                    console.log(result);
                                    if (err) {
                                        log.error(err);
                                        pLog.status = PaymentLog_1.PaymentStatuses.Fail;
                                        pLog.bt_result_json = JSON.stringify(result);
                                        pLog.save()
                                            .then(l => {
                                            reject(err);
                                        })
                                            .catch(e => {
                                            log.error(e);
                                            reject(err);
                                        });
                                    }
                                    else if (!result.success) {
                                        log.error(result);
                                        pLog.status = PaymentLog_1.PaymentStatuses.Fail;
                                        pLog.bt_result_json = JSON.stringify(result);
                                        pLog.save()
                                            .then(l => {
                                            reject(new Error(result.message));
                                        })
                                            .catch(e => {
                                            log.error(e);
                                            reject(err);
                                        });
                                    }
                                    else {
                                        pLog.status = PaymentLog_1.PaymentStatuses.Success;
                                        pLog.bt_pay_id = result.transaction.id;
                                        pLog.bt_status = result.transaction.status;
                                        pLog.bt_type = result.transaction.bt_type;
                                        pLog.bt_currencyIsoCode = result.transaction.currencyIsoCode;
                                        pLog.bt_amount = result.transaction.amount;
                                        pLog.bt_result_json = JSON.stringify(result);
                                        pLog.save()
                                            .then(l => {
                                            resolve(result);
                                        })
                                            .catch(e => {
                                            log.error(e);
                                            resolve(result);
                                        });
                                    }
                                });
                            }
                        }
                        if (!found) {
                            reject(new JError_1.JError("Card not found"));
                        }
                    })
                        .catch(err => {
                        reject(err);
                    });
                })
                    .catch(err => {
                    reject(err);
                });
            }
        });
    }
    static paymentFormCard(uniqueNumberIdentifier, amount, user) {
        return new Promise((resolve, reject) => {
            if (amount <= 0) {
                reject("err");
            }
            else {
                let paymentLog = new PaymentLog_1.PaymentLogModel({
                    user: user._id,
                    type: PaymentLog_1.PaymentTypes.Payment,
                    amount: amount * -1
                });
                paymentLog
                    .save()
                    .then(pLog => {
                    PaymentModule
                        .findUser(user.bt_customer_id)
                        .then(result => {
                        let found = false;
                        for (let i = 0; i < result.length; i++) {
                            if (result[i].uniqueNumberIdentifier === uniqueNumberIdentifier) {
                                found = true;
                                found = true;
                                PaymentModule
                                    .getGateway()
                                    .transaction.sale({
                                    amount: amount,
                                    paymentMethodToken: result[i].token,
                                    customerId: user.bt_customer_id,
                                    options: {
                                        submitForSettlement: true
                                    }
                                }, (err, result) => {
                                    if (err) {
                                        log.error(err);
                                        pLog.status = PaymentLog_1.PaymentStatuses.Fail;
                                        pLog.save()
                                            .then(l => {
                                            reject(err);
                                        })
                                            .catch(e => {
                                            log.error(e);
                                            reject(err);
                                        });
                                    }
                                    else if (!result.status) {
                                        log.error(err);
                                        log.error(result);
                                        pLog.status = PaymentLog_1.PaymentStatuses.Fail;
                                        pLog.save()
                                            .then(l => {
                                            reject(err);
                                        })
                                            .catch(e => {
                                            log.error(e);
                                            reject(err);
                                        });
                                    }
                                    else {
                                        pLog.status = PaymentLog_1.PaymentStatuses.Success;
                                        pLog.bt_pay_id = result.id;
                                        pLog.save()
                                            .then(l => {
                                            resolve(result);
                                        })
                                            .catch(e => {
                                            log.error(e);
                                            resolve(result);
                                        });
                                    }
                                });
                            }
                        }
                        if (!found) {
                            reject(new Error("Card not found"));
                        }
                    })
                        .catch(err => {
                        reject(err);
                    });
                })
                    .catch(err => {
                    reject(err);
                });
            }
        });
    }
    static paymentFromBalance(amount, user) {
        return new Promise((resolve, reject) => {
            if (amount <= 0) {
                reject("err");
            }
            else {
                let paymentLog = new PaymentLog_1.PaymentLogModel({
                    user: user._id,
                    type: PaymentLog_1.PaymentTypes.Payment,
                    amount: amount * -1
                });
                paymentLog
                    .save()
                    .then(pLog => {
                    UserModel_1.UserModel.findOneAndUpdate({ _id: user._id }, { $inc: { balance: (-1 * amount) } }, { new: true })
                        .then(user => {
                        pLog.status = PaymentLog_1.PaymentStatuses.Success;
                        pLog.save()
                            .then(l => {
                            resolve(user);
                        })
                            .catch(e => {
                            log.error(e);
                            resolve(user);
                        });
                    })
                        .catch(err => {
                        log.error(err);
                        pLog.status = PaymentLog_1.PaymentStatuses.Fail;
                        pLog.save()
                            .then(l => {
                            reject(err);
                        })
                            .catch(e => {
                            log.error(e);
                            reject(err);
                        });
                    });
                })
                    .catch(err => {
                    reject(err);
                });
            }
        });
    }
    static createCustomer(firstName, lastName, email, phone) {
        return new Promise((resolve, reject) => {
            PaymentModule
                .getGateway()
                .customer.create({
                firstName: firstName,
                lastName: lastName,
                email: email,
                phone: phone
            }, (err, result) => {
                if (err) {
                    log.error(err);
                    reject(err);
                }
                else {
                    resolve(result);
                }
            });
        });
    }
    static createCustomerAndPayment(firstName, lastName, email, phone, nonceFromTheClient) {
        return new Promise((resolve, reject) => {
            PaymentModule
                .getGateway()
                .customer.create({
                firstName: firstName,
                lastName: lastName,
                email: email,
                phone: phone,
                paymentMethodNonce: nonceFromTheClient
            }, (err, result) => {
                if (err) {
                    log.error(err);
                    reject(err);
                }
                else {
                    resolve(result);
                }
            });
        });
    }
    static createPayment(customerId, nonceFromTheClient) {
        return new Promise((resolve, reject) => {
            PaymentModule
                .getGateway()
                .paymentMethod.create({
                customerId: customerId,
                paymentMethodNonce: nonceFromTheClient
            }, (err, result) => {
                if (err) {
                    log.error(err);
                    reject(err);
                }
                else {
                    PaymentModule
                        .findUser(customerId)
                        .then(r => {
                        resolve({
                            success: true,
                            customer: r
                        });
                    })
                        .catch(reject);
                }
            });
        });
    }
    static removeCard(customer_id, uniqueNumberIdentifier) {
        return new Promise((resolve, reject) => {
            PaymentModule
                .findUser(customer_id)
                .then(result => {
                let deleted = false;
                for (let i = 0; i < result.creditCards.length; i++) {
                    if (result.creditCards[i].uniqueNumberIdentifier === uniqueNumberIdentifier) {
                        deleted = true;
                        PaymentModule
                            .getGateway()
                            .paymentMethod.delete(result.creditCards[i].token, (err) => {
                            if (err) {
                                log.error(err);
                                reject(err);
                            }
                            else {
                                resolve(result);
                            }
                        });
                    }
                }
                if (!deleted) {
                    resolve(0);
                }
            })
                .catch(err => {
                log.error(err);
                reject(err);
            });
        });
    }
    static setDefault(customer_id, uniqueNumberIdentifier) {
        console.log("PaymentModule::setDefault");
        return new Promise((resolve, reject) => {
            console.log("PaymentModule::findUser");
            PaymentModule
                .findUser(customer_id)
                .then(result => {
                let found = false;
                for (let i = 0; i < result.creditCards.length; i++) {
                    if (result.creditCards[i].uniqueNumberIdentifier === uniqueNumberIdentifier) {
                        found = true;
                        console.log("PaymentModule::getGateway.paymentMethod.update");
                        PaymentModule
                            .getGateway()
                            .paymentMethod
                            .update(result.creditCards[i].token, {
                            options: {
                                makeDefault: true
                            }
                        }, (err) => {
                            if (err) {
                                log.error(err);
                                reject(err);
                            }
                            else {
                                resolve(1);
                            }
                        });
                        break;
                    }
                }
                if (!found) {
                    reject(new JError_1.JError("Card not found"));
                }
            })
                .catch(err => {
                log.error(err);
                reject(err);
            });
        });
    }
    static correctBalance(type, customer_id, amount) {
        return new Promise((resolve, reject) => {
            let paymentLog = new PaymentLog_1.PaymentLogModel({
                user: customer_id,
                type: type,
                amount: amount
            });
            paymentLog
                .save()
                .then(pLog => {
                UserModel_1.UserModel.findOneAndUpdate({ _id: customer_id }, { $inc: { balance: amount } }, { new: true })
                    .then(user => {
                    pLog.status = PaymentLog_1.PaymentStatuses.Success;
                    pLog.save()
                        .then(l => {
                        resolve(user);
                    })
                        .catch(e => {
                        log.error(e);
                        resolve(user);
                    });
                })
                    .catch(err => {
                    log.error(err);
                    pLog.status = PaymentLog_1.PaymentStatuses.Fail;
                    pLog.save()
                        .then(l => {
                        reject(err);
                    })
                        .catch(e => {
                        log.error(e);
                        reject(err);
                    });
                });
            })
                .catch(err => {
                reject(err);
            });
        });
    }
    static moneyBack(customer_id, amount) {
        return PaymentModule.correctBalance(PaymentLog_1.PaymentTypes.Refund, customer_id, amount);
    }
    static promoBonusAdd(customer_id, amount) {
        return PaymentModule.correctBalance(PaymentLog_1.PaymentTypes.Promo, customer_id, amount);
    }
    static promoCourierAdd(customer_id, amount) {
        return PaymentModule.correctCourierLimit(CourierBalanceLogModel_1.CourierBalanceTypes.Promo, customer_id, amount);
    }
    static courierTakeUserDebt(customer_id, amount) {
        return PaymentModule.correctCourierJabroolFee(CourierBalanceLogModel_1.CourierBalanceTypes.Jabrool, customer_id, amount);
    }
    static correctBalanceFromAdmin(customer_id, amount) {
        return PaymentModule.correctBalance(PaymentLog_1.PaymentTypes.Admin, customer_id, amount);
    }
    static invaiteBonus(customer_id, amount) {
        return PaymentModule.correctBalance(PaymentLog_1.PaymentTypes.Invite, customer_id, amount);
    }
    static courierOrder(courier, order) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("courierOrder");
            let earning = order.cost - order.serviceFee;
            let paymentLog = new PaymentLog_1.PaymentLogModel({
                user: courier._id,
                type: PaymentLog_1.PaymentTypes.Order,
                amount: earning,
                order: order._id
            });
            yield paymentLog
                .save();
            console.log("courierOrder jabrool fee " + order.serviceFee);
            switch (order.pay_type) {
                case "card":
                    return yield PaymentModule
                        .correctCourierJabroolFee(CourierBalanceLogModel_1.CourierBalanceTypes.Order, courier._id, (order.cost - order.serviceFee) * -1);
                case "cash":
                    return yield PaymentModule
                        .correctCourierJabroolFee(CourierBalanceLogModel_1.CourierBalanceTypes.Order, courier._id, (order.serviceFee - order.bonus));
            }
        });
    }
    static correctCourierLimit(type, customer_id, amount) {
        return new Promise((resolve, reject) => {
            let model = new CourierBalanceLogModel_1.CourierBalanceLogModel({
                user: customer_id,
                amount: amount,
                type: type
            });
            model
                .save()
                .then(pLog => {
                UserModel_1.UserModel.findOneAndUpdate({ _id: customer_id }, { $inc: { courierLimit: amount } }, { new: true })
                    .then(user => {
                    resolve(user);
                })
                    .catch(err => {
                    reject(err);
                });
            })
                .catch(err => {
                reject(err);
            });
        });
    }
    static correctCourierJabroolFee(type, customer_id, amount) {
        return new Promise((resolve, reject) => {
            let model = new CourierBalanceLogModel_1.CourierBalanceLogModel({
                user: customer_id,
                amount: amount,
                type: type
            });
            model
                .save()
                .then(pLog => {
                UserModel_1.UserModel.findOneAndUpdate({ _id: customer_id }, { $inc: { jabroolFee: amount } }, { new: true })
                    .then(user => {
                    if (user.courierLimit <= user.jabroolFee) {
                        SmsSender_1.SmsSender.send(user.phone, "Limit is less than 0");
                        EmailSender_1.EmailSender.sendTemplateToUser(user, "Courier limit", {}, "courier_limit_notification");
                    }
                    resolve(user);
                })
                    .catch(err => {
                    reject(err);
                });
            })
                .catch(err => {
                reject(err);
            });
        });
    }
}
exports.PaymentModule = PaymentModule;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const UserModel_1 = require("../../models/UserModel");
const RandNumber_1 = require("../RandNumber");
class JidGenerator {
    static getRendCharset() {
        let text = "";
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (let i = 0; i < 3; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }
    static gen(user) {
        return new Promise((resolve, reject) => {
            let name = JidGenerator.getRendCharset();
            let fi = () => {
                let jabroolid = RandNumber_1.RandNumber.ri(1000, 9999) + "";
                jabroolid = name + RandNumber_1.RandNumber.ri(10000, 99999);
                UserModel_1.UserModel.findOne({ jabroolid: jabroolid })
                    .then(item => {
                    if (item != null) {
                        fi();
                    }
                    else {
                        user.jabroolid = jabroolid;
                        resolve();
                    }
                })
                    .catch(err => {
                    reject(err);
                });
            };
            if (name.length > 4) {
                name = name.substring(0, 3);
            }
            name = name.toString().toUpperCase();
            fi();
        });
    }
}
exports.JidGenerator = JidGenerator;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const UserModel_1 = require("../../models/UserModel");
const OrderModel_1 = require("../../models/OrderModel");
const DeliveryConfig_1 = require("../../models/configs/DeliveryConfig");
const SocketServer_1 = require("../SocketServer");
const NotificationModule_1 = require("../NotificationModule");
class CheckCourierStatus {
    static getStatus(user_id) {
        return new Promise((resolve, reject) => {
            UserModel_1.UserModel
                .findOne({ _id: user_id })
                .then((user) => {
                if (user === null) {
                    reject(new Error("Not found"));
                }
                else {
                    let d = new Date();
                    new DeliveryConfig_1.DeliveryConfig()
                        .getInstanse()
                        .then(config => {
                        let delivery_time = config.waiting_time * 60 * 1000;
                        let items = [];
                        if (user.current_orders && user.current_orders.length > 0) {
                            OrderModel_1.OrderModel
                                .find({ _id: { $in: user.current_orders } })
                                .then((orders) => {
                                function compare(a, b) {
                                    if (a.start_at < b.start_at) {
                                        return -1;
                                    }
                                    else if (a.start_at > b.start_at) {
                                        return 1;
                                    }
                                    else {
                                        return 0;
                                    }
                                }
                                if (orders.length > 0) {
                                    let sorted_current_orders = orders.sort(compare);
                                    let first_order = sorted_current_orders[0];
                                    delivery_time = delivery_time - (d.getTime() - first_order.start_at);
                                    delivery_time = delivery_time < 0 ? 0 : delivery_time;
                                    let has_express = false;
                                    for (let i = 0; i < orders.length; i++) {
                                        if (orders[i].type === OrderModel_1.OrderTypes.Express) {
                                            has_express = true;
                                        }
                                    }
                                    if ((delivery_time < 1 || user.current_orders.length >= 10 || has_express) && !user.in_progress) {
                                        user.in_progress = true;
                                        UserModel_1.UserModel.findOneAndUpdate({ _id: user._id }, { in_progress: true }, { new: true })
                                            .then(us => {
                                            if (us != null) {
                                                SocketServer_1.SocketServer.emit([us._id], "start_delivery", {});
                                                NotificationModule_1.NotificationModule.getInstance().send([us._id], NotificationModule_1.NotificationTypes.Start, {
                                                    delivery_time: delivery_time < 1,
                                                    current_orders: user.current_orders.length >= 10,
                                                    has_express: has_express
                                                }, us._id);
                                            }
                                        })
                                            .catch(err => {
                                            reject(err);
                                        });
                                    }
                                    for (let i = 0; i < orders.length; i++) {
                                        items.push(orders[i].getPublicFields());
                                    }
                                }
                                resolve({
                                    in_progress: user.in_progress,
                                    accept_in_progress: user.accept_in_progress,
                                    delivery_time: delivery_time,
                                    orders: items,
                                    courier: user.getApiPublicFields()
                                });
                            })
                                .catch((err) => {
                                reject(err);
                            });
                        }
                        else {
                            UserModel_1.UserModel.findOneAndUpdate({ _id: user._id }, {
                                in_progress: false,
                                accept_in_progress: false
                            }, { new: true })
                                .then(updatedUser => {
                                resolve({
                                    in_progress: updatedUser.in_progress,
                                    accept_in_progress: updatedUser.accept_in_progress,
                                    delivery_time: delivery_time,
                                    orders: [],
                                    courier: updatedUser
                                });
                            })
                                .catch(err => {
                                reject(err);
                            });
                        }
                    })
                        .catch(err => {
                        reject(err);
                    });
                }
            })
                .catch((err) => {
                reject(err);
            });
        });
    }
}
exports.CheckCourierStatus = CheckCourierStatus;

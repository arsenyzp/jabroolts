"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("../config");
const nodemailer = require("nodemailer");
const path = require("path");
const Log_1 = require("../Log");
const EmailLogModel_1 = require("../../models/logs/EmailLogModel");
const LogsConfig_1 = require("../../models/configs/LogsConfig");
const transporter = nodemailer.createTransport(config_1.AppConfig.getInstanse().get("mail_config"));
const EmailTemplate = require("email-templates").EmailTemplate;
const log = Log_1.Log.getInstanse()(module);
class EmailSender {
    static sendTemplateToUser(user, subject, data, template) {
        let lang = user.local;
        if (["en", "ar"].indexOf(lang) < 0) {
            lang = "en";
        }
        const templateDir = path.join(__dirname, "../../../src/views/mail", template + "_" + lang);
        const tmpl = new EmailTemplate(templateDir);
        const variables = { user: user, data: data };
        tmpl.render(variables, (err, result) => {
            if (err) {
                log.error(err);
            }
            else {
                this.send(user.email, subject, result.html, result.text);
            }
        });
    }
    static send(email, subject, html, text) {
        log.debug("send email " + text + " to " + email);
        let logConfig = new LogsConfig_1.LogsConfig();
        logConfig
            .getInstanse()
            .then((conf) => {
            if (conf.email) {
                let logModel = new EmailLogModel_1.EmailLogModel();
                logModel.email = email;
                logModel.subject = subject;
                logModel.html = html;
                logModel.text = text;
                logModel.save();
            }
        })
            .catch(err => {
            log.error(err);
        });
        const mailOptions = {
            from: config_1.AppConfig.getInstanse().get("mail_sender"),
            to: email,
            subject: subject,
            text: text ? text : html,
            html: html
        };
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            else {
                log.debug("Message sent: " + info.response);
            }
        });
    }
}
exports.EmailSender = EmailSender;

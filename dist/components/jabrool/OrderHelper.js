"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const OrderModel_1 = require("../../models/OrderModel");
const UserModel_1 = require("../../models/UserModel");
const SocketServer_1 = require("../SocketServer");
const PaymentModule_1 = require("./PaymentModule");
const NotificationModule_1 = require("../NotificationModule");
const CheckCourierStatus_1 = require("./CheckCourierStatus");
const CarOverloadError_1 = require("../CarOverloadError");
const PackageRatioConfig_1 = require("../../models/configs/PackageRatioConfig");
const CarTypeModel_1 = require("../../models/CarTypeModel");
const CostParts_1 = require("../CostParts");
const PaymentLog_1 = require("../../models/PaymentLog");
const PriceConfig_1 = require("../../models/configs/PriceConfig");
const NotificationModel_1 = require("../../models/NotificationModel");
const OnlineLogModel_1 = require("../../models/OnlineLogModel");
const Log_1 = require("../Log");
const Events_1 = require("../events/Events");
const EventBus = require("eventbusjs");
const log = Log_1.Log.getInstanse()(module);
class OrderHelper {
    static getConfirmCode() {
        return Math.floor(111111 + Math.random() * (999999 - 111111)) + "";
    }
    static payOrder(user, order, cost_parts) {
        return new Promise((resolve, reject) => {
            OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, {
                status: order.status === OrderModel_1.OrderStatuses.WaitCourierReturnConfirmCode ?
                    OrderModel_1.OrderStatuses.WaitCourierReturnConfirmCode :
                    OrderModel_1.OrderStatuses.WaitCourierPickUpConfirmCode,
                paid: true,
                bt_pay_id: user.bt_customer_id,
                cash: cost_parts.cash,
                card: cost_parts.card,
                bonus: cost_parts.balance
            }, { new: true })
                .then(doc => {
                let price = cost_parts.cash + cost_parts.card + cost_parts.balance;
                EventBus.dispatch(Events_1.EVENT_ORDER_PAID, this, { user: user, order: doc, price: price });
                let paymentLog = new PaymentLog_1.PaymentLogModel({
                    user: order.courier,
                    type: PaymentLog_1.PaymentTypes.Order,
                    amount: cost_parts.courier,
                    status: PaymentLog_1.PaymentStatuses.Success
                });
                paymentLog
                    .save()
                    .then(_ => {
                    resolve({
                        order: doc,
                        cost_parts: cost_parts
                    });
                })
                    .catch(err => {
                    reject(err);
                });
            })
                .catch(err => {
                reject(err);
            });
        });
    }
    static paymentProcessNew(user, order, cost) {
        return __awaiter(this, void 0, void 0, function* () {
            let cost_parts = new CostParts_1.CostParts();
            let userCredit = 0;
            if (user.balance < 0) {
                userCredit = user.balance;
            }
            if (order.use_bonus) {
                if (user.pay_type === "card") {
                    if (user.balance > 0) {
                        if (user.balance >= cost) {
                            cost_parts.balance = cost;
                        }
                        else {
                            cost_parts.balance = user.balance;
                            cost_parts.card = cost - user.balance;
                        }
                    }
                    else {
                        cost_parts.card = cost;
                    }
                }
                if (user.pay_type === "cash") {
                    if (user.balance > 0) {
                        if (user.balance >= cost) {
                            cost_parts.balance = cost;
                        }
                        else {
                            cost_parts.balance = user.balance;
                            cost_parts.cash = cost - user.balance;
                        }
                    }
                    else {
                        cost_parts.cash = cost;
                    }
                }
            }
            else {
                if (user.pay_type === "card") {
                    cost_parts.card = cost;
                }
                if (user.pay_type === "cash") {
                    cost_parts.cash = cost;
                }
            }
            if (cost_parts.card > 0) {
                cost_parts.card += 0.035 * cost_parts.card;
                if (userCredit !== 0) {
                    cost_parts.card += userCredit * -1;
                }
            }
            let configPrice = yield new PriceConfig_1.PriceConfig().getInstanse();
            if (order.type === OrderModel_1.OrderTypes.Jabrool) {
                cost_parts.courier = (cost_parts.cash + cost_parts.balance + cost_parts.card) -
                    ((cost_parts.cash + cost_parts.balance + cost_parts.card) * configPrice.j_percent) / 100;
            }
            else {
                cost_parts.courier = (cost_parts.cash + cost_parts.balance + cost_parts.card) -
                    ((cost_parts.cash + cost_parts.balance + cost_parts.card) * configPrice.e_percent) / 100;
            }
            if (order.use_bonus && user.balance > 0) {
                user = yield PaymentModule_1.PaymentModule
                    .paymentFromBalance(cost_parts.balance, user);
            }
            else {
                if (userCredit !== 0) {
                    yield PaymentModule_1.PaymentModule
                        .courierTakeUserDebt(order.courier.toString(), (-1 * userCredit));
                    user = yield UserModel_1.UserModel
                        .findOneAndUpdate({ _id: user._id }, { $inc: { balance: (-1 * userCredit) }
                    });
                    yield NotificationModel_1.NotificationModel.update({ user: order.owner, type: { $in: [
                                NotificationModule_1.NotificationTypes.JabroolDebt
                            ] } }, { is_view: true }, { multi: true });
                }
                order = yield OrderModel_1.OrderModel
                    .findOneAndUpdate({ _id: order._id }, { userCredit: userCredit }, { new: true });
            }
            switch (order.pay_type) {
                case "card":
                    yield PaymentModule_1.PaymentModule.paymentFormDefaultCard(cost_parts.card, user);
                    break;
                case "cash":
                    console.log("Pay from cash");
                    break;
                default:
                    throw new Error("Pay error");
            }
            let result = yield OrderHelper
                .payOrder(user, order, cost_parts);
            return result.order;
        });
    }
    static removeOrderFromCourier(orderCanceled) {
        console.log("removeOrderFromCourier", orderCanceled._id);
        return new Promise((resolve, reject) => {
            UserModel_1.UserModel.findOneAndUpdate({ current_orders: orderCanceled._id }, { $pull: { current_orders: orderCanceled._id } }, { new: true })
                .then(us => {
                if (us && us.current_orders.length < 1) {
                    UserModel_1.UserModel.findOneAndUpdate({ _id: us._id }, {
                        in_progress: false,
                        accept_in_progress: false
                    }).then(_ => {
                        resolve(orderCanceled);
                    }).catch(err => {
                        reject(err);
                    });
                }
                else {
                    resolve(orderCanceled);
                }
            })
                .catch(err => {
                reject(err);
            });
        });
    }
    static compare(a, b) {
        if (a.start_at < b.start_at) {
            return -1;
        }
        else if (a.start_at > b.start_at) {
            return 1;
        }
        else {
            return 0;
        }
    }
    static acceptToCourier(courier, doc) {
        return new Promise((resolve, reject) => {
            UserModel_1.UserModel
                .findOneAndUpdate({ _id: courier._id }, { $push: { current_orders: doc._id } }, { new: true })
                .then(user => {
                NotificationModule_1.NotificationModule.getInstance().send([doc.owner.toString()], NotificationModule_1.NotificationTypes.RequestAccepted, {}, doc._id.toString());
                OnlineLogModel_1.OnlineLogModel
                    .findOne({ user: user._id })
                    .sort({ created_at: -1 })
                    .then(l => {
                    l.type = "order";
                    l.save().then(_ => {
                        log.info("saved in online log");
                    }).catch(err => {
                        log.error(err);
                    });
                })
                    .catch(err => {
                    log.error(err);
                });
                CheckCourierStatus_1.CheckCourierStatus.getStatus(user.id)
                    .then(result => {
                    let owner_id = doc.owner._id ? doc.owner._id.toString() : doc.owner.toString();
                    SocketServer_1.SocketServer.emit([owner_id], "request_accept", {
                        order: doc.getPublicFields(),
                        courier: user.getApiFields(),
                        pickup_time: result.delivery_time
                    });
                    resolve(result);
                })
                    .catch((err) => {
                    reject(err);
                });
            })
                .catch(err => {
                reject(err);
            });
        });
    }
    static checkOverloadCar(courier, order) {
        return new Promise((resolve, reject) => {
            if (courier.current_orders && courier.current_orders.length > 0) {
                OrderModel_1.OrderModel
                    .find({ _id: { $in: courier.current_orders } })
                    .then((orders) => {
                    let load_small = 0;
                    let load_medium = 0;
                    let load_large = 0;
                    let load_small_in_small = 0;
                    let load_medium_in_small = 0;
                    let load_large_in_small = 0;
                    for (let i = 0; i < orders.length; i++) {
                        load_small += orders[i].small_package_count;
                        load_medium += orders[i].medium_package_count;
                        load_large += orders[i].large_package_count;
                    }
                    let packageConfigModel = new PackageRatioConfig_1.PackageRatioConfig();
                    packageConfigModel
                        .getInstanse()
                        .then(packageConfig => {
                        load_small_in_small = load_small * packageConfig.small;
                        load_medium_in_small = load_medium * packageConfig.medium;
                        load_large_in_small = load_large * packageConfig.large;
                        CarTypeModel_1.CarTypeModel
                            .findOne({ _id: courier.vehicle.type })
                            .then((car_type) => {
                            if (car_type != null) {
                                if (order.small_package_count +
                                    load_medium_in_small +
                                    load_large_in_small > car_type.size) {
                                    resolve(new CarOverloadError_1.CarOverloadError(CarOverloadError_1.CarOverloadTypes.TOTAL, "Сar overload"));
                                }
                                else {
                                    resolve(new CarOverloadError_1.CarOverloadError(CarOverloadError_1.CarOverloadTypes.NONE, "OK"));
                                }
                            }
                            else {
                                resolve(new CarOverloadError_1.CarOverloadError(CarOverloadError_1.CarOverloadTypes.NONE, "OK"));
                            }
                        })
                            .catch(err => {
                            reject(err);
                        });
                    })
                        .catch(err => {
                        reject(err);
                    });
                })
                    .catch((err) => {
                    reject(err);
                });
            }
            else {
                resolve(new CarOverloadError_1.CarOverloadError(CarOverloadError_1.CarOverloadTypes.NONE, "OK"));
            }
        });
    }
    static getRandCharset() {
        let text = "";
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (let i = 0; i < 3; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }
    static getRandCode() {
        return Math.floor(111 + Math.random() * (999 - 111)) + "";
    }
    static generateOrderId(type = "jabrool") {
        return __awaiter(this, void 0, void 0, function* () {
            let count = yield OrderModel_1.OrderModel.count({});
            let OrderID = "";
            switch (type) {
                case "jabrool":
                    OrderID += "J";
                    break;
                case "express":
                    OrderID += "E";
                    break;
            }
            OrderID += count;
            OrderID += OrderHelper.getRandCode();
            return OrderID;
        });
    }
}
exports.OrderHelper = OrderHelper;

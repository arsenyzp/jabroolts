"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Log_1 = require("../Log");
const request = require("request");
const querystring = require("querystring");
const SmsLogModel_1 = require("../../models/logs/SmsLogModel");
const LogsConfig_1 = require("../../models/configs/LogsConfig");
const config_1 = require("../config");
const log = Log_1.Log.getInstanse()(module);
class SmsSender {
    static send(phone, text) {
        let url = "https://www.experttexting.com/ExptRestApi/sms/json/Message/Send?username=jabrool&";
        let form = {
            password: "ABC12345",
            api_key: "w84ccyknuehi7g6",
            from: "jabrool",
            to: "+" + parseInt(phone),
            text: text
        };
        let logModel = new SmsLogModel_1.SmsLogModel();
        let logConfig = new LogsConfig_1.LogsConfig();
        logConfig
            .getInstanse()
            .then((conf) => {
            if (conf.sms) {
                logModel.text = text;
                logModel.phone = phone;
                logModel.save();
            }
        })
            .catch(err => {
            log.error(err);
        });
        if (!config_1.AppConfig.getInstanse().get("enable_sms")) {
            return;
        }
        let formData = querystring.stringify(form);
        request({
            uri: url + formData,
            method: "GET"
        }, (error, res, body) => {
            if (!error) {
                try {
                    let json = JSON.parse(body);
                    if (json.Status !== 0) {
                        log.error("Sms error send to " + phone);
                    }
                    else {
                        log.debug("Sms success send to " + phone);
                        let logConfig = new LogsConfig_1.LogsConfig();
                        logConfig
                            .getInstanse()
                            .then((conf) => {
                            if (conf.sms) {
                                logModel.phone = phone;
                                logModel.mid = json.message_id;
                                logModel.cost = json.price;
                                logModel.save();
                            }
                        })
                            .catch(err => {
                            log.error(err);
                        });
                    }
                }
                catch (e) {
                    logModel.phone = phone;
                    logModel.mid = e.message;
                    logModel.save();
                }
            }
            else {
                logModel.phone = phone;
                logModel.mid = "error";
                logModel.save();
            }
        });
    }
}
exports.SmsSender = SmsSender;

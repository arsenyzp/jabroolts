"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Log_1 = require("../Log");
const UserModel_1 = require("../../models/UserModel");
const SocketServer_1 = require("../SocketServer");
const log = Log_1.Log.getInstanse()(module);
class ClearSocket {
    run() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let users = yield UserModel_1.UserModel.find({ $or: [
                        { web_socket_ids: { $exists: true, $not: { $size: 0 } } },
                        { socket_ids: { $exists: true, $not: { $size: 0 } } }
                    ]
                });
                let wrong_socket_ids = [];
                let wrong_web_socket_ids = [];
                let wrong_admin_socket_ids = [];
                users.map((v, k, a) => {
                    v.socket_ids.map((id, key, array) => {
                        if (!SocketServer_1.SocketServer.getInstance().sockets.sockets[id] ||
                            !SocketServer_1.SocketServer.getInstance().sockets.sockets[id].emit) {
                            wrong_socket_ids.push(id);
                        }
                    });
                    v.web_socket_ids.map((id, key, array) => {
                        if (!SocketServer_1.SocketServer.getInstance().sockets.sockets[id] ||
                            !SocketServer_1.SocketServer.getInstance().sockets.sockets[id].emit) {
                            wrong_web_socket_ids.push(id);
                        }
                    });
                    v.socket_admin_ids.map((id, key, array) => {
                        if (!SocketServer_1.SocketServer.getInstance().sockets.sockets[id] ||
                            !SocketServer_1.SocketServer.getInstance().sockets.sockets[id].emit) {
                            wrong_admin_socket_ids.push(id);
                        }
                    });
                });
                wrong_socket_ids.map((v, k, a) => {
                    UserModel_1.UserModel.findOneAndUpdate({ socket_ids: v }, {
                        $pull: { socket_ids: v }
                    });
                });
                wrong_web_socket_ids.map((v, k, a) => {
                    UserModel_1.UserModel.findOneAndUpdate({ socket_ids: v }, {
                        $pull: { socket_ids: v }
                    });
                });
                wrong_admin_socket_ids.map((v, k, a) => {
                    UserModel_1.UserModel.findOneAndUpdate({ socket_ids: v }, {
                        $pull: { socket_ids: v }
                    });
                });
            }
            catch (e) {
                log.error(e);
            }
        });
    }
}
exports.ClearSocket = ClearSocket;

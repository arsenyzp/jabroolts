"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Log_1 = require("../Log");
const UserModel_1 = require("../../models/UserModel");
const DebtLogModel_1 = require("../../models/DebtLogModel");
const log = Log_1.Log.getInstanse()(module);
class CalculateDebt {
    run() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let users = yield UserModel_1.UserModel.find({ balance: { $lt: 0 } });
                let total = 0;
                users.map((v, k, a) => {
                    total -= v.balance;
                });
                let m = new DebtLogModel_1.DebtLogModel();
                m.value = total * -1;
                yield m.save();
            }
            catch (e) {
                log.error(e);
            }
        });
    }
}
exports.CalculateDebt = CalculateDebt;

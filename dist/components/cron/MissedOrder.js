"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const EventBus = require("eventbusjs");
const Log_1 = require("../Log");
const OrderModel_1 = require("../../models/OrderModel");
const DeliveryConfig_1 = require("../../models/configs/DeliveryConfig");
const Events_1 = require("../events/Events");
const log = Log_1.Log.getInstanse()(module);
class MissedOrder {
    run() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let model = yield new DeliveryConfig_1.DeliveryConfig().getInstanse();
                let maxTime = new Date().getTime() - model.missed_time * 60 * 1000;
                let missedOrders = yield OrderModel_1.OrderModel
                    .find({ status: OrderModel_1.OrderStatuses.New, created_at: { $lte: maxTime } }).lean();
                missedOrders.forEach((order, key, array) => __awaiter(this, void 0, void 0, function* () {
                    let model = yield OrderModel_1.OrderModel.findById(order._id);
                    EventBus.dispatch(Events_1.EVENT_ORDER_REQUEST_CANCELED, this, {
                        order: model
                    });
                    EventBus.dispatch(Events_1.EVENT_ORDER_MISSED_TIMEOUT, this, {
                        order: model
                    });
                    try {
                        yield OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, {
                            status: OrderModel_1.OrderStatuses.Missed
                        }, { new: true });
                    }
                    catch (e) {
                        log.error(e);
                    }
                }));
            }
            catch (e) {
                log.error(e);
            }
        });
    }
}
exports.MissedOrder = MissedOrder;

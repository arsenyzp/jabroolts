"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Log_1 = require("../Log");
const UserModel_1 = require("../../models/UserModel");
const UserActivityLogModel_1 = require("../../models/UserActivityLogModel");
const log = Log_1.Log.getInstanse()(module);
class UserActivity {
    run() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let couriers = yield UserModel_1.UserModel.count({ $or: [
                        { web_socket_ids: { $exists: true, $not: { $size: 0 } } },
                        { socket_ids: { $exists: true, $not: { $size: 0 } } }
                    ],
                    status: UserModel_1.UserStatus.Active
                });
                let customers = yield UserModel_1.UserModel.count({ $or: [
                        { web_socket_ids: { $exists: true, $not: { $size: 0 } } },
                        { socket_ids: { $exists: true, $not: { $size: 0 } } }
                    ],
                    status: { $nin: [UserModel_1.UserStatus.Active] }
                });
                let m = new UserActivityLogModel_1.UserActivityLogModel();
                m.customers = customers;
                m.couriers = couriers;
                yield m.save();
            }
            catch (e) {
                log.error(e);
            }
        });
    }
}
exports.UserActivity = UserActivity;

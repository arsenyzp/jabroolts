"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const UserModel_1 = require("../../models/UserModel");
const CheckCourierStatus_1 = require("../jabrool/CheckCourierStatus");
const Log_1 = require("../Log");
const delay = require("delay");
const log = Log_1.Log.getInstanse()(module);
class StartDelivery {
    run() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let users = yield UserModel_1.UserModel.find({ in_progress: false, status: UserModel_1.UserStatus.Active })
                    .lean();
                for (let i = 0; i < users.length; i++) {
                    if (i % 100 === 0) {
                        yield delay(100);
                    }
                    if (users[i].current_orders.length > 0) {
                        yield CheckCourierStatus_1.CheckCourierStatus
                            .getStatus(users[i]._id);
                    }
                }
            }
            catch (err) {
                log.error(err);
            }
        });
    }
}
exports.StartDelivery = StartDelivery;

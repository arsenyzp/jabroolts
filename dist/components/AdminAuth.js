"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const UserModel_1 = require("../models/UserModel");
const config_1 = require("./config");
class AdminAuth {
    static check(req, res, next) {
        if (req.session.user) {
            if (req.session.user.role === UserModel_1.UserRoles.admin
                || req.session.user.role === UserModel_1.UserRoles.dev) {
                return next();
            }
        }
        res.redirect(config_1.AppConfig.getInstanse().get("urls:admin_login"));
    }
}
exports.AdminAuth = AdminAuth;

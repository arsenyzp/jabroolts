"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class RandNumber {
    static ri(min, max) {
        let rand = min + Math.random() * (max - min);
        rand = Math.round(rand);
        return rand;
    }
}
exports.RandNumber = RandNumber;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ErrorReporting = require("@google-cloud/error-reporting");
class GErrorReporting {
    static init() {
        this._instanse = new ErrorReporting({
            projectId: "affable-case-191910",
            key: "AIzaSyDom3ZItsDeVyCZAgJztK5fofeCZLbmH0s",
            ignoreEnvironmentCheck: true
        });
    }
    static report(err) {
        if (GErrorReporting._instanse === null) {
            GErrorReporting.init();
        }
        GErrorReporting._instanse.report(err, () => console.log("Error reporting sent"));
    }
}
GErrorReporting._instanse = null;
exports.default = GErrorReporting;

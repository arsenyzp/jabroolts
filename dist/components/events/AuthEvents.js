"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const EventBus = require("eventbusjs");
const i18n = require("i18n");
const Events_1 = require("./Events");
const UserModel_1 = require("../../models/UserModel");
const SmsSender_1 = require("../jabrool/SmsSender");
const EmailSender_1 = require("../jabrool/EmailSender");
const Log_1 = require("../Log");
const config_1 = require("../config");
const QoyodComponent_1 = require("../QoyodComponent");
const log = Log_1.Log.getInstanse()(module);
class AuthEvents {
    static init() {
        EventBus.addEventListener(Events_1.EVENT_MOB_USER_LOGIN, (params, arg) => {
            log.info(Events_1.EVENT_MOB_USER_LOGIN);
            let user = arg.user;
            UserModel_1.UserModel
                .findOneAndUpdate({ _id: user._id }, { login_at: new Date().getTime() })
                .then(user => {
                log.info(Events_1.EVENT_MOB_USER_LOGIN + " set user last login");
            })
                .catch(err => {
                log.error(err);
            });
        });
        EventBus.addEventListener(Events_1.EVENT_MOB_USER_REGISTER, (params, arg) => __awaiter(this, void 0, void 0, function* () {
            log.info(Events_1.EVENT_MOB_USER_REGISTER);
            try {
                let customer = yield QoyodComponent_1.QoyodComponent.createCustomer(arg.user.first_name + " " + arg.user.last_name, arg.user.phone, arg.user.email);
                yield UserModel_1.UserModel.findOneAndUpdate({
                    _id: arg.user._id
                }, {
                    qo_customer_id: customer
                });
            }
            catch (e) {
                console.log(e);
            }
            let user = arg.user;
            if (user.phone !== "") {
                SmsSender_1.SmsSender.send(user.phone, i18n.__("api.sms.ConfirmCode", user.confirm_sms_code));
            }
            EmailSender_1.EmailSender.sendTemplateToUser(user, i18n.__("api.email.SubjectSuccessRegister"), { url: config_1.AppConfig.getInstanse().get("base_url") }, "registration");
        }));
        EventBus.addEventListener(Events_1.EVENT_MOB_USER_CONFIRM_PHONE, (params, arg) => __awaiter(this, void 0, void 0, function* () {
            log.info(Events_1.EVENT_MOB_USER_CONFIRM_PHONE);
        }));
        EventBus.addEventListener(Events_1.EVENT_MOB_USER_RESTORE_PASSWORD, (params, arg) => __awaiter(this, void 0, void 0, function* () {
            log.info(Events_1.EVENT_MOB_USER_RESTORE_PASSWORD);
            let user = arg.user;
            if (user.phone !== "") {
                SmsSender_1.SmsSender.send(user.phone, i18n.__("api.sms.NewPassword", arg.password));
            }
            EmailSender_1.EmailSender.sendTemplateToUser(user, i18n.__("api.email.SubjectRestorePassword"), { new_password: arg.password }, "forgot");
        }));
    }
}
exports.AuthEvents = AuthEvents;

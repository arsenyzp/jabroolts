"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const EventBus = require("eventbusjs");
const Events_1 = require("./Events");
const Log_1 = require("../Log");
const QoyodComponent_1 = require("../QoyodComponent");
const log = Log_1.Log.getInstanse()(module);
class FinishOrderEvent {
    static init() {
        EventBus.addEventListener(Events_1.EVENT_ORDER_FINISHED, (params, arg) => __awaiter(this, void 0, void 0, function* () {
            log.info(Events_1.EVENT_ORDER_FINISHED);
            let invoice = yield QoyodComponent_1.QoyodComponent.createInvoice(arg.user.qo_customer_id, arg.order.orderId, "Draft", (arg.order.type === "jabrool" ? QoyodComponent_1.jid : QoyodComponent_1.eid), arg.price);
        }));
    }
}
exports.FinishOrderEvent = FinishOrderEvent;

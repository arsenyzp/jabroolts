"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const EventBus = require("eventbusjs");
const Events_1 = require("./Events");
const Log_1 = require("../Log");
const log = Log_1.Log.getInstanse()(module);
class PayOrderEvent {
    static init() {
        EventBus.addEventListener(Events_1.EVENT_ORDER_PAID, (params, arg) => __awaiter(this, void 0, void 0, function* () {
            log.info(Events_1.EVENT_ORDER_PAID);
        }));
    }
}
exports.PayOrderEvent = PayOrderEvent;

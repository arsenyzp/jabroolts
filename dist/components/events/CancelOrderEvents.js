"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const EventBus = require("eventbusjs");
const Events_1 = require("./Events");
const Log_1 = require("../Log");
const SocketServer_1 = require("../SocketServer");
const log = Log_1.Log.getInstanse()(module);
class CancelOrderEvents {
    static init() {
        EventBus.addEventListener(Events_1.EVENT_ORDER_REQUEST_CANCELED, (params, arg) => {
            log.info(Events_1.EVENT_ORDER_REQUEST_CANCELED);
            let orderCanceled = arg.order;
            let notified_couriers = [];
            for (let i = 0; i < orderCanceled.notified_couriers.length; i++) {
                notified_couriers.push(orderCanceled.notified_couriers[i].toString());
            }
            if (orderCanceled.courier) {
                notified_couriers = notified_couriers.concat([orderCanceled.courier.toString()]);
            }
            SocketServer_1.SocketServer.emit(notified_couriers, "request_canceled", {
                order: orderCanceled.getPublicFields()
            });
        });
    }
}
exports.CancelOrderEvents = CancelOrderEvents;

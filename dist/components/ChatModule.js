"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const async = require("async");
const mongoose = require("mongoose");
const UserModel_1 = require("../models/UserModel");
const OrderModel_1 = require("../models/OrderModel");
const MessageModel_1 = require("../models/MessageModel");
const Log_1 = require("./Log");
const PushService_1 = require("./PushService");
const config_1 = require("./config");
const FileHelper_1 = require("./FileHelper");
const SocketServer_1 = require("./SocketServer");
const NotificationModule_1 = require("./NotificationModule");
const i18n = require("i18n");
const log = Log_1.Log.getInstanse()(module);
class ChatModule {
    static sendMessage(socket, data) {
        let ts = new Date().getTime();
        console.log(ts);
        if (data.text) {
            data.text = data.text.trim();
        }
        if (!mongoose.Types.ObjectId.isValid(data.order_id)) {
            socket.emit("socket_error", { message: "Invalid order id" });
            return;
        }
        if (data.text === "" && data.image === "") {
            socket.emit("socket_error", { message: "Empty message" });
            return;
        }
        let sender;
        let recipient;
        let order;
        async.parallel([
            (cb) => {
                UserModel_1.UserModel
                    .findOne({ socket_ids: socket.id })
                    .then((user) => {
                    if (user === null) {
                        socket.emit("socket_error", { message: "Sender profile not found" });
                        cb({ message: "Sender profile not found" });
                    }
                    else {
                        sender = user;
                        cb();
                    }
                })
                    .catch((err) => {
                    cb(err);
                });
            },
            (cb) => {
                OrderModel_1.OrderModel
                    .findOne({ _id: data.order_id })
                    .populate({ path: "owner", model: UserModel_1.UserModel })
                    .populate({ path: "courier", model: UserModel_1.UserModel })
                    .then((o) => {
                    if (o === null) {
                        socket.emit("socket_error", { message: "Order not found" });
                        cb({ message: "Order not found" });
                        return;
                    }
                    if (!o.owner) {
                        socket.emit("socket_error", { message: "Customer profile not found" });
                        cb({ message: "Customer profile not found" });
                        return;
                    }
                    if (!o.courier) {
                        socket.emit("socket_error", { message: "Courier profile not found" });
                        cb({ message: "Courier profile not found" });
                        return;
                    }
                    order = o;
                    cb();
                })
                    .catch((err) => {
                    log.error(err);
                    socket.emit("socket_error", { message: "Jabrool is under maintenance please try again later" });
                    cb({ message: "Jabrool is under maintenance please try again later" });
                });
            }
        ], err => {
            if (err) {
                log.error(err);
            }
            else {
                if (sender._id.toString() === order.owner._id.toString()) {
                    recipient = order.courier;
                }
                else {
                    recipient = order.owner;
                }
                let message = new MessageModel_1.MessageModel({
                    sender: sender._id,
                    recipient: recipient._id,
                    order: data.order_id,
                    mid: data.mid
                });
                if (data.text) {
                    message.text = data.text;
                }
                if (data.image) {
                    message.image = data.image;
                    FileHelper_1.FileHelper.move(config_1.AppConfig.getInstanse().get("files:user_uploads") + "/" + data.image, config_1.AppConfig.getInstanse().get("files:chat") + "/" + data.image, (im) => {
                        if (!im) {
                            log.error("Error move image");
                        }
                        log.info("Move image success");
                    });
                }
                UserModel_1.UserModel.findOneAndUpdate({ _id: recipient._id }, { $inc: { unread_messages: 1 } }, (err) => {
                    log.error(err);
                });
                if (recipient._id.toString() === order.owner._id.toString()) {
                    OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, { $inc: { unread_messages_owner: 1 } }, (err) => {
                        log.error(err);
                    });
                }
                if (recipient._id.toString() === order.courier._id.toString()) {
                    OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, { $inc: { unread_messages_courier: 1 } }, (err) => {
                        log.error(err);
                    });
                }
                message
                    .save()
                    .then(message => {
                    SocketServer_1.SocketServer.emit([message.sender.toString(), message.recipient.toString()], "new_message", message.getPublicFields());
                    let text = message.text ? message.text : "";
                    if (text.length > 100) {
                        text = text.substring(0, 100);
                    }
                    let avatar = config_1.AppConfig.getInstanse().get("base_url") +
                        config_1.AppConfig.getInstanse().get("urls:url_avatar") +
                        sender.avatar;
                    if (order.courier._id === recipient._id) {
                        PushService_1.PushService.send(recipient.push_device_android.concat(recipient.push_device_ios), i18n.__("api.push.NewMessageFromCustomer", sender.first_name, sender.last_name), text, {
                            data: {
                                sender_id: sender._id,
                                data_id: message.order,
                                sender_name: sender.first_name + " " + sender.last_name,
                                avatar: avatar,
                                message: text,
                                type: NotificationModule_1.NotificationTypes.Message
                            },
                            notification_id: message._id,
                            type: NotificationModule_1.NotificationTypes.Message
                        });
                    }
                    if (!recipient.socket_ids || !recipient.socket_ids.length || recipient.socket_ids.length === 0) {
                        let text = message.text ? message.text : "";
                        if (text.length > 100) {
                            text = text.substring(0, 100);
                        }
                        let avatar = config_1.AppConfig.getInstanse().get("base_url") +
                            config_1.AppConfig.getInstanse().get("urls:url_avatar") +
                            sender.avatar;
                        if (order.courier._id !== recipient._id) {
                            PushService_1.PushService.send(recipient.push_device_android.concat(recipient.push_device_ios), i18n.__("api.push.NewMessageFromCourier", sender.first_name, sender.last_name), text, {
                                data: {
                                    sender_id: sender._id,
                                    data_id: message.order,
                                    sender_name: sender.first_name + " " + sender.last_name,
                                    avatar: avatar,
                                    message: text,
                                    type: NotificationModule_1.NotificationTypes.Message
                                },
                                notification_id: message._id,
                                type: NotificationModule_1.NotificationTypes.Message
                            });
                        }
                    }
                })
                    .catch(err => {
                    log.error(err);
                    socket.emit("socket_error", err);
                });
            }
        });
    }
    static readMessage(socket, data) {
        if (!mongoose.Types.ObjectId.isValid(data.message_id)) {
            socket.emit("socket_error", { message: "Invalid message id" });
            return;
        }
        let recipient;
        let order;
        async.parallel([
            (cb) => {
                UserModel_1.UserModel
                    .findOne({ socket_ids: socket.id })
                    .then((user) => {
                    if (user === null) {
                        socket.emit("socket_error", { message: "Profile not found" });
                        cb({ message: "Profile not found" });
                    }
                    else {
                        recipient = user;
                        cb();
                    }
                })
                    .catch((err) => {
                    cb(err);
                });
            },
            (cb) => {
                MessageModel_1.MessageModel
                    .findOne({ _id: data.message_id })
                    .populate({ path: "order", model: OrderModel_1.OrderModel })
                    .then((m) => {
                    if (m) {
                        order = m.order;
                    }
                    cb();
                })
                    .catch((err) => {
                    cb(err);
                });
            },
            (cb) => {
                UserModel_1.UserModel
                    .findOneAndUpdate({ socket_ids: socket.id, unread_messages: { $gte: 1 } }, { $inc: { unread_messages: -1 } }, (err) => {
                    if (err) {
                        log.error(err);
                    }
                    cb(err);
                });
            },
            (cb) => {
                MessageModel_1.MessageModel.findOneAndUpdate({ _id: data.message_id }, { is_read: true }, (err) => {
                    if (err) {
                        log.error(err);
                    }
                    cb(err);
                });
            }
        ], (err) => {
            if (!err && order) {
                if (!!order.courier && recipient._id.toString() === order.owner.toString()) {
                    OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, { unread_messages_owner: 0 }, (err) => {
                        log.error(err);
                    });
                }
                if (!!order.courier && recipient._id.toString() === order.courier.toString()) {
                    OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, { unread_messages_courier: 0 }, (err) => {
                        log.error(err);
                    });
                }
            }
            log.info("message set read success");
        });
    }
}
exports.ChatModule = ChatModule;

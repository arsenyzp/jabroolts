"use strict";
const AjaxResponse_1 = require("../../components/AjaxResponse");
const UserModel_1 = require("../../models/UserModel");
const Log_1 = require("../../components/Log");
const async = require("async");
const log = Log_1.Log.getInstanse()(module);
var Route;
(function (Route) {
    class Profile {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            UserModel_1.UserModel
                .findOne({ _id: req.session.user._id })
                .then(u => {
                if (u == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                ajaxResponse.setDate(u);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        getToken(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            UserModel_1.UserModel
                .findOne({ _id: req.session.user._id })
                .then(u => {
                if (u == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                ajaxResponse.setDate(u);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
    }
    Route.Profile = Profile;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
class LicensePost {
}
__decorate([
    class_validator_1.Length(2, 25, {
        message: "admin.validation.LicenseNumber"
    })
], LicensePost.prototype, "number", void 0);
__decorate([
    class_validator_1.Length(2, 25, {
        message: "admin.validation.LicenseName"
    })
], LicensePost.prototype, "name", void 0);
__decorate([
    class_validator_1.IsDateString({
        message: "admin.validation.LicenseIssueDate"
    })
], LicensePost.prototype, "issue_date", void 0);
__decorate([
    class_validator_1.IsDateString({
        message: "admin.validation.LicenseInvalidExpiryDate"
    })
], LicensePost.prototype, "expiry_date", void 0);
__decorate([
    class_validator_1.Length(2, 25, {
        message: "admin.validation.LicenseInvalidCountry"
    })
], LicensePost.prototype, "country_code", void 0);
__decorate([
    class_validator_1.Length(0, 25, {
        message: "admin.validation.LicenseInvalidImage"
    })
], LicensePost.prototype, "image", void 0);
exports.LicensePost = LicensePost;

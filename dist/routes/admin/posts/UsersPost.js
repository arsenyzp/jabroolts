"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
class UsersPost {
    constructor() {
        this._id = "";
    }
}
__decorate([
    class_validator_1.Length(2, 25, {
        message: "admin.validation.UserFirstName"
    })
], UsersPost.prototype, "first_name", void 0);
__decorate([
    class_validator_1.Length(2, 25, {
        message: "admin.validation.UserLastName"
    })
], UsersPost.prototype, "last_name", void 0);
__decorate([
    class_validator_1.IsEmail({}, {
        message: "admin.validation.UserInvalidEmail"
    })
], UsersPost.prototype, "email", void 0);
__decorate([
    class_validator_1.Length(2, 25, {
        message: "admin.validation.UserInvalidPhone"
    })
], UsersPost.prototype, "phone", void 0);
__decorate([
    class_validator_1.IsNumber({
        message: "admin.validation.UserInvalidBalance"
    })
], UsersPost.prototype, "new_balance", void 0);
__decorate([
    class_validator_1.IsNumber({
        message: "admin.validation.UserInvalidCourierValue"
    })
], UsersPost.prototype, "new_courierLimit", void 0);
__decorate([
    class_validator_1.Length(2, 25, {
        message: "admin.validation.UserInvalidUserStatus"
    })
], UsersPost.prototype, "status", void 0);
__decorate([
    class_validator_1.Length(2, 25, {
        message: "admin.validation.UserInvalidUserRole"
    })
], UsersPost.prototype, "role", void 0);
__decorate([
    class_validator_1.Length(2, 25, {
        message: "admin.validation.UserInvalidUserType"
    })
], UsersPost.prototype, "type", void 0);
__decorate([
    class_validator_1.Length(0, 25, {
        message: "admin.validation.UserInvalidUserInvalidImage"
    })
], UsersPost.prototype, "avatar", void 0);
__decorate([
    class_validator_1.IsBoolean({
        message: "admin.validation.VisibleInvalidValue"
    })
], UsersPost.prototype, "visible", void 0);
__decorate([
    class_validator_1.IsBoolean({
        message: "admin.validation.InprogressInvalidValue"
    })
], UsersPost.prototype, "in_progress", void 0);
__decorate([
    class_validator_1.Length(0, 8, {
        message: "admin.validation.PasswordInvalidValue",
        always: false
    })
], UsersPost.prototype, "new_password", void 0);
exports.UsersPost = UsersPost;

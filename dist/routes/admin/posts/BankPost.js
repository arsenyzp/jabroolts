"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
class BankPost {
}
__decorate([
    class_validator_1.Length(2, 25, {
        message: "admin.validation.BankBranch"
    })
], BankPost.prototype, "branch", void 0);
__decorate([
    class_validator_1.Length(2, 25, {
        message: "admin.validation.BankAccount"
    })
], BankPost.prototype, "account_number", void 0);
__decorate([
    class_validator_1.Length(2, 25, {
        message: "admin.validation.HolderName"
    })
], BankPost.prototype, "holder_name", void 0);
__decorate([
    class_validator_1.Length(2, 25, {
        message: "admin.validation.BankType"
    })
], BankPost.prototype, "type", void 0);
__decorate([
    class_validator_1.Length(2, 25, {
        message: "admin.validation.InvalidBank"
    })
], BankPost.prototype, "bank", void 0);
__decorate([
    class_validator_1.Length(2, 25, {
        message: "admin.validation.InvalidCountry"
    })
], BankPost.prototype, "country_code", void 0);
exports.BankPost = BankPost;

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
class VehiclePost {
}
__decorate([
    class_validator_1.Length(2, 4, {
        message: "admin.validation.InvalidYear"
    })
], VehiclePost.prototype, "year", void 0);
__decorate([
    class_validator_1.Length(2, 25, {
        message: "admin.validation.InvalidModel"
    })
], VehiclePost.prototype, "model", void 0);
__decorate([
    class_validator_1.Length(2, 25, {
        message: "admin.validation.VehicleType"
    })
], VehiclePost.prototype, "type", void 0);
__decorate([
    class_validator_1.Length(2, 25, {
        message: "admin.validation.VehicleNumber"
    })
], VehiclePost.prototype, "number", void 0);
__decorate([
    class_validator_1.Length(0, 25, {
        message: "admin.validation.InvalidImageFront"
    })
], VehiclePost.prototype, "image_front", void 0);
__decorate([
    class_validator_1.Length(0, 25, {
        message: "admin.validation.InvalidImageBack"
    })
], VehiclePost.prototype, "image_back", void 0);
__decorate([
    class_validator_1.Length(0, 25, {
        message: "admin.validation.InvalidImageSide"
    })
], VehiclePost.prototype, "image_side", void 0);
__decorate([
    class_validator_1.Length(0, 25, {
        message: "admin.validation.InvalidImagesInsurance",
        each: true
    })
], VehiclePost.prototype, "images_insurance", void 0);
__decorate([
    class_validator_1.Length(0, 25, {
        message: "admin.validation.ImagesInsuranceDelete",
        each: true
    })
], VehiclePost.prototype, "images_insurance_delete", void 0);
exports.VehiclePost = VehiclePost;

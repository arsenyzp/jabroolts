"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
const escapeStringRegexp = require("escape-string-regexp");
const AjaxResponse_1 = require("../../components/AjaxResponse");
const PromoCodModel_1 = require("../../models/PromoCodModel");
const class_validator_1 = require("class-validator");
const PromoCodHelper_1 = require("../../components/jabrool/PromoCodHelper");
var Route;
(function (Route) {
    class PromocodesPost {
        constructor() {
            this._id = "";
        }
    }
    __decorate([
        class_validator_1.IsInt({
            message: "admin.validation.PromoAmount"
        }),
        class_validator_1.Min(1, {
            message: "admin.validation.PromoAmount"
        })
    ], PromocodesPost.prototype, "amount", void 0);
    __decorate([
        class_validator_1.IsBoolean({
            message: "admin.validation.PromoForCourier"
        })
    ], PromocodesPost.prototype, "forCourier", void 0);
    class Promocodes {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let sort = {};
            if (req.query.sortKey) {
                sort[req.query.sortKey] = req.query.reverse;
            }
            else {
                sort = { created_at: -1 };
            }
            let filter = {};
            let search = "";
            if (req.query.q !== undefined && req.query.q !== "") {
                search = escapeStringRegexp(req.query.q);
                filter = { $or: [] };
                if (parseInt(search).toString() === search.toString()) {
                    filter.$or.push({ amount: parseInt(search) });
                    filter.$or.push({ code: new RegExp(".*" + search + ".*", "i") });
                }
                else {
                    filter.$or.push({ code: new RegExp(".*" + search + ".*", "i") });
                }
            }
            let skip = 0;
            let limit = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }
            PromoCodModel_1.PromoCodModel
                .count(filter)
                .then(total_count => {
                PromoCodModel_1.PromoCodModel
                    .find(filter)
                    .sort(sort)
                    .limit(limit)
                    .skip(skip)
                    .then(items => {
                    ajaxResponse.setDate({
                        items: items,
                        total_count: total_count
                    });
                    res.json(ajaxResponse.get());
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        get(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            PromoCodModel_1.PromoCodModel
                .findOne({ _id: req.params.id })
                .then(bank => {
                if (bank == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                ajaxResponse.setDate(bank);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        delete(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            PromoCodModel_1.PromoCodModel
                .findOne({ _id: req.params.id })
                .then(bank => {
                if (bank == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                PromoCodModel_1.PromoCodModel
                    .remove({ _id: req.params.id })
                    .then(r => {
                    res.json(ajaxResponse.get());
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        save(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let post = Object.create(PromocodesPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                }
                else {
                    PromoCodModel_1.PromoCodModel
                        .findOne({ _id: post._id })
                        .then(item => {
                        if (item == null) {
                            item = new PromoCodModel_1.PromoCodModel();
                        }
                        item.amount = post.amount;
                        item.forCourier = post.forCourier;
                        let helper = new PromoCodHelper_1.PromoCodHelper(item);
                        helper
                            .save()
                            .then(item => {
                            ajaxResponse.setDate(item);
                            res.json(ajaxResponse.get());
                        })
                            .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                }
            });
        }
    }
    Route.Promocodes = Promocodes;
})(Route || (Route = {}));
module.exports = Route;

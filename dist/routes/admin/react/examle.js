"use strict";
var Route;
(function (Route) {
    class Index {
        index(req, res, next) {
            let css = [
                "/bower_components/angular-ui-notification/dist/angular-ui-notification.min.css",
                "/bower_components/angular-popeye/release/popeye.min.css",
                "/bower_components/components-font-awesome/css/font-awesome.min.css",
                "/js/libs/ui-bootstrap-custom-build/ui-bootstrap-custom-2.5.0-csp.css",
                "/css/admin.css"
            ];
            let js = [
                "/bower_components/jquery/dist/jquery.min.js",
                "/bower_components/popper.js/dist/umd/popper.min.js",
                "/bower_components/bootstrap/dist/js/bootstrap.min.js",
                "/bower_components/angular/angular.min.js",
                "/bower_components/angular-ui-notification/dist/angular-ui-notification.min.js",
                "/bower_components/angular-popeye/release/popeye.min.js",
                "/bower_components/angular-spinner/dist/angular-spinner.min.js",
                "/bower_components/angularUtils-pagination/dirPagination.js",
                "/bower_components/angular-route/angular-route.min.js",
                "/bower_components/ng-file-upload-shim/ng-file-upload-shim.min.js",
                "/bower_components/ng-file-upload/ng-file-upload.min.js",
                "/bower_components/angular-translate/angular-translate.min.js",
                "/js/libs/ui-bootstrap-custom-build/ui-bootstrap-custom-2.5.0.min.js",
                "/js/libs/ui-bootstrap-custom-build/ui-bootstrap-custom-tpls-2.5.0.min.js",
                "/js/admin/admin.js",
                "/js/admin/main.js",
                "/js/admin/users.js",
                "/js/admin/newUsers.js",
                "/js/admin/banks.js",
                "/js/admin/countries.js",
                "/js/admin/manufacturers.js",
                "/js/admin/car_types.js",
                "/js/admin/bank_types.js",
                "/js/admin/promocodes.js",
                "/js/admin/config/packages.js",
                "/js/admin/config/radius.js",
                "/js/admin/config/package_ratio.js",
                "/js/admin/config/delivery.js",
                "/js/admin/config/terms.js",
                "/js/admin/config/cancel_params.js",
                "/js/admin/config/logs.js",
                "/js/admin/config/price.js",
                "/js/admin/config/bonuses.js",
                "/js/admin/logs/api.js",
                "/js/admin/logs/sms.js",
                "/js/admin/logs/email.js",
                "/js/admin/payments.js",
                "/js/admin/contact_us.js",
                "/js/admin/reports/couriers.js",
                "/js/admin/reports/hours_reports.js",
                "/js/admin/reports/orders.js",
                "/js/admin/reports/customers.js",
                "/js/admin/reports/courior_clusters.js",
                "/js/admin/reports/cusotmer_clusters.js",
            ];
            res.render("admin/react/example", {
                css: css,
                js: js
            });
        }
        react(req, res, next) {
            let css = [];
            let js = [
                "/react/dist/index_bundle.js",
            ];
            res.render("admin/layouts/main", {
                css: css,
                js: js
            });
        }
    }
    Route.Index = Index;
})(Route || (Route = {}));
module.exports = Route;

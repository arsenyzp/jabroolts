"use strict";
const escapeStringRegexp = require("escape-string-regexp");
const AjaxResponse_1 = require("../../components/AjaxResponse");
const UserModel_1 = require("../../models/UserModel");
var Route;
(function (Route) {
    class NewUsers {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let sort = {};
            if (req.query.sortKey) {
                sort[req.query.sortKey] = req.query.reverse;
            }
            else {
                sort = { created_at: -1 };
            }
            let filter = {
                "license.image": { $ne: "" },
                "vehicle.number": { $ne: "" },
                "bank.account_number": { $ne: "" },
                "status": { $ne: UserModel_1.UserStatus.Active }
            };
            let search = "";
            if (req.query.q !== undefined && req.query.q !== "") {
                search = escapeStringRegexp(req.query.q);
                filter.$or = [
                    { first_name: new RegExp(".*" + search + ".*", "i") },
                    { last_name: new RegExp(".*" + search + ".*", "i") },
                    { phone: new RegExp(".*" + search + ".*", "i") },
                ];
                sort = { created_at: -1 };
            }
            filter.deleted = { $in: [false, null] };
            let skip = 0;
            let limit = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }
            UserModel_1.UserModel
                .count(filter)
                .then(total_count => {
                UserModel_1.UserModel
                    .find(filter)
                    .sort(sort)
                    .limit(limit)
                    .skip(skip)
                    .then(users => {
                    let items = [];
                    for (let i = 0; i < users.length; i++) {
                        let item = users[i].getApiFields();
                        item.bank = users[i].getApiFields();
                        item.vehicle = users[i].getVehicle();
                        item.bank = users[i].getBank();
                        item.license = users[i].getLicense();
                        item.socket_ids = users[i].socket_ids;
                        item._id = users[i]._id;
                        items.push(item);
                    }
                    ajaxResponse.setDate({
                        items: items,
                        total_count: total_count
                    });
                    res.json(ajaxResponse.get());
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
    }
    Route.NewUsers = NewUsers;
})(Route || (Route = {}));
module.exports = Route;

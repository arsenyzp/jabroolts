"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const escapeStringRegexp = require("escape-string-regexp");
const AjaxResponse_1 = require("../../components/AjaxResponse");
const UserModel_1 = require("../../models/UserModel");
const class_validator_1 = require("class-validator");
const FileHelper_1 = require("../../components/FileHelper");
const config_1 = require("../../components/config");
const Log_1 = require("../../components/Log");
const BankModel_1 = require("../../models/BankModel");
const CountryModel_1 = require("../../models/CountryModel");
const BankTypeModel_1 = require("../../models/BankTypeModel");
const CarTypeModel_1 = require("../../models/CarTypeModel");
const ManufactureModel_1 = require("../../models/ManufactureModel");
const PaymentModule_1 = require("../../components/jabrool/PaymentModule");
const UsersPost_1 = require("./posts/UsersPost");
const BankPost_1 = require("./posts/BankPost");
const LicensePost_1 = require("./posts/LicensePost");
const VehiclePost_1 = require("./posts/VehiclePost");
const CourierBalanceLogModel_1 = require("../../models/CourierBalanceLogModel");
const BonusesConfig_1 = require("../../models/configs/BonusesConfig");
const SocketServer_1 = require("../../components/SocketServer");
const NotificationModule_1 = require("../../components/NotificationModule");
const async = require("async");
const log = Log_1.Log.getInstanse()(module);
var Route;
(function (Route) {
    class Users {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let sort = {};
            if (req.query.sortKey) {
                sort[req.query.sortKey] = req.query.reverse;
            }
            else {
                sort = { created_at: -1 };
            }
            let filter = {};
            let search = "";
            if (req.query.q !== undefined && req.query.q !== "") {
                search = escapeStringRegexp(req.query.q);
                filter = { $or: [
                        { first_name: new RegExp(".*" + search + ".*", "i") },
                        { last_name: new RegExp(".*" + search + ".*", "i") },
                        { phone: new RegExp(".*" + search + ".*", "i") },
                    ] };
                sort = { created_at: -1 };
            }
            filter.deleted = { $in: [false, null] };
            let skip = 0;
            let limit = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }
            UserModel_1.UserModel
                .count(filter)
                .then(total_count => {
                UserModel_1.UserModel
                    .find(filter)
                    .sort(sort)
                    .limit(limit)
                    .skip(skip)
                    .then(users => {
                    let items = [];
                    for (let i = 0; i < users.length; i++) {
                        let item = users[i].getApiFields();
                        item.bank = users[i].getApiFields();
                        item.vehicle = users[i].getVehicle();
                        item.bank = users[i].getBank();
                        item.license = users[i].getLicense();
                        item.socket_ids = users[i].socket_ids;
                        item._id = users[i]._id;
                        items.push(item);
                    }
                    ajaxResponse.setDate({
                        items: items,
                        total_count: total_count
                    });
                    res.json(ajaxResponse.get());
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        business(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let sort = {};
            if (req.query.sortKey) {
                sort[req.query.sortKey] = req.query.reverse;
            }
            else {
                sort = { created_at: -1 };
            }
            let filter = { type: UserModel_1.UserTypes.Business };
            let search = "";
            if (req.query.q !== undefined && req.query.q !== "") {
                search = escapeStringRegexp(req.query.q);
                filter = { $or: [
                        { first_name: new RegExp(".*" + search + ".*", "i") },
                        { last_name: new RegExp(".*" + search + ".*", "i") },
                        { phone: new RegExp(".*" + search + ".*", "i") },
                    ],
                    type: UserModel_1.UserTypes.Business
                };
                sort = { created_at: -1 };
            }
            filter.deleted = { $in: [false, null] };
            let skip = 0;
            let limit = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }
            UserModel_1.UserModel
                .count(filter)
                .then(total_count => {
                UserModel_1.UserModel
                    .find(filter)
                    .sort(sort)
                    .limit(limit)
                    .skip(skip)
                    .then(users => {
                    let items = [];
                    for (let i = 0; i < users.length; i++) {
                        let item = users[i].getApiFields();
                        item.bank = users[i].getApiFields();
                        item.vehicle = users[i].getVehicle();
                        item.bank = users[i].getBank();
                        item.license = users[i].getLicense();
                        item.socket_ids = users[i].socket_ids;
                        item._id = users[i]._id;
                        items.push(item);
                    }
                    ajaxResponse.setDate({
                        items: items,
                        total_count: total_count
                    });
                    res.json(ajaxResponse.get());
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        get(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            UserModel_1.UserModel
                .findOne({ _id: req.params.id })
                .then(u => {
                if (u == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                ajaxResponse.setDate(u);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        resetJabroolFee(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let bonusesModel = new BonusesConfig_1.BonusesConfig();
            bonusesModel
                .getInstanse()
                .then(conf => {
                UserModel_1.UserModel
                    .findOneAndUpdate({ _id: req.params.id }, {
                    courierLimit: conf.defaultCourierLimit,
                    jabroolFee: 0
                }, { new: true })
                    .then(u => {
                    if (u == null) {
                        ajaxResponse.addErrorMessage("Item not found");
                        res.status(500);
                        return res.json(ajaxResponse.get());
                    }
                    ajaxResponse.setDate(u);
                    res.json(ajaxResponse.get());
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        getBank(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let user;
            let banks;
            let countries;
            let types;
            async.parallel([
                    cb => {
                    UserModel_1.UserModel
                        .findOne({ _id: req.params.id })
                        .then(u => {
                        if (u == null) {
                            cb("Item not found");
                        }
                        user = u;
                        cb();
                    })
                        .catch(err => {
                        cb(err);
                    });
                },
                    cb => {
                    BankModel_1.BankModel
                        .find({})
                        .then(u => {
                        banks = u;
                        cb();
                    })
                        .catch(err => {
                        cb(err);
                    });
                },
                    cb => {
                    BankTypeModel_1.BankTypeModel
                        .find({})
                        .then(u => {
                        types = u;
                        cb();
                    })
                        .catch(err => {
                        cb(err);
                    });
                },
                    cb => {
                    CountryModel_1.CountryModel
                        .find({})
                        .then(u => {
                        countries = u;
                        cb();
                    })
                        .catch(err => {
                        cb(err);
                    });
                },
            ], err => {
                if (err) {
                    log.error(err);
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
                else {
                    ajaxResponse.setDate({
                        user: user,
                        banks: banks,
                        types: types,
                        countries: countries
                    });
                    res.json(ajaxResponse.get());
                }
            });
        }
        saveBank(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let post = Object.create(BankPost_1.BankPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                }
                else {
                    UserModel_1.UserModel
                        .findOne({ _id: req.params.id })
                        .then(item => {
                        if (item == null) {
                            item = new UserModel_1.UserModel();
                        }
                        item.bank.bank = post.bank;
                        item.bank.account_number = post.account_number;
                        item.bank.branch = post.branch;
                        item.bank.country_code = post.country_code;
                        item.bank.type = post.type;
                        item.bank.holder_name = post.holder_name;
                        Users._save(item, res, ajaxResponse);
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                }
            });
        }
        getLicense(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let user;
            let countries;
            async.parallel([
                    cb => {
                    UserModel_1.UserModel
                        .findOne({ _id: req.params.id })
                        .then(u => {
                        if (u == null) {
                            cb("Item not found");
                        }
                        user = u;
                        cb();
                    })
                        .catch(err => {
                        cb(err);
                    });
                },
                    cb => {
                    CountryModel_1.CountryModel
                        .find({})
                        .then(u => {
                        countries = u;
                        cb();
                    })
                        .catch(err => {
                        cb(err);
                    });
                },
            ], err => {
                if (err) {
                    log.error(err);
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
                else {
                    ajaxResponse.setDate({
                        user: user,
                        countries: countries
                    });
                    res.json(ajaxResponse.get());
                }
            });
        }
        saveLicense(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let post = Object.create(LicensePost_1.LicensePost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                }
                else {
                    UserModel_1.UserModel
                        .findOne({ _id: req.params.id })
                        .then(item => {
                        if (item == null) {
                            item = new UserModel_1.UserModel();
                        }
                        item.license.name = post.name;
                        item.license.number = post.number;
                        item.license.issue_date = new Date(post.issue_date).getTime();
                        item.license.expiry_date = new Date(post.expiry_date).getTime();
                        item.license.country_code = post.country_code;
                        if (item.license.image !== post.image) {
                            if (post.image !== "") {
                                FileHelper_1.FileHelper.move(config_1.AppConfig.getInstanse().get("files:user_uploads") + "/" + post.image, config_1.AppConfig.getInstanse().get("files:user_license") + "/" + post.image, err => {
                                    if (err) {
                                        ajaxResponse.addError(err);
                                        res.status(500);
                                        res.json(ajaxResponse.get());
                                    }
                                    else {
                                        item.license.image = post.image;
                                        Users._save(item, res, ajaxResponse);
                                    }
                                });
                            }
                            else {
                                item.license.image = "";
                                Users._save(item, res, ajaxResponse);
                            }
                        }
                        else {
                            Users._save(item, res, ajaxResponse);
                        }
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                }
            });
        }
        getVehicle(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                try {
                    let user = yield UserModel_1.UserModel
                        .findOne({ _id: req.params.id });
                    let manufacturer = yield ManufactureModel_1.ManufactureModel.findOne({ _id: user.vehicle.model });
                    let types = [];
                    if (manufacturer !== null) {
                        types = yield CarTypeModel_1.CarTypeModel
                            .find({ _id: manufacturer.types });
                    }
                    let manufacturers = yield ManufactureModel_1.ManufactureModel.find({});
                    let years = [];
                    for (let i = 2000; i <= new Date().getUTCFullYear(); i++) {
                        years.push(i);
                    }
                    ajaxResponse.setDate({
                        user: user,
                        types: types,
                        models: manufacturers,
                        years: years
                    });
                    res.json(ajaxResponse.get());
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
        saveVehicle(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let post = Object.create(VehiclePost_1.VehiclePost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                }
                else {
                    UserModel_1.UserModel
                        .findOne({ _id: req.params.id })
                        .then(item => {
                        if (item == null) {
                            item = new UserModel_1.UserModel();
                        }
                        item.vehicle.number = post.number;
                        item.vehicle.type = post.type;
                        item.vehicle.model = post.model;
                        item.vehicle.year = post.year;
                        async.parallel([
                                cb => {
                                if (item.vehicle.image_front !== post.image_front) {
                                    if (post.image_front !== "") {
                                        FileHelper_1.FileHelper.move(config_1.AppConfig.getInstanse().get("files:user_uploads") + "/" + post.image_front, config_1.AppConfig.getInstanse().get("files:user_vehicle") + "/" + post.image_front, err => {
                                            if (err) {
                                                ajaxResponse.addError(err);
                                                res.status(500);
                                                res.json(ajaxResponse.get());
                                            }
                                            else {
                                                item.vehicle.image_front = post.image_front;
                                                cb();
                                            }
                                        });
                                    }
                                    else {
                                        item.vehicle.image_front = "";
                                        cb();
                                    }
                                }
                                else {
                                    cb();
                                }
                            },
                                cb => {
                                if (item.vehicle.image_side !== post.image_side) {
                                    if (post.image_side !== "") {
                                        FileHelper_1.FileHelper.move(config_1.AppConfig.getInstanse().get("files:user_uploads") + "/" + post.image_side, config_1.AppConfig.getInstanse().get("files:user_vehicle") + "/" + post.image_side, err => {
                                            if (err) {
                                                ajaxResponse.addError(err);
                                                res.status(500);
                                                res.json(ajaxResponse.get());
                                            }
                                            else {
                                                item.vehicle.image_side = post.image_side;
                                                cb();
                                            }
                                        });
                                    }
                                    else {
                                        item.vehicle.image_side = "";
                                        cb();
                                    }
                                }
                                else {
                                    cb();
                                }
                            },
                                cb => {
                                if (item.vehicle.image_back !== post.image_back) {
                                    if (post.image_back !== "") {
                                        FileHelper_1.FileHelper.move(config_1.AppConfig.getInstanse().get("files:user_uploads") + "/" + post.image_back, config_1.AppConfig.getInstanse().get("files:user_vehicle") + "/" + post.image_back, err => {
                                            if (err) {
                                                ajaxResponse.addError(err);
                                                res.status(500);
                                                res.json(ajaxResponse.get());
                                            }
                                            else {
                                                item.vehicle.image_back = post.image_back;
                                                cb();
                                            }
                                        });
                                    }
                                    else {
                                        item.vehicle.image_back = "";
                                        cb();
                                    }
                                }
                                else {
                                    cb();
                                }
                            },
                                cb => {
                                let images = [];
                                async.forEachOfSeries(post.images_insurance, (v, k, cb) => {
                                    if (item.vehicle.images_insurance.indexOf(v) < 0) {
                                        FileHelper_1.FileHelper.move(config_1.AppConfig.getInstanse().get("files:user_uploads") + "/" + v, config_1.AppConfig.getInstanse().get("files:user_vehicle") + "/" + v, err => {
                                            if (err) {
                                                cb(err);
                                            }
                                            else {
                                                images.push(v);
                                                cb();
                                            }
                                        });
                                    }
                                    else {
                                        images.push(v);
                                        cb();
                                    }
                                }, err => {
                                    if (err) {
                                        cb(err);
                                    }
                                    else {
                                        item.vehicle.images_insurance = images;
                                        cb();
                                    }
                                });
                            },
                                cb => {
                                let images = [];
                                for (let i = 0; i < item.vehicle.images_insurance.length; i++) {
                                    if (post.images_insurance_delete.indexOf(item.vehicle.images_insurance[i]) < 0) {
                                        images.push(item.vehicle.images_insurance[i]);
                                    }
                                }
                                item.vehicle.images_insurance = images;
                                cb();
                            },
                        ], err => {
                            if (err) {
                                ajaxResponse.addError(err);
                                res.status(500);
                                res.json(ajaxResponse.get());
                            }
                            else {
                                Users._save(item, res, ajaxResponse);
                            }
                        });
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                }
            });
        }
        delete(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            UserModel_1.UserModel
                .findOne({ _id: req.params.id })
                .then(u => {
                if (u == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                if (u.socket_ids && u.socket_ids.length > 0) {
                    for (let i = 0; i < u.socket_ids.length; i++) {
                        if (SocketServer_1.SocketServer.getInstance().sockets.sockets[u.socket_ids[i]]) {
                            SocketServer_1.SocketServer.getInstance().sockets.sockets[u.socket_ids[i]].emit("logout", {});
                        }
                    }
                }
                UserModel_1.UserModel
                    .findOneAndUpdate({ _id: req.params.id }, {
                    phone: u._id + "+" + u.phone,
                    email: u._id + "+" + u.email,
                    deleted: true
                })
                    .then(user => {
                    res.json(ajaxResponse.get());
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        save(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let post = Object.create(UsersPost_1.UsersPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                }
                else {
                    UserModel_1.UserModel.findOne({
                        $or: [
                            { email: post.email },
                            { phone: post.phone }
                        ],
                        _id: { $ne: post._id }
                    })
                        .then(u => {
                        if (u !== null) {
                            if (u.phone === post.phone) {
                                ajaxResponse.addErrorMessage("User phone already used");
                            }
                            else {
                                ajaxResponse.addErrorMessage("User email already used");
                            }
                            res.status(500);
                            res.json(ajaxResponse.get());
                        }
                        else {
                            let bonusesModel = new BonusesConfig_1.BonusesConfig();
                            bonusesModel
                                .getInstanse()
                                .then(conf => {
                                UserModel_1.UserModel
                                    .findOne({ _id: post._id })
                                    .then(item => {
                                    if (item == null) {
                                        item = new UserModel_1.UserModel();
                                        item.location.coordinates = [0, 0];
                                        item.location.type = "Point";
                                        item.courierLimit = conf.defaultCourierLimit;
                                        item.genToken();
                                    }
                                    if (post.new_password !== "") {
                                        item.setPassword(post.new_password);
                                        item.genToken();
                                    }
                                    item.first_name = post.first_name;
                                    item.last_name = post.last_name;
                                    item.email = post.email;
                                    item.phone = post.phone;
                                    item.role = post.role;
                                    item.type = post.type;
                                    if (item.status !== post.status && post.status === UserModel_1.UserStatus.Active) {
                                        NotificationModule_1.NotificationModule.getInstance().send([item._id], NotificationModule_1.NotificationTypes.Activated, {}, item._id);
                                    }
                                    item.status = post.status;
                                    item.visible = post.visible;
                                    item.in_progress = post.in_progress;
                                    if (post.new_courierLimit !== 0) {
                                        let model = new CourierBalanceLogModel_1.CourierBalanceLogModel();
                                        model.user = item;
                                        model.amount = post.new_courierLimit;
                                        model.type = CourierBalanceLogModel_1.CourierBalanceTypes.Admin;
                                        model.save();
                                        item.courierLimit += post.new_courierLimit;
                                    }
                                    if (post.new_balance !== 0) {
                                        PaymentModule_1.PaymentModule
                                            .correctBalanceFromAdmin(item._id, post.new_balance)
                                            .then(r => {
                                            Users._preSave(item, post, ajaxResponse, res);
                                        })
                                            .catch(err => {
                                            ajaxResponse.addError(err);
                                            res.status(500);
                                            res.json(ajaxResponse.get());
                                        });
                                    }
                                    else {
                                        Users._preSave(item, post, ajaxResponse, res);
                                    }
                                })
                                    .catch(err => {
                                    ajaxResponse.addError(err);
                                    res.status(500);
                                    res.json(ajaxResponse.get());
                                });
                            })
                                .catch(err => {
                                ajaxResponse.addError(err);
                                res.status(500);
                                res.json(ajaxResponse.get());
                            });
                        }
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                }
            });
        }
        static _preSave(item, post, ajaxResponse, res) {
            if (item.avatar !== post.avatar) {
                if (post.avatar !== "") {
                    FileHelper_1.FileHelper.move(config_1.AppConfig.getInstanse().get("files:user_uploads") + "/" + post.avatar, config_1.AppConfig.getInstanse().get("files:user_avatars") + "/" + post.avatar, err => {
                        if (err) {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        }
                        else {
                            item.avatar = post.avatar;
                            Users._save(item, res, ajaxResponse);
                        }
                    });
                }
                else {
                    item.avatar = "";
                    Users._save(item, res, ajaxResponse);
                }
            }
            else {
                Users._save(item, res, ajaxResponse);
            }
        }
        static _save(item, res, ajaxResponse) {
            if (!item.jabroolid) {
                item
                    .generateJId()
                    .then(() => {
                    item
                        .save()
                        .then(item => {
                        ajaxResponse.setDate(item);
                        res.json(ajaxResponse.get());
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            }
            else {
                item
                    .save()
                    .then(item => {
                    ajaxResponse.setDate(item);
                    res.json(ajaxResponse.get());
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            }
        }
    }
    Route.Users = Users;
})(Route || (Route = {}));
module.exports = Route;

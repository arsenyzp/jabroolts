"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const escapeStringRegexp = require("escape-string-regexp");
const AjaxResponse_1 = require("../../components/AjaxResponse");
const PaymentLog_1 = require("../../models/PaymentLog");
const UserModel_1 = require("../../models/UserModel");
var Route;
(function (Route) {
    class Payments {
        index(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                let sort = {};
                if (req.query.sortKey) {
                    sort[req.query.sortKey] = req.query.reverse;
                }
                else {
                    sort = { created_at: -1 };
                }
                let filter = {};
                let search = "";
                if (req.query.q !== undefined && req.query.q !== "") {
                    search = escapeStringRegexp(req.query.q);
                    let filter2 = { $or: [
                            { "email": new RegExp(".*" + search + ".*", "i") },
                            { "phone": new RegExp(".*" + search + ".*", "i") }
                        ] };
                    let users = yield UserModel_1.UserModel.find(filter2);
                    let ids = [];
                    users.map((v, k, a) => {
                        ids.push(v._id.toString());
                    });
                    filter.user = { $in: ids };
                }
                let skip = 0;
                let limit = 10;
                if (req.params.page && req.params.perPage) {
                    req.params.page = parseInt(req.params.page) - 1;
                    skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                    limit = parseInt(req.params.perPage);
                }
                PaymentLog_1.PaymentLogModel
                    .count(filter)
                    .then(total_count => {
                    PaymentLog_1.PaymentLogModel
                        .find(filter)
                        .populate({ path: "user", model: UserModel_1.UserModel })
                        .sort(sort)
                        .limit(limit)
                        .skip(skip)
                        .then(items => {
                        ajaxResponse.setDate({
                            items: items,
                            total_count: total_count
                        });
                        res.json(ajaxResponse.get());
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            });
        }
        get(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            PaymentLog_1.PaymentLogModel
                .findOne({ _id: req.params.id })
                .populate({ path: "user", model: UserModel_1.UserModel })
                .then(bank => {
                if (bank == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                ajaxResponse.setDate(bank);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        delete(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            PaymentLog_1.PaymentLogModel
                .findOne({ _id: req.params.id })
                .then(bank => {
                if (bank == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                PaymentLog_1.PaymentLogModel
                    .remove({ _id: req.params.id })
                    .then(r => {
                    res.json(ajaxResponse.get());
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
    }
    Route.Payments = Payments;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
var Route;
(function (Route) {
    class Index {
        index(req, res, next) {
            let css = [];
            let js = [
                "/dist_admin/admin_bundle.js",
            ];
            res.render("admin/layouts/main", {
                css: css,
                js: js
            });
        }
    }
    Route.Index = Index;
})(Route || (Route = {}));
module.exports = Route;

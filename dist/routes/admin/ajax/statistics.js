"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const UserModel_1 = require("../../../models/UserModel");
const OrderModel_1 = require("../../../models/OrderModel");
const DebtLogModel_1 = require("../../../models/DebtLogModel");
const UserActivityLogModel_1 = require("../../../models/UserActivityLogModel");
var Route;
(function (Route) {
    class StatObj {
        constructor() {
            this.total_users = 0;
            this.total_couriers = 0;
            this.total_orders = 0;
            this.jabrool_earning = 0;
            this.couriers_earning = 0;
            this.users_debt = 0;
            this.percent_total_users = 0;
            this.percent_total_couriers = 0;
            this.percent_total_orders = 0;
            this.percent_jabrool_earning = 0;
            this.percent_couriers_earning = 0;
            this.percent_users_debt = 0;
        }
    }
    class DateInterval {
        constructor() {
            this.startAt = 0;
            this.endAt = 0;
        }
    }
    class Statistics {
        index(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                let stat = new StatObj();
                let currentDate = new Date();
                let currentWeek = new DateInterval();
                currentWeek.endAt = currentDate.getTime();
                currentDate.setDate(currentDate.getDate() - currentDate.getDay() + 1);
                currentWeek.startAt = currentDate.getTime();
                let prevWeek = new DateInterval();
                prevWeek.endAt = currentDate.getTime();
                currentDate.setDate(currentDate.getDate() - 7);
                prevWeek.startAt = currentDate.getTime();
                let totalUsers = yield UserModel_1.UserModel.count({});
                let totalUsersCurrentWeek = yield UserModel_1.UserModel.count({ created_at: { $gte: currentWeek.startAt, $lte: currentWeek.endAt } });
                let totalUsersPrevWeek = yield UserModel_1.UserModel.count({ created_at: { $gte: prevWeek.startAt, $lte: prevWeek.endAt } });
                stat.total_users = totalUsers;
                stat.percent_total_users = (totalUsersCurrentWeek - totalUsersPrevWeek) / (totalUsersPrevWeek / 100);
                if (totalUsersPrevWeek === 0) {
                    stat.percent_total_users = 0;
                }
                let totalCouriers = yield UserModel_1.UserModel.count({ status: UserModel_1.UserStatus.Active });
                let totalCouriersCurrentWeek = yield UserModel_1.UserModel.count({
                    created_at: { $gte: currentWeek.startAt, $lte: currentWeek.endAt },
                    status: UserModel_1.UserStatus.Active
                });
                let totalCouriersPrevWeek = yield UserModel_1.UserModel.count({
                    created_at: { $gte: prevWeek.startAt, $lte: prevWeek.endAt },
                    status: UserModel_1.UserStatus.Active
                });
                stat.total_couriers = totalCouriers;
                stat.percent_total_couriers = (totalCouriersCurrentWeek - totalCouriersPrevWeek) / (totalCouriersPrevWeek / 100);
                if (totalCouriersPrevWeek === 0) {
                    stat.percent_total_couriers = 0;
                }
                let totalOrders = yield OrderModel_1.OrderModel.count({});
                let totalOrdersCurrentWeek = yield OrderModel_1.OrderModel.count({ created_at: { $gte: currentWeek.startAt, $lte: currentWeek.endAt } });
                let totalOrdersPrevWeek = yield OrderModel_1.OrderModel.count({ created_at: { $gte: prevWeek.startAt, $lte: prevWeek.endAt } });
                stat.total_orders = totalOrders;
                stat.percent_total_orders = (totalOrdersCurrentWeek - totalOrdersPrevWeek) / (totalOrdersPrevWeek / 100);
                if (totalOrdersPrevWeek === 0) {
                    stat.percent_total_orders = 0;
                }
                let jabroolEarningTotal = 0;
                let jabroolEarningCurrentWeek = 0;
                let jabroolEarningPrevWeek = 0;
                let courierEarningTotal = 0;
                let courierEarningCurrentWeek = 0;
                let courierEarningPrevWeek = 0;
                let ordersFinished = yield OrderModel_1.OrderModel.find({ status: OrderModel_1.OrderStatuses.Finished });
                let ordersFinishedCurrentWeek = yield OrderModel_1.OrderModel.find({
                    created_at: { $gte: currentWeek.startAt, $lte: currentWeek.endAt },
                    status: OrderModel_1.OrderStatuses.Finished
                });
                let ordersFinishedPrevWeek = yield OrderModel_1.OrderModel.find({
                    created_at: { $gte: prevWeek.startAt, $lte: prevWeek.endAt },
                    status: OrderModel_1.OrderStatuses.Finished
                });
                ordersFinished.map((v, k, a) => {
                    jabroolEarningTotal += v.serviceFee;
                    courierEarningTotal += v.cost - v.serviceFee;
                });
                ordersFinishedCurrentWeek.map((v, k, a) => {
                    jabroolEarningCurrentWeek += v.serviceFee;
                    courierEarningCurrentWeek += v.cost - v.serviceFee;
                });
                ordersFinishedPrevWeek.map((v, k, a) => {
                    jabroolEarningPrevWeek += v.serviceFee;
                    courierEarningPrevWeek += v.cost - v.serviceFee;
                });
                stat.jabrool_earning = jabroolEarningTotal;
                stat.percent_jabrool_earning = (jabroolEarningCurrentWeek - jabroolEarningPrevWeek) / (jabroolEarningPrevWeek / 100);
                if (jabroolEarningPrevWeek === 0) {
                    stat.percent_jabrool_earning = 0;
                }
                stat.couriers_earning = courierEarningTotal;
                stat.percent_couriers_earning = (courierEarningCurrentWeek - courierEarningPrevWeek) / (courierEarningPrevWeek / 100);
                if (courierEarningPrevWeek === 0) {
                    stat.percent_couriers_earning = 0;
                }
                let users = yield UserModel_1.UserModel.find({ balance: { $lt: 0 } });
                let totalDebt = 0;
                users.map((v, k, a) => {
                    totalDebt -= v.balance;
                });
                let totalDebtCurrentWeek = 0;
                let totalDebtPrevWeek = 0;
                let logDebtCurrentWeek = yield DebtLogModel_1.DebtLogModel.find({
                    created_at: { $gte: currentWeek.startAt, $lte: currentWeek.endAt }
                });
                let logDebtPrevWeek = yield DebtLogModel_1.DebtLogModel.find({
                    created_at: { $gte: prevWeek.startAt, $lte: prevWeek.endAt }
                });
                logDebtCurrentWeek.map((v, k, a) => {
                    totalDebtCurrentWeek += v.value;
                });
                logDebtPrevWeek.map((v, k, a) => {
                    totalDebtPrevWeek += v.value;
                });
                stat.users_debt = totalDebt;
                stat.percent_users_debt = (totalDebtCurrentWeek - totalDebtPrevWeek) / (totalDebtPrevWeek / 100);
                if (totalDebtPrevWeek === 0) {
                    stat.percent_users_debt = 0;
                }
                console.log(stat);
                ajaxResponse.setDate(stat);
                res.json(ajaxResponse.get());
            });
        }
        users(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let currentDate = new Date();
            let currentWeek = new DateInterval();
            currentWeek.endAt = currentDate.getTime();
            currentDate.setDate(currentDate.getDate() - 7);
            currentWeek.startAt = currentDate.getTime();
            UserActivityLogModel_1.UserActivityLogModel
                .find({ created_at: { $gte: currentWeek.startAt, $lte: currentWeek.endAt } })
                .sort({ created_at: -1 })
                .then(items => {
                ajaxResponse.setDate(items);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        now(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                try {
                    let couriers = yield UserModel_1.UserModel.count({ $or: [
                            { web_socket_ids: { $exists: true, $not: { $size: 0 } } },
                            { socket_ids: { $exists: true, $not: { $size: 0 } } }
                        ],
                        status: UserModel_1.UserStatus.Active
                    });
                    let customers = yield UserModel_1.UserModel.count({ $or: [
                            { web_socket_ids: { $exists: true, $not: { $size: 0 } } },
                            { socket_ids: { $exists: true, $not: { $size: 0 } } }
                        ],
                        status: { $ne: UserModel_1.UserStatus.Active }
                    });
                    let total = yield UserModel_1.UserModel.count({});
                    let orders = yield OrderModel_1.OrderModel.find({ status: { $nin: [
                                OrderModel_1.OrderStatuses.Finished,
                                OrderModel_1.OrderStatuses.Canceled,
                                OrderModel_1.OrderStatuses.Missed
                            ] } });
                    let je = 0;
                    let ce = 0;
                    orders.map((v, k, a) => {
                        je += v.serviceFee;
                        ce += (v.cost - v.serviceFee);
                    });
                    let avarageCourierRating = 0;
                    let avarageCustomerRating = 0;
                    let avarageCourierCount = 0;
                    let avarageCustomerCount = 0;
                    let users = yield UserModel_1.UserModel.find({ deleted: false });
                    users.map((v, k, a) => {
                        if (v.status === UserModel_1.UserStatus.Active) {
                            avarageCourierRating += v.rating;
                            avarageCourierCount++;
                        }
                        else {
                            avarageCustomerRating += v.rating;
                            avarageCustomerCount++;
                        }
                    });
                    avarageCourierRating = avarageCourierRating / avarageCourierCount;
                    if (avarageCourierCount === 0) {
                        avarageCourierRating = 0;
                    }
                    avarageCustomerRating = avarageCustomerRating / avarageCustomerCount;
                    if (avarageCustomerCount === 0) {
                        avarageCustomerRating = 0;
                    }
                    ajaxResponse.setDate({
                        couriers: couriers,
                        customers: customers,
                        total: total,
                        je: je,
                        ce: ce,
                        avarageCourierRating: avarageCourierRating,
                        avarageCustomerRating: avarageCustomerRating,
                        orders: orders.length
                    });
                    res.json(ajaxResponse.get());
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
    }
    Route.Statistics = Statistics;
})(Route || (Route = {}));
module.exports = Route;

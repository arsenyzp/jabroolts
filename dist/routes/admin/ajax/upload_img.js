"use strict";
const express = require("express");
const multer = require("multer");
const mime = require("mime-types");
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const config_1 = require("../../../components/config");
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, config_1.AppConfig.getInstanse().get("files:user_uploads"));
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + "-" + Date.now() + "." + mime.extension(file.mimetype));
    }
});
const upload = multer({ storage: storage, limits: { fileSize: 3000000 } });
const cpUpload = upload.single("image");
var Route;
(function (Route) {
    class UploadsImg {
        upload() {
            let router;
            router = express.Router();
            router.post("/", cpUpload, this.index.bind(this.index));
            return router;
        }
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let file = req.file.filename;
            ajaxResponse.setDate({
                image: file,
                image_url: config_1.AppConfig.getInstanse().get("urls:user_uploads") + "/" + file
            });
            res.json(ajaxResponse.get());
        }
    }
    Route.UploadsImg = UploadsImg;
})(Route || (Route = {}));
module.exports = Route;

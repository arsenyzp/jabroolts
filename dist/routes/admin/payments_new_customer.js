"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const escapeStringRegexp = require("escape-string-regexp");
const AjaxResponse_1 = require("../../components/AjaxResponse");
const UserModel_1 = require("../../models/UserModel");
const PaymentCustomerNewLogModel_1 = require("../../models/PaymentCustomerNewLogModel");
var Route;
(function (Route) {
    class PaymentsNewCustomer {
        index(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                let sort = {};
                if (req.query.sortKey) {
                    sort[req.query.sortKey] = req.query.reverse;
                }
                else {
                    sort = { created_at: -1 };
                }
                let filter = {};
                let search = "";
                if (req.query.q !== undefined && req.query.q !== "") {
                    search = escapeStringRegexp(req.query.q);
                    let filter2 = { $or: [
                            { "email": new RegExp(".*" + search + ".*", "i") },
                            { "phone": new RegExp(".*" + search + ".*", "i") }
                        ] };
                    let users = yield UserModel_1.UserModel.find(filter2);
                    let ids = [];
                    users.map((v, k, a) => {
                        ids.push(v._id.toString());
                    });
                    filter.user = { $in: ids };
                }
                let skip = 0;
                let limit = 10;
                if (req.params.page && req.params.perPage) {
                    req.params.page = parseInt(req.params.page) - 1;
                    skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                    limit = parseInt(req.params.perPage);
                }
                PaymentCustomerNewLogModel_1.PaymentCustomerNewLogModel
                    .count(filter)
                    .then(total_count => {
                    PaymentCustomerNewLogModel_1.PaymentCustomerNewLogModel
                        .find(filter)
                        .populate({ path: "customer", model: UserModel_1.UserModel })
                        .sort(sort)
                        .limit(limit)
                        .skip(skip)
                        .then(items => {
                        ajaxResponse.setDate({
                            items: items,
                            total_count: total_count
                        });
                        res.json(ajaxResponse.get());
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            });
        }
        get(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            PaymentCustomerNewLogModel_1.PaymentCustomerNewLogModel
                .findOne({ _id: req.params.id })
                .populate({ path: "customer", model: UserModel_1.UserModel })
                .then(bank => {
                if (bank == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                ajaxResponse.setDate(bank);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        delete(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            PaymentCustomerNewLogModel_1.PaymentCustomerNewLogModel
                .findOne({ _id: req.params.id })
                .then(bank => {
                if (bank == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                PaymentCustomerNewLogModel_1.PaymentCustomerNewLogModel
                    .remove({ _id: req.params.id })
                    .then(r => {
                    res.json(ajaxResponse.get());
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
    }
    Route.PaymentsNewCustomer = PaymentsNewCustomer;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const TermsConfig_1 = require("../../../models/configs/TermsConfig");
const class_validator_1 = require("class-validator");
var Route;
(function (Route) {
    class TermsPost {
    }
    __decorate([
        class_validator_1.Length(0, 9000, {
            message: "admin.validation.TermsRequired"
        })
    ], TermsPost.prototype, "terms_courier_en", void 0);
    __decorate([
        class_validator_1.Length(0, 9000, {
            message: "admin.validation.TermsRequired"
        })
    ], TermsPost.prototype, "terms_courier_ar", void 0);
    __decorate([
        class_validator_1.Length(0, 9000, {
            message: "admin.validation.TermsRequired"
        })
    ], TermsPost.prototype, "terms_customer_en", void 0);
    __decorate([
        class_validator_1.Length(0, 9000, {
            message: "admin.validation.TermsRequired"
        })
    ], TermsPost.prototype, "terms_customer_ar", void 0);
    __decorate([
        class_validator_1.Length(0, 9000, {
            message: "admin.validation.PrivacyRequired"
        })
    ], TermsPost.prototype, "privacy_en", void 0);
    __decorate([
        class_validator_1.Length(0, 9000, {
            message: "admin.validation.PrivacyRequired"
        })
    ], TermsPost.prototype, "privacy_ar", void 0);
    class Terms {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let model = new TermsConfig_1.TermsConfig();
            model
                .getInstanse()
                .then(config => {
                ajaxResponse.setDate(config);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        save(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let post = Object.create(TermsPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                }
                else {
                    let model = new TermsConfig_1.TermsConfig();
                    model.terms_courier_en = post.terms_courier_en;
                    model.terms_courier_ar = post.terms_courier_ar;
                    model.terms_customer_en = post.terms_customer_en;
                    model.terms_customer_ar = post.terms_customer_ar;
                    model.privacy_en = post.privacy_en;
                    model.privacy_ar = post.privacy_ar;
                    model
                        .save()
                        .then(bank => {
                        ajaxResponse.setDate(model);
                        res.json(ajaxResponse.get());
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                }
            });
        }
    }
    Route.Terms = Terms;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const class_validator_1 = require("class-validator");
const PriceConfig_1 = require("../../../models/configs/PriceConfig");
var Route;
(function (Route) {
    class PricePost {
    }
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.EPercentRequired"
        })
    ], PricePost.prototype, "e_percent", void 0);
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.JPercentRequired"
        })
    ], PricePost.prototype, "j_percent", void 0);
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.EFixCost"
        })
    ], PricePost.prototype, "e_fix_cost", void 0);
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.JFixCost"
        })
    ], PricePost.prototype, "j_fix_cost", void 0);
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.EMinCost"
        })
    ], PricePost.prototype, "e_min_cost", void 0);
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.JMinCost"
        })
    ], PricePost.prototype, "j_min_cost", void 0);
    class Price {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let model = new PriceConfig_1.PriceConfig();
            model
                .getInstanse()
                .then(config => {
                ajaxResponse.setDate(config);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        save(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let post = Object.create(PricePost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                }
                else {
                    let model = new PriceConfig_1.PriceConfig();
                    model.e_percent = post.e_percent;
                    model.j_percent = post.j_percent;
                    model.e_fix_cost = post.e_fix_cost;
                    model.j_fix_cost = post.j_fix_cost;
                    model.e_min_cost = post.e_min_cost;
                    model.j_min_cost = post.j_min_cost;
                    model
                        .save()
                        .then(bank => {
                        ajaxResponse.setDate(model);
                        res.json(ajaxResponse.get());
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                }
            });
        }
    }
    Route.Price = Price;
})(Route || (Route = {}));
module.exports = Route;

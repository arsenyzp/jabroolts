"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const PackageConfigPrice_1 = require("../../../models/configs/PackageConfigPrice");
const class_validator_1 = require("class-validator");
var Route;
(function (Route) {
    class PackagePostItem {
    }
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.JCost"
        })
    ], PackagePostItem.prototype, "j_cost", void 0);
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.ECost"
        })
    ], PackagePostItem.prototype, "e_cost", void 0);
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.FromDimension"
        })
    ], PackagePostItem.prototype, "from_dimension", void 0);
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.ToDimension"
        })
    ], PackagePostItem.prototype, "to_dimension", void 0);
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.MaxCount"
        })
    ], PackagePostItem.prototype, "max_count", void 0);
    class PackagePost {
    }
    __decorate([
        class_validator_1.ValidateNested()
    ], PackagePost.prototype, "small", void 0);
    __decorate([
        class_validator_1.ValidateNested()
    ], PackagePost.prototype, "medium", void 0);
    __decorate([
        class_validator_1.ValidateNested()
    ], PackagePost.prototype, "large", void 0);
    class Packages {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let model = new PackageConfigPrice_1.PackageConfigPrice();
            model
                .getInstanse()
                .then(config => {
                ajaxResponse.setDate(config);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        save(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let post = Object.create(PackagePost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                }
                else {
                    let model = new PackageConfigPrice_1.PackageConfigPrice();
                    model.small.e_cost = post.small.e_cost;
                    model.small.j_cost = post.small.j_cost;
                    model.small.max_count = post.small.max_count;
                    model.small.from_dimension = post.medium.from_dimension;
                    model.small.to_dimension = post.medium.to_dimension;
                    model.medium.e_cost = post.medium.e_cost;
                    model.medium.j_cost = post.medium.j_cost;
                    model.medium.max_count = post.medium.max_count;
                    model.medium.from_dimension = post.medium.from_dimension;
                    model.medium.to_dimension = post.medium.to_dimension;
                    model.large.e_cost = post.large.e_cost;
                    model.large.j_cost = post.large.j_cost;
                    model.large.max_count = post.large.max_count;
                    model.large.from_dimension = post.medium.from_dimension;
                    model.large.to_dimension = post.medium.to_dimension;
                    model
                        .save()
                        .then(bank => {
                        ajaxResponse.setDate(model);
                        res.json(ajaxResponse.get());
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                }
            });
        }
    }
    Route.Packages = Packages;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const LogsConfig_1 = require("../../../models/configs/LogsConfig");
const class_validator_1 = require("class-validator");
var Route;
(function (Route) {
    class LogsConfigPost {
    }
    __decorate([
        class_validator_1.IsBoolean({
            message: "Api invalid value"
        })
    ], LogsConfigPost.prototype, "api", void 0);
    __decorate([
        class_validator_1.IsBoolean({
            message: "Error invalid value"
        })
    ], LogsConfigPost.prototype, "error", void 0);
    __decorate([
        class_validator_1.IsBoolean({
            message: "Email invalid value"
        })
    ], LogsConfigPost.prototype, "email", void 0);
    __decorate([
        class_validator_1.IsBoolean({
            message: "Sms invalid value"
        })
    ], LogsConfigPost.prototype, "sms", void 0);
    class Logs {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let model = new LogsConfig_1.LogsConfig();
            model
                .getInstanse()
                .then(config => {
                ajaxResponse.setDate(config);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        save(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let post = Object.create(LogsConfigPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                }
                else {
                    let model = new LogsConfig_1.LogsConfig();
                    model.api = post.api;
                    model.error = post.error;
                    model.email = post.email;
                    model.sms = post.sms;
                    model
                        .save()
                        .then(bank => {
                        ajaxResponse.setDate(model);
                        res.json(ajaxResponse.get());
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                }
            });
        }
    }
    Route.Logs = Logs;
})(Route || (Route = {}));
module.exports = Route;

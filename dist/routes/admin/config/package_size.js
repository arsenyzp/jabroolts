"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const class_validator_1 = require("class-validator");
const PackageRatioConfig_1 = require("../../../models/configs/PackageRatioConfig");
var Route;
(function (Route) {
    class RadiusPost {
    }
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.SmallPackage"
        })
    ], RadiusPost.prototype, "small", void 0);
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.MediumPackage"
        })
    ], RadiusPost.prototype, "medium", void 0);
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.LagrePackage"
        })
    ], RadiusPost.prototype, "large", void 0);
    class PackageSize {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let model = new PackageRatioConfig_1.PackageRatioConfig();
            model
                .getInstanse()
                .then(config => {
                ajaxResponse.setDate(config);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        save(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let post = Object.create(RadiusPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                }
                else {
                    let model = new PackageRatioConfig_1.PackageRatioConfig();
                    model.small = post.small;
                    model.medium = post.medium;
                    model.large = post.large;
                    model
                        .save()
                        .then(bank => {
                        ajaxResponse.setDate(model);
                        res.json(ajaxResponse.get());
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                }
            });
        }
    }
    Route.PackageSize = PackageSize;
})(Route || (Route = {}));
module.exports = Route;

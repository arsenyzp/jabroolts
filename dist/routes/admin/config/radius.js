"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const RadiusConfig_1 = require("../../../models/configs/RadiusConfig");
const class_validator_1 = require("class-validator");
var Route;
(function (Route) {
    class RadiusPost {
    }
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.RadiusRequest"
        })
    ], RadiusPost.prototype, "request", void 0);
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.RadiusFeed"
        })
    ], RadiusPost.prototype, "feed", void 0);
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.RadiusDropOff"
        })
    ], RadiusPost.prototype, "drop_off", void 0);
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.CloseRadius"
        })
    ], RadiusPost.prototype, "close_radius", void 0);
    class Radius {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let model = new RadiusConfig_1.RadiusConfig();
            model
                .getInstanse()
                .then(config => {
                ajaxResponse.setDate(config);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        save(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let post = Object.create(RadiusPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                }
                else {
                    let model = new RadiusConfig_1.RadiusConfig();
                    model.drop_off = post.drop_off;
                    model.feed = post.feed;
                    model.request = post.request;
                    model.close_radius = post.close_radius;
                    model
                        .save()
                        .then(bank => {
                        ajaxResponse.setDate(model);
                        res.json(ajaxResponse.get());
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                }
            });
        }
    }
    Route.Radius = Radius;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const class_validator_1 = require("class-validator");
const DeliveryConfig_1 = require("../../../models/configs/DeliveryConfig");
var Route;
(function (Route) {
    class DeliveryPost {
    }
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.WaitingPeriod"
        })
    ], DeliveryPost.prototype, "waiting_time", void 0);
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.SpeedDefault"
        })
    ], DeliveryPost.prototype, "average_speed", void 0);
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.MissedTime"
        })
    ], DeliveryPost.prototype, "missed_time", void 0);
    class Delivery {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let model = new DeliveryConfig_1.DeliveryConfig();
            model
                .getInstanse()
                .then(config => {
                ajaxResponse.setDate(config);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        save(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let post = Object.create(DeliveryPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                }
                else {
                    let model = new DeliveryConfig_1.DeliveryConfig();
                    model.waiting_time = post.waiting_time;
                    model.average_speed = post.average_speed;
                    model.missed_time = post.missed_time;
                    model
                        .save()
                        .then(bank => {
                        ajaxResponse.setDate(model);
                        res.json(ajaxResponse.get());
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                }
            });
        }
    }
    Route.Delivery = Delivery;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const class_validator_1 = require("class-validator");
const BonusesConfig_1 = require("../../../models/configs/BonusesConfig");
const UserModel_1 = require("../../../models/UserModel");
var Route;
(function (Route) {
    class BonusesPost {
        constructor() {
            this.setForAll = false;
        }
    }
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.BonusValue"
        })
    ], BonusesPost.prototype, "inviteBonuses", void 0);
    __decorate([
        class_validator_1.IsNumber({
            message: "admin.validation.CourierDefaultLimit"
        })
    ], BonusesPost.prototype, "defaultCourierLimit", void 0);
    class Bonuses {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let model = new BonusesConfig_1.BonusesConfig();
            model
                .getInstanse()
                .then(config => {
                ajaxResponse.setDate(config);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        save(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                let post = Object.create(BonusesPost.prototype);
                try {
                    Object.assign(post, req.body, {});
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        ajaxResponse.addValidationErrors(req, errors);
                        res.status(500);
                        res.json(ajaxResponse);
                    }
                    else {
                        let model = new BonusesConfig_1.BonusesConfig();
                        model.inviteBonuses = post.inviteBonuses;
                        let diff = post.defaultCourierLimit - model.defaultCourierLimit;
                        model.defaultCourierLimit = post.defaultCourierLimit;
                        if (post.setForAll) {
                            yield UserModel_1.UserModel.update({}, {
                                $inc: { courierLimit: diff }
                            }, { multi: true });
                        }
                        yield model
                            .save();
                        ajaxResponse.setDate(model);
                        res.json(ajaxResponse.get());
                    }
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
    }
    Route.Bonuses = Bonuses;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const escapeStringRegexp = require("escape-string-regexp");
const AjaxResponse_1 = require("../../components/AjaxResponse");
const ManufactureModel_1 = require("../../models/ManufactureModel");
const class_validator_1 = require("class-validator");
const CarTypeModel_1 = require("../../models/CarTypeModel");
var Route;
(function (Route) {
    class ManufacturePost {
        constructor() {
            this._id = "";
        }
    }
    __decorate([
        class_validator_1.Length(3, 20, {
            message: "admin.validation.ManufacturerName"
        })
    ], ManufacturePost.prototype, "name", void 0);
    __decorate([
        class_validator_1.IsArray({
            message: "admin.validation.InvalidArrayTypes"
        })
    ], ManufacturePost.prototype, "types", void 0);
    class Manufacturers {
        index(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                let sort = {};
                if (req.query.sortKey) {
                    sort[req.query.sortKey] = req.query.reverse;
                }
                else {
                    sort = { name: 1 };
                }
                let filter = {};
                let search = "";
                if (req.query.q !== undefined && req.query.q !== "") {
                    search = escapeStringRegexp(req.query.q);
                    filter = { $or: [
                            { name: new RegExp(".*" + search + ".*", "i") }
                        ] };
                    sort = { name: 1 };
                }
                let skip = 0;
                let limit = 10;
                if (req.params.page && req.params.perPage) {
                    req.params.page = parseInt(req.params.page) - 1;
                    skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                    limit = parseInt(req.params.perPage);
                }
                try {
                    let total_count = yield ManufactureModel_1.ManufactureModel.count(filter);
                    let items = yield ManufactureModel_1.ManufactureModel
                        .find(filter)
                        .sort(sort)
                        .limit(limit)
                        .skip(skip);
                    ajaxResponse.setDate({
                        items: items,
                        total_count: total_count
                    });
                    res.json(ajaxResponse.get());
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
        get(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                try {
                    let model = yield ManufactureModel_1.ManufactureModel
                        .findOne({ _id: req.params.id })
                        .populate({ path: "types", model: CarTypeModel_1.CarTypeModel, sort: "name ASC" });
                    if (model == null) {
                        ajaxResponse.addErrorMessage("Item not found");
                        res.status(500);
                        return res.json(ajaxResponse.get());
                    }
                    ajaxResponse.setDate(model);
                    res.json(ajaxResponse.get());
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
        getTypes(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                try {
                    let manufacturer = yield ManufactureModel_1.ManufactureModel.findOne({ _id: req.params.id });
                    let types = [];
                    if (manufacturer !== null) {
                        types = yield CarTypeModel_1.CarTypeModel
                            .find({ _id: manufacturer.types }).sort({ name: 1 });
                    }
                    ajaxResponse.setDate({
                        types: types
                    });
                    res.json(ajaxResponse.get());
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
        delete(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                try {
                    let model = yield ManufactureModel_1.ManufactureModel.findOne({ _id: req.params.id });
                    if (model == null) {
                        ajaxResponse.addErrorMessage("Item not found");
                        res.status(500);
                        return res.json(ajaxResponse.get());
                    }
                    yield CarTypeModel_1.CarTypeModel.remove({ _id: model.types });
                    yield ManufactureModel_1.ManufactureModel
                        .remove({ _id: req.params.id });
                    res.json(ajaxResponse.get());
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
        save(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                let post = Object.create(ManufacturePost.prototype);
                Object.assign(post, req.body, {});
                try {
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        ajaxResponse.addValidationErrors(req, errors);
                        res.status(500);
                        res.json(ajaxResponse);
                    }
                    else {
                        let item = yield ManufactureModel_1.ManufactureModel
                            .findOne({ _id: post._id });
                        if (item == null) {
                            item = new ManufactureModel_1.ManufactureModel();
                        }
                        item.name = post.name;
                        item.types = [];
                        let tt = [];
                        let newTypes = [];
                        for (let i = 0; i < post.types.length; i++) {
                            let v = post.types[i];
                            if (!v._id) {
                                let ct = new CarTypeModel_1.CarTypeModel();
                                ct.size = v.size;
                                ct.name = v.name;
                                yield ct.save();
                                tt.push(ct._id);
                                newTypes.push(ct);
                            }
                            else {
                                let ct = yield CarTypeModel_1.CarTypeModel.findOneAndUpdate({ _id: v._id }, {
                                    size: v.size,
                                    name: v.name
                                }, { new: true });
                                tt.push(ct._id);
                                newTypes.push(ct);
                            }
                        }
                        let types_to_remove = item.types.filter(x => tt.indexOf(x._id.toString()) === -1);
                        yield CarTypeModel_1.CarTypeModel.remove({ _id: types_to_remove });
                        item.types = newTypes;
                        yield item.save();
                        item = yield ManufactureModel_1.ManufactureModel
                            .findOne({ _id: item._id })
                            .populate({ path: "types", model: CarTypeModel_1.CarTypeModel });
                        ajaxResponse.setDate(item);
                        res.json(ajaxResponse.get());
                    }
                }
                catch (err) {
                    console.log(err);
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
    }
    Route.Manufacturers = Manufacturers;
})(Route || (Route = {}));
module.exports = Route;

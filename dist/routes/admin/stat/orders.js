"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const async = require("async");
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const UserModel_1 = require("../../../models/UserModel");
const OrderModel_1 = require("../../../models/OrderModel");
var Route;
(function (Route) {
    class Orders {
        active(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                let filter = { status: { $nin: [OrderModel_1.OrderStatuses.Missed, OrderModel_1.OrderStatuses.Canceled, OrderModel_1.OrderStatuses.Finished] } };
                let skip = 0;
                let limit = 10;
                if (req.params.page && req.params.perPage) {
                    req.params.page = parseInt(req.params.page) - 1;
                    skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                    limit = parseInt(req.params.perPage);
                }
                try {
                    let total_count = yield OrderModel_1.OrderModel
                        .count(filter);
                    let items = yield OrderModel_1.OrderModel
                        .find(filter)
                        .populate({ path: "courier", model: UserModel_1.UserModel })
                        .populate({ path: "owner", model: UserModel_1.UserModel })
                        .sort({ created_at: -1 })
                        .limit(limit)
                        .skip(skip);
                    let data = [];
                    async.eachOfSeries(items, (item, key, cb) => {
                        let model = {};
                        model._id = item._id;
                        model.order = item._id;
                        model.courier = !!item.courier ? item.courier.jabroolid : "--";
                        model.owner = !!item.owner ? item.owner.jabroolid : "--";
                        model.pickup_location = item.owner_address;
                        model.drop_off_location = item.recipient_address;
                        model.route = item.route;
                        model.number_of_parcel = item.small_package_count + item.medium_package_count + item.large_package_count;
                        model.accept_time = item.accept_at;
                        model.amount = item.cost;
                        model.pay_type = item.pay_type;
                        model.status = item.status;
                        model.created_at = item.created_at;
                        model.orderId = item.orderId;
                        data.push(model);
                        cb();
                    }, err => {
                        if (err) {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        }
                        else {
                            ajaxResponse.setDate({
                                items: data,
                                total_count: total_count
                            });
                            res.json(ajaxResponse.get());
                        }
                    });
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
        missed(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                let filter = { status: { $in: [OrderModel_1.OrderStatuses.Missed] } };
                let skip = 0;
                let limit = 10;
                if (req.params.page && req.params.perPage) {
                    req.params.page = parseInt(req.params.page) - 1;
                    skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                    limit = parseInt(req.params.perPage);
                }
                try {
                    let total_count = yield OrderModel_1.OrderModel
                        .count(filter);
                    let items = yield OrderModel_1.OrderModel
                        .find(filter)
                        .populate({ path: "courier", model: UserModel_1.UserModel })
                        .populate({ path: "owner", model: UserModel_1.UserModel })
                        .sort({ created_at: -1 })
                        .limit(limit)
                        .skip(skip);
                    let data = [];
                    async.eachOfSeries(items, (item, key, cb) => {
                        let model = {};
                        model._id = item._id;
                        model.order = item._id;
                        model.courier = !!item.courier ? item.courier.jabroolid : "--";
                        model.owner = !!item.owner ? item.owner.jabroolid : "--";
                        model.pickup_location = item.owner_address;
                        model.drop_off_location = item.recipient_address;
                        model.route = item.route;
                        model.number_of_parcel = item.small_package_count + item.medium_package_count + item.large_package_count;
                        model.accept_time = item.accept_at;
                        model.amount = item.cost;
                        model.pay_type = item.pay_type;
                        model.status = item.status;
                        model.created_at = item.created_at;
                        model.orderId = item.orderId;
                        data.push(model);
                        cb();
                    }, err => {
                        if (err) {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        }
                        else {
                            ajaxResponse.setDate({
                                items: data,
                                total_count: total_count
                            });
                            res.json(ajaxResponse.get());
                        }
                    });
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
    }
    Route.Orders = Orders;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
const escapeStringRegexp = require("escape-string-regexp");
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const UserModel_1 = require("../../../models/UserModel");
const Log_1 = require("../../../components/Log");
const log = Log_1.Log.getInstanse()(module);
var Route;
(function (Route) {
    class Users {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let sort = {};
            if (req.query.sortKey) {
                sort[req.query.sortKey] = req.query.reverse;
            }
            else {
                sort = { created_at: -1 };
            }
            let filter = {
                $and: [
                    { $or: [
                            { web_socket_ids: { $exists: true, $not: { $size: 0 } } },
                            { socket_ids: { $exists: true, $not: { $size: 0 } } }
                        ] }
                ]
            };
            let search = "";
            if (req.query.q !== undefined && req.query.q !== "") {
                search = escapeStringRegexp(req.query.q);
                filter.$and.push({ $or: [
                        { first_name: new RegExp(".*" + search + ".*", "i") },
                        { last_name: new RegExp(".*" + search + ".*", "i") },
                        { phone: new RegExp(".*" + search + ".*", "i") },
                    ] });
                sort = { created_at: -1 };
            }
            filter.deleted = { $in: [false, null] };
            let skip = 0;
            let limit = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }
            UserModel_1.UserModel
                .count(filter)
                .then(total_count => {
                UserModel_1.UserModel
                    .find(filter)
                    .sort(sort)
                    .limit(limit)
                    .skip(skip)
                    .then(users => {
                    let items = [];
                    for (let i = 0; i < users.length; i++) {
                        let item = users[i].getApiFields();
                        item.bank = users[i].getApiFields();
                        item.vehicle = users[i].getVehicle();
                        item.bank = users[i].getBank();
                        item.license = users[i].getLicense();
                        item.socket_ids = users[i].socket_ids;
                        item._id = users[i]._id;
                        items.push(item);
                    }
                    ajaxResponse.setDate({
                        items: items,
                        total_count: total_count
                    });
                    res.json(ajaxResponse.get());
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        get(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            UserModel_1.UserModel
                .findOne({ _id: req.params.id })
                .then(u => {
                if (u == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                ajaxResponse.setDate(u);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
    }
    Route.Users = Users;
})(Route || (Route = {}));
module.exports = Route;

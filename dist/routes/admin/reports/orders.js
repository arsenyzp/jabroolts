"use strict";
const async = require("async");
const json2csv = require("json2csv");
const moment = require("moment");
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const UserModel_1 = require("../../../models/UserModel");
const OrderModel_1 = require("../../../models/OrderModel");
var Route;
(function (Route) {
    class Orders {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let filter = { courier: { $nin: [null] }, status: OrderModel_1.OrderStatuses.Finished };
            let skip = 0;
            let limit = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }
            OrderModel_1.OrderModel
                .count(filter)
                .then(total_count => {
                OrderModel_1.OrderModel
                    .find(filter)
                    .populate({ path: "courier", model: UserModel_1.UserModel })
                    .sort({ created_at: -1 })
                    .limit(limit)
                    .skip(skip)
                    .then(items => {
                    let data = [];
                    async.eachOfSeries(items, (item, key, cb) => {
                        let model = {};
                        model._id = item._id;
                        model.tripID = item.orderId;
                        model.date = item.created_at;
                        model.city = item.owner_map_url;
                        model.deliveryType = item.type;
                        model.courierJID = item.courier.jabroolid;
                        model.tripDuration = (item.end_at - item.start_at) / 60 * 1000;
                        model.tripDistance = item.route;
                        model.tripWaitTime = (item.start_at - item.created_at) / 60 * 1000;
                        model.tripPrice = item.cost;
                        model.tripPrice = item.cost - item.serviceFee;
                        model.paymentMethod = item.pay_type;
                        model.jabroolEarnings = item.serviceFee;
                        model.roundDown = item.cost;
                        model.orderId = item.orderId;
                        data.push(model);
                        cb();
                    }, err => {
                        if (err) {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        }
                        else {
                            ajaxResponse.setDate({
                                items: data,
                                total_count: total_count
                            });
                            res.json(ajaxResponse.get());
                        }
                    });
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        export(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let fields = [
                "tripID",
                "date",
                "city",
                "deliveryType",
                "courierJID",
                "tripDuration",
                "tripDistance",
                "tripWaitTime",
                "tripPrice",
                "courierEarnings",
                "paymentMethod",
                "jabroolEarnings",
                "roundDown"
            ];
            let fieldNames = [
                "Trip ID",
                "Trip date and time",
                "City",
                "Delivery type",
                "Courior ID",
                "Trip duration",
                "Distance travelled (km)",
                "Courior wait time for customer at pickup (mins)",
                "Trip price (SAR)",
                "Courior earnings (SAR)",
                "Payment method",
                "Jabrool ernings",
                "round down"
            ];
            let startTime = new Date(parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000);
            let endTime = new Date(parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000);
            startTime.setUTCHours(0, 0, 0, 0);
            endTime.setUTCHours(0, 0, 0, 0);
            if (startTime.getTime() === endTime.getTime()) {
                endTime.setUTCHours(23, 59, 59, 0);
            }
            let filter = {
                courier: { $nin: [null] },
                created_at: { $gte: startTime.getTime(), $lte: endTime.getTime() },
                status: OrderModel_1.OrderStatuses.Finished
            };
            OrderModel_1.OrderModel
                .find(filter)
                .sort({ created_at: -1 })
                .then(items => {
                let data = [];
                async.eachOfSeries(items, (item, key, cb) => {
                    let model = {};
                    model.tripID = item.orderId;
                    model.date = item.created_at;
                    model.city = item.owner_map_url;
                    model.deliveryType = item.type;
                    model.courierJID = item.courier.jabroolid;
                    model.tripDuration = (item.end_at - item.start_at) / 60 * 1000;
                    model.tripDistance = item.route;
                    model.tripWaitTime = (item.start_at - item.created_at) / 60 * 1000;
                    model.tripPrice = item.cost;
                    model.tripPrice = item.cost - item.serviceFee;
                    model.paymentMethod = item.pay_type;
                    model.jabroolEarnings = item.serviceFee;
                    model.roundDown = item.cost;
                    data.push(model);
                    cb();
                }, err => {
                    if (err) {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    }
                    else {
                        let result = json2csv({ data: data, fields: fields, fieldNames: fieldNames, del: ";" });
                        console.log(result);
                        res.setHeader("Content-disposition", "attachment; filename=data.csv");
                        res.header("Content-Type", "text/csv");
                        res.send(result);
                    }
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
    }
    Route.Orders = Orders;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
const async = require("async");
const json2csv = require("json2csv");
const moment = require("moment");
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const OrderModel_1 = require("../../../models/OrderModel");
const OpenAppLogModel_1 = require("../../../models/OpenAppLogModel");
const DeliveryConfig_1 = require("../../../models/configs/DeliveryConfig");
var Route;
(function (Route) {
    class HoursReports {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let skip = 0;
            let limit = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }
            let dates = [];
            let currentDate = new Date();
            currentDate.setUTCMinutes(0);
            currentDate.setUTCHours(currentDate.getUTCHours() - skip);
            for (let i = 0; i < limit; i++) {
                currentDate.setUTCHours(currentDate.getUTCHours() - 1);
                dates.push(currentDate.getTime());
            }
            const resultData = [];
            async.eachOfSeries(dates, (item, key, cb) => {
                let it = {};
                async.parallel([
                        cb => {
                        OpenAppLogModel_1.OpenAppLogModel
                            .count({ created_at: { $gte: item, $lte: item + 60 * 60 * 1000 } })
                            .then(c => {
                            it.opened = c;
                            cb();
                        })
                            .catch(err => {
                            cb(err);
                        });
                    },
                        cb => {
                        OrderModel_1.OrderModel
                            .count({ end_at: { $gte: item, $lte: item + 60 * 60 * 1000 }, status: OrderModel_1.OrderStatuses.Finished })
                            .then(c => {
                            it.completed = c;
                            cb();
                        })
                            .catch(err => {
                            cb(err);
                        });
                    },
                        cb => {
                        OrderModel_1.OrderModel
                            .count({
                            created_at: { $gte: item, $lte: item + 60 * 60 * 1000 },
                            status: OrderModel_1.OrderStatuses.New,
                            "notified_couriers.0": { "$exists": false }
                        })
                            .then(c => {
                            it.unoccupied = c;
                            cb();
                        })
                            .catch(err => {
                            cb(err);
                        });
                    },
                        cb => {
                        let deliveryConfig = new DeliveryConfig_1.DeliveryConfig();
                        deliveryConfig
                            .getInstanse()
                            .then(cf => {
                            let missedTime = cf.missed_time * 60 * 1000;
                            OrderModel_1.OrderModel
                                .count({
                                created_at: { $gte: item + missedTime, $lte: item + 60 * 60 * 1000 + missedTime },
                                status: OrderModel_1.OrderStatuses.New,
                                "notified_couriers.0": { "$exists": true }
                            })
                                .then(c => {
                                it.missed = c;
                                cb();
                            })
                                .catch(err => {
                                cb(err);
                            });
                        })
                            .catch(err => {
                            cb(err);
                        });
                    }
                ], err => {
                    if (err) {
                        cb(err);
                    }
                    else {
                        it.date = item;
                        resultData.push(it);
                        cb();
                    }
                });
            }, err => {
                if (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
                else {
                    ajaxResponse.setDate({
                        items: resultData,
                        total_count: dates.length * 100
                    });
                    res.json(ajaxResponse.get());
                }
            });
        }
        export(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let fields = [
                "date",
                "hour",
                "opened",
                "completed",
                "unoccupied",
                "missed"
            ];
            let fieldNames = [
                "Date",
                "Hour",
                "Number of times Jabrool app was opened",
                "Number of completed trips",
                "Number of unoccupied Couriors",
                "Number of missed bookings (no car was available)"
            ];
            let dates = [];
            let startTime = new Date(parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000);
            let endTime = new Date(parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000);
            startTime.setUTCHours(0, 0, 0, 0);
            endTime.setUTCHours(0, 0, 0, 0);
            if (startTime.getTime() === endTime.getTime()) {
                endTime.setUTCHours(23, 59, 59, 0);
            }
            while (startTime.getTime() <= endTime.getTime() && dates.length < 1000) {
                startTime.setUTCHours(startTime.getUTCHours() + 1);
                dates.push(startTime.getTime());
                console.log(startTime + " - " + endTime);
            }
            const resultData = [];
            async.eachOfSeries(dates, (item, key, cb) => {
                let it = {};
                async.parallel([
                        cb => {
                        OpenAppLogModel_1.OpenAppLogModel
                            .count({ created_at: { $gte: item, $lte: item + 60 * 60 * 1000 } })
                            .then(c => {
                            it.opened = c;
                            cb();
                        })
                            .catch(err => {
                            cb(err);
                        });
                    },
                        cb => {
                        OrderModel_1.OrderModel
                            .count({ end_at: { $gte: item, $lte: item + 60 * 60 * 1000 }, status: OrderModel_1.OrderStatuses.Finished })
                            .then(c => {
                            it.completed = c;
                            cb();
                        })
                            .catch(err => {
                            cb(err);
                        });
                    },
                        cb => {
                        OrderModel_1.OrderModel
                            .count({
                            created_at: { $gte: item, $lte: item + 60 * 60 * 1000 },
                            status: OrderModel_1.OrderStatuses.New,
                            "notified_couriers.0": { "$exists": false }
                        })
                            .then(c => {
                            it.unoccupied = c;
                            cb();
                        })
                            .catch(err => {
                            cb(err);
                        });
                    },
                        cb => {
                        let deliveryConfig = new DeliveryConfig_1.DeliveryConfig();
                        deliveryConfig
                            .getInstanse()
                            .then(cf => {
                            let missedTime = cf.missed_time * 60 * 1000;
                            OrderModel_1.OrderModel
                                .count({
                                created_at: { $gte: item + missedTime, $lte: item + 60 * 60 * 1000 + missedTime },
                                status: OrderModel_1.OrderStatuses.New,
                                "notified_couriers.0": { "$exists": true }
                            })
                                .then(c => {
                                it.missed = c;
                                cb();
                            })
                                .catch(err => {
                                cb(err);
                            });
                        })
                            .catch(err => {
                            cb(err);
                        });
                    }
                ], err => {
                    if (err) {
                        cb(err);
                    }
                    else {
                        it.date = moment(item / 1000, "X").format("DD-MM-YYYY");
                        it.hour = moment(item / 1000, "X").format("HH");
                        resultData.push(it);
                        cb();
                    }
                });
            }, err => {
                if (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
                else {
                    let result = json2csv({ data: resultData, fields: fields, fieldNames: fieldNames, del: ";" });
                    console.log(result);
                    res.setHeader("Content-disposition", "attachment; filename=data.csv");
                    res.header("Content-Type", "text/csv");
                    res.send(result);
                }
            });
        }
    }
    Route.HoursReports = HoursReports;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
const async = require("async");
const json2csv = require("json2csv");
const moment = require("moment");
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const OrderModel_1 = require("../../../models/OrderModel");
var Route;
(function (Route) {
    class CustomerClusters {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let startTime = new Date(parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000);
            let endTime = new Date(parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000);
            startTime.setUTCHours(0, 0, 0, 0);
            endTime.setUTCHours(23, 59, 59, 0);
            let filter = {
                "notified_couriers.0": { "$exists": false },
                status: { $in: [OrderModel_1.OrderStatuses.New, OrderModel_1.OrderStatuses.Missed] },
                created_at: { $gte: startTime.getTime(), $lte: endTime.getTime() }
            };
            let skip = 0;
            let limit = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }
            OrderModel_1.OrderModel
                .count(filter)
                .then(total_count => {
                OrderModel_1.OrderModel
                    .find(filter)
                    .sort({ created_at: -1 })
                    .limit(limit)
                    .skip(skip)
                    .then(items => {
                    let data = [];
                    async.eachOfSeries(items, (item, key, cb) => {
                        let model = {};
                        model.tripID = item._id;
                        model.date = item.created_at;
                        model.map = item.owner_map_url;
                        model.address = item.owner_address;
                        model.count = 1;
                        model.lat = item.owner_location.coordinates[0];
                        model.lon = item.owner_location.coordinates[1];
                        data.push(model);
                        cb();
                    }, err => {
                        if (err) {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        }
                        else {
                            ajaxResponse.setDate({
                                items: data,
                                total_count: total_count
                            });
                            res.json(ajaxResponse.get());
                        }
                    });
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        export(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let fields = [
                "address",
                "map",
                "count",
                "date"
            ];
            let fieldNames = [
                "Locations where no couriors avalabile for requests",
                "Map",
                "Number of times no Couriors around at this location",
                "Timestamp"
            ];
            let startTime = new Date(parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000);
            let endTime = new Date(parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000);
            startTime.setUTCHours(0, 0, 0, 0);
            endTime.setUTCHours(0, 0, 0, 0);
            if (startTime.getTime() === endTime.getTime()) {
                endTime.setUTCHours(23, 59, 59, 0);
            }
            let filter = {
                "notified_couriers.0": { "$exists": false },
                status: { $in: [OrderModel_1.OrderStatuses.New, OrderModel_1.OrderStatuses.Missed] },
                created_at: { $gte: startTime.getTime(), $lte: endTime.getTime() }
            };
            OrderModel_1.OrderModel
                .find(filter)
                .sort({ created_at: -1 })
                .then(items => {
                let data = [];
                async.eachOfSeries(items, (item, key, cb) => {
                    let model = {};
                    model.address = item.owner_address;
                    model.map = item.owner_map_url;
                    model.count = 1;
                    model.date = item.created_at;
                    data.push(model);
                    cb();
                }, err => {
                    if (err) {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    }
                    else {
                        let result = json2csv({ data: data, fields: fields, fieldNames: fieldNames, del: ";" });
                        console.log(result);
                        res.setHeader("Content-disposition", "attachment; filename=data.csv");
                        res.header("Content-Type", "text/csv");
                        res.send(result);
                    }
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
    }
    Route.CustomerClusters = CustomerClusters;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
const async = require("async");
const json2csv = require("json2csv");
const moment = require("moment");
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const UserModel_1 = require("../../../models/UserModel");
const OrderModel_1 = require("../../../models/OrderModel");
const OpenAppLogModel_1 = require("../../../models/OpenAppLogModel");
const DeliveryConfig_1 = require("../../../models/configs/DeliveryConfig");
var Route;
(function (Route) {
    class Customers {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let sort = {};
            if (req.query.sortKey) {
                sort[req.query.sortKey] = req.query.reverse;
            }
            else {
                sort = { created_at: -1 };
            }
            let filter = {};
            let skip = 0;
            let limit = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }
            UserModel_1.UserModel
                .count(filter)
                .then(total_count => {
                UserModel_1.UserModel
                    .find(filter)
                    .sort(sort)
                    .limit(limit)
                    .skip(skip)
                    .then(items => {
                    let data = [];
                    async.eachOfSeries(items, (item, key, cb) => {
                        let model = {};
                        model._id = item._id;
                        model.jabroolid = item.jabroolid;
                        model.average = item.rating;
                        async.parallel([
                                cb => {
                                OrderModel_1.OrderModel
                                    .count({ owner: item._id, status: OrderModel_1.OrderStatuses.Finished })
                                    .then(completed => {
                                    model.completed = completed;
                                    cb();
                                })
                                    .catch(err => {
                                    cb(err);
                                });
                            },
                                cb => {
                                OrderModel_1.OrderModel
                                    .count({ owner: item._id, status: OrderModel_1.OrderStatuses.Canceled })
                                    .then(canceled => {
                                    model.canceled = canceled;
                                    cb();
                                })
                                    .catch(err => {
                                    cb(err);
                                });
                            },
                                cb => {
                                OpenAppLogModel_1.OpenAppLogModel
                                    .count({ user: item._id })
                                    .then(c => {
                                    model.opened = c;
                                    cb();
                                })
                                    .catch(err => {
                                    cb(err);
                                });
                            },
                                cb => {
                                OrderModel_1.OrderModel
                                    .count({ owner: item._id, status: OrderModel_1.OrderStatuses.Missed })
                                    .then(all => {
                                    model.missed = all;
                                    cb();
                                })
                                    .catch(err => {
                                    cb(err);
                                });
                            },
                                cb => {
                                OrderModel_1.OrderModel
                                    .find({ owner: item._id, status: OrderModel_1.OrderStatuses.Finished })
                                    .then(all => {
                                    model.paid = 0;
                                    for (let i = 0; i < all.length; i++) {
                                        model.paid += all[i].cost;
                                    }
                                    cb();
                                })
                                    .catch(err => {
                                    cb(err);
                                });
                            }
                        ], err => {
                            if (err) {
                                cb(err);
                            }
                            else {
                                data.push(model);
                                cb();
                            }
                        });
                    }, err => {
                        if (err) {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        }
                        else {
                            ajaxResponse.setDate({
                                items: data,
                                total_count: total_count
                            });
                            res.json(ajaxResponse.get());
                        }
                    });
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        export(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let fields = [
                "jabroolid",
                "completed",
                "canceled",
                "average",
                "opened",
                "missed",
                "paid"
            ];
            let fieldNames = [
                "Customer ID",
                "Number of Trips completed",
                "Number of trips calcelled",
                "Average rating",
                "Number of times open app",
                "Number of times no Couriors around",
                "Total amount paid"
            ];
            let startTime = new Date(parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000);
            let endTime = new Date(parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000);
            startTime.setUTCHours(0, 0, 0, 0);
            endTime.setUTCHours(0, 0, 0, 0);
            if (startTime.getTime() === endTime.getTime()) {
                endTime.setUTCHours(23, 59, 59, 0);
            }
            let filter = {
                status: UserModel_1.UserStatus.Active
            };
            UserModel_1.UserModel
                .find(filter)
                .sort({ created_at: -1 })
                .then(items => {
                let data = [];
                async.eachOfSeries(items, (item, key, cb) => {
                    let model = {};
                    model.jabroolid = item.jabroolid;
                    model.average = item.rating;
                    async.parallel([
                            cb => {
                            OrderModel_1.OrderModel
                                .count({
                                created_at: {
                                    $gte: startTime.getTime(),
                                    $lte: endTime.getTime()
                                },
                                owner: item._id,
                                status: OrderModel_1.OrderStatuses.Finished
                            })
                                .then(completed => {
                                model.completed = completed;
                                cb();
                            })
                                .catch(err => {
                                cb(err);
                            });
                        },
                            cb => {
                            OrderModel_1.OrderModel
                                .count({
                                created_at: {
                                    $gte: parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000,
                                    $lte: parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000
                                },
                                owner: item._id,
                                status: OrderModel_1.OrderStatuses.Canceled
                            })
                                .then(canceled => {
                                model.canceled = canceled;
                                cb();
                            })
                                .catch(err => {
                                cb(err);
                            });
                        },
                            cb => {
                            OpenAppLogModel_1.OpenAppLogModel
                                .count({
                                created_at: {
                                    $gte: parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000,
                                    $lte: parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000
                                },
                                user: item._id
                            })
                                .then(c => {
                                model.opened = c;
                                cb();
                            })
                                .catch(err => {
                                cb(err);
                            });
                        },
                            cb => {
                            let deliveryConfig = new DeliveryConfig_1.DeliveryConfig();
                            deliveryConfig
                                .getInstanse()
                                .then(cf => {
                                let missedTime = cf.missed_time * 60 * 1000;
                                OrderModel_1.OrderModel
                                    .find({
                                    created_at: {
                                        $gte: parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000,
                                        $lte: parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000
                                    },
                                    owner: item._id
                                })
                                    .then(all => {
                                    model.missed = 0;
                                    for (let i = 0; i < all.length; i++) {
                                        if (all[i].start_at > 0 && all[i].start_at - all[i].created_at > missedTime) {
                                            model.missed++;
                                        }
                                        if (all[i].notified_couriers.length === 0) {
                                            model.missed++;
                                        }
                                    }
                                    cb();
                                })
                                    .catch(err => {
                                    cb(err);
                                });
                            })
                                .catch(err => {
                                cb(err);
                            });
                        },
                            cb => {
                            OrderModel_1.OrderModel
                                .find({
                                created_at: {
                                    $gte: parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000,
                                    $lte: parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000
                                },
                                owner: item._id,
                                status: OrderModel_1.OrderStatuses.Finished
                            })
                                .then(all => {
                                model.paid = 0;
                                for (let i = 0; i < all.length; i++) {
                                    model.paid += all[i].cost;
                                }
                                cb();
                            })
                                .catch(err => {
                                cb(err);
                            });
                        }
                    ], err => {
                        if (err) {
                            cb(err);
                        }
                        else {
                            cb();
                        }
                    });
                }, err => {
                    if (err) {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    }
                    else {
                        let result = json2csv({ data: data, fields: fields, fieldNames: fieldNames, del: ";" });
                        console.log(result);
                        res.setHeader("Content-disposition", "attachment; filename=data.csv");
                        res.header("Content-Type", "text/csv");
                        res.send(result);
                    }
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
    }
    Route.Customers = Customers;
})(Route || (Route = {}));
module.exports = Route;

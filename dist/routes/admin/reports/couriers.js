"use strict";
const escapeStringRegexp = require("escape-string-regexp");
const async = require("async");
const json2csv = require("json2csv");
const moment = require("moment");
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const UserModel_1 = require("../../../models/UserModel");
const OrderModel_1 = require("../../../models/OrderModel");
const OnlineLogModel_1 = require("../../../models/OnlineLogModel");
var Route;
(function (Route) {
    class Couriers {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let filter = { status: UserModel_1.UserStatus.Active };
            let skip = 0;
            let limit = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }
            let search = "";
            if (req.query.q !== undefined && req.query.q !== "") {
                search = escapeStringRegexp(req.query.q);
                filter = { $or: [
                        { jabroolid: new RegExp(".*" + search + ".*", "i") },
                    ] };
            }
            filter.deleted = { $in: [false, null] };
            UserModel_1.UserModel
                .count(filter)
                .then(total_count => {
                UserModel_1.UserModel
                    .find(filter)
                    .sort({ created_at: -1 })
                    .limit(limit)
                    .skip(skip)
                    .then(items => {
                    let data = [];
                    async.eachOfSeries(items, (item, key, cb) => {
                        let model = {};
                        model._id = item._id;
                        model.jabroolid = item.jabroolid;
                        model.account_type = "individual";
                        model.average = item.rating;
                        async.parallel([
                                cb => {
                                OrderModel_1.OrderModel
                                    .count({ courier: item._id, status: OrderModel_1.OrderStatuses.Finished })
                                    .then(completed => {
                                    model.completed = completed;
                                    cb();
                                })
                                    .catch(err => {
                                    cb(err);
                                });
                            },
                                cb => {
                                OrderModel_1.OrderModel
                                    .count({ courier: item._id, status: OrderModel_1.OrderStatuses.Canceled })
                                    .then(canceled => {
                                    model.canceled = canceled;
                                    cb();
                                })
                                    .catch(err => {
                                    cb(err);
                                });
                            },
                                cb => {
                                OnlineLogModel_1.OnlineLogModel
                                    .find({ user: item._id })
                                    .then(logsOnline => {
                                    let online = 0;
                                    for (let i = 0; i < logsOnline.length; i++) {
                                        online += logsOnline[i].duration;
                                    }
                                    model.online = (online / (1000 * 60 * 60)).toFixed(2);
                                    data.push(model);
                                    cb();
                                })
                                    .catch(err => {
                                    cb(err);
                                });
                            },
                                cb => {
                                OrderModel_1.OrderModel
                                    .count({
                                    $or: [
                                        { courier: { $ne: item._id }, notified_couriers: item._id },
                                        { courier: { $ne: item._id }, declined_couriers: item._id },
                                        { courier: { $ne: item._id }, canceled_couriers: item._id }
                                    ]
                                })
                                    .then(all => {
                                    OrderModel_1.OrderModel
                                        .count({ courier: item._id })
                                        .then(real => {
                                        if (real === 0) {
                                            model.acceptance = 0;
                                        }
                                        else {
                                            model.acceptance = (real / (all / 100)).toFixed(2);
                                        }
                                        cb();
                                    })
                                        .catch(err => {
                                        cb(err);
                                    });
                                })
                                    .catch(err => {
                                    cb(err);
                                });
                            }
                        ], err => {
                            if (err) {
                                cb(err);
                            }
                            else {
                                cb();
                            }
                        });
                    }, err => {
                        if (err) {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        }
                        else {
                            ajaxResponse.setDate({
                                items: data,
                                total_count: total_count
                            });
                            res.json(ajaxResponse.get());
                        }
                    });
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        export(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let fields = [
                "jabroolid",
                "account_type",
                "completed",
                "acceptance",
                "average",
                "online",
                "canceled"
            ];
            let fieldNames = [
                "Courior ID",
                "Account",
                "Number of Trips completed",
                "Acceptance rate",
                "Average rating",
                "Hours online",
                "Number of trips calcelled"
            ];
            let startTime = new Date(parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000);
            let endTime = new Date(parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000);
            startTime.setUTCHours(0, 0, 0, 0);
            endTime.setUTCHours(0, 0, 0, 0);
            if (startTime.getTime() === endTime.getTime()) {
                endTime.setUTCHours(23, 59, 59, 0);
            }
            let filter = {
                status: UserModel_1.UserStatus.Active
            };
            UserModel_1.UserModel
                .find(filter)
                .sort({ created_at: -1 })
                .then(items => {
                let data = [];
                async.eachOfSeries(items, (item, key, cb) => {
                    let model = {};
                    model.jabroolid = item.jabroolid;
                    model.account_type = "individual";
                    model.average = item.rating;
                    async.parallel([
                            cb => {
                            OrderModel_1.OrderModel
                                .count({
                                created_at: {
                                    $gte: startTime.getTime(),
                                    $lte: endTime.getTime()
                                },
                                courier: item._id, status: OrderModel_1.OrderStatuses.Finished
                            })
                                .then(completed => {
                                model.completed = completed;
                                cb();
                            })
                                .catch(err => {
                                cb(err);
                            });
                        },
                            cb => {
                            OrderModel_1.OrderModel
                                .count({
                                created_at: {
                                    $gte: parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000,
                                    $lte: parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000
                                },
                                courier: item._id, status: OrderModel_1.OrderStatuses.Canceled
                            })
                                .then(canceled => {
                                model.canceled = canceled;
                                cb();
                            })
                                .catch(err => {
                                cb(err);
                            });
                        },
                            cb => {
                            OnlineLogModel_1.OnlineLogModel
                                .find({
                                created_at: {
                                    $gte: parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000,
                                    $lte: parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000
                                },
                                user: item._id
                            })
                                .then(logsOnline => {
                                let online = 0;
                                for (let i = 0; i < logsOnline.length; i++) {
                                    online += logsOnline[i].duration;
                                }
                                model.online = (online / (1000 * 60 * 60)).toFixed(2);
                                data.push(model);
                                cb();
                            })
                                .catch(err => {
                                cb(err);
                            });
                        },
                            cb => {
                            OrderModel_1.OrderModel
                                .count({
                                created_at: {
                                    $gte: parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000,
                                    $lte: parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000
                                },
                                courier: { $ne: item._id },
                                notified_couriers: item._id
                            })
                                .then(all => {
                                OrderModel_1.OrderModel
                                    .count({
                                    created_at: {
                                        $gte: parseInt(moment(req.query.exportDateFrom, "DD-MM-YYYY").format("X")) * 1000,
                                        $lte: parseInt(moment(req.query.exportDateTo, "DD-MM-YYYY").format("X")) * 1000
                                    },
                                    courier: item._id
                                })
                                    .then(real => {
                                    if (real === 0) {
                                        model.acceptance = 0;
                                    }
                                    else {
                                        model.acceptance = (real / (all / 100)).toFixed(2);
                                    }
                                    cb();
                                })
                                    .catch(err => {
                                    cb(err);
                                });
                            })
                                .catch(err => {
                                cb(err);
                            });
                        }
                    ], err => {
                        if (err) {
                            cb(err);
                        }
                        else {
                            cb();
                        }
                    });
                }, err => {
                    if (err) {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    }
                    else {
                        let result = json2csv({ data: data, fields: fields, fieldNames: fieldNames, del: ";" });
                        console.log(result);
                        res.setHeader("Content-disposition", "attachment; filename=data.csv");
                        res.header("Content-Type", "text/csv");
                        res.send(result);
                    }
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
    }
    Route.Couriers = Couriers;
})(Route || (Route = {}));
module.exports = Route;

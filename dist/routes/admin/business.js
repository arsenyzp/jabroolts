"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const escapeStringRegexp = require("escape-string-regexp");
const AjaxResponse_1 = require("../../components/AjaxResponse");
const UserModel_1 = require("../../models/UserModel");
var Route;
(function (Route) {
    class Business {
        index(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                let user = yield UserModel_1.UserModel.findOne({ _id: req.params.id });
                if (user === null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                let sort = {};
                if (req.query.sortKey) {
                    sort[req.query.sortKey] = req.query.reverse;
                }
                else {
                    sort = { created_at: -1 };
                }
                let filter = { _id: { $in: user.couriers } };
                let search = "";
                if (req.query.q !== undefined && req.query.q !== "") {
                    search = escapeStringRegexp(req.query.q);
                    filter = { $or: [
                            { first_name: new RegExp(".*" + search + ".*", "i") },
                            { last_name: new RegExp(".*" + search + ".*", "i") },
                            { phone: new RegExp(".*" + search + ".*", "i") },
                        ],
                        _id: { $in: user.couriers }
                    };
                    sort = { created_at: -1 };
                }
                filter.deleted = { $in: [false, null] };
                let skip = 0;
                let limit = 10;
                if (req.params.page && req.params.perPage) {
                    req.params.page = parseInt(req.params.page) - 1;
                    skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                    limit = parseInt(req.params.perPage);
                }
                UserModel_1.UserModel
                    .count(filter)
                    .then(total_count => {
                    UserModel_1.UserModel
                        .find(filter)
                        .sort(sort)
                        .limit(limit)
                        .skip(skip)
                        .then(users => {
                        let items = [];
                        for (let i = 0; i < users.length; i++) {
                            let item = users[i].getApiFields();
                            item.bank = users[i].getApiFields();
                            item.vehicle = users[i].getVehicle();
                            item.bank = users[i].getBank();
                            item.license = users[i].getLicense();
                            item.socket_ids = users[i].socket_ids;
                            item._id = users[i]._id;
                            items.push(item);
                        }
                        ajaxResponse.setDate({
                            items: items,
                            total_count: total_count
                        });
                        res.json(ajaxResponse.get());
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            });
        }
        delete(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            UserModel_1.UserModel
                .findOneAndUpdate({ _id: req.params.id }, {
                $pull: { couriers: req.params.courier }
            })
                .then(user => {
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        save(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                let user = yield UserModel_1.UserModel.findOne({ jabroolid: req.params.courier });
                if (user === null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                UserModel_1.UserModel
                    .findOneAndUpdate({ _id: req.params.id }, {
                    $pull: { couriers: user._id.toString() }
                })
                    .then(user => {
                    UserModel_1.UserModel
                        .findOneAndUpdate({ _id: req.params.id }, {
                        $push: { couriers: user._id.toString() }
                    })
                        .then(user => {
                        res.json(ajaxResponse.get());
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            });
        }
    }
    Route.Business = Business;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const AjaxResponse_1 = require("../../components/AjaxResponse");
const class_validator_1 = require("class-validator");
const ServiceMessageModel_1 = require("../../models/ServiceMessageModel");
const UserModel_1 = require("../../models/UserModel");
const NotificationModule_1 = require("../../components/NotificationModule");
var Route;
(function (Route) {
    class ServiceMessagesPost {
    }
    __decorate([
        class_validator_1.Length(5, 400, {
            message: "admin.validation.MessageMin5Max400"
        })
    ], ServiceMessagesPost.prototype, "text", void 0);
    __decorate([
        class_validator_1.Length(20, 40, {
            message: "admin.validation.InvalidUserId"
        })
    ], ServiceMessagesPost.prototype, "user", void 0);
    class ServiceMessages {
        index(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                let sort = {};
                if (req.query.sortKey) {
                    sort[req.query.sortKey] = req.query.reverse;
                }
                else {
                    sort = { created_at: -1 };
                }
                let filter = {};
                if (!!req.query.user) {
                    filter.recipient = req.query.user;
                }
                let skip = 0;
                let limit = 10;
                if (req.params.page && req.params.perPage) {
                    req.params.page = parseInt(req.params.page) - 1;
                    skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                    limit = parseInt(req.params.perPage);
                }
                try {
                    let total_count = yield ServiceMessageModel_1.ServiceMessageModel
                        .count(filter);
                    let items = yield ServiceMessageModel_1.ServiceMessageModel
                        .find(filter)
                        .sort(sort)
                        .limit(limit)
                        .skip(skip);
                    let user = yield UserModel_1.UserModel.findOne({ _id: req.query.user });
                    ajaxResponse.setDate({
                        items: items,
                        total_count: total_count,
                        user: user
                    });
                    res.json(ajaxResponse.get());
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
        save(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let post = Object.create(ServiceMessagesPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                }
                else {
                    UserModel_1.UserModel
                        .findOne({ _id: post.user })
                        .then(user => {
                        if (user === null) {
                            ajaxResponse.addError({ message: "User not found" });
                            res.status(500);
                            res.json(ajaxResponse.get());
                        }
                        else {
                            let model = new ServiceMessageModel_1.ServiceMessageModel();
                            model.sender = req.session.user._id;
                            model.recipient = user;
                            model.text = post.text;
                            model.save()
                                .then(item => {
                                NotificationModule_1.NotificationModule.getInstance().send([user._id.toString()], NotificationModule_1.NotificationTypes.JabroolMessage, { text: post.text }, model._id.toString());
                                ajaxResponse.setDate(item);
                                res.json(ajaxResponse.get());
                            })
                                .catch(err => {
                                ajaxResponse.addError(err);
                                res.status(500);
                                res.json(ajaxResponse.get());
                            });
                        }
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                }
            });
        }
    }
    Route.ServiceMessages = ServiceMessages;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
const AjaxResponse_1 = require("../../components/AjaxResponse");
const class_validator_1 = require("class-validator");
const UserModel_1 = require("../../models/UserModel");
var Route;
(function (Route) {
    class LoginPost {
    }
    __decorate([
        class_validator_1.IsEmail({}, {
            message: "admin.validation.LoginInvalidEmail"
        })
    ], LoginPost.prototype, "email", void 0);
    __decorate([
        class_validator_1.Length(6, 20, {
            message: "admin.validation.LoginInvalidPassword"
        })
    ], LoginPost.prototype, "password", void 0);
    class Index {
        index(req, res, next) {
            res.render("admin/login");
        }
        login(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let post = Object.create(LoginPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    ajaxResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.json(ajaxResponse);
                }
                else {
                    UserModel_1.UserModel.findOne({ email: post.email })
                        .then(user => {
                        if (user == null) {
                            ajaxResponse.addErrorMessage("User not found");
                            res.status(500);
                            return res.json(ajaxResponse.get());
                        }
                        if (!user.comparePassword(post.password)) {
                            ajaxResponse.addErrorMessage("Incorrect password");
                            res.status(500);
                            return res.json(ajaxResponse.get());
                        }
                        if (user.role !== UserModel_1.UserRoles.dev && user.role !== UserModel_1.UserRoles.admin) {
                            ajaxResponse.addErrorMessage("Incorrect role user");
                            res.status(500);
                            return res.json(ajaxResponse.get());
                        }
                        req.session.user = user;
                        res.json(ajaxResponse.get());
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                }
            });
        }
    }
    Route.Index = Index;
})(Route || (Route = {}));
module.exports = Route;

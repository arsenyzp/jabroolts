"use strict";
const config_1 = require("../../components/config");
var Route;
(function (Route) {
    class Index {
        index(req, res, next) {
            req.session.user = null;
            res.redirect(config_1.AppConfig.getInstanse().get("urls:admin_login"));
        }
    }
    Route.Index = Index;
})(Route || (Route = {}));
module.exports = Route;

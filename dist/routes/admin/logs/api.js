"use strict";
const escapeStringRegexp = require("escape-string-regexp");
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const ApiLogModel_1 = require("../../../models/logs/ApiLogModel");
var Route;
(function (Route) {
    class ApiLog {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let sort = {};
            if (req.query.sortKey) {
                sort[req.query.sortKey] = req.query.reverse;
            }
            else {
                sort = { created_at: -1 };
            }
            let filter = {};
            let search = "";
            if (req.query.q !== undefined && req.query.q !== "") {
                search = escapeStringRegexp(req.query.q);
                filter = { $or: [
                        { method: new RegExp(".*" + search + ".*", "i") },
                        { params: new RegExp(".*" + search + ".*", "i") },
                        { body: new RegExp(".*" + search + ".*", "i") },
                        { url: new RegExp(".*" + search + ".*", "i") },
                        { response: new RegExp(".*" + search + ".*", "i") }
                    ] };
            }
            let skip = 0;
            let limit = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }
            ApiLogModel_1.ApiLogModel
                .count(filter)
                .then(total_count => {
                ApiLogModel_1.ApiLogModel
                    .find(filter)
                    .sort(sort)
                    .limit(limit)
                    .skip(skip)
                    .then(items => {
                    ajaxResponse.setDate({
                        items: items,
                        total_count: total_count
                    });
                    res.json(ajaxResponse.get());
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        get(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            ApiLogModel_1.ApiLogModel
                .findOne({ _id: req.params.id })
                .then(bank => {
                if (bank == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                ajaxResponse.setDate(bank);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        delete(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            ApiLogModel_1.ApiLogModel
                .findOne({ _id: req.params.id })
                .then(bank => {
                if (bank == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                ApiLogModel_1.ApiLogModel
                    .remove({ _id: req.params.id })
                    .then(r => {
                    res.json(ajaxResponse.get());
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
    }
    Route.ApiLog = ApiLog;
})(Route || (Route = {}));
module.exports = Route;

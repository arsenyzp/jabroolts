"use strict";
const escapeStringRegexp = require("escape-string-regexp");
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const EmailLogModel_1 = require("../../../models/logs/EmailLogModel");
var Route;
(function (Route) {
    class EmailLog {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let sort = {};
            if (req.query.sortKey) {
                sort[req.query.sortKey] = req.query.reverse;
            }
            else {
                sort = { created_at: -1 };
            }
            let filter = {};
            let search = "";
            if (req.query.q !== undefined && req.query.q !== "") {
                search = escapeStringRegexp(req.query.q);
                filter = { $or: [
                        { email: new RegExp(".*" + search + ".*", "i") },
                        { subject: new RegExp(".*" + search + ".*", "i") },
                        { text: new RegExp(".*" + search + ".*", "i") },
                        { html: new RegExp(".*" + search + ".*", "i") }
                    ] };
            }
            let skip = 0;
            let limit = 10;
            if (req.params.page && req.params.perPage) {
                req.params.page = parseInt(req.params.page) - 1;
                skip = parseInt(req.params.page) * parseInt(req.params.perPage);
                limit = parseInt(req.params.perPage);
            }
            EmailLogModel_1.EmailLogModel
                .count(filter)
                .then(total_count => {
                EmailLogModel_1.EmailLogModel
                    .find(filter)
                    .sort(sort)
                    .limit(limit)
                    .skip(skip)
                    .then(items => {
                    ajaxResponse.setDate({
                        items: items,
                        total_count: total_count
                    });
                    res.json(ajaxResponse.get());
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        get(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            EmailLogModel_1.EmailLogModel
                .findOne({ _id: req.params.id })
                .then(bank => {
                if (bank == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                ajaxResponse.setDate(bank);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        delete(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            EmailLogModel_1.EmailLogModel
                .findOne({ _id: req.params.id })
                .then(bank => {
                if (bank == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                EmailLogModel_1.EmailLogModel
                    .remove({ _id: req.params.id })
                    .then(r => {
                    res.json(ajaxResponse.get());
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
    }
    Route.EmailLog = EmailLog;
})(Route || (Route = {}));
module.exports = Route;

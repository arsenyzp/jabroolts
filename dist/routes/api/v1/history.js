"use strict";
const express = require("express");
const historyMethods = require("../../../api/methods/HistoryOrdersMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class History {
        index() {
            let router;
            router = express.Router();
            let history = new historyMethods.HistoryOrdersMethods();
            router.post("/history", ApiAuth_1.ApiAuth.check, history.index.bind(history.index));
            return router;
        }
    }
    Route.History = History;
})(Route || (Route = {}));
module.exports = Route;

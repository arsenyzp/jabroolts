"use strict";
const express = require("express");
const locationsMethods = require("../../../api/methods/LocationHistoryMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class LocationsHistory {
        index() {
            let router;
            router = express.Router();
            let history = new locationsMethods.LocationHistoryMethods();
            router.post("/location_history", ApiAuth_1.ApiAuth.check, history.index.bind(history.index));
            return router;
        }
    }
    Route.LocationsHistory = LocationsHistory;
})(Route || (Route = {}));
module.exports = Route;

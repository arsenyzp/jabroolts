"use strict";
const express = require("express");
const authMethods = require("../../../api/methods/AuthMethods");
var Route;
(function (Route) {
    class Auth {
        index() {
            let router;
            router = express.Router();
            let login = new authMethods.AuthMethods();
            router.post("/login", login.login.bind(login.login));
            router.post("/register", login.register.bind(login.register));
            router.post("/forgot", login.forgot.bind(login.forgot));
            return router;
        }
    }
    Route.Auth = Auth;
})(Route || (Route = {}));
module.exports = Route;

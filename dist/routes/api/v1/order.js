"use strict";
const express = require("express");
const createOrderMethods = require("../../../api/methods/order/CreateOrderMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class Order {
        index() {
            let router;
            router = express.Router();
            let createOrder = new createOrderMethods.CreateOrderMethods();
            router.post("/calculate", ApiAuth_1.ApiAuth.check, createOrder.calculate.bind(createOrder.calculate));
            router.post("/new_order", ApiAuth_1.ApiAuth.check, createOrder.create.bind(createOrder.create));
            return router;
        }
    }
    Route.Order = Order;
})(Route || (Route = {}));
module.exports = Route;

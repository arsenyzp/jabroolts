"use strict";
const express = require("express");
const cardsMethods = require("../../../api/methods/profile/CardsMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class Cards {
        index() {
            let router;
            router = express.Router();
            let cards = new cardsMethods.CardsMethods();
            router.post("/cards", ApiAuth_1.ApiAuth.check, cards.cardsList.bind(cards.cardsList));
            router.post("/save_card", ApiAuth_1.ApiAuth.check, cards.saveCard.bind(cards.saveCard));
            router.post("/remove_card", ApiAuth_1.ApiAuth.check, cards.removeCard.bind(cards.removeCard));
            return router;
        }
    }
    Route.Cards = Cards;
})(Route || (Route = {}));
module.exports = Route;

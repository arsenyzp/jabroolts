"use strict";
const express = require("express");
const listOrderMethods = require("../../../api/methods/order/OrdersListMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class OrderList {
        index() {
            let router;
            router = express.Router();
            let listOrder = new listOrderMethods.OrdersListMethods();
            router.post("/my_order_courier", ApiAuth_1.ApiAuth.check, listOrder.getMyOrdersCourier.bind(listOrder.getMyOrdersCourier));
            router.post("/my_order_customer", ApiAuth_1.ApiAuth.check, listOrder.getMyOrdersCustomer.bind(listOrder.getMyOrdersCustomer));
            return router;
        }
    }
    Route.OrderList = OrderList;
})(Route || (Route = {}));
module.exports = Route;

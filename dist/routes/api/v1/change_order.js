"use strict";
const express = require("express");
const changeMethods = require("../../../api/methods/order/ChangeOrderMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class ChangeOrder {
        index() {
            let router;
            router = express.Router();
            let change = new changeMethods.ChangeOrderMethods();
            router.post("/courier_change_order", ApiAuth_1.ApiAuth.check, change.courierChangeOrder.bind(change.courierChangeOrder));
            router.post("/customer_accept_change", ApiAuth_1.ApiAuth.check, change.customerAcceptChange.bind(change.customerAcceptChange));
            router.post("/update_receiver_number", ApiAuth_1.ApiAuth.check, change.updateRecipientPhone.bind(change.updateRecipientPhone));
            return router;
        }
    }
    Route.ChangeOrder = ChangeOrder;
})(Route || (Route = {}));
module.exports = Route;

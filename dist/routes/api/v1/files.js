"use strict";
const express = require("express");
const filesMethods = require("../../../api/methods/FilesMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class Files {
        index() {
            let router;
            router = express.Router();
            let files = new filesMethods.FilesMethods();
            router.post("/upload_image", ApiAuth_1.ApiAuth.check, files.upload.bind(files.upload));
            router.post("/upload", ApiAuth_1.ApiAuth.check, files.upload.bind(files.upload));
            router.post("/delete_image", ApiAuth_1.ApiAuth.check, files.delete.bind(files.delete));
            router.get("/resize_image", ApiAuth_1.ApiAuth.check, files.resize.bind(files.resize));
            return router;
        }
    }
    Route.Files = Files;
})(Route || (Route = {}));
module.exports = Route;

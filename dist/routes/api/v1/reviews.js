"use strict";
const express = require("express");
const reviewsMethods = require("../../../api/methods/ReviewMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class Reviews {
        index() {
            let router;
            router = express.Router();
            let review = new reviewsMethods.ReviewMethods();
            router.post("/save_review", ApiAuth_1.ApiAuth.check, review.saveReview.bind(review.saveReview));
            return router;
        }
    }
    Route.Reviews = Reviews;
})(Route || (Route = {}));
module.exports = Route;

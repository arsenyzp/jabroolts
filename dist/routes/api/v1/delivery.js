"use strict";
const express = require("express");
const deliveryMethods = require("../../../api/methods/order/DeliveryMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class Delivery {
        index() {
            let router;
            router = express.Router();
            let delivery = new deliveryMethods.DeliveryMethods();
            router.post("/confirm_pickup", ApiAuth_1.ApiAuth.check, delivery.confirmPickUp.bind(delivery.confirmPickUp));
            router.post("/confirm_delivery", ApiAuth_1.ApiAuth.check, delivery.confirmDelivery.bind(delivery.confirmDelivery));
            router.post("/start_delivery", ApiAuth_1.ApiAuth.check, delivery.startDelivery.bind(delivery.startDelivery));
            router.post("/courier_on_way", ApiAuth_1.ApiAuth.check, delivery.courierOnWay.bind(delivery.courierOnWay));
            router.post("/at_destination", ApiAuth_1.ApiAuth.check, delivery.atDestinition.bind(delivery.atDestinition));
            return router;
        }
    }
    Route.Delivery = Delivery;
})(Route || (Route = {}));
module.exports = Route;

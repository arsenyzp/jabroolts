"use strict";
const express = require("express");
const catalogMethods = require("../../../api/methods/CatalogMethods");
var Route;
(function (Route) {
    class Catalog {
        index() {
            let router;
            router = express.Router();
            let catalog = new catalogMethods.CatalogMethods();
            router.post("/catalogs/banks", catalog.bank.bind(catalog.bank));
            router.post("/catalogs/countries", catalog.countries.bind(catalog.countries));
            router.post("/catalogs/catalogs", catalog.catalogs.bind(catalog.catalogs));
            router.post("/catalogs/car_types", catalog.сarTypes.bind(catalog.сarTypes));
            router.post("/catalogs/packages", catalog.packages.bind(catalog.packages));
            router.post("/catalogs/terms", catalog.terms.bind(catalog.terms));
            return router;
        }
    }
    Route.Catalog = Catalog;
})(Route || (Route = {}));
module.exports = Route;

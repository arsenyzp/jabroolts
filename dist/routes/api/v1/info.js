"use strict";
const express = require("express");
const infoMethods = require("../../../api/methods/order/InfoMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class Info {
        index() {
            let router;
            router = express.Router();
            let info = new infoMethods.InfoMethods();
            router.post("/get_profile_by_jid", ApiAuth_1.ApiAuth.check, info.getProfileByJid.bind(info.getProfileByJid));
            router.post("/get_order_info", ApiAuth_1.ApiAuth.check, info.getOrderById.bind(info.getOrderById));
            router.post("/get_pickup_confirm_code", ApiAuth_1.ApiAuth.check, info.getPickupConfirmCode.bind(info.getPickupConfirmCode));
            router.post("/get_delivery_confirm_code", ApiAuth_1.ApiAuth.check, info.getDeliveryConfirmCode.bind(info.getDeliveryConfirmCode));
            router.post("/get_courier_profile_by_order", ApiAuth_1.ApiAuth.check, info.getCourierProfileByOrder.bind(info.getCourierProfileByOrder));
            return router;
        }
    }
    Route.Info = Info;
})(Route || (Route = {}));
module.exports = Route;

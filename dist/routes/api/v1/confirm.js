"use strict";
const express = require("express");
const confirmMethods = require("../../../api/methods/ConfirmMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class Confirm {
        index() {
            let router;
            router = express.Router();
            let confirm = new confirmMethods.ConfirmMethods();
            router.post("/confirm", ApiAuth_1.ApiAuth.check, confirm.confirm.bind(confirm.confirm));
            router.post("/resend", ApiAuth_1.ApiAuth.check, confirm.resend.bind(confirm.resend));
            return router;
        }
    }
    Route.Confirm = Confirm;
})(Route || (Route = {}));
module.exports = Route;

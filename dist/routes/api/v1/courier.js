"use strict";
const express = require("express");
const courierMethods = require("../../../api/methods/CourierMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class Courier {
        index() {
            let router;
            router = express.Router();
            let courier = new courierMethods.CourierMethods();
            router.post("/check_courier_status", ApiAuth_1.ApiAuth.check, courier.checkCourierStatus.bind(courier.checkCourierStatus));
            router.post("/get_courier_profile", ApiAuth_1.ApiAuth.check, courier.getCourierProfile.bind(courier.getCourierProfile));
            router.post("/courier_requests", ApiAuth_1.ApiAuth.check, courier.getCourierRequests.bind(courier.getCourierRequests));
            router.post("/accept_request", ApiAuth_1.ApiAuth.check, courier.acceptRequests.bind(courier.acceptRequests));
            router.post("/decline_request", ApiAuth_1.ApiAuth.check, courier.declineRequests.bind(courier.declineRequests));
            router.post("/request_update_number", ApiAuth_1.ApiAuth.check, courier.requestUpdateNumber.bind(courier.requestUpdateNumber));
            return router;
        }
    }
    Route.Courier = Courier;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
const express = require("express");
const openAppMethods = require("../../../api/methods/reports/OpenAppMethods");
var Route;
(function (Route) {
    class Reports {
        index() {
            let router;
            router = express.Router();
            let openApp = new openAppMethods.OpenAppMethods();
            router.post("/open_app", openApp.openApp.bind(openApp.openApp));
            return router;
        }
    }
    Route.Reports = Reports;
})(Route || (Route = {}));
module.exports = Route;

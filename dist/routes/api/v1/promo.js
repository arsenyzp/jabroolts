"use strict";
const express = require("express");
const promoMethods = require("../../../api/methods/PromoMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class Promo {
        index() {
            let router;
            router = express.Router();
            let promo = new promoMethods.PromoMethods();
            router.post("/check_promo", ApiAuth_1.ApiAuth.check, promo.checkPromo.bind(promo.checkPromo));
            router.post("/save_promo", ApiAuth_1.ApiAuth.check, promo.save.bind(promo.save));
            router.post("/day_bonus", ApiAuth_1.ApiAuth.check, promo.dayPromo.bind(promo.dayPromo));
            router.post("/courier_limit", ApiAuth_1.ApiAuth.check, promo.getCourierLimit.bind(promo.getCourierLimit));
            return router;
        }
    }
    Route.Promo = Promo;
})(Route || (Route = {}));
module.exports = Route;

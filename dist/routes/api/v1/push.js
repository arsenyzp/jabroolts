"use strict";
const express = require("express");
const pushMethods = require("../../../api/methods/PushMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class Push {
        index() {
            let router;
            router = express.Router();
            let push = new pushMethods.PushMethods();
            router.post("/add_device", ApiAuth_1.ApiAuth.check, push.save.bind(push.save));
            router.post("/remove_device", ApiAuth_1.ApiAuth.check, push.remove.bind(push.remove));
            return router;
        }
    }
    Route.Push = Push;
})(Route || (Route = {}));
module.exports = Route;

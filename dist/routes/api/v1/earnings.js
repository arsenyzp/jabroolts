"use strict";
const express = require("express");
const earningsMethods = require("../../../api/methods/EarningsMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class Earnings {
        index() {
            let router;
            router = express.Router();
            let earnings = new earningsMethods.EarningsMethods();
            router.post("/earnings_week", ApiAuth_1.ApiAuth.check, earnings.week.bind(earnings.week));
            router.post("/earnings_month", ApiAuth_1.ApiAuth.check, earnings.month.bind(earnings.month));
            router.post("/earnings_year", ApiAuth_1.ApiAuth.check, earnings.year.bind(earnings.year));
            return router;
        }
    }
    Route.Earnings = Earnings;
})(Route || (Route = {}));
module.exports = Route;

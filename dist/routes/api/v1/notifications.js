"use strict";
const express = require("express");
const notificationsMethods = require("../../../api/methods/NotificationMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class Notifications {
        index() {
            let router;
            router = express.Router();
            let confirm = new notificationsMethods.NotificationMethods();
            router.post("/notifications", ApiAuth_1.ApiAuth.check, confirm.index.bind(confirm.index));
            return router;
        }
    }
    Route.Notifications = Notifications;
})(Route || (Route = {}));
module.exports = Route;

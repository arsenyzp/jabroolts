"use strict";
const express = require("express");
const favoriteLocationMethods = require("../../../api/methods/FavoriteLocationsMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class FavoriteLocation {
        index() {
            let router;
            router = express.Router();
            let confirm = new favoriteLocationMethods.FavoriteLocationsMethods();
            router.post("/favorites", ApiAuth_1.ApiAuth.check, confirm.list.bind(confirm.list));
            router.post("/add_favorite_location", ApiAuth_1.ApiAuth.check, confirm.save.bind(confirm.save));
            router.post("/remove_favorite_location", ApiAuth_1.ApiAuth.check, confirm.delete.bind(confirm.delete));
            return router;
        }
    }
    Route.FavoriteLocation = FavoriteLocation;
})(Route || (Route = {}));
module.exports = Route;

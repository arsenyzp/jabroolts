"use strict";
const express = require("express");
const messagesMethods = require("../../../api/methods/MessagesMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class Messages {
        index() {
            let router;
            router = express.Router();
            let confirm = new messagesMethods.MessagesMethods();
            router.post("/messages", ApiAuth_1.ApiAuth.check, confirm.index.bind(confirm.index));
            return router;
        }
    }
    Route.Messages = Messages;
})(Route || (Route = {}));
module.exports = Route;

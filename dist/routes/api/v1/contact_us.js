"use strict";
const express = require("express");
const contactUsMethods = require("../../../api/methods/ContactUsMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class ContactUs {
        index() {
            let router;
            router = express.Router();
            let contactUs = new contactUsMethods.ContactUsMethods();
            router.post("/contact_us", ApiAuth_1.ApiAuth.check, contactUs.contact.bind(contactUs.contact));
            return router;
        }
    }
    Route.ContactUs = ContactUs;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
const express = require("express");
const confirmMethods = require("../../../api/methods/profile/ConfirmCodeMethods");
const bankMethods = require("../../../api/methods/profile/BankMethods");
const vehicleMethods = require("../../../api/methods/profile/VehicleMethods");
const profileMethods = require("../../../api/methods/profile/ProfileMethods");
const paymentSettingsMethods = require("../../../api/methods/profile/ProfilePaymentSettings");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class Profile {
        index() {
            let router;
            router = express.Router();
            let confirm = new confirmMethods.ConfirmCodeMethods();
            router.post("/get_confirm_code", ApiAuth_1.ApiAuth.check, confirm.getCode.bind(confirm.getCode));
            router.post("/get_confirm_password_code", ApiAuth_1.ApiAuth.check, confirm.getPasswordCode.bind(confirm.getPasswordCode));
            let bank = new bankMethods.BankMethods();
            router.post("/get_bank", ApiAuth_1.ApiAuth.check, bank.get.bind(bank.get));
            router.post("/save_bank", ApiAuth_1.ApiAuth.check, bank.save.bind(bank.save));
            let vehicle = new vehicleMethods.VehicleMethods();
            router.post("/save_license_info", ApiAuth_1.ApiAuth.check, vehicle.saveLicense.bind(vehicle.saveLicense));
            router.post("/save_vehicle", ApiAuth_1.ApiAuth.check, vehicle.saveVehicle.bind(vehicle.saveVehicle));
            let paymentsSettings = new paymentSettingsMethods.ProfilePaymentSettings();
            router.post("/get_token", ApiAuth_1.ApiAuth.check, paymentsSettings.getToken.bind(paymentsSettings.getToken));
            router.post("/set_pay_type", ApiAuth_1.ApiAuth.check, paymentsSettings.setPayType.bind(paymentsSettings.setPayType));
            let profile = new profileMethods.ProfileMethods();
            router.post("/get_profile", ApiAuth_1.ApiAuth.check, profile.get.bind(profile.get));
            router.post("/save_profile", ApiAuth_1.ApiAuth.check, profile.save.bind(profile.save));
            router.post("/set_password", ApiAuth_1.ApiAuth.check, profile.setPassword.bind(profile.setPassword));
            router.post("/confirm_change_password", ApiAuth_1.ApiAuth.check, profile.confirmPassword.bind(profile.confirmPassword));
            router.post("/set_visible", ApiAuth_1.ApiAuth.check, profile.setVisible.bind(profile.setVisible));
            router.post("/upload_avatar", ApiAuth_1.ApiAuth.check, profile.uploadAvatar.bind(profile.uploadAvatar));
            return router;
        }
    }
    Route.Profile = Profile;
})(Route || (Route = {}));
module.exports = Route;

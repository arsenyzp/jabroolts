"use strict";
const express = require("express");
const cancelMethods = require("../../../api/methods/order/CancelOrderMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class CancelOrder {
        index() {
            let router;
            router = express.Router();
            let cancel = new cancelMethods.CancelOrderMethods();
            router.post("/cancel_cost", ApiAuth_1.ApiAuth.check, cancel.cancelCost.bind(cancel.cancelCost));
            router.post("/cancel_request", ApiAuth_1.ApiAuth.check, cancel.cancelRequest.bind(cancel.cancelRequest));
            router.post("/cancel_delivery", ApiAuth_1.ApiAuth.check, cancel.cancelDelivery.bind(cancel.cancelDelivery));
            return router;
        }
    }
    Route.CancelOrder = CancelOrder;
})(Route || (Route = {}));
module.exports = Route;

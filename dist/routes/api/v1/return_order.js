"use strict";
const express = require("express");
const returnMethods = require("../../../api/methods/order/ReturnOrderMethods");
const ApiAuth_1 = require("../../../components/ApiAuth");
var Route;
(function (Route) {
    class Return {
        index() {
            let router;
            router = express.Router();
            let returnOrder = new returnMethods.ReturnMethods();
            router.post("/start_return", ApiAuth_1.ApiAuth.check, returnOrder.startReturn.bind(returnOrder.startReturn));
            router.post("/cancel_return", ApiAuth_1.ApiAuth.check, returnOrder.cancelReturn.bind(returnOrder.cancelReturn));
            router.post("/at_return_destination", ApiAuth_1.ApiAuth.check, returnOrder.atReturnDestination.bind(returnOrder.atReturnDestination));
            router.post("/get_return_confirm_code", ApiAuth_1.ApiAuth.check, returnOrder.getReturnConfirmCode.bind(returnOrder.getReturnConfirmCode));
            router.post("/confirm_return", ApiAuth_1.ApiAuth.check, returnOrder.confirmReturn.bind(returnOrder.confirmReturn));
            router.post("/return_cost", ApiAuth_1.ApiAuth.check, returnOrder.getReturnCost.bind(returnOrder.getReturnCost));
            return router;
        }
    }
    Route.Return = Return;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
const express = require("express");
const publicIndexndexRoute = require("./public/index");
const adminIndexndexRoute = require("./admin/index");
const apiIndexndexRoute = require("./api/index");
const ApiParser_1 = require("../components/ApiParser");
var Route;
(function (Route) {
    class Index {
        index() {
            let router;
            router = express.Router();
            let publicIndex = new publicIndexndexRoute.Index();
            router.use("/", publicIndex.index());
            let adminIndex = new adminIndexndexRoute.Index();
            router.use("/admin", adminIndex.index());
            let apiIndexndex = new apiIndexndexRoute.Index();
            router.use("/api", ApiParser_1.ApiParser.parser, apiIndexndex.index());
            return router;
        }
    }
    Route.Index = Index;
})(Route || (Route = {}));
module.exports = Route;

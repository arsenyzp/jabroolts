"use strict";
const express = require("express");
const home = require("./home");
const login = require("./login");
const confirm = require("./confirm");
const terms = require("./terms");
const ajax = require("./ajax");
const account = require("./account/index");
const orders = require("./account/orders");
const promo = require("./account/promo");
const settings = require("./account/settings");
const payments = require("./account/payments");
const UserAuth_1 = require("../../components/UserAuth");
var Route;
(function (Route) {
    class Index {
        index() {
            let router;
            router = express.Router();
            let homeRoute = new home.Index();
            router.get("/site", homeRoute.index.bind(homeRoute.index));
            router.get("/register", homeRoute.register.bind(homeRoute.register));
            router.get("/pricing", homeRoute.pricing.bind(homeRoute.pricing));
            router.get("/about", homeRoute.about.bind(homeRoute.about));
            router.get("/download", homeRoute.download.bind(homeRoute.download));
            router.get("/logout", homeRoute.logout.bind(homeRoute.logout));
            router.get("/", homeRoute.tmp.bind(homeRoute.tmp));
            router.post("/subscribe", homeRoute.subscribe.bind(homeRoute.subscribe));
            let loginRoute = new login.Index();
            router.get("/login", loginRoute.index.bind(loginRoute.index));
            let confirmRoute = new confirm.Index();
            router.get("/confirm/:confirm_email_code", confirmRoute.index.bind(confirmRoute.index));
            let termsRoute = new terms.Index();
            router.get("/privacy_policy", termsRoute.index.bind(termsRoute.index));
            router.get("/privacy_policy_ar", termsRoute.indexAr.bind(termsRoute.indexAr));
            let ajaxRoute = new ajax.Ajax();
            router.post("/site/login", ajaxRoute.login.bind(ajaxRoute.login));
            router.post("/site/register", ajaxRoute.register.bind(ajaxRoute.register));
            router.post("/site/contact_us", ajaxRoute.contact_us.bind(ajaxRoute.contact_us));
            router.use("/ajax/uploads_img", ajaxRoute.upload());
            router.post("/ajax/calculateDistance", ajaxRoute.calculateDistance.bind(ajaxRoute.calculateDistance));
            router.post("/ajax/saveTmpOrder", ajaxRoute.saveTmpOrder.bind(ajaxRoute.saveTmpOrder));
            let accountRoute = new account.Index();
            router.get("/jid", accountRoute.jid.bind(accountRoute.jid));
            router.get("/account", UserAuth_1.UserAuth.check, accountRoute.home.bind(accountRoute.home));
            router.get("/account/getToken", UserAuth_1.UserAuth.check, accountRoute.getToken.bind(accountRoute.getToken));
            router.get("/account/ordersHistory", UserAuth_1.UserAuth.check, accountRoute.ordersHistory.bind(accountRoute.ordersHistory));
            router.get("/account/getProfile", UserAuth_1.UserAuth.check, accountRoute.getProfile.bind(accountRoute.getProfile));
            router.post("/account/saveProfile", UserAuth_1.UserAuth.check, accountRoute.saveProfile.bind(accountRoute.saveProfile));
            router.get("/account/getPaymentToken", UserAuth_1.UserAuth.check, accountRoute.getPaymentToken.bind(accountRoute.getPaymentToken));
            let ordersRoute = new orders.Orders();
            router.get("/account/orders", UserAuth_1.UserAuth.check, ordersRoute.list.bind(ordersRoute.list));
            router.get("/account/cancel_cost", UserAuth_1.UserAuth.check, ordersRoute.cancelCost.bind(ordersRoute.cancelCost));
            router.get("/account/cancel", UserAuth_1.UserAuth.check, ordersRoute.cancel.bind(ordersRoute.cancel));
            router.post("/account/saveOrder", UserAuth_1.UserAuth.check, ordersRoute.saveOrder.bind(ordersRoute.saveOrder));
            let promoRoute = new promo.Promo();
            router.get("/account/day_bonus", UserAuth_1.UserAuth.check, promoRoute.dayPromo.bind(promoRoute.dayPromo));
            router.get("/account/apply_code", UserAuth_1.UserAuth.check, promoRoute.save.bind(promoRoute.save));
            let settingsRoute = new settings.Settings();
            router.get("/account/favoriteLocation", UserAuth_1.UserAuth.check, settingsRoute.getFavoriteLocation.bind(settingsRoute.getFavoriteLocation));
            router.get("/account/getHistory", UserAuth_1.UserAuth.check, settingsRoute.getHistory.bind(settingsRoute.getHistory));
            let paymentsRoute = new payments.Payments();
            router.get("/account/getProfileCards", UserAuth_1.UserAuth.check, paymentsRoute.index.bind(paymentsRoute.index));
            router.post("/account/saveDefaultPayment", UserAuth_1.UserAuth.check, paymentsRoute.saveDefaultPayment.bind(paymentsRoute.saveDefaultPayment));
            router.post("/account/saveCard", UserAuth_1.UserAuth.check, paymentsRoute.saveCard.bind(paymentsRoute.saveCard));
            return router;
        }
    }
    Route.Index = Index;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const express = require("express");
const EventBus = require("eventbusjs");
const Events_1 = require("../../components/events/Events");
const UserModel_1 = require("../../models/UserModel");
const class_validator_1 = require("class-validator");
const AjaxResponse_1 = require("../../components/AjaxResponse");
const RegisterModel_1 = require("./postModels/RegisterModel");
const ContactUsModel_1 = require("./postModels/ContactUsModel");
const LoginModel_1 = require("./postModels/LoginModel");
const ContactUsModel_2 = require("../../models/ContactUsModel");
const config_1 = require("../../components/config");
const multer = require("multer");
const mime = require("mime-types");
const CreateOrderWebPost_1 = require("./postModels/CreateOrderWebPost");
const CreateOrderHelper_1 = require("../../components/jabrool/CreateOrderHelper");
const axios_1 = require("axios");
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, config_1.AppConfig.getInstanse().get("files:user_uploads"));
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + "-" + Date.now() + "." + mime.extension(file.mimetype));
    }
});
const upload = multer({ storage: storage, limits: { fileSize: 3000000 } });
const cpUpload = upload.single("image");
var Route;
(function (Route) {
    class Ajax {
        login(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                let post = Object.create(LoginModel_1.LoginPost.prototype);
                Object.assign(post, req.body, {});
                try {
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        ajaxResponse.addValidationErrors(req, errors);
                        res.json(ajaxResponse.get());
                    }
                    else {
                        let user = yield UserModel_1.UserModel
                            .findOne({ email: post.email, deleted: false });
                        if (user == null) {
                            ajaxResponse.addErrorMessage(req.__("site.errors.UserNotFound"));
                            res.json(ajaxResponse.get());
                        }
                        else {
                            if (user.comparePassword(post.password)) {
                                if (user.in_progress && post.type === "customer") {
                                    res.status(403);
                                    ajaxResponse.addErrorMessage(req.__("site.errors.YouAreAlreadyLoginAsCourier"));
                                    res.json(ajaxResponse.get());
                                }
                                else {
                                    EventBus.dispatch(Events_1.EVENT_WEB_USER_LOGIN, this, { user: user });
                                    req.session.user = user;
                                    ajaxResponse.setDate(user.getApiFields());
                                    res.json(ajaxResponse.get());
                                }
                            }
                            else {
                                ajaxResponse.addErrorMessage(req.__("site.errors.IncorrectPassword"));
                                res.json(ajaxResponse.get());
                            }
                        }
                    }
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
        register(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                let post = Object.create(RegisterModel_1.RegisterPost.prototype);
                Object.assign(post, req.body, {});
                try {
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        ajaxResponse.addValidationErrors(req, errors);
                        res.json(ajaxResponse.get());
                    }
                    else {
                        post.phone = post.phone.replace(/\D/g, "");
                        let user = yield UserModel_1.UserModel.findOne({
                            $or: [
                                { email: post.email },
                                { phone: post.phone }
                            ]
                        });
                        if (user !== null) {
                            if (user.email === post.email) {
                                ajaxResponse.addErrorMessage(req.__("site.errors.EmailAlreadyUsedInService"));
                                res.json(ajaxResponse.get());
                            }
                            else if (user.phone === post.phone) {
                                ajaxResponse.addErrorMessage(req.__("site.errors.PhoneAlreadyUsedInService"));
                                res.json(ajaxResponse.get());
                            }
                        }
                        else {
                            let item = new UserModel_1.UserModel();
                            item.location.coordinates = [0, 0];
                            item.location.type = "Point";
                            item.phone = post.getPhone();
                            item.email = post.email;
                            item.first_name = post.first_name;
                            item.last_name = post.last_name;
                            item.role = UserModel_1.UserRoles.user;
                            item.setPassword(post.password);
                            item.status = UserModel_1.UserStatus.Review;
                            item.genToken();
                            item.generateConfirmSmsCode();
                            item.generateConfirmEmail();
                            if (req.session.jid && req.session.jid !== "") {
                                let ref = yield UserModel_1.UserModel.findOne({ jabroolid: req.session.jid });
                                if (ref !== null) {
                                    post.jid = req.session.jid;
                                }
                            }
                            if (post.jid) {
                                let refferer = yield UserModel_1.UserModel
                                    .findOneAndUpdate({
                                    jabroolid: post.jid,
                                }, {
                                    $push: {
                                        referals: item._id
                                    }
                                });
                                if (refferer != null) {
                                    item.referrer = refferer._id;
                                    yield item.save();
                                }
                                else {
                                    ajaxResponse.addErrorMessage(req.__("site.errors.JidUserNotFound"));
                                    return res.json(ajaxResponse.get());
                                }
                            }
                            yield item
                                .generateJId();
                            yield item
                                .save();
                            item.new_password = post.password;
                            EventBus.dispatch(Events_1.EVENT_WEB_USER_REGISTER, this, { user: item });
                            req.session.user = item;
                            ajaxResponse.setDate(item.getApiFields());
                            return res.json(ajaxResponse.get());
                        }
                    }
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
        contact_us(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                let post = Object.create(ContactUsModel_1.ContactUsModel.prototype);
                Object.assign(post, req.body, {});
                class_validator_1.validate(post).then(errors => {
                    if (errors.length > 0) {
                        ajaxResponse.addValidationErrors(req, errors);
                        res.json(ajaxResponse.get());
                    }
                    else {
                        let model = new ContactUsModel_2.ContactUsModel();
                        model.phone = post.number;
                        model.email = "--";
                        model.text = "from web site";
                        model.name = post.name;
                        model
                            .save()
                            .then(m => {
                            ajaxResponse.setDate(m.getPublicFields());
                            return res.json(ajaxResponse.get());
                        })
                            .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                    }
                });
            });
        }
        upload() {
            let router;
            router = express.Router();
            router.post("/", cpUpload, this._upload.bind(this._upload));
            return router;
        }
        _upload(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let file = req.file.filename;
            ajaxResponse.setDate({
                image: file,
                image_url: config_1.AppConfig.getInstanse().get("urls:user_uploads") + "/" + file
            });
            res.json(ajaxResponse.get());
        }
        saveTmpOrder(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                if (!req.session.user) {
                    req.body.phone = req.body.phone.replace(/\D/g, "");
                    let user = yield UserModel_1.UserModel.findOne({
                        $or: [
                            { email: req.body.email },
                            { phone: req.body.phone }
                        ]
                    });
                    if (user !== null) {
                        if (user.email === req.body.email) {
                            ajaxResponse.addErrorMessage(req.__("site.errors.EmailAlreadyUsedInService"));
                            res.json(ajaxResponse.get());
                        }
                        else if (user.phone === req.body.phone) {
                            ajaxResponse.addErrorMessage(req.__("site.errors.PhoneAlreadyUsedInService"));
                            res.json(ajaxResponse.get());
                        }
                    }
                    else {
                        let item = new UserModel_1.UserModel();
                        item.location.coordinates = [0, 0];
                        item.location.type = "Point";
                        item.phone = "+" + req.body.phone;
                        item.email = req.body.email;
                        item.first_name = req.body.first_name;
                        item.last_name = req.body.last_name;
                        item.role = UserModel_1.UserRoles.user;
                        item.setPassword(req.body.password);
                        item.status = UserModel_1.UserStatus.Review;
                        item.pay_type = "cash";
                        item.genToken();
                        item.generateConfirmSmsCode();
                        item.generateConfirmEmail();
                        if (req.session.jid && req.session.jid !== "") {
                            let ref = yield UserModel_1.UserModel.findOne({ jabroolid: req.session.jid });
                            if (ref !== null) {
                                req.body.rjid = req.session.jid;
                            }
                        }
                        if (req.body.rjid) {
                            let refferer = yield UserModel_1.UserModel
                                .findOneAndUpdate({
                                jabroolid: req.body.rjid,
                            }, {
                                $push: {
                                    referals: item._id
                                }
                            });
                            if (refferer != null) {
                                item.referrer = refferer._id;
                                yield item.save();
                            }
                            else {
                                ajaxResponse.addErrorMessage(req.__("site.errors.JidUserNotFound"));
                                return res.json(ajaxResponse.get());
                            }
                        }
                        yield item
                            .generateJId();
                        yield item
                            .save();
                        item.new_password = req.body.password;
                        EventBus.dispatch(Events_1.EVENT_WEB_USER_REGISTER, this, { user: item });
                        req.session.user = item;
                    }
                }
                let post = Object.create(CreateOrderWebPost_1.CreateOrderWebPost.prototype);
                Object.assign(post, req.body, {});
                class_validator_1.validate(post).then(errors => {
                    if (errors.length > 0) {
                        ajaxResponse.addValidationErrors(req, errors);
                        res.json(ajaxResponse.get());
                    }
                    else {
                        CreateOrderHelper_1.CreateOrderHelper.findRecipientWeb(req, res, post, ajaxResponse);
                    }
                });
            });
        }
        calculateDistance(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                try {
                    let response = yield axios_1.default.get("https://maps.googleapis.com/maps/api/distancematrix/json", {
                        params: {
                            units: "metric",
                            origins: req.body.owner.lat + "," + req.body.owner.lon,
                            destinations: req.body.recipient.lat + "," + req.body.recipient.lon,
                            key: config_1.AppConfig.getInstanse().get("gmap_key")
                        }
                    });
                    ajaxResponse.setDate(response.data);
                    return res.json(ajaxResponse.get());
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
    }
    Route.Ajax = Ajax;
})(Route || (Route = {}));
module.exports = Route;

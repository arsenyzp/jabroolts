"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
class RegisterPost {
    constructor() {
        this.jid = "";
    }
    getPhone() {
        let phone = this.phone.toString().match(/\d/g);
        return phone.join("");
    }
}
__decorate([
    class_validator_1.IsEmail({}, {
        message: "site.validation.InvalidEmail"
    })
], RegisterPost.prototype, "email", void 0);
__decorate([
    class_validator_1.Length(6, 20, {
        message: "site.validation.InvalidPhone"
    })
], RegisterPost.prototype, "phone", void 0);
__decorate([
    class_validator_1.Length(6, 20, {
        message: "site.validation.InvalidPassword"
    })
], RegisterPost.prototype, "password", void 0);
__decorate([
    class_validator_1.Length(2, 50, {
        message: "site.validation.InvalidFirstName"
    })
], RegisterPost.prototype, "first_name", void 0);
__decorate([
    class_validator_1.Length(2, 50, {
        message: "site.validation.InvalidLastName"
    })
], RegisterPost.prototype, "last_name", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.jid; }),
    class_validator_1.Length(6, 20, {
        message: "site.validation.InvalidJID"
    })
], RegisterPost.prototype, "jid", void 0);
exports.RegisterPost = RegisterPost;

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const UserModel_1 = require("../../models/UserModel");
var Route;
(function (Route) {
    class Index {
        index(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    let user = yield UserModel_1.UserModel.findOneAndUpdate({ confirm_email_code: req.params.confirm_email_code }, { confirm_email: true });
                    if (user === null) {
                        res.render("public/confirm", { error: true });
                    }
                    else {
                        res.render("public/confirm", { error: false });
                    }
                }
                catch (err) {
                    res.render("public/confirm", { error: true });
                }
            });
        }
    }
    Route.Index = Index;
})(Route || (Route = {}));
module.exports = Route;

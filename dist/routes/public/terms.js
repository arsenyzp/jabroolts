"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const TermsConfig_1 = require("../../models/configs/TermsConfig");
var Route;
(function (Route) {
    class Index {
        index(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let conf = new TermsConfig_1.TermsConfig();
                conf
                    .getInstanse()
                    .then(c => {
                    res.render("public/terms", { privacy: c.privacy_en });
                });
            });
        }
        indexAr(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let conf = new TermsConfig_1.TermsConfig();
                conf
                    .getInstanse()
                    .then(c => {
                    res.render("public/terms_ar", { privacy: c.privacy_ar });
                });
            });
        }
    }
    Route.Index = Index;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const UserModel_1 = require("../../../models/UserModel");
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const NotificationModule_1 = require("../../../components/NotificationModule");
const PromoCodHelper_1 = require("../../../components/jabrool/PromoCodHelper");
const PaymentLog_1 = require("../../../models/PaymentLog");
const NotificationModel_1 = require("../../../models/NotificationModel");
var Route;
(function (Route) {
    class Promo {
        dayPromo(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            let date = new Date();
            date.setUTCHours(0);
            date.setUTCMinutes(0);
            date.setUTCSeconds(0);
            date.setUTCMilliseconds(0);
            let time = date.getTime();
            PaymentLog_1.PaymentLogModel
                .find({
                user: req.session.user._id,
                type: { $in: [PaymentLog_1.PaymentTypes.Promo, PaymentLog_1.PaymentTypes.Invite, PaymentLog_1.PaymentTypes.Admin] },
                created_at: { $gte: time }
            })
                .then((logs) => {
                let amount = 0;
                for (let i = 0; i < logs.length; i++) {
                    amount += logs[i].amount;
                }
                ajaxResponse.setDate({
                    bonus: amount.toFixed(2),
                    jabroolFee: req.session.user.jabroolFee,
                    courierLimit: req.session.user.courierLimit
                });
                res.json(ajaxResponse.get());
            })
                .catch((err) => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        save(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                try {
                    let result = yield PromoCodHelper_1.PromoCodHelper.use(req.session.user, req.query.code, "client");
                    switch (result.status) {
                        case PromoCodHelper_1.PromoCodeStatus.NotFound:
                            res.status(500);
                            ajaxResponse.addErrorMessage(req.__("site.errors.NotFoundCode"));
                            res.json(ajaxResponse.get());
                            break;
                        case PromoCodHelper_1.PromoCodeStatus.Used:
                            res.status(500);
                            ajaxResponse.addErrorMessage(req.__("site.errors.UsedCode"));
                            res.json(ajaxResponse.get());
                            break;
                        case PromoCodHelper_1.PromoCodeStatus.Ready:
                            if (req.session.user.balance < 0) {
                                let user = yield UserModel_1.UserModel.findOne({ _id: req.session.user._id });
                                if (user !== null && user.balance >= 0) {
                                    yield NotificationModel_1.NotificationModel.update({
                                        user: req.user._id, type: {
                                            $in: [
                                                NotificationModule_1.NotificationTypes.JabroolDebt
                                            ]
                                        }
                                    }, { is_view: true }, { multi: true });
                                }
                            }
                            ajaxResponse.setDate({ discount: result.promo.amount });
                            res.json(ajaxResponse.get());
                            break;
                    }
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
    }
    Route.Promo = Promo;
})(Route || (Route = {}));
module.exports = Route;

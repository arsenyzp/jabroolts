"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const EventBus = require("eventbusjs");
const UserModel_1 = require("../../../models/UserModel");
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const ManufactureModel_1 = require("../../../models/ManufactureModel");
const OrderModel_1 = require("../../../models/OrderModel");
const CancelParamsConfig_1 = require("../../../models/configs/CancelParamsConfig");
const PaymentModule_1 = require("../../../components/jabrool/PaymentModule");
const NotificationModule_1 = require("../../../components/NotificationModule");
const OrderHelper_1 = require("../../../components/jabrool/OrderHelper");
const Events_1 = require("../../../components/events/Events");
const PromoCodHelper_1 = require("../../../components/jabrool/PromoCodHelper");
const SocketServer_1 = require("../../../components/SocketServer");
const CreateOrderWebPost_1 = require("../postModels/CreateOrderWebPost");
const class_validator_1 = require("class-validator");
const CreateOrderHelper_1 = require("../../../components/jabrool/CreateOrderHelper");
const config_1 = require("../../../components/config");
var Route;
(function (Route) {
    class Orders {
        list(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                try {
                    let carModels = yield ManufactureModel_1.ManufactureModel.find({});
                    let cars = [];
                    carModels.map((v, k, a) => {
                        cars[v._id.toString()] = v.name;
                    });
                    let itemsActive = yield OrderModel_1.OrderModel
                        .find({
                        status: { $nin: [OrderModel_1.OrderStatuses.Missed, OrderModel_1.OrderStatuses.Finished, OrderModel_1.OrderStatuses.Canceled] },
                        owner: req.session.user._id
                    })
                        .populate({ path: "courier", model: UserModel_1.UserModel })
                        .populate({ path: "owner", model: UserModel_1.UserModel })
                        .sort({ created_at: -1 });
                    let ordersActive = [];
                    itemsActive.map((v, k, a) => {
                        let m = v.getApiFields();
                        m.vehicle = v.courier ? v.courier.getVehicle() : null;
                        if (m.vehicle !== null) {
                            m.vehicle.model = cars[m.vehicle.model];
                        }
                        m.courier = v.courier ? v.courier.getSiteFields() : null;
                        let lat_center = (v.owner_location.coordinates[0] + v.recipient_location.coordinates[0]) / 2;
                        let lon_center = (v.owner_location.coordinates[1] + v.recipient_location.coordinates[1]) / 2;
                        m.map = "https://maps.googleapis.com/maps/api/staticmap?center=" +
                            lon_center + "," + lat_center + "&zoom=13&size=320x150&maptype=roadmap" +
                            "&markers=color:blue%7Clabel:S%7C" +
                            v.owner_location.coordinates[1] + "," + v.owner_location.coordinates[0] +
                            "&markers=color:green%7Clabel:G%7C40.711614,-74.012318" +
                            "&markers=color:red%7Clabel:C%7C" +
                            v.recipient_location.coordinates[1] + "," + v.recipient_location.coordinates[0] +
                            "&key=" + config_1.AppConfig.getInstanse().get("gmap_key");
                        ordersActive.push(m);
                    });
                    let itemsCompleted = yield OrderModel_1.OrderModel
                        .find({
                        status: { $in: [OrderModel_1.OrderStatuses.Finished] },
                        owner: req.session.user._id
                    })
                        .populate({ path: "courier", model: UserModel_1.UserModel })
                        .populate({ path: "owner", model: UserModel_1.UserModel })
                        .sort({ created_at: -1 });
                    let ordersCompleted = [];
                    itemsCompleted.map((v, k, a) => {
                        let m = v.getApiFields();
                        m.vehicle = v.courier ? v.courier.getVehicle() : null;
                        m.courier = v.courier ? v.courier.getSiteFields() : null;
                        if (m.vehicle !== null) {
                            m.vehicle.model = cars[m.vehicle.model];
                        }
                        let lat_center = (v.owner_location.coordinates[0] + v.recipient_location.coordinates[0]) / 2;
                        let lon_center = (v.owner_location.coordinates[1] + v.recipient_location.coordinates[1]) / 2;
                        m.map = "https://maps.googleapis.com/maps/api/staticmap?center=" +
                            lon_center + "," + lat_center + "&zoom=13&size=320x150&maptype=roadmap" +
                            "&markers=color:blue%7Clabel:S%7C" +
                            v.owner_location.coordinates[1] + "," + v.owner_location.coordinates[0] +
                            "&markers=color:green%7Clabel:G%7C40.711614,-74.012318" +
                            "&markers=color:red%7Clabel:C%7C" +
                            v.recipient_location.coordinates[1] + "," + v.recipient_location.coordinates[0] +
                            "&key=" + config_1.AppConfig.getInstanse().get("gmap_key");
                        ordersCompleted.push(m);
                    });
                    let itemsCanceled = yield OrderModel_1.OrderModel
                        .find({
                        status: { $in: [OrderModel_1.OrderStatuses.Canceled, OrderModel_1.OrderStatuses.Missed] },
                        owner: req.session.user._id
                    })
                        .populate({ path: "courier", model: UserModel_1.UserModel })
                        .populate({ path: "owner", model: UserModel_1.UserModel })
                        .sort({ created_at: -1 });
                    let ordersCanceled = [];
                    itemsCanceled.map((v, k, a) => {
                        let m = v.getApiFields();
                        m.vehicle = v.courier ? v.courier.getVehicle() : null;
                        m.courier = v.courier ? v.courier.getSiteFields() : null;
                        if (m.vehicle !== null) {
                            m.vehicle.model = cars[m.vehicle.model];
                        }
                        let lat_center = (v.owner_location.coordinates[0] + v.recipient_location.coordinates[0]) / 2;
                        let lon_center = (v.owner_location.coordinates[1] + v.recipient_location.coordinates[1]) / 2;
                        m.map = "https://maps.googleapis.com/maps/api/staticmap?center=" +
                            lon_center + "," + lat_center + "&zoom=13&size=320x150&maptype=roadmap" +
                            "&markers=color:blue%7Clabel:S%7C" +
                            v.owner_location.coordinates[1] + "," + v.owner_location.coordinates[0] +
                            "&markers=color:green%7Clabel:G%7C40.711614,-74.012318" +
                            "&markers=color:red%7Clabel:C%7C" +
                            v.recipient_location.coordinates[1] + "," + v.recipient_location.coordinates[0] +
                            "&key=" + config_1.AppConfig.getInstanse().get("gmap_key");
                        ordersCanceled.push(m);
                    });
                    ajaxResponse.setDate({
                        ordersActive: ordersActive,
                        ordersCompleted: ordersCompleted,
                        ordersCanceled: ordersCanceled
                    });
                    res.json(ajaxResponse.get());
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
        cancelCost(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                try {
                    let params = new CancelParamsConfig_1.CancelParamsConfig();
                    let conf = yield params.getInstanse();
                    let order = yield OrderModel_1.OrderModel
                        .findOne({
                        _id: req.query.id,
                        owner: req.session.user._id
                    });
                    if (order === null) {
                        res.status(500);
                        ajaxResponse.addErrorMessage(req.__("site.errors.OrderNotFound"));
                        res.json(ajaxResponse.get());
                    }
                    else {
                        switch (order.status) {
                            case OrderModel_1.OrderStatuses.New:
                                ajaxResponse.setDate({ cost: 0 });
                                break;
                            case OrderModel_1.OrderStatuses.WaitCourierPickUpConfirmCode:
                            case OrderModel_1.OrderStatuses.WaitCustomerAccept:
                            case OrderModel_1.OrderStatuses.WaitCustomerPayment:
                            case OrderModel_1.OrderStatuses.WaitPickUp:
                                if (new Date().getTime() - order.accept_at <= 60 * 3 * 1000) {
                                    ajaxResponse.setDate({ cost: 0 });
                                }
                                else {
                                    ajaxResponse.setDate({ cost: conf.cost });
                                }
                                break;
                            case OrderModel_1.OrderStatuses.InProgress:
                            case OrderModel_1.OrderStatuses.WaitAcceptDelivery:
                                ajaxResponse.setDate({ cost: (order.cost / 2).toFixed(2) });
                                break;
                            default:
                                ajaxResponse.setDate({ cost: 0 });
                        }
                        res.json(ajaxResponse.get());
                    }
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
        cancel(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                try {
                    let params = new CancelParamsConfig_1.CancelParamsConfig();
                    let conf = yield params
                        .getInstanse();
                    let order = yield OrderModel_1.OrderModel
                        .findOne({
                        _id: req.query.id,
                        owner: req.session.user._id,
                        status: {
                            $nin: [
                                OrderModel_1.OrderStatuses.Canceled,
                                OrderModel_1.OrderStatuses.Finished,
                                OrderModel_1.OrderStatuses.WaitCourierReturnConfirmCode,
                                OrderModel_1.OrderStatuses.ReturnInProgress,
                                OrderModel_1.OrderStatuses.WaitAcceptReturn
                            ]
                        }
                    })
                        .populate({ path: "owner", model: UserModel_1.UserModel })
                        .populate({ path: "courier", model: UserModel_1.UserModel });
                    if (order === null) {
                        res.status(500);
                        ajaxResponse.addErrorMessage(req.__("site.errors.OrderNotFound"));
                        res.json(ajaxResponse.get());
                    }
                    else {
                        switch (order.status) {
                            case OrderModel_1.OrderStatuses.New:
                                {
                                    let orderCanceled = yield OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, {
                                        status: OrderModel_1.OrderStatuses.Canceled,
                                        owner_canceled: true
                                    }, { new: true });
                                    EventBus.dispatch(Events_1.EVENT_ORDER_REQUEST_CANCELED, this, {
                                        order: orderCanceled
                                    });
                                    if (order.code) {
                                        yield PromoCodHelper_1.PromoCodHelper
                                            .unblock(req.user, order.code);
                                    }
                                    order.status = OrderModel_1.OrderStatuses.Canceled;
                                }
                                break;
                            case OrderModel_1.OrderStatuses.WaitCourierPickUpConfirmCode:
                            case OrderModel_1.OrderStatuses.WaitCustomerAccept:
                            case OrderModel_1.OrderStatuses.WaitCustomerPayment:
                            case OrderModel_1.OrderStatuses.WaitPickUp:
                                if (new Date().getTime() - order.accept_at <= 60 * 3 * 1000) {
                                    let orderCanceled = yield OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, {
                                        status: OrderModel_1.OrderStatuses.Canceled,
                                        owner_canceled: true
                                    }, { new: true });
                                    EventBus.dispatch(Events_1.EVENT_ORDER_REQUEST_CANCELED, this, {
                                        order: orderCanceled
                                    });
                                    yield OrderHelper_1.OrderHelper
                                        .removeOrderFromCourier(orderCanceled);
                                    if (order.code) {
                                        yield PromoCodHelper_1.PromoCodHelper
                                            .unblock(req.user, order.code);
                                    }
                                    if (order.paid) {
                                        if (order.pay_type === "card") {
                                            yield PaymentModule_1.PaymentModule
                                                .moneyBack(order.owner._id, order.cost);
                                        }
                                        else if (order.pay_type === "cash") {
                                            if (order.bonus > 0) {
                                                yield PaymentModule_1.PaymentModule
                                                    .promoBonusAdd(order.owner._id, order.bonus);
                                            }
                                        }
                                        yield OrderModel_1.OrderModel
                                            .findOneAndUpdate({ _id: order._id }, { paid: false });
                                    }
                                    if (order.paid && order.userCredit !== 0) {
                                        yield UserModel_1.UserModel.findOneAndUpdate({ _id: order.owner._id }, { $inc: { balance: order.userCredit } });
                                        yield PaymentModule_1.PaymentModule
                                            .courierTakeUserDebt(order.courier._id, order.userCredit);
                                    }
                                }
                                else {
                                    yield PaymentModule_1.PaymentModule
                                        .paymentFromBalance(conf.cost, req.user);
                                    let orderCanceled = yield OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, {
                                        status: OrderModel_1.OrderStatuses.Canceled,
                                        owner_canceled: true
                                    }, { new: true });
                                    EventBus.dispatch(Events_1.EVENT_ORDER_REQUEST_CANCELED, this, {
                                        order: orderCanceled
                                    });
                                    yield OrderHelper_1.OrderHelper
                                        .removeOrderFromCourier(orderCanceled);
                                    if (order.paid && order.userCredit !== 0) {
                                        yield UserModel_1.UserModel.findOneAndUpdate({ _id: order.owner._id }, { $inc: { balance: order.userCredit } });
                                        yield PaymentModule_1.PaymentModule
                                            .courierTakeUserDebt(order.courier._id, order.userCredit);
                                    }
                                }
                                break;
                            case OrderModel_1.OrderStatuses.InProgress:
                            case OrderModel_1.OrderStatuses.WaitAcceptDelivery:
                                {
                                    let orderCanceled = yield OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, {
                                        status: OrderModel_1.OrderStatuses.ReturnInProgress,
                                        owner_canceled: true
                                    }, { new: true });
                                    if (orderCanceled.courier) {
                                        SocketServer_1.SocketServer.emit([orderCanceled.courier.toString()], "request_canceled", {
                                            order: orderCanceled.getPublicFields()
                                        });
                                    }
                                    order.status = OrderModel_1.OrderStatuses.ReturnInProgress;
                                }
                                break;
                        }
                        let orderUpdated = yield OrderModel_1.OrderModel
                            .findOne({
                            _id: order._id
                        })
                            .populate({ path: "owner", model: UserModel_1.UserModel })
                            .populate({ path: "courier", model: UserModel_1.UserModel });
                        if (orderUpdated.courier) {
                            if (orderUpdated.status === OrderModel_1.OrderStatuses.Canceled) {
                                NotificationModule_1.NotificationModule.getInstance().send([orderUpdated.courier._id], NotificationModule_1.NotificationTypes.RequestCancelled, {}, orderUpdated._id.toString());
                            }
                            if (orderUpdated.status === OrderModel_1.OrderStatuses.ReturnInProgress) {
                                NotificationModule_1.NotificationModule.getInstance().send([orderUpdated.owner._id], NotificationModule_1.NotificationTypes.RequestCancelledNeedReturn, { returnCost: orderUpdated.returnCost.toFixed(2) }, orderUpdated._id.toString());
                            }
                        }
                        ajaxResponse.setDate(orderUpdated.getApiFields());
                        res.json(ajaxResponse.get());
                    }
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
        saveOrder(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                let post = Object.create(CreateOrderWebPost_1.CreateOrderWebPost.prototype);
                Object.assign(post, req.body, {});
                class_validator_1.validate(post).then(errors => {
                    if (errors.length > 0) {
                        ajaxResponse.addValidationErrors(req, errors);
                        res.json(ajaxResponse.get());
                    }
                    else {
                        CreateOrderHelper_1.CreateOrderHelper.findRecipientWeb(req, res, post, ajaxResponse);
                    }
                });
            });
        }
    }
    Route.Orders = Orders;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const UserModel_1 = require("../../../models/UserModel");
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const PaymentModule_1 = require("../../../components/jabrool/PaymentModule");
const CardLabelModel_1 = require("../../../models/CardLabelModel");
var Route;
(function (Route) {
    class Payments {
        index(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            if (!req.session.user.bt_customer_id || req.session.user.bt_customer_id === "") {
                ajaxResponse.setDate([]);
                res.json(ajaxResponse.get());
                return;
            }
            PaymentModule_1.PaymentModule
                .listCards(req.session.user.bt_customer_id)
                .then(result => {
                let cards = [];
                if (result.creditCards.length > 0) {
                    let uids = [];
                    for (let i = 0; i < result.creditCards.length; i++) {
                        uids.push(result.creditCards[i].uniqueNumberIdentifier);
                    }
                    CardLabelModel_1.CardLabelModel
                        .find({ uniqueNumberIdentifier: { $in: uids } })
                        .then((labels) => {
                        let labels_arr = {};
                        for (let i = 0; i < labels.length; i++) {
                            labels_arr[labels[i].uniqueNumberIdentifier] = labels[i].label;
                        }
                        for (let i = 0; i < result.creditCards.length; i++) {
                            let label = "";
                            if (labels_arr[result.creditCards[i].uniqueNumberIdentifier]) {
                                label = labels_arr[result.creditCards[i].uniqueNumberIdentifier];
                            }
                            cards.push({
                                cardType: result.creditCards[i].cardType,
                                uniqueNumberIdentifier: result.creditCards[i].uniqueNumberIdentifier,
                                imageUrl: result.creditCards[i].imageUrl,
                                maskedNumber: result.creditCards[i].maskedNumber,
                                default: (req.session.user.pay_type === "card") ? result.creditCards[i].default : false,
                                label: label
                            });
                        }
                        ajaxResponse.setDate(cards);
                        res.json(ajaxResponse.get());
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                }
                else {
                    ajaxResponse.setDate([]);
                    res.json(ajaxResponse.get());
                    return;
                }
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        saveDefaultPayment(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                try {
                    UserModel_1.UserModel.findOneAndUpdate({ _id: req.session.user._id }, {
                        pay_type: req.body.type,
                        defaultUniqueNumberIdentifier: req.body.uniqueNumberIdentifier
                    }, { new: true })
                        .then(user => {
                        if (user.pay_type === "card" &&
                            req.body.uniqueNumberIdentifier &&
                            req.body.uniqueNumberIdentifier !== "" &&
                            user.bt_customer_id &&
                            user.bt_customer_id !== "") {
                            PaymentModule_1.PaymentModule.setDefault(req.session.user.bt_customer_id, req.body.uniqueNumberIdentifier).then(r => {
                                ajaxResponse.setDate(user.getApiFields());
                                res.json(ajaxResponse.get());
                                return;
                            }).catch(err => {
                                ajaxResponse.addErrorMessage(err.message);
                                res.status(500);
                                res.json(ajaxResponse.get());
                            });
                        }
                        else {
                            ajaxResponse.setDate(user.getApiFields());
                            res.json(ajaxResponse.get());
                            return;
                        }
                    })
                        .catch(err => {
                        ajaxResponse.addError(err);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    });
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
        saveCard(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                let promise;
                if (req.session.user.bt_customer_id) {
                    promise = PaymentModule_1.PaymentModule.createPayment(req.session.user.bt_customer_id, req.body.payment_method_nonce);
                }
                else {
                    promise = PaymentModule_1.PaymentModule.createCustomerAndPayment(req.session.user.first_name, req.session.user.last_name, req.session.user.email, req.session.user.phone, req.body.payment_method_nonce);
                }
                promise.then(result => {
                    console.log(result);
                    if (!result.success) {
                        ajaxResponse.addErrorMessage(result.message);
                        res.status(500);
                        res.json(ajaxResponse.get());
                    }
                    else {
                        let customerId = result.customer.id;
                        UserModel_1.UserModel.findOneAndUpdate({ _id: req.session.user._id }, { bt_customer_id: customerId })
                            .then(doc => {
                            let label = new CardLabelModel_1.CardLabelModel({
                                user: req.session.user._id,
                                uniqueNumberIdentifier: result.customer.creditCards[0].uniqueNumberIdentifier,
                                label: result.customer.creditCards[0].maskedNumber
                            });
                            label
                                .save()
                                .then(r => {
                                ajaxResponse.setDate({
                                    cardType: result.customer.creditCards[0].cardType,
                                    uniqueNumberIdentifier: result.customer.creditCards[0].uniqueNumberIdentifier,
                                    imageUrl: result.customer.creditCards[0].imageUrl,
                                    maskedNumber: result.customer.creditCards[0].maskedNumber,
                                    label: result.customer.creditCards[0].maskedNumber
                                });
                                res.json(ajaxResponse.get());
                                return;
                            })
                                .catch(err => {
                                ajaxResponse.addError(err);
                                res.status(500);
                                res.json(ajaxResponse.get());
                            });
                        })
                            .catch(err => {
                            ajaxResponse.addError(err);
                            res.status(500);
                            res.json(ajaxResponse.get());
                        });
                    }
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            });
        }
    }
    Route.Payments = Payments;
})(Route || (Route = {}));
module.exports = Route;

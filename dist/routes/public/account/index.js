"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const UserModel_1 = require("../../../models/UserModel");
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const NotificationModule_1 = require("../../../components/NotificationModule");
const ProfilePost_1 = require("../../../api/postModels/ProfilePost");
const SmsSender_1 = require("../../../components/jabrool/SmsSender");
const FileHelper_1 = require("../../../components/FileHelper");
const config_1 = require("../../../components/config");
const class_validator_1 = require("class-validator");
const EmailSender_1 = require("../../../components/jabrool/EmailSender");
const async = require("async");
const OrderModel_1 = require("../../../models/OrderModel");
const PaymentModule_1 = require("../../../components/jabrool/PaymentModule");
var Route;
(function (Route) {
    class Index {
        home(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let user = yield UserModel_1.UserModel.findOne({ _id: req.session.user._id });
                res.render("public/account/home", {
                    user: user.getApiPublicFields()
                });
            });
        }
        getToken(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            UserModel_1.UserModel
                .findOne({ _id: req.session.user._id })
                .then(u => {
                if (u == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                ajaxResponse.setDate(u);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        getProfile(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            UserModel_1.UserModel
                .findOne({ _id: req.session.user._id })
                .then(u => {
                if (u == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                ajaxResponse.setDate(u);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        jid(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                req.session.jid = req.query.jid;
                res.redirect("/");
            });
        }
        saveProfile(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                let post = Object.create(ProfilePost_1.ProfilePost.prototype);
                Object.assign(post, req.body, {});
                try {
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        ajaxResponse.addValidationErrors(req, errors);
                        res.json(ajaxResponse.get());
                    }
                    else {
                        post.phone = post.phone.replace(/\D/g, "");
                        let user = yield UserModel_1.UserModel
                            .findOne({ $or: [
                                { email: post.email, _id: { $ne: req.session.user._id } },
                                { phone: post.phone, _id: { $ne: req.session.user._id } }
                            ] });
                        if (user !== null) {
                            if (user.email === post.email) {
                                ajaxResponse.addErrorMessage(req.__("api.errors.EmailAlreadyUsedInService"));
                                res.status(500);
                                return res.json(ajaxResponse.get());
                            }
                            else if (user.phone === post.phone) {
                                ajaxResponse.addErrorMessage(req.__("api.errors.PhoneAlreadyUsedInService"));
                                res.status(500);
                                return res.json(ajaxResponse.get());
                            }
                        }
                        else {
                            user = yield UserModel_1.UserModel.findOne({ _id: req.session.user._id });
                            let new_data = {};
                            new_data.first_name = post.first_name;
                            new_data.last_name = post.last_name;
                            if (req.session.user.email !== post.email) {
                                new_data.email = post.email;
                                req.session.user.email = new_data.email;
                                new_data.confirm_email_code = user.generateConfirmEmail();
                                new_data.confirm_email = false;
                                req.session.user.confirm_email_code = new_data.confirm_email_code;
                                EmailSender_1.EmailSender.sendTemplateToUser(req.session.user, req.__("api.email.SubjectConfirmEmail"), {}, "confirm_email");
                            }
                            if (req.session.user.phone !== post.phone) {
                                new_data.tmp_phone = post.phone;
                                req.session.user.phone = post.phone;
                                new_data.confirm_sms_code = user.generateConfirmSmsCode();
                                req.session.user.confirm_sms_code = new_data.confirm_sms_code;
                                SmsSender_1.SmsSender.send(req.session.user.phone, req.__("api.sms.ConfirmCode", req.session.user.confirm_sms_code));
                                NotificationModule_1.NotificationModule.getInstance().send([req.session.user._id], NotificationModule_1.NotificationTypes.PhoneOtpSent, { id: req.session.user._id.toString() }, req.session.user._id.toString());
                            }
                            if (post.password) {
                                user.setPassword(post.password);
                                new_data.password = user.password;
                            }
                            async.waterfall([
                                    cb => {
                                    if (post.avatar && post.avatar !== req.session.user.avatar && post.avatar.length > 5) {
                                        FileHelper_1.FileHelper.move(config_1.AppConfig.getInstanse().get("files:user_uploads") + "/" + post.avatar, config_1.AppConfig.getInstanse().get("files:user_avatars") + "/" + post.avatar, err => {
                                            if (err) {
                                                cb(err);
                                            }
                                            else {
                                                new_data.avatar = post.avatar;
                                                cb();
                                            }
                                        });
                                    }
                                    else {
                                        cb();
                                    }
                                },
                                    cb => {
                                    UserModel_1.UserModel.findOneAndUpdate({ _id: req.session.user._id }, new_data, { new: true })
                                        .then(user => {
                                        ajaxResponse.setDate(user);
                                        cb();
                                    })
                                        .catch(err => {
                                        cb(err);
                                    });
                                }
                            ], err => {
                                if (err) {
                                    ajaxResponse.addError(err);
                                    res.status(500);
                                    res.json(ajaxResponse.get());
                                }
                                else {
                                    res.json(ajaxResponse.get());
                                }
                            });
                        }
                    }
                }
                catch (err) {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                }
            });
        }
        ordersHistory(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                let filter = {
                    $or: [
                        { owner: req.session.user._id }
                    ],
                    status: { $in: [
                            OrderModel_1.OrderStatuses.Finished
                        ] }
                };
                OrderModel_1.OrderModel
                    .find(filter)
                    .populate({ path: "courier", model: UserModel_1.UserModel })
                    .populate({ path: "recipient", model: UserModel_1.UserModel })
                    .populate({ path: "owner", model: UserModel_1.UserModel })
                    .sort({ created_at: -1 })
                    .then((orders) => {
                    let items = [];
                    for (let i = 0; i < orders.length; i++) {
                        let order = orders[i].getApiFields();
                        items.push(order);
                    }
                    ajaxResponse.setDate(items);
                    res.json(ajaxResponse.get());
                })
                    .catch((err) => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            });
        }
        getPaymentToken(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                PaymentModule_1.PaymentModule
                    .getToken()
                    .then(token => {
                    ajaxResponse.setDate({ token: token });
                    res.json(ajaxResponse.get());
                })
                    .catch(err => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            });
        }
    }
    Route.Index = Index;
})(Route || (Route = {}));
module.exports = Route;

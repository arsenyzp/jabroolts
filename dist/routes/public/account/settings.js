"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const UserModel_1 = require("../../../models/UserModel");
const AjaxResponse_1 = require("../../../components/AjaxResponse");
const FavoriteLocationModel_1 = require("../../../models/FavoriteLocationModel");
const HistoryLocationModel_1 = require("../../../models/HistoryLocationModel");
var Route;
(function (Route) {
    class Settings {
        getSettings(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            UserModel_1.UserModel
                .findOne({ _id: req.session.user._id })
                .then(u => {
                if (u == null) {
                    ajaxResponse.addErrorMessage("Item not found");
                    res.status(500);
                    return res.json(ajaxResponse.get());
                }
                ajaxResponse.setDate(u);
                res.json(ajaxResponse.get());
            })
                .catch(err => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
        getHistory(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let ajaxResponse = new AjaxResponse_1.AjaxResponse();
                HistoryLocationModel_1.HistoryLocationModel
                    .find({ user: req.session.user._id })
                    .sort({ created_at: -1 })
                    .then((its) => {
                    let items = [];
                    for (let i = 0; i < its.length; i++) {
                        items.push(its[i].getPublicFields());
                    }
                    ajaxResponse.setDate(items);
                    res.json(ajaxResponse.get());
                })
                    .catch((err) => {
                    ajaxResponse.addError(err);
                    res.status(500);
                    res.json(ajaxResponse.get());
                });
            });
        }
        getFavoriteLocation(req, res, next) {
            let ajaxResponse = new AjaxResponse_1.AjaxResponse();
            FavoriteLocationModel_1.FavoriteLocationModel
                .find({ user: req.session.user._id })
                .sort({ created_at: -1 })
                .then((its) => {
                let items = [];
                for (let i = 0; i < its.length; i++) {
                    items.push(its[i].getPublicFields());
                }
                ajaxResponse.setDate(items);
                res.json(ajaxResponse.get());
            })
                .catch((err) => {
                ajaxResponse.addError(err);
                res.status(500);
                res.json(ajaxResponse.get());
            });
        }
    }
    Route.Settings = Settings;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const SubscriberModel_1 = require("../../models/SubscriberModel");
const TermsConfig_1 = require("../../models/configs/TermsConfig");
var Route;
(function (Route) {
    class Index {
        index(req, res, next) {
            res.render("public/index");
        }
        register(req, res, next) {
            res.render("public/register");
        }
        download(req, res, next) {
            res.render("public/download");
        }
        about(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let conf = new TermsConfig_1.TermsConfig();
                conf
                    .getInstanse()
                    .then(c => {
                    res.render("public/about", {
                        privacy: c.privacy_en,
                        terms_courier: c.terms_courier_en,
                        terms_customer: c.terms_customer_en
                    });
                });
            });
        }
        pricing(req, res, next) {
            res.render("public/pricing");
        }
        logout(req, res, next) {
            req.session.user = null;
            res.redirect("/");
        }
        tmp(req, res, next) {
            res.render("public/tmp");
        }
        subscribe(req, res, next) {
            if (!!req.body.email) {
                let m = new SubscriberModel_1.SubscriberModel();
                m.email = req.body.email;
                m.name = req.body.name;
                m.text = "";
                m.phone = "";
                m.save().then(_ => {
                    res.json({});
                })
                    .catch(err => {
                    res.json({ "error": "" });
                });
            }
            res.json({ "error": "" });
        }
    }
    Route.Index = Index;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const UserModel_1 = require("./UserModel");
const mongoose_2 = require("../components/mongoose");
const OrderModel_1 = require("./OrderModel");
exports.PaymentTypes = {
    Invite: "invite",
    Promo: "promo",
    Refund: "refund",
    Fee: "fee",
    Payment: "payment",
    Refill: "refill",
    Order: "order",
    Admin: "admin"
};
exports.PaymentStatuses = {
    Init: "init",
    Success: "success",
    Fail: "fail",
};
exports.PaymentLogSchema = new mongoose_1.Schema({
    user: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    order: { type: mongoose_1.Schema.Types.ObjectId, ref: OrderModel_1.OrderModel },
    type: { type: String, default: exports.PaymentTypes.Payment },
    status: { type: String, default: exports.PaymentStatuses.Init },
    amount: { type: Number, default: 0 },
    bt_pay_id: { type: String, default: "" },
    bt_status: { type: String, default: "" },
    bt_type: { type: String, default: "" },
    bt_currencyIsoCode: { type: String, default: "" },
    bt_amount: { type: String, default: "" },
    bt_result_json: { type: String, default: "{}" },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.PaymentLogSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.PaymentLogModel = mongoose_2.ConnectionDb.getInstanse().model("PaymentLogModel", exports.PaymentLogSchema);

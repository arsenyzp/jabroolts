"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../components/mongoose");
const UserModel_1 = require("./UserModel");
exports.ServiceMessageSchema = new mongoose_1.Schema({
    mid: { type: String },
    sender: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    recipient: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    text: { type: String },
    is_read: { type: Boolean, default: false },
    is_view: { type: Boolean, default: false },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.ServiceMessageSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.ServiceMessageSchema.methods.getPublicFields = function () {
    return {
        id: this._id,
        mid: this.mid,
        is_read: this.is_read,
        is_view: this.is_view,
        sender: this.sender ? this.sender.toString() : "",
        recipient: this.recipient ? this.recipient.toString() : "",
        text: this.text ? this.text : "",
        created_at: this.created_at
    };
};
exports.ServiceMessageModel = mongoose_2.ConnectionDb.getInstanse().model("ServiceMessageModel", exports.ServiceMessageSchema);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const UserModel_1 = require("./UserModel");
const mongoose_2 = require("../components/mongoose");
const OrderModel_1 = require("./OrderModel");
exports.CourierBalanceTypes = {
    Promo: "promo",
    Order: "order",
    Admin: "admin",
    Jabrool: "jabrool"
};
exports.CourierBalanceLogModelSchema = new mongoose_1.Schema({
    user: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    order: { type: mongoose_1.Schema.Types.ObjectId, ref: OrderModel_1.OrderModel },
    type: { type: String, default: exports.CourierBalanceTypes.Order },
    amount: { type: Number, default: 0 },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.CourierBalanceLogModelSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.CourierBalanceLogModel = mongoose_2.ConnectionDb.getInstanse().model("CourierBalanceLogModel", exports.CourierBalanceLogModelSchema);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const UserModel_1 = require("./UserModel");
const mongoose_2 = require("../components/mongoose");
exports.PromoCodSchema = new mongoose_1.Schema({
    code: { type: String, trim: true },
    date_expiry: { type: Number },
    amount: { type: Number },
    is_used: { type: Boolean, default: false },
    is_blocked: { type: Boolean, default: false },
    owner: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    forCourier: { type: Boolean, default: false },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.PromoCodSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.PromoCodModel = mongoose_2.ConnectionDb.getInstanse().model("PromoCodModel", exports.PromoCodSchema);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const UserModel_1 = require("./UserModel");
const mongoose_2 = require("../components/mongoose");
exports.OnlineLogSchema = new mongoose_1.Schema({
    user: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    duration: { type: Number, default: 0 },
    type: { type: String, default: "no_orders" },
    location: {
        type: {
            type: String,
            default: "Point"
        },
        coordinates: [Number]
    },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.OnlineLogSchema.index({ location: "2dsphere" });
exports.OnlineLogSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.OnlineLogModel = mongoose_2.ConnectionDb.getInstanse().model("OnlineLogModel", exports.OnlineLogSchema);

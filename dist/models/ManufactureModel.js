"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../components/mongoose");
const CarTypeModel_1 = require("./CarTypeModel");
exports.ManufactureSchema = new mongoose_1.Schema({
    name: { type: String, trim: true },
    types: [
        { type: mongoose_1.Schema.Types.ObjectId, ref: CarTypeModel_1.CarTypeModel }
    ],
    created_at: { type: Number },
    updated_at: { type: Number }
});
exports.ManufactureSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.ManufactureSchema.methods.getPublicFields = function () {
    return {
        id: this._id,
        name: this.name,
        type: this.types
    };
};
exports.ManufactureModel = mongoose_2.ConnectionDb.getInstanse().model("ManufactureModel", exports.ManufactureSchema);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../components/mongoose");
exports.ConfigSchema = new mongoose_1.Schema({
    name: { type: String, trim: true },
    code: { type: String, trim: true },
    value: { type: String, trim: true },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.ConfigSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.ConfigModel = mongoose_2.ConnectionDb.getInstanse().model("ConfigModel", exports.ConfigSchema);

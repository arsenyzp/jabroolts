"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../components/mongoose");
const UserModel_1 = require("./UserModel");
const OrderModel_1 = require("./OrderModel");
const config_1 = require("../components/config");
exports.MessageSchema = new mongoose_1.Schema({
    mid: { type: String },
    sender: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    recipient: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    order: { type: mongoose_1.Schema.Types.ObjectId, ref: OrderModel_1.OrderModel },
    text: { type: String },
    image: { type: String },
    is_read: { type: Boolean, default: false },
    is_view: { type: Boolean, default: false },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.MessageSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.MessageSchema.methods.getPublicFields = function () {
    return {
        id: this._id,
        mid: this.mid,
        is_read: this.is_read,
        is_view: this.is_view,
        sender: this.sender ? this.sender.toString() : "",
        recipient: this.recipient ? this.recipient.toString() : "",
        order: this.order ? this.order.toString() : "",
        text: this.text ? this.text : "",
        image: this.image ? config_1.AppConfig.getInstanse().get("base_url") + config_1.AppConfig.getInstanse().get("urls:chat") + "/" + this.image : "",
        created_at: this.created_at
    };
};
exports.MessageModel = mongoose_2.ConnectionDb.getInstanse().model("MessageModel", exports.MessageSchema);

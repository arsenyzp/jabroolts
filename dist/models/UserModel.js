"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../components/mongoose");
const BankModel_1 = require("./BankModel");
const OrderModel_1 = require("./OrderModel");
const BankTypeModel_1 = require("./BankTypeModel");
const CarTypeModel_1 = require("./CarTypeModel");
const ManufactureModel_1 = require("./ManufactureModel");
const config_1 = require("../components/config");
const JidGenerator_1 = require("../components/jabrool/JidGenerator");
const md5 = require("blueimp-md5");
exports.UserTypes = {
    User: "user",
    Business: "business"
};
exports.UserStatus = {
    Active: "active",
    Review: "review",
    Deleted: "deleted"
};
exports.UserRoles = {
    dev: "dev",
    admin: "admin",
    manager: "manager",
    user: "user"
};
exports.UserSchema = new mongoose_1.Schema({
    jabroolid: { type: String, index: true, unique: [true, "This id already exists"], sparse: true },
    email: { type: String, index: true, unique: [true, "This email already exists"], sparse: true },
    first_name: { type: String, trim: true },
    last_name: { type: String, trim: true },
    phone: { type: String, trim: true },
    tmp_phone: { type: String, trim: true },
    avatar: { type: String, trim: true },
    password: { type: String, required: [true, "Password is require"] },
    tmp_password: { type: String },
    token: { type: String, trim: true, default: null },
    status: { type: String, default: exports.UserStatus.Review, enum: [exports.UserStatus.Active, exports.UserStatus.Review, exports.UserStatus.Deleted] },
    visible: { type: Boolean, default: false },
    balance: { type: Number, default: 0 },
    courierLimit: { type: Number, default: 0 },
    jabroolFee: { type: Number, default: 0 },
    role: {
        type: String, default: exports.UserRoles.user, enum: [
            exports.UserRoles.admin,
            exports.UserRoles.user,
            exports.UserRoles.dev
        ]
    },
    created_at: { type: Number },
    updated_at: { type: Number },
    login_at: { type: Number },
    push_device_ios: [
        { type: String, default: null }
    ],
    push_device_android: [
        { type: String, default: null }
    ],
    socket_ids: [
        { type: String }
    ],
    web_socket_ids: [
        { type: String }
    ],
    socket_admin_ids: [
        { type: String }
    ],
    tz: { type: Number, default: 0 },
    local: { type: String, default: "en" },
    duid: { type: String },
    confirm_sms_code: { type: String },
    confirm_phone: { type: Boolean, default: false },
    confirm_profile: { type: Boolean, default: false },
    confirm_email_code: { type: String },
    confirm_email: { type: Boolean, default: false },
    license: {
        image: { type: String, default: "" },
        number: { type: String, default: "" },
        name: { type: String, default: "" },
        issue_date: { type: Number },
        expiry_date: { type: Number },
        country_code: { type: String, default: "" }
    },
    vehicle: {
        year: { type: String, default: "" },
        model: { type: mongoose_1.Schema.Types.ObjectId, ref: ManufactureModel_1.ManufactureModel },
        type: { type: mongoose_1.Schema.Types.ObjectId, ref: CarTypeModel_1.CarTypeModel },
        number: { type: String, default: "" },
        image_front: { type: String, default: "" },
        image_back: { type: String, default: "" },
        image_side: { type: String, default: "" },
        fk_company: { type: String, default: "" },
        images_insurance: [
            { type: String }
        ]
    },
    bank: {
        type: { type: mongoose_1.Schema.Types.ObjectId, ref: BankTypeModel_1.BankTypeModel },
        bank: { type: mongoose_1.Schema.Types.ObjectId, ref: BankModel_1.BankModel },
        country_code: { type: String },
        branch: { type: String },
        account_number: { type: String },
        holder_name: { type: String }
    },
    referrer: { type: String },
    referals: [
        { type: String }
    ],
    current_orders: [
        { type: mongoose_1.Schema.Types.ObjectId, ref: OrderModel_1.OrderModel }
    ],
    in_progress: { type: Boolean, default: false },
    deleted: { type: Boolean, default: false },
    accept_in_progress: { type: Boolean, default: false },
    bt_customer_id: { type: String, default: "" },
    qo_customer_id: { type: Number, default: -1 },
    pay_type: { type: String, default: "cash" },
    defaultUniqueNumberIdentifier: { type: String, default: "" },
    tmp_code: { type: String, default: "#@!@" },
    rating: { type: Number, default: 5 },
    review_count: { type: Number, default: 0 },
    unread_count: { type: Number, default: 0 },
    unread_messages: { type: Number, default: 0 },
    location: {
        type: {
            type: String,
            default: "Point"
        },
        coordinates: [Number]
    },
    type: { type: String, default: exports.UserTypes.User },
    couriers: [
        { type: String }
    ]
});
exports.UserSchema.index({ location: "2dsphere" });
exports.UserSchema.methods.generateJId = function () {
    return JidGenerator_1.JidGenerator.gen(this);
};
exports.UserSchema.methods.genToken = function () {
    let d = new Date();
    this.token = md5((Math.random() * (999999 - 111111)) + "token") + md5(d.getTime() + this._id.toString()) + this._id.toString();
    return this.token;
};
exports.UserSchema.methods.gen = function () {
    let rand = 111111 + Math.random() * (999999 - 111111);
    rand = Math.round(rand);
    this.setPassword(rand + "");
    return rand + "";
};
exports.UserSchema.methods.setPassword = function (password) {
    this.password = md5(password);
};
exports.UserSchema.methods.comparePassword = function (password) {
    return this.password === md5(password);
};
exports.UserSchema.methods.generateConfirmSmsCode = function () {
    this.confirm_sms_code = Math.floor(111111 + Math.random() * (999999 - 111111));
    return this.confirm_sms_code;
};
exports.UserSchema.methods.generateConfirmEmail = function () {
    this.confirm_email_code = Math.floor(111111 + Math.random() * (999999 - 111111)) + this.token;
    return this.confirm_email_code;
};
exports.UserSchema.methods.getApiFields = function () {
    let complete_license = !!(this.license && this.license.number && this.license.number !== "");
    let complete_bank = !!(this.bank && this.bank.account_number && this.bank.account_number !== "");
    let complete_vehicle = !!(this.vehicle && this.vehicle.number && this.vehicle.number !== "");
    let avatar = this.avatar ?
        config_1.AppConfig.getInstanse().get("base_url") + config_1.AppConfig.getInstanse().get("urls:user_avatars") + "/" + this.avatar :
        config_1.AppConfig.getInstanse().get("base_url") + "/images/empty.png";
    return {
        id: this._id,
        token: this.token,
        email: this.email !== undefined ? this.email.toString() : "",
        phone: this.phone !== undefined ? this.phone.toString() : "",
        first_name: this.first_name !== undefined ? this.first_name.toString() : "",
        last_name: this.last_name !== undefined ? this.last_name.toString() : "",
        jabroolid: this.jabroolid !== undefined ? this.jabroolid.toString() : "",
        avatar: avatar,
        created_at: this.created_at,
        updated_at: this.updated_at,
        status: this.status,
        visible: this.visible,
        in_progress: this.in_progress,
        accept_in_progress: this.accept_in_progress,
        pay_type: this.pay_type,
        rating: this.rating,
        unread_count: this.unread_count,
        review_count: this.review_count,
        confirm_email: this.confirm_email,
        confirm_phone: this.confirm_phone,
        complete_license: complete_license,
        complete_bank: complete_bank,
        complete_vehicle: complete_vehicle,
        unread_messages: this.unread_messages
    };
};
exports.UserSchema.methods.getSiteFields = function () {
    let complete_license = !!(this.license && this.license.number && this.license.number !== "");
    let complete_bank = !!(this.bank && this.bank.account_number && this.bank.account_number !== "");
    let complete_vehicle = !!(this.vehicle && this.vehicle.number && this.vehicle.number !== "");
    let avatar = this.avatar ?
        config_1.AppConfig.getInstanse().get("base_url") + config_1.AppConfig.getInstanse().get("urls:user_avatars") + "/" + this.avatar :
        config_1.AppConfig.getInstanse().get("base_url") + "/images/empty.png";
    return {
        id: this._id,
        token: this.token,
        email: this.email !== undefined ? this.email.toString() : "",
        phone: this.phone !== undefined ? this.phone.toString() : "",
        first_name: this.first_name !== undefined ? this.first_name.toString() : "",
        last_name: this.last_name !== undefined ? this.last_name.toString() : "",
        jabroolid: this.jabroolid !== undefined ? this.jabroolid.toString() : "",
        avatar: avatar,
        created_at: this.created_at,
        updated_at: this.updated_at,
        status: this.status,
        visible: this.visible,
        in_progress: this.in_progress,
        accept_in_progress: this.accept_in_progress,
        pay_type: this.pay_type,
        rating: this.rating,
        unread_count: this.unread_count,
        review_count: this.review_count,
        confirm_email: this.confirm_email,
        confirm_phone: this.confirm_phone,
        complete_license: complete_license,
        complete_bank: complete_bank,
        complete_vehicle: complete_vehicle,
        unread_messages: this.unread_messages,
        vehicle: this.vehicle
    };
};
exports.UserSchema.methods.getApiPublicFields = function () {
    let complete_license = !!(this.license && this.license.number && this.license.number !== "");
    let complete_bank = !!(this.bank && this.bank.account_number && this.bank.account_number !== "");
    let complete_vehicle = !!(this.vehicle && this.vehicle.number && this.vehicle.number !== "");
    let avatar = this.avatar ?
        config_1.AppConfig.getInstanse().get("base_url") + config_1.AppConfig.getInstanse().get("urls:user_avatars") + "/" + this.avatar :
        config_1.AppConfig.getInstanse().get("base_url") + "/images/empty.png";
    return {
        id: this._id,
        email: this.email !== undefined ? this.email.toString() : "",
        phone: this.phone !== undefined ? this.phone.toString() : "",
        first_name: this.first_name !== undefined ? this.first_name.toString() : "",
        last_name: this.last_name !== undefined ? this.last_name.toString() : "",
        jabroolid: this.jabroolid !== undefined ? this.jabroolid.toString() : "",
        avatar: avatar,
        created_at: this.created_at,
        updated_at: this.updated_at,
        status: this.status,
        visible: this.visible,
        in_progress: this.in_progress,
        accept_in_progress: this.accept_in_progress,
        rating: this.rating,
        unread_count: this.unread_count,
        review_count: this.review_count,
        confirm_email: this.confirm_email,
        confirm_phone: this.confirm_phone,
        complete_license: complete_license,
        complete_bank: complete_bank,
        complete_vehicle: complete_vehicle,
        unread_messages: this.unread_messages
    };
};
exports.UserSchema.methods.getBank = function () {
    return this.bank;
};
exports.UserSchema.methods.getLicense = function () {
    return this.license;
};
exports.UserSchema.methods.getVehicle = function () {
    let image_front = this.vehicle.image_front ?
        config_1.AppConfig.getInstanse().get("base_url") + config_1.AppConfig.getInstanse().get("urls:user_vehicle") + "/" + this.vehicle.image_front :
        config_1.AppConfig.getInstanse().get("base_url") + "/images/empty.png";
    let image_back = this.vehicle.image_back ?
        config_1.AppConfig.getInstanse().get("base_url") + config_1.AppConfig.getInstanse().get("urls:user_vehicle") + "/" + this.vehicle.image_back :
        config_1.AppConfig.getInstanse().get("base_url") + "/images/empty.png";
    let image_side = this.vehicle.image_side ?
        config_1.AppConfig.getInstanse().get("base_url") + config_1.AppConfig.getInstanse().get("urls:user_vehicle") + "/" + this.vehicle.image_side :
        config_1.AppConfig.getInstanse().get("base_url") + "/images/empty.png";
    let images_insurance = [];
    for (let i = 0; i < this.vehicle.images_insurance.length; i++) {
        images_insurance.push(config_1.AppConfig.getInstanse().get("base_url") +
            config_1.AppConfig.getInstanse().get("urls:user_vehicle") +
            "/" + this.vehicle.images_insurance[i]);
    }
    return {
        year: this.vehicle.year,
        model: this.vehicle.model,
        type: this.vehicle.type,
        number: this.vehicle.number,
        image_front: image_front,
        image_back: image_back,
        image_side: image_side,
        fk_company: this.vehicle.fk_company,
        images_insurance: images_insurance
    };
};
exports.UserSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    if (!this.jabroolid) {
        JidGenerator_1.JidGenerator.gen(this).then(() => {
            next();
        }).catch(err => {
            next(err);
        });
    }
    else {
        next();
    }
});
exports.UserSchema.methods.fullName = function () {
    return (this.first_name.trim() + " " + this.last_name.trim());
};
exports.UserModel = mongoose_2.ConnectionDb.getInstanse().model("UserModel", exports.UserSchema);

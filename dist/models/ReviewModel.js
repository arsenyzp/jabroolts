"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../components/mongoose");
const UserModel_1 = require("./UserModel");
const OrderModel_1 = require("./OrderModel");
exports.ReviewModelSchema = new mongoose_1.Schema({
    owner: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    user: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    order: { type: mongoose_1.Schema.Types.ObjectId, ref: OrderModel_1.OrderModel },
    text: { type: String },
    rate: { type: Number },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.ReviewModelSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.ReviewModelSchema.methods.getPublicFields = function () {
    return {
        id: this._id,
        data_id: this.ref_id,
        type: this.type,
        text: this.text,
        created_at: this.created_at
    };
};
exports.ReviewModel = mongoose_2.ConnectionDb.getInstanse().model("ReviewModel", exports.ReviewModelSchema);

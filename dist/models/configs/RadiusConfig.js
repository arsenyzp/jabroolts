"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ConfigModel_1 = require("../ConfigModel");
class RadiusConfig {
    constructor() {
        this._key = "RadiusConfig";
        this._name = "RadiusConfig";
        this.request = 1000;
        this.feed = 1000;
        this.drop_off = 1000;
        this.close_radius = 1000;
    }
    save() {
        return new Promise((resolve, reject) => {
            ConfigModel_1.ConfigModel
                .findOne({ code: this._key })
                .then(config => {
                if (config == null) {
                    config = new ConfigModel_1.ConfigModel();
                    config.code = this._key;
                    config.name = this._name;
                }
                config.value = JSON.stringify({
                    request: this.request,
                    feed: this.feed,
                    drop_off: this.drop_off,
                    close_radius: this.close_radius
                });
                config
                    .save()
                    .then(model => {
                    resolve(model);
                })
                    .catch(err => {
                    reject(err);
                });
            })
                .catch(err => {
                reject(err);
            });
        });
    }
    getInstanse() {
        return new Promise((resolve, reject) => {
            ConfigModel_1.ConfigModel
                .findOne({ code: this._key })
                .then(config => {
                if (config == null) {
                    config = new ConfigModel_1.ConfigModel();
                    config.code = this._key;
                    config.name = this._name;
                    config.value = JSON.stringify({
                        request: this.request,
                        feed: this.feed,
                        drop_off: this.drop_off,
                        close_radius: this.close_radius
                    });
                }
                let value = JSON.parse(config.value);
                let item = Object.create(RadiusConfig.prototype);
                Object.assign(item, value, {});
                this.request = item.request;
                this.feed = item.feed;
                this.drop_off = item.drop_off;
                this.close_radius = item.close_radius;
                resolve(this);
            })
                .catch(err => {
                reject(err);
            });
        });
    }
}
exports.RadiusConfig = RadiusConfig;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ConfigModel_1 = require("../ConfigModel");
const cache = require("memory-cache");
class PackageRatioConfig {
    constructor() {
        this._key = "PackageRatioConfig";
        this._name = "PackageRatioConfig";
        this.small = 1;
        this.medium = 3;
        this.large = 7;
    }
    save() {
        return new Promise((resolve, reject) => {
            ConfigModel_1.ConfigModel
                .findOne({ code: this._key })
                .then(config => {
                if (config == null) {
                    config = new ConfigModel_1.ConfigModel();
                    config.code = this._key;
                    config.name = this._name;
                }
                config.value = JSON.stringify({
                    small: this.small,
                    medium: this.medium,
                    large: this.large
                });
                config
                    .save()
                    .then(model => {
                    cache.put(this._key, config.value, 500000);
                    resolve(model);
                })
                    .catch(err => {
                    reject(err);
                });
            })
                .catch(err => {
                reject(err);
            });
        });
    }
    getInstanse() {
        return new Promise((resolve, reject) => {
            let value = cache.get(this._key);
            if (value) {
                let item = Object.create(PackageRatioConfig.prototype);
                Object.assign(item, JSON.parse(value), {});
                this.small = item.small;
                this.medium = item.medium;
                this.large = item.large;
                resolve(this);
            }
            else {
                ConfigModel_1.ConfigModel
                    .findOne({ code: this._key })
                    .then(config => {
                    if (config == null) {
                        config = new ConfigModel_1.ConfigModel();
                        config.code = this._key;
                        config.name = this._name;
                        config.value = JSON.stringify({
                            small: this.small,
                            medium: this.medium,
                            large: this.large
                        });
                    }
                    cache.put(this._key, config.value, 500000);
                    let value = JSON.parse(config.value);
                    let item = Object.create(PackageRatioConfig.prototype);
                    Object.assign(item, value, {});
                    this.small = item.small;
                    this.medium = item.medium;
                    this.large = item.large;
                    resolve(this);
                })
                    .catch(err => {
                    reject(err);
                });
            }
        });
    }
}
exports.PackageRatioConfig = PackageRatioConfig;

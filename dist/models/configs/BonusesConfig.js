"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ConfigModel_1 = require("../ConfigModel");
class BonusesConfig {
    constructor() {
        this._key = "BonusesConfig";
        this._name = "BonusesConfig";
        this.inviteBonuses = 5;
        this.defaultCourierLimit = 100;
    }
    save() {
        return new Promise((resolve, reject) => {
            ConfigModel_1.ConfigModel
                .findOne({ code: this._key })
                .then(config => {
                if (config == null) {
                    config = new ConfigModel_1.ConfigModel();
                    config.code = this._key;
                    config.name = this._name;
                }
                config.value = JSON.stringify({
                    inviteBonuses: this.inviteBonuses,
                    defaultCourierLimit: this.defaultCourierLimit
                });
                config
                    .save()
                    .then(model => {
                    resolve(model);
                })
                    .catch(err => {
                    reject(err);
                });
            })
                .catch(err => {
                reject(err);
            });
        });
    }
    getInstanse() {
        return new Promise((resolve, reject) => {
            ConfigModel_1.ConfigModel
                .findOne({ code: this._key })
                .then(config => {
                if (config == null) {
                    config = new ConfigModel_1.ConfigModel();
                    config.code = this._key;
                    config.name = this._name;
                    config.value = JSON.stringify({
                        inviteBonuses: this.inviteBonuses,
                        defaultCourierLimit: this.defaultCourierLimit
                    });
                }
                let value = JSON.parse(config.value);
                let item = Object.create(BonusesConfig.prototype);
                Object.assign(item, value, {});
                this.inviteBonuses = item.inviteBonuses;
                this.defaultCourierLimit = item.defaultCourierLimit;
                resolve(this);
            })
                .catch(err => {
                reject(err);
            });
        });
    }
}
exports.BonusesConfig = BonusesConfig;

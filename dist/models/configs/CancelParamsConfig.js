"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ConfigModel_1 = require("../ConfigModel");
class CancelParamsConfig {
    constructor() {
        this._key = "CancelParamsConfig";
        this._name = "CancelParamsConfig";
        this.cost = 5;
    }
    save() {
        return new Promise((resolve, reject) => {
            ConfigModel_1.ConfigModel
                .findOne({ code: this._key })
                .then(config => {
                if (config == null) {
                    config = new ConfigModel_1.ConfigModel();
                    config.code = this._key;
                    config.name = this._name;
                }
                config.value = JSON.stringify({
                    cost: this.cost,
                });
                config
                    .save()
                    .then(model => {
                    resolve(model);
                })
                    .catch(err => {
                    reject(err);
                });
            })
                .catch(err => {
                reject(err);
            });
        });
    }
    getInstanse() {
        return new Promise((resolve, reject) => {
            ConfigModel_1.ConfigModel
                .findOne({ code: this._key })
                .then(config => {
                if (config == null) {
                    config = new ConfigModel_1.ConfigModel();
                    config.code = this._key;
                    config.name = this._name;
                    config.value = JSON.stringify({
                        cost: this.cost
                    });
                }
                let value = JSON.parse(config.value);
                let item = Object.create(CancelParamsConfig.prototype);
                Object.assign(item, value, {});
                this.cost = item.cost;
                resolve(this);
            })
                .catch(err => {
                reject(err);
            });
        });
    }
}
exports.CancelParamsConfig = CancelParamsConfig;

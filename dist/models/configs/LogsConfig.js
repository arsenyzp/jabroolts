"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ConfigModel_1 = require("../ConfigModel");
class LogsConfig {
    constructor() {
        this._key = "LogsConfig";
        this._name = "LogsConfig";
        this.api = false;
        this.error = false;
        this.sms = false;
        this.email = false;
    }
    save() {
        return new Promise((resolve, reject) => {
            ConfigModel_1.ConfigModel
                .findOne({ code: this._key })
                .then(config => {
                if (config == null) {
                    config = new ConfigModel_1.ConfigModel();
                    config.code = this._key;
                    config.name = this._name;
                }
                config.value = JSON.stringify({
                    api: this.api,
                    error: this.error,
                    sms: this.sms,
                    email: this.email
                });
                config
                    .save()
                    .then(model => {
                    resolve(model);
                })
                    .catch(err => {
                    reject(err);
                });
            })
                .catch(err => {
                reject(err);
            });
        });
    }
    getInstanse() {
        return new Promise((resolve, reject) => {
            ConfigModel_1.ConfigModel
                .findOne({ code: this._key })
                .then(config => {
                if (config == null) {
                    config = new ConfigModel_1.ConfigModel();
                    config.code = this._key;
                    config.name = this._name;
                    config.value = JSON.stringify({
                        api: this.api,
                        error: this.error,
                        sms: this.sms,
                        email: this.email
                    });
                }
                let value = JSON.parse(config.value);
                let model = Object.create(LogsConfig.prototype);
                Object.assign(model, value, {});
                this.api = model.api;
                this.error = model.error;
                this.sms = model.sms;
                this.email = model.email;
                resolve(this);
            })
                .catch(err => {
                reject(err);
            });
        });
    }
}
exports.LogsConfig = LogsConfig;

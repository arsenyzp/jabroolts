"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ConfigModel_1 = require("../ConfigModel");
class DeliveryConfig {
    constructor() {
        this._key = "DeliveryConfig";
        this._name = "DeliveryConfig";
        this.waiting_time = 120;
        this.average_speed = 100;
        this.missed_time = 120;
    }
    save() {
        return new Promise((resolve, reject) => {
            ConfigModel_1.ConfigModel
                .findOne({ code: this._key })
                .then(config => {
                if (config == null) {
                    config = new ConfigModel_1.ConfigModel();
                    config.code = this._key;
                    config.name = this._name;
                }
                config.value = JSON.stringify({
                    waiting_time: this.waiting_time,
                    average_speed: this.average_speed,
                    missed_time: this.missed_time
                });
                config
                    .save()
                    .then(model => {
                    resolve(model);
                })
                    .catch(err => {
                    reject(err);
                });
            })
                .catch(err => {
                reject(err);
            });
        });
    }
    getInstanse() {
        return new Promise((resolve, reject) => {
            ConfigModel_1.ConfigModel
                .findOne({ code: this._key })
                .then(config => {
                if (config == null) {
                    config = new ConfigModel_1.ConfigModel();
                    config.code = this._key;
                    config.name = this._name;
                    config.value = JSON.stringify({
                        waiting_time: this.waiting_time,
                        average_speed: this.average_speed,
                        missed_time: this.missed_time
                    });
                }
                let value = JSON.parse(config.value);
                let item = Object.create(DeliveryConfig.prototype);
                Object.assign(item, value, {});
                this.waiting_time = item.waiting_time;
                this.average_speed = item.average_speed;
                this.missed_time = item.missed_time;
                resolve(this);
            })
                .catch(err => {
                reject(err);
            });
        });
    }
}
exports.DeliveryConfig = DeliveryConfig;

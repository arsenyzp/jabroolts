"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ConfigModel_1 = require("../ConfigModel");
class PriceConfig {
    constructor() {
        this._key = "PriceConfig";
        this._name = "PriceConfig";
        this.e_percent = 20;
        this.j_percent = 20;
        this.e_fix_cost = 20;
        this.j_fix_cost = 20;
        this.e_min_cost = 20;
        this.j_min_cost = 20;
    }
    save() {
        return new Promise((resolve, reject) => {
            ConfigModel_1.ConfigModel
                .findOne({ code: this._key })
                .then(config => {
                if (config == null) {
                    config = new ConfigModel_1.ConfigModel();
                    config.code = this._key;
                    config.name = this._name;
                }
                config.value = JSON.stringify({
                    e_percent: this.e_percent,
                    j_percent: this.j_percent,
                    e_fix_cost: this.e_fix_cost,
                    j_fix_cost: this.j_fix_cost,
                    e_min_cost: this.e_min_cost,
                    j_min_cost: this.j_min_cost
                });
                config
                    .save()
                    .then(model => {
                    resolve(model);
                })
                    .catch(err => {
                    reject(err);
                });
            })
                .catch(err => {
                reject(err);
            });
        });
    }
    getInstanse() {
        return new Promise((resolve, reject) => {
            ConfigModel_1.ConfigModel
                .findOne({ code: this._key })
                .then(config => {
                if (config == null) {
                    config = new ConfigModel_1.ConfigModel();
                    config.code = this._key;
                    config.name = this._name;
                    config.value = JSON.stringify({
                        e_percent: this.e_percent,
                        j_percent: this.j_percent,
                        e_fix_cost: this.e_fix_cost,
                        j_fix_cost: this.j_fix_cost,
                        e_min_cost: this.e_min_cost,
                        j_min_cost: this.j_min_cost
                    });
                }
                let value = JSON.parse(config.value);
                let item = Object.create(PriceConfig.prototype);
                Object.assign(item, value, {});
                this.e_percent = item.e_percent;
                this.j_percent = item.j_percent;
                this.e_fix_cost = item.e_fix_cost;
                this.j_fix_cost = item.j_fix_cost;
                this.e_min_cost = item.e_min_cost;
                this.j_min_cost = item.j_min_cost;
                resolve(this);
            })
                .catch(err => {
                reject(err);
            });
        });
    }
}
exports.PriceConfig = PriceConfig;

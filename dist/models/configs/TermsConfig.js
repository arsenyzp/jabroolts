"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ConfigModel_1 = require("../ConfigModel");
class TermsConfig {
    constructor() {
        this._key = "TermsConfig";
        this._name = "TermsConfig";
        this.terms_courier_en = "";
        this.terms_courier_ar = "";
        this.terms_customer_en = "";
        this.terms_customer_ar = "";
        this.privacy_en = "";
        this.privacy_ar = "";
    }
    save() {
        return new Promise((resolve, reject) => {
            ConfigModel_1.ConfigModel
                .findOne({ code: this._key })
                .then(config => {
                if (config == null) {
                    config = new ConfigModel_1.ConfigModel();
                    config.code = this._key;
                    config.name = this._name;
                }
                config.value = JSON.stringify({
                    terms_courier_en: this.terms_courier_en,
                    terms_courier_ar: this.terms_courier_ar,
                    terms_customer_en: this.terms_customer_en,
                    terms_customer_ar: this.terms_customer_ar,
                    privacy_en: this.privacy_en,
                    privacy_ar: this.privacy_ar,
                });
                config
                    .save()
                    .then(model => {
                    resolve(model);
                })
                    .catch(err => {
                    reject(err);
                });
            })
                .catch(err => {
                reject(err);
            });
        });
    }
    getInstanse() {
        return new Promise((resolve, reject) => {
            ConfigModel_1.ConfigModel
                .findOne({ code: this._key })
                .then(config => {
                if (config == null) {
                    config = new ConfigModel_1.ConfigModel();
                    config.code = this._key;
                    config.name = this._name;
                    config.value = JSON.stringify({
                        terms_courier_en: this.terms_courier_en,
                        terms_courier_ar: this.terms_courier_ar,
                        terms_customer_en: this.terms_customer_en,
                        terms_customer_ar: this.terms_customer_ar,
                        privacy_en: this.privacy_en,
                        privacy_ar: this.privacy_ar
                    });
                }
                let value = JSON.parse(config.value);
                let item = Object.create(TermsConfig.prototype);
                Object.assign(item, value, {});
                this.terms_courier_en = item.terms_courier_en;
                this.terms_courier_ar = item.terms_courier_ar;
                this.terms_customer_en = item.terms_customer_en;
                this.terms_customer_ar = item.terms_customer_ar;
                this.privacy_en = item.privacy_en;
                this.privacy_ar = item.privacy_ar;
                resolve(this);
            })
                .catch(err => {
                reject(err);
            });
        });
    }
}
exports.TermsConfig = TermsConfig;

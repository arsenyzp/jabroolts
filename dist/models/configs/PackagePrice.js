"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class PackagePrice {
    constructor() {
        this.j_cost = 0;
        this.e_cost = 0;
        this.from_dimension = 0;
        this.to_dimension = 0;
        this.max_count = 0;
    }
}
exports.PackagePrice = PackagePrice;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const PackagePrice_1 = require("./PackagePrice");
const ConfigModel_1 = require("../ConfigModel");
class PackageConfigPrice {
    constructor() {
        this._key = "PackageConfigPrice";
        this._name = "PackageConfigPrice";
        this.small = new PackagePrice_1.PackagePrice();
        this.medium = new PackagePrice_1.PackagePrice();
        this.large = new PackagePrice_1.PackagePrice();
    }
    save() {
        return new Promise((resolve, reject) => {
            ConfigModel_1.ConfigModel
                .findOne({ code: this._key })
                .then(config => {
                if (config == null) {
                    config = new ConfigModel_1.ConfigModel();
                    config.code = this._key;
                    config.name = this._name;
                }
                config.value = JSON.stringify({
                    small: this.small,
                    medium: this.medium,
                    large: this.large
                });
                config
                    .save()
                    .then(model => {
                    resolve(model);
                })
                    .catch(err => {
                    reject(err);
                });
            })
                .catch(err => {
                reject(err);
            });
        });
    }
    getInstanse() {
        return new Promise((resolve, reject) => {
            ConfigModel_1.ConfigModel
                .findOne({ code: this._key })
                .then(config => {
                if (config == null) {
                    config = new ConfigModel_1.ConfigModel();
                    config.code = this._key;
                    config.name = this._name;
                    config.value = JSON.stringify({
                        small: this.small,
                        medium: this.medium,
                        large: this.large
                    });
                }
                let value = JSON.parse(config.value);
                let small = Object.create(PackagePrice_1.PackagePrice.prototype);
                Object.assign(small, value.small, {});
                this.small = small;
                let medium = Object.create(PackagePrice_1.PackagePrice.prototype);
                Object.assign(medium, value.medium, {});
                this.medium = medium;
                let large = Object.create(PackagePrice_1.PackagePrice.prototype);
                Object.assign(large, value.large, {});
                this.large = large;
                resolve(this);
            })
                .catch(err => {
                reject(err);
            });
        });
    }
}
exports.PackageConfigPrice = PackageConfigPrice;

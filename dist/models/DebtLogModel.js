"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../components/mongoose");
exports.DebtLogSchema = new mongoose_1.Schema({
    value: { type: Number, default: 0 },
    created_at: { type: Number },
    updated_at: { type: Number }
});
exports.DebtLogSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.DebtLogSchema.methods.getPublicFields = function () {
    return {
        id: this._id,
        name: this.name
    };
};
exports.DebtLogModel = mongoose_2.ConnectionDb.getInstanse().model("DebtLogModel", exports.DebtLogSchema);

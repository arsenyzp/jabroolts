"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../components/mongoose");
const UserModel_1 = require("./UserModel");
exports.HistoryLocationSchema = new mongoose_1.Schema({
    user: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    address: { type: String, trim: true },
    lat: { type: Number },
    lon: { type: Number },
    favorite: { type: Boolean, default: false },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.HistoryLocationSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.HistoryLocationSchema.methods.getPublicFields = function () {
    return {
        id: this._id,
        address: this.address,
        lat: this.lat,
        lon: this.lon,
        favorite: this.favorite,
        created_at: this.created_at
    };
};
exports.HistoryLocationModel = mongoose_2.ConnectionDb.getInstanse().model("HistoryLocationModel", exports.HistoryLocationSchema);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../components/mongoose");
exports.CarTypeSchema = new mongoose_1.Schema({
    name: { type: String, trim: true },
    size: { type: Number, default: 0 },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.CarTypeSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.CarTypeSchema.methods.getPublicFields = function () {
    return {
        id: this._id,
        name: this.name,
        size: this.size
    };
};
exports.CarTypeModel = mongoose_2.ConnectionDb.getInstanse().model("CarTypeModel", exports.CarTypeSchema);

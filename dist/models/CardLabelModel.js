"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../components/mongoose");
const UserModel_1 = require("./UserModel");
exports.CardLabelSchema = new mongoose_1.Schema({
    user: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    uniqueNumberIdentifier: { type: String, trim: true },
    label: { type: String, trim: true },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.CardLabelSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.CardLabelSchema.methods.getPublicFields = function () {
    return {
        id: this._id,
        label: this.label
    };
};
exports.CardLabelModel = mongoose_2.ConnectionDb.getInstanse().model("CardLabelModel", exports.CardLabelSchema);

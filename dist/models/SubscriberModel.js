"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../components/mongoose");
exports.SubscriberSchema = new mongoose_1.Schema({
    name: { type: String, trim: true },
    email: { type: String, trim: true },
    phone: { type: String, trim: true },
    text: { type: String, trim: true },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.SubscriberSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.SubscriberSchema.methods.getPublicFields = function () {
    return {
        id: this._id,
        name: this.name,
        email: this.email,
        phone: this.phone,
        text: this.text
    };
};
exports.SubscriberModel = mongoose_2.ConnectionDb.getInstanse().model("SubscriberModel", exports.SubscriberSchema);

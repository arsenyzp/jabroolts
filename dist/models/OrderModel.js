"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const UserModel_1 = require("./UserModel");
const PromoCodModel_1 = require("./PromoCodModel");
const mongoose_2 = require("../components/mongoose");
const config_1 = require("../components/config");
const MessageModel_1 = require("./MessageModel");
const SocketServer_1 = require("../components/SocketServer");
exports.OrderStatuses = {
    New: "new",
    WaitPickUp: "wait_pick_up",
    WaitCustomerAccept: "wait_customer_accept",
    WaitCustomerPayment: "wait_customer_payment",
    WaitCourierPickUpConfirmCode: "wait_courier_pick_up_confirm_code",
    WaitCourierDeliveryConfirmCode: "wait_courier_delivery_confirm_code",
    WaitCourierReturnConfirmCode: "wait_courier_return_confirm_code",
    InProgress: "in_progress",
    ReturnInProgress: "return_in_progress",
    WaitAcceptDelivery: "wait_accept_delivery",
    WaitAcceptReturn: "wait_accept_return",
    Canceled: "canceled",
    Finished: "finished",
    Missed: "missed"
};
exports.OrderTypes = {
    Jabrool: "jabrool",
    Express: "express"
};
exports.OrderSizes = {
    small: "small",
    medium: "medium",
    large: "large"
};
exports.OrderPayers = {
    Requester: "requester",
    Receiver: "receiver",
};
exports.OrderPayTypes = {
    Cash: "cash",
    Card: "card",
    Bonus: "bonus"
};
exports.OrderSchema = new mongoose_1.Schema({
    orderId: { type: String },
    owner: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    owner_name: { type: String },
    owner_contact: { type: String },
    courier: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    recipient: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    code: { type: mongoose_1.Schema.Types.ObjectId, ref: PromoCodModel_1.PromoCodModel },
    weight: { type: Number, default: 0 },
    small_package_count: { type: Number, default: 0 },
    medium_package_count: { type: Number, default: 0 },
    large_package_count: { type: Number, default: 0 },
    route: { type: Number, default: 0 },
    cost: { type: Number, default: 0 },
    returnCost: { type: Number, default: 0 },
    serviceFee: { type: Number, default: 0 },
    userCredit: { type: Number, default: 0 },
    card: { type: Number, default: 0 },
    cash: { type: Number, default: 0 },
    bonus: { type: Number, default: 0 },
    recipient_name: { type: String },
    recipient_contact: { type: String },
    owner_address: { type: String },
    recipient_address: { type: String },
    owner_comment: { type: String },
    recipient_comment: { type: String },
    owner_map_url: { type: String },
    recipient_map_url: { type: String },
    size: {
        type: String, enum: [
            exports.OrderSizes.small,
            exports.OrderSizes.medium,
            exports.OrderSizes.large
        ]
    },
    photos: [
        { type: String }
    ],
    status: {
        type: String, default: exports.OrderStatuses.New, enum: [
            exports.OrderStatuses.New,
            exports.OrderStatuses.WaitPickUp,
            exports.OrderStatuses.WaitCustomerAccept,
            exports.OrderStatuses.WaitCustomerPayment,
            exports.OrderStatuses.WaitCourierPickUpConfirmCode,
            exports.OrderStatuses.WaitCourierDeliveryConfirmCode,
            exports.OrderStatuses.WaitCourierReturnConfirmCode,
            exports.OrderStatuses.InProgress,
            exports.OrderStatuses.ReturnInProgress,
            exports.OrderStatuses.WaitAcceptDelivery,
            exports.OrderStatuses.WaitAcceptReturn,
            exports.OrderStatuses.Canceled,
            exports.OrderStatuses.Finished,
            exports.OrderStatuses.Missed
        ]
    },
    type: {
        type: String, default: exports.OrderTypes.Jabrool, enum: [
            exports.OrderTypes.Jabrool,
            exports.OrderTypes.Express
        ]
    },
    pay_type: {
        type: String, default: exports.OrderPayTypes.Card, enum: [
            exports.OrderPayTypes.Card,
            exports.OrderPayTypes.Cash
        ]
    },
    use_bonus: { type: Boolean, default: false },
    paid: { type: Boolean, default: false },
    paid_return: { type: Boolean, default: false },
    owner_canceled: { type: Boolean, default: false },
    owner_location: {
        type: {
            type: String,
            default: "Point"
        },
        coordinates: [Number]
    },
    recipient_location: {
        type: {
            type: String,
            default: "Point"
        },
        coordinates: [Number]
    },
    notified_couriers: [
        { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel }
    ],
    declined_couriers: [
        { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel }
    ],
    canceled_couriers: [
        { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel }
    ],
    confirm_pickup_code: { type: String },
    confirm_delivery_code: { type: String },
    confirm_start_return_code: { type: String },
    confirm_return_code: { type: String },
    accept_at: { type: Number },
    start_at: { type: Number },
    end_at: { type: Number },
    unread_messages_owner: { type: Number, default: 0 },
    unread_messages_courier: { type: Number, default: 0 },
    near_courier_notification: { type: Number, default: 0 },
    near_owner_notification: { type: Number, default: 0 },
    bt_pay_id: { type: String, default: "" },
    created_at: { type: Number },
    updated_at: { type: Number }
});
exports.OrderSchema.index({ owner_location: "2dsphere" });
exports.OrderSchema.index({ recipient_location: "2dsphere" });
exports.OrderSchema.pre("save", function (next) {
    console.log("-------OrderSchema.pre save----------");
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    if ([exports.OrderStatuses.Canceled, exports.OrderStatuses.Finished].indexOf(this.status) >= 0) {
        MessageModel_1.MessageModel
            .remove({ order: this._id })
            .then(_ => {
            console.log("Order messages update");
        })
            .catch(err => {
            console.log(err);
        });
    }
    next();
});
exports.OrderSchema.post("findOneAndUpdate", function (doc) {
    if (doc !== null) {
        console.log("-------OrderSchema.post findOneAndUpdate---------- " + doc.status);
        if ([exports.OrderStatuses.Canceled, exports.OrderStatuses.Finished].indexOf(doc.status) >= 0) {
            MessageModel_1.MessageModel
                .remove({ order: doc._id })
                .then(_ => {
                console.log("Order messages update");
                if (doc.courier) {
                    MessageModel_1.MessageModel.count({ recipient: doc.courier, is_view: false })
                        .then(c => {
                        UserModel_1.UserModel
                            .findOneAndUpdate({ _id: doc.courier }, { unread_messages: c }, { new: true })
                            .then(u => {
                            SocketServer_1.SocketServer.emit([doc.courier.toString()], "unread_count", { unread_count: c });
                        })
                            .catch(err => {
                            console.log(err);
                        });
                    })
                        .catch(err => {
                        console.log(err);
                    });
                }
                MessageModel_1.MessageModel.count({ recipient: doc.owner, is_view: false })
                    .then(c => {
                    UserModel_1.UserModel
                        .findOneAndUpdate({ _id: doc.owner }, { unread_messages: c }, { new: true })
                        .then(u => {
                        SocketServer_1.SocketServer.emit([doc.owner.toString()], "unread_count", { unread_count: c });
                    })
                        .catch(err => {
                        console.log(err);
                    });
                })
                    .catch(err => {
                    console.log(err);
                });
            })
                .catch(err => {
                console.log(err);
            });
        }
    }
});
exports.OrderSchema.methods.getPublicFields = function (user) {
    let photos = [];
    if (this.photos && this.photos.length > 0) {
        for (let i = 0; i < this.photos.length; i++) {
            photos.push(config_1.AppConfig.getInstanse().get("base_url") + config_1.AppConfig.getInstanse().get("urls:order_files") + "/" + this.photos[i]);
        }
    }
    let unread_messages = 0;
    if (user && user._id) {
        if (this.owner && this.owner._id) {
            if (this.owner._id.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_owner;
            }
        }
        else if (this.owner) {
            if (this.owner.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_owner;
            }
        }
        if (this.courier && this.courier._id) {
            if (this.courier._id.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_courier;
            }
        }
        else if (this.courier) {
            if (this.courier.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_courier;
            }
        }
    }
    return {
        id: this._id,
        orderId: this.orderId,
        owner: (!!this.owner && !!this.owner._id) ? this.owner._id : this.owner,
        courier: (!!this.courier && !!this.courier._id) ? this.courier._id : this.courier,
        weight: this.weight,
        small_package_count: this.small_package_count,
        medium_package_count: this.medium_package_count,
        large_package_count: this.large_package_count,
        size: this.size,
        photos: photos,
        status: this.status,
        type: this.type,
        owner_name: this.owner_name,
        owner_contact: this.owner_contact,
        owner_address: this.owner_address,
        owner_comment: this.owner_comment,
        owner_location: this.owner_location.coordinates,
        recipient_address: this.recipient_address,
        recipient_comment: this.recipient_comment,
        recipient_location: this.recipient_location.coordinates,
        recipient_name: this.recipient_name,
        recipient_contact: this.recipient_contact,
        created_at: this.created_at,
        start_at: this.start_at,
        end_at: this.end_at,
        paid: this.paid,
        paid_return: this.paid_return,
        pay_type: this.pay_type,
        route: this.route,
        cost: this.cost,
        serviceFee: this.serviceFee,
        userCredit: this.userCredit * -1,
        returnCost: this.returnCost,
        card: this.card,
        cash: this.cash,
        bonus: this.bonus,
        updated_at: this.updated_at,
        use_bonus: this.use_bonus,
        owner_canceled: this.owner_canceled,
        unread_messages: unread_messages
    };
};
exports.OrderSchema.methods.getApiFields = function (user) {
    let photos = [];
    if (this.photos && this.photos.length > 0) {
        for (let i = 0; i < this.photos.length; i++) {
            photos.push(config_1.AppConfig.getInstanse().get("base_url") + config_1.AppConfig.getInstanse().get("urls:order_files") + "/" + this.photos[i]);
        }
    }
    let unread_messages = 0;
    if (user && user._id) {
        if (this.owner && this.owner._id) {
            if (this.owner._id.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_owner;
            }
        }
        else if (this.owner) {
            if (this.owner.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_owner;
            }
        }
        if (this.courier && this.courier._id) {
            if (this.courier._id.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_courier;
            }
        }
        else if (this.courier) {
            if (this.courier.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_courier;
            }
        }
    }
    return {
        order: {
            id: this._id,
            orderId: this.orderId,
            weight: this.weight,
            small_package_count: this.small_package_count,
            medium_package_count: this.medium_package_count,
            large_package_count: this.large_package_count,
            size: this.size,
            photos: photos,
            status: this.status,
            type: this.type,
            owner_name: this.owner_name,
            owner_contact: this.owner_contact,
            owner_address: this.owner_address,
            owner_comment: this.owner_comment,
            owner_location: this.owner_location.coordinates,
            recipient_address: this.recipient_address,
            recipient_comment: this.recipient_comment,
            recipient_location: this.recipient_location.coordinates,
            recipient_name: this.recipient_name,
            recipient_contact: this.recipient_contact,
            created_at: this.created_at,
            start_at: this.start_at,
            end_at: this.end_at,
            paid: this.paid,
            paid_return: this.paid_return,
            pay_type: this.pay_type,
            route: this.route,
            cost: this.cost,
            serviceFee: this.serviceFee,
            userCredit: this.userCredit * -1,
            returnCost: this.returnCost,
            card: this.card,
            cash: this.cash,
            bonus: this.bonus,
            updated_at: this.updated_at,
            use_bonus: this.use_bonus,
            owner_canceled: this.owner_canceled,
            unread_messages: unread_messages,
            owner: (!!this.owner && !!this.owner._id) ? this.owner._id.toString() : this.owner,
            courier: (!!this.courier && !!this.courier._id) ? this.courier._id.toString() : this.courier,
            recipient: (!!this.recipient && !!this.recipient._id) ? this.recipient._id.toString() : this.recipient
        },
        owner: (!!this.owner && !!this.owner._id) ? this.owner.getApiPublicFields() : this.owner,
        courier: (!!this.courier && !!this.courier._id) ? this.courier.getApiPublicFields() : this.courier,
        recipient: (!!this.recipient && !!this.recipient._id) ? this.recipient.getApiPublicFields() : this.recipient
    };
};
exports.OrderSchema.methods.getApiCourierFields = function (user) {
    let photos = [];
    if (this.photos && this.photos.length > 0) {
        for (let i = 0; i < this.photos.length; i++) {
            photos.push(config_1.AppConfig.getInstanse().get("base_url") + config_1.AppConfig.getInstanse().get("urls:order_files") + "/" + this.photos[i]);
        }
    }
    let unread_messages = 0;
    if (user && user._id) {
        if (this.owner && this.owner._id) {
            if (this.owner._id.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_owner;
            }
        }
        else if (this.owner) {
            if (this.owner.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_owner;
            }
        }
        if (this.courier && this.courier._id) {
            if (this.courier._id.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_courier;
            }
        }
        else if (this.courier) {
            if (this.courier.toString() === user._id.toString()) {
                unread_messages = this.unread_messages_courier;
            }
        }
    }
    return {
        order: {
            id: this._id,
            orderId: this.orderId,
            weight: this.weight,
            small_package_count: this.small_package_count,
            medium_package_count: this.medium_package_count,
            large_package_count: this.large_package_count,
            size: this.size,
            photos: photos,
            status: this.status,
            type: this.type,
            owner_name: this.owner_name,
            owner_contact: this.owner_contact,
            owner_address: this.owner_address,
            owner_comment: this.owner_comment,
            owner_location: this.owner_location.coordinates,
            recipient_address: this.recipient_address,
            recipient_comment: this.recipient_comment,
            recipient_location: this.recipient_location.coordinates,
            recipient_name: this.recipient_name,
            recipient_contact: this.recipient_contact,
            created_at: this.created_at,
            start_at: this.start_at,
            end_at: this.end_at,
            paid: this.paid,
            paid_return: this.paid_return,
            pay_type: this.pay_type,
            route: this.route,
            cost: this.cost - this.serviceFee,
            serviceFee: this.serviceFee,
            userCredit: this.userCredit * -1,
            returnCost: this.returnCost,
            card: this.card,
            cash: this.cash,
            bonus: this.bonus,
            updated_at: this.updated_at,
            use_bonus: this.use_bonus,
            owner_canceled: this.owner_canceled,
            unread_messages: unread_messages,
            owner: (!!this.owner && !!this.owner._id) ? this.owner._id.toString() : this.owner,
            courier: (!!this.courier && !!this.courier._id) ? this.courier._id.toString() : this.courier,
            recipient: (!!this.recipient && !!this.recipient._id) ? this.recipient._id.toString() : this.recipient
        },
        owner: (!!this.owner && !!this.owner._id) ? this.owner.getApiPublicFields() : this.owner,
        courier: (!!this.courier && !!this.courier._id) ? this.courier.getApiPublicFields() : this.courier,
        recipient: (!!this.recipient && !!this.recipient._id) ? this.recipient.getApiPublicFields() : this.recipient
    };
};
exports.OrderModel = mongoose_2.ConnectionDb.getInstanse().model("OrderModel", exports.OrderSchema);

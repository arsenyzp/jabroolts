"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../components/mongoose");
exports.CountrySchema = new mongoose_1.Schema({
    name: { type: String, trim: true },
    code: { type: String, trim: true },
    phone: { type: String, trim: true },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.CountrySchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.CountrySchema.methods.getPublicFields = function () {
    return {
        id: this._id,
        name: this.name,
        code: this.code,
        phone: this.phone
    };
};
exports.CountryModel = mongoose_2.ConnectionDb.getInstanse().model("CountryModel", exports.CountrySchema);

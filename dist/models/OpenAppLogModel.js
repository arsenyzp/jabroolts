"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const UserModel_1 = require("./UserModel");
const mongoose_2 = require("../components/mongoose");
exports.OpenAppLogSchema = new mongoose_1.Schema({
    user: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    duration: { type: Number, default: 0 },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.OpenAppLogSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.OpenAppLogModel = mongoose_2.ConnectionDb.getInstanse().model("OpenAppLogModel", exports.OpenAppLogSchema);

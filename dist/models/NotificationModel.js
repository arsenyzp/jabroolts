"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const UserModel_1 = require("./UserModel");
const mongoose_2 = require("../components/mongoose");
exports.NotificationSchema = new mongoose_1.Schema({
    user: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    type: { type: String },
    ref_id: { type: String },
    text: { type: String },
    title: { type: String },
    is_view: { type: Boolean, default: false },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.NotificationSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.NotificationSchema.methods.getPublicFields = function () {
    return {
        id: this._id,
        data_id: this.ref_id,
        type: this.type,
        text: this.text,
        title: this.title,
        created_at: this.created_at,
        is_view: this.is_view
    };
};
exports.NotificationModel = mongoose_2.ConnectionDb
    .getInstanse()
    .model("NotificationModel", exports.NotificationSchema);

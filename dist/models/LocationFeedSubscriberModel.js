"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../components/mongoose");
const UserModel_1 = require("./UserModel");
exports.LocationFeedSubscriberSchema = new mongoose_1.Schema({
    user: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    location: {
        type: {
            type: String,
            default: "Point"
        },
        coordinates: [Number]
    },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.LocationFeedSubscriberSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.LocationFeedSubscriberSchema.index({ location: "2dsphere" });
exports.LocationFeedSubscriberSchema.methods.getPublicFields = function () {
    return {
        id: this._id,
        user: this.user,
        location: this.location
    };
};
exports.LocationFeedSubscriberModel = mongoose_2.ConnectionDb.getInstanse().model("LocationFeedSubscriberModel", exports.LocationFeedSubscriberSchema);

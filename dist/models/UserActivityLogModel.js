"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../components/mongoose");
exports.UserActivityLogSchema = new mongoose_1.Schema({
    customers: { type: Number, default: 0 },
    couriers: { type: Number, default: 0 },
    created_at: { type: Number },
    updated_at: { type: Number }
});
exports.UserActivityLogSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.UserActivityLogSchema.methods.getPublicFields = function () {
    return {
        id: this._id,
        customers: this.customers,
        couriers: this.couriers
    };
};
exports.UserActivityLogModel = mongoose_2.ConnectionDb.getInstanse().model("UserActivityLogModel", exports.UserActivityLogSchema);

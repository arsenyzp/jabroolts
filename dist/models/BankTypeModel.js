"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../components/mongoose");
exports.BankTypeSchema = new mongoose_1.Schema({
    name: { type: String, trim: true },
    created_at: { type: Number },
    updated_at: { type: Number }
});
exports.BankTypeSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.BankTypeSchema.methods.getPublicFields = function () {
    return {
        id: this._id,
        name: this.name
    };
};
exports.BankTypeModel = mongoose_2.ConnectionDb.getInstanse().model("BankTypeModel", exports.BankTypeSchema);

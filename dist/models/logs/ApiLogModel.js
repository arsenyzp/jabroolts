"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../../components/mongoose");
exports.ApiLogSchema = new mongoose_1.Schema({
    method: { type: String, trim: true },
    params: { type: String, trim: true },
    body: { type: String, trim: true },
    headers: { type: String, trim: true },
    url: { type: String, trim: true },
    query: { type: String, trim: true },
    rawHeaders: { type: String, trim: true },
    path: { type: String, trim: true },
    status: { type: String, trim: true },
    response: { type: String, trim: true },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.ApiLogSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.ApiLogModel = mongoose_2.ConnectionDb.getInstanse().model("ApiLogModel", exports.ApiLogSchema);

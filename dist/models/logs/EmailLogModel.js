"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../../components/mongoose");
exports.EmailLogSchema = new mongoose_1.Schema({
    email: { type: String, trim: true },
    subject: { type: String, trim: true },
    text: { type: String, trim: true },
    html: { type: String, trim: true },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.EmailLogSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.EmailLogModel = mongoose_2.ConnectionDb.getInstanse().model("EmailLogModel", exports.EmailLogSchema);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../../components/mongoose");
exports.SmsLogSchema = new mongoose_1.Schema({
    phone: { type: String, trim: true },
    text: { type: String, trim: true },
    mid: { type: String, trim: true },
    cost: { type: String, trim: true },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.SmsLogSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.SmsLogModel = mongoose_2.ConnectionDb.getInstanse().model("SmsLogModel", exports.SmsLogSchema);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const UserModel_1 = require("./UserModel");
const mongoose_2 = require("../components/mongoose");
const OrderModel_1 = require("./OrderModel");
exports.PaymentStatuses = {
    Init: "init",
    Success: "success",
    Fail: "fail",
};
exports.PaymentCustomerNewLogSchema = new mongoose_1.Schema({
    customer: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    order: { type: mongoose_1.Schema.Types.ObjectId, ref: OrderModel_1.OrderModel },
    status: { type: String, default: exports.PaymentStatuses.Init },
    amount: { type: Number, default: 0 },
    payment_type: { type: String, default: "" },
    payment_method: { type: String, default: "" },
    customerDebt: { type: Number, default: 0 },
    customerBonus: { type: Number, default: 0 },
    created_at: { type: Number },
    updated_at: { type: Number },
});
exports.PaymentCustomerNewLogSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.PaymentCustomerNewLogModel = mongoose_2.ConnectionDb.getInstanse().model("PaymentCustomerNewLog", exports.PaymentCustomerNewLogSchema);

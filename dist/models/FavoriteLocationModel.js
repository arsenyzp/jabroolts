"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose_2 = require("../components/mongoose");
const HistoryLocationModel_1 = require("./HistoryLocationModel");
const UserModel_1 = require("./UserModel");
exports.FavoriteLocationSchema = new mongoose_1.Schema({
    user: { type: mongoose_1.Schema.Types.ObjectId, ref: UserModel_1.UserModel },
    history_id: { type: mongoose_1.Schema.Types.ObjectId, ref: HistoryLocationModel_1.HistoryLocationModel },
    address: { type: String, trim: true },
    name: { type: String, trim: true },
    lat: { type: Number },
    lon: { type: Number },
    created_at: { type: Number },
    updated_at: { type: Number }
});
exports.FavoriteLocationSchema.pre("save", function (next) {
    if (!this.created_at) {
        this.created_at = new Date().getTime();
    }
    this.updated_at = new Date().getTime();
    next();
});
exports.FavoriteLocationSchema.methods.getPublicFields = function () {
    return {
        id: this._id,
        history_id: this.history_id,
        address: this.address,
        lat: this.lat,
        lon: this.lon,
        name: this.name,
        created_at: this.created_at
    };
};
exports.FavoriteLocationModel = mongoose_2.ConnectionDb.getInstanse().model("FavoriteLocationModel", exports.FavoriteLocationSchema);

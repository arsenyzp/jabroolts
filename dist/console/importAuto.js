"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const async = require("async");
const csvtojson = require("csvtojson");
const ManufactureModel_1 = require("../models/ManufactureModel");
const CarTypeModel_1 = require("../models/CarTypeModel");
const data = {};
const csvFilePath = __dirname + "/../../data/csv_data.csv";
csvtojson()
    .fromFile(csvFilePath)
    .on("json", (jsonObj) => {
    if (!data[jsonObj.make]) {
        data[jsonObj.make] = [];
    }
    data[jsonObj.make].push(jsonObj.model);
})
    .on("done", (error) => {
    console.log("end");
    console.log(data);
    let keys = Object.keys(data);
    async.forEachOfSeries(keys, (v, k, cb) => {
        let model = new ManufactureModel_1.ManufactureModel();
        model.name = v;
        model.save().then(doc => {
            let ar = data[v].filter((v, i, a) => a.indexOf(v) === i);
            async.forEachOfSeries(ar, (v, k, call) => {
                let type = new CarTypeModel_1.CarTypeModel();
                type.name = v.toString();
                type.size = 350;
                type.save().then(t => {
                    doc.types.push(t._id);
                    call();
                }).catch(err => {
                    call(err);
                });
            }, err => {
                doc.save().then(_ => { cb(err); }).catch(err => {
                    cb(err);
                });
            });
        }).catch(err => {
            cb(err);
        });
    }, err => {
        console.log(err);
    });
});

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Log_1 = require("../components/Log");
const UserActivityLogModel_1 = require("../models/UserActivityLogModel");
const RandNumber_1 = require("../components/RandNumber");
const log = Log_1.Log.getInstanse()(module);
let date = new Date();
for (let i = 400; i > 0; i--) {
    date.setUTCHours(date.getUTCHours() - 1);
    let m = new UserActivityLogModel_1.UserActivityLogModel();
    m.customers = RandNumber_1.RandNumber.ri(0, 100);
    m.couriers = RandNumber_1.RandNumber.ri(0, 100);
    m.created_at = date.getTime();
    m.save();
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const UserModel_1 = require("../models/UserModel");
const Log_1 = require("../components/Log");
const log = Log_1.Log.getInstanse()(module);
const user = new UserModel_1.UserModel();
user.first_name = "admin1";
user.last_name = "Admin";
user.phone = "111222333";
user.email = "admin@devapp.pro";
user.role = "admin";
user.setPassword("123456");
log.info(user.password);
user.location.coordinates = [0, 0];
user.save().then(res => {
    log.info(res);
    process.exit(0);
}).catch(err => {
    log.error(err);
    process.exit(0);
});

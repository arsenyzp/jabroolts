"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Log_1 = require("../components/Log");
const OrderModel_1 = require("../models/OrderModel");
const OrderHelper_1 = require("../components/jabrool/OrderHelper");
const log = Log_1.Log.getInstanse()(module);
OrderModel_1.OrderModel.find({}).then(orders => {
    orders.map((v, k, a) => {
        if (!v.orderId) {
            OrderHelper_1.OrderHelper.generateOrderId(v.type).then(id => {
                v.orderId = id + OrderHelper_1.OrderHelper.getRandCharset();
                v.save();
            });
        }
    });
});

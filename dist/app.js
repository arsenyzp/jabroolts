"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const express = require("express");
const path = require("path");
const validator = require("express-validator");
const cron = require("cron");
const i18n = require("i18n");
const indexRoute = require("./routes/index");
const mongoose_1 = require("./components/mongoose");
const config_1 = require("./components/config");
const StartDelivery_1 = require("./components/cron/StartDelivery");
const MissedOrder_1 = require("./components/cron/MissedOrder");
const AuthEvents_1 = require("./components/events/AuthEvents");
const CancelOrderEvents_1 = require("./components/events/CancelOrderEvents");
const CalculateDebt_1 = require("./components/cron/CalculateDebt");
const UserActivity_1 = require("./components/cron/UserActivity");
const ClearSocket_1 = require("./components/cron/ClearSocket");
const PayOrderEvent_1 = require("./components/events/PayOrderEvent");
const FinishOrderEvent_1 = require("./components/events/FinishOrderEvent");
const CurrencyComponent_1 = require("./components/CurrencyComponent");
const MongoStore = require("connect-mongo")(session);
class Server {
    static bootstrap() {
        return new Server();
    }
    constructor() {
        this.app = express();
        this.config();
        this.routes();
        this.cron();
        this.eventBusInit();
    }
    config() {
        this.app.set("views", path.join(__dirname, "../src/views"));
        this.app.set("view engine", "ejs");
        this.app.use(bodyParser.json({ limit: "10mb" }));
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(cookieParser());
        i18n.configure({
            locales: ["en", "ar"],
            directory: __dirname + "/../locales",
            defaultLocale: "en",
            queryParameter: "lang",
            objectNotation: true
        });
        this.app.use(i18n.init);
        this.app.use(express.static(path.join(__dirname, "../public")));
        this.app.use(express.static(path.join(__dirname, "../")));
        this.app.use(express.static(path.join("/mnt/data001/public")));
        this.app.use(validator({}));
        this.app.use(session({
            secret: config_1.AppConfig.getInstanse().get("secret"),
            resave: true,
            saveUninitialized: true,
            cookie: { secure: false },
            store: new MongoStore({ mongooseConnection: mongoose_1.ConnectionDb.getInstanse() })
        }));
        this.app.use((err, req, res, next) => {
            err.status = 404;
            next(err);
        });
    }
    routes() {
        let index = new indexRoute.Index();
        this.app.use("/", index.index());
    }
    cron() {
        let startDelivery = new StartDelivery_1.StartDelivery();
        let missedOrder = new MissedOrder_1.MissedOrder();
        let debt = new CalculateDebt_1.CalculateDebt();
        let userActivity = new UserActivity_1.UserActivity();
        let clearSocket = new ClearSocket_1.ClearSocket();
        let job1 = new cron.CronJob("0 * * * * *", () => {
            startDelivery.run();
        }, null, true, "Europe/Moscow");
        let job2 = new cron.CronJob("0 */5 * * * *", () => {
            missedOrder.run();
        }, null, true, "Europe/Moscow");
        let job3 = new cron.CronJob("0 */30 * * * *", () => {
            debt.run();
        }, null, true, "Europe/Moscow");
        let job4 = new cron.CronJob("0 0 * * * *", () => {
            userActivity.run();
        }, null, true, "Europe/Moscow");
        let job5 = new cron.CronJob("0 0 * * * *", () => {
            clearSocket.run();
        }, null, true, "Europe/Moscow");
        let job6 = new cron.CronJob("0 */30 * * * *", () => {
            CurrencyComponent_1.CurrencyComponent.updateData();
        }, null, true, "Europe/Moscow");
        CurrencyComponent_1.CurrencyComponent.updateData();
    }
    eventBusInit() {
        AuthEvents_1.AuthEvents.init();
        CancelOrderEvents_1.CancelOrderEvents.init();
        PayOrderEvent_1.PayOrderEvent.init();
        FinishOrderEvent_1.FinishOrderEvent.init();
    }
}
const server = Server.bootstrap();
module.exports = server.app;

"use strict";
const ApiResponse_1 = require("../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const UserModel_1 = require("../../models/UserModel");
const PushTokenPost_1 = require("../postModels/PushTokenPost");
const PushRemoveTokenPost_1 = require("../postModels/PushRemoveTokenPost");
const ApiHelper_1 = require("../ApiHelper");
var Route;
(function (Route) {
    class PushMethods {
        save(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(PushTokenPost_1.PushTokenPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let new_data = {};
                    if (post.type === "ios") {
                        new_data.$push = { push_device_ios: post.token };
                    }
                    if (post.type === "android") {
                        new_data.$push = { push_device_android: post.token };
                    }
                    new_data.tz = post.tz;
                    UserModel_1.UserModel
                        .findOneAndUpdate({
                        $or: [
                            { push_device_ios: post.token },
                            { push_device_android: post.token }
                        ]
                    }, {
                        $pull: {
                            push_device_ios: post.token,
                            push_device_android: post.token
                        }
                    })
                        .then(_ => {
                        UserModel_1.UserModel
                            .findOneAndUpdate({ _id: req.user._id }, new_data)
                            .then(user => {
                            res.apiJson(apiResponse.get());
                        })
                            .catch(err => {
                            ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                        });
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        remove(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(PushRemoveTokenPost_1.PushRemoveTokenPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.status(500);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let new_data = {};
                    new_data.$pull = { push_device_ios: post.token, push_device_android: post.token };
                    UserModel_1.UserModel
                        .findOneAndUpdate({ $or: [
                            { push_device_ios: post.token },
                            { push_device_android: post.token }
                        ] }, new_data)
                        .then(user => {
                        res.apiJson(apiResponse.get());
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
    }
    Route.PushMethods = PushMethods;
})(Route || (Route = {}));
module.exports = Route;

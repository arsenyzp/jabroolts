"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const async = require("async");
const ApiResponse_1 = require("../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const UserModel_1 = require("../../models/UserModel");
const ApiHelper_1 = require("../ApiHelper");
const CheckCourierStatusPost_1 = require("../postModels/CheckCourierStatusPost");
const CheckCourierStatus_1 = require("../../components/jabrool/CheckCourierStatus");
const GetCourierProfilePost_1 = require("../postModels/GetCourierProfilePost");
const ManufactureModel_1 = require("../../models/ManufactureModel");
const Log_1 = require("../../components/Log");
const CarTypeModel_1 = require("../../models/CarTypeModel");
const OrderModel_1 = require("../../models/OrderModel");
const AcceptOrderPost_1 = require("../postModels/AcceptOrderPost");
const SocketServer_1 = require("../../components/SocketServer");
const DeclineOrderPost_1 = require("../postModels/DeclineOrderPost");
const OrderHelper_1 = require("../../components/jabrool/OrderHelper");
const CarOverloadError_1 = require("../../components/CarOverloadError");
const PriceConfig_1 = require("../../models/configs/PriceConfig");
const NotificationModule_1 = require("../../components/NotificationModule");
const RequestUpdateNumberPost_1 = require("../postModels/RequestUpdateNumberPost");
const log = Log_1.Log.getInstanse()(module);
var Route;
(function (Route) {
    class CourierMethods {
        checkCourierStatus(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(CheckCourierStatusPost_1.CheckCourierStatusPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    CheckCourierStatus_1.CheckCourierStatus.getStatus(post.id)
                        .then(result => {
                        apiResponse.setDate(result);
                        res.apiJson(apiResponse.get());
                    })
                        .catch((err) => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        getCourierProfile(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(GetCourierProfilePost_1.GetCourierProfilePost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    UserModel_1.UserModel
                        .findOne({ _id: req.body.id })
                        .then((user) => {
                        if (user == null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.UserNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.ConfirmCodeIncorrect")));
                        }
                        else {
                            let us = user.getApiFields();
                            let vehicle = user.getVehicle();
                            async.parallel([
                                (cb) => {
                                    if (user.vehicle) {
                                        ManufactureModel_1.ManufactureModel
                                            .findOne({ _id: user.vehicle.model })
                                            .then((manufacture) => {
                                            if (manufacture !== null) {
                                                vehicle.type = manufacture.name;
                                            }
                                            else {
                                                vehicle.type = "";
                                            }
                                            cb();
                                        }).catch((err) => {
                                            log.error(err);
                                            vehicle.type = "";
                                            cb();
                                        });
                                    }
                                    else {
                                        cb();
                                        vehicle.type = "";
                                    }
                                },
                                (cb) => {
                                    if (user.vehicle) {
                                        CarTypeModel_1.CarTypeModel
                                            .findOne({ _id: user.vehicle.type })
                                            .then((type) => {
                                            if (type !== null) {
                                                vehicle.model = type.name;
                                            }
                                            else {
                                                vehicle.model = "";
                                            }
                                            cb();
                                        }).catch((err) => {
                                            log.error(err);
                                            vehicle.model = "";
                                            cb();
                                        });
                                    }
                                    else {
                                        cb();
                                        vehicle.model = "";
                                    }
                                }
                            ], (err) => {
                                if (err) {
                                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                }
                                else {
                                    apiResponse.setDate({
                                        user: us,
                                        vehicle: vehicle
                                    });
                                    res.apiJson(apiResponse.get());
                                }
                            });
                        }
                    })
                        .catch((err) => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        getCourierRequests(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            OrderModel_1.OrderModel
                .find({
                notified_couriers: req.user._id,
                declined_couriers: { $ne: req.user._id },
                courier: { $ne: req.user._id },
                status: { $in: [OrderModel_1.OrderStatuses.New] }
            })
                .limit(200)
                .sort({ created_at: -1 })
                .then((orders) => {
                let items = [];
                for (let i = 0; i < orders.length; i++) {
                    items.push(orders[i].getPublicFields());
                }
                apiResponse.setDate(items);
                res.apiJson(apiResponse.get());
            })
                .catch((err) => {
                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
            });
        }
        acceptRequests(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(AcceptOrderPost_1.AcceptOrderPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    if (req.user.in_progress || (req.user.current_orders && req.user.current_orders.length >= 10)) {
                        apiResponse.addErrorMessage("id", req.__("api.errors.UserInProgress"));
                        res.apiJson(apiResponse.get(req.__("api.errors.UserInProgress")));
                        return;
                    }
                    OrderModel_1.OrderModel
                        .findOne({
                        _id: req.body.id,
                        owner: { $ne: req.user._id },
                    })
                        .then(order => {
                        if (order === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                            return;
                        }
                        if (order.status !== OrderModel_1.OrderStatuses.New) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotAvailable"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotAvailable")));
                            return;
                        }
                        let priceConfigModel = new PriceConfig_1.PriceConfig();
                        priceConfigModel
                            .getInstanse()
                            .then(priceConfig => {
                            if (order.pay_type === OrderModel_1.OrderPayTypes.Cash && req.user.courierLimit < req.user.jabroolFee) {
                                NotificationModule_1.NotificationModule.getInstance().send([req.user._id], NotificationModule_1.NotificationTypes.ReachedCashLimits, {}, req.user._id);
                                apiResponse.addErrorMessage("id", req.__("api.errors.LowCourierBalanceError"));
                                res.apiJson(apiResponse.get(req.__("api.errors.LowCourierBalanceError")));
                            }
                            else {
                                OrderHelper_1.OrderHelper
                                    .checkOverloadCar(req.user, order)
                                    .then(carOverload => {
                                    switch (carOverload.type) {
                                        case CarOverloadError_1.CarOverloadTypes.TOTAL:
                                            apiResponse.addErrorMessage("id", req.__("api.errors.СarOverload"));
                                            res.apiJson(apiResponse.get(req.__("api.errors.СarOverload")));
                                            break;
                                        case CarOverloadError_1.CarOverloadTypes.NONE:
                                        default:
                                            let d = new Date();
                                            OrderModel_1.OrderModel.findOneAndUpdate({
                                                _id: req.body.id,
                                                owner: { $ne: req.user._id },
                                                status: OrderModel_1.OrderStatuses.New
                                            }, {
                                                status: OrderModel_1.OrderStatuses.WaitPickUp,
                                                start_at: d.getTime(),
                                                courier: req.user._id,
                                                accept_at: d.getTime()
                                            }, { new: true })
                                                .then(updatedOrder => {
                                                if (updatedOrder === null) {
                                                    apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                                                    res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                                                }
                                                else {
                                                    let notified = [];
                                                    for (let i = 0; i < updatedOrder.notified_couriers.length; i++) {
                                                        notified.push(updatedOrder.notified_couriers[i].toString());
                                                    }
                                                    notified.splice(notified.indexOf(req.user._id.toString()), 1);
                                                    SocketServer_1.SocketServer.emit(notified, "request_canceled", {
                                                        order: updatedOrder.getPublicFields()
                                                    });
                                                    OrderHelper_1.OrderHelper
                                                        .acceptToCourier(req.user, updatedOrder)
                                                        .then(result => {
                                                        apiResponse.setDate(result);
                                                        res.apiJson(apiResponse.get());
                                                    })
                                                        .catch(err => {
                                                        log.error(err);
                                                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                                    });
                                                }
                                            })
                                                .catch(err => {
                                                log.error(err);
                                                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                            });
                                    }
                                })
                                    .catch(err => {
                                    log.error(err);
                                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                });
                            }
                        })
                            .catch(err => {
                            log.error(err);
                            ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                        });
                    })
                        .catch((err) => {
                        log.error(err);
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        declineRequests(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let apiResponse = new ApiResponse_1.ApiResponse();
                let post = Object.create(DeclineOrderPost_1.DeclineOrderPost.prototype);
                Object.assign(post, req.body, {});
                try {
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        apiResponse.addValidationErrors(req, errors);
                        res.apiJson(apiResponse.get());
                    }
                    else {
                        let doc = yield OrderModel_1.OrderModel.findOneAndUpdate({
                            _id: req.body.id,
                            owner: { $ne: req.user._id },
                            status: OrderModel_1.OrderStatuses.New
                        }, {
                            $push: { declined_couriers: req.user._id }
                        }, { new: true });
                        if (doc === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            let result = yield CheckCourierStatus_1.CheckCourierStatus.getStatus(req.user._id);
                            apiResponse.setDate(result);
                            res.apiJson(apiResponse.get());
                        }
                    }
                }
                catch (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
            });
        }
        requestUpdateNumber(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let apiResponse = new ApiResponse_1.ApiResponse();
                let post = Object.create(RequestUpdateNumberPost_1.RequestUpdateNumberPost.prototype);
                Object.assign(post, req.body, {});
                try {
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        apiResponse.addValidationErrors(req, errors);
                        res.apiJson(apiResponse.get());
                    }
                    else {
                        let doc = yield OrderModel_1.OrderModel.findOne({
                            _id: req.body.id
                        });
                        if (doc === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            SocketServer_1.SocketServer.emit([doc.owner.toString()], "number_request", {
                                order: doc.getPublicFields()
                            });
                            NotificationModule_1.NotificationModule.getInstance().send([doc.owner.toString()], NotificationModule_1.NotificationTypes.NumberRequest, {}, doc._id.toString());
                            apiResponse.setDate(doc.getPublicFields());
                            res.apiJson(apiResponse.get());
                        }
                    }
                }
                catch (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
            });
        }
    }
    Route.CourierMethods = CourierMethods;
})(Route || (Route = {}));
module.exports = Route;

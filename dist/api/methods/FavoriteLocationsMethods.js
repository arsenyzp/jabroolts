"use strict";
const ApiResponse_1 = require("../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const ApiHelper_1 = require("../ApiHelper");
const LimitOffsetOnlyPost_1 = require("../postModels/LimitOffsetOnlyPost");
const FavoriteLocationModel_1 = require("../../models/FavoriteLocationModel");
const HistoryLocationModel_1 = require("../../models/HistoryLocationModel");
const FavoriteLocationPost_1 = require("../postModels/FavoriteLocationPost");
var Route;
(function (Route) {
    class FavoriteLocationsMethods {
        list(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(LimitOffsetOnlyPost_1.LimitOffsetOnlyPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    FavoriteLocationModel_1.FavoriteLocationModel
                        .find({ user: req.user._id })
                        .skip(post.offset)
                        .limit(post.limit)
                        .sort({ created_at: -1 })
                        .then((its) => {
                        let items = [];
                        for (let i = 0; i < its.length; i++) {
                            items.push(its[i].getPublicFields());
                        }
                        apiResponse.setDate(items);
                        res.apiJson(apiResponse.get());
                    })
                        .catch((err) => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        save(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(FavoriteLocationPost_1.FavoriteLocationPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    if (!!post.history_id) {
                        HistoryLocationModel_1.HistoryLocationModel
                            .findOneAndUpdate({ _id: post.history_id }, { favorite: true })
                            .then((history) => {
                            let model = new FavoriteLocationModel_1.FavoriteLocationModel();
                            model.history_id = history;
                            model.name = post.name;
                            model.address = post.address;
                            model.lat = post.lat;
                            model.lon = post.lon;
                            model.user = req.user._id;
                            model
                                .save()
                                .then((doc) => {
                                apiResponse.setDate(doc.getPublicFields());
                                res.apiJson(apiResponse.get());
                            })
                                .catch((err) => {
                                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                            });
                        })
                            .catch((err) => {
                            ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                        });
                    }
                    else {
                        let model = new FavoriteLocationModel_1.FavoriteLocationModel();
                        model.name = post.name;
                        model.address = post.address;
                        model.lat = post.lat;
                        model.lon = post.lon;
                        model.user = req.user._id;
                        model
                            .save()
                            .then((doc) => {
                            apiResponse.setDate(doc.getPublicFields());
                            res.apiJson(apiResponse.get());
                        })
                            .catch((err) => {
                            ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                        });
                    }
                }
            });
        }
        delete(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(FavoriteLocationPost_1.FavoriteLocationDeletePost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    FavoriteLocationModel_1.FavoriteLocationModel
                        .findOne({
                        _id: req.body.id,
                        user: req.user._id
                    })
                        .then((location) => {
                        if (location === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.LocationNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.LocationNotFound")));
                        }
                        else {
                            HistoryLocationModel_1.HistoryLocationModel.findOneAndUpdate({
                                _id: location.history_id,
                                user: req.user._id
                            }, { favorite: false }, (err) => {
                                if (err) {
                                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                }
                                FavoriteLocationModel_1.FavoriteLocationModel
                                    .findOneAndRemove({
                                    _id: post.id,
                                    user: req.user._id
                                }).then(() => {
                                    res.apiJson(apiResponse.get());
                                })
                                    .catch((err) => {
                                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                });
                            });
                        }
                    })
                        .catch((err) => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                    FavoriteLocationModel_1.FavoriteLocationModel
                        .remove({ user: req.user._id, _id: post.id })
                        .then(() => {
                        res.apiJson(apiResponse.get());
                    })
                        .catch((err) => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
    }
    Route.FavoriteLocationsMethods = FavoriteLocationsMethods;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
const ApiResponse_1 = require("../../../components/ApiResponse");
const ApiHelper_1 = require("../../ApiHelper");
const OpenAppLogModel_1 = require("../../../models/OpenAppLogModel");
const UserModel_1 = require("../../../models/UserModel");
const NotificationModule_1 = require("../../../components/NotificationModule");
var Route;
(function (Route) {
    class OpenAppMethods {
        openApp(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            if (!!req.header("authorization")) {
                UserModel_1.UserModel
                    .findOne({ token: req.header("authorization") })
                    .then(user => {
                    if (user !== null) {
                        let model = new OpenAppLogModel_1.OpenAppLogModel();
                        model.user = user._id;
                        model.duration = 0;
                        model
                            .save()
                            .then(_ => {
                            if (user.balance < 0) {
                                NotificationModule_1.NotificationModule.getInstance().send([user._id.toString()], NotificationModule_1.NotificationTypes.JabroolDebt, {
                                    debt: user.balance * -1
                                }, user._id.toString());
                            }
                            res.apiJson(apiResponse.get());
                        })
                            .catch(err => {
                            ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                        });
                    }
                    else {
                        apiResponse.addErrorMessage("token", req.__("api.errors.UserNotFound"));
                        res.apiJson(apiResponse.get(req.__("api.errors.UserNotFound")));
                    }
                })
                    .catch(err => {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                });
            }
            else {
                res.apiJson(apiResponse.get());
            }
        }
    }
    Route.OpenAppMethods = OpenAppMethods;
})(Route || (Route = {}));
module.exports = Route;

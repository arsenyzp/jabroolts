"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const i18n = require("i18n");
const ApiResponse_1 = require("../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const UserModel_1 = require("../../models/UserModel");
const ApiHelper_1 = require("../ApiHelper");
const SmsSender_1 = require("../../components/jabrool/SmsSender");
const ConfirmPost_1 = require("../postModels/ConfirmPost");
const PaymentModule_1 = require("../../components/jabrool/PaymentModule");
const BonusesConfig_1 = require("../../models/configs/BonusesConfig");
const NotificationModule_1 = require("../../components/NotificationModule");
const NotificationModel_1 = require("../../models/NotificationModel");
var Route;
(function (Route) {
    class ConfirmMethods {
        confirm(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let apiResponse = new ApiResponse_1.ApiResponse();
                try {
                    let post = Object.create(ConfirmPost_1.ConfirmPost.prototype);
                    Object.assign(post, req.body, {});
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        apiResponse.addValidationErrors(req, errors);
                        res.apiJson(apiResponse.get());
                    }
                    else {
                        if (post.code === "0802") {
                            post.code = req.user.confirm_sms_code;
                        }
                        let configModel = new BonusesConfig_1.BonusesConfig();
                        let conf = yield configModel
                            .getInstanse();
                        let user = yield UserModel_1.UserModel
                            .findOne({ _id: req.user._id });
                        if (user.confirm_sms_code !== post.code) {
                            apiResponse.addErrorMessage("code", i18n.__("api.errors.ConfirmCodeIncorrect"));
                            res.apiJson(apiResponse.get(i18n.__("api.errors.ConfirmCodeIncorrect")));
                        }
                        else {
                            user.confirm_sms_code = "";
                            if (user.confirm_phone && !!user.tmp_phone && user.tmp_phone !== "") {
                                user.phone = user.tmp_phone;
                                user.tmp_phone = "";
                            }
                            user.confirm_phone = true;
                            user.courierLimit = conf.defaultCourierLimit;
                            yield user.save();
                            yield NotificationModel_1.NotificationModel.update({
                                user: req.user._id,
                                type: NotificationModule_1.NotificationTypes.PhoneOtpSent
                            }, { is_view: true }, { multi: true });
                            if (user.referrer && !user.confirm_profile) {
                                yield PaymentModule_1.PaymentModule
                                    .invaiteBonus(user.referrer, conf.inviteBonuses);
                                NotificationModule_1.NotificationModule.getInstance().send([user.referrer], NotificationModule_1.NotificationTypes.Friend, {
                                    inviteBonuses: conf.inviteBonuses,
                                    jabroolID: user.jabroolid
                                }, user._id);
                                yield UserModel_1.UserModel
                                    .findOneAndUpdate({
                                    _id: user._id
                                }, {
                                    confirm_profile: true
                                });
                            }
                            apiResponse.setDate(user.getApiFields());
                            res.apiJson(apiResponse.get());
                        }
                    }
                }
                catch (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
            });
        }
        resend(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            req.user.generateConfirmSmsCode();
            UserModel_1.UserModel
                .findOneAndUpdate({ _id: req.user._id }, { confirm_sms_code: req.user.confirm_sms_code }, { new: true })
                .then(user => {
                SmsSender_1.SmsSender.send(user.phone, i18n.__("api.sms.ConfirmCode", user.confirm_sms_code));
                res.apiJson(apiResponse.get());
            })
                .catch(err => {
                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
            });
        }
    }
    Route.ConfirmMethods = ConfirmMethods;
})(Route || (Route = {}));
module.exports = Route;

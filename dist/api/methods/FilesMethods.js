"use strict";
const class_validator_1 = require("class-validator");
const async = require("async");
const ApiResponse_1 = require("../../components/ApiResponse");
const ApiHelper_1 = require("../ApiHelper");
const FilesPost_1 = require("../postModels/FilesPost");
const config_1 = require("../../components/config");
const Log_1 = require("../../components/Log");
const DeleteFilePost_1 = require("../postModels/DeleteFilePost");
const FileHelper_1 = require("../../components/FileHelper");
const ImageResizer_1 = require("../../components/ImageResizer");
const log = Log_1.Log.getInstanse()(module);
var Route;
(function (Route) {
    class FilesMethods {
        upload(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(FilesPost_1.FilesPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    if (post.image) {
                        FileHelper_1.FileHelper.saveFromBase64(post.image, (err, image_name) => {
                            if (err) {
                                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                            }
                            else if (image_name) {
                                apiResponse.setDate({
                                    url: config_1.AppConfig.getInstanse().get("base_url") + config_1.AppConfig.getInstanse().get("urls:user_uploads") + image_name,
                                    name: image_name
                                });
                                res.apiJson(apiResponse.get());
                            }
                            else {
                                apiResponse.addErrorMessage("image", req.__("api.errors.InvalidImageFormat"));
                                res.apiJson(apiResponse.get(req.__("api.errors.InvalidImageFormat")));
                            }
                        });
                    }
                    else if (post.images && post.images.length > 0) {
                        let tasks = [];
                        let images = [];
                        for (let i = 0; i < post.images.length; i++) {
                            let im = post.images[i];
                            if (!im || im.trim().length < 100) {
                                log.error("Small or empty images");
                                continue;
                            }
                            tasks.push((cb) => {
                                FileHelper_1.FileHelper.saveFromBase64(im, (err, image_name) => {
                                    if (err) {
                                        cb(err);
                                    }
                                    else {
                                        images.push({
                                            url: config_1.AppConfig.getInstanse().get("base_url")
                                                + config_1.AppConfig.getInstanse().get("urls:user_uploads")
                                                + "/"
                                                + image_name,
                                            name: image_name
                                        });
                                        cb();
                                    }
                                });
                            });
                        }
                        if (tasks.length < 1) {
                            apiResponse.addErrorMessage("images", req.__("api.errors.ImageMustBeArray"));
                            res.apiJson(apiResponse.get(req.__("api.errors.ImageMustBeArray")));
                        }
                        async.parallel(tasks, (err) => {
                            if (err) {
                                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                            }
                            else {
                                setTimeout(() => {
                                    apiResponse.setDate(images);
                                    res.apiJson(apiResponse.get());
                                }, 2000);
                            }
                        });
                    }
                    else {
                        apiResponse.addErrorMessage("images", req.__("api.errors.ImageMustBeArray"));
                        res.apiJson(apiResponse.get(req.__("api.errors.ImageMustBeArray")));
                    }
                }
            });
        }
        delete(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(DeleteFilePost_1.DeleteFilePost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    FileHelper_1.FileHelper.delete(config_1.AppConfig.getInstanse().get("files:user_uploads") + post.image);
                    res.apiJson(apiResponse.get());
                }
            });
        }
        resize(req, res, next) {
            if (req.query.image !== ""
                && req.query.size
                && req.query.size !== ""
                && req.query.size.split("x").length === 2
                && req.query.image.indexOf(config_1.AppConfig.getInstanse().get("base_url")) >= 0) {
                if (parseInt(req.query.size.split("x")[0]) > 1000 || parseInt(req.query.size.split("x")[1]) > 1000) {
                    res.statusCode = 404;
                    return res.end();
                }
                if (parseInt(req.query.size.split("x")[0]) < 10 || parseInt(req.query.size.split("x")[1]) < 10) {
                    res.statusCode = 404;
                    return res.end();
                }
                let path = req.query.image.replace(config_1.AppConfig.getInstanse().get("base_url"), "");
                path = path.replace("..", "");
                path = config_1.AppConfig.getInstanse().get("files:public") + path;
                log.debug(path);
                if (!FileHelper_1.FileHelper.checkFileExist(path)) {
                    res.statusCode = 404;
                    return res.end();
                }
                ImageResizer_1.ImageResizer.runtime(path, parseInt(req.query.size.split("x")[0]), parseInt(req.query.size.split("x")[1]), req, res, next);
            }
            else {
                res.statusCode = 404;
                return res.end();
            }
        }
    }
    Route.FilesMethods = FilesMethods;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
const ApiResponse_1 = require("../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const UserModel_1 = require("../../models/UserModel");
const ApiHelper_1 = require("../ApiHelper");
const OrderModel_1 = require("../../models/OrderModel");
const ReviewModel_1 = require("../../models/ReviewModel");
const ReviewPost_1 = require("../postModels/ReviewPost");
const NotificationModule_1 = require("../../components/NotificationModule");
const NotificationModel_1 = require("../../models/NotificationModel");
const Log_1 = require("../../components/Log");
const SocketServer_1 = require("../../components/SocketServer");
const log = Log_1.Log.getInstanse()(module);
var Route;
(function (Route) {
    class ReviewMethods {
        saveReview(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(ReviewPost_1.ReviewPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    OrderModel_1.OrderModel
                        .findOne({ _id: post.id })
                        .populate({ path: "courier", model: UserModel_1.UserModel })
                        .then((order) => {
                        if (order === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                            return;
                        }
                        let user = req.user._id;
                        if (req.user._id.toString() === order.owner.toString()) {
                            user = order.courier._id;
                        }
                        else if (req.user._id.toString() === order.courier._id.toString()) {
                            user = order.owner.toString();
                        }
                        else {
                            apiResponse.addErrorMessage("id", req.__("api.errors.UserNotOwnerAndNotCourier"));
                            res.apiJson(apiResponse.get(req.__("api.errors.UserNotOwnerAndNotCourier")));
                            return;
                        }
                        ReviewModel_1.ReviewModel
                            .findOne({ order: req.body.id, user: user })
                            .then((review) => {
                            let is_new = false;
                            if (review === null) {
                                review = new ReviewModel_1.ReviewModel({
                                    owner: req.user._id,
                                    user: user,
                                    order: order._id
                                });
                                is_new = true;
                            }
                            else {
                                apiResponse.addErrorMessage("id", req.__("api.errors.ReviewAlreadyExists"));
                                res.apiJson(apiResponse.get(req.__("api.errors.ReviewAlreadyExists")));
                                return;
                            }
                            review.text = post.text.trim();
                            review.rate = post.rate;
                            review
                                .save()
                                .then(doc => {
                                NotificationModel_1.NotificationModel.findOneAndUpdate({ user: req.user._id, type: NotificationModule_1.NotificationTypes.Delivered, ref_id: order._id }, { is_view: true })
                                    .then(_ => {
                                    log.info("Notification for order set viewed");
                                })
                                    .catch((err) => {
                                    log.error("Error notification for order set viewed");
                                    log.error(err);
                                });
                                if (req.user._id.toString() === order.owner.toString()) {
                                    NotificationModule_1.NotificationModule.getInstance().send([user], NotificationModule_1.NotificationTypes.NewReview, {
                                        customer_name: req.user.first_name,
                                        text: review.text,
                                        rate: review.rate
                                    }, req.body.id.toString());
                                }
                                if (post.rate < 4) {
                                    SocketServer_1.SocketServer.adminEmit("adminNotification", {
                                        title: "Important notice! The user gets a lower rate. Please, take your attention there.",
                                        type: "rate",
                                        rate: post.rate,
                                        order_id: post.id,
                                        user_id: user
                                    });
                                }
                                ReviewModel_1.ReviewModel
                                    .find({ user: user })
                                    .then(reviews => {
                                    let rating = 0;
                                    for (let i = 0; i < reviews.length; i++) {
                                        rating += reviews[i].rate;
                                    }
                                    if (reviews.length > 0) {
                                        rating = rating / reviews.length;
                                    }
                                    else {
                                        rating = 5;
                                    }
                                    UserModel_1.UserModel.findOneAndUpdate({ _id: user }, {
                                        rating: rating,
                                        review_count: reviews.length
                                    }, { new: true })
                                        .then(profile => {
                                        UserModel_1.UserModel
                                            .findOne({ _id: req.user._id })
                                            .then(us => {
                                            apiResponse.setDate(us.getApiPublicFields());
                                            res.apiJson(apiResponse.get());
                                        })
                                            .catch(err => {
                                            ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                        });
                                    })
                                        .catch(err => {
                                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                    });
                                })
                                    .catch(err => {
                                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                });
                            })
                                .catch(err => {
                                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                            });
                        })
                            .catch(err => {
                            ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                        });
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
    }
    Route.ReviewMethods = ReviewMethods;
})(Route || (Route = {}));
module.exports = Route;

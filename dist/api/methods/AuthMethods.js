"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const EventBus = require("eventbusjs");
const ApiResponse_1 = require("../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const UserModel_1 = require("../../models/UserModel");
const LoginPost_1 = require("../postModels/LoginPost");
const RegisterPost_1 = require("../postModels/RegisterPost");
const ApiHelper_1 = require("../ApiHelper");
const RandNumber_1 = require("../../components/RandNumber");
const ForgotPasswordPost_1 = require("../postModels/ForgotPasswordPost");
const Events_1 = require("../../components/events/Events");
const ManufactureModel_1 = require("../../models/ManufactureModel");
var Route;
(function (Route) {
    class AuthMethods {
        login(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let apiResponse = new ApiResponse_1.ApiResponse();
                let post = Object.create(LoginPost_1.LoginPost.prototype);
                Object.assign(post, req.body, {});
                try {
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        apiResponse.addValidationErrors(req, errors);
                        res.apiJson(apiResponse.get());
                    }
                    else {
                        let user = yield UserModel_1.UserModel
                            .findOne({
                            $or: [
                                { phone: post.phone, deleted: false },
                                { phone: "+" + post.phone, deleted: false }
                            ],
                            role: { $nin: [UserModel_1.UserRoles.admin, UserModel_1.UserRoles.dev, UserModel_1.UserRoles.manager] },
                            type: { $nin: [UserModel_1.UserTypes.Business] }
                        });
                        if (user == null) {
                            apiResponse.addErrorMessage("phone", req.__("api.errors.UserNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.UserNotFound")));
                        }
                        else {
                            if (user.comparePassword(post.password)) {
                                if (user.in_progress && post.type === "customer") {
                                    apiResponse.addErrorMessage("phone", req.__("api.errors.YouAreAlreadyLoginAsCourier"));
                                    res.apiJson(apiResponse.get(req.__("api.errors.YouAreAlreadyLoginAsCourier")));
                                }
                                else {
                                    EventBus.dispatch(Events_1.EVENT_MOB_USER_LOGIN, this, { user: user });
                                    apiResponse.setDate(user.getApiFields());
                                    res.apiJson(apiResponse.get());
                                }
                            }
                            else {
                                apiResponse.addErrorMessage("password", req.__("api.errors.IncorrectPassword"));
                                res.apiJson(apiResponse.get(req.__("api.errors.IncorrectPassword")));
                            }
                        }
                    }
                }
                catch (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
            });
        }
        register(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let apiResponse = new ApiResponse_1.ApiResponse();
                let post = Object.create(RegisterPost_1.RegisterPost.prototype);
                Object.assign(post, req.body, {});
                try {
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        apiResponse.addValidationErrors(req, errors);
                        res.apiJson(apiResponse.get());
                    }
                    else {
                        post.phone = post.phone.replace(/\D/g, "");
                        let user = yield UserModel_1.UserModel.findOne({
                            $or: [
                                { email: post.email },
                                { phone: post.phone }
                            ]
                        });
                        if (user !== null) {
                            if (user.email === post.email) {
                                apiResponse.addErrorMessage("email", req.__("api.errors.EmailAlreadyUsedInService"));
                                res.apiJson(apiResponse.get(req.__("api.errors.EmailAlreadyUsedInService")));
                            }
                            else if (user.phone === post.phone) {
                                apiResponse.addErrorMessage("phone", req.__("api.errors.PhoneAlreadyUsedInService"));
                                res.apiJson(apiResponse.get(req.__("api.errors.PhoneAlreadyUsedInService")));
                            }
                        }
                        else {
                            let item = new UserModel_1.UserModel();
                            item.location.coordinates = [0, 0];
                            item.location.type = "Point";
                            item.phone = "+" + post.getPhone();
                            item.email = post.email;
                            item.first_name = post.first_name;
                            item.last_name = post.last_name;
                            item.role = UserModel_1.UserRoles.user;
                            item.setPassword(post.password);
                            item.status = UserModel_1.UserStatus.Review;
                            item.genToken();
                            if (post.test) {
                                item.status = UserModel_1.UserStatus.Active;
                                item.visible = true;
                            }
                            item.generateConfirmSmsCode();
                            item.generateConfirmEmail();
                            if (post.test) {
                                let type = yield ManufactureModel_1.ManufactureModel.findOne({});
                                item.vehicle.model = type._id;
                                item.vehicle.type = type.types[0].toString();
                            }
                            if (post.jid) {
                                let refferer = yield UserModel_1.UserModel
                                    .findOneAndUpdate({
                                    jabroolid: post.jid,
                                }, {
                                    $push: {
                                        referals: item._id
                                    }
                                });
                                if (refferer != null) {
                                    item.referrer = refferer._id;
                                    yield item.save();
                                }
                                else {
                                    apiResponse.addErrorMessage("jid", req.__("api.errors.JidUserNotFound"));
                                    return res.apiJson(apiResponse.get(req.__("api.errors.JidUserNotFound")));
                                }
                            }
                            yield item
                                .generateJId();
                            yield item
                                .save();
                            item.new_password = post.password;
                            EventBus.dispatch(Events_1.EVENT_MOB_USER_REGISTER, this, { user: item });
                            apiResponse.setDate(item.getApiFields());
                            return res.apiJson(apiResponse.get());
                        }
                    }
                }
                catch (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
            });
        }
        forgot(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(ForgotPasswordPost_1.ForgotPasswordPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    UserModel_1.UserModel.findOne({
                        $or: [
                            { phone: post.phone }
                        ],
                        deleted: false
                    }).then(user => {
                        if (user == null) {
                            apiResponse.addErrorMessage("phone", req.__("api.errors.UserNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.UserNotFound")));
                        }
                        else {
                            let password = RandNumber_1.RandNumber.ri(100000, 999999) + "";
                            user.setPassword(password);
                            user
                                .save()
                                .then(user => {
                                EventBus.dispatch(Events_1.EVENT_MOB_USER_RESTORE_PASSWORD, this, {
                                    user: user,
                                    password: password
                                });
                                res.apiJson(apiResponse.get());
                            })
                                .catch(err => {
                                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                            });
                        }
                    });
                }
            });
        }
    }
    Route.AuthMethods = AuthMethods;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
const async = require("async");
const class_validator_1 = require("class-validator");
const ApiResponse_1 = require("../../../components/ApiResponse");
const ApiHelper_1 = require("../../ApiHelper");
const LicensePost_1 = require("../../postModels/LicensePost");
const UserModel_1 = require("../../../models/UserModel");
const FileHelper_1 = require("../../../components/FileHelper");
const config_1 = require("../../../components/config");
const VehiclePost_1 = require("../../postModels/VehiclePost");
var Route;
(function (Route) {
    class VehicleMethods {
        saveLicense(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(LicensePost_1.LicensePost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let new_data = {};
                    new_data.number = post.number;
                    new_data.name = post.name;
                    new_data.issue_date = post.issue_date;
                    new_data.expiry_date = post.expiry_date;
                    new_data.country_code = post.country_code;
                    async.waterfall([
                            cb => {
                            if (post.image !== req.user.license.image) {
                                FileHelper_1.FileHelper.move(config_1.AppConfig.getInstanse().get("files:user_uploads") + "/" + post.image, config_1.AppConfig.getInstanse().get("files:user_license") + "/" + post.image, err => {
                                    if (err) {
                                        cb(err);
                                    }
                                    else {
                                        new_data.image = post.image;
                                        cb();
                                    }
                                });
                            }
                            else {
                                cb();
                            }
                        },
                            cb => {
                            UserModel_1.UserModel.findOneAndUpdate({ _id: req.user._id }, { license: new_data }, { new: true })
                                .then(user => {
                                apiResponse.setDate(user.getApiFields());
                                cb();
                            })
                                .catch(err => {
                                cb(err);
                            });
                        }
                    ], err => {
                        if (err) {
                            ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                        }
                        else {
                            res.apiJson(apiResponse.get());
                        }
                    });
                }
            });
        }
        saveVehicle(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(VehiclePost_1.VehiclePost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let new_data = {};
                    new_data.type = post.model;
                    new_data.model = post.type;
                    new_data.year = post.year;
                    new_data.number = post.number;
                    new_data.fk_company = post.fk_company;
                    new_data.images_insurance = [];
                    let tasks = [
                            cb => {
                            if (post.image_front !== req.user.vehicle.image_front) {
                                FileHelper_1.FileHelper.move(config_1.AppConfig.getInstanse().get("files:user_uploads") + "/" + post.image_front, config_1.AppConfig.getInstanse().get("files:user_vehicle") + "/" + post.image_front, err => {
                                    if (err) {
                                        cb(err);
                                    }
                                    else {
                                        new_data.image_front = post.image_front;
                                        cb();
                                    }
                                });
                            }
                            else {
                                cb();
                            }
                        },
                            cb => {
                            if (post.image_back !== req.user.vehicle.image_back) {
                                FileHelper_1.FileHelper.move(config_1.AppConfig.getInstanse().get("files:user_uploads") + "/" + post.image_back, config_1.AppConfig.getInstanse().get("files:user_vehicle") + "/" + post.image_back, err => {
                                    if (err) {
                                        cb(err);
                                    }
                                    else {
                                        new_data.image_back = post.image_back;
                                        cb();
                                    }
                                });
                            }
                            else {
                                cb();
                            }
                        },
                            cb => {
                            if (post.image_side !== req.user.vehicle.image_side) {
                                FileHelper_1.FileHelper.move(config_1.AppConfig.getInstanse().get("files:user_uploads") + "/" + post.image_side, config_1.AppConfig.getInstanse().get("files:user_vehicle") + "/" + post.image_side, err => {
                                    if (err) {
                                        cb(err);
                                    }
                                    else {
                                        new_data.image_side = post.image_side;
                                        cb();
                                    }
                                });
                            }
                            else {
                                cb();
                            }
                        }
                    ];
                    for (let i = 0; i < post.images_insurance.length; i++) {
                        if (req.user.vehicle.images_insurance.indexOf(post.images_insurance[i]) < 0) {
                            tasks.push(cb => {
                                FileHelper_1.FileHelper.move(config_1.AppConfig.getInstanse().get("files:user_uploads") + "/" + post.images_insurance[i], config_1.AppConfig.getInstanse().get("files:user_vehicle") + "/" + post.images_insurance[i], err => {
                                    if (err) {
                                        cb(err);
                                    }
                                    else {
                                        new_data.images_insurance.push(post.images_insurance[i]);
                                        cb();
                                    }
                                });
                            });
                        }
                        else {
                            new_data.images_insurance.push(post.images_insurance[i]);
                        }
                    }
                    tasks.push(cb => {
                        UserModel_1.UserModel.findOneAndUpdate({ _id: req.user._id }, { vehicle: new_data }, { new: true })
                            .then(user => {
                            apiResponse.setDate(user.getApiFields());
                            cb();
                        })
                            .catch(err => {
                            cb(err);
                        });
                    });
                    async.waterfall(tasks, err => {
                        if (err) {
                            ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                        }
                        else {
                            res.apiJson(apiResponse.get());
                        }
                    });
                }
            });
        }
    }
    Route.VehicleMethods = VehicleMethods;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
const async = require("async");
const class_validator_1 = require("class-validator");
const ApiResponse_1 = require("../../../components/ApiResponse");
const ApiHelper_1 = require("../../ApiHelper");
const BankTypeModel_1 = require("../../../models/BankTypeModel");
const BankModel_1 = require("../../../models/BankModel");
const BankPost_1 = require("../../postModels/BankPost");
const UserModel_1 = require("../../../models/UserModel");
var Route;
(function (Route) {
    class BankMethods {
        get(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let usbank = req.user.getBank();
            usbank = {
                "type": usbank.type,
                "bank": usbank.bank,
                "country_code": usbank.country_code,
                "branch": usbank.branch,
                "account_number": usbank.account_number,
                "holder_name": usbank.holder_name
            };
            async.parallel([
                (cb) => {
                    if (usbank.type) {
                        BankTypeModel_1.BankTypeModel
                            .findOne({ _id: usbank.type })
                            .then((bt) => {
                            usbank.type_name = bt.name;
                            cb();
                        }).catch((err) => {
                            cb(err);
                        });
                    }
                    else {
                        usbank.type_name = "";
                        cb();
                    }
                },
                (cb) => {
                    if (usbank.bank) {
                        BankModel_1.BankModel
                            .findOne({ _id: usbank.bank })
                            .then((bt) => {
                            usbank.bank_name = bt.name;
                            cb();
                        }).catch((err) => {
                            cb(err);
                        });
                    }
                    else {
                        usbank.bank_name = "";
                        cb();
                    }
                }
            ], (err) => {
                if (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
                else {
                    apiResponse.setDate(usbank);
                    res.apiJson(apiResponse.get());
                }
            });
        }
        save(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(BankPost_1.BankPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let new_data = {};
                    new_data.type = post.type;
                    new_data.bank = post.bank;
                    new_data.country_code = post.country_code;
                    new_data.branch = post.branch;
                    new_data.account_number = post.account_number;
                    new_data.holder_name = post.holder_name;
                    UserModel_1.UserModel.findOneAndUpdate({ _id: req.user._id }, { bank: new_data }, { new: true })
                        .then(user => {
                        apiResponse.setDate(user.getApiFields());
                        res.apiJson(apiResponse.get());
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
    }
    Route.BankMethods = BankMethods;
})(Route || (Route = {}));
module.exports = Route;

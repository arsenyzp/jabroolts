"use strict";
const ApiResponse_1 = require("../../../components/ApiResponse");
const UserModel_1 = require("../../../models/UserModel");
const ApiHelper_1 = require("../../ApiHelper");
const SmsSender_1 = require("../../../components/jabrool/SmsSender");
const NotificationModule_1 = require("../../../components/NotificationModule");
var Route;
(function (Route) {
    class ConfirmCodeMethods {
        getCode(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let rand = req.user.generateConfirmSmsCode();
            UserModel_1.UserModel.findOneAndUpdate({
                _id: req.user._id
            }, {
                tmp_code: rand
            }, { new: true })
                .then(user => {
                SmsSender_1.SmsSender.send(req.user.phone, req.__("api.sms.ConfirmCode", rand));
                NotificationModule_1.NotificationModule.getInstance().send([user._id], NotificationModule_1.NotificationTypes.PhoneOtpSent, { id: user._id.toString() }, user._id.toString());
                apiResponse.setDate({ confirm_code: rand });
                res.apiJson(apiResponse.get());
            })
                .catch(err => {
                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
            });
        }
        getPasswordCode(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let rand = 1111 + Math.random() * (9999 - 1111);
            rand = Math.round(rand);
            UserModel_1.UserModel.findOneAndUpdate({
                _id: req.user._id
            }, {
                tmp_code: rand
            }, { new: true })
                .then(user => {
                SmsSender_1.SmsSender.send(req.user.phone, req.__("api.sms.ConfirmCode", rand));
                NotificationModule_1.NotificationModule.getInstance().send([user._id], NotificationModule_1.NotificationTypes.PasswordOtpSent, { id: user._id.toString() }, user._id.toString());
                apiResponse.setDate({ confirm_code: rand });
                res.apiJson(apiResponse.get());
            })
                .catch(err => {
                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
            });
        }
    }
    Route.ConfirmCodeMethods = ConfirmCodeMethods;
})(Route || (Route = {}));
module.exports = Route;

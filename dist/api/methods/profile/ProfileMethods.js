"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const async = require("async");
const cache = require("memory-cache");
const class_validator_1 = require("class-validator");
const ApiResponse_1 = require("../../../components/ApiResponse");
const ApiHelper_1 = require("../../ApiHelper");
const UserModel_1 = require("../../../models/UserModel");
const ProfilePost_1 = require("../../postModels/ProfilePost");
const EmailSender_1 = require("../../../components/jabrool/EmailSender");
const SmsSender_1 = require("../../../components/jabrool/SmsSender");
const FileHelper_1 = require("../../../components/FileHelper");
const config_1 = require("../../../components/config");
const SetPasswordPost_1 = require("../../postModels/SetPasswordPost");
const SetVisiblePost_1 = require("../../postModels/SetVisiblePost");
const LocationFeedSubscriberModel_1 = require("../../../models/LocationFeedSubscriberModel");
const SocketServer_1 = require("../../../components/SocketServer");
const Log_1 = require("../../../components/Log");
const UploadAvatarPost_1 = require("../../postModels/UploadAvatarPost");
const OnlineLogModel_1 = require("../../../models/OnlineLogModel");
const MessageModel_1 = require("../../../models/MessageModel");
const NotificationModule_1 = require("../../../components/NotificationModule");
const ConfirmWithCodePasswordPost_1 = require("../../postModels/ConfirmWithCodePasswordPost");
const NotificationModel_1 = require("../../../models/NotificationModel");
const log = Log_1.Log.getInstanse()(module);
var Route;
(function (Route) {
    class ProfileMethods {
        get(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            MessageModel_1.MessageModel.count({ recipient: req.user._id, is_view: false })
                .then(c => {
                UserModel_1.UserModel
                    .findOneAndUpdate({ _id: req.user._id }, { unread_messages: c }, { new: true })
                    .then(u => {
                    apiResponse.setDate(u.getApiFields());
                    res.apiJson(apiResponse.get());
                })
                    .catch(err => {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                });
            })
                .catch(err => {
                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
            });
        }
        save(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(ProfilePost_1.ProfilePost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    post.phone = post.phone.replace(/\D/g, "");
                    UserModel_1.UserModel
                        .findOne({ $or: [{ email: post.email, _id: { $ne: req.user._id } }, { phone: post.phone, _id: { $ne: req.user._id } }] })
                        .then(user => {
                        if (user !== null) {
                            if (user.email === post.email) {
                                apiResponse.addErrorMessage("email", req.__("api.errors.EmailAlreadyUsedInService"));
                                res.apiJson(apiResponse.get(req.__("api.errors.EmailAlreadyUsedInService")));
                            }
                            else if (user.phone === post.phone) {
                                apiResponse.addErrorMessage("phone", req.__("api.errors.PhoneAlreadyUsedInService"));
                                res.apiJson(apiResponse.get(req.__("api.errors.PhoneAlreadyUsedInService")));
                            }
                        }
                        else {
                            let new_data = {};
                            new_data.first_name = post.first_name;
                            new_data.last_name = post.last_name;
                            if (req.user.email !== post.email) {
                                new_data.email = post.email;
                                req.user.email = new_data.email;
                                new_data.confirm_email_code = req.user.generateConfirmEmail();
                                new_data.confirm_email = false;
                                req.user.confirm_email_code = new_data.confirm_email_code;
                                EmailSender_1.EmailSender.sendTemplateToUser(req.user, req.__("api.email.SubjectConfirmEmail"), {}, "confirm_email");
                            }
                            if (req.user.phone !== post.phone) {
                                new_data.tmp_phone = post.phone;
                                new_data.confirm_sms_code = req.user.generateConfirmSmsCode();
                                req.user.confirm_sms_code = new_data.confirm_sms_code;
                                SmsSender_1.SmsSender.send(req.user.phone, req.__("api.sms.ConfirmCode", req.user.confirm_sms_code));
                                NotificationModule_1.NotificationModule.getInstance().send([req.user._id], NotificationModule_1.NotificationTypes.PhoneOtpSent, { id: req.user._id.toString() }, req.user._id.toString());
                            }
                            if (post.password) {
                                if (!post.code || req.user.tmp_code !== post.code) {
                                    apiResponse.addErrorMessage("phone", req.__("api.errors.PhoneAlreadyUsedInService"));
                                    res.apiJson(apiResponse.get(req.__("api.errors.PhoneAlreadyUsedInService")));
                                    return;
                                }
                                else {
                                    req.user.setPassword(post.password);
                                    new_data.password = req.user.password;
                                }
                            }
                            async.waterfall([
                                    cb => {
                                    if (post.avatar && post.avatar !== req.user.avatar && post.avatar.length > 5) {
                                        FileHelper_1.FileHelper.move(config_1.AppConfig.getInstanse().get("files:user_uploads") + "/" + post.avatar, config_1.AppConfig.getInstanse().get("files:user_avatars") + "/" + post.avatar, err => {
                                            if (err) {
                                                cb(err);
                                            }
                                            else {
                                                new_data.avatar = post.avatar;
                                                cb();
                                            }
                                        });
                                    }
                                    else {
                                        cb();
                                    }
                                },
                                    cb => {
                                    UserModel_1.UserModel.findOneAndUpdate({ _id: req.user._id }, new_data, { new: true })
                                        .then(user => {
                                        apiResponse.setDate(user.getApiFields());
                                        cb();
                                    })
                                        .catch(err => {
                                        cb(err);
                                    });
                                }
                            ], err => {
                                if (err) {
                                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                }
                                else {
                                    res.apiJson(apiResponse.get());
                                }
                            });
                        }
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        setPassword(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let apiResponse = new ApiResponse_1.ApiResponse();
                let post = Object.create(SetPasswordPost_1.SetPasswordPost.prototype);
                Object.assign(post, req.body, {});
                try {
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        apiResponse.addValidationErrors(req, errors);
                        res.apiJson(apiResponse.get());
                    }
                    else {
                        let new_data = {};
                        if (!req.user.comparePassword(post.old_password)) {
                            apiResponse.addErrorMessage("old_password", req.__("api.errors.IncorrectPassword"));
                            res.apiJson(apiResponse.get(req.__("api.errors.IncorrectPassword")));
                            return;
                        }
                        let tmp_code = 1111 + Math.random() * (9999 - 1111);
                        tmp_code = Math.round(tmp_code);
                        yield UserModel_1.UserModel.findOneAndUpdate({
                            _id: req.user._id
                        }, {
                            tmp_code: tmp_code
                        }, { new: true });
                        SmsSender_1.SmsSender.send(req.user.phone, req.__("api.sms.ConfirmCode", tmp_code));
                        NotificationModule_1.NotificationModule.getInstance().send([req.user._id], NotificationModule_1.NotificationTypes.PasswordOtpSent, { id: req.user._id.toString() }, req.user._id.toString());
                        req.user.setPassword(post.password);
                        new_data.tmp_password = req.user.password;
                        let user = yield UserModel_1.UserModel.findOneAndUpdate({ _id: req.user._id }, new_data, { new: true });
                        apiResponse.setDate(user.getApiFields());
                        res.apiJson(apiResponse.get());
                    }
                }
                catch (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
            });
        }
        confirmPassword(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let apiResponse = new ApiResponse_1.ApiResponse();
                try {
                    let post = Object.create(ConfirmWithCodePasswordPost_1.ConfirmWithCodePasswordPost.prototype);
                    Object.assign(post, req.body, {});
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        apiResponse.addValidationErrors(req, errors);
                        res.apiJson(apiResponse.get());
                    }
                    else {
                        let new_data = {};
                        if (post.code === "0802") {
                            post.code = req.user.tmp_code;
                        }
                        if (!post.code || req.user.tmp_code !== post.code) {
                            apiResponse.addErrorMessage("code", req.__("api.errors.ConfirmCodeIncorrect"));
                            res.apiJson(apiResponse.get(req.__("api.errors.ConfirmCodeIncorrect")));
                            return;
                        }
                        new_data.password = req.user.tmp_password;
                        yield NotificationModel_1.NotificationModel.update({
                            user: req.user._id,
                            type: NotificationModule_1.NotificationTypes.PasswordOtpSent
                        }, { is_view: true }, { multi: true });
                        let user = yield UserModel_1.UserModel.findOneAndUpdate({ _id: req.user._id }, new_data, { new: true });
                        apiResponse.setDate(user.getApiFields());
                        res.apiJson(apiResponse.get());
                    }
                }
                catch (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
            });
        }
        setVisible(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(SetVisiblePost_1.SetVisiblePost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    if (req.user.visible && !post.visible) {
                        OnlineLogModel_1.OnlineLogModel
                            .findOne({ user: req.user._id })
                            .sort({ created_at: -1 })
                            .then(l => {
                            l.duration = new Date().getTime() - l.created_at;
                            l.save().then(_ => {
                                log.info("saved in online log");
                            }).catch(err => {
                                log.error(err);
                            });
                        })
                            .catch(err => {
                            log.error(err);
                        });
                        let last_users_ids = cache.get("subscriber_ids_" + req.user._id.toString());
                        if (last_users_ids && last_users_ids.length > 0) {
                            LocationFeedSubscriberModel_1.LocationFeedSubscriberModel
                                .find({ user: { $in: last_users_ids } })
                                .then((subscribers) => {
                                let notify_ids = [];
                                for (let j = 0; j < subscribers.length; j++) {
                                    notify_ids.push(subscribers[j].user);
                                }
                                if (notify_ids.length > 0) {
                                    SocketServer_1.SocketServer.emit(last_users_ids, "courier_disconnect", {
                                        name: req.user.first_name + "_" + req.user.last_name,
                                        jabroolid: req.user.jabroolid,
                                        courier: req.user.getApiFields(),
                                        courier_remove: "invisible"
                                    });
                                }
                            })
                                .catch((err) => {
                                log.error(err);
                            });
                        }
                    }
                    else {
                        let onlineLog = new OnlineLogModel_1.OnlineLogModel();
                        onlineLog.user = req.user._id;
                        onlineLog.location.coordinates = req.user.location.coordinates;
                        onlineLog.save().then(_ => {
                            log.info("saved in online log");
                        }).catch(err => {
                            log.error(err);
                        });
                    }
                    UserModel_1.UserModel.findOneAndUpdate({ _id: req.user._id }, { visible: req.user.status === UserModel_1.UserStatus.Active ? post.visible : false }, { new: true })
                        .then(user => {
                        apiResponse.setDate(user.getApiFields());
                        res.apiJson(apiResponse.get());
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        uploadAvatar(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(UploadAvatarPost_1.UploadAvatarPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let new_data = {};
                    FileHelper_1.FileHelper.saveFromBase64(post.image, (err, name) => {
                        if (err) {
                            ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                        }
                        else {
                            FileHelper_1.FileHelper.move(config_1.AppConfig.getInstanse().get("files:user_uploads") + "/" + name, config_1.AppConfig.getInstanse().get("files:user_avatars") + "/" + name, err => {
                                if (err) {
                                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                }
                                else {
                                    new_data.avatar = name;
                                    UserModel_1.UserModel.findOneAndUpdate({ _id: req.user._id }, new_data, { new: true })
                                        .then(user => {
                                        apiResponse.setDate(user.getApiFields());
                                        res.apiJson(apiResponse.get());
                                    })
                                        .catch(err => {
                                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    }
    Route.ProfileMethods = ProfileMethods;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
const ApiResponse_1 = require("../../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const UserModel_1 = require("../../../models/UserModel");
const ApiHelper_1 = require("../../ApiHelper");
const PaymentModule_1 = require("../../../components/jabrool/PaymentModule");
const CardLabelModel_1 = require("../../../models/CardLabelModel");
const SaveCardPost_1 = require("../../postModels/SaveCardPost");
const RemoveCardPost_1 = require("../../postModels/RemoveCardPost");
var Route;
(function (Route) {
    class CardsMethods {
        cardsList(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            if (!req.user.bt_customer_id || req.user.bt_customer_id === "") {
                apiResponse.setDate([]);
                res.apiJson(apiResponse.get());
                return;
            }
            PaymentModule_1.PaymentModule
                .listCards(req.user.bt_customer_id)
                .then(result => {
                let cards = [];
                if (result.creditCards.length > 0) {
                    let uids = [];
                    for (let i = 0; i < result.creditCards.length; i++) {
                        uids.push(result.creditCards[i].uniqueNumberIdentifier);
                    }
                    CardLabelModel_1.CardLabelModel
                        .find({ uniqueNumberIdentifier: { $in: uids } })
                        .then((labels) => {
                        let labels_arr = {};
                        for (let i = 0; i < labels.length; i++) {
                            labels_arr[labels[i].uniqueNumberIdentifier] = labels[i].label;
                        }
                        for (let i = 0; i < result.creditCards.length; i++) {
                            let label = "";
                            if (labels_arr[result.creditCards[i].uniqueNumberIdentifier]) {
                                label = labels_arr[result.creditCards[i].uniqueNumberIdentifier];
                            }
                            cards.push({
                                cardType: result.creditCards[i].cardType,
                                uniqueNumberIdentifier: result.creditCards[i].uniqueNumberIdentifier,
                                imageUrl: result.creditCards[i].imageUrl,
                                maskedNumber: result.creditCards[i].maskedNumber,
                                default: (req.user.pay_type === "card") ? result.creditCards[i].default : false,
                                label: label
                            });
                        }
                        apiResponse.setDate(cards);
                        res.apiJson(apiResponse.get());
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
                else {
                    apiResponse.setDate([]);
                    res.apiJson(apiResponse.get());
                    return;
                }
            })
                .catch(err => {
                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
            });
        }
        saveCard(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(SaveCardPost_1.SaveCardPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let promise;
                    if (req.user.bt_customer_id) {
                        promise = PaymentModule_1.PaymentModule.createPayment(req.user.bt_customer_id, post.payment_method_nonce);
                    }
                    else {
                        promise = PaymentModule_1.PaymentModule.createCustomerAndPayment(req.user.first_name, req.user.last_name, req.user.email, req.user.phone, post.payment_method_nonce);
                    }
                    promise.then(result => {
                        console.log(result);
                        if (!result.success) {
                            apiResponse.addErrorMessage("payment_method_nonce", result.message);
                            res.apiJson(apiResponse.get(result.message));
                        }
                        else {
                            let customerId = result.customer.id;
                            UserModel_1.UserModel.findOneAndUpdate({ _id: req.user._id }, { bt_customer_id: customerId })
                                .then(doc => {
                                let label = new CardLabelModel_1.CardLabelModel({
                                    uniqueNumberIdentifier: result.customer.creditCards[0].uniqueNumberIdentifier,
                                    label: post.label
                                });
                                label
                                    .save()
                                    .then(r => {
                                    apiResponse.setDate({
                                        cardType: result.customer.creditCards[0].cardType,
                                        uniqueNumberIdentifier: result.customer.creditCards[0].uniqueNumberIdentifier,
                                        imageUrl: result.customer.creditCards[0].imageUrl,
                                        maskedNumber: result.customer.creditCards[0].maskedNumber,
                                        label: post.label
                                    });
                                    res.apiJson(apiResponse.get());
                                })
                                    .catch(err => {
                                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                });
                            })
                                .catch(err => {
                                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                            });
                        }
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        removeCard(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(RemoveCardPost_1.RemoveCardPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    PaymentModule_1.PaymentModule.removeCard(req.user.bt_customer_id, post.uniqueNumberIdentifier)
                        .then(result => {
                        if (!result.success) {
                            apiResponse.addErrorMessage("uniqueNumberIdentifier", result.message);
                            res.apiJson(apiResponse.get(result.message));
                        }
                        else {
                            if (!result.creditCards || result.creditCards.length < 1) {
                                UserModel_1.UserModel.findOneAndUpdate({ _id: req.user._id }, {
                                    pay_type: "cash",
                                    defaultUniqueNumberIdentifier: ""
                                }, { new: true })
                                    .then(user => {
                                    res.apiJson(apiResponse.get());
                                })
                                    .catch(err => {
                                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                });
                            }
                            else if (post.uniqueNumberIdentifier === req.user.defaultUniqueNumberIdentifier) {
                                PaymentModule_1.PaymentModule
                                    .setDefault(req.user.bt_customer_id, result.creditCards[0].uniqueNumberIdentifier)
                                    .then(r => {
                                    UserModel_1.UserModel.findOneAndUpdate({ _id: req.user._id }, {
                                        pay_type: "card",
                                        defaultUniqueNumberIdentifier: result.creditCards[0].uniqueNumberIdentifier
                                    }, { new: true })
                                        .then(user => {
                                        res.apiJson(apiResponse.get());
                                    })
                                        .catch(err => {
                                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                    });
                                }).catch(err => {
                                    apiResponse.addErrorMessage("uniqueNumberIdentifier", err.message);
                                    res.apiJson(apiResponse.get(err.message));
                                });
                            }
                            else {
                                res.apiJson(apiResponse.get());
                            }
                        }
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
    }
    Route.CardsMethods = CardsMethods;
})(Route || (Route = {}));
module.exports = Route;

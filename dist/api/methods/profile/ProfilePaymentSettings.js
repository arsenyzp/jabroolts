"use strict";
const ApiResponse_1 = require("../../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const UserModel_1 = require("../../../models/UserModel");
const ApiHelper_1 = require("../../ApiHelper");
const PaymentModule_1 = require("../../../components/jabrool/PaymentModule");
const SetPayTypePost_1 = require("../../postModels/SetPayTypePost");
var Route;
(function (Route) {
    class ProfilePaymentSettings {
        getToken(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            PaymentModule_1.PaymentModule
                .getToken()
                .then(token => {
                apiResponse.setDate({ token: token });
                res.apiJson(apiResponse.get());
            })
                .catch(err => {
                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
            });
        }
        setPayType(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(SetPayTypePost_1.SetPayTypePost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    console.log("setPayType");
                    UserModel_1.UserModel.findOneAndUpdate({ _id: req.user._id }, {
                        pay_type: post.type,
                        defaultUniqueNumberIdentifier: post.uniqueNumberIdentifier
                    }, { new: true })
                        .then(user => {
                        if (user.pay_type === "card" &&
                            post.uniqueNumberIdentifier &&
                            post.uniqueNumberIdentifier !== "" &&
                            user.bt_customer_id &&
                            user.bt_customer_id !== "") {
                            PaymentModule_1.PaymentModule.setDefault(req.user.bt_customer_id, post.uniqueNumberIdentifier).then(r => {
                                apiResponse.setDate(user.getApiFields());
                                res.apiJson(apiResponse.get());
                            }).catch(err => {
                                apiResponse.addErrorMessage("uniqueNumberIdentifier", err.message);
                                res.apiJson(apiResponse.get(err.message));
                            });
                        }
                        else {
                            apiResponse.setDate(user.getApiFields());
                            res.apiJson(apiResponse.get());
                        }
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
    }
    Route.ProfilePaymentSettings = ProfilePaymentSettings;
})(Route || (Route = {}));
module.exports = Route;

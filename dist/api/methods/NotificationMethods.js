"use strict";
const ApiResponse_1 = require("../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const UserModel_1 = require("../../models/UserModel");
const ApiHelper_1 = require("../ApiHelper");
const Log_1 = require("../../components/Log");
const LimitOffsetOnlyPost_1 = require("../postModels/LimitOffsetOnlyPost");
const NotificationModel_1 = require("../../models/NotificationModel");
const NotificationModule_1 = require("../../components/NotificationModule");
const log = Log_1.Log.getInstanse()(module);
var Route;
(function (Route) {
    class NotificationMethods {
        index(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(LimitOffsetOnlyPost_1.LimitOffsetOnlyPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    NotificationModel_1.NotificationModel
                        .find({ user: req.user._id, created_at: { $gte: (new Date().getTime() - 7 * 24 * 60 * 60 * 1000) } })
                        .skip(post.offset)
                        .limit(post.limit)
                        .sort({ created_at: -1 })
                        .then((its) => {
                        let items = [];
                        let ids = [];
                        for (let i = 0; i < its.length; i++) {
                            items.push(its[i].getPublicFields());
                            ids.push(its[i]._id);
                        }
                        NotificationModel_1.NotificationModel.update({ user: req.user._id, type: { $nin: [
                                    NotificationModule_1.NotificationTypes.Delivered,
                                    NotificationModule_1.NotificationTypes.JabroolDebt
                                ] } }, { is_view: true }, { multi: true })
                            .then(_ => {
                            UserModel_1.UserModel.findOneAndUpdate({ _id: req.user._id }, { unread_count: 0 })
                                .then(_ => {
                                apiResponse.setDate(items);
                                res.apiJson(apiResponse.get());
                            })
                                .catch((err) => {
                                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                            });
                        })
                            .catch((err) => {
                            ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                        });
                    })
                        .catch((err) => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
    }
    Route.NotificationMethods = NotificationMethods;
})(Route || (Route = {}));
module.exports = Route;

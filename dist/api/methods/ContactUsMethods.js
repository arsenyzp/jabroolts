"use strict";
const ApiResponse_1 = require("../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const ApiHelper_1 = require("../ApiHelper");
const ContactUsPost_1 = require("../postModels/ContactUsPost");
const ContactUsModel_1 = require("../../models/ContactUsModel");
var Route;
(function (Route) {
    class ContactUsMethods {
        contact(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(ContactUsPost_1.ContactUsPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let model = new ContactUsModel_1.ContactUsModel();
                    model.phone = post.number;
                    model.email = post.email;
                    model.text = post.text;
                    model.name = post.name;
                    model
                        .save()
                        .then(m => {
                        apiResponse.setDate(m.getPublicFields());
                        res.apiJson(apiResponse.get());
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
    }
    Route.ContactUsMethods = ContactUsMethods;
})(Route || (Route = {}));
module.exports = Route;

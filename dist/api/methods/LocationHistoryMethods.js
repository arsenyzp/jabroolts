"use strict";
const ApiResponse_1 = require("../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const ApiHelper_1 = require("../ApiHelper");
const HistoryLocationModel_1 = require("../../models/HistoryLocationModel");
const LimitOffsetOnlyPost_1 = require("../postModels/LimitOffsetOnlyPost");
var Route;
(function (Route) {
    class LocationHistoryMethods {
        index(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(LimitOffsetOnlyPost_1.LimitOffsetOnlyPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    HistoryLocationModel_1.HistoryLocationModel
                        .find({ user: req.user._id })
                        .skip(post.offset)
                        .limit(post.limit)
                        .sort({ created_at: -1 })
                        .then((its) => {
                        let items = [];
                        for (let i = 0; i < its.length; i++) {
                            items.push(its[i].getPublicFields());
                        }
                        apiResponse.setDate(items);
                        res.apiJson(apiResponse.get());
                    })
                        .catch((err) => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
    }
    Route.LocationHistoryMethods = LocationHistoryMethods;
})(Route || (Route = {}));
module.exports = Route;

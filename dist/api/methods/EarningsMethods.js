"use strict";
const ApiResponse_1 = require("../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const UserModel_1 = require("../../models/UserModel");
const ApiHelper_1 = require("../ApiHelper");
const OrderModel_1 = require("../../models/OrderModel");
const EarningsWeekPost_1 = require("../postModels/EarningsWeekPost");
const DateHelper_1 = require("../../components/DateHelper");
const EarningsMonthPost_1 = require("../postModels/EarningsMonthPost");
const EarningsYearPost_1 = require("../postModels/EarningsYearPost");
var Route;
(function (Route) {
    class EarningsMethods {
        week(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(EarningsWeekPost_1.EarningsWeekPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let date = DateHelper_1.DateHelper.getDateOfWeek(post.week, post.year);
                    date.setUTCHours(0);
                    date.setUTCMinutes(0);
                    date.setUTCSeconds(0);
                    date.setUTCMilliseconds(0);
                    let time_start = date.getTime();
                    date.setUTCDate(date.getUTCDate() + 7);
                    let time_end = date.getTime();
                    let filter = {
                        courier: req.user._id,
                        status: OrderModel_1.OrderStatuses.Finished
                    };
                    filter.end_at = { $gte: time_start, $lte: time_end };
                    OrderModel_1.OrderModel
                        .find(filter)
                        .populate({ path: "courier", model: UserModel_1.UserModel })
                        .populate({ path: "owner", model: UserModel_1.UserModel })
                        .sort({ end_at: -1 })
                        .then((orders) => {
                        let items = [];
                        for (let i = 0; i < orders.length; i++) {
                            let order = orders[i].getApiCourierFields();
                            items.push(order);
                        }
                        apiResponse.setDate(items);
                        res.apiJson(apiResponse.get());
                    })
                        .catch((err) => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        month(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(EarningsMonthPost_1.EarningsMonthPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let filter = {
                        courier: req.user._id,
                        status: OrderModel_1.OrderStatuses.Finished
                    };
                    let date = new Date();
                    date.setUTCFullYear(post.year);
                    date.setUTCMonth(post.month);
                    date.setUTCDate(1);
                    date.setUTCHours(0);
                    date.setUTCMinutes(0);
                    date.setUTCSeconds(0);
                    date.setUTCMilliseconds(0);
                    let time_start = date.getTime();
                    date.setUTCMonth(req.body.month + 1);
                    let time_end = date.getTime();
                    filter.end_at = { $gte: time_start, $lte: time_end };
                    OrderModel_1.OrderModel.find(filter)
                        .sort({ end_at: -1 })
                        .then(orders => {
                        let items = {};
                        let items_format = [];
                        for (let i = 0; i < orders.length; i++) {
                            let order = orders[i];
                            let created_at = new Date(order.end_at);
                            let week = DateHelper_1.DateHelper.getWeekNumber(created_at);
                            if (!items[week]) {
                                items[week] = {
                                    week: week,
                                    week_in_month: DateHelper_1.DateHelper.wInM(created_at),
                                    earning: 0,
                                    bonus: 0
                                };
                            }
                            items[week].earning += order.cost - order.serviceFee;
                        }
                        for (const i of Object.keys(items)) {
                            items_format.push(items[i]);
                        }
                        apiResponse.setDate({
                            weeks: items_format
                        });
                        res.apiJson(apiResponse.get());
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        year(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(EarningsYearPost_1.EarningsYearPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let filter = {
                        courier: req.user._id,
                        status: OrderModel_1.OrderStatuses.Finished
                    };
                    let date = new Date();
                    date.setUTCFullYear(post.year);
                    date.setUTCMonth(0);
                    date.setUTCDate(1);
                    date.setUTCHours(0);
                    date.setUTCMinutes(0);
                    date.setUTCSeconds(0);
                    date.setUTCMilliseconds(0);
                    let time_start = date.getTime();
                    date.setUTCFullYear(post.year + 1);
                    let time_end = date.getTime();
                    filter.end_at = { $gte: time_start, $lte: time_end };
                    console.log(filter);
                    OrderModel_1.OrderModel.find(filter)
                        .sort({ end_at: -1 })
                        .then(orders => {
                        console.log("Orders y: " + orders.length);
                        let items = {};
                        let items_format = [];
                        let total = 0;
                        for (let i = 0; i < orders.length; i++) {
                            let order = orders[i];
                            let created_at = new Date(order.end_at);
                            let month = created_at.getMonth();
                            if (!items[month + ""]) {
                                items[month + ""] = {
                                    month: month,
                                    earning: 0,
                                    bonus: 0
                                };
                            }
                            items[month + ""].earning += (order.cost - order.serviceFee);
                            total += (order.cost - order.serviceFee);
                        }
                        for (const i of Object.keys(items)) {
                            items_format.push(items[i]);
                        }
                        apiResponse.setDate({
                            total: total,
                            months: items_format,
                            jabroolFee: req.user.jabroolFee,
                            courierLimit: req.user.courierLimit
                        });
                        res.apiJson(apiResponse.get());
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
    }
    Route.EarningsMethods = EarningsMethods;
})(Route || (Route = {}));
module.exports = Route;

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const ApiResponse_1 = require("../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const ApiHelper_1 = require("../ApiHelper");
const PromoCodHelper_1 = require("../../components/jabrool/PromoCodHelper");
const CheckPromoPost_1 = require("../postModels/CheckPromoPost");
const PaymentLog_1 = require("../../models/PaymentLog");
const SavePromoPost_1 = require("../postModels/SavePromoPost");
const NotificationModel_1 = require("../../models/NotificationModel");
const NotificationModule_1 = require("../../components/NotificationModule");
const UserModel_1 = require("../../models/UserModel");
var Route;
(function (Route) {
    class PromoMethods {
        checkPromo(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(CheckPromoPost_1.CheckPromoPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let type = req.header("application-type");
                    PromoCodHelper_1.PromoCodHelper.check(post.code, type).then(result => {
                        switch (result.status) {
                            case PromoCodHelper_1.PromoCodeStatus.NotFound:
                                apiResponse.addErrorMessage("code", req.__("api.errors.NotFoundCode"));
                                res.apiJson(apiResponse.get(req.__("api.errors.NotFoundCode")));
                                break;
                            case PromoCodHelper_1.PromoCodeStatus.Used:
                                apiResponse.addErrorMessage("code", req.__("api.errors.UsedCode"));
                                res.apiJson(apiResponse.get(req.__("api.errors.UsedCode")));
                                break;
                            case PromoCodHelper_1.PromoCodeStatus.Ready:
                                apiResponse.setDate({ discount: result.promo.amount });
                                break;
                        }
                        res.apiJson(apiResponse.get());
                    }).catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        dayPromo(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let date = new Date();
            date.setUTCHours(0);
            date.setUTCMinutes(0);
            date.setUTCSeconds(0);
            date.setUTCMilliseconds(0);
            let time = date.getTime();
            PaymentLog_1.PaymentLogModel
                .find({
                user: req.user._id,
                status: PaymentLog_1.PaymentStatuses.Success,
                type: { $in: [PaymentLog_1.PaymentTypes.Promo, PaymentLog_1.PaymentTypes.Invite, PaymentLog_1.PaymentTypes.Admin] },
                created_at: { $gte: time }
            })
                .then((logs) => {
                let amount = 0;
                for (let i = 0; i < logs.length; i++) {
                    amount += logs[i].amount;
                }
                apiResponse.setDate({
                    bonus: req.user.balance,
                    jabroolFee: req.user.jabroolFee,
                    courierLimit: req.user.courierLimit
                });
                res.apiJson(apiResponse.get());
            })
                .catch((err) => {
                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
            });
        }
        save(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let apiResponse = new ApiResponse_1.ApiResponse();
                let post = Object.create(SavePromoPost_1.SavePromoPost.prototype);
                Object.assign(post, req.body, {});
                try {
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        apiResponse.addValidationErrors(req, errors);
                        res.apiJson(apiResponse.get());
                    }
                    else {
                        let type = req.header("application-type");
                        let result = yield PromoCodHelper_1.PromoCodHelper.use(req.user, post.code, type);
                        switch (result.status) {
                            case PromoCodHelper_1.PromoCodeStatus.NotFound:
                                apiResponse.addErrorMessage("code", req.__("api.errors.NotFoundCode"));
                                res.apiJson(apiResponse.get(req.__("api.errors.NotFoundCode")));
                                break;
                            case PromoCodHelper_1.PromoCodeStatus.Used:
                                apiResponse.addErrorMessage("code", req.__("api.errors.UsedCode"));
                                res.apiJson(apiResponse.get(req.__("api.errors.UsedCode")));
                                break;
                            case PromoCodHelper_1.PromoCodeStatus.Ready:
                                if (req.user.balance < 0) {
                                    let user = yield UserModel_1.UserModel.findOne({ _id: req.user._id });
                                    if (user !== null && user.balance >= 0) {
                                        yield NotificationModel_1.NotificationModel.update({ user: req.user._id, type: { $in: [
                                                    NotificationModule_1.NotificationTypes.JabroolDebt
                                                ] } }, { is_view: true }, { multi: true });
                                    }
                                }
                                apiResponse.setDate({ discount: result.promo.amount });
                                break;
                        }
                        res.apiJson(apiResponse.get());
                    }
                }
                catch (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
            });
        }
        getCourierLimit(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            apiResponse.setDate({
                jabroolFee: req.user.jabroolFee,
                courierLimit: req.user.courierLimit
            });
            res.apiJson(apiResponse.get());
        }
    }
    Route.PromoMethods = PromoMethods;
})(Route || (Route = {}));
module.exports = Route;

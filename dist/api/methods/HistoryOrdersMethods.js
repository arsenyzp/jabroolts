"use strict";
const ApiResponse_1 = require("../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const UserModel_1 = require("../../models/UserModel");
const ApiHelper_1 = require("../ApiHelper");
const LimitOffsetOnlyPost_1 = require("../postModels/LimitOffsetOnlyPost");
const OrderModel_1 = require("../../models/OrderModel");
var Route;
(function (Route) {
    class HistoryOrdersMethods {
        index(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(LimitOffsetOnlyPost_1.LimitOffsetOnlyPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let filter = {
                        $or: [
                            { owner: req.user._id }
                        ],
                        status: { $in: [
                                OrderModel_1.OrderStatuses.Finished
                            ] }
                    };
                    OrderModel_1.OrderModel
                        .find(filter)
                        .populate({ path: "courier", model: UserModel_1.UserModel })
                        .populate({ path: "recipient", model: UserModel_1.UserModel })
                        .populate({ path: "owner", model: UserModel_1.UserModel })
                        .skip(post.offset)
                        .limit(post.limit)
                        .sort({ created_at: -1 })
                        .then((orders) => {
                        let items = [];
                        for (let i = 0; i < orders.length; i++) {
                            let order = orders[i].getApiFields();
                            items.push(order);
                        }
                        apiResponse.setDate(items);
                        res.apiJson(apiResponse.get());
                    })
                        .catch((err) => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
    }
    Route.HistoryOrdersMethods = HistoryOrdersMethods;
})(Route || (Route = {}));
module.exports = Route;

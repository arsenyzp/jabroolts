"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const async = require("async");
const ApiResponse_1 = require("../../components/ApiResponse");
const ApiHelper_1 = require("../ApiHelper");
const BankTypeModel_1 = require("../../models/BankTypeModel");
const BankModel_1 = require("../../models/BankModel");
const CountryModel_1 = require("../../models/CountryModel");
const ManufactureModel_1 = require("../../models/ManufactureModel");
const CarTypeModel_1 = require("../../models/CarTypeModel");
const PackageConfigPrice_1 = require("../../models/configs/PackageConfigPrice");
const TermsConfig_1 = require("../../models/configs/TermsConfig");
const class_validator_1 = require("class-validator");
const GetCarTypesPost_1 = require("../postModels/GetCarTypesPost");
var Route;
(function (Route) {
    class CatalogMethods {
        bank(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let countries = [];
            let banks = [];
            let bank_types = [];
            async.parallel([
                    cb => {
                    BankTypeModel_1.BankTypeModel.find({}).then((items) => {
                        for (let i = 0; i < items.length; i++) {
                            bank_types.push(items[i].getPublicFields());
                        }
                        cb();
                    }).catch((err) => {
                        cb(err);
                    });
                },
                    cb => {
                    BankModel_1.BankModel.find({}).sort({ name: 1 }).then((items) => {
                        for (let i = 0; i < items.length; i++) {
                            banks.push(items[i].getPublicFields());
                        }
                        cb();
                    }).catch((err) => {
                        cb(err);
                    });
                },
                    cb => {
                    CountryModel_1.CountryModel.find({}).sort({ name: 1 }).then((items) => {
                        for (let i = 0; i < items.length; i++) {
                            countries.push(items[i].getPublicFields());
                        }
                        cb();
                    }).catch((err) => {
                        cb(err);
                    });
                },
            ], err => {
                if (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
                else {
                    apiResponse.setDate({
                        countries: countries,
                        banks: banks,
                        bank_types: bank_types
                    });
                    res.apiJson(apiResponse.get());
                }
            });
        }
        countries(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let countries = [];
            CountryModel_1.CountryModel.find({}).sort({ name: 1 }).then((items) => {
                for (let i = 0; i < items.length; i++) {
                    countries.push(items[i].getPublicFields());
                }
                apiResponse.setDate({
                    countries: countries
                });
                res.apiJson(apiResponse.get());
            }).catch((err) => {
                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
            });
        }
        catalogs(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let countries = [];
            let manufacturers = [];
            async.parallel([
                    cb => {
                    ManufactureModel_1.ManufactureModel.find({}).sort({ name: 1 }).then((items) => {
                        for (let i = 0; i < items.length; i++) {
                            manufacturers.push(items[i].getPublicFields());
                        }
                        cb();
                    }).catch((err) => {
                        cb(err);
                    });
                },
                    cb => {
                    CountryModel_1.CountryModel.find({}).sort({ name: 1 }).then((items) => {
                        for (let i = 0; i < items.length; i++) {
                            countries.push(items[i].getPublicFields());
                        }
                        cb();
                    }).catch((err) => {
                        cb(err);
                    });
                },
            ], err => {
                if (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
                else {
                    apiResponse.setDate({
                        countries: countries,
                        types: manufacturers
                    });
                    res.apiJson(apiResponse.get());
                }
            });
        }
        сarTypes(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let apiResponse = new ApiResponse_1.ApiResponse();
                let post = Object.create(GetCarTypesPost_1.GetCarTypesPost.prototype);
                if (req.body.a) {
                    req.body.id = req.body.a;
                }
                Object.assign(post, req.body, {});
                try {
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        apiResponse.addValidationErrors(req, errors);
                        res.apiJson(apiResponse.get());
                    }
                    else {
                        let manufacturer = yield ManufactureModel_1.ManufactureModel
                            .findOne({ _id: post.id });
                        let types = yield CarTypeModel_1.CarTypeModel
                            .find({ _id: { $in: manufacturer.types } })
                            .sort({ name: 1 });
                        let typesData = [];
                        types.map((v, k, a) => {
                            typesData.push(v.getPublicFields());
                        });
                        apiResponse.setDate({
                            models: typesData
                        });
                        res.apiJson(apiResponse.get());
                    }
                }
                catch (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
            });
        }
        packages(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let confPackages = new PackageConfigPrice_1.PackageConfigPrice();
            confPackages
                .getInstanse()
                .then(c => {
                apiResponse.setDate(c);
                res.apiJson(apiResponse.get());
            })
                .catch(err => {
                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
            });
        }
        terms(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let conf = new TermsConfig_1.TermsConfig();
            conf
                .getInstanse()
                .then(c => {
                let json = {
                    terms: "",
                    privacy: ""
                };
                if (!!req.header("local") && req.header("local").indexOf("ar") >= 0) {
                    json.terms = c.terms_customer_ar;
                    json.privacy = c.privacy_ar;
                }
                else {
                    json.terms = c.terms_customer_en;
                    json.privacy = c.privacy_en;
                }
                if (req.header("application-type") === "courier") {
                    if (!!req.header("local") && req.header("local").indexOf("ar") >= 0) {
                        json.terms = c.terms_courier_ar;
                        json.privacy = c.privacy_ar;
                    }
                    else {
                        json.terms = c.terms_courier_en;
                        json.privacy = c.privacy_en;
                    }
                }
                apiResponse.setDate(json);
                res.apiJson(apiResponse.get());
            })
                .catch(err => {
                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
            });
        }
    }
    Route.CatalogMethods = CatalogMethods;
})(Route || (Route = {}));
module.exports = Route;

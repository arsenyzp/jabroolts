"use strict";
const ApiResponse_1 = require("../../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const ApiHelper_1 = require("../../ApiHelper");
const CalculatePost_1 = require("../../postModels/CalculatePost");
const CalculateCost_1 = require("../../../components/jabrool/CalculateCost");
const CreateOrderPost_1 = require("../../postModels/CreateOrderPost");
const PromoCodHelper_1 = require("../../../components/jabrool/PromoCodHelper");
const Log_1 = require("../../../components/Log");
const CreateOrderHelper_1 = require("../../../components/jabrool/CreateOrderHelper");
const log = Log_1.Log.getInstanse()(module);
var Route;
(function (Route) {
    class CreateOrderMethods {
        calculate(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(CalculatePost_1.CalculatePost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    if (!post.small_package_count && !post.medium_package_count && !post.large_package_count) {
                        apiResponse.addErrorMessage("small_package_count", req.__("api.errors.InvalidOrderSize"));
                        apiResponse.addErrorMessage("medium_package_count", req.__("api.errors.InvalidOrderSize"));
                        apiResponse.addErrorMessage("large_package_count", req.__("api.errors.InvalidOrderSize"));
                        res.apiJson(apiResponse.get(req.__("api.errors.InvalidOrderSize")));
                    }
                    else {
                        CalculateCost_1.CalculateCost
                            .process(post.route, post.small_package_count, post.medium_package_count, post.large_package_count)
                            .then(price => {
                            apiResponse.setDate({
                                jabrool: price.j_cost,
                                express: price.e_cost
                            });
                            res.apiJson(apiResponse.get());
                        })
                            .catch(err => {
                            ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                        });
                    }
                }
            });
        }
        create(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(CreateOrderPost_1.CreateOrderPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    if (post.code) {
                        PromoCodHelper_1.PromoCodHelper
                            .block(req.user, post.code)
                            .then(result => {
                            switch (result.status) {
                                case PromoCodHelper_1.PromoCodeStatus.NotFound:
                                    apiResponse.addErrorMessage("code", req.__("api.errors.PromoCodeNotFound"));
                                    res.apiJson(apiResponse.get(req.__("api.errors.PromoCodeNotFound")));
                                    break;
                                case PromoCodHelper_1.PromoCodeStatus.Used:
                                    apiResponse.addErrorMessage("code", req.__("api.errors.PromoCodeUsed"));
                                    res.apiJson(apiResponse.get(req.__("api.errors.PromoCodeUsed")));
                                    break;
                                default:
                                    CreateOrderHelper_1.CreateOrderHelper.findRecipient(req, res, post, apiResponse, result.promo);
                            }
                        }).catch(err => {
                            ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                        });
                    }
                    else {
                        CreateOrderHelper_1.CreateOrderHelper.findRecipient(req, res, post, apiResponse);
                    }
                }
            });
        }
    }
    Route.CreateOrderMethods = CreateOrderMethods;
})(Route || (Route = {}));
module.exports = Route;

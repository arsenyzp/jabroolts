"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const ApiResponse_1 = require("../../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const OrderModel_1 = require("../../../models/OrderModel");
const ChangeOrderPost_1 = require("../../postModels/ChangeOrderPost");
const CalculateCost_1 = require("../../../components/jabrool/CalculateCost");
const SocketServer_1 = require("../../../components/SocketServer");
const ApiHelper_1 = require("../../ApiHelper");
const CustomerAcceptChangePost_1 = require("../../postModels/CustomerAcceptChangePost");
const PromoCodModel_1 = require("../../../models/PromoCodModel");
const OrderHelper_1 = require("../../../components/jabrool/OrderHelper");
const NotificationModule_1 = require("../../../components/NotificationModule");
const UserModel_1 = require("../../../models/UserModel");
const UpdateNumberPost_1 = require("../../postModels/UpdateNumberPost");
const NewPaymentLog_1 = require("../../../components/NewPaymentLog");
var Route;
(function (Route) {
    class ChangeOrderMethods {
        courierChangeOrder(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let apiResponse = new ApiResponse_1.ApiResponse();
                try {
                    let post = Object.create(ChangeOrderPost_1.ChangeOrderPost.prototype);
                    Object.assign(post, req.body, {});
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        apiResponse.addValidationErrors(req, errors);
                        res.apiJson(apiResponse.get());
                    }
                    else {
                        let recipient_location = { type: "Point", coordinates: [req.body.recipient_lon, req.body.recipient_lat] };
                        let order = yield OrderModel_1.OrderModel.findOne({
                            _id: req.body.id,
                            courier: req.user._id,
                            status: { $in: [OrderModel_1.OrderStatuses.WaitPickUp, OrderModel_1.OrderStatuses.WaitCustomerAccept] }
                        });
                        if (order === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else if (!post.small_package_count && !post.medium_package_count && !post.large_package_count) {
                            apiResponse.addErrorMessage("small_package_count", req.__("api.errors.InvalidOrderSize"));
                            apiResponse.addErrorMessage("medium_package_count", req.__("api.errors.InvalidOrderSize"));
                            apiResponse.addErrorMessage("large_package_count", req.__("api.errors.InvalidOrderSize"));
                            res.apiJson(apiResponse.get(req.__("api.errors.InvalidOrderSize")));
                        }
                        else {
                            let price = yield CalculateCost_1.CalculateCost
                                .process(post.route, post.small_package_count, post.medium_package_count, post.large_package_count);
                            let serviceFee = price.j_serviceFee;
                            let cost = price.j_cost;
                            if (order.type === "express") {
                                cost = price.e_cost;
                                serviceFee = price.e_serviceFee;
                            }
                            let returnCost = cost / 2;
                            let updatedOrder = yield OrderModel_1.OrderModel.findOneAndUpdate({
                                _id: req.body.id,
                                courier: req.user._id
                            }, {
                                recipient_location: recipient_location,
                                small_package_count: post.small_package_count,
                                medium_package_count: post.medium_package_count,
                                large_package_count: post.large_package_count,
                                recipient_address: post.recipient_address,
                                status: OrderModel_1.OrderStatuses.WaitCustomerAccept,
                                cost: cost,
                                serviceFee: serviceFee,
                                returnCost: returnCost,
                                route: post.route
                            }, { new: true });
                            if (updatedOrder === null) {
                                apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                                res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                            }
                            else {
                                let isChanged = false;
                                if (order.recipient_location.coordinates[0] !== recipient_location.coordinates[0]) {
                                    isChanged = true;
                                }
                                if (order.recipient_location.coordinates[1] !== recipient_location.coordinates[1]) {
                                    isChanged = true;
                                }
                                if (order.small_package_count !== post.small_package_count) {
                                    isChanged = true;
                                }
                                if (order.medium_package_count !== post.medium_package_count) {
                                    isChanged = true;
                                }
                                if (order.large_package_count !== post.large_package_count) {
                                    isChanged = true;
                                }
                                if (order.recipient_address !== post.recipient_address) {
                                    isChanged = true;
                                }
                                SocketServer_1.SocketServer.emit([updatedOrder.owner.toString()], "order_update", {
                                    order: updatedOrder.getPublicFields(),
                                    updated: isChanged
                                });
                                NotificationModule_1.NotificationModule.getInstance().send([order.owner.toString()], NotificationModule_1.NotificationTypes.CourierChanged, {
                                    isChanged: isChanged
                                }, order._id.toString());
                                apiResponse.setDate(updatedOrder.getPublicFields());
                                res.apiJson(apiResponse.get());
                            }
                        }
                    }
                }
                catch (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
            });
        }
        customerAcceptChange(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let apiResponse = new ApiResponse_1.ApiResponse();
                try {
                    let post = Object.create(CustomerAcceptChangePost_1.CustomerAcceptChangePost.prototype);
                    Object.assign(post, req.body, {});
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        apiResponse.addValidationErrors(req, errors);
                        res.apiJson(apiResponse.get());
                    }
                    else {
                        let order = yield OrderModel_1.OrderModel.findOneAndUpdate({
                            _id: post.id,
                            owner: req.user._id,
                            status: { $in: [OrderModel_1.OrderStatuses.WaitCustomerAccept, OrderModel_1.OrderStatuses.WaitCustomerPayment] }
                        }, {
                            status: OrderModel_1.OrderStatuses.WaitCustomerPayment
                        }, { new: true });
                        if (order === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            return res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        if (order.paid) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            return res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        let full_cost = order.cost;
                        if (order.code) {
                            let code = yield PromoCodModel_1.PromoCodModel
                                .findOne({ _id: order.code });
                            if (code === null) {
                                apiResponse.addErrorMessage("code", req.__("api.errors.IncorrectPromoCode"));
                                res.apiJson(apiResponse.get(req.__("api.errors.IncorrectPromoCode")));
                            }
                            else {
                                full_cost = full_cost - code.amount;
                                full_cost = full_cost >= 0 ? full_cost : 0;
                            }
                        }
                        yield OrderHelper_1.OrderHelper
                            .paymentProcessNew(req.user, order, full_cost);
                        if (order.pay_type === "card") {
                            NotificationModule_1.NotificationModule.getInstance().send([order.owner.toString()], NotificationModule_1.NotificationTypes.PaymentMade, {}, order._id.toString());
                            NotificationModule_1.NotificationModule.getInstance().send([order.courier.toString()], NotificationModule_1.NotificationTypes.PaymentMade, {}, order._id.toString());
                        }
                        let updatedOrder = yield OrderModel_1.OrderModel.findOne({ _id: order._id })
                            .populate({ path: "owner", model: UserModel_1.UserModel })
                            .populate({ path: "courier", model: UserModel_1.UserModel });
                        let it = updatedOrder.getPublicFields();
                        SocketServer_1.SocketServer.emit([order.courier.toString()], "payment_made", it);
                        yield NewPaymentLog_1.NewPaymentLog.saveCustomerOrder(updatedOrder.owner, updatedOrder.cost, "success", updatedOrder.pay_type, updatedOrder);
                        yield NewPaymentLog_1.NewPaymentLog.saveCourierOrder(updatedOrder.courier, updatedOrder, updatedOrder.cost, "success");
                        if (updatedOrder.userCredit > 0) {
                            yield NewPaymentLog_1.NewPaymentLog.saveCustomerDebt(updatedOrder.owner, updatedOrder.userCredit, "success", updatedOrder.pay_type, updatedOrder);
                            yield NewPaymentLog_1.NewPaymentLog.saveCourierDebt(updatedOrder.courier, updatedOrder, updatedOrder.userCredit, "success");
                        }
                        apiResponse.setDate(updatedOrder.getApiFields());
                        res.apiJson(apiResponse.get());
                    }
                }
                catch (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
            });
        }
        updateRecipientPhone(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let apiResponse = new ApiResponse_1.ApiResponse();
                let post = Object.create(UpdateNumberPost_1.UpdateNumberPost.prototype);
                Object.assign(post, req.body, {});
                try {
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        apiResponse.addValidationErrors(req, errors);
                        res.apiJson(apiResponse.get());
                    }
                    else {
                        let doc = yield OrderModel_1.OrderModel.findOneAndUpdate({
                            _id: req.body.id,
                            owner: req.user._id
                        }, {
                            recipient_contact: post.phone
                        }, { new: true });
                        if (doc === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            if (!!doc.courier) {
                                SocketServer_1.SocketServer.emit([doc.courier.toString()], "number_updated", {
                                    order: doc.getPublicFields()
                                });
                                NotificationModule_1.NotificationModule.getInstance().send([doc.courier.toString()], NotificationModule_1.NotificationTypes.NumberUpdated, {}, doc._id.toString());
                            }
                            apiResponse.setDate(doc.getPublicFields());
                            res.apiJson(apiResponse.get());
                        }
                    }
                }
                catch (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
            });
        }
    }
    Route.ChangeOrderMethods = ChangeOrderMethods;
})(Route || (Route = {}));
module.exports = Route;

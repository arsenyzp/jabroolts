"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const ApiResponse_1 = require("../../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const UserModel_1 = require("../../../models/UserModel");
const ApiHelper_1 = require("../../ApiHelper");
const ConfirmPickupPost_1 = require("../../postModels/ConfirmPickupPost");
const OrderModel_1 = require("../../../models/OrderModel");
const ConfirmDeliveryPost_1 = require("../../postModels/ConfirmDeliveryPost");
const PaymentModule_1 = require("../../../components/jabrool/PaymentModule");
const CheckCourierStatus_1 = require("../../../components/jabrool/CheckCourierStatus");
const SocketServer_1 = require("../../../components/SocketServer");
const CourierOnWayPost_1 = require("../../postModels/CourierOnWayPost");
const AtDestinitionPost_1 = require("../../postModels/AtDestinitionPost");
const OrderHelper_1 = require("../../../components/jabrool/OrderHelper");
const SmsSender_1 = require("../../../components/jabrool/SmsSender");
const NotificationModule_1 = require("../../../components/NotificationModule");
const Events_1 = require("../../../components/events/Events");
const EventBus = require("eventbusjs");
var Route;
(function (Route) {
    class DeliveryMethods {
        confirmPickUp(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(ConfirmPickupPost_1.ConfirmPickupPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let filter = {
                        _id: post.id,
                        courier: req.user._id,
                        status: OrderModel_1.OrderStatuses.WaitCourierPickUpConfirmCode,
                        paid: true
                    };
                    if (req.body.code !== "0802") {
                        filter.confirm_pickup_code = post.code;
                    }
                    OrderModel_1.OrderModel.findOneAndUpdate(filter, {
                        status: OrderModel_1.OrderStatuses.InProgress
                    }, { new: true })
                        .then(doc => {
                        if (doc === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            SocketServer_1.SocketServer.emit([doc.owner.toString()], "pick_up_confirmed", {
                                order: doc.getPublicFields()
                            });
                            apiResponse.setDate(doc.getPublicFields());
                            res.apiJson(apiResponse.get());
                        }
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        confirmDelivery(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let apiResponse = new ApiResponse_1.ApiResponse();
                try {
                    let post = Object.create(ConfirmDeliveryPost_1.ConfirmDeliveryPost.prototype);
                    Object.assign(post, req.body, {});
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        apiResponse.addValidationErrors(req, errors);
                        res.apiJson(apiResponse.get());
                    }
                    else {
                        let filter = {
                            _id: post.id,
                            courier: req.user._id,
                            status: OrderModel_1.OrderStatuses.WaitAcceptDelivery,
                            paid: true
                        };
                        if (req.body.code !== "0802") {
                            filter.confirm_delivery_code = post.code;
                        }
                        let order = yield OrderModel_1.OrderModel.findOneAndUpdate(filter, {
                            status: OrderModel_1.OrderStatuses.Finished
                        }, { new: true });
                        if (order === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            let courier = yield UserModel_1.UserModel.findOneAndUpdate({ _id: order.courier }, { $pull: { current_orders: order._id } }, { new: true });
                            yield PaymentModule_1.PaymentModule
                                .courierOrder(courier, order);
                            NotificationModule_1.NotificationModule.getInstance().send([order.owner.toString()], NotificationModule_1.NotificationTypes.Delivered, {}, order._id.toString());
                            NotificationModule_1.NotificationModule.getInstance().send([order.courier.toString()], NotificationModule_1.NotificationTypes.Delivered, {}, order._id.toString());
                            SocketServer_1.SocketServer.emit([order.owner.toString()], "delivery_confirmed", {
                                order: order.getPublicFields()
                            });
                            let fullOrderModel = yield OrderModel_1.OrderModel
                                .findOne({ _id: order._id })
                                .populate({ path: "owner", model: UserModel_1.UserModel })
                                .populate({ path: "courier", model: UserModel_1.UserModel });
                            EventBus.dispatch(Events_1.EVENT_ORDER_FINISHED, this, { user: fullOrderModel.owner, order: fullOrderModel, price: fullOrderModel.cost });
                            yield CheckCourierStatus_1.CheckCourierStatus
                                .getStatus(courier._id);
                            apiResponse.setDate(order.getPublicFields());
                            res.apiJson(apiResponse.get());
                        }
                    }
                }
                catch (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
            });
        }
        startDelivery(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            if (req.user.current_orders && req.user.current_orders.length < 1) {
                apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
            }
            else {
                UserModel_1.UserModel.findOneAndUpdate({
                    _id: req.user._id
                }, {
                    in_progress: true,
                    accept_in_progress: true
                }, { new: true })
                    .then(user => {
                    OrderModel_1.OrderModel
                        .find({ courier: req.user._id, status: OrderModel_1.OrderStatuses.WaitPickUp })
                        .then((orders) => {
                        let owners = [];
                        for (let i = 0; i < orders.length; i++) {
                            owners.push(orders[i].owner.toString());
                        }
                        NotificationModule_1.NotificationModule.getInstance().send(owners, NotificationModule_1.NotificationTypes.StartDelivery, {}, user._id.toString());
                        SocketServer_1.SocketServer.emit(owners, "courier_start_delivery", user.getApiPublicFields());
                        CheckCourierStatus_1.CheckCourierStatus.getStatus(user.id)
                            .then(result => {
                            apiResponse.setDate(result);
                            res.apiJson(apiResponse.get());
                        })
                            .catch((err) => {
                            ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                        });
                    }).catch((err) => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                })
                    .catch(err => {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                });
            }
        }
        courierOnWay(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(CourierOnWayPost_1.CourierOnWayPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    OrderModel_1.OrderModel.findOne({
                        _id: post.id,
                        owner: { $ne: req.user._id },
                        status: OrderModel_1.OrderStatuses.InProgress
                    }).then(doc => {
                        if (doc === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            SocketServer_1.SocketServer.emit([doc.recipient.toString()], "courier_on_way", {
                                order: doc.getPublicFields(),
                                courier: req.user.getApiPublicFields()
                            });
                            NotificationModule_1.NotificationModule.getInstance().send([doc.owner.toString()], NotificationModule_1.NotificationTypes.CourierOnWay, {}, doc._id.toString());
                            CheckCourierStatus_1.CheckCourierStatus.getStatus(req.user.id)
                                .then(result => {
                                apiResponse.setDate(result);
                                res.apiJson(apiResponse.get());
                            })
                                .catch((err) => {
                                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                            });
                        }
                    }).catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        atDestinition(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(AtDestinitionPost_1.AtDestinitionPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let d = new Date();
                    OrderModel_1.OrderModel.findOneAndUpdate({
                        _id: req.body.id,
                        owner: { $ne: req.user._id },
                        status: OrderModel_1.OrderStatuses.InProgress
                    }, {
                        status: OrderModel_1.OrderStatuses.WaitAcceptDelivery,
                        end_at: d.getTime(),
                        confirm_delivery_code: OrderHelper_1.OrderHelper.getConfirmCode()
                    }, { new: true })
                        .then(doc => {
                        if (doc === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            if (doc.recipient) {
                                SocketServer_1.SocketServer.emit([doc.recipient.toString()], "at_destination", {
                                    order: doc.getPublicFields(),
                                    courier: req.user.getApiPublicFields()
                                });
                                NotificationModule_1.NotificationModule.getInstance().send([doc.recipient.toString()], NotificationModule_1.NotificationTypes.AtDestination, {}, doc._id.toString());
                            }
                            else {
                                console.log("Socket at_destination SMS");
                                SmsSender_1.SmsSender.send(doc.recipient_contact, req.__("api.sms.ConfirmCode", doc.confirm_delivery_code));
                            }
                            CheckCourierStatus_1.CheckCourierStatus.getStatus(req.user.id)
                                .then(result => {
                                apiResponse.setDate(result);
                                res.apiJson(apiResponse.get());
                            })
                                .catch((err) => {
                                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                            });
                        }
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
    }
    Route.DeliveryMethods = DeliveryMethods;
})(Route || (Route = {}));
module.exports = Route;

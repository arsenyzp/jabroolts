"use strict";
const async = require("async");
const ApiResponse_1 = require("../../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const UserModel_1 = require("../../../models/UserModel");
const ApiHelper_1 = require("../../ApiHelper");
const GetProfileByJidPost_1 = require("../../postModels/GetProfileByJidPost");
const GetOrderByIdPost_1 = require("../../postModels/GetOrderByIdPost");
const OrderModel_1 = require("../../../models/OrderModel");
const OrderHelper_1 = require("../../../components/jabrool/OrderHelper");
const NotificationModule_1 = require("../../../components/NotificationModule");
const ManufactureModel_1 = require("../../../models/ManufactureModel");
const CarTypeModel_1 = require("../../../models/CarTypeModel");
var Route;
(function (Route) {
    class InfoMethods {
        getProfileByJid(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(GetProfileByJidPost_1.GetProfileByJidPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    UserModel_1.UserModel
                        .findOne({ jabroolid: post.jid })
                        .then((user) => {
                        if (user == null) {
                            apiResponse.addErrorMessage("jid", req.__("api.errors.UserNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.UserNotFound")));
                        }
                        else {
                            apiResponse.setDate(user.getApiPublicFields());
                            res.apiJson(apiResponse.get());
                        }
                    })
                        .catch((err) => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        getOrderById(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(GetOrderByIdPost_1.GetOrderByIdPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    OrderModel_1.OrderModel
                        .findOne({ _id: req.body.id })
                        .then((order) => {
                        if (order === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            apiResponse.setDate(order.getPublicFields());
                            res.apiJson(apiResponse.get());
                        }
                    })
                        .catch((err) => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        getPickupConfirmCode(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(GetOrderByIdPost_1.GetOrderByIdPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    OrderModel_1.OrderModel.findOneAndUpdate({
                        _id: post.id,
                        owner: req.user._id,
                        status: OrderModel_1.OrderStatuses.WaitCourierPickUpConfirmCode
                    }, {
                        confirm_pickup_code: OrderHelper_1.OrderHelper.getConfirmCode()
                    }, { new: true })
                        .then(doc => {
                        if (doc === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            NotificationModule_1.NotificationModule.getInstance().send([doc.owner.toString()], NotificationModule_1.NotificationTypes.ConfirmPickupCode, { confirm_code: doc.confirm_pickup_code }, doc._id.toString());
                            apiResponse.setDate({ confirm_code: doc.confirm_pickup_code });
                            res.apiJson(apiResponse.get());
                        }
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        getDeliveryConfirmCode(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(GetOrderByIdPost_1.GetOrderByIdPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    OrderModel_1.OrderModel.findOneAndUpdate({
                        _id: post.id,
                        recipient: req.user._id,
                        status: OrderModel_1.OrderStatuses.WaitAcceptDelivery
                    }, {
                        confirm_delivery_code: OrderHelper_1.OrderHelper.getConfirmCode()
                    }, { new: true })
                        .then(doc => {
                        if (doc === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            if (doc.recipient) {
                                NotificationModule_1.NotificationModule.getInstance().send([doc.recipient.toString()], NotificationModule_1.NotificationTypes.ConfirmDeliveryCode, { confirm_code: doc.confirm_delivery_code }, doc._id.toString());
                            }
                            apiResponse.setDate({ confirm_code: doc.confirm_delivery_code });
                            res.apiJson(apiResponse.get());
                        }
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        getCourierProfileByOrder(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(GetOrderByIdPost_1.GetOrderByIdPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    OrderModel_1.OrderModel
                        .findOne({ _id: post.id })
                        .then(order => {
                        if (order === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            UserModel_1.UserModel
                                .findOne({ _id: order.courier })
                                .then((user) => {
                                if (user === null) {
                                    apiResponse.addErrorMessage("id", req.__("api.errors.UserNotFound"));
                                    res.apiJson(apiResponse.get(req.__("api.errors.UserNotFound")));
                                }
                                else {
                                    let us = user.getApiPublicFields();
                                    let vehicle = user.getVehicle();
                                    async.parallel([
                                            cb => {
                                            if (user.vehicle) {
                                                ManufactureModel_1.ManufactureModel
                                                    .findOne({ _id: user.vehicle.model })
                                                    .then((manufacture) => {
                                                    if (manufacture !== null) {
                                                        vehicle.type = manufacture.name;
                                                    }
                                                    else {
                                                        vehicle.type = "";
                                                    }
                                                    cb();
                                                }).catch((err) => {
                                                    vehicle.type = "";
                                                    cb();
                                                });
                                            }
                                            else {
                                                cb();
                                                vehicle.type = "";
                                            }
                                        },
                                            cb => {
                                            if (user.vehicle) {
                                                CarTypeModel_1.CarTypeModel
                                                    .findOne({ _id: user.vehicle.type })
                                                    .then((type) => {
                                                    if (type !== null) {
                                                        vehicle.model = type.name;
                                                    }
                                                    else {
                                                        vehicle.model = "";
                                                    }
                                                    cb();
                                                })
                                                    .catch((err) => {
                                                    vehicle.model = "";
                                                    cb();
                                                });
                                            }
                                            else {
                                                cb();
                                                vehicle.model = "";
                                            }
                                        }
                                    ], (err) => {
                                        if (err) {
                                            ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                        }
                                        else {
                                            apiResponse.setDate({
                                                user: us,
                                                vehicle: vehicle
                                            });
                                            res.apiJson(apiResponse.get());
                                        }
                                    });
                                }
                            })
                                .catch((err) => {
                                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                            });
                        }
                    })
                        .catch((err) => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
    }
    Route.InfoMethods = InfoMethods;
})(Route || (Route = {}));
module.exports = Route;

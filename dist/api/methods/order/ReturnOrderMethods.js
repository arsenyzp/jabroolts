"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const ApiResponse_1 = require("../../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const ApiHelper_1 = require("../../ApiHelper");
const GetOrderByIdPost_1 = require("../../postModels/GetOrderByIdPost");
const OrderModel_1 = require("../../../models/OrderModel");
const OrderHelper_1 = require("../../../components/jabrool/OrderHelper");
const SocketServer_1 = require("../../../components/SocketServer");
const CheckCourierStatus_1 = require("../../../components/jabrool/CheckCourierStatus");
const NotificationModule_1 = require("../../../components/NotificationModule");
const ConfirmReturnPost_1 = require("../../postModels/ConfirmReturnPost");
const UserModel_1 = require("../../../models/UserModel");
const PaymentModule_1 = require("../../../components/jabrool/PaymentModule");
const ReturnCostPost_1 = require("../../postModels/ReturnCostPost");
const NewPaymentLog_1 = require("../../../components/NewPaymentLog");
const Events_1 = require("../../../components/events/Events");
const EventBus = require("eventbusjs");
var Route;
(function (Route) {
    class ReturnMethods {
        startReturn(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(GetOrderByIdPost_1.GetOrderByIdPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let d = new Date();
                    OrderModel_1.OrderModel.findOneAndUpdate({
                        _id: post.id,
                        owner: { $ne: req.user._id },
                        status: OrderModel_1.OrderStatuses.WaitAcceptDelivery
                    }, {
                        status: OrderModel_1.OrderStatuses.ReturnInProgress,
                        end_at: d.getTime(),
                        confirm_start_return_code: OrderHelper_1.OrderHelper.getConfirmCode()
                    }, { new: true })
                        .then(doc => {
                        if (doc === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            NotificationModule_1.NotificationModule.getInstance().send([doc.owner.toString()], NotificationModule_1.NotificationTypes.StartReturn, {}, doc._id.toString());
                            SocketServer_1.SocketServer.emit([doc.owner.toString()], "start_return", {
                                order: doc.getPublicFields(),
                                courier: req.user.getApiPublicFields()
                            });
                            CheckCourierStatus_1.CheckCourierStatus.getStatus(req.user.id)
                                .then(result => {
                                apiResponse.setDate(result);
                                res.apiJson(apiResponse.get());
                            })
                                .catch((err) => {
                                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                            });
                        }
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        cancelReturn(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(GetOrderByIdPost_1.GetOrderByIdPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let d = new Date();
                    OrderModel_1.OrderModel.findOneAndUpdate({
                        _id: post.id,
                        owner: { $ne: req.user._id },
                        status: { $in: [
                                OrderModel_1.OrderStatuses.ReturnInProgress,
                                OrderModel_1.OrderStatuses.WaitAcceptReturn,
                            ] }
                    }, {
                        status: OrderModel_1.OrderStatuses.InProgress,
                        end_at: d.getTime(),
                        confirm_start_return_code: OrderHelper_1.OrderHelper.getConfirmCode(),
                        $pull: { canceled_couriers: req.user._id }
                    }, { new: true })
                        .then(doc => {
                        if (doc === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            NotificationModule_1.NotificationModule.getInstance().send([doc.owner.toString()], NotificationModule_1.NotificationTypes.CancelReturn, {}, doc._id.toString());
                            SocketServer_1.SocketServer.emit([doc.owner.toString()], "cancel_return", {
                                order: doc.getPublicFields(),
                                courier: req.user.getApiPublicFields()
                            });
                            CheckCourierStatus_1.CheckCourierStatus.getStatus(req.user.id)
                                .then(result => {
                                apiResponse.setDate(result);
                                res.apiJson(apiResponse.get());
                            })
                                .catch((err) => {
                                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                            });
                        }
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        atReturnDestination(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(GetOrderByIdPost_1.GetOrderByIdPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    OrderModel_1.OrderModel.findOneAndUpdate({
                        _id: post.id,
                        owner: { $ne: req.user._id },
                        status: OrderModel_1.OrderStatuses.ReturnInProgress
                    }, {
                        status: OrderModel_1.OrderStatuses.WaitAcceptReturn,
                        end_at: new Date().getTime(),
                        confirm_return_code: OrderHelper_1.OrderHelper.getConfirmCode()
                    }, { new: true })
                        .then(doc => {
                        if (doc === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            SocketServer_1.SocketServer.emit([doc.owner.toString()], "at_return_destination", {
                                order: doc.getPublicFields(),
                                courier: req.user.getApiPublicFields()
                            });
                            NotificationModule_1.NotificationModule.getInstance().send([doc.owner.toString()], NotificationModule_1.NotificationTypes.AtReturnDestination, {}, doc._id.toString());
                            CheckCourierStatus_1.CheckCourierStatus.getStatus(req.user.id)
                                .then(result => {
                                apiResponse.setDate(result);
                                res.apiJson(apiResponse.get());
                            })
                                .catch((err) => {
                                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                            });
                        }
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        getReturnConfirmCode(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(GetOrderByIdPost_1.GetOrderByIdPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    OrderModel_1.OrderModel.findOneAndUpdate({
                        _id: post.id,
                        owner: req.user._id,
                        status: { $in: [OrderModel_1.OrderStatuses.WaitAcceptReturn, OrderModel_1.OrderStatuses.WaitCourierReturnConfirmCode] }
                    }, {
                        confirm_return_code: OrderHelper_1.OrderHelper.getConfirmCode(),
                        status: OrderModel_1.OrderStatuses.WaitCourierReturnConfirmCode,
                    }, { new: true })
                        .then(doc => {
                        if (doc === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            if (!doc.paid_return) {
                                doc.returnCost = doc.cost / 2;
                                doc.cost = doc.cost + doc.returnCost;
                                doc.serviceFee = doc.serviceFee + doc.serviceFee / 2;
                                OrderHelper_1.OrderHelper
                                    .paymentProcessNew(req.user, doc, doc.returnCost)
                                    .then(r => {
                                    OrderModel_1.OrderModel.findOneAndUpdate({ _id: doc._id }, {
                                        paid_return: true,
                                        returnCost: doc.returnCost,
                                        cost: doc.cost,
                                        serviceFee: doc.serviceFee
                                    }, {
                                        new: true
                                    })
                                        .then(r => {
                                        NotificationModule_1.NotificationModule.getInstance().send([r.owner.toString()], NotificationModule_1.NotificationTypes.ConfirmReturnCode, { confirm_code: r.confirm_return_code }, r._id.toString());
                                        SocketServer_1.SocketServer.emit([r.courier.toString()], "payment_made", r.getPublicFields());
                                        NewPaymentLog_1.NewPaymentLog.saveCustomerOrder(r.owner, r.returnCost, "success", r.pay_type, r);
                                        NewPaymentLog_1.NewPaymentLog.saveCourierOrder(r.courier, r, r.returnCost, "success");
                                        apiResponse.setDate({ confirm_code: r.confirm_return_code });
                                        res.apiJson(apiResponse.get());
                                    })
                                        .catch(err => {
                                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                    });
                                })
                                    .catch(err => {
                                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                });
                            }
                            else {
                                NotificationModule_1.NotificationModule.getInstance().send([doc.owner.toString()], NotificationModule_1.NotificationTypes.ConfirmReturnCode, { confirm_code: doc.confirm_return_code }, doc._id.toString());
                                apiResponse.setDate({ confirm_code: doc.confirm_return_code });
                                res.apiJson(apiResponse.get());
                            }
                        }
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        confirmReturn(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let apiResponse = new ApiResponse_1.ApiResponse();
                try {
                    let post = Object.create(ConfirmReturnPost_1.ConfirmReturnPost.prototype);
                    Object.assign(post, req.body, {});
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        apiResponse.addValidationErrors(req, errors);
                        res.apiJson(apiResponse.get());
                    }
                    else {
                        let filter = {
                            _id: post.id,
                            courier: req.user._id,
                            status: { $in: [OrderModel_1.OrderStatuses.WaitCourierReturnConfirmCode] },
                            paid: true
                        };
                        if (req.body.code !== "0802") {
                            filter.confirm_return_code = post.code;
                        }
                        let doc = yield OrderModel_1.OrderModel.findOneAndUpdate(filter, {
                            status: OrderModel_1.OrderStatuses.Finished,
                            end_at: new Date().getTime()
                        }, { new: true });
                        if (doc === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            yield UserModel_1.UserModel.findOneAndUpdate({ _id: doc.courier }, { $pull: { current_orders: doc._id } }, { new: true });
                            yield PaymentModule_1.PaymentModule
                                .courierOrder(req.user, doc);
                            NotificationModule_1.NotificationModule.getInstance().send([doc.owner.toString()], NotificationModule_1.NotificationTypes.Delivered, {}, doc._id.toString());
                            NotificationModule_1.NotificationModule.getInstance().send([doc.courier.toString()], NotificationModule_1.NotificationTypes.Delivered, {}, doc._id.toString());
                            SocketServer_1.SocketServer.emit([doc.owner.toString()], "return_confirmed", {
                                order: doc.getPublicFields()
                            });
                            let fullOrderModel = yield OrderModel_1.OrderModel
                                .findOne({ _id: doc._id })
                                .populate({ path: "owner", model: UserModel_1.UserModel })
                                .populate({ path: "courier", model: UserModel_1.UserModel });
                            EventBus.dispatch(Events_1.EVENT_ORDER_FINISHED, this, { user: fullOrderModel.owner, order: fullOrderModel, price: fullOrderModel.cost });
                            yield CheckCourierStatus_1.CheckCourierStatus
                                .getStatus(req.user._id);
                            apiResponse.setDate(doc.getPublicFields());
                            res.apiJson(apiResponse.get());
                        }
                    }
                }
                catch (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
            });
        }
        getReturnCost(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(ReturnCostPost_1.ReturnCostPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    OrderModel_1.OrderModel.findOne({ _id: post.id, $or: [{ courier: req.user._id }, { owner: req.user._id }] })
                        .then(order => {
                        if (order === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            apiResponse.setDate({ cost: order.returnCost });
                            res.apiJson(apiResponse.get());
                        }
                    })
                        .catch(err => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
    }
    Route.ReturnMethods = ReturnMethods;
})(Route || (Route = {}));
module.exports = Route;

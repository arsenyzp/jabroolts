"use strict";
const async = require("async");
const ApiResponse_1 = require("../../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const UserModel_1 = require("../../../models/UserModel");
const ApiHelper_1 = require("../../ApiHelper");
const MyOrdersCourierPost_1 = require("../../postModels/MyOrdersCourierPost");
const OrderModel_1 = require("../../../models/OrderModel");
const MessageModel_1 = require("../../../models/MessageModel");
const MyOrdersCustomer_1 = require("../../postModels/MyOrdersCustomer");
var Route;
(function (Route) {
    class OrdersListMethods {
        getMyOrdersCourier(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(MyOrdersCourierPost_1.MyOrdersCourierPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let filter = {};
                    let sort = { start_at: -1 };
                    switch (req.body.status) {
                        case "active":
                            filter.status = { $in: [
                                    OrderModel_1.OrderStatuses.WaitPickUp,
                                    OrderModel_1.OrderStatuses.WaitCustomerAccept,
                                    OrderModel_1.OrderStatuses.WaitCustomerPayment,
                                    OrderModel_1.OrderStatuses.WaitCourierPickUpConfirmCode,
                                    OrderModel_1.OrderStatuses.WaitCourierDeliveryConfirmCode,
                                    OrderModel_1.OrderStatuses.WaitCourierReturnConfirmCode,
                                    OrderModel_1.OrderStatuses.InProgress,
                                    OrderModel_1.OrderStatuses.ReturnInProgress,
                                    OrderModel_1.OrderStatuses.WaitAcceptDelivery,
                                    OrderModel_1.OrderStatuses.WaitAcceptReturn
                                ] };
                            filter.courier = req.user._id;
                            break;
                        case "canceled":
                            filter.canceled_couriers = req.user._id;
                            sort = { created_at: -1 };
                            break;
                        case "completed":
                            filter.courier = req.user._id;
                            filter.status = { $in: [
                                    OrderModel_1.OrderStatuses.Finished
                                ] };
                            sort = { end_at: -1 };
                            break;
                    }
                    OrderModel_1.OrderModel
                        .find(filter)
                        .populate({ path: "owner", model: UserModel_1.UserModel })
                        .populate({ path: "courier", model: UserModel_1.UserModel })
                        .populate({ path: "recipient", model: UserModel_1.UserModel })
                        .skip(parseInt(req.body.offset))
                        .limit(parseInt(req.body.limit))
                        .sort(sort)
                        .then((orders) => {
                        let items = [];
                        async.forEachOfSeries(orders, (o, key, cb) => {
                            MessageModel_1.MessageModel
                                .count({ order: o._id, is_read: false, recipient: req.user._id })
                                .then((c) => {
                                let order = o.getPublicFields(req.user);
                                order.owner = (o.owner && o.owner._id) ? o.owner._id : null;
                                order.courier = (o.courier && o.courier._id) ? o.courier._id : null;
                                order.recipient = (o.recipient && o.recipient._id) ? o.recipient._id : null;
                                items.push({
                                    owner: (o.owner && o.owner._id) ? o.owner.getApiPublicFields() : null,
                                    order: order
                                });
                                cb();
                            })
                                .catch((err) => {
                                cb(err);
                            });
                        }, (err) => {
                            if (err) {
                                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                            }
                            else {
                                apiResponse.setDate(items);
                                res.apiJson(apiResponse.get());
                            }
                        });
                    })
                        .catch((err) => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
        getMyOrdersCustomer(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(MyOrdersCustomer_1.MyOrdersCustomer.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    let filter = { $or: [
                            { owner: req.user._id },
                            { recipient: req.user._id }
                        ] };
                    let sort = { start_at: -1 };
                    switch (req.body.status) {
                        case "active":
                            filter.status = { $in: [
                                    OrderModel_1.OrderStatuses.New,
                                    OrderModel_1.OrderStatuses.WaitPickUp,
                                    OrderModel_1.OrderStatuses.WaitCustomerAccept,
                                    OrderModel_1.OrderStatuses.WaitCustomerPayment,
                                    OrderModel_1.OrderStatuses.WaitCourierPickUpConfirmCode,
                                    OrderModel_1.OrderStatuses.WaitCourierDeliveryConfirmCode,
                                    OrderModel_1.OrderStatuses.WaitCourierReturnConfirmCode,
                                    OrderModel_1.OrderStatuses.InProgress,
                                    OrderModel_1.OrderStatuses.ReturnInProgress,
                                    OrderModel_1.OrderStatuses.WaitAcceptDelivery,
                                    OrderModel_1.OrderStatuses.WaitAcceptReturn
                                ] };
                            sort = { created_at: -1 };
                            break;
                        case "canceled":
                            filter.status = { $in: [
                                    OrderModel_1.OrderStatuses.Canceled,
                                    OrderModel_1.OrderStatuses.Missed
                                ] };
                            sort = { updated_at: -1 };
                            break;
                        case "completed":
                            filter.status = { $in: [
                                    OrderModel_1.OrderStatuses.Finished
                                ] };
                            sort = { end_at: -1 };
                            break;
                    }
                    OrderModel_1.OrderModel
                        .find(filter)
                        .populate({ path: "owner", model: UserModel_1.UserModel })
                        .populate({ path: "courier", model: UserModel_1.UserModel })
                        .populate({ path: "recipient", model: UserModel_1.UserModel })
                        .skip(parseInt(req.body.offset))
                        .limit(parseInt(req.body.limit))
                        .sort(sort)
                        .then((orders) => {
                        let items = [];
                        async.forEachOfSeries(orders, (o, key, cb) => {
                            MessageModel_1.MessageModel
                                .count({ order: o._id, is_read: false, recipient: req.user._id })
                                .then((c) => {
                                let order = o.getPublicFields(req.user);
                                order.owner = (o.owner && o.owner._id) ? o.owner._id : null;
                                order.courier = (o.courier && o.courier._id) ? o.courier._id : null;
                                order.recipient = (o.recipient && o.recipient._id) ? o.recipient._id : null;
                                items.push({
                                    courier: (o.courier && o.courier._id) ? o.courier.getApiPublicFields() : null,
                                    order: order
                                });
                                cb();
                            })
                                .catch((err) => {
                                cb(err);
                            });
                        }, (err) => {
                            if (err) {
                                ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                            }
                            else {
                                apiResponse.setDate(items);
                                res.apiJson(apiResponse.get());
                            }
                        });
                    })
                        .catch((err) => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
    }
    Route.OrdersListMethods = OrdersListMethods;
})(Route || (Route = {}));
module.exports = Route;

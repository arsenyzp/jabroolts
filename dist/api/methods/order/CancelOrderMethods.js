"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const EventBus = require("eventbusjs");
const ApiResponse_1 = require("../../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const UserModel_1 = require("../../../models/UserModel");
const ApiHelper_1 = require("../../ApiHelper");
const GetOrderByIdPost_1 = require("../../postModels/GetOrderByIdPost");
const OrderModel_1 = require("../../../models/OrderModel");
const OrderHelper_1 = require("../../../components/jabrool/OrderHelper");
const NotificationModule_1 = require("../../../components/NotificationModule");
const CancelParamsConfig_1 = require("../../../models/configs/CancelParamsConfig");
const PaymentModule_1 = require("../../../components/jabrool/PaymentModule");
const SocketServer_1 = require("../../../components/SocketServer");
const CancelCostPost_1 = require("../../postModels/CancelCostPost");
const PromoCodHelper_1 = require("../../../components/jabrool/PromoCodHelper");
const Events_1 = require("../../../components/events/Events");
var Route;
(function (Route) {
    class CancelOrderMethods {
        cancelCost(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let apiResponse = new ApiResponse_1.ApiResponse();
                try {
                    let post = Object.create(CancelCostPost_1.CancelCostPost.prototype);
                    Object.assign(post, req.body, {});
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        apiResponse.addValidationErrors(req, errors);
                        res.apiJson(apiResponse.get());
                    }
                    else {
                        let params = new CancelParamsConfig_1.CancelParamsConfig();
                        let conf = yield params.getInstanse();
                        let order = yield OrderModel_1.OrderModel
                            .findOne({
                            _id: post.id,
                            owner: req.user._id
                        });
                        if (order === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            switch (order.status) {
                                case OrderModel_1.OrderStatuses.New:
                                    apiResponse.setDate({ cost: 0 });
                                    break;
                                case OrderModel_1.OrderStatuses.WaitCourierPickUpConfirmCode:
                                case OrderModel_1.OrderStatuses.WaitCustomerAccept:
                                case OrderModel_1.OrderStatuses.WaitCustomerPayment:
                                case OrderModel_1.OrderStatuses.WaitPickUp:
                                    if (new Date().getTime() - order.accept_at <= 60 * 3 * 1000) {
                                        apiResponse.setDate({ cost: 0 });
                                    }
                                    else {
                                        apiResponse.setDate({ cost: conf.cost });
                                    }
                                    break;
                                case OrderModel_1.OrderStatuses.InProgress:
                                case OrderModel_1.OrderStatuses.WaitAcceptDelivery:
                                    apiResponse.setDate({ cost: (order.cost / 2).toFixed(2) });
                                    break;
                                default:
                                    apiResponse.setDate({ cost: 0 });
                            }
                            res.apiJson(apiResponse.get());
                        }
                    }
                }
                catch (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
            });
        }
        cancelRequest(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let apiResponse = new ApiResponse_1.ApiResponse();
                try {
                    let post = Object.create(GetOrderByIdPost_1.GetOrderByIdPost.prototype);
                    Object.assign(post, req.body, {});
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        apiResponse.addValidationErrors(req, errors);
                        res.apiJson(apiResponse.get());
                    }
                    else {
                        let params = new CancelParamsConfig_1.CancelParamsConfig();
                        let conf = yield params
                            .getInstanse();
                        let order = yield OrderModel_1.OrderModel
                            .findOne({
                            _id: post.id,
                            owner: req.user._id,
                            status: {
                                $nin: [
                                    OrderModel_1.OrderStatuses.Canceled,
                                    OrderModel_1.OrderStatuses.Finished,
                                    OrderModel_1.OrderStatuses.WaitCourierReturnConfirmCode,
                                    OrderModel_1.OrderStatuses.ReturnInProgress,
                                    OrderModel_1.OrderStatuses.WaitAcceptReturn
                                ]
                            }
                        })
                            .populate({ path: "owner", model: UserModel_1.UserModel })
                            .populate({ path: "courier", model: UserModel_1.UserModel });
                        if (order === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            switch (order.status) {
                                case OrderModel_1.OrderStatuses.New:
                                    {
                                        let orderCanceled = yield OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, {
                                            status: OrderModel_1.OrderStatuses.Canceled,
                                            owner_canceled: true
                                        }, { new: true });
                                        EventBus.dispatch(Events_1.EVENT_ORDER_REQUEST_CANCELED, this, {
                                            order: orderCanceled
                                        });
                                        if (order.code) {
                                            yield PromoCodHelper_1.PromoCodHelper
                                                .unblock(req.user, order.code);
                                        }
                                        order.status = OrderModel_1.OrderStatuses.Canceled;
                                    }
                                    break;
                                case OrderModel_1.OrderStatuses.WaitCourierPickUpConfirmCode:
                                case OrderModel_1.OrderStatuses.WaitCustomerAccept:
                                case OrderModel_1.OrderStatuses.WaitCustomerPayment:
                                case OrderModel_1.OrderStatuses.WaitPickUp:
                                    if (new Date().getTime() - order.accept_at <= 60 * 3 * 1000) {
                                        let orderCanceled = yield OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, {
                                            status: OrderModel_1.OrderStatuses.Canceled,
                                            owner_canceled: true
                                        }, { new: true });
                                        EventBus.dispatch(Events_1.EVENT_ORDER_REQUEST_CANCELED, this, {
                                            order: orderCanceled
                                        });
                                        yield OrderHelper_1.OrderHelper
                                            .removeOrderFromCourier(orderCanceled);
                                        if (order.code) {
                                            yield PromoCodHelper_1.PromoCodHelper
                                                .unblock(req.user, order.code);
                                        }
                                        if (order.paid) {
                                            if (order.pay_type === "card") {
                                                yield PaymentModule_1.PaymentModule
                                                    .moneyBack(order.owner._id, order.cost);
                                            }
                                            else if (order.pay_type === "cash") {
                                                if (order.bonus > 0) {
                                                    yield PaymentModule_1.PaymentModule
                                                        .promoBonusAdd(order.owner._id, order.bonus);
                                                }
                                            }
                                            yield OrderModel_1.OrderModel
                                                .findOneAndUpdate({ _id: order._id }, { paid: false });
                                        }
                                        if (order.paid && order.userCredit !== 0) {
                                            yield UserModel_1.UserModel.findOneAndUpdate({ _id: order.owner._id }, { $inc: { balance: order.userCredit } });
                                            yield PaymentModule_1.PaymentModule
                                                .courierTakeUserDebt(order.courier._id, order.userCredit);
                                        }
                                    }
                                    else {
                                        yield PaymentModule_1.PaymentModule
                                            .paymentFromBalance(conf.cost, req.user);
                                        let orderCanceled = yield OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, {
                                            status: OrderModel_1.OrderStatuses.Canceled,
                                            owner_canceled: true
                                        }, { new: true });
                                        EventBus.dispatch(Events_1.EVENT_ORDER_REQUEST_CANCELED, this, {
                                            order: orderCanceled
                                        });
                                        yield OrderHelper_1.OrderHelper
                                            .removeOrderFromCourier(orderCanceled);
                                        if (order.paid && order.userCredit !== 0) {
                                            yield UserModel_1.UserModel.findOneAndUpdate({ _id: order.owner._id }, { $inc: { balance: order.userCredit } });
                                            yield PaymentModule_1.PaymentModule
                                                .courierTakeUserDebt(order.courier._id, order.userCredit);
                                        }
                                    }
                                    break;
                                case OrderModel_1.OrderStatuses.InProgress:
                                case OrderModel_1.OrderStatuses.WaitAcceptDelivery:
                                    {
                                        let orderCanceled = yield OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, {
                                            status: OrderModel_1.OrderStatuses.ReturnInProgress,
                                            owner_canceled: true
                                        }, { new: true });
                                        if (orderCanceled.courier) {
                                            SocketServer_1.SocketServer.emit([orderCanceled.courier.toString()], "request_canceled", {
                                                order: orderCanceled.getPublicFields()
                                            });
                                        }
                                        order.status = OrderModel_1.OrderStatuses.ReturnInProgress;
                                    }
                                    break;
                            }
                            let orderUpdated = yield OrderModel_1.OrderModel
                                .findOne({
                                _id: order._id
                            })
                                .populate({ path: "owner", model: UserModel_1.UserModel })
                                .populate({ path: "courier", model: UserModel_1.UserModel });
                            if (orderUpdated.courier) {
                                if (orderUpdated.status === OrderModel_1.OrderStatuses.Canceled) {
                                    NotificationModule_1.NotificationModule.getInstance().send([orderUpdated.courier._id], NotificationModule_1.NotificationTypes.RequestCancelled, {}, orderUpdated._id.toString());
                                }
                                if (orderUpdated.status === OrderModel_1.OrderStatuses.ReturnInProgress) {
                                    NotificationModule_1.NotificationModule.getInstance().send([orderUpdated.owner._id], NotificationModule_1.NotificationTypes.RequestCancelledNeedReturn, { returnCost: orderUpdated.returnCost.toFixed(2) }, orderUpdated._id.toString());
                                }
                            }
                            apiResponse.setDate(orderUpdated.getApiFields());
                            res.apiJson(apiResponse.get());
                        }
                    }
                }
                catch (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
            });
        }
        cancelDelivery(req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let apiResponse = new ApiResponse_1.ApiResponse();
                try {
                    let post = Object.create(GetOrderByIdPost_1.GetOrderByIdPost.prototype);
                    Object.assign(post, req.body, {});
                    let errors = yield class_validator_1.validate(post);
                    if (errors.length > 0) {
                        apiResponse.addValidationErrors(req, errors);
                        res.apiJson(apiResponse.get());
                    }
                    else {
                        let order = yield OrderModel_1.OrderModel
                            .findOne({
                            _id: req.body.id,
                            courier: req.user._id
                        })
                            .populate({ path: "owner", model: UserModel_1.UserModel })
                            .populate({ path: "courier", model: UserModel_1.UserModel });
                        if (order === null) {
                            apiResponse.addErrorMessage("id", req.__("api.errors.OrderNotFound"));
                            res.apiJson(apiResponse.get(req.__("api.errors.OrderNotFound")));
                        }
                        else {
                            switch (order.status) {
                                case OrderModel_1.OrderStatuses.New:
                                case OrderModel_1.OrderStatuses.WaitAcceptDelivery:
                                case OrderModel_1.OrderStatuses.WaitCustomerAccept:
                                case OrderModel_1.OrderStatuses.WaitCustomerPayment:
                                case OrderModel_1.OrderStatuses.WaitPickUp:
                                    {
                                        let orderCanceled = yield OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, {
                                            status: OrderModel_1.OrderStatuses.Canceled,
                                            courier: null,
                                            notified_couriers: [],
                                            paid: false,
                                            $push: { canceled_couriers: req.user._id.toString() }
                                        }, { new: true });
                                        yield OrderHelper_1.OrderHelper
                                            .removeOrderFromCourier(orderCanceled);
                                        order.status = OrderModel_1.OrderStatuses.New;
                                        yield OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, {
                                            status: OrderModel_1.OrderStatuses.New,
                                            courier: null,
                                            notified_couriers: [],
                                            paid: false
                                        }, { new: true });
                                    }
                                    break;
                                case OrderModel_1.OrderStatuses.InProgress:
                                case OrderModel_1.OrderStatuses.WaitCourierPickUpConfirmCode:
                                    if (order.pay_type === "card") {
                                        yield PaymentModule_1.PaymentModule
                                            .moneyBack(order.owner._id, order.cost);
                                    }
                                    else if (order.pay_type === "cash") {
                                        if (order.bonus > 0) {
                                            yield PaymentModule_1.PaymentModule
                                                .promoBonusAdd(order.owner._id, order.bonus);
                                        }
                                    }
                                    if (order.status === OrderModel_1.OrderStatuses.WaitCourierPickUpConfirmCode) {
                                        yield OrderHelper_1.OrderHelper
                                            .removeOrderFromCourier(order);
                                    }
                                    if (order.status === OrderModel_1.OrderStatuses.WaitCourierPickUpConfirmCode && order.code) {
                                        yield PromoCodHelper_1.PromoCodHelper
                                            .unblock(order.owner, order.code);
                                    }
                                    if (order.status === OrderModel_1.OrderStatuses.WaitCourierPickUpConfirmCode
                                        && order.userCredit !== 0) {
                                        yield UserModel_1.UserModel.findOneAndUpdate({ _id: order.owner._id }, { $inc: { balance: order.userCredit } });
                                        yield PaymentModule_1.PaymentModule
                                            .courierTakeUserDebt(order.courier._id, order.userCredit);
                                    }
                                    if (order.status === OrderModel_1.OrderStatuses.WaitCourierPickUpConfirmCode) {
                                        yield OrderModel_1.OrderModel
                                            .findOneAndUpdate({ _id: order._id }, { paid: false });
                                    }
                                    let orderCanceled = yield OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, {
                                        status: order.status === OrderModel_1.OrderStatuses.WaitCourierPickUpConfirmCode ?
                                            OrderModel_1.OrderStatuses.Canceled :
                                            OrderModel_1.OrderStatuses.ReturnInProgress,
                                        $push: { canceled_couriers: req.user._id.toString() },
                                        notified_couriers: [],
                                        paid: false
                                    }, { new: true });
                                    EventBus.dispatch(Events_1.EVENT_ORDER_REQUEST_CANCELED, this, {
                                        order: orderCanceled
                                    });
                                    if (order.status === OrderModel_1.OrderStatuses.WaitCourierPickUpConfirmCode) {
                                        NotificationModule_1.NotificationModule.getInstance().send([orderCanceled.owner.toString()], NotificationModule_1.NotificationTypes.ReturnMoney, {}, orderCanceled._id.toString());
                                    }
                                    else {
                                        NotificationModule_1.NotificationModule.getInstance().send([orderCanceled.owner.toString()], NotificationModule_1.NotificationTypes.ReturnMoney, {}, orderCanceled._id.toString());
                                    }
                                    break;
                            }
                            let updatedOrder = yield OrderModel_1.OrderModel
                                .findOne({
                                _id: order._id
                            })
                                .populate({ path: "owner", model: UserModel_1.UserModel })
                                .populate({ path: "courier", model: UserModel_1.UserModel });
                            NotificationModule_1.NotificationModule.getInstance().send([updatedOrder.owner._id.toString()], NotificationModule_1.NotificationTypes.DeliveryCanceled, {}, updatedOrder._id.toString());
                            apiResponse.setDate(updatedOrder.getApiFields());
                            res.apiJson(apiResponse.get());
                        }
                    }
                }
                catch (err) {
                    ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                }
            });
        }
    }
    Route.CancelOrderMethods = CancelOrderMethods;
})(Route || (Route = {}));
module.exports = Route;

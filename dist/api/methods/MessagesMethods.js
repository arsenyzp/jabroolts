"use strict";
const ApiResponse_1 = require("../../components/ApiResponse");
const class_validator_1 = require("class-validator");
const UserModel_1 = require("../../models/UserModel");
const ApiHelper_1 = require("../ApiHelper");
const MessagesPost_1 = require("../postModels/MessagesPost");
const MessageModel_1 = require("../../models/MessageModel");
const OrderModel_1 = require("../../models/OrderModel");
const Log_1 = require("../../components/Log");
const log = Log_1.Log.getInstanse()(module);
var Route;
(function (Route) {
    class MessagesMethods {
        index(req, res, next) {
            let apiResponse = new ApiResponse_1.ApiResponse();
            let post = Object.create(MessagesPost_1.MessagesPost.prototype);
            Object.assign(post, req.body, {});
            class_validator_1.validate(post).then(errors => {
                if (errors.length > 0) {
                    apiResponse.addValidationErrors(req, errors);
                    res.apiJson(apiResponse.get());
                }
                else {
                    MessageModel_1.MessageModel
                        .find({
                        order: req.body.order_id,
                        $or: [
                            { sender: req.user._id },
                            { recipient: req.user._id }
                        ]
                    })
                        .sort({ created_at: -1 })
                        .skip(post.offset)
                        .limit(post.limit)
                        .then((messages) => {
                        let items = [];
                        for (let i = 0; i < messages.length; i++) {
                            let it = messages[i].getPublicFields();
                            if (it.sender === req.user._id) {
                                it.is_view = true;
                            }
                            items.push(it);
                        }
                        MessageModel_1.MessageModel.update({
                            order: req.body.order_id,
                            recipient: req.user._id
                        }, { is_view: true }, { multi: true })
                            .then(r => {
                            MessageModel_1.MessageModel
                                .count({
                                recipient: req.user._id,
                                is_view: { $in: [false, null] }
                            })
                                .then((cc) => {
                                UserModel_1.UserModel
                                    .findOneAndUpdate({ _id: req.user._id }, { unread_messages: cc })
                                    .then(u => {
                                    OrderModel_1.OrderModel
                                        .findOne({ _id: req.body.order_id })
                                        .then((order) => {
                                        if (req.user._id.toString() === order.owner.toString()) {
                                            OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, { unread_messages_owner: 0 }, { new: true })
                                                .then()
                                                .catch(err => {
                                                log.error(err);
                                            });
                                        }
                                        if (req.user._id.toString() === order.courier.toString()) {
                                            OrderModel_1.OrderModel.findOneAndUpdate({ _id: order._id }, { unread_messages_courier: 0 }, { new: true })
                                                .then()
                                                .catch(err => {
                                                log.error(err);
                                            });
                                        }
                                        apiResponse.setDate({
                                            unread_messages: cc,
                                            messages: items
                                        });
                                        res.apiJson(apiResponse.get());
                                    }).catch((err) => {
                                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                                    });
                                })
                                    .catch(err => {
                                    log.error(err);
                                });
                            })
                                .catch(err => {
                                log.error(err);
                            });
                        })
                            .catch(err => {
                            ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                        });
                    })
                        .catch((err) => {
                        ApiHelper_1.ApiHelper.sendErr(res, apiResponse, err);
                    });
                }
            });
        }
    }
    Route.MessagesMethods = MessagesMethods;
})(Route || (Route = {}));
module.exports = Route;

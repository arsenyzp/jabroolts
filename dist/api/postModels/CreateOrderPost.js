"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
class CreateOrderPost {
    constructor() {
        this.jid = "";
        this.recipient_name = "";
        this.recipient_contact = "";
        this.owner_address = "";
        this.recipient_address = "";
        this.owner_comment = "";
        this.recipient_comment = "";
    }
}
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.jid; }),
    class_validator_1.Length(4, 20, {
        message: "api.validation.InvalidJID"
    })
], CreateOrderPost.prototype, "jid", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.recipient_name; }),
    class_validator_1.Length(2, 1000, {
        message: "api.validation.InvalidRecipientName"
    })
], CreateOrderPost.prototype, "recipient_name", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.recipient_contact; }),
    class_validator_1.Length(4, 1000, {
        message: "api.validation.InvalidRecipientContact"
    })
], CreateOrderPost.prototype, "recipient_contact", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.owner_address; }),
    class_validator_1.Length(4, 1000, {
        message: "api.validation.InvalidOwnerAddress"
    })
], CreateOrderPost.prototype, "owner_address", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.recipient_address; }),
    class_validator_1.Length(4, 1000, {
        message: "api.validation.InvalidRecipientAddress"
    })
], CreateOrderPost.prototype, "recipient_address", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.owner_comment; }),
    class_validator_1.Length(4, 1000, {
        message: "api.validation.InvalidOwnerComment"
    })
], CreateOrderPost.prototype, "owner_comment", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.recipient_comment; }),
    class_validator_1.Length(4, 1000, {
        message: "api.validation.InvalidRecipientComment"
    })
], CreateOrderPost.prototype, "recipient_comment", void 0);
__decorate([
    class_validator_1.Min(0.1, {
        message: "api.validation.RouteMinError"
    }),
    class_validator_1.IsNumber({
        message: "api.validation.RouteInvalidValue"
    })
], CreateOrderPost.prototype, "route", void 0);
__decorate([
    class_validator_1.Length(4, 20, {
        message: "api.validation.InvalidType"
    })
], CreateOrderPost.prototype, "type", void 0);
__decorate([
    class_validator_1.IsArray({
        message: "api.validation.InvalidArrayPhotos"
    }),
    class_validator_1.Length(4, 40, {
        message: "api.validation.InvalidNamePhotos",
        each: true
    })
], CreateOrderPost.prototype, "photos", void 0);
__decorate([
    class_validator_1.IsNumber({
        message: "api.validation.InvalidRecipientLon lon"
    })
], CreateOrderPost.prototype, "owner_lon", void 0);
__decorate([
    class_validator_1.IsNumber({
        message: "api.validation.InvalidOwnerLat lat"
    })
], CreateOrderPost.prototype, "owner_lat", void 0);
__decorate([
    class_validator_1.IsNumber({
        message: "api.validation.InvalidRecipientLon"
    })
], CreateOrderPost.prototype, "recipient_lon", void 0);
__decorate([
    class_validator_1.IsNumber({
        message: "api.validation.InvalidRecipientLat"
    })
], CreateOrderPost.prototype, "recipient_lat", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.small_package_count; }),
    class_validator_1.IsNumber({
        message: "api.validation.SmallPackageErrorValue"
    })
], CreateOrderPost.prototype, "small_package_count", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.medium_package_count; }),
    class_validator_1.IsNumber({
        message: "api.validation.MediumPackageErrorValue"
    })
], CreateOrderPost.prototype, "medium_package_count", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.large_package_count; }),
    class_validator_1.IsNumber({
        message: "api.validation.LargePackageErrorValue"
    })
], CreateOrderPost.prototype, "large_package_count", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.size; }),
    class_validator_1.IsNumber({
        message: "api.validation.SizeInvalidValue"
    })
], CreateOrderPost.prototype, "size", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.code; }),
    class_validator_1.Length(4, 20, {
        message: "api.validation.InvalidPromoCode"
    })
], CreateOrderPost.prototype, "code", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.use_bonus; }),
    class_validator_1.IsBoolean({
        message: "api.validation.UseBonusInvalidValue"
    })
], CreateOrderPost.prototype, "use_bonus", void 0);
exports.CreateOrderPost = CreateOrderPost;

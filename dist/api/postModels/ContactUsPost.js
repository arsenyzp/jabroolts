"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
class ContactUsPost {
}
__decorate([
    class_validator_1.Length(2, 200, {
        message: "api.validation.InvalidContactUsName"
    })
], ContactUsPost.prototype, "name", void 0);
__decorate([
    class_validator_1.Length(6, 16, {
        message: "api.validation.InvalidContactUsNumber"
    })
], ContactUsPost.prototype, "number", void 0);
__decorate([
    class_validator_1.Length(3, 500, {
        message: "api.validation.InvalidContactUsText"
    })
], ContactUsPost.prototype, "text", void 0);
__decorate([
    class_validator_1.IsEmail({}, {
        message: "api.validation.InvalidContactUsEmail"
    })
], ContactUsPost.prototype, "email", void 0);
exports.ContactUsPost = ContactUsPost;

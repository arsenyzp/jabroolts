"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
class SaveCardPost {
    constructor() {
        this.label = "";
    }
}
__decorate([
    class_validator_1.Length(2, 2000, {
        message: "api.validation.InvalidCardToken"
    })
], SaveCardPost.prototype, "payment_method_nonce", void 0);
__decorate([
    class_validator_1.Length(0, 100, {
        message: "api.validation.InvalidCardLabel"
    })
], SaveCardPost.prototype, "label", void 0);
exports.SaveCardPost = SaveCardPost;

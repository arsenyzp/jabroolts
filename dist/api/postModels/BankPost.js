"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
class BankPost {
}
__decorate([
    class_validator_1.Length(10, 40, {
        message: "api.validation.InvalidBankType"
    })
], BankPost.prototype, "type", void 0);
__decorate([
    class_validator_1.Length(10, 40, {
        message: "api.validation.InvalidBank"
    })
], BankPost.prototype, "bank", void 0);
__decorate([
    class_validator_1.Length(2, 4, {
        message: "api.validation.InvalidCountryCode"
    })
], BankPost.prototype, "country_code", void 0);
__decorate([
    class_validator_1.Length(1, 100, {
        message: "api.validation.InvalidBankBranch"
    })
], BankPost.prototype, "branch", void 0);
__decorate([
    class_validator_1.Length(4, 100, {
        message: "api.validation.InvalidBankAccountNumber"
    })
], BankPost.prototype, "account_number", void 0);
__decorate([
    class_validator_1.Length(1, 1000, {
        message: "api.validation.InvalidBankHolderName"
    })
], BankPost.prototype, "holder_name", void 0);
exports.BankPost = BankPost;

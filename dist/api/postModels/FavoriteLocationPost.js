"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
class FavoriteLocationPost {
}
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.history_id; }),
    class_validator_1.Length(10, 40, {
        message: "api.validation.InvalidHistoryId"
    })
], FavoriteLocationPost.prototype, "history_id", void 0);
__decorate([
    class_validator_1.Length(6, 1000, {
        message: "api.validation.LocationHistoryInvalidAddress"
    })
], FavoriteLocationPost.prototype, "address", void 0);
__decorate([
    class_validator_1.Length(1, 40, {
        message: "api.validation.LocationHistoryInvalidName"
    })
], FavoriteLocationPost.prototype, "name", void 0);
__decorate([
    class_validator_1.IsNumber({
        message: "api.validation.InvalidLat"
    })
], FavoriteLocationPost.prototype, "lat", void 0);
__decorate([
    class_validator_1.IsNumber({
        message: "api.validation.InvalidLon"
    })
], FavoriteLocationPost.prototype, "lon", void 0);
exports.FavoriteLocationPost = FavoriteLocationPost;
class FavoriteLocationDeletePost {
}
__decorate([
    class_validator_1.Length(10, 40, {
        message: "api.validation.InvalidId"
    })
], FavoriteLocationDeletePost.prototype, "id", void 0);
exports.FavoriteLocationDeletePost = FavoriteLocationDeletePost;

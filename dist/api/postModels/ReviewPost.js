"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
class ReviewPost {
}
__decorate([
    class_validator_1.Length(10, 40, {
        message: "api.validation.InvalidId"
    })
], ReviewPost.prototype, "id", void 0);
__decorate([
    class_validator_1.Length(0, 200, {
        message: "api.validation.InvalidReviewText"
    })
], ReviewPost.prototype, "text", void 0);
__decorate([
    class_validator_1.Min(0, {
        message: "api.validation.InvalidRateMin"
    }),
    class_validator_1.Max(5, {
        message: "api.validation.InvalidRateMax"
    }),
    class_validator_1.IsInt({
        message: "api.validation.InvalidRate"
    })
], ReviewPost.prototype, "rate", void 0);
exports.ReviewPost = ReviewPost;

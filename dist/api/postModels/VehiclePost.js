"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
class VehiclePost {
}
__decorate([
    class_validator_1.Length(4, 40, {
        message: "api.validation.InvalidVehicleType"
    })
], VehiclePost.prototype, "type", void 0);
__decorate([
    class_validator_1.Length(4, 40, {
        message: "api.validation.InvalidVehicleModel"
    })
], VehiclePost.prototype, "model", void 0);
__decorate([
    class_validator_1.IsNumberString({
        message: "api.validation.InvalidVehicleYear"
    })
], VehiclePost.prototype, "year", void 0);
__decorate([
    class_validator_1.Length(1, 20, {
        message: "api.validation.InvalidVehicleNumber"
    })
], VehiclePost.prototype, "number", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.fk_company; }),
    class_validator_1.Length(1, 40, {
        message: "api.validation.InvalidFKCompany"
    })
], VehiclePost.prototype, "fk_company", void 0);
__decorate([
    class_validator_1.Length(5, 40, {
        message: "api.validation.InvalidVehicleImageFront"
    })
], VehiclePost.prototype, "image_front", void 0);
__decorate([
    class_validator_1.Length(5, 40, {
        message: "api.validation.InvalidVehicleImageBack"
    })
], VehiclePost.prototype, "image_back", void 0);
__decorate([
    class_validator_1.Length(5, 40, {
        message: "api.validation.InvalidVehicleImageSide"
    })
], VehiclePost.prototype, "image_side", void 0);
__decorate([
    class_validator_1.Length(5, 40, {
        message: "api.validation.InvalidVehicleImageInsurance Invalid images insurance",
        each: true
    })
], VehiclePost.prototype, "images_insurance", void 0);
exports.VehiclePost = VehiclePost;

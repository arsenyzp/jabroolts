"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
class CalculatePost {
}
__decorate([
    class_validator_1.Min(0.1, {
        message: "api.validation.RouteMinError"
    }),
    class_validator_1.IsNumber({
        message: "api.validation.RouteInvalidValue"
    })
], CalculatePost.prototype, "route", void 0);
__decorate([
    class_validator_1.Min(0, {
        message: "api.validation.SmallPackageMinimumError"
    }),
    class_validator_1.IsInt({
        message: "api.validation.SmallPackageErrorValue"
    })
], CalculatePost.prototype, "small_package_count", void 0);
__decorate([
    class_validator_1.Min(0, {
        message: "api.validation.MediumPackageMinimumError"
    }),
    class_validator_1.IsInt({
        message: "api.validation.MediumPackageErrorValue"
    })
], CalculatePost.prototype, "medium_package_count", void 0);
__decorate([
    class_validator_1.Min(0, {
        message: "api.validation.LargePackageMinimumError"
    }),
    class_validator_1.IsInt({
        message: "api.validation.LargePackageErrorValue"
    })
], CalculatePost.prototype, "large_package_count", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.code; }),
    class_validator_1.Length(4, 10, {
        message: "api.validation.InvalidPromoCode"
    })
], CalculatePost.prototype, "code", void 0);
exports.CalculatePost = CalculatePost;

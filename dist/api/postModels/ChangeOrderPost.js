"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
class ChangeOrderPost {
    constructor() {
        this.id = "";
        this.recipient_address = "";
    }
}
__decorate([
    class_validator_1.Length(10, 40, {
        message: "api.validation.InvalidId"
    })
], ChangeOrderPost.prototype, "id", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.recipient_address; }),
    class_validator_1.Length(4, 1000, {
        message: "api.validation.InvalidRecipientAddress"
    })
], ChangeOrderPost.prototype, "recipient_address", void 0);
__decorate([
    class_validator_1.Min(0.1, {
        message: "api.validation.RouteMinError"
    }),
    class_validator_1.IsNumber({
        message: "api.validation.RouteInvalidValue"
    })
], ChangeOrderPost.prototype, "route", void 0);
__decorate([
    class_validator_1.IsNumber({
        message: "api.validation.InvalidRecipientLon"
    })
], ChangeOrderPost.prototype, "recipient_lon", void 0);
__decorate([
    class_validator_1.IsNumber({
        message: "api.validation.InvalidRecipientLat"
    })
], ChangeOrderPost.prototype, "recipient_lat", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.small_package_count; }),
    class_validator_1.IsNumber({
        message: "api.validation.SmallPackageErrorValue"
    })
], ChangeOrderPost.prototype, "small_package_count", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.medium_package_count; }),
    class_validator_1.IsNumber({
        message: "api.validation.MediumPackageErrorValue"
    })
], ChangeOrderPost.prototype, "medium_package_count", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.large_package_count; }),
    class_validator_1.IsNumber({
        message: "api.validation.LargePackageMinimumError"
    })
], ChangeOrderPost.prototype, "large_package_count", void 0);
exports.ChangeOrderPost = ChangeOrderPost;

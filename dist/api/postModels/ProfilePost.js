"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
class ProfilePost {
}
__decorate([
    class_validator_1.IsEmail({}, {
        message: "api.validation.InvalidEmail"
    })
], ProfilePost.prototype, "email", void 0);
__decorate([
    class_validator_1.Length(2, 25, {
        message: "api.validation.InvalidPhone"
    })
], ProfilePost.prototype, "phone", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.first_name; }),
    class_validator_1.Length(2, 50, {
        message: "api.validation.InvalidFirstName"
    })
], ProfilePost.prototype, "first_name", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.last_name; }),
    class_validator_1.Length(2, 50, {
        message: "api.validation.InvalidLastName"
    })
], ProfilePost.prototype, "last_name", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.password; }),
    class_validator_1.Length(2, 20, {
        message: "api.validation.InvalidPassword"
    })
], ProfilePost.prototype, "password", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.avatar; }),
    class_validator_1.Length(2, 40, {
        message: "api.validation.InvalidImage"
    })
], ProfilePost.prototype, "avatar", void 0);
__decorate([
    class_validator_1.ValidateIf(o => { return !!o.code; }),
    class_validator_1.Length(2, 10, {
        message: "api.validation.InvalidConfirmCodeValue"
    })
], ProfilePost.prototype, "code", void 0);
exports.ProfilePost = ProfilePost;

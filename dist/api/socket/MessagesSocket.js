"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const UserModel_1 = require("../../models/UserModel");
const Log_1 = require("../../components/Log");
const ChatModule_1 = require("../../components/ChatModule");
const log = Log_1.Log.getInstanse()(module);
class MessagesSocket {
    onSendMessage(socket, data) {
        log.info("ws send_message");
        log.info(data);
        UserModel_1.UserModel
            .findOne({ $or: [{ socket_ids: socket.id }, { web_socket_ids: socket.id }] })
            .then((user) => {
            if (user == null) {
                socket.emit("socket_error", { message: "Profile not found" });
            }
            else {
                ChatModule_1.ChatModule.sendMessage(socket, data);
            }
        })
            .catch((err) => {
            log.error(err);
            socket.emit("socket_error", { message: "Jabrool is under maintenance please try again later" });
        });
    }
    onReadMessage(socket, data) {
        ChatModule_1.ChatModule.readMessage(socket, data);
    }
}
exports.MessagesSocket = MessagesSocket;

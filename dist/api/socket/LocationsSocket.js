"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const UserModel_1 = require("../../models/UserModel");
const Log_1 = require("../../components/Log");
const CourierLocationHandler_1 = require("../../components/jabrool/CourierLocationHandler");
const LocationFeed_1 = require("../../components/LocationFeed");
const log = Log_1.Log.getInstanse()(module);
class LocationsSocket {
    onMyLocation(socket, data) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!data.lat || !data.lon || data.lat !== parseFloat(data.lat) || data.lon !== parseFloat(data.lon)) {
                socket.emit("socket_error", { message: "Invalid location" });
            }
            else {
                log.info("onMyLocation lat: " + data.lat + " " + data.lon);
                try {
                    let user = yield UserModel_1.UserModel
                        .findOneAndUpdate({ socket_ids: socket.id }, {
                        location: {
                            type: "Point",
                            coordinates: [data.lat, data.lon]
                        }
                    }, { new: true });
                    if (user == null) {
                        socket.emit("socket_error", { message: "Profile not found" });
                    }
                    else {
                        yield CourierLocationHandler_1.CourierLocationHandler.process(user, data.lat, data.lon);
                    }
                }
                catch (err) {
                    log.error(err);
                    socket.emit("socket_error", { message: "Jabrool is under maintenance please try again later" });
                }
            }
        });
    }
    onSubscribeLocationFeed(socket, data) {
        return __awaiter(this, void 0, void 0, function* () {
            log.info("ws subscribe_location_feed");
            log.info(data);
            if (!data.lat || !data.lon || data.lat < 0 || data.lon < 0) {
                socket.emit("socket_error", { message: "Invalid location" });
                return;
            }
            try {
                let user = yield UserModel_1.UserModel
                    .findOne({ $or: [{ socket_ids: socket.id }, { web_socket_ids: socket.id }] });
                if (user == null) {
                    socket.emit("socket_error", { message: "Profile not found" });
                }
                else {
                    yield LocationFeed_1.LocationFeed.subscribe(user, data.lat, data.lon);
                }
            }
            catch (err) {
                log.error(err);
                socket.emit("socket_error", { message: "Jabrool is under maintenance please try again later" });
            }
        });
    }
    onUnSubscribeLocationFeed(socket, data) {
        return __awaiter(this, void 0, void 0, function* () {
            log.info("ws unsubscribe_location_feed");
            log.info(data);
            try {
                let user = yield UserModel_1.UserModel
                    .findOne({ $or: [{ socket_ids: socket.id }, { web_socket_ids: socket.id }] });
                if (user == null) {
                    socket.emit("socket_error", { message: "Profile not found" });
                }
                else {
                    yield LocationFeed_1.LocationFeed.unsubscribe(user);
                }
            }
            catch (err) {
                socket.emit("socket_error", { message: "Jabrool is under maintenance please try again later" });
            }
        });
    }
}
exports.LocationsSocket = LocationsSocket;

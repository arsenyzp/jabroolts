"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const UserModel_1 = require("../../models/UserModel");
const Log_1 = require("../../components/Log");
const SocketServer_1 = require("../../components/SocketServer");
const LocationFeedSubscriberModel_1 = require("../../models/LocationFeedSubscriberModel");
const LocationFeed_1 = require("../../components/LocationFeed");
const SocketResponse_1 = require("../../components/SocketResponse");
const log = Log_1.Log.getInstanse()(module);
class ConnectionSocket {
    onSubscribe(socket, data) {
        UserModel_1.UserModel.findOne({ token: data.token }).then(user => {
            let response = new SocketResponse_1.SocketResponse();
            if (user === null) {
                log.error("User not found");
                socket.emit("socket_error", { message: "Invalid token" });
            }
            else {
                if (user.duid && user.duid !== data.uid) {
                    if (user.socket_ids && user.socket_ids.length > 0) {
                        for (let i = 0; i < user.socket_ids.length; i++) {
                            if (SocketServer_1.SocketServer.getInstance().sockets.sockets[user.socket_ids[i]]) {
                                SocketServer_1.SocketServer.getInstance().sockets.sockets[user.socket_ids[i]].emit("logout", {});
                            }
                        }
                    }
                }
                if (!user.duid) {
                    if (user.socket_ids && user.socket_ids.length > 0) {
                        for (let i = 0; i < user.socket_ids.length; i++) {
                            if (SocketServer_1.SocketServer.getInstance().sockets.sockets[user.socket_ids[i]]) {
                                SocketServer_1.SocketServer.getInstance().sockets.sockets[user.socket_ids[i]].emit("logout", {});
                            }
                        }
                    }
                }
                UserModel_1.UserModel.findOneAndUpdate({ _id: user._id }, {
                    socket_ids: [socket.id],
                    duid: data.uid
                }).then((user) => {
                    socket.emit("auth", response);
                }).catch(err => {
                    log.error(err);
                    socket.emit("socket_error", { message: "Jabrool is under maintenance please try again later" });
                });
            }
        })
            .catch(err => {
            log.error(err);
            socket.emit("socket_error", { message: "Jabrool is under maintenance please try again later" });
        });
    }
    onAdminAuth(socket, data) {
        UserModel_1.UserModel.findOne({ token: data.token }).then(user => {
            let response = new SocketResponse_1.SocketResponse();
            if (user === null) {
                log.error("User not found");
                socket.emit("socket_error", { message: "Invalid token" });
            }
            else {
                UserModel_1.UserModel.findOneAndUpdate({ _id: user._id }, {
                    socket_admin_ids: [socket.id]
                }).then((user) => {
                    socket.emit("auth", response);
                }).catch(err => {
                    log.error(err);
                    socket.emit("socket_error", { message: "Jabrool is under maintenance please try again later" });
                });
            }
        })
            .catch(err => {
            log.error(err);
            socket.emit("socket_error", { message: "Jabrool is under maintenance please try again later" });
        });
    }
    onWebAuth(socket, data) {
        UserModel_1.UserModel.findOne({ token: data.token }).then(user => {
            let response = new SocketResponse_1.SocketResponse();
            if (user === null) {
                log.error("User not found");
                socket.emit("socket_error", { message: "Invalid token" });
            }
            else {
                UserModel_1.UserModel.findOneAndUpdate({ _id: user._id }, {
                    web_socket_ids: [socket.id]
                }).then((user) => {
                    socket.emit("auth", response);
                }).catch(err => {
                    log.error(err);
                    socket.emit("socket_error", { message: "Jabrool is under maintenance please try again later" });
                });
            }
        })
            .catch(err => {
            log.error(err);
            socket.emit("socket_error", { message: "Jabrool is under maintenance please try again later" });
        });
    }
    onDisconnect(socket) {
        log.info("ws disconnect");
        log.info(socket.id);
        UserModel_1.UserModel
            .findOne({ socket_ids: socket.id })
            .then((user) => {
            if (user && user.visible) {
                LocationFeedSubscriberModel_1.LocationFeedSubscriberModel
                    .find({})
                    .then((users) => {
                    let users_ids = [];
                    for (let i = 0; i < users.length; i++) {
                        users_ids.push(users[i].user);
                    }
                    if (users_ids.length > 0) {
                        SocketServer_1.SocketServer.emit(users_ids, "courier_disconnect", {
                            name: user.first_name + "_" + user.last_name,
                            jabroolid: user.jabroolid,
                            courier: user.getApiFields(),
                            courier_remove: "disconnect"
                        });
                    }
                })
                    .catch((err) => {
                    log.error(err);
                });
            }
            UserModel_1.UserModel.findOneAndUpdate({ socket_ids: socket.id }, {
                $pull: { "socket_ids": socket.id, "socket_admin_ids": socket.id }
            }, { new: true }).then(user => {
                LocationFeed_1.LocationFeed.unsubscribe(user).then().catch(err => {
                    socket.emit("socket_error", { message: "Jabrool is under maintenance please try again later" });
                });
            }).catch(err => {
                log.error(err);
            });
        })
            .catch((err) => {
            log.error(err);
        });
    }
}
exports.ConnectionSocket = ConnectionSocket;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Log_1 = require("../components/Log");
const GErrorReporting_1 = require("../components/GErrorReporting");
const log = Log_1.Log.getInstanse()(module);
class ApiHelper {
    static sendErr(res, apiResponse, err) {
        GErrorReporting_1.default.report(err);
        log.error(err.message);
        log.error(err.stack);
        apiResponse.addError(err);
        res.status(500);
        res.apiJson(apiResponse.get("Jabrool is under maintenance please try again later"));
    }
}
exports.ApiHelper = ApiHelper;
